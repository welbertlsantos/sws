﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmClienteSelecionar : frmTemplateConsulta
    {

        private Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }

        Boolean? prestador = null;
        Boolean? credenciada = null;
        Boolean? unidade = null;
        DateTime? dataCadastroFinal = null;
        Boolean? aptoContrato = null;
        bool? particular;
        bool? credenciadora;
        bool? fisica;
        bool? ativo;

        public frmClienteSelecionar(Boolean? prestador, Boolean? credenciada, Boolean? unidade, DateTime? dataCadastroFinal, Boolean? aptoContrato, bool? particular, bool? credenciadora, bool? fisica, bool? ativo)
        {
            InitializeComponent();
            validaPermissoes();
            this.prestador = prestador;
            this.credenciada = credenciada;
            this.unidade = unidade;
            this.dataCadastroFinal = dataCadastroFinal;
            this.aptoContrato = aptoContrato;
            this.particular = particular;
            this.credenciadora = credenciadora;
            this.fisica = fisica;
            this.ativo = ativo;
            ComboHelper.TipoPesquisaCliente(cbTipoPesquisa);
            ActiveControl = text_procura;
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCliente.CurrentRow == null)
                    throw new Exception("Selecione um linha!");
             
                this.Cursor = Cursors.WaitCursor;
             
                ClienteFacade clienteFacade = ClienteFacade.getInstance();
                cliente = clienteFacade.findClienteById((Int64)dgvCliente.CurrentRow.Cells[0].Value);
                
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            try
            {
                frmClientesIncluir incluirCliente = new frmClientesIncluir();
                incluirCliente.ShowDialog();

                if (incluirCliente.Cliente != null)
                {
                    text_procura.Text = incluirCliente.Cliente.RazaoSocial.ToUpper();
                    cbTipoPesquisa.SelectedValue = ApplicationConstants.RAZAO_SOCIAL;
                    montaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbTipoPesquisa_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (String.Equals(((SelectItem)cbTipoPesquisa.SelectedItem).Valor, ApplicationConstants.CNPJ))
            {
                lblChave.Text = "CNPJ do cliente";
                text_procura.Text = String.Empty;
                text_procura.Mask = "99,999,999/9999-99";

            }
            else if (String.Equals(((SelectItem)cbTipoPesquisa.SelectedItem).Valor, ApplicationConstants.CPF))
            {
                lblChave.Text = "CPF do cliente";
                text_procura.Text = String.Empty;
                text_procura.Mask = "999,999,999-99";
            }

            else if (String.Equals(((SelectItem)cbTipoPesquisa.SelectedItem).Valor, ApplicationConstants.RAZAO_SOCIAL))
            {
                lblChave.Text = "Razão Social";
                text_procura.Text = String.Empty;
                text_procura.Mask = "";
            }
            else
            {
                lblChave.Text = "Nome de Fantasia";
                text_procura.Text = String.Empty;
                text_procura.Mask = "";
            }

            text_procura.Focus();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
            btn_novo.Enabled = permissionamentoFacade.hasPermission("CLIENTE", "INCLUIR");
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.dgvCliente.Columns.Clear();
                montaDataGrid();
                text_procura.Focus();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

        public void montaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                if (String.IsNullOrEmpty(text_procura.Text.Replace(".", "").Replace("/", "").Replace("-", "").Trim()))
                {
                    if (MessageBox.Show("Sem informação para pesquisa. Isso Pode levar um tempo. Deseja continuar mesmo assim?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        montaGridCore();
                }
                else
                    montaGridCore();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        public void montaGridCore()
        {

            ClienteFacade clienteFacade = ClienteFacade.getInstance();

            Cliente cliente = new Cliente();

            if (String.Equals(((SelectItem)cbTipoPesquisa.SelectedItem).Valor, ApplicationConstants.CNPJ))
            {

                if (String.IsNullOrEmpty(text_procura.Text.Replace(".", "").Replace("/", "").Replace("-", "").Trim()))
                    throw new Exception("CNPJ não informado.");
                else if (!ValidaCampoHelper.ValidaCNPJ(text_procura.Text))
                    throw new Exception("CNPJ inválido.");

                cliente.Cnpj = text_procura.Text;
                cliente.RazaoSocial = String.Empty;
                cliente.Fantasia = String.Empty;
            }
            if (String.Equals(((SelectItem)cbTipoPesquisa.SelectedItem).Valor, ApplicationConstants.CPF))
            {

                if (String.IsNullOrEmpty(text_procura.Text.Replace(".", "").Replace("-", "").Trim()))
                    throw new Exception("CPF não informado.");
                else if (!ValidaCampoHelper.ValidaCPF(text_procura.Text))
                    throw new Exception("CPF inválido.");

                cliente.Cnpj = text_procura.Text;
                cliente.RazaoSocial = String.Empty;
                cliente.Fantasia = String.Empty;
            }

            else if (String.Equals(((SelectItem)cbTipoPesquisa.SelectedItem).Valor, ApplicationConstants.RAZAO_SOCIAL))
            {
                cliente.RazaoSocial = text_procura.Text.ToUpper();
                cliente.Cnpj = String.Empty;
                cliente.Fantasia = String.Empty;
            }
            else if (String.Equals(((SelectItem)cbTipoPesquisa.SelectedItem).Valor, ApplicationConstants.NOME_FANTASIA))
            {
                cliente.Fantasia = text_procura.Text.ToUpper();
                cliente.Cnpj = String.Empty;
                cliente.RazaoSocial = String.Empty;

            }

            if (ativo != null)
            {
                cliente.Situacao = ativo == true ? ApplicationConstants.ATIVO : ApplicationConstants.DESATIVADO;
            } 

            DataSet ds = clienteFacade.findClienteByFilter(cliente, prestador, credenciada, unidade, dataCadastroFinal, aptoContrato, particular, credenciadora, fisica);

            dgvCliente.DataSource = ds.Tables["Clientes"].DefaultView;

            dgvCliente.Columns[0].HeaderText = "Id Cliente";
            dgvCliente.Columns[0].Visible = false;

            dgvCliente.Columns[1].HeaderText = "Razão Social";
            dgvCliente.Columns[2].HeaderText = "Nome Fantasia";
            dgvCliente.Columns[3].HeaderText = "CNPJ";

            dgvCliente.Columns[4].HeaderText = "Inscrição Estadual";
            dgvCliente.Columns[4].Visible = false;

            dgvCliente.Columns[5].HeaderText = "Endereço";
            dgvCliente.Columns[5].Visible = false;

            dgvCliente.Columns[6].HeaderText = "Número";
            dgvCliente.Columns[6].Visible = false;

            dgvCliente.Columns[7].HeaderText = "Complemento";
            dgvCliente.Columns[7].Visible = false;

            dgvCliente.Columns[8].HeaderText = "Bairro";
            dgvCliente.Columns[8].Visible = false;

            dgvCliente.Columns[9].HeaderText = "Cidade";
            dgvCliente.Columns[9].Visible = false;

            dgvCliente.Columns[10].HeaderText = "CEP";
            dgvCliente.Columns[10].Visible = false;

            dgvCliente.Columns[11].HeaderText = "UF";
            dgvCliente.Columns[11].Visible = false;

            dgvCliente.Columns[12].HeaderText = "Telefone";
            dgvCliente.Columns[12].Visible = false;

            dgvCliente.Columns[13].HeaderText = "FAX";
            dgvCliente.Columns[13].Visible = false;

            dgvCliente.Columns[14].HeaderText = "Email";
            dgvCliente.Columns[14].Visible = false;

            dgvCliente.Columns[15].HeaderText = "Site";
            dgvCliente.Columns[15].Visible = false;

            dgvCliente.Columns[16].HeaderText = "Data Cadastro";
            dgvCliente.Columns[16].Visible = false;

            dgvCliente.Columns[17].HeaderText = "Responsável";
            dgvCliente.Columns[17].Visible = false;

            dgvCliente.Columns[18].HeaderText = "Cargo";
            dgvCliente.Columns[18].Visible = false;

            dgvCliente.Columns[19].HeaderText = "Documento";
            dgvCliente.Columns[19].Visible = false;

            dgvCliente.Columns[20].HeaderText = "Escopo";
            dgvCliente.Columns[20].Visible = false;

            dgvCliente.Columns[21].HeaderText = "Jornada";
            dgvCliente.Columns[21].Visible = false;

            dgvCliente.Columns[22].HeaderText = "Situação";
            dgvCliente.Columns[22].Visible = false;

            dgvCliente.Columns[23].HeaderText = "Estimativa Masc";
            dgvCliente.Columns[23].Visible = false;

            dgvCliente.Columns[24].HeaderText = "Estimativa Fem";
            dgvCliente.Columns[24].Visible = false;

            dgvCliente.Columns[25].HeaderText = "Endereço Cobrança";
            dgvCliente.Columns[25].Visible = false;

            dgvCliente.Columns[26].HeaderText = "Numero Cobrança";
            dgvCliente.Columns[26].Visible = false;

            dgvCliente.Columns[27].HeaderText = "Complemento Cobrança";
            dgvCliente.Columns[27].Visible = false;

            dgvCliente.Columns[28].HeaderText = "Bairro Cobrança";
            dgvCliente.Columns[28].Visible = false;

            dgvCliente.Columns[29].HeaderText = "Cep Cobrança";
            dgvCliente.Columns[29].Visible = false;

            dgvCliente.Columns[30].HeaderText = "Cidade Cobrança";
            dgvCliente.Columns[30].Visible = false;

            dgvCliente.Columns[31].HeaderText = "UF Cobrança";
            dgvCliente.Columns[31].Visible = false;

            dgvCliente.Columns[32].HeaderText = "Telefone Cobrança";
            dgvCliente.Columns[32].Visible = false;

            dgvCliente.Columns[33].HeaderText = "Fax Cobrança";
            dgvCliente.Columns[33].Visible = false;

            dgvCliente.Columns[34].HeaderText = "Coordenador";
            dgvCliente.Columns[34].Visible = false;

            dgvCliente.Columns[35].HeaderText = "VIP";
            dgvCliente.Columns[35].Visible = false;

            dgvCliente.Columns[36].HeaderText = "Telefone do Responsável";
            dgvCliente.Columns[36].Visible = false;

            dgvCliente.Columns[37].HeaderText = "Usa Contrato";
            dgvCliente.Columns[37].Visible = false;

            dgvCliente.Columns[38].HeaderText = "Particular";
            dgvCliente.Columns[38].Visible = false;

            dgvCliente.Columns[39].HeaderText = "Unidade";
            dgvCliente.Columns[39].Visible = false;

            dgvCliente.Columns[40].HeaderText = "Pode ser Credenciada";
            dgvCliente.Columns[40].Visible = false;

            dgvCliente.Columns[41].HeaderText = "Prestador";
            dgvCliente.Columns[41].Visible = false;

            dgvCliente.Columns[42].HeaderText = "Destaca ISS";
            dgvCliente.Columns[42].Visible = false;

            dgvCliente.Columns[43].HeaderText = "Gera Cobranca Vlr Líquido";
            dgvCliente.Columns[43].Visible = false;

            dgvCliente.Columns[44].HeaderText = "Aliquota ISS";
            dgvCliente.Columns[44].Visible = false;

        }

        private void frmClienteSelecionar_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                SendKeys.Send("{TAB}");
        }

        private void text_procura_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (!String.Equals(((SelectItem)this.cbTipoPesquisa.SelectedItem).Valor, "C"))
            //{
            //    if ((System.Windows.Forms.Keys)e.KeyChar != Keys.Back)
            //    {
            //        if (e.KeyChar.ToString().ToLower().Equals(e.KeyChar.ToString()))
            //        {
            //            e.Handled = true;
            //            this.text_procura.Text += char.ToUpper(e.KeyChar);
            //            this.text_procura.SelectionStart = text_procura.TextLength + 1;
            //        }
            //    }
            //}
            e.KeyChar = char.ToUpper(e.KeyChar);
        }

        private void dgvCliente_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btn_ok.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void dgvCliente_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (string.Equals(dgv.Rows[e.RowIndex].Cells[22].Value.ToString(), ApplicationConstants.DESATIVADO))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;

        }

        private void text_procura_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                Clipboard.SetText(text_procura.Text);
                e.Handled = true;
            }
        }
    }
}
