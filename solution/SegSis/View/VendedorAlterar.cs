﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmVendedorAlterar : frmVendedorIncluir
    {
        private bool flagAlteraVendedor;

        public bool FlagAlteraVendedor
        {
            get { return flagAlteraVendedor; }
            set { flagAlteraVendedor = value; }
        }
        
        public frmVendedorAlterar(Vendedor vendedor) :base()
        {
            InitializeComponent();
            this.Vendedor = vendedor;

            ComboHelper.unidadeFederativa(cbUf);

            textNome.Text = vendedor.Nome;
            textCpf.Text = vendedor.Cpf;
            textRG.Text = vendedor.Rg;
            textEndereco.Text = vendedor.Endereco;
            textNumero.Text = vendedor.Numero;
            textComplemento.Text = vendedor.Complemento;
            textBairro.Text = vendedor.Bairro;
            cbUf.SelectedValue = vendedor.Uf;
            this.cidade = vendedor.CidadeIbge;
            textCidade.Text = cidade != null ? cidade.Nome : string.Empty;
            textCep.Text = vendedor.Cep;
            textEmail.Text = vendedor.Email;
            textTelefone.Text = vendedor.Telefone1;
            textCelular.Text = vendedor.Telefone2;
            textComissao.Text = Convert.ToString(vendedor.Comissao);

        }

        protected override void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                this.errorIncluirVendedor.Clear();
                VendedorFacade vendedorFacade = VendedorFacade.getInstance();

                if (!this.validaCamposObrigatorio())
                    throw new Exception("Reveja os campos obrigatórios.");

                if (!validaCpf())
                    throw new Exception("Reveja o valor digitado do cpf.");

                if (!ValidaCampoHelper.ValidaCepDigitado(textCep.Text))
                    throw new Exception("Reveja o CEP digitado");

                Vendedor vendedorAltera = vendedorFacade.findVendedorByCpf(this.textCpf.Text.Replace(".", "").Replace(",", "").Replace("-", ""));

                /* verificando se o cpf já foi utilizado no sistema */
                if (vendedorAltera != null && vendedorAltera.Id != vendedor.Id)
                    throw new Exception("Cpf já utilizado em outro Vendedor.");


                this.Vendedor = new Vendedor(this.vendedor.Id, textNome.Text, textCpf.Text, textRG.Text, textEndereco.Text, textNumero.Text, textComplemento.Text, textBairro.Text, textCidade.Text, Convert.ToString(cbUf.SelectedValue), textCep.Text, textTelefone.Text, textCelular.Text, textEmail.Text, Convert.ToDecimal(textComissao.Text.Replace(".", ",")), this.vendedor.DataCadastro, ApplicationConstants.ATIVO, cidade);

                vendedor = vendedorFacade.updateVendedor(vendedor);

                MessageBox.Show("Vendedor alterado com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                flagAlteraVendedor = true;
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }

}
