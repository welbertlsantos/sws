﻿using SWS.Entidade;
using SWS.Facade;
using SWS.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmAtendimentoImprimirEncaminhamento : frmTemplateConsulta
    {

        private Aso atendimento;
        private string numeroVias;
        
        public frmAtendimentoImprimirEncaminhamento(Aso atendimento, bool termica)
        {
            InitializeComponent();
            ComboHelper.tipoImpressora(cbTipoImpressora);
            this.atendimento = atendimento;
            PermissionamentoFacade.mapaConfiguracoes.TryGetValue(ConfigurationConstans.QTD_VIAS_IMPRESSORA_TERMICA, out numeroVias);
            this.text_quantidadeVia.Text = numeroVias;
            ActiveControl = text_quantidadeVia;

            if (termica)
            {
                cbTipoImpressora.SelectedValue = "T";
                cbTipoImpressora.Enabled = false;
            }

        }

        private void btn_sim_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.Equals(((SelectItem)cbTipoImpressora.SelectedItem).Valor, "P"))
                {
                    String comando = "-jar Relatorio\\MiddlewareJasper.jar " + "Relatorio\\Report\\ENCAMINHAMENTO_EXAMES.jasper ID_ASO:" + atendimento.Id + ":INTEGER";
                    System.Diagnostics.Process.Start("Relatorio\\jre7\\bin\\java", comando);
                }
                else
                {
                    AsoFacade asoFacade = AsoFacade.getInstance();
                    asoFacade.impressaoImpressoraTermica(atendimento, text_quantidadeVia.Text);
                }
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_nao_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void text_quantidadeVia_Leave(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(this.text_quantidadeVia.Text))
                this.text_quantidadeVia.Text = this.text_quantidadeVia.Tag.ToString();
        }

        private void text_quantidadeVia_Enter(object sender, EventArgs e)
        {
            this.text_quantidadeVia.Tag = this.text_quantidadeVia.Text;
            this.text_quantidadeVia.Text = String.Empty;
        }

        private void frmAtendimentoImprimirEncaminhamento_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
            {
                SendKeys.Send("{TAB}");
            }
        }


    }
}
