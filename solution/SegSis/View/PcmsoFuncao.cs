﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.View.Resources;
using SWS.Facade;

namespace SWS.View
{
    public partial class frmPcmsoFuncao : SWS.View.frmTemplateConsulta
    {
        private Cliente cliente;

        private GheSetor gheSetor;

        private List<GheSetorClienteFuncao> gheSetorClienteFuncoes = new List<GheSetorClienteFuncao>();

        public List<GheSetorClienteFuncao> GheSetorClienteFuncoes
        {
            get { return gheSetorClienteFuncoes; }
            set { gheSetorClienteFuncoes = value; }
        }

        public static String msg1 = "Caso a função participe de alguma atividade em espaço confinado, marque essa caixa ";
        public static String msg2 = "Caso a função participe de alguma atividade em altura, marque essa caixa ";
        public static string msg3 = "Caso a função participe de alguma serviço em eletricidade, marque essa caixa ";

        public frmPcmsoFuncao(Cliente cliente, GheSetor gheSetor)
        {
            InitializeComponent();
            this.cliente = cliente;
            this.gheSetor = gheSetor;
            ActiveControl = textDescricao;
        }

        private void btIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                this.Cursor = Cursors.WaitCursor;

                foreach (DataGridViewRow dvRow in dgvFuncao.Rows)
                {
                    if ((Boolean)dvRow.Cells[4].Value == true)
                    {
                        gheSetorClienteFuncoes.Add(pcmsoFacade.insertGheSetorClienteFuncao(new GheSetorClienteFuncao(null, new ClienteFuncao((Int64)dvRow.Cells[0].Value, cliente, new Funcao((Int64)dvRow.Cells[1].Value, (String)dvRow.Cells[2].Value, (String)dvRow.Cells[3].Value, String.Empty, String.Empty), ApplicationConstants.ATIVO, null, null), gheSetor, (Boolean)dvRow.Cells[5].Value == true ? true : false, (Boolean)dvRow.Cells[6].Value == true ? true : false, ApplicationConstants.ATIVO,null, (bool)dvRow.Cells[7].Value == true ? true : false)));
                    }
                }

                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                montaDataGrid();
                textDescricao.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btNovo_Click(object sender, EventArgs e)
        {
            try
            {
                /* incluíndo uma nova função no cliente */
                frmClienteFuncaoSeleciona incluirFuncao = new frmClienteFuncaoSeleciona(cliente);
                incluirFuncao.ShowDialog();

                if (incluirFuncao.ClienteFuncao != null)
                {
                    textDescricao.Text = incluirFuncao.ClienteFuncao.Funcao.Descricao;
                    btBuscar.PerformClick();

                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void montaDataGrid()
        {
            try
            {

                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();
                this.Cursor = Cursors.WaitCursor;

                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                dgvFuncao.Columns.Clear();

                DataSet ds = clienteFacade.findFuncaoByClienteFuncaoNotInGheSetor(new ClienteFuncao(null, cliente, new Funcao(null, textDescricao.Text, String.Empty, String.Empty, String.Empty), string.Empty, null, null), gheSetor);

                dgvFuncao.ColumnCount = 4;

                dgvFuncao.Columns[0].HeaderText = "id_seg_cliente_funcao";
                dgvFuncao.Columns[0].Visible = false;

                dgvFuncao.Columns[1].HeaderText = "Código Função";
                dgvFuncao.Columns[1].ReadOnly = true;

                dgvFuncao.Columns[2].HeaderText = "Descrição";
                dgvFuncao.Columns[2].ReadOnly = true;

                dgvFuncao.Columns[3].HeaderText = "CBO";
                dgvFuncao.Columns[3].ReadOnly = true;

                DataGridViewCheckBoxColumn select = new DataGridViewCheckBoxColumn();
                select.Name = "Selec";

                dgvFuncao.Columns.Add(select);
                dgvFuncao.Columns[4].HeaderText = "Selec";
                dgvFuncao.Columns[4].ReadOnly = false;
                dgvFuncao.Columns[4].DisplayIndex = 0;

                DataGridViewCheckBoxColumn confinado = new DataGridViewCheckBoxColumn();
                confinado.Name = "Confinado";

                dgvFuncao.Columns.Add(confinado);
                dgvFuncao.Columns[5].HeaderText = "Confinado";
                dgvFuncao.Columns[5].ReadOnly = false;

                DataGridViewCheckBoxColumn altura = new DataGridViewCheckBoxColumn();
                altura.Name = "Altura";

                dgvFuncao.Columns.Add(altura);
                dgvFuncao.Columns[6].HeaderText = "Altura";
                dgvFuncao.Columns[6].ReadOnly = false;

                DataGridViewCheckBoxColumn eletricidade = new DataGridViewCheckBoxColumn();
                eletricidade.Name = "Eletricidade";

                dgvFuncao.Columns.Add(eletricidade);
                dgvFuncao.Columns[7].HeaderText = "Eletricidade";
                dgvFuncao.Columns[7].ReadOnly = false;

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    dgvFuncao.Rows.Add(Convert.ToInt64(row["id_seg_cliente_funcao"]),
                        Convert.ToInt64(row["id_funcao"]),
                        row["descricao"].ToString(),
                        row["cod_cbo"].ToString(),
                        false,
                        false,
                        false,
                        false);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void dgvFuncao_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 5)
            {
                DataGridViewCell cell = this.dgvFuncao.Rows[e.RowIndex].Cells[e.ColumnIndex];
                cell.ToolTipText = msg1;
            }

            else if (e.ColumnIndex == 6)
            {
                DataGridViewCell cell = this.dgvFuncao.Rows[e.RowIndex].Cells[e.ColumnIndex];
                cell.ToolTipText = msg2;
            }

            else if (e.ColumnIndex == 7)
            {
                DataGridViewCell cell = this.dgvFuncao.Rows[e.RowIndex].Cells[e.ColumnIndex];
                cell.ToolTipText = msg3;
            }

        }

        private void frmPcmsoFuncao_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F4)
                btIncluir.PerformClick();
        }


    }
}
