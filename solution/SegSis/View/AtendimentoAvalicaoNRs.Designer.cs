﻿namespace SWS.View
{
    partial class frmAtendimentoAvalicaoNRs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnConfirma = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.cbAltura = new SWS.ComboBoxWithBorder();
            this.cbConfinado = new SWS.ComboBoxWithBorder();
            this.lblTrabalhoAltura = new System.Windows.Forms.TextBox();
            this.lblEspacoConfinado = new System.Windows.Forms.TextBox();
            this.lblEletricidade = new System.Windows.Forms.TextBox();
            this.cbEletricidade = new SWS.ComboBoxWithBorder();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flowLayoutPanel1);
            this.scForm.Panel1MinSize = 30;
            this.scForm.Size = new System.Drawing.Size(372, 126);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.cbEletricidade);
            this.pnlForm.Controls.Add(this.lblEletricidade);
            this.pnlForm.Controls.Add(this.cbAltura);
            this.pnlForm.Controls.Add(this.cbConfinado);
            this.pnlForm.Controls.Add(this.lblTrabalhoAltura);
            this.pnlForm.Controls.Add(this.lblEspacoConfinado);
            this.pnlForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlForm.Size = new System.Drawing.Size(372, 92);
            // 
            // banner
            // 
            this.banner.Size = new System.Drawing.Size(372, 41);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnConfirma);
            this.flowLayoutPanel1.Controls.Add(this.btnFechar);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(372, 30);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btnConfirma
            // 
            this.btnConfirma.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirma.Image = global::SWS.Properties.Resources.Gravar;
            this.btnConfirma.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfirma.Location = new System.Drawing.Point(3, 3);
            this.btnConfirma.Name = "btnConfirma";
            this.btnConfirma.Size = new System.Drawing.Size(75, 23);
            this.btnConfirma.TabIndex = 3;
            this.btnConfirma.Text = "&Confirmar";
            this.btnConfirma.UseVisualStyleBackColor = true;
            this.btnConfirma.Click += new System.EventHandler(this.btnConfirma_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Image = global::SWS.Properties.Resources.close;
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(84, 3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 1;
            this.btnFechar.TabStop = false;
            this.btnFechar.Text = "Fechar";
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // cbAltura
            // 
            this.cbAltura.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAltura.BackColor = System.Drawing.Color.LightGray;
            this.cbAltura.BorderColor = System.Drawing.Color.DimGray;
            this.cbAltura.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbAltura.FormattingEnabled = true;
            this.cbAltura.Location = new System.Drawing.Point(152, 28);
            this.cbAltura.Name = "cbAltura";
            this.cbAltura.Size = new System.Drawing.Size(207, 21);
            this.cbAltura.TabIndex = 2;
            this.cbAltura.SelectedIndexChanged += new System.EventHandler(this.cbAltura_SelectedIndexChanged);
            // 
            // cbConfinado
            // 
            this.cbConfinado.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbConfinado.BackColor = System.Drawing.Color.LightGray;
            this.cbConfinado.BorderColor = System.Drawing.Color.DimGray;
            this.cbConfinado.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbConfinado.FormattingEnabled = true;
            this.cbConfinado.Location = new System.Drawing.Point(152, 8);
            this.cbConfinado.Name = "cbConfinado";
            this.cbConfinado.Size = new System.Drawing.Size(207, 21);
            this.cbConfinado.TabIndex = 1;
            this.cbConfinado.SelectedIndexChanged += new System.EventHandler(this.cbConfinado_SelectedIndexChanged);
            // 
            // lblTrabalhoAltura
            // 
            this.lblTrabalhoAltura.BackColor = System.Drawing.Color.LightGray;
            this.lblTrabalhoAltura.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTrabalhoAltura.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrabalhoAltura.Location = new System.Drawing.Point(6, 28);
            this.lblTrabalhoAltura.MaxLength = 15;
            this.lblTrabalhoAltura.Name = "lblTrabalhoAltura";
            this.lblTrabalhoAltura.ReadOnly = true;
            this.lblTrabalhoAltura.Size = new System.Drawing.Size(147, 21);
            this.lblTrabalhoAltura.TabIndex = 58;
            this.lblTrabalhoAltura.TabStop = false;
            this.lblTrabalhoAltura.Text = "Trabalho em altura";
            // 
            // lblEspacoConfinado
            // 
            this.lblEspacoConfinado.BackColor = System.Drawing.Color.LightGray;
            this.lblEspacoConfinado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEspacoConfinado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEspacoConfinado.Location = new System.Drawing.Point(6, 8);
            this.lblEspacoConfinado.MaxLength = 15;
            this.lblEspacoConfinado.Name = "lblEspacoConfinado";
            this.lblEspacoConfinado.ReadOnly = true;
            this.lblEspacoConfinado.Size = new System.Drawing.Size(147, 21);
            this.lblEspacoConfinado.TabIndex = 57;
            this.lblEspacoConfinado.TabStop = false;
            this.lblEspacoConfinado.Text = "Espaço confinado";
            // 
            // lblEletricidade
            // 
            this.lblEletricidade.BackColor = System.Drawing.Color.LightGray;
            this.lblEletricidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEletricidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEletricidade.Location = new System.Drawing.Point(6, 48);
            this.lblEletricidade.MaxLength = 15;
            this.lblEletricidade.Name = "lblEletricidade";
            this.lblEletricidade.ReadOnly = true;
            this.lblEletricidade.Size = new System.Drawing.Size(147, 21);
            this.lblEletricidade.TabIndex = 59;
            this.lblEletricidade.TabStop = false;
            this.lblEletricidade.Text = "Serv. em Eletricidade";
            // 
            // cbEletricidade
            // 
            this.cbEletricidade.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbEletricidade.BackColor = System.Drawing.Color.LightGray;
            this.cbEletricidade.BorderColor = System.Drawing.Color.DimGray;
            this.cbEletricidade.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbEletricidade.FormattingEnabled = true;
            this.cbEletricidade.Location = new System.Drawing.Point(152, 48);
            this.cbEletricidade.Name = "cbEletricidade";
            this.cbEletricidade.Size = new System.Drawing.Size(207, 21);
            this.cbEletricidade.TabIndex = 60;
            this.cbEletricidade.SelectedIndexChanged += new System.EventHandler(this.cbEletricidade_SelectedIndexChanged);
            // 
            // frmAtendimentoAvalicaoNRs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(371, 164);
            this.Name = "frmAtendimentoAvalicaoNRs";
            this.Text = "VALIDAÇÃO CARACTERISTICA DA FUNÇÃO";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmAtendimentoAvalicaoNRs_KeyDown);
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnConfirma;
        private System.Windows.Forms.Button btnFechar;
        private ComboBoxWithBorder cbAltura;
        private ComboBoxWithBorder cbConfinado;
        private System.Windows.Forms.TextBox lblTrabalhoAltura;
        private System.Windows.Forms.TextBox lblEspacoConfinado;
        private ComboBoxWithBorder cbEletricidade;
        private System.Windows.Forms.TextBox lblEletricidade;
    }
}