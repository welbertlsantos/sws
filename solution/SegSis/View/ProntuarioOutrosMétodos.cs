﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmProntuarioOutrosMetodos : frmTemplateConsulta
    {
        private string outros;

        public string Outros
        {
            get { return outros; }
            set { outros = value; }
        }

        public frmProntuarioOutrosMetodos(string str)
        {
            InitializeComponent();
            this.outros = str;
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                outros = textOutros.Text;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
