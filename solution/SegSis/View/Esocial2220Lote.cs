﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmEsocial2220Lote : frmTemplate
    {
        
        private Cliente cliente;
        private List<long> atendimentosSelecionados = new List<long>();
        private List<Aso> asos = new List<Aso>();  // lista de asos no e-social.

        //variáveis auxiliares
        int quantidadeLotes = 0;

        
        public frmEsocial2220Lote()
        {
            InitializeComponent();
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                EsocialFacade esocialFacade = EsocialFacade.getInstance();

                //Pegar os itens selecionados no na grid
                atendimentosSelecionados.Clear();

                foreach (DataGridViewRow dvRom in dgvAtendimento.Rows)
                {
                    if (Convert.ToBoolean(dvRom.Cells[14].Value) == true)
                        atendimentosSelecionados.Add((Int64)dvRom.Cells[0].Value);
                }

                if (atendimentosSelecionados.Count == 0)
                    throw new Exception("É obrigatório selecionar um atendimentos.");

                //Dividir a colecao
                List<long[]> atendimentosLote = dividirColecao(atendimentosSelecionados, 999);

                //Gravar o lote
                esocialFacade.gerarLotes(atendimentosLote, cliente);

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btnPesquisa_Click(object sender, EventArgs e)
        {
            try
            {

                /*fazendo as validações de tela para a pesquisa.
                 *se o usuario selecionou o cliente, ou o cliente e a função, ou não
                 *preencheu nenhum atributo do funcionário ou do aso então será obrigatório
                 *usar o período para o filtro.
                 */

                if (!this.periodoCriacaoFinal.Checked && !this.periodoCriacaoInicial.Checked)
                    throw new Exception("É obrigatório selecionar um período.");

                /* verificando para saber se as datas estão em formato crescente e com lógica */

                if (periodoCriacaoInicial.Checked)
                {
                    if (!periodoCriacaoFinal.Checked)
                        throw new Exception("Você deve marcar também a data de emissão final para a pesquisa");

                    if (Convert.ToDateTime(periodoCriacaoInicial.Text) > Convert.ToDateTime(periodoCriacaoFinal.Text))
                        throw new Exception("A data de emissão final não pode ser maior que a data de emissão final.");
                }

                if (this.cliente == null)
                    throw new Exception("É obrigatório selecionar um cliente.");

                this.Cursor = Cursors.WaitCursor;

                if (chk_marcarTodos.Checked == true)
                    montaDataGrid(true);
                else
                {
                    montaDataGrid(false);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar formCliente = new frmClienteSelecionar(null, null, false, null, null, null, null, null, null);
                formCliente.ShowDialog();

                if (formCliente.Cliente != null)
                {
                    cliente = formCliente.Cliente;
                    this.clienteText.Text = this.cliente.RazaoSocial + " - " + ((int)this.cliente.tipoCliente() == 1 ? ValidaCampoHelper.FormataCnpj(this.cliente.Cnpj) : ValidaCampoHelper.FormataCpf(this.cliente.Cnpj));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new Exception("Selecione primeiro o cliente.");

                cliente = null;
                clienteText.Text = string.Empty;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void chk_marcarTodos_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (chk_marcarTodos.Checked == true)
                    montaDataGrid(true);
                else
                    montaDataGrid(false);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        static List<T[]> dividirColecao<T>(IList<T> original, Int32 n)
        {
            var listlist = new List<T[]>();
            var list = new List<T>();

            var count = 0;
            for (int i = 0; i < original.Count(); i++)
            {
                var item = original[i];
                list.Add(item);

                if ((count + 1) == n || i == original.Count() - 1)
                {
                    listlist.Add(list.ToArray());
                    list.Clear();
                    count = 0;
                }
                else
                {
                    count++;
                }
            }

            return listlist;
        }

        public void montaDataGrid(bool check)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.dgvAtendimento.Columns.Clear();
                textTotal.Text = String.Empty;

                AsoFacade asoFacade = AsoFacade.getInstance();

                DataSet ds = asoFacade.findAsoByFilter(cliente, Convert.ToDateTime(periodoCriacaoInicial.Text + " 00:00:00"), Convert.ToDateTime(periodoCriacaoFinal.Text + " 23:59:59"), check);

                dgvAtendimento.DataSource = ds.Tables["Asos"].DefaultView;

                // montando ids das colunas
                dgvAtendimento.Columns[0].HeaderText = "id_aso";
                dgvAtendimento.Columns[0].Visible = false;

                dgvAtendimento.Columns[1].HeaderText = "id_cliente_funcao_funcionario";
                dgvAtendimento.Columns[1].Visible = false;

                dgvAtendimento.Columns[2].HeaderText = "Código Atendimento";
                dgvAtendimento.Columns[2].ReadOnly = true;

                dgvAtendimento.Columns[3].HeaderText = "Data";
                dgvAtendimento.Columns[3].ReadOnly = true;

                dgvAtendimento.Columns[4].HeaderText = "Nome Funcionário";
                dgvAtendimento.Columns[4].ReadOnly = true;

                dgvAtendimento.Columns[5].HeaderText = "CPF";
                dgvAtendimento.Columns[5].ReadOnly = true;

                dgvAtendimento.Columns[6].HeaderText = "Matrícula";
                dgvAtendimento.Columns[6].ReadOnly = true;

                dgvAtendimento.Columns[7].HeaderText = "Razão Social";
                dgvAtendimento.Columns[7].ReadOnly = true;

                dgvAtendimento.Columns[8].HeaderText = "CNPJ";
                dgvAtendimento.Columns[8].ReadOnly = true;

                dgvAtendimento.Columns[9].HeaderText = "id_periodicidade";
                dgvAtendimento.Columns[9].Visible = false;

                dgvAtendimento.Columns[10].HeaderText = "Periodicidade";
                dgvAtendimento.Columns[10].ReadOnly = true;

                dgvAtendimento.Columns[11].HeaderText = "Situação";
                dgvAtendimento.Columns[11].ReadOnly = true;

                dgvAtendimento.Columns[12].HeaderText = "Data Finalização";
                dgvAtendimento.Columns[12].ReadOnly = true;

                dgvAtendimento.Columns[13].HeaderText = "Laudo";
                dgvAtendimento.Columns[13].ReadOnly = true;

                dgvAtendimento.Columns[14].HeaderText = "Selec";
                dgvAtendimento.Columns[14].Width = 30;

                /* ordenaçao da grid */
                dgvAtendimento.Columns[14].DisplayIndex = 0;
                dgvAtendimento.Columns[2].DisplayIndex = 1;
                dgvAtendimento.Columns[3].DisplayIndex = 2;
                dgvAtendimento.Columns[4].DisplayIndex = 3;
                dgvAtendimento.Columns[5].DisplayIndex = 4;
                dgvAtendimento.Columns[6].DisplayIndex = 5;
                dgvAtendimento.Columns[7].DisplayIndex = 6;
                dgvAtendimento.Columns[8].DisplayIndex = 7;
                dgvAtendimento.Columns[10].DisplayIndex = 8;
                dgvAtendimento.Columns[11].DisplayIndex = 9;
                dgvAtendimento.Columns[12].DisplayIndex = 10;
                dgvAtendimento.Columns[13].DisplayIndex = 11;

                textTotal.Text = dgvAtendimento.RowCount.ToString();

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
    }
}
