﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Facade;
using SWS.Entidade;
using SWS.View.Resources;


namespace SWS.View
{
    public partial class frm_ppraGheFonteIncluir : BaseFormConsulta
    {
        private Ghe ghe = null;

        public frm_ppraGheFonteIncluir(Ghe ghe)
        {
            InitializeComponent();
            this.ghe = ghe;
            validaPermissoes();
        }

        public void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
            btn_novo.Enabled = permissionamentoFacade.hasPermission("FONTE", "INCLUIR");
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.grd_fonte.Columns.Clear();
                montaDataGrid();
                text_descricao.Focus();

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        public void montaDataGrid()
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                DataSet ds = ppraFacade.findAllFonteAtivaByGhe(new Fonte(null, text_descricao.Text.Trim(), ApplicationConstants.ATIVO, false), ghe);
                
                grd_fonte.DataSource = ds.Tables[0].DefaultView;

                grd_fonte.Columns[0].HeaderText = "IdFonte";
                grd_fonte.Columns[0].Visible = false;

                grd_fonte.Columns[1].HeaderText = "Descrição";
                grd_fonte.Columns[1].ReadOnly = true;
                grd_fonte.Columns[1].DisplayIndex = 2;

                grd_fonte.Columns[2].HeaderText = "Situação";
                grd_fonte.Columns[2].Visible = false;

                grd_fonte.Columns[3].HeaderText = "Selec";
                grd_fonte.Columns[3].DisplayIndex = 0;

                grd_fonte.Columns[4].HeaderText = "Principal?";

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_fechar_Click_1(object sender, EventArgs e)
        {
            this.Close();

        }

        private void btn_incluir_Click_1(object sender, EventArgs e)
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();
                
                this.Cursor = Cursors.WaitCursor;

                foreach (DataGridViewRow dvRow in grd_fonte.Rows)
                    if ((Boolean)dvRow.Cells[3].Value)
                        ppraFacade.incluiGheFonte(new GheFonte(null, new Fonte((Int64)dvRow.Cells[0].Value, (String)dvRow.Cells[1].Value, (String)dvRow.Cells[2].Value, false), ghe, (Boolean)dvRow.Cells[4].Value == true ? true : false, String.Empty));

                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_novo_Click_1(object sender, EventArgs e)
        {
            frmFonteIncluir formFonteIncluir = new frmFonteIncluir();
            formFonteIncluir.ShowDialog();

            if (formFonteIncluir.Fonte != null)
            {
                text_descricao.Text = formFonteIncluir.Fonte.Descricao;
                btn_incluir.PerformClick();
            }
            
        }

        private void chk_marcaDesmarca_CheckedChanged(object sender, EventArgs e)
        {
            Boolean situacao = chk_marcaDesmarca.Checked ? true : false;

            foreach (DataGridViewRow dvRow in grd_fonte.Rows)
                dvRow.Cells[3].Value = situacao;
        }

    }
}
