﻿using SWS.Entidade;
using SWS.Excecao;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmContratos : frmTemplate
    {
        private Contrato contrato;
        private Cliente cliente;
        private Contrato contratoVigente;

        private frmJustificativa formMotivoCancelamento;
        
        public frmContratos()
        {
            InitializeComponent();
            validaPermissoesTela();
            ComboHelper.cbEstadoContrato(cbSituacao);
            ComboHelper.tipoContrato(cbTipo);
            ActiveControl = textCodigo;

        }

        private void frmContratos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                SendKeys.Send("{TAB}");
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {

            frmContratoIncluirSelecionarTipo formSelecionarTipo = new frmContratoIncluirSelecionarTipo();
            formSelecionarTipo.ShowDialog();

            if (formSelecionarTipo.Contrato != null)
            {
                contrato = formSelecionarTipo.Contrato;
                montaDataGrid();
            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvContrato.CurrentRow == null)
                    throw new Exception("Selecione um contrato.");

                /* recuperando informação do contrato selecionado */

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                Contrato contratoFind = financeiroFacade.findContratoById((Int64)dgvContrato.CurrentRow.Cells[0].Value);

                /* verificando o status do contrato. 
                 * Só será possível alterar contrato ou proposta que esteja em aberto */

                if (!String.Equals(contratoFind.Situacao, ApplicationConstants.CONTRATO_GRAVADO))
                    throw new Exception("Somente contratos em contruções podem ser alterados.");

                /* verificando que tipo de produto será alterado(contrato, proposta). */

                if (contratoFind.Proposta)
                {
                    frmContratosPropostaAlterar formProposta = new frmContratosPropostaAlterar(contratoFind);
                    formProposta.ShowDialog();

                    contrato = formProposta.Proposta;
                    montaDataGrid();
                    
                }
                else
                {
                    frmContratosAlterar alterarContrato = new frmContratosAlterar(contratoFind);
                    alterarContrato.ShowDialog();

                    contrato = alterarContrato.Contrato;
                    montaDataGrid();
                    
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvContrato.CurrentRow == null)
                    throw new Exception("Selecione um contrato.");

                /* iniciando validações do contrato / proposta */

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                /* capturando o contrato */
                Contrato contratoExcluir = financeiroFacade.findContratoById((Int64)dgvContrato.CurrentRow.Cells[0].Value);

                if (MessageBox.Show("Deseja excluir o contrato selecionado?", "Atençao", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    /* verificando situação de contrato / proposta */

                    if (!String.Equals(contratoExcluir.Situacao, ApplicationConstants.CONTRATO_GRAVADO))
                        throw new Exception("Somente contratos em contruções podem ser excluídos.");

                    /* verificando se o objeto selecionado é uma proposta. Caso seja uma proposta então poderá ser excluído
                     * sem problema algum */

                    if (contratoExcluir.Proposta)
                        excluirCor(contratoExcluir);

                    /* verificando que tipo de contrato é. Se for contrato simples, então ele não poderá ser excluído. Apenas
                     * será cancelado. Se for um aditivo terá algumas validações para saber se ele pode ser excluído ou cancelado. */
                    else if (contratoExcluir.ContratoPai == null && contratoExcluir.ContratoRaiz == null)
                    {
                        /* o contrato é um contrato padrão e não pode ser excluído. Ele poderá ser cancelado caso não
                         * tenha dependencia em outros contratos*/

                        if (!financeiroFacade.verificaUltimoContrato(contratoExcluir))
                            throw new ContratoException(ContratoException.msg6);
                        else
                            cancelaCor(contratoExcluir);
                    }

                    /* nesse caso o contrato é um aditivo e deverá ser verificado algumas informação para proceder com a exclusão ou com o cancelamento */
                    /* 
                     * Se o aditivo for o ultimo aditivo vigente e estiver em construção então ele poderá ser excluído sem problemas. 
                    */

                    else if (financeiroFacade.verificaUltimoContrato(contrato))
                    {
                        if (String.Equals(contratoExcluir.Situacao, ApplicationConstants.CONTRATO_GRAVADO))
                            excluirCor(contratoExcluir);
                        else
                        {
                            cancelaCor(contratoExcluir);
                            /* nesse caso o aditivo está fechado e ele será cancelado, porém o contrato anterior a ele deverá passar para
                             * fechado caso tenha um período de vigencia ainda permitido */

                            if (!String.IsNullOrEmpty(this.formMotivoCancelamento.Justificativa))
                            {
                                /* alterando o contrato pai para o status de fechado novamente */
                                financeiroFacade.changeStatusContrato(contratoExcluir);
                                MessageBox.Show("Contrato anterior foi alterado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                btnPesquisar.PerformClick();

                            }
                        }
                    }

                    /* nesse caso o contrato é um aditivo porém não é o último aditivo vigente. Então a exclusão e /ou cancelamento
                     * não pode ser permitida */
                    else
                        throw new ContratoException(ContratoException.msg6);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnValidar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvContrato.CurrentRow == null)
                    throw new Exception("Selecione um contrato.");

                /* verificando que tipo de produto e ação será tomada. Se for contrato então o contrato será alterado
                 * para finalizado. Se for proposta então a proposta será transformada em um contrato */

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                Contrato contratoValidar = financeiroFacade.findContratoById((Int64)dgvContrato.CurrentRow.Cells[0].Value);

                /* verificando o status do contrato / proposta. Somente contrato em construção pode ser validado */
                if (!String.Equals(contratoValidar.Situacao, ApplicationConstants.CONTRATO_GRAVADO))
                    throw new Exception("Somente contratos em construção podem ser validados.");

                if (!contratoValidar.Proposta)
                {
                    #region contrato

                    if (MessageBox.Show("Deseja validar o contrato selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {

                        /* verificando se o contrato pode ser validado. Levando em conta se existe algum contrato 
                         * vigente. Caso exista então será informado ao usuário que existe um contrato vigente e que esse
                         * contrato deverá ser encerrado para que o contrato atual seja finalizado. */

                        contratoVigente = financeiroFacade.verificaContratoVigente((DateTime)contratoValidar.DataInicio, (DateTime?)contratoValidar.DataFim, contratoValidar.Cliente, contratoValidar.Prestador);

                        if (contratoVigente != null)
                            MessageBox.Show("Existe um contrato em vigência para o período informado. Você deve encerrá-lo para poder finalizar esse contrato ou alterar as datas do contrato que deseja validar. " + System.Environment.NewLine + "O contrato é o de código : " + (!String.IsNullOrEmpty(contratoVigente.CodigoContrato) ? ValidaCampoHelper.RetornaCodigoEstudoFormatado(contratoVigente.CodigoContrato) : "Contrato antigo. Verificar os contratos do cliente"), "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        else
                        {
                            contratoValidar.Situacao = ApplicationConstants.CONTRATO_FECHADO;
                            contratoValidar.UsuarioFinalizou = PermissionamentoFacade.usuarioAutenticado;
                            financeiroFacade.validarContrato(contratoValidar);
                            MessageBox.Show("Contrato validado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            montaDataGrid();
                        }

                    }

                    #endregion
                }
                else
                {
                    #region proposta

                    /* nessa condição o produto é uma proposta e será transformado em contrato */

                    if (MessageBox.Show("Deseja validar a proposta e transformá-la em um contrato?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {

                        /* verificando se já existe algum contrato vigente no período de vigência da proposta se a proposta foi feita para
                         * um cliente do sistema.*/

                        if (contratoValidar.Cliente != null)
                            contratoVigente = financeiroFacade.findContratoVigenteByData((DateTime)contratoValidar.DataInicio, contratoValidar.Cliente, null);

                        if (contratoVigente == null)
                        {
                            financeiroFacade.validaProposta(contratoValidar);
                            MessageBox.Show("Proposta validada com sucesso. Será aberto a tela de contrato para concluir a validação.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            frmContratosAlterar formContrato = new frmContratosAlterar(financeiroFacade.findContratoById((Int64)contratoValidar.Id));
                            formContrato.ShowDialog();
                        }
                        else
                        {
                            /* o usuário selecionou efetivar a proposta porém existe um contrato vigente nesse período.
                             * Avisar ao usuário que ele deve colocar a vigência da proposta para outra data */

                            MessageBox.Show("Você deverá alterar a data de início e fim do novo contrato porque já existe um contrato em vigência nesse período." + System.Environment.NewLine + "A data de início e fim do contrato atual é: " + contratoVigente.DataInicio.ToString() + " e " + contratoVigente.DataFim.ToString(), "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            financeiroFacade.validaProposta(contratoValidar);

                            frmContratosAlterar formContrato = new frmContratosAlterar(financeiroFacade.findContratoById((Int64)contratoValidar.Id));
                            formContrato.ShowDialog();

                        }
                    }
                    #endregion
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnEncerrar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvContrato.CurrentRow == null)
                    throw new Exception("Selecione um contrato.");

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                Contrato contratoEncerrar = financeiroFacade.findContratoById((Int64)dgvContrato.CurrentRow.Cells[0].Value);

                /* verificando se o contrato pode ser encerrado */
                if (!String.Equals(contratoEncerrar.Situacao, ApplicationConstants.CONTRATO_FECHADO))
                    throw new Exception("Somente contratos validados podem ser encerrados.");

                /* verificando com o usuário se deseja encerrar o contrato. */
                if (MessageBox.Show("Deseja encerrar o contrato selecionado?" + System.Environment.NewLine + " O contrato será encerrado com a data de Final igual a: " + DateTime.Now.ToString(), "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
                {
                    /* solicitando ao usuário o motivo do encerramento do contrato */

                    frmJustificativa motivoEncerramento = new frmJustificativa("MOTIVO DE CANCELAMENTO");
                    motivoEncerramento.ShowDialog();

                    if (!String.IsNullOrEmpty(motivoEncerramento.Justificativa))
                    {
                        contratoEncerrar.MotivoEncerramento = motivoEncerramento.Justificativa;
                        contratoEncerrar.DataEncerramento = DateTime.Now;

                        /* verificando se a data de início será maior que a data fim. Nesse caso será ajustado para
                         * a data inicio + 1 minutos. */

                        if ((DateTime)contratoEncerrar.DataInicio > DateTime.Now)
                            contrato.DataFim = Convert.ToDateTime(contratoEncerrar.DataInicio).AddMinutes(1);
                        else
                            contratoEncerrar.DataFim = DateTime.Now;

                        contratoEncerrar.UsuarioEncerrou = PermissionamentoFacade.usuarioAutenticado;

                        financeiroFacade.encerraContrato(contratoEncerrar);

                        MessageBox.Show("Contrato encerrado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        montaDataGrid();

                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (dgvContrato.CurrentRow == null)
                    throw new Exception("Selecione um contrato.");

                /* recuperando informação do contrato */
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                Empresa empresa = PermissionamentoFacade.usuarioAutenticado.Empresa;
                Contrato contratoFind = financeiroFacade.findContratoById((Int64)dgvContrato.CurrentRow.Cells[0].Value);

                /* verificando que tipo de produto será alterada: contrato ou proposta. */

                if (contratoFind.Proposta)
                {
                    frmContratosPropostaConsulta formProposta = new frmContratosPropostaConsulta(contratoFind);
                    formProposta.ShowDialog();
                }
                else
                {
                    frmContratosConsulta formContrato = new frmContratosConsulta(contratoFind, empresa);
                    formContrato.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.Default;

                if (validaDados())
                {
                    contrato = new Contrato(null, textCodigo.Text, cliente, null, dtElaboracaoInicial.Checked ? (DateTime?)(Convert.ToDateTime(dtElaboracaoInicial.Text + " 00:00:00")) : null, dtVencimentoInicial.Checked ? (DateTime?)(Convert.ToDateTime(dtVencimentoInicial.Text + " 00:00:00")) : null, dtFechamentoInicial.Checked ? (DateTime?)(Convert.ToDateTime(dtFechamentoInicial.Text + " 00:00:00")) : null, cbSituacao.SelectedIndex == -1 ? String.Empty : ((SelectItem)cbSituacao.SelectedItem).Valor, String.Empty, null, null, string.Empty, null, false, dtCancelamentoInicial.Checked ? (DateTime?)(Convert.ToDateTime(dtCancelamentoInicial.Text + " 00:00:00")) : null, null, false, false, false, null, false, null, null, null, null, null, string.Equals(((SelectItem)cbTipo.SelectedItem).Valor, "P") ? true : false, null, null, null, String.Empty, null);
                    montaDataGrid();
                    textCodigo.Focus();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            textCodigo.Text = String.Empty;
            ComboHelper.cbEstadoContrato(cbSituacao);
            ComboHelper.tipoContrato(cbTipo);
            textCliente.Text = String.Empty;
            cliente = null;
            contrato = null;
            dtElaboracaoInicial.Checked = false;
            dtElaboracaoFinal.Checked = false;
            dtFechamentoInicial.Checked = false;
            dtFechamentoFinal.Checked = false;
            dtVencimentoInicial.Checked = false;
            dtVencimentoFinal.Checked = false;
            dgvContrato.Columns.Clear();
            dgvAditivo.Columns.Clear();
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            try
            {

                if (dgvContrato.CurrentRow == null)
                    throw new Exception("Selecione um contrato para impressão.");

                this.Cursor = Cursors.WaitCursor;

                /* verificando que tipo será impresso. Contrato ou proposta */

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                Contrato contratoImprimir = financeiroFacade.findContratoById((Int64)dgvContrato.CurrentRow.Cells[0].Value);

                /* somente contratos que não foram excluidos poderão ser impressos */

                if (String.Equals(contratoImprimir.Situacao, ApplicationConstants.CONTRATO_CANCELADO) || String.Equals(contratoImprimir.Situacao, ApplicationConstants.CONTRATO_ENCERRADO))
                    throw new ContratoException(ContratoException.msg7);

                String tipo = contratoImprimir.Proposta ? ApplicationConstants.PROPOSTA : ApplicationConstants.CONTRATO;


                if (String.Equals(tipo, ApplicationConstants.PROPOSTA))
                {
                    /* impressão de proposta */

                    var process = new System.Diagnostics.Process();

                    process.StartInfo.FileName = "Relatorio\\jre7\\bin\\java";
                    process.StartInfo.Arguments = " -jar Relatorio\\MiddlewareJasper.jar " + "Relatorio\\Report\\" + "PROPOSTA.JASPER" + " ID_CONTRATO:" + contratoImprimir.Id + ":Integer";
                    process.StartInfo.UseShellExecute = false;
                    process.StartInfo.RedirectStandardError = true;
                    process.StartInfo.RedirectStandardInput = true;
                    process.StartInfo.RedirectStandardOutput = true;
                    process.StartInfo.CreateNoWindow = true;

                    process.Start();
                    process.StandardOutput.ReadToEnd();
                    process.WaitForExit();


                }
                else
                {
                    /* impressão de contrato */

                    var process = new System.Diagnostics.Process();

                    process.StartInfo.FileName = "Relatorio\\jre7\\bin\\java";
                    process.StartInfo.Arguments = " -jar Relatorio\\MiddlewareJasper.jar " + "Relatorio\\Report\\" + "CONTRATO.JASPER" + " ID_CONTRATO:" + contratoImprimir.Id + ":Integer";
                    process.StartInfo.UseShellExecute = false;
                    process.StartInfo.RedirectStandardError = true;
                    process.StartInfo.RedirectStandardInput = true;
                    process.StartInfo.RedirectStandardOutput = true;
                    process.StartInfo.CreateNoWindow = true;

                    process.Start();
                    process.StandardOutput.ReadToEnd();
                    process.WaitForExit();


                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void validaPermissoesTela()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            this.btnIncluir.Enabled = permissionamentoFacade.hasPermission("CONTRATO_COMERCIAL", "INCLUIR");
            this.btnExcluir.Enabled = permissionamentoFacade.hasPermission("CONTRATO_COMERCIAL", "EXCLUIR");
            this.btnAlterar.Enabled = permissionamentoFacade.hasPermission("CONTRATO_COMERCIAL", "ALTERAR");
            this.btnConsultar.Enabled = permissionamentoFacade.hasPermission("CONTRATO_COMERCIAL", "DETALHAR");
            this.btnImprimir.Enabled = permissionamentoFacade.hasPermission("CONTRATO_COMERCIAL", "IMPRIMIR");
            this.btnValidar.Enabled = permissionamentoFacade.hasPermission("CONTRATO_COMERCIAL", "FINALIZAR");
            this.btnEncerrar.Enabled = permissionamentoFacade.hasPermission("CONTRATO_COMERCIAL", "ENCERRAR");

        }

        private void btnCliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar formCliente = new frmClienteSelecionar(null, null, false, null, true, null, null, null, null);
                formCliente.ShowDialog();

                if (formCliente.Cliente != null)
                {
                    cliente = formCliente.Cliente;
                    textCliente.Text = cliente.RazaoSocial;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool validaDados()
        {
            bool retorno = true;

            try
            {
                if (dtCancelamentoInicial.Checked)
                {
                    if (!dtCancelamentoInicial.Checked)
                        throw new Exception("Selecione a data de cancelamento final.");

                    if (Convert.ToDateTime(dtCancelamentoFinal.Text) < Convert.ToDateTime(dtCancelamentoInicial.Text))
                        throw new Exception("A data de cancelamento final não pode ser menor que a data de cancelamento inicial.");
                }

                if (dtCancelamentoFinal.Checked)
                {
                    if (!dtCancelamentoInicial.Checked)
                        throw new Exception("Selecione a data de cancelamento Inicial.");
                    
                    if (Convert.ToDateTime(dtCancelamentoFinal.Text) < Convert.ToDateTime(dtCancelamentoInicial.Text))
                        throw new Exception("A data de cancelamento final não pode ser menor que a data de cancelamento inicial.");

                }

                if (dtElaboracaoInicial.Checked)
                {
                    if (!dtElaboracaoFinal.Checked)
                        throw new Exception("Selecione a data de elaboracao final.");

                    if (Convert.ToDateTime(dtElaboracaoFinal.Text) < Convert.ToDateTime(dtElaboracaoInicial.Text))
                        throw new Exception("A data de elaboracao final não pode ser menor que a data de elaboracao inicial. ");
                    
                }

                if (dtElaboracaoFinal.Checked)
                {
                    if (!dtElaboracaoInicial.Checked)
                        throw new Exception("Selecione a data de elaboracao inicial.");

                    if (Convert.ToDateTime(dtElaboracaoFinal.Text) < Convert.ToDateTime(dtElaboracaoInicial.Text))
                        throw new Exception("A data de elaboracao final não pode ser menor que a data de elaboracao inicial. ");

                }

                if (dtVencimentoInicial.Checked)
                {
                    if (!dtVencimentoFinal.Checked)
                        throw new Exception("Selecione a data de vencimento final.");
                    
                    if (Convert.ToDateTime(dtVencimentoFinal.Text) < Convert.ToDateTime(dtVencimentoInicial.Text))
                        throw new Exception("A data de vencimento final não pode ser menor que a data de vencimento inicial.");
                }

                if (dtVencimentoFinal.Checked)
                {
                    if (!dtVencimentoInicial.Checked)
                        throw new Exception("Selecione a data de vencimento inicial.");

                    if (Convert.ToDateTime(dtVencimentoFinal.Text) < Convert.ToDateTime(dtVencimentoInicial.Text))
                        throw new Exception("A data de vencimento final não pode ser menor que a data de vencimento inicial.");
                }

                if (dtFechamentoInicial.Checked)
                {
                    if (!dtFechamentoFinal.Checked)
                        throw new Exception("Selecione a data de fechamento final.");

                    if (Convert.ToDateTime(dtFechamentoFinal.Text) < Convert.ToDateTime(dtFechamentoInicial.Text))
                        throw new Exception("A data de fechamento final não pode ser menor que a data de fechamento inicial.");
                }

                if (dtFechamentoFinal.Checked)
                {
                    if (!dtFechamentoInicial.Checked)
                        throw new Exception("Selecione a data de fechamento inicial.");

                    if (Convert.ToDateTime(dtFechamentoFinal.Text) < Convert.ToDateTime(dtFechamentoInicial.Text))
                        throw new Exception("A data de fechamento final não pode ser menor que a data de fechamento inicial.");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                retorno = false;
            }

            return retorno;
        }

        private void dgvContrato_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.ClearSelection();
        }

        private void dgvAditivo_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.ClearSelection();
        }

        private void montaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                dgvContrato.Columns.Clear();
                dgvAditivo.Columns.Clear();

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                DataSet ds = financeiroFacade.findContratoByFilter(contrato, dtElaboracaoFinal.Checked ? Convert.ToDateTime(dtElaboracaoFinal.Text + " 23:59:59") : (DateTime?)null, dtVencimentoFinal.Checked ? Convert.ToDateTime(dtVencimentoFinal.Text + " 23:59:59") : (DateTime?)null, dtFechamentoFinal.Checked ? Convert.ToDateTime(dtFechamentoFinal.Text + " 23:59:59") : (DateTime?)null, dtCancelamentoFinal.Checked ? Convert.ToDateTime(dtCancelamentoFinal.Text + " 23:59:59") : (DateTime?)null);

                dgvContrato.DataSource = ds.Tables[0].DefaultView;

                dgvContrato.Columns[0].HeaderText = "id_contrato";
                dgvContrato.Columns[0].Visible = false;

                dgvContrato.Columns[1].HeaderText = "Código";
                dgvContrato.Columns[1].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvContrato.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvContrato.Columns[1].DefaultCellStyle.BackColor = Color.LightYellow;

                dgvContrato.Columns[2].HeaderText = "id_cliente";
                dgvContrato.Columns[2].Visible = false;

                dgvContrato.Columns[3].HeaderText = "Cliente";

                dgvContrato.Columns[4].HeaderText = "CNPJ";
                dgvContrato.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvContrato.Columns[5].HeaderText = "Data de Elaboração";
                dgvContrato.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvContrato.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvContrato.Columns[6].HeaderText = "Data Fim de Vigência";
                dgvContrato.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvContrato.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvContrato.Columns[7].HeaderText = "Data Início de Vigência";
                dgvContrato.Columns[7].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvContrato.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvContrato.Columns[8].HeaderText = "Situação";
                dgvContrato.Columns[8].Visible = false;

                dgvContrato.Columns[9].HeaderText = "Observação";
                dgvContrato.Columns[9].Visible = false;

                dgvContrato.Columns[10].HeaderText = "id_usuaruio_criador";
                dgvContrato.Columns[10].Visible = false;

                dgvContrato.Columns[11].HeaderText = "Usuario que criou";

                dgvContrato.Columns[12].HeaderText = "id_usuario_finalizou";
                dgvContrato.Columns[12].Visible = false;

                dgvContrato.Columns[13].HeaderText = "Usuario que finalizou";

                dgvContrato.Columns[14].HeaderText = "id_usuario_cancelou";
                dgvContrato.Columns[14].Visible = false;

                dgvContrato.Columns[15].HeaderText = "Usuario que cancelou";

                dgvContrato.Columns[16].HeaderText = "motivo cancelamento";
                dgvContrato.Columns[16].Visible = false;

                dgvContrato.Columns[17].HeaderText = "id_cliente_prestador";
                dgvContrato.Columns[17].Visible = false;

                dgvContrato.Columns[18].HeaderText = "Prestador";

                dgvContrato.Columns[19].HeaderText = "Data de Cancelamento";
                dgvContrato.Columns[19].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvContrato.Columns[19].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvContrato.Columns[20].HeaderText = "id_contrato_pai";
                dgvContrato.Columns[20].Visible = false;

                dgvContrato.Columns[21].HeaderText = "id_contrato_raiz";
                dgvContrato.Columns[21].Visible = false;

                dgvContrato.Columns[22].HeaderText = "id_cliente_proposta";
                dgvContrato.Columns[22].Visible = false;

                dgvContrato.Columns[23].HeaderText = "id_usuario_encerrou";
                dgvContrato.Columns[23].Visible = false;
                
                dgvContrato.Columns[24].HeaderText = "Usuario que Encerrou";

                dgvContrato.Columns[25].HeaderText = "Data de Encerramento";
                dgvContrato.Columns[25].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvContrato.Columns[25].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                
                dgvContrato.Columns[26].HeaderText = "Cliente Proposta";
                

                /* acertando os indices */


                dgvContrato.Columns[1].DisplayIndex = 1; //codigo do contrato
                dgvContrato.Columns[3].DisplayIndex = 2; //cliente
                dgvContrato.Columns[4].DisplayIndex = 3; //cnpj
                dgvContrato.Columns[26].DisplayIndex = 4; //cliente_proposta
                dgvContrato.Columns[5].DisplayIndex = 5; // data elaboracao
                dgvContrato.Columns[7].DisplayIndex = 6; // data_inicio
                dgvContrato.Columns[6].DisplayIndex = 7; // data_fim
                dgvContrato.Columns[25].DisplayIndex = 8; // data_encerramento
                dgvContrato.Columns[19].DisplayIndex = 9;// data_cancelamento
                dgvContrato.Columns[11].DisplayIndex = 10; //Usuario que criou
                dgvContrato.Columns[13].DisplayIndex = 11; //Usuario que finalizou
                dgvContrato.Columns[15].DisplayIndex = 12; //Usuario que cancelou
                dgvContrato.Columns[24].DisplayIndex = 13; //Usuario que encerrou
                dgvContrato.Columns[18].DisplayIndex = 14; //Prestador

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void montaDataGridAditivos(DataSet ds)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                dgvAditivo.Columns.Clear();

                /* preenchendo grid principal */

                dgvAditivo.DataSource = ds.Tables["Aditivos"].DefaultView;

                dgvAditivo.DefaultCellStyle.BackColor = Color.LightGray;

                dgvAditivo.Columns[0].HeaderText = "id_contrato";
                dgvAditivo.Columns[0].Visible = false;

                dgvAditivo.Columns[1].HeaderText = "Código";
                dgvAditivo.Columns[1].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvAditivo.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvAditivo.Columns[1].DefaultCellStyle.BackColor = Color.LightYellow;

                dgvAditivo.Columns[2].HeaderText = "id_cliente";
                dgvAditivo.Columns[2].Visible = false;

                dgvAditivo.Columns[3].HeaderText = "Cliente";

                dgvAditivo.Columns[4].HeaderText = "CNPJ";
                dgvAditivo.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvAditivo.Columns[5].HeaderText = "Data de Elaboração";
                dgvAditivo.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvAditivo.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvAditivo.Columns[6].HeaderText = "Data Início de Vigência";
                dgvAditivo.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvAditivo.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvAditivo.Columns[7].HeaderText = "Data Fim de Vigência";
                dgvAditivo.Columns[7].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvAditivo.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvAditivo.Columns[8].HeaderText = "Situação";
                dgvAditivo.Columns[8].Visible = false;

                dgvAditivo.Columns[9].HeaderText = "Observação";
                dgvAditivo.Columns[9].Visible = false;

                dgvAditivo.Columns[10].HeaderText = "id_usuaruio_criador";
                dgvAditivo.Columns[10].Visible = false;

                dgvAditivo.Columns[11].HeaderText = "Usuario criador";

                dgvAditivo.Columns[12].HeaderText = "id_usuario_finalizou";
                dgvAditivo.Columns[12].Visible = false;

                dgvAditivo.Columns[13].HeaderText = "Usuario Finalizador";

                dgvAditivo.Columns[14].HeaderText = "id_usuario_cancelou";
                dgvAditivo.Columns[14].Visible = false;

                dgvAditivo.Columns[15].HeaderText = "Usuario Cancelamento";

                dgvAditivo.Columns[16].HeaderText = "motivo cancelamento";
                dgvAditivo.Columns[16].Visible = false;

                dgvAditivo.Columns[17].HeaderText = "id_cliente_prestador";
                dgvAditivo.Columns[17].Visible = false;

                dgvAditivo.Columns[18].HeaderText = "Prestador";

                dgvAditivo.Columns[19].HeaderText = "Data Cancelamento";
                dgvAditivo.Columns[19].DisplayIndex = 8;
                dgvAditivo.Columns[19].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvAditivo.Columns[19].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvAditivo.Columns[20].HeaderText = "id_contrato_pai";
                dgvAditivo.Columns[20].Visible = false;

                dgvAditivo.Columns[21].HeaderText = "id_contrato_raiz";
                dgvAditivo.Columns[21].Visible = false;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void excluirCor(Contrato contrato)
        {
            try
            {
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                financeiroFacade.deleteContrato(contrato);
                MessageBox.Show("Contrato ou proposta excluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                montaDataGrid();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cancelaCor(Contrato contrato)
        {
            try
            {
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                frmJustificativa formCancelamento = new frmJustificativa("MOTIVO DE CANCELAMENTO");
                formCancelamento.ShowDialog();

                if (!String.IsNullOrEmpty(formCancelamento.Justificativa))
                {
                    contrato.MotivoCancelamento = formCancelamento.Justificativa;
                    contrato.UsuarioCancelamento = PermissionamentoFacade.usuarioAutenticado;
                    contrato.DataCancelamento = DateTime.Now;
                    this.Cursor = Cursors.WaitCursor;
                    financeiroFacade.cancelaContrato(contrato);
                    MessageBox.Show("Contrato / Proposta cancelado(a) com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    btnPesquisar.PerformClick();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvContrato_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (String.Equals(dgv.Rows[e.RowIndex].Cells[8].Value, ApplicationConstants.CONTRATO_CANCELADO))
            {
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgv.Rows[e.RowIndex].DefaultCellStyle.Font = new System.Drawing.Font(this.Font, FontStyle.Strikeout);
            }
            else if ((String.Equals(dgv.Rows[e.RowIndex].Cells[8].Value, ApplicationConstants.CONTRATO_ENCERRADO)) ||
                (!String.IsNullOrEmpty(dgv.Rows[e.RowIndex].Cells[6].Value.ToString()) &&
                (Convert.ToDateTime(dgv.Rows[e.RowIndex].Cells[6].Value) < Convert.ToDateTime(DateTime.Now))))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Blue;

            else if (String.Equals(dgv.Rows[e.RowIndex].Cells[8].Value, ApplicationConstants.CONTRATO_FECHADO)
                && (String.IsNullOrEmpty(dgv.Rows[e.RowIndex].Cells[6].Value.ToString())
                || Convert.ToDateTime(dgv.Rows[e.RowIndex].Cells[6].Value) > Convert.ToDateTime(DateTime.Now)))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Green;
        }

        private void dgvAditivo_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgvAditivo.CurrentRow == null)
                    throw new Exception("Selecione um aditivo.");

                /* recuperando informação do contrato selecionado */

                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                Contrato contratoFind = financeiroFacade.findContratoById((Int64)dgvAditivo.CurrentRow.Cells[0].Value);

                /* verificando o status do contrato. 
                 * Só será possível alterar aditivo que esteja em construção */

                if (!String.Equals(contratoFind.Situacao, ApplicationConstants.CONTRATO_GRAVADO))
                    throw new Exception("Somente aditivos em construções podem ser alterados.");

                frmContratosAlterar alterarContrato = new frmContratosAlterar(contratoFind);
                alterarContrato.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvAditivo_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (String.Equals(dgv.Rows[e.RowIndex].Cells[8].Value, ApplicationConstants.CONTRATO_CANCELADO))
            {
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgv.Rows[e.RowIndex].DefaultCellStyle.Font = new System.Drawing.Font(this.Font, FontStyle.Strikeout);
            }
            else if (String.Equals(dgv.Rows[e.RowIndex].Cells[8].Value, ApplicationConstants.CONTRATO_ENCERRADO))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Blue;
            else if (String.Equals(dgv.Rows[e.RowIndex].Cells[8].Value, ApplicationConstants.CONTRATO_FECHADO))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Green;
        }

        private void dgvContrato_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgvContrato.Rows.Count > 0)
                {
                    FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();
                    DataSet ds = financeiroFacade.findAditivoByContrato(new Contrato((Int64)dgvContrato.Rows[e.RowIndex].Cells[0].Value));
                    montaDataGridAditivos(ds);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvContrato_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            dgvContrato_CellContentClick(sender, e);
        }

        private void btnExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new Exception("Você deve selecionar primeiro o cliente.");

                cliente = null;
                textCliente.Text = string.Empty;
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvContrato_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btnAlterar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnCopiar_Click(object sender, EventArgs e)
        {
            frmContratoCopiar copiarContrato = new frmContratoCopiar();
            copiarContrato.ShowDialog();
        }
    }
}
