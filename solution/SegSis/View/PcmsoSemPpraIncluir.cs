﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.View.ViewHelper;
using SWS.View.Resources;
using SWS.Facade;
using SWS.ViewHelper;
using SWS.Excecao;

namespace SWS.View
{
    public partial class frm_PcmsoSemPpraIncluir : BaseForm
    {

        public static String msg1 = "Selecione uma linha.";
        public static String msg2 = "Atenção!";
        public static String msg3 = "Cronograma Alterado com sucesso.";
        public static String msg4 = "Confirmação";
        public static String msg5 = "Cronograma incluído com sucesso";
        public static String msg6 = "Deseja excluir essa atividade?";
        public static String msg7 = "Material Hospitalar Salvo com sucesso.";
        public static String msg8 = "Deseja excluir o material hospitalar?";
        public static String msg9 = "Material hospitalar excluído com sucesso.";
        public static String msg10 = "Deseja excluir esse hospital?";
        public static String msg11 = "Hospital excluído com sucesso.";
        public static String msg12 = "Deseja excluir o médico?";
        public static String msg13 = "Médico excluído com sucesso.";
        public static String msg14 = "Deseja excluir esse conteúdo?";
        public static String msg15 = "Conteúdo excluído com sucesso.";
        public static String msg16 = "Deseja excluir o exame selecionado?";
        public static String msg17 = "Cadastre um GHE primeiro.";

        

        public Ghe gheSelecionado;
        public CronogramaAtividade cronogramaAtividadeProcurado = new CronogramaAtividade();
        public Int64 idGheFonteAgente;
        private Int64 idGheFonteAgenteExame;
        private String controleDigitacao;

        private Cliente clienteContratanteCheck = new Cliente();

        //Fim atributos Exames

        private frm_pcmso formPcmso;

        public Estudo pcmso = new Estudo();

        // relacionado ao cronograma do estudo
        public Cronograma cronograma = new Cronograma();
        public HashSet<CronogramaAtividade> listCronogramaAtividade = new HashSet<CronogramaAtividade>();

        // relacionado aos ghes do estudo
        public Ghe ghe;

        public GheFonte gheFonte = new GheFonte();

        // relacinando agente ao ghe fonte
        public GheFonteAgente gheFonteAgente = new GheFonteAgente();

        // relacionado ao GheSetor do estudo
        public GheSetor gheSetor = new GheSetor();
        
        // relacionado ao GheSetorFuncao do estudo
        public GheSetorClienteFuncao gheSetorClienteFuncao = new GheSetorClienteFuncao(null, null, null, false, false, String.Empty, null, false);
        
        #region Mensagem da classe

        public static String Msg01 = " Confirmação ";
        public static String Msg02 = " PCMSO ";
        public static String Msg03 = " gravado com sucesso ";
        public static String Msg04 = " PCMSO alterado com sucesso";
        public static String Msg05 = " Cronograma do PCMSO incluído com sucesso";
        public static String Msg06 = " Não é possível incluir cronograma sem descrição";
        public static String Msg07 = " Selecione uma linha!";
        public static String Msg08 = " Atenção";
        public static String Msg09 = " Cronograma do PCMSO alterado com sucesso";
        public static String Msg10 = " Deseja excluir esta atividade do cronograma? ";
        public static String Msg11 = " Atividade excluída do cronograma com sucesso ";
        public static String Msg12 = " GHE incluído com sucesso";
        public static String Msg13 = " Não é possível criar um PPRA novo sem um cliente ";

        public static String Msg14 = " Ghe Alterado com sucesso";
        public static String Msg15 = " Deseja realmente excluir o GHE? ";
        public static String Msg16 = " Essa ação excluirá todos os relacionamentos.";
        public static String Msg17 = " GHE excluído com sucesso ";
        public static String Msg18 = " Deseja realmente excluir a Fonte?";
        public static String Msg19 = " Essa acao excluirá a(s) fonte(s) do GHE ";
        public static String Msg20 = " Fonte excluída com sucesso ";
        public static String Msg21 = " Selecione um GHE";
        public static String Msg22 = " Selecione uma fonte";
        public static String Msg23 = " Deseja excluir o agente?";
        public static String Msg24 = " Agente(s) excluídos com sucesso";

        public static String Msg25 = " Deseja realmente excluir o Epi?";
        public static String Msg26 = " Epi Excluído com sucesso!";

        public static String Msg27 = " Deseja realmente excluir o setor? ";
        public static String Msg28 = " Essa acao excluirá o(s) setor(es) do GHE";
        public static String Msg29 = " Setor(es) excluído(s) com sucesso";

        public static String Msg30 = " Deseja realmente excluir a função do cliente? ";
        public static String Msg31 = " Essa ação desativará a função ";
        public static String Msg32 = " Função(ões) excluído(s) com sucesso";
        public static String Msg33 = " Selecione um setor";
        public static String Msg34 = " Deseja realmente excluir a função do Setor? ";
        public static String Msg35 = " Função do Setor excluída com sucesso";

        public static String msg36 = "Deseja excluir esse conteúdo?";
        public static String msg37 = "Conteúdo excluido com sucesso";

        public static String msg38 = " Selecione um Exame.";

        public static String msg39 = "O contratante não pode ser o mesmo cliente do estudo.";

        public static String msg40 = "Clique no botão para selecionar a atividade da função.";

        public static String msg41 = "Precisa gravar primeiro o estudo.";
        public static String msg42 = "Número maior do que o permitido para o campo. ";

        public static String msg43 = "Caso a função participe de alguma atividade em espaço confinado, marque essa caixa ";
        public static String msg44 = "Caso a função participe de alguma atividade em altura, marque essa caixa ";

        #endregion

        frm_PcmsoAlterar formPcmsoAlterar;

        public frm_PcmsoSemPpraIncluir(frm_pcmso formPcmso)
        {
            InitializeComponent();
            this.formPcmso = formPcmso;
        }

        public frm_PcmsoSemPpraIncluir(frm_PcmsoAlterar formPcmsoAlterar, Estudo pcmso)
        {
            InitializeComponent();
            this.formPcmsoAlterar = formPcmsoAlterar;
            this.pcmso = pcmso;
            carregaDadosPrincipaisPcmso();
            montaGridEstimativa();
        }

        public frm_PcmsoSemPpraIncluir()
        {
            InitializeComponent();
        }

        public Estudo getEstudo()
        {
            return this.pcmso;
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_cliente_Click(object sender, EventArgs e)
        {
            frm_clienteVendedorIncluir formClienteVendedorIncluir = new frm_clienteVendedorIncluir();
            formClienteVendedorIncluir.ShowDialog();

            if (formClienteVendedorIncluir.getVendedorCliente() != null)
            {
                pcmso.VendedorCliente = formClienteVendedorIncluir.getVendedorCliente();
                text_cliente.Text = formClienteVendedorIncluir.getVendedorCliente().getCliente().RazaoSocial.ToUpper();
                grb_contratante_contrato.Enabled = true;
                btn_contratante.Enabled = true;
                btn_cliente.Enabled = true;
            }
        }

        private void btn_tecnico_Click(object sender, EventArgs e)
        {
            frm_ppraUsuarioTecnicoIncluir formUsuarioTecnicoIncluirPpra = new frm_ppraUsuarioTecnicoIncluir();
            formUsuarioTecnicoIncluirPpra.ShowDialog();

            if (formUsuarioTecnicoIncluirPpra.getUsuario() != null)
            {
                pcmso.Tecno = formUsuarioTecnicoIncluirPpra.getUsuario();
                text_tecnico.Text = formUsuarioTecnicoIncluirPpra.getUsuario().Nome.ToUpper();
            }
        }

        private void btn_contratante_Click(object sender, EventArgs e)
        {
            try
            {
                //zerando informações do contratante
                text_numContrato.Text = String.Empty;
                text_inicioContr.Text = String.Empty;
                text_fimContr.Text = String.Empty;

                frmClienteSelecionar formCliente = new frmClienteSelecionar(null, null, null, null, null, null, null, false, true);
                formCliente.ShowDialog();

                if (formCliente.Cliente != null)
                {
                    if (formCliente.Cliente.Id == pcmso.VendedorCliente.getCliente().Id)
                    {
                        throw new Exception(msg39);
                    }

                    pcmso.ClienteContratante = formCliente.Cliente;
                    text_clienteContratante.Text = formCliente.Cliente.RazaoSocial;
                    text_clienteContratante.Enabled = true;
                    text_inicioContr.Enabled = true;
                    text_fimContr.Enabled = true;
                    text_numContrato.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void PcmsoSemPpraIncluir_Load(object sender, EventArgs e)
        {
            ComboHelper.startComboUfSave(cb_uf);
            ComboHelper.grauRisco(cb_grauRisco);
            dt_criacao.Checked = true;
            dt_vencimento.Checked = true;
            if (pcmso != null && pcmso.Id != null)
            {
                enableCommand(Convert.ToBoolean(true));
                btn_salvar.Text = "&Gravar";
            }
            else
            {
                enableCommand(Convert.ToBoolean(false));
            }

            if (pcmso.Cronograma != null && pcmso.Cronograma.Id != null)
            {
                btn_incluirAtividade.Enabled = true;
                btn_excluirAtividade.Enabled = true;
                btn_alterarAtividade.Enabled = true;
            }

            if (pcmso != null)
            {
                if (pcmso.Uf != null)
                {
                    cb_uf.SelectedValue = pcmso.Uf;
                }

                if (pcmso.GrauRisco != null)
                {
                    cb_grauRisco.SelectedValue = pcmso.GrauRisco;
                }
            }

            this.btn_exame_alterar.Enabled = false;
        }

        private void btn_salvar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (pcmso.Id == null)
                {

                    if (String.IsNullOrEmpty(text_cliente.Text))
                    {
                        throw new PcmsoException(PcmsoException.msg11);
                    }

                    PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();
                    PpraFacade ppraFacade = PpraFacade.getInstance();
                    RelatorioFacade relatorioFacade = RelatorioFacade.getInstance();

                    //setando valores para o estudo.

                    // parametros do contratante.
                    pcmso.DataInicioContrato = Convert.ToString(text_inicioContr.Text);
                    pcmso.DataFimContrato = Convert.ToString(text_fimContr.Text);
                    pcmso.NumContrato = text_numContrato.Text;

                    // parametros da criacao
                    pcmso.DataCriacao = Convert.ToDateTime(dt_criacao.Text);
                    pcmso.DataVencimento = Convert.ToDateTime(dt_vencimento.Text);

                    // parametros do risco
                    pcmso.GrauRisco = Convert.ToString(cb_grauRisco.SelectedValue);

                    // parametros do endereco
                    pcmso.Endereco = text_endereco.Text;
                    pcmso.Numero = text_numero.Text;
                    pcmso.Complemento = text_complemento.Text;
                    pcmso.Bairro = text_bairro.Text;
                    pcmso.Cidade = text_cidade.Text;
                    pcmso.Uf = Convert.ToString(cb_uf.SelectedValue);
                    pcmso.Obra = text_obra.Text;
                    pcmso.LocalObra = text_localObra.Text;
                    pcmso.Situacao = ApplicationConstants.CONSTRUCAO;

                    //validando o cep digitado.

                    if (!ValidaCampoHelper.ValidaCepDigitado(text_cep.Text))
                        throw new Exception("CEP Inválido.");

                    pcmso.Cep = text_cep.Text;
                    pcmso.Comentario = text_comentario.Text;

                    pcmso.IdRelatorio = relatorioFacade.findUltimaVersaoByTipo(ApplicationConstants.RELATORIO_PCMSO);

                    // chamada do metodo para incluir o estudo
                    pcmso = pcmsoFacade.incluirPcmso(pcmso);

                    //Criando Cronograma
                    // montando entidade cronograma do relacionamento.
                    cronograma.Descricao = "CRONOGRAMA DE ATIVIDADES";
                    cronograma.CronogramaAtividade = listCronogramaAtividade;
                    pcmso.Cronograma = cronograma;
                    pcmso = ppraFacade.incluiCronograma(pcmso);
                    this.cronograma = pcmso.Cronograma;

                    MessageBox.Show(Msg02 + pcmso.CodigoEstudo + Msg03, Msg01);
                    text_codPpra.Text = Convert.ToString(pcmso.CodigoEstudo);

                    // desabilitando botão impedindo a gravação

                    enableCommand(true);

                    // Mundando nome do botão novo

                    btn_salvar.Text = "&Gravar";

                }
                else
                {
                    PpraFacade ppraFacade = PpraFacade.getInstance();

                    //setando valores para o estudo.

                    // parametros do contratante.
                    pcmso.DataInicioContrato = Convert.ToString(text_inicioContr.Text);
                    pcmso.DataFimContrato = Convert.ToString(text_fimContr.Text);
                    pcmso.NumContrato = text_numContrato.Text;

                    // parametros da criacao
                    pcmso.DataCriacao = Convert.ToDateTime(dt_criacao.Text);
                    pcmso.DataVencimento = Convert.ToDateTime(dt_vencimento.Text);

                    // parametros do risco
                    pcmso.GrauRisco = Convert.ToString(cb_grauRisco.SelectedValue);

                    // parametros do endereco
                    pcmso.Endereco = text_endereco.Text;
                    pcmso.Numero = text_numero.Text;
                    pcmso.Complemento = text_complemento.Text;
                    pcmso.Bairro = text_bairro.Text;
                    pcmso.Cidade = text_cidade.Text;
                    pcmso.Uf = Convert.ToString(cb_uf.SelectedValue);
                    pcmso.Obra = text_obra.Text;
                    pcmso.LocalObra = text_localObra.Text;
                    pcmso.Situacao = ApplicationConstants.CONSTRUCAO;

                    //validando o cep digitado.

                    if (!ValidaCampoHelper.ValidaCepDigitado(text_cep.Text))
                        throw new Exception("CEP Inválido.");
                    
                    pcmso.Cep = text_cep.Text;
                    pcmso.Comentario = text_comentario.Text;

                    // chamada no metodo para alterar o estudo em tempo e execucao da inclusão do estudo.
                    pcmso = ppraFacade.alteraPpra(pcmso);

                    MessageBox.Show(Msg04, Msg01);
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        protected void enableCommand(Boolean abilitaComando)
        {
            grb_contratante_contrato.Enabled = abilitaComando;


            // cronograma
            grb_atividade.Enabled = abilitaComando;
            btn_incluirAtividade.Enabled = abilitaComando;
            btn_alterarAtividade.Enabled = abilitaComando;
            btn_excluirAtividade.Enabled = abilitaComando;


            // GHE
            grb_ghePPRA.Enabled = abilitaComando;
            btn_incluirGHE.Enabled = abilitaComando;
            btn_alterarGhe.Enabled = abilitaComando;
            btn_excluirGHE.Enabled = abilitaComando;

            //fontes
            grb_gheFonte.Enabled = abilitaComando;
            grb_fonte.Enabled = abilitaComando;
            btn_incluirFonte.Enabled = abilitaComando;
            btn_excluirFonte.Enabled = abilitaComando;

            //agentes
            grb_gheAgente.Enabled = abilitaComando;
            grb_fonteAgente.Enabled = abilitaComando;
            grb_agente.Enabled = abilitaComando;
            btn_incluirAgente.Enabled = abilitaComando;
            btn_excluirAgente.Enabled = abilitaComando;

            //Setores
            grb_gheSetor.Enabled = abilitaComando;
            grb_setor.Enabled = abilitaComando;
            btn_incluirSetor.Enabled = abilitaComando;
            btn_excluirSetor.Enabled = abilitaComando;

            //funcaoCliente
            grb_funcao.Enabled = abilitaComando;
            btn_incluirFuncaoClientePPRA.Enabled = abilitaComando;
            btn_excluirFuncaoClientePPRA.Enabled = abilitaComando;

            //funcao
            grb_gheFuncao.Enabled = abilitaComando;
            grb_setorFuncao.Enabled = abilitaComando;
            grb_funcaoCliente.Enabled = abilitaComando;
            btn_incluirFuncaoCliente.Enabled = abilitaComando;
            btn_excluirFuncaoCliente.Enabled = abilitaComando;

            //Anexo
            grb_anexo.Enabled = abilitaComando;
            btn_incluirAnexo.Enabled = abilitaComando;
            btn_excluirAnexo.Enabled = abilitaComando;

            // comentário
            grb_comentario.Enabled = abilitaComando;

            //exames
            grb_gheExame.Enabled = abilitaComando;
            grb_funcoesExame.Enabled = abilitaComando;
            grb_risco.Enabled = abilitaComando;
            grb_exames.Enabled = abilitaComando;
            grb_periodicidade.Enabled = abilitaComando;
            btn_incluir.Enabled = abilitaComando;
            btn_exame_alterar.Enabled = abilitaComando;
            btn_excluir.Enabled = abilitaComando;


            //hospitais e médicos
            grb_hospital.Enabled = abilitaComando;
            btn_incluirHospital.Enabled = abilitaComando;
            btn_excluirHospital.Enabled = abilitaComando;
            grb_medico.Enabled = abilitaComando;
            btn_incluirMedico.Enabled = abilitaComando;
            btn_excluirMedico.Enabled = abilitaComando;

            //material hospitalar
            grb_material.Enabled = abilitaComando;
            grb_material_salvar.Enabled = abilitaComando;
            btn_incluirMaterial.Enabled = abilitaComando;
            btn_excluirMaterial.Enabled = abilitaComando;


        }

        protected void enableNewPPRa(Boolean retorno)
        {
            grb_dados.Enabled = retorno;
            grb_grauRisco.Enabled = retorno;
            text_endereco.Enabled = retorno;
            text_numero.Enabled = retorno;
            text_complemento.Enabled = retorno;
            text_bairro.Enabled = retorno;
            text_cidade.Enabled = retorno;
            cb_uf.Enabled = retorno;
            text_obra.Enabled = retorno;
            text_localObra.Enabled = retorno;
            btn_salvar.Enabled = retorno;
            btn_limpar.Enabled = retorno;
        }

        private void btn_limpar_Click(object sender, EventArgs e)
        {
            text_endereco.Text = String.Empty;
            text_numero.Text = String.Empty;
            text_complemento.Text = String.Empty;
            text_bairro.Text = String.Empty;
            text_cidade.Text = String.Empty;
            ComboHelper.startComboUfSave(cb_uf);
            text_obra.Text = String.Empty;
            text_localObra.Text = String.Empty;

            pcmso.Tecno = null;
            text_tecnico.Text = String.Empty;
            pcmso.ClienteContratante = null;
            text_clienteContratante.Text = String.Empty;
            text_numContrato.Text = String.Empty;
        }

        private void btn_incluirAtividade_Click(object sender, EventArgs e)
        {
            frmCronogramaAtividadeIncluir formPpraAtividadeCronograma = new frmCronogramaAtividadeIncluir(cronograma);
            formPpraAtividadeCronograma.ShowDialog();

            if (formPpraAtividadeCronograma.CronogramaAtividade != null)
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();
                listCronogramaAtividade.Add(pcmsoFacade.incluirCronogramaAtividade(formPpraAtividadeCronograma.CronogramaAtividade));
                MontarGrid();
            }

        }
        public void MontarGrid()
        {
            grd_atividadeCronograma.Columns.Clear();

            
            grd_atividadeCronograma.ColumnCount = 7;

            grd_atividadeCronograma.Columns[0].Name = "IdAtividade";
            grd_atividadeCronograma.Columns[0].Visible = false;
                
            grd_atividadeCronograma.Columns[1].Name = "Atividade";
            grd_atividadeCronograma.Columns[2].Name = "Mês Realizado";
            grd_atividadeCronograma.Columns[3].Name = "Ano Realizado";
            grd_atividadeCronograma.Columns[4].Name = "Público Alvo";
            grd_atividadeCronograma.Columns[5].Name = "Evidência / Registro";
                
            grd_atividadeCronograma.Columns[6].Name = "IdCronograma";
            grd_atividadeCronograma.Columns[6].Visible = false;
                
            foreach (CronogramaAtividade cronogramaAtividade in listCronogramaAtividade)
                grd_atividadeCronograma.Rows.Add(cronogramaAtividade.Atividade.Id, cronogramaAtividade.Atividade.Descricao, cronogramaAtividade.MesRealizado, cronogramaAtividade.AnoRealizado, cronogramaAtividade.PublicoAlvo, cronogramaAtividade.Evidencia, cronogramaAtividade.Cronograma.Id);
        }

        private void btn_alterarAtividade_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_atividadeCronograma.CurrentRow == null)
                    throw new Exception("Selecione uma linha");

                int index = 0;

                foreach (CronogramaAtividade cronogramaAtividade in listCronogramaAtividade)
                {
                    if (cronogramaAtividade.Cronograma.Id == (long)grd_atividadeCronograma.CurrentRow.Cells[6].Value && cronogramaAtividade.Atividade.Id == (long)grd_atividadeCronograma.CurrentRow.Cells[0].Value)
                        break;
                    else
                        index++;
                }

                frmCronogramaAtividadeAlterar formPppraAtividadeCronogramaAlterar = new frmCronogramaAtividadeAlterar(listCronogramaAtividade.ElementAt(index));
                formPppraAtividadeCronogramaAlterar.ShowDialog();
                MontarGrid();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_excluirAtividade_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_atividadeCronograma.CurrentRow == null)
                    throw new Exception ("Selecione uma linha");

                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();
                CronogramaAtividade cronogramaAtividadeExcluir;
                int index = 0;

                if (MessageBox.Show(Msg10, Msg01, MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    foreach (CronogramaAtividade cronogramaAtividade in listCronogramaAtividade)
                    {
                        if (cronogramaAtividade.Atividade.Id == (long)grd_atividadeCronograma.CurrentRow.Cells[0].Value && cronogramaAtividade.Cronograma.Id == (long)grd_atividadeCronograma.CurrentRow.Cells[6].Value)
                            break;
                        else
                            index++;
                    }
                    cronogramaAtividadeExcluir = listCronogramaAtividade.ElementAt(index);
                    pcmsoFacade.excluiCronogramaAtividade(listCronogramaAtividade.ElementAt(index));
                    listCronogramaAtividade.Remove(cronogramaAtividadeExcluir);
                    MontarGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_incluirGHE_Click(object sender, EventArgs e)
        {
            try
            {

                frm_GheIncluir formGheIncluir = new frm_GheIncluir(pcmso);
                formGheIncluir.ShowDialog();

                if (formGheIncluir.Ghe != null)
                {
                    PpraFacade ppraFacade = PpraFacade.getInstance();

                    ghe = formGheIncluir.Ghe;
                    montaGridGhe();

                    iniciaComboGhe(cb_ghe);
                    iniciaComboGhe(cb_gheSetor);
                    iniciaComboGhe(cb_gheFuncao);
                    iniciaComboGhe(cb_gheAgente);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        public void montaGridGhe()
        {
            try
            {
                grd_ghe.Columns.Clear();

                PpraFacade ppraFacade = PpraFacade.getInstance();

                DataSet ds = ppraFacade.findGhePpra(pcmso);

                grd_ghe.DataSource = ds.Tables["Ghes"].DefaultView;
                grd_ghe.RowsDefaultCellStyle.BackColor = Color.White;
                grd_ghe.AutoResizeColumn(2);

                grd_ghe.Columns[0].HeaderText = "IdGhe";
                grd_ghe.Columns[1].HeaderText = "IdPpra";
                grd_ghe.Columns[2].HeaderText = "Descrição";

                grd_ghe.Columns[0].Visible = false;
                grd_ghe.Columns[1].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void montaGridGheExame()
        {
            try
            {
                grd_ghe.Columns.Clear();

                PpraFacade ppraFacade = PpraFacade.getInstance();

                DataSet ds = ppraFacade.findGhePpra(pcmso);

                grd_gheExame.DataSource = ds.Tables["Ghes"].DefaultView;
                grd_gheExame.RowsDefaultCellStyle.BackColor = Color.White;
                grd_gheExame.AutoResizeColumn(2);

                grd_gheExame.Columns[0].HeaderText = "IdGhe";
                grd_gheExame.Columns[1].HeaderText = "IdPpra";
                grd_gheExame.Columns[2].HeaderText = "Descrição";
                grd_gheExame.Columns[3].HeaderText = "nexp";
                grd_gheExame.Columns[4].HeaderText = "Número PO";

                grd_gheExame.Columns[0].Visible = false;
                grd_gheExame.Columns[1].Visible = false;
                grd_gheExame.Columns[3].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_alterarGhe_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_ghe.CurrentRow != null)
                {
                    PpraFacade ppraFacade = PpraFacade.getInstance();

                    Int64 id = (Int64)grd_ghe.CurrentRow.Cells[0].Value;

                    ghe = ppraFacade.findGheById(id);
                    ghe.Estudo = pcmso;

                    frm_GheAlterar formGheAlterar = new frm_GheAlterar(ghe);
                    formGheAlterar.ShowDialog();

                    ghe = formGheAlterar.Ghe;
                    montaGridGhe();
                    
                    iniciaComboGhe(cb_ghe);
                    iniciaComboGhe(cb_gheSetor);
                    iniciaComboGhe(cb_gheFuncao);
                }
                else
                {
                    MessageBox.Show(Msg07, Msg08);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_excluirGHE_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_ghe.CurrentRow != null)
                {

                    if (MessageBox.Show(Msg15 + Msg16, Msg01, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        PpraFacade ppraFacade = PpraFacade.getInstance();

                        ppraFacade.deleteGheById((Int64)grd_ghe.CurrentRow.Cells[0].Value, pcmso);

                        montaGridGhe();
                        iniciaComboGhe(cb_ghe);
                        iniciaComboGhe(cb_gheSetor);
                        iniciaComboGhe(cb_gheFuncao);
                        iniciaComboGhe(cb_gheAgente);
                        grd_fonte.Columns.Clear();
                        grd_setor.Columns.Clear();

                        MessageBox.Show(Msg17, Msg01);
                    }
                }
                else
                {
                    MessageBox.Show(Msg07, Msg08);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void iniciaComboGhe(ComboBox combo)
        {
            PpraFacade ppraFacade = PpraFacade.getInstance();

            SelectItem item = null;
            String valor;
            String descricao;

            combo.Items.Clear();

            foreach (DataRow rows in ppraFacade.findGhePpra(pcmso).Tables[0].Rows)
            {
                descricao = Convert.ToString(rows[Convert.ToString("descricao")]);
                valor = Convert.ToString(rows[Convert.ToString("id_ghe")]);

                item = new SelectItem(valor, descricao);

                combo.Items.Add(item);
            }

            combo.ValueMember = "nome";
        }

        private void btn_incluirFonte_Click(object sender, EventArgs e)
        {
            try
            {
                if (cb_ghe.SelectedIndex == -1)
                {
                    throw new Exception(Msg21);
                }

                frm_ppraGheFonteIncluir formPpraGheFonteIncluir = new frm_ppraGheFonteIncluir(ghe);
                formPpraGheFonteIncluir.ShowDialog();

                montaGridGheFonte();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected void cb_ghe_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectItem itemSelecionado = (SelectItem)cb_ghe.SelectedItem;
                PpraFacade ppraFacade = PpraFacade.getInstance();

                // criando o objeto GHE
                ghe = ppraFacade.findGheById(Convert.ToInt64(itemSelecionado.Valor));
                montaGridGheFonte();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void montaGridGheFonte()
        {
            try
            {
                grd_fonte.Columns.Clear();

                PpraFacade ppraFacade = PpraFacade.getInstance();

                DataSet ds = ppraFacade.findAllFonteByGheDataSet(ghe);

                grd_fonte.DataSource = ds.Tables[0].DefaultView;

                grd_fonte.Columns[0].HeaderText = "id_ghe_fonte";
                grd_fonte.Columns[0].Visible = false;

                grd_fonte.Columns[1].HeaderText = "id_fonte";
                grd_fonte.Columns[1].Visible = false;

                grd_fonte.Columns[2].HeaderText = "Fonte";
                grd_fonte.Columns[2].DefaultCellStyle.ForeColor = Color.Red;

                grd_fonte.Columns[3].HeaderText = "Id_ghe";
                grd_fonte.Columns[3].Visible = false;

                grd_fonte.Columns[4].HeaderText = "Ghe";
                grd_fonte.Columns[4].Visible = false;

                grd_fonte.Columns[5].HeaderText = "Principal?";
            
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btn_excluirFonte_Click(object sender, EventArgs e)
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();
                GheFonte ghefonteSelecionado = null;

                if (grd_fonte.CurrentRow != null)
                {

                    if (MessageBox.Show(Msg18 + Msg19, Msg01, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        foreach (DataGridViewRow dvRow in grd_fonte.SelectedRows)
                        {
                            ghefonteSelecionado = new GheFonte((Int64)grd_fonte.CurrentRow.Cells[0].Value, null, null);

                            ppraFacade.deleteGheFonteById(ghefonteSelecionado);
                        }

                        MessageBox.Show(Msg20, Msg01);

                        montaGridGheFonte();
                    }
                }
                else
                {
                    MessageBox.Show(Msg07, Msg08);
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        private void cb_gheAgente_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectItem itemSelecionado = (SelectItem)cb_gheAgente.SelectedItem;
                grd_agente.Columns.Clear();
                PpraFacade ppraFacade = PpraFacade.getInstance();
                // criando o objeto GHE
                ghe = ppraFacade.findGheById(Convert.ToInt64(itemSelecionado.Valor));
                iniciaComboGheFonte(cb_fonteAgente, false);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected void iniciaComboGheFonte(ComboBox combo, Boolean selecao)
        {
            PpraFacade ppraFacade = PpraFacade.getInstance();

            SelectItem item = null;
            String valor;
            String descricao;

            combo.Items.Clear();

            if (selecao == true)
            {
                foreach (DataRow rows in ppraFacade.findAllFonteByGheInEpi(ghe).Tables[0].Rows)
                {
                    descricao = Convert.ToString(rows[Convert.ToString("descricao")]);
                    valor = Convert.ToString(rows[Convert.ToString("id_fonte")]);

                    item = new SelectItem(valor, descricao);

                    combo.Items.Add(item);
                }
            }
            else
            {
                foreach (DataRow rows in ppraFacade.findAllFonteByGheDataSet(ghe).Tables[0].Rows)
                {

                    String principal = (Boolean)rows["Principal"] ? " - (PRINCIPAL)" : String.Empty;

                    descricao = Convert.ToString(rows[Convert.ToString("descricao")])
                        + principal;

                    valor = Convert.ToString(rows[Convert.ToString("id_fonte")]);
                    item = new SelectItem(valor, descricao);

                    combo.Items.Add(item);
                }
            }

            combo.ValueMember = "nome";
        }

        private void btn_incluirAgente_Click(object sender, EventArgs e)
        {
            try
            {
                if (cb_fonteAgente.SelectedIndex == -1)
                {
                    throw new Exception (Msg22);
                }
            
                frm_ppraAgenteIncluir formPpraAgenteIncluir = new frm_ppraAgenteIncluir(gheFonte);
                formPpraAgenteIncluir.ShowDialog();

                montaGridAgente();
                
            
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        private void btn_excluirAgente_Click(object sender, EventArgs e)
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                if (grd_agente.CurrentRow != null)
                {

                    if (MessageBox.Show(Msg23 + Msg16, Msg01, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        foreach (DataGridViewRow dvRow in grd_agente.SelectedRows)
                        {
                            ppraFacade.deleteGheFonteAgenteById((Int64)dvRow.Cells[0].Value);
                        }

                        MessageBox.Show(Msg24, Msg01);
                        montaGridAgente();
                    }
                }
                else
                {
                    MessageBox.Show(Msg07, Msg08);
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        public void montaGridAgente()
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();
                this.Cursor = Cursors.WaitCursor;

                grd_agente.Columns.Clear();

                DataSet ds = ppraFacade.findAllAgenteByGheFonte(gheFonte);

                grd_agente.DataSource = ds.Tables[0].DefaultView;

                grd_agente.Columns[0].HeaderText = "Id_ghe_fonte_agente";
                grd_agente.Columns[0].Visible = false;

                grd_agente.Columns[1].HeaderText = "Id_agente";
                grd_agente.Columns[1].Visible = false;

                grd_agente.Columns[2].HeaderText = "Agente";
                grd_agente.Columns[2].DefaultCellStyle.ForeColor = Color.Red;

                grd_agente.Columns[3].HeaderText = "Id GheFonte";
                grd_agente.Columns[3].Visible = false;

                grd_agente.Columns[4].HeaderText = "Risco";
                grd_agente.Columns[4].DefaultCellStyle.ForeColor = Color.Blue;

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        protected void btn_incluirSetor_Click(object sender, EventArgs e)
        {
            try
            {
                if (cb_gheSetor.SelectedIndex == -1)
                {
                    throw new Exception(Msg21);
                }

                frm_PpraSetor formPpraSetor = new frm_PpraSetor(ghe);
                formPpraSetor.ShowDialog();

                montaGridGheSetor();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btn_excluirSetor_Click(object sender, EventArgs e)
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                GheSetor gheSetorExcluir = new GheSetor();

                if (grd_setor.CurrentRow != null)
                {

                    if (MessageBox.Show(Msg27 + Msg28, Msg01, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        foreach (DataGridViewRow dvRow in grd_setor.SelectedRows)
                        {
                            gheSetorExcluir.Id = (long)dvRow.Cells[0].Value;

                            ppraFacade.deleteGheSetor(gheSetorExcluir);
                        }

                        MessageBox.Show(Msg29, Msg01);
                        montaGridGheSetor();
                    }
                }
                else
                {
                    MessageBox.Show(Msg07, Msg08);
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        public void montaGridGheSetor()
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                grd_setor.Columns.Clear();

                DataSet ds = ppraFacade.findAllSetorByGhe(ghe);

                grd_setor.DataSource = ds.Tables[0].DefaultView;

                grd_setor.Columns[0].HeaderText = "id_ghe_Setor";
                grd_setor.Columns[0].Visible = false;

                grd_setor.Columns[1].HeaderText = "id_setor";
                grd_setor.Columns[1].Visible = false;

                grd_setor.Columns[2].HeaderText = "Setor";

                grd_setor.Columns[3].HeaderText = "Id_ghe";
                grd_setor.Columns[3].Visible = false;

                grd_setor.Columns[4].HeaderText = "GHE";
                grd_setor.Columns[4].Visible = false;

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void cb_fonteAgente_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                Fonte fonte = new Fonte();

                SelectItem gheSelecionado = (SelectItem)cb_gheAgente.SelectedItem;

                ghe = ppraFacade.findGheById(Convert.ToInt64(gheSelecionado.Valor));

                SelectItem fonteSelecioando = (SelectItem)cb_fonteAgente.SelectedItem;
                
                fonte = ppraFacade.findFonteById(Convert.ToInt64(fonteSelecioando.Valor));

                gheFonte = ppraFacade.findGheFonteByGheFonte(new GheFonte(null, fonte, ghe));

                gheFonteAgente = new GheFonteAgente(null, null, null, null, gheFonte, null, string.Empty, ApplicationConstants.ATIVO);

                montaGridAgente();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_incluirFuncaoClientePPRA_Click(object sender, EventArgs e)
        {
            try
            {

                if (String.IsNullOrEmpty(text_cliente.Text))
                {
                    throw new ClienteException(ClienteException.msg1);
                }

                frm_ClienteFuncaoIncluirFuncao formPpraClienteFuncao = new frm_ClienteFuncaoIncluirFuncao(this);
                formPpraClienteFuncao.ShowDialog();
                

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btn_excluirFuncaoClientePPRA_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(text_cliente.Text))
                {
                    PpraFacade ppraFacade = PpraFacade.getInstance();

                    if (grd_funcaoCliente.CurrentRow != null)
                    {

                        if (MessageBox.Show(Msg30 + Msg31, Msg01, MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            foreach (DataGridViewRow dvRow in grd_funcaoCliente.SelectedRows)
                            {
                                ClienteFuncao clienteFuncaoExcluir = ppraFacade.findClienteFuncaoById((Int64)grd_funcaoCliente.CurrentRow.Cells[0].Value);
                                clienteFuncaoExcluir.DataExclusao = DateTime.Now;
                                clienteFuncaoExcluir.Situacao = ApplicationConstants.DESATIVADO;
                                ppraFacade.updateClienteFuncao(clienteFuncaoExcluir);
                            }

                            MessageBox.Show(Msg32, Msg01);
                            montaGridFuncao();
                            grd_setorFuncao.Columns.Clear();


                        }
                    }
                    else
                    {
                        MessageBox.Show(Msg07, Msg08);
                    }

                }
                else
                {
                    throw new ClienteException(ClienteException.msg1);

                }

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void montaGridFuncao()
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                DataSet ds = ppraFacade.findAllClienteFuncao(pcmso.VendedorCliente.getCliente());

                // montando a gride.

                grd_funcaoCliente.DataSource = ds.Tables["Funcoes"].DefaultView;

                grd_funcaoCliente.Columns[0].HeaderText = "id_seg_cliente_funcao";
                grd_funcaoCliente.Columns[1].HeaderText = "id_cliente";
                grd_funcaoCliente.Columns[2].HeaderText = "id_funcao";
                grd_funcaoCliente.Columns[3].HeaderText = "Descrição";
                grd_funcaoCliente.Columns[4].HeaderText = "CBO";

                grd_funcaoCliente.Columns[0].Visible = false;
                grd_funcaoCliente.Columns[1].Visible = false;
                grd_funcaoCliente.Columns[2].Visible = false;
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        private void tb_fonteGHE_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tb_fonteGHE.SelectedIndex == 0)
            {
                iniciaComboGhe(cb_ghe);
                grd_fonte.Columns.Clear();
            }

            if (tb_fonteGHE.SelectedIndex == 0)
            {
                iniciaComboGhe(cb_gheAgente);
                cb_fonteAgente.Items.Clear();
                grd_agente.Columns.Clear();
            }
        }

        private void cb_gheSetor_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectItem itemSelecionado = (SelectItem)cb_gheSetor.SelectedItem;

                // criando o objeto GHE
                PpraFacade ppraFacade = PpraFacade.getInstance();
                ghe = ppraFacade.findGheById(Convert.ToInt64(itemSelecionado.Valor));
                montaGridGheSetor();

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cb_setorFuncao_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                Setor setor = new Setor();

                SelectItem gheSelecionado = (SelectItem)cb_gheFuncao.SelectedItem;
                ghe = ppraFacade.findGheById(Convert.ToInt64(gheSelecionado.Valor));

                SelectItem setorSelecioando = (SelectItem)cb_setorFuncao.SelectedItem;
                setor = new Setor(Convert.ToInt64(setorSelecioando.Valor), Convert.ToString(setorSelecioando.Nome), ApplicationConstants.ATIVO);

                gheSetor = new GheSetor(null, setor, ghe, ApplicationConstants.ATIVO);
                gheSetor = ppraFacade.findAllGheSetorByGheAndSetor(ghe, setor).First(x => x.Ghe.Id == ghe.Id && x.Setor.Id == setor.Id);

                gheSetorClienteFuncao = new GheSetorClienteFuncao(null, null, gheSetor, false, false, string.Empty, null, false);

                montaGridClienteFuncao();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void montaGridClienteFuncao()
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                grd_setorFuncao.Columns.Clear();

                DataSet ds = ppraFacade.findAllGheSetorClienteFuncaoByGheSetor(gheSetor);

                grd_setorFuncao.DataSource = ds.Tables[0].DefaultView;

                grd_setorFuncao.Columns[0].HeaderText = "id_ghe_setor_cliente_funcao";
                grd_setorFuncao.Columns[0].Visible = false;

                grd_setorFuncao.Columns[1].HeaderText = "id_ghe_setor";
                grd_setorFuncao.Columns[1].Visible = false;

                grd_setorFuncao.Columns[2].HeaderText = "id_funcao";
                grd_setorFuncao.Columns[2].Visible = false;

                grd_setorFuncao.Columns[3].HeaderText = "Função";
                grd_setorFuncao.Columns[3].DisplayIndex = 1;
                grd_setorFuncao.Columns[3].ReadOnly = true;

                grd_setorFuncao.Columns[4].HeaderText = "Descrição da Atividade";
                grd_setorFuncao.Columns[4].DisplayIndex = 5;
                grd_setorFuncao.Columns[4].ReadOnly = true;

                grd_setorFuncao.Columns[5].HeaderText = "Confinado";
                grd_setorFuncao.Columns[5].DisplayIndex = 2;

                grd_setorFuncao.Columns[6].HeaderText = "Altura";
                grd_setorFuncao.Columns[6].DisplayIndex = 3;

                grd_setorFuncao.Columns[7].HeaderText = "id_seg_cliente_funcao";
                grd_setorFuncao.Columns[7].Visible = false;

                grd_setorFuncao.Columns[8].HeaderText = "id_comentario_funcao";
                grd_setorFuncao.Columns[8].Visible = false;

                grd_setorFuncao.Columns[9].HeaderText = "eletricidade";
                grd_setorFuncao.Columns[9].DisplayIndex = 4;

                /* incluindo botão na grid para selecionar a atividade da função */
                DataGridViewButtonColumn btn = new DataGridViewButtonColumn();
                grd_setorFuncao.Columns.Add(btn);

                btn.HeaderText = "Atividade";
                btn.Text = "...";
                btn.Name = "btn_atividade";
                btn.UseColumnTextForButtonValue = true;
                grd_setorFuncao.Columns[10].DisplayIndex = 5;
                grd_setorFuncao.Columns[10].ReadOnly = true;


            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
        }

        private void btn_incluirFuncaoCliente_Click(object sender, EventArgs e)
        {

            try
            {
                if (cb_setorFuncao.SelectedIndex == -1)
                {
                    throw new Exception(Msg33);
                }

                frm_ppraGheSetorClienteFuncao formGheSetorClienteFuncao = new frm_ppraGheSetorClienteFuncao(gheSetor, pcmso.VendedorCliente.getCliente());
                formGheSetorClienteFuncao.ShowDialog();

                montaGridClienteFuncao();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
        }

        private void btn_excluirFuncaoCliente_Click(object sender, EventArgs e)
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                GheSetorClienteFuncao gheSetorClienteFuncaoExcluir = new GheSetorClienteFuncao(null, null, null, false, false, String.Empty, null, false);

                if (grd_setorFuncao.CurrentRow != null)
                {

                    if (MessageBox.Show(Msg34, Msg08, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        foreach (DataGridViewRow dvRow in grd_setorFuncao.SelectedRows)
                        {
                            ppraFacade.excluirGheSetorClienteFuncao(new GheSetorClienteFuncao((Int64)dvRow.Cells[0].Value, null, null, false, false, String.Empty, null, false));
                        }

                        MessageBox.Show(Msg35, Msg01);
                        montaGridClienteFuncao();
                    }
                }
                else
                {
                    MessageBox.Show(Msg07, Msg08);
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        private void cb_gheFuncao_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectItem itemSelecionado = (SelectItem)cb_gheFuncao.SelectedItem;
                grd_setorFuncao.Columns.Clear();
                PpraFacade ppraFacade = PpraFacade.getInstance();

                ghe = ppraFacade.findGheById(Convert.ToInt64(itemSelecionado.Valor));
                iniciaComboSetorFuncao(cb_setorFuncao);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected void iniciaComboSetorFuncao(ComboBox combo)
        {
            PpraFacade ppraFacade = PpraFacade.getInstance();

            SelectItem item = null;
            String valor;
            String descricao;

            combo.Items.Clear();

            foreach (DataRow rows in ppraFacade.findAllSetorByGhe(ghe).Tables[0].Rows)
            {
                descricao = Convert.ToString(rows[Convert.ToString("setor")]);
                valor = Convert.ToString(rows[Convert.ToString("id_setor")]);

                item = new SelectItem(valor, descricao);

                combo.Items.Add(item);
            }

            combo.ValueMember = "nome";
        }

        private void carregaDadosPrincipaisPcmso()
        {
            if (pcmso.Cronograma != null)
            {
                this.cronograma = pcmso.Cronograma;
                this.listCronogramaAtividade = pcmso.Cronograma.CronogramaAtividade;
            }

            if (pcmso.ClienteContratante != null)
            {
                text_clienteContratante.Text = pcmso.ClienteContratante.RazaoSocial;
            }

            if (pcmso.Id != null)
            {
                text_codPpra.Text = pcmso.CodigoEstudo;
                text_cliente.Text = pcmso.VendedorCliente.getCliente().RazaoSocial;

                dt_criacao.Text = Convert.ToString(pcmso.DataCriacao);
                dt_vencimento.Text = Convert.ToString(pcmso.DataVencimento);
                text_endereco.Text = pcmso.Endereco;
                text_numero.Text = pcmso.Numero;
                text_complemento.Text = pcmso.Complemento;
                text_bairro.Text = pcmso.Bairro;
                text_cidade.Text = pcmso.Cidade;
                text_obra.Text = pcmso.Obra;
                text_localObra.Text = pcmso.LocalObra;
                text_cep.Text = pcmso.Cep;
                text_comentario.Text = pcmso.Comentario;

                text_inicioContr.Text = pcmso.DataInicioContrato;
                text_fimContr.Text = pcmso.DataFimContrato;
                text_numContrato.Text = pcmso.NumContrato;

                btn_cliente.Enabled = false;
            }

            if (pcmso.ClienteContratante != null)
            {
                this.clienteContratanteCheck = pcmso.ClienteContratante;
                text_clienteContratante.Text = pcmso.ClienteContratante.RazaoSocial;
                text_inicioContr.Text = pcmso.DataInicioContrato;
                text_fimContr.Text = pcmso.DataFimContrato;
                text_numContrato.Text = pcmso.NumContrato;

                text_inicioContr.Enabled = true;
                text_fimContr.Enabled = true;
                text_inicioContr.Enabled = true;
                text_numContrato.Enabled = true;
            }
            else
            {
                text_inicioContr.Enabled = false;
                text_fimContr.Enabled = false;
                text_inicioContr.Enabled = false;
                text_numContrato.Enabled = false;
            }

            if (pcmso.Tecno != null)
            {
                text_tecnico.Text = pcmso.Tecno.Nome;
            }
        }

        private void tb_dados_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Carrega Dados Pcmso
            if (tb_dados.SelectedIndex == 0)
            {
                carregaDadosPrincipaisPcmso();
            }

            //Carrega Ghe
            if (tb_dados.SelectedIndex == 1)
            {
                montaGridGhe();
            }

            //Carrega Ghe
            if (tb_dados.SelectedIndex == 2)
            {
                iniciaComboGhe(cb_ghe);
                iniciaComboGhe(cb_gheAgente);
            }

            //Carrega Ghe
            if (tb_dados.SelectedIndex == 3)
            {
                iniciaComboGhe(cb_gheSetor);
            }

            //Carrega Cronograma
            if (tb_dados.SelectedIndex == 4)
            {
                // montando grid de atividades do cronograma.
                MontarGrid();

            }

            //Carrega Comentário
            if (tb_dados.SelectedIndex == 5)
            {
                text_comentario.Text = pcmso.Comentario;
            }

            //Carrega Anexo
            if (tb_dados.SelectedIndex == 6)
            {
                text_conteudo.Focus();
                MontaGridAnexo();
            }

            if (tb_dados.SelectedIndex == 7)
            {
                //Carrega Ghes
                montaGridGheExame();
            }

            //Carrega Hospitais e Médicos
            if (tb_dados.SelectedIndex == 8)
            {
                montaDataGridHospital();
                montaDataGridMedico();
            }

            //Carrega Equipamentos
            if (tb_dados.SelectedIndex == 9)
            {
                MontaDataGridMaterialHospitalar();
            }
        }

        private void tb_gheSetor_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tb_gheSetor.SelectedIndex == 0)
            {
                iniciaComboGhe(cb_gheSetor);
                grd_setor.Columns.Clear();
            }

            if (tb_gheSetor.SelectedIndex == 1 && !String.IsNullOrEmpty(text_cliente.Text))
            {
                montaGridFuncao();
            }

            if (tb_gheSetor.SelectedIndex == 2)
            {
                iniciaComboGhe(cb_gheFuncao);
                cb_setorFuncao.Items.Clear();
                grd_setorFuncao.Columns.Clear();
            }
        }

        private void btn_gravarComentario_Click(object sender, EventArgs e)
        {
            SalvarPcmso();
        }

        private void SalvarPcmso()
        {
            try
            {
                if (pcmso.Id == null)
                {

                    if (String.IsNullOrEmpty(text_cliente.Text))
                    {
                        throw new PpraException(PpraException.msg);
                    }

                    PpraFacade ppraFacade = PpraFacade.getInstance();

                    //setando valores para o estudo.

                    // parametros do contratante.
                    pcmso.DataInicioContrato = Convert.ToString(text_inicioContr.Text);
                    pcmso.DataFimContrato = Convert.ToString(text_fimContr.Text);
                    pcmso.NumContrato = text_numContrato.Text;

                    // parametros da criacao
                    pcmso.DataCriacao = Convert.ToDateTime(dt_criacao.Text);
                    pcmso.DataVencimento = Convert.ToDateTime(dt_vencimento.Text);

                    // parametros do risco
                    pcmso.GrauRisco = Convert.ToString(cb_grauRisco.SelectedValue);

                    // parametros do endereco
                    pcmso.Endereco = text_endereco.Text;
                    pcmso.Numero = text_numero.Text;
                    pcmso.Complemento = text_complemento.Text;
                    pcmso.Bairro = text_bairro.Text;
                    pcmso.Cidade = text_cidade.Text;
                    pcmso.Uf = Convert.ToString(cb_uf.SelectedValue);
                    pcmso.Obra = text_obra.Text;
                    pcmso.LocalObra = text_localObra.Text;
                    pcmso.Situacao = ApplicationConstants.CONSTRUCAO;
                    pcmso.Cep = text_cep.Text;
                    pcmso.Comentario = text_comentario.Text;

                    // chamada do metodo para incluir o estudo
                    pcmso = ppraFacade.incluirPpra(pcmso);

                    MessageBox.Show(Msg02 + pcmso.CodigoEstudo + Msg03, Msg01);
                    text_codPpra.Text = Convert.ToString(pcmso.CodigoEstudo);

                    // desabilitando botão impedindo a gravação

                    enableCommand(true);

                    // Mundando nome do botão novo

                    btn_salvar.Text = "&Gravar";

                }
                else
                {
                    PpraFacade ppraFacade = PpraFacade.getInstance();

                    //setando valores para o estudo.

                    // parametros do contratante.
                    pcmso.DataInicioContrato = Convert.ToString(text_inicioContr.Text);
                    pcmso.DataFimContrato = Convert.ToString(text_fimContr.Text);
                    pcmso.NumContrato = text_numContrato.Text;

                    // parametros da criacao
                    pcmso.DataCriacao = Convert.ToDateTime(dt_criacao.Text);
                    pcmso.DataVencimento = Convert.ToDateTime(dt_vencimento.Text);

                    // parametros do risco
                    pcmso.GrauRisco = Convert.ToString(cb_grauRisco.SelectedValue);

                    // parametros do endereco
                    pcmso.Endereco = text_endereco.Text;
                    pcmso.Numero = text_numero.Text;
                    pcmso.Complemento = text_complemento.Text;
                    pcmso.Bairro = text_bairro.Text;
                    pcmso.Cidade = text_cidade.Text;
                    pcmso.Uf = Convert.ToString(cb_uf.SelectedValue);
                    pcmso.Obra = text_obra.Text;
                    pcmso.LocalObra = text_localObra.Text;
                    pcmso.Situacao = ApplicationConstants.CONSTRUCAO;
                    pcmso.Cep = text_cep.Text;
                    pcmso.Comentario = text_comentario.Text;

                    // chamada no metodo para alterar o estudo em tempo e execucao da inclusão do estudo.
                    pcmso = ppraFacade.alteraPpra(pcmso);

                    MessageBox.Show(Msg04, Msg01);
                }
            }
            catch (ExameException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_incluirAnexo_Click(object sender, EventArgs e)
        {
            try
            {
                //Validando dados


                if (!String.IsNullOrEmpty(text_conteudo.Text))
                {
                    //Incluindo o anexo no banco de dados

                    PpraFacade ppraFacade = PpraFacade.getInstance();

                    Anexo anexo = new Anexo(null, pcmso, Convert.ToString(text_conteudo.Text.ToUpper()));

                    ppraFacade.incluiAnexo(anexo);

                    MontaGridAnexo();

                    text_conteudo.Text = String.Empty;
                    text_conteudo.Focus();

                }
                else
                {
                    throw new AnexoException(AnexoException.msg);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void MontaGridAnexo()
        {
            try
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();

                grd_anexo.Columns.Clear();

                DataSet ds = ppraFacade.findAllAnexoByEstudo(pcmso);

                grd_anexo.DataSource = ds.Tables["Anexos"].DefaultView;

                grd_anexo.Columns[0].HeaderText = "Id";
                grd_anexo.Columns[1].HeaderText = "Id_Estudo";
                grd_anexo.Columns[2].HeaderText = "Conteudo";

                grd_anexo.Columns[0].Visible = false;
                grd_anexo.Columns[1].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_excluirAnexo_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_anexo.CurrentRow != null)
                {
                    PpraFacade ppraFacade = PpraFacade.getInstance();

                    if (MessageBox.Show(msg36, Msg08, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        Int32 cellValue = grd_anexo.CurrentRow.Index;

                        Int64 id = (Int64)grd_anexo.Rows[cellValue].Cells[0].Value;

                        Anexo anexo = new Anexo();
                        anexo.setId(id);

                        ppraFacade.excluiAnexo(anexo);

                        MessageBox.Show(msg37, Msg01);

                        MontaGridAnexo();
                    }
                }
                else
                {
                    MessageBox.Show(Msg07, Msg08);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_excluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_exame.CurrentRow != null)
                {

                    if (MessageBox.Show(msg16, msg4, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                        Int64 id = (Int64)grd_exame.CurrentRow.Cells[0].Value;

                        pcmsoFacade.deleteGheFonteAgenteExame(new GheFonteAgenteExame(id, null, null, 0, ApplicationConstants.DESATIVADO, false, false, false));
                        montaDataGridExame();
                    }

                }
                else
                {
                    MessageBox.Show(msg1, msg2);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void montaDataGridExame()
        {
            try
            {

                grd_exame.Columns.Clear();

                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                // Capturando dados do GheFonteAgente selecionado

                Int32 cellValue = grd_risco.CurrentRow.Index;

                if (!String.IsNullOrEmpty((String)this.grd_risco.CurrentRow.Cells[0].Value.ToString()))
                {
                    DataSet ds = pcmsoFacade.findAllExamesByGheFonteAgente(new GheFonteAgente((Int64)grd_risco.Rows[cellValue].Cells[0].Value));

                    grd_exame.DataSource = ds.Tables["Exames"].DefaultView;

                    grd_exame.Columns[0].HeaderText = "idGheFonteAgenteExame";
                    grd_exame.Columns[0].Visible = false;
                    grd_exame.Columns[0].DisplayIndex = 0;

                    grd_exame.Columns[1].HeaderText = "IdExame";
                    grd_exame.Columns[1].Visible = false;
                    grd_exame.Columns[1].DisplayIndex = 1;

                    grd_exame.Columns[2].HeaderText = "IdGheFonteAgente";
                    grd_exame.Columns[2].Visible = false;
                    grd_exame.Columns[2].DisplayIndex = 2;

                    grd_exame.Columns[3].HeaderText = "Idade Exame";
                    grd_exame.Columns[3].ValueType = typeof(String);
                    grd_exame.Columns[3].DisplayIndex = 6;

                    grd_exame.Columns[4].HeaderText = "Descrição";
                    grd_exame.Columns[4].ReadOnly = true;
                    grd_exame.Columns[4].DisplayIndex = 3;

                    grd_exame.Columns[5].HeaderText = "Situação";
                    grd_exame.Columns[5].Visible = false;
                    grd_exame.Columns[5].DisplayIndex = 4;

                    grd_exame.Columns[6].HeaderText = "Laboratório";
                    grd_exame.Columns[6].ReadOnly = true;
                    grd_exame.Columns[6].DisplayIndex = 5;

                    grd_exame.Columns[7].HeaderText = "Confinado";
                    grd_exame.Columns[7].DisplayIndex = 7;
                    grd_exame.Columns[7].ReadOnly = true;

                    grd_exame.Columns[8].HeaderText = "Altura";
                    grd_exame.Columns[8].DisplayIndex = 8;
                    grd_exame.Columns[8].ReadOnly = true;
                    
                    grd_exame.Columns[9].HeaderText = "Eletricidade";
                    grd_exame.Columns[9].DisplayIndex = 9;
                    grd_exame.Columns[9].ReadOnly = true;

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                // Capturando dados do GheFonteAgente Selecionado.

                if (grd_risco.CurrentRow != null)
                {

                    if (!String.IsNullOrEmpty((String)grd_risco.CurrentRow.Cells[0].Value.ToString()))
                    {
                        Int32 cellValue = grd_risco.CurrentRow.Index;
                        idGheFonteAgente = (Int64)grd_risco.Rows[cellValue].Cells[0].Value;

                        frm_PcmsoExameIncluir formPcmsoExameIncluir = new frm_PcmsoExameIncluir(this, pcmso.VendedorCliente.getCliente(), gheSelecionado, null);
                        formPcmsoExameIncluir.ShowDialog();

                        montaDataGridExame();

                    }
                }
                else
                {
                    throw new GheFonteAgenteException(GheFonteAgenteException.msg);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void grd_exame_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            controleDigitacao = this.grd_exame.CurrentCell.Value.ToString();
            this.grd_exame.CurrentCell.Value = 0;
        }

        private void grd_exame_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                MontaGridPeriodicidade();

                this.btn_exame_alterar.Enabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void MontaGridPeriodicidade()
        {

            try
            {
                grd_periodicidade.Columns.Clear();

                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                // Capturando dados do GheFonteAgenteExame selecionado

                Int32 cellValue = grd_exame.CurrentRow.Index;
                idGheFonteAgenteExame = (Int64)grd_exame.Rows[cellValue].Cells[0].Value;

                DataSet ds = pcmsoFacade.findAllPeriodicidadeByGheFonteAgenteExame(idGheFonteAgenteExame);


                grd_periodicidade.DataSource = ds.Tables["Periodicidades"].DefaultView;

                grd_periodicidade.Columns[0].HeaderText = "idPeriodicidade";
                grd_periodicidade.Columns[1].HeaderText = "Periodicidade";

                grd_periodicidade.Columns[0].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void grd_exame_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                // montando objeto ghefonteAgenteExame e enviando para update.

                GheFonteAgenteExame gheFonteAgenteExameAlterado = new GheFonteAgenteExame((Int64)grd_exame.CurrentRow.Cells[0].Value, null, null,
                    (Int32)grd_exame.CurrentRow.Cells[3].Value, ApplicationConstants.ATIVO, (Boolean)grd_exame.CurrentRow.Cells[7].Value, (Boolean)grd_exame.CurrentRow.Cells[8].Value, (bool)grd_exame.CurrentRow.Cells[9].Value);

                pcmsoFacade.updateGheFonteAgenteExame(gheFonteAgenteExameAlterado);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void grd_exame_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewTextBoxEditingControl)
            {

                e.Control.KeyPress += new KeyPressEventHandler(this.grd_exame_KeyPress);

            }
        }

        private void grd_exame_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (grd_exame.CurrentRow.Cells[3].EditedFormattedValue.ToString().Length < 2)
            {
                e.Handled = ValidaCampoHelper.FormataCampoNumerico(sender, e);
            }
            else
            {
                if (e.KeyChar == (char)8)
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }
            }

            if (e.KeyChar == (char)27)
            {
                if (!String.IsNullOrEmpty(controleDigitacao))
                {
                    this.grd_exame.CurrentCell.Value = controleDigitacao;
                    controleDigitacao = String.Empty;
                }

            }
        }

        private void grd_risco_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                montaDataGridExame();
                grd_periodicidade.Columns.Clear();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*
        private void btn_nota_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_gheExame.CurrentRow != null)
                {

                    Int32 cellValue = grd_gheExame.CurrentRow.Index;

                    Int64 idGhe = (Int64)grd_gheExame.Rows[cellValue].Cells[0].Value;
                    String descricao = (String)grd_gheExame.Rows[cellValue].Cells[2].Value;
                    Int32 numeroExposto = (Int32)grd_gheExame.Rows[cellValue].Cells[3].Value;

                    // montando dados do ghe.

                    gheSelecionado = new Ghe(idGhe, pcmso, descricao, numeroExposto);

                    frm_PcmsoGheNotas formPcmsoGheNotas = new frm_PcmsoGheNotas(this);
                    formPcmsoGheNotas.ShowDialog();

                }
                else
                {
                    throw new GheException(GheException.msg);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
         * */


        private void grd_ghe_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (grd_gheExame.RowCount > 0)
                {
                    MontaGridFuncao();
                    MontaGridRisco();
                    grd_exame.Columns.Clear();
                    grd_periodicidade.Columns.Clear();
                }
                else
                {
                    throw new Exception(msg17);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void grd_ghe_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                MontaGridFuncao();
                MontaGridRisco();
                grd_exame.Columns.Clear();
                grd_periodicidade.Columns.Clear();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void MontaGridFuncao()
        {
            try
            {

                grd_funcao.Columns.Clear();

                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                // Capturando dados do Ghe Selecionado.

                Int32 cellValue = grd_gheExame.CurrentRow.Index;

                Int64 idGhe = (Int64)grd_gheExame.Rows[cellValue].Cells[0].Value;
                String descricao = (String)grd_gheExame.Rows[cellValue].Cells[2].Value;
                Int32 numeroExposto = (Int32)grd_gheExame.Rows[cellValue].Cells[3].Value;
                string numeroPo = grd_gheExame.Rows[cellValue].Cells[4].Value.ToString();

                // montando dados do ghe.

                gheSelecionado = new Ghe(idGhe, pcmso, descricao, numeroExposto, ApplicationConstants.ATIVO, numeroPo);

                // Buscando todas as funçõs do gheSelecionado.

                DataSet ds = pcmsoFacade.findAllFuncaoByGhe(gheSelecionado);

                grd_funcao.DataSource = ds.Tables["Funcoes"].DefaultView;
                grd_funcao.RowsDefaultCellStyle.BackColor = Color.White;

                grd_funcao.Columns[0].HeaderText = "IdFuncao";
                grd_funcao.Columns[1].HeaderText = "Descricao";
                grd_funcao.Columns[2].HeaderText = "Cod_CBO";
                grd_funcao.Columns[3].HeaderText = "Situacao";
                grd_funcao.Columns[4].HeaderText = "Comentário";

                grd_funcao.Columns[0].Visible = false;
                grd_funcao.Columns[3].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        public void MontaGridRisco()
        {
            try
            {
                grd_risco.Columns.Clear();

                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                // Capturando dados do Ghe Selecionado.

                Int32 cellValue = grd_gheExame.CurrentRow.Index;

                Int64 idGhe = (Int64)grd_gheExame.Rows[cellValue].Cells[0].Value;
                String descricao = (String)grd_gheExame.Rows[cellValue].Cells[2].Value;
                Int32 numeroExposto = (Int32)grd_gheExame.Rows[cellValue].Cells[3].Value;
                string numeroPo = grd_gheExame.Rows[cellValue].Cells[4].Value.ToString();

                // montando dados do ghe.

                gheSelecionado = new Ghe(idGhe, pcmso, descricao, numeroExposto, ApplicationConstants.ATIVO, numeroPo );

                // Buscando todas as funçõs do gheSelecionado.

                DataSet ds = pcmsoFacade.findAllRiscosByGhe(gheSelecionado);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    grd_risco.DataSource = ds.Tables["Riscos"].DefaultView;

                    grd_risco.Columns[0].HeaderText = "idGheFonteAgente";
                    grd_risco.Columns[1].HeaderText = "id_risco";
                    grd_risco.Columns[2].HeaderText = "Agente";
                    grd_risco.Columns[3].HeaderText = "Danos";
                    grd_risco.Columns[4].HeaderText = "Limite";
                    grd_risco.Columns[5].HeaderText = "Risco";

                    grd_risco.Columns[0].Visible = false;
                    grd_risco.Columns[1].Visible = false;

                    grd_risco.Columns[0].DisplayIndex = 0;
                    grd_risco.Columns[1].DisplayIndex = 1;
                    grd_risco.Columns[2].DisplayIndex = 2;
                    grd_risco.Columns[3].DisplayIndex = 4;
                    grd_risco.Columns[4].DisplayIndex = 5;
                    grd_risco.Columns[5].DisplayIndex = 3;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btn_excluirMedico_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_medico.CurrentRow != null)
                {
                    PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                    if (MessageBox.Show(msg12, msg4, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {

                        Int32 cellValue = grd_medico.CurrentRow.Index;

                        Int64 id_medico = (Int64)grd_medico.Rows[cellValue].Cells[0].Value;

                        Medico medico = new Medico();
                        medico.Id = id_medico;

                        MedicoEstudo medicoEstudo = new MedicoEstudo(pcmso, medico, false, ApplicationConstants.ATIVO);

                        pcmsoFacade.excluirMedicoEstudo(medicoEstudo);

                        MessageBox.Show(msg13, msg4);

                        montaDataGridMedico();
                    }
                }
                else
                {
                    MessageBox.Show(msg1, msg2);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void montaDataGridMedico()
        {
            try
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();
                grd_medico.Columns.Clear();

                DataSet ds = pcmsoFacade.findAllMedicoEstudo(pcmso);

                grd_medico.DataSource = ds.Tables["Medicos"].DefaultView;

                grd_medico.Columns[0].HeaderText = "IdMedico";
                grd_medico.Columns[1].HeaderText = "Nome";
                grd_medico.Columns[2].HeaderText = "CRM";
                grd_medico.Columns[3].HeaderText = "Situação";

                grd_medico.Columns[0].Visible = false;
                grd_medico.Columns[3].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_incluirMedico_Click(object sender, EventArgs e)
        {
            frm_PcmsoMedicoIncluir formPcmsoIncluir = new frm_PcmsoMedicoIncluir(pcmso);
            formPcmsoIncluir.ShowDialog();

            if (formPcmsoIncluir.getMedicoEstudo() != null)
            {
                montaDataGridMedico();
            }
        }

        private void btn_excluirHospital_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_hospital.CurrentRow != null)
                {
                    PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                    if (MessageBox.Show(msg10, msg4, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {

                        Int32 cellValue = grd_hospital.CurrentRow.Index;

                        Int64 id_hospital = (Int64)grd_hospital.Rows[cellValue].Cells[0].Value;

                        Hospital hospital = new Hospital();
                        hospital.Id = id_hospital;

                        HospitalEstudo hospitalEstudo = new HospitalEstudo(pcmso, hospital);

                        pcmsoFacade.excluirHospitalEstudo(hospitalEstudo);

                        MessageBox.Show(msg11, msg4);

                        montaDataGridHospital();

                    }
                }
                else
                {
                    MessageBox.Show(msg1, msg2);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void montaDataGridHospital()
        {
            try
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();
                grd_hospital.Columns.Clear();

                DataSet ds = pcmsoFacade.findAllHospitalEstudo(pcmso);

                grd_hospital.DataSource = ds.Tables["Hospitais"].DefaultView;

                grd_hospital.Columns[0].HeaderText = "IdHospital";
                grd_hospital.Columns[1].HeaderText = "Nome";
                grd_hospital.Columns[2].HeaderText = "Endereço";
                grd_hospital.Columns[3].HeaderText = "Número";
                grd_hospital.Columns[4].HeaderText = "Complemento";
                grd_hospital.Columns[5].HeaderText = "Bairro";
                grd_hospital.Columns[6].HeaderText = "Cidade";
                grd_hospital.Columns[7].HeaderText = "CEP";
                grd_hospital.Columns[8].HeaderText = "UF";
                grd_hospital.Columns[9].HeaderText = "Telefone";
                grd_hospital.Columns[10].HeaderText = "Fax";
                grd_hospital.Columns[11].HeaderText = "Observação";
                grd_hospital.Columns[12].HeaderText = "Situação";

                grd_hospital.Columns[0].Visible = false;
                grd_hospital.Columns[12].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_incluirHospital_Click(object sender, EventArgs e)
        {
            frm_PcmsoHospitaisIncluir formPcmsoHospitalIncluir = new frm_PcmsoHospitaisIncluir(this);
            formPcmsoHospitalIncluir.ShowDialog();
        }

        private Boolean validaQuantidadeZero()
        {
            Boolean retorno = true;

            foreach (DataGridViewRow dvRom in grd_material.Rows)
            {
                if ((Int32)dvRom.Cells[3].Value == (Int32)0)
                {
                    retorno = false;
                    break;
                }
            }
            return retorno;
        }

        private void btn_salvarMaterial_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaQuantidadeZero())
                {
                    PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                    MaterialHospitalar materialHospitalar;
                    MaterialHospitalarEstudo materialHospitalarEstudo;


                    foreach (DataGridViewRow dvRom in grd_material.Rows)
                    {
                        materialHospitalar = new MaterialHospitalar((Int64)dvRom.Cells[1].Value, null, null, null);
                        materialHospitalarEstudo = new MaterialHospitalarEstudo(pcmso, materialHospitalar, (Int32)dvRom.Cells[3].Value);
                        pcmsoFacade.alteraMaterialHospitalarEstudo(materialHospitalarEstudo);


                    }
                    MessageBox.Show(msg7, msg2);

                }
                else
                {
                    throw new MaterialHospitalarException(MaterialHospitalarException.msg3);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_excluirMaterial_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_material.CurrentRow != null)
                {

                    PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                    if (MessageBox.Show(msg8, msg4, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {

                        Int32 cellValue = grd_material.CurrentRow.Index;

                        Int64 id_Atividade = (Int64)grd_material.Rows[cellValue].Cells[1].Value;

                        pcmsoFacade.excluirMaterialHospitalarEstudo(id_Atividade, pcmso);

                        MessageBox.Show(msg9, msg4);

                        MontaDataGridMaterialHospitalar();

                    }
                }
                else
                {
                    MessageBox.Show(msg1, msg2);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void MontaDataGridMaterialHospitalar()
        {
            try
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                grd_material.Columns.Clear();

                DataSet ds = pcmsoFacade.findAllMaterialHospitalarEstudo(pcmso);

                grd_material.DataSource = ds.Tables["MateriaisHospitalaresEstudos"].DefaultView;

                grd_material.Columns[0].HeaderText = "IdEstudo";
                grd_material.Columns[1].HeaderText = "IdMaterial";
                grd_material.Columns[2].HeaderText = "Descrição";
                grd_material.Columns[3].HeaderText = "Quantidade";

                grd_material.Columns[0].Visible = false;
                grd_material.Columns[1].Visible = false;

                grd_material.Columns[2].ReadOnly = true;

                grd_material.Columns[3].DefaultCellStyle.BackColor = Color.Bisque;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_incluirMaterial_Click(object sender, EventArgs e)
        {
            frm_pcmoMaterialIncluir formMaterialIncluir = new frm_pcmoMaterialIncluir(this);
            formMaterialIncluir.ShowDialog();
        }

        private void grd_material_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewTextBoxEditingControl)
            {
                e.Control.KeyPress += new KeyPressEventHandler(grd_material_KeyPress);

            }
        }

        private void grd_material_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != (char)8)
            {
                e.Handled = true;

            }
        }

        private void btn_exame_alterar_Click(object sender, EventArgs e)
        {
            try
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                if (this.grd_exame.CurrentRow != null)
                {
                    Int64 id = (Int64)grd_exame.CurrentRow.Cells[0].Value;

                    GheFonteAgenteExame GheFonteAgenteExame = pcmsoFacade.findGheFonteAgenteExameById(id);

                    frm_PcmsoExameIncluir formPcmsoExameIncluir = new frm_PcmsoExameIncluir(this, pcmso.VendedorCliente.getCliente(), gheSelecionado, GheFonteAgenteExame);
                    formPcmsoExameIncluir.ShowDialog();

                    montaGridGheExame();
                    montaDataGridExame();
                }
                else
                {
                    MessageBox.Show(msg38);
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        private void grd_setorFuncao_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 5 || e.ColumnIndex == 6)
            {
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                grd_setorFuncao.EndEdit();

                pcmsoFacade.updateGheSetorClienteFuncao(new GheSetorClienteFuncao((Int64)grd_setorFuncao.Rows[e.RowIndex].Cells[0].Value, null, null, (Boolean)grd_setorFuncao.Rows[e.RowIndex].Cells[5].Value, (Boolean)grd_setorFuncao.Rows[e.RowIndex].Cells[6].Value, ApplicationConstants.ATIVO, (Int64?)grd_setorFuncao.Rows[e.RowIndex].Cells[8].Value == null ? null : new ComentarioFuncao((Int64)grd_setorFuncao.Rows[e.RowIndex].Cells[8].Value, null, grd_setorFuncao.Rows[e.RowIndex].Cells[4].Value.ToString(), string.Empty), (bool)grd_setorFuncao.Rows[e.RowIndex].Cells[9].Value)); 
            }
        }

        private void grd_setorFuncao_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 7)
            {
                DataGridViewCell cell = this.grd_setorFuncao.Rows[e.RowIndex].Cells[e.ColumnIndex];

                cell.ToolTipText = msg40;
            }

            else if (e.ColumnIndex == 5)
            {
                DataGridViewCell cell = this.grd_setorFuncao.Rows[e.RowIndex].Cells[e.ColumnIndex];

                cell.ToolTipText = msg43;
            }
            else if (e.ColumnIndex == 6)
            {
                DataGridViewCell cell = this.grd_setorFuncao.Rows[e.RowIndex].Cells[e.ColumnIndex];

                cell.ToolTipText = msg44;

            }
        }
        
        public void montaGridEstimativa()
        {
            this.dgPrevisao.Columns.Clear();

            PpraFacade ppraFacade = PpraFacade.getInstance();

            DataSet ds = ppraFacade.findAllEstimativaInEstudo(pcmso);

            dgPrevisao.DataSource = ds.Tables[0].DefaultView;

            dgPrevisao.Columns[0].HeaderText = "Id_Estimativa_estudo";
            dgPrevisao.Columns[0].Visible = false;

            dgPrevisao.Columns[1].HeaderText = "Id_Estimativa";
            dgPrevisao.Columns[1].Visible = false;

            dgPrevisao.Columns[2].HeaderText = "Id Estudo";
            dgPrevisao.Columns[2].Visible = false;

            dgPrevisao.Columns[3].HeaderText = "Estimativa";
            dgPrevisao.Columns[3].ReadOnly = true;

            dgPrevisao.Columns[4].HeaderText = "Qtde";
            dgPrevisao.Columns[4].DefaultCellStyle.BackColor = Color.White;

        }

        private void btnIncluirPrevisao_Click(object sender, EventArgs e)
        {
            try
            {
                /* somente poderá ser incluído caso o estudo já tenha sido gravado */
                if (pcmso.Id == null)
                    throw new Exception(msg41);

                frmPcmsoEstimativa formEstimativa = new frmPcmsoEstimativa(pcmso);
                formEstimativa.ShowDialog();

                montaGridEstimativa();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnExcluirPrevisao_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgPrevisao.CurrentRow == null)
                {
                    throw new Exception(Msg07);
                }

                PpraFacade ppraFacade = PpraFacade.getInstance();

                ppraFacade.deleteEstimativaEstudo(new EstimativaEstudo(Convert.ToInt64(dgPrevisao.CurrentRow.Cells[0].Value),
                                pcmso, new Estimativa(Convert.ToInt64(dgPrevisao.CurrentRow.Cells[1].Value), (String)dgPrevisao.CurrentRow.Cells[3].Value, String.Empty),
                                Convert.ToInt32(dgPrevisao.CurrentRow.Cells[4].EditedFormattedValue)));

                montaGridEstimativa();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void dgPrevisao_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                /* validando o valor digitado caso o usuário não tenha digitado nenhuma informação o campo
                 * obrigatoriamente ficará com zero */

                if (String.IsNullOrEmpty(dgPrevisao.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString().Trim()))
                {
                    this.dgPrevisao.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 0;
                }

                /* gravando a informação no banco de dados */

                PpraFacade ppraFacade = PpraFacade.getInstance();


                ppraFacade.updateEstimaEstudo(new EstimativaEstudo(Convert.ToInt64(dgPrevisao.Rows[e.RowIndex].Cells[0].Value),
                                pcmso, new Estimativa(Convert.ToInt64(dgPrevisao.Rows[e.RowIndex].Cells[1].Value), (String)dgPrevisao.Rows[e.RowIndex].Cells[3].Value, String.Empty),
                                Convert.ToInt32(dgPrevisao.Rows[e.RowIndex].Cells[4].EditedFormattedValue)));


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void dgPrevisao_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                /*validando valor digitado para não dar estouro de inteiro32 */

                int number = Int32.Parse((String)dgPrevisao.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue);
            }

            catch (OverflowException)
            {
                MessageBox.Show(msg42, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void dgPrevisao_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewTextBoxEditingControl)
            {
                e.Control.KeyPress += new KeyPressEventHandler(dgPrevisao_KeyPress);

            }
        }

        private void dgPrevisao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != (char)8)
            {
                e.Handled = true;

            }
        }

        private void grd_setorFuncao_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 10)
            {
                PpraFacade ppraFacade = PpraFacade.getInstance();
                PcmsoFacade pcmsoFacade = PcmsoFacade.getInstance();

                frmFuncaoComentario buscarComentario = new frmFuncaoComentario(
                    ppraFacade.findFuncaoById((Int64)grd_setorFuncao.Rows[e.RowIndex].Cells[2].Value));
                buscarComentario.ShowDialog();

                if (buscarComentario.ComentarioFuncao != null)
                {
                    gheSetorClienteFuncao = ppraFacade.findGheSetorClienteFuncaoById((Int64)grd_setorFuncao.Rows[e.RowIndex].Cells[0].Value);
                    gheSetorClienteFuncao.ComentarioFuncao = buscarComentario.ComentarioFuncao;
                    pcmsoFacade.updateGheSetorClienteFuncao(gheSetorClienteFuncao);
                }

                montaGridClienteFuncao();
            }
        }


    }
}