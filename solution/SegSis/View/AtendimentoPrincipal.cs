﻿using SWS.Entidade;
using SWS.Excecao;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmAtendimentoPrincipal : frmTemplate
    {

        private Aso atendimento;
        private Cliente cliente;
        private ClienteFuncao clienteFuncao;
        
        public frmAtendimentoPrincipal()
        {
            InitializeComponent();
            validaPermissoes();
            /*montando combos de pesquisa */
            ComboHelper.PeriodicidadeASo(cbTipo);
            ComboHelper.situacaoAso(cbSituacao);

            /* verificando restrinção cliente do usuário. Caso o usuario tenha apenas um 
             * cliente então não será permitido que ele selecione outro cliente. */
            if (PermissionamentoFacade.usuarioAutenticado.UsuarioCliente.Count == 1)
            {
                cliente = PermissionamentoFacade.usuarioAutenticado.UsuarioCliente.ElementAt(0).Cliente;
                textCliente.Text = cliente.RazaoSocial;
                btCliente.Enabled = false;
                btnExcluirCliente.Enabled = false;
            }

            /* caso o usuário tenha mais de um cliente relacionado então 
             * será necessário que ele possa selecionar o cliente para pesquisa. */

            ActiveControl = textCodigo;
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            this.btIncluir.Enabled = permissionamentoFacade.hasPermission("ASO", "INCLUIR");
            this.btAlterar.Enabled = permissionamentoFacade.hasPermission("ASO", "ALTERAR");
            this.btImprimir.Enabled = permissionamentoFacade.hasPermission("ASO", "IMPRIMIR");
            this.btFinalizar.Enabled = permissionamentoFacade.hasPermission("ASO", "FINALIZAR");
            this.btCancelar.Enabled = permissionamentoFacade.hasPermission("ASO", "CANCELAR");
            this.btLaudar.Enabled = permissionamentoFacade.hasPermission("ASO", "LAUDAR");
            this.bt2Via.Enabled = permissionamentoFacade.hasPermission("ASO", "IMPRIME_SEGUNDA_VIA");
            this.btProntuario.Enabled = permissionamentoFacade.hasPermission("ASO", "PRONTUARIZAR");
            this.btAnexo.Enabled = permissionamentoFacade.hasPermission("ASO", "ANEXAR");
            this.btTermica.Enabled = permissionamentoFacade.hasPermission("ASO", "IMPRIMIR_TERMICA");
            this.btPesquisa.Enabled = permissionamentoFacade.hasPermission("ASO", "PESQUISAR");

        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                frmAtendimentoIncluir incluirAtendimento = new frmAtendimentoIncluir();
                incluirAtendimento.ShowDialog();
                if (incluirAtendimento.Atendimento != null && incluirAtendimento.Atendimento.Id != null)
                {
                    textCodigo.Text = incluirAtendimento.Atendimento.Codigo;
                    btPesquisa.PerformClick();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_alterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvAtendimento.CurrentRow == null)
                throw new Exception("Selecione uma linha");

                this.Cursor = Cursors.WaitCursor;

                AsoFacade asoFacade = AsoFacade.getInstance();
                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                atendimento = asoFacade.findAtendimentoById((Int64)dgvAtendimento.CurrentRow.Cells[0].Value);

                if (String.Equals(atendimento.Situacao, ApplicationConstants.CONSTRUCAO))
                {
                    frmAtendimentoAlterar alterarAtendimento = new frmAtendimentoAlterar(atendimento);
                    alterarAtendimento.ShowDialog();
                    btPesquisa.PerformClick();
                }
                
                else if (string.Equals(atendimento.Situacao, ApplicationConstants.DESATIVADO))
                        throw new Exception("Atendimento cancelado não poderá ser alterado.");
                else
                {
                
                    /* atendimento finalizado */
                    if (permissionamentoFacade.hasPermission("ASO", "ALTERAR_ASO_DEPOIS_FINALIZADO"))
                    {
                        frmAtendimentoAlterar alterarAtendimentoRealizado = new frmAtendimentoAlterar(atendimento);
                        alterarAtendimentoRealizado.ShowDialog();
                        btPesquisa.PerformClick();
                        
                    }
                    else
                    {
                        throw new Exception("Você não tem autorização para alterar um atendimento já finalizado.");
                    }
                
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_finalizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvAtendimento.CurrentRow == null)
                    throw new Exception("Selecione uma linha");
                
                this.Cursor = Cursors.WaitCursor;

                AsoFacade asoFacade = AsoFacade.getInstance();
                
                atendimento = asoFacade.findAtendimentoById((Int64)dgvAtendimento.CurrentRow.Cells[0].Value);
                atendimento.Finalizador = PermissionamentoFacade.usuarioAutenticado;

                asoFacade.finalizaAso(atendimento);
                MessageBox.Show("Atendimento Finalizado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.btPesquisa.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_cancelado_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvAtendimento.CurrentRow == null)
                    throw new Exception("Selecione uma linha");

                if (MessageBox.Show("Deseja cancelar o Atendimento selecionado?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                 
                    this.Cursor = Cursors.WaitCursor;
                    
                    AsoFacade asoFacade = AsoFacade.getInstance();
                    
                    atendimento = asoFacade.findAtendimentoById((Int64)dgvAtendimento.CurrentRow.Cells[0].Value);

                    /* se o atendimento já estiver cancelado entao será levantada a exceção */

                    if (String.Equals(atendimento.Situacao, ApplicationConstants.DESATIVADO))
                        throw new Exception("Atendimento já cancelado.");

                    /* gravando justificativa de cancelamento tarefa 113 */

                    frmJustificativa JustificativaCancelamentoAtendimento = new frmJustificativa("CANCELAR ATENDIMENTO");
                    JustificativaCancelamentoAtendimento.ShowDialog();

                    if (String.IsNullOrEmpty(JustificativaCancelamentoAtendimento.Justificativa))
                        throw new Exception("Para cancelar o atendimento você deve informar uma justificativa.");
                        
                    else
                        atendimento.Justificativa = JustificativaCancelamentoAtendimento.Justificativa;

                    atendimento.UsuarioCancelamento = PermissionamentoFacade.usuarioAutenticado;
                    asoFacade.cancelaAso(atendimento);

                    MessageBox.Show("Atendimento cancelado com sucesso." , "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    dgvAtendimento.Columns.Clear();
                    this.btPesquisa.PerformClick();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_imprimir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvAtendimento.CurrentRow == null)
                    throw new Exception("Selecione uma linha");

                if (MessageBox.Show("Deseja imprimir o Atendimento selecionado?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    AsoFacade asoFacade = AsoFacade.getInstance();
                    atendimento = asoFacade.findAtendimentoById((Int64)dgvAtendimento.CurrentRow.Cells[0].Value);

                    if (atendimento.Situacao.Equals(ApplicationConstants.DESATIVADO))
                        throw new AsoException(AsoException.msg6);

                     /* 21072014 - iniciando permissão para imprimir aso inapto. welbert.l.santos */
                     /* verificando se o aso já foi laudado. */

                     if (String.IsNullOrEmpty(dgvAtendimento.CurrentRow.Cells[14].Value.ToString()))
                     {
                         frmAtendimentoImprimir formAsoImprimir = new frmAtendimentoImprimir(atendimento, false);
                         formAsoImprimir.ShowDialog();
                     }
                     /* verificando se o aso está inapto. */
                     else if (String.Equals((String)dgvAtendimento.CurrentRow.Cells[14].Value, ApplicationConstants.LAUDOINAPTO))
                     {
                         PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                         
                         if (!permissionamentoFacade.hasPermission("ASO", "IMPRIMIR_ASO_INAPTO"))
                             throw new Exception("Usuário sem permissão para imprimir aso INAPTO.");
                         
                         frmAtendimentoImprimir formAsoImprimir = new frmAtendimentoImprimir(atendimento, false);
                         formAsoImprimir.ShowDialog();
                     }
                     else
                     {
                         frmAtendimentoImprimir formAsoImprimir = new frmAtendimentoImprimir(atendimento, false);
                         formAsoImprimir.ShowDialog();
                     }
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_anexos_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvAtendimento.CurrentRow == null)
                    throw new Exception ("Selecione uma linha");
             
                AsoFacade asoFacade = AsoFacade.getInstance();
                frmAtendimentoAnexo formAnexoAsoPrincipal = new frmAtendimentoAnexo(asoFacade.findAtendimentoById((Int64)dgvAtendimento.CurrentRow.Cells[0].Value));
                formAnexoAsoPrincipal.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_2via_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvAtendimento.CurrentRow == null)
                    throw new Exception("Selecione uma linha");

                if (MessageBox.Show("Deseja gerar uma 2 via de impressão do atendimento? Poderá haver cobranças ao cliente. ", "Confirmação", MessageBoxButtons.YesNo,MessageBoxIcon.Question) == DialogResult.Yes)
                {
                
                    AsoFacade asoFacade = AsoFacade.getInstance();
                    
                    atendimento = asoFacade.findAtendimentoById((Int64)dgvAtendimento.CurrentRow.Cells[0].Value);

                    if (atendimento.Situacao.Equals(ApplicationConstants.DESATIVADO))
                        throw new AsoException(AsoException.msg6);
                    
                    asoFacade.gera2Via(atendimento);
                    
                    /* 21072014 - iniciando permissão para imprimir aso inapto.                      
                     * welbert.l.santos
                     * verificando se o aso já foi laudado. */

                    if (String.IsNullOrEmpty(dgvAtendimento.CurrentRow.Cells[14].Value.ToString()))
                    {
                        frmAtendimentoImprimir formAsoImprimir = new frmAtendimentoImprimir(atendimento, true);
                        formAsoImprimir.ShowDialog();
                    }

                    /* verificando se o aso está inapto. */
                    else if (String.Equals((String)dgvAtendimento.CurrentRow.Cells[14].Value, ApplicationConstants.LAUDOINAPTO))
                    {
                        PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                        
                        if (!permissionamentoFacade.hasPermission("ASO", "IMPRIMIR_ASO_INAPTO"))
                            throw new Exception("Usuário sem permissão para imprimir aso INAPTO.");
                            
                        frmAtendimentoImprimir formAsoImprimir = new frmAtendimentoImprimir(atendimento, true);
                        formAsoImprimir.ShowDialog();
                    }
                    else
                    {
                        frmAtendimentoImprimir formAsoImprimir = new frmAtendimentoImprimir(atendimento, true);
                        formAsoImprimir.ShowDialog();
                    }
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_imprimir_termica_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvAtendimento.CurrentRow == null)
                    throw new Exception ("Selecione uma linha");
              
                AsoFacade asoFacade = AsoFacade.getInstance();
                atendimento = asoFacade.findAtendimentoById((Int64)dgvAtendimento.CurrentRow.Cells[0].Value);

                /* verificando se o padrão é gravar o atendimento bloqueado. Caso seja, então será liberado a impressão da
                 * filipeta térmica */

                string atendimentoBloqueado;
                PermissionamentoFacade.mapaConfiguracoes.TryGetValue(SWS.Resources.ConfigurationConstans.GRAVA_ATENDIMENTO_BLOQUEADO, out atendimentoBloqueado);

                if (Convert.ToBoolean(atendimentoBloqueado) != true)
                {
                    if (!String.Equals(atendimento.Situacao, ApplicationConstants.CONSTRUCAO))
                        throw new Exception("É permitido somente impressão para atendimentos em construção");
                }

                frmAtendimentoImprimirEncaminhamento formAsoImprimirTermica = new frmAtendimentoImprimirEncaminhamento(atendimento, true);
                formAsoImprimirTermica.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_laudar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (dgvAtendimento.CurrentRow == null)
                    throw new Exception("Selecione uma linha");
                    
                if (!String.Equals(dgvAtendimento.CurrentRow.Cells[11].Value, ApplicationConstants.FECHADO))
                    throw new Exception ("Somente é possível laudar asos já finalizados.");

                /* verificado se o atendimento já foi laudado. Caso tenha sido, somente quem tem permissão para
                 * alterar o laudo poderá acessar a tela */

                PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();
                
                    AsoFacade asoFacade = AsoFacade.getInstance();
                    atendimento = asoFacade.findAsoByIdComExamesTranscritos((Int64)dgvAtendimento.CurrentRow.Cells[0].Value);
                    
                    if (MessageBox.Show("Deseja laudar o aso selecionado?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        /* verificando lista de exames para laudar somente exames que foram finalizados e não estão cancelados 23/07/2015 */
                        
                        FilaFacade filaFacade = FilaFacade.getInstance();
                        LinkedList<ItemFilaAtendimento> examesLaudo = filaFacade.buscaExamesInSituacaoByAso(atendimento, true, true, false, null, false);

                        /* verificado se o atendimento já foi laudado. Caso tenha sido, somente quem tem permissão para
                        * alterar o laudo poderá acessar a tela */

                        if (!string.IsNullOrEmpty(atendimento.Conclusao) && !permissionamentoFacade.hasPermission("ASO", "ALTERAR_LAUDO"))
                            throw new Exception("Sem permissão para alterar o laudo do atendimento.");

                        frmAtendimentoLaudar formAsoLaudar = new frmAtendimentoLaudar(atendimento, examesLaudo, true);
                        formAsoLaudar.ShowDialog();
                    }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_prontuarioDigital_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                AsoFacade asoFacade = AsoFacade.getInstance();

                if (this.dgvAtendimento.CurrentRow == null)
                    throw new Exception("Selecione uma linha");
                
                atendimento = asoFacade.findAtendimentoById((Int64)dgvAtendimento.CurrentRow.Cells[0].Value);
                
                Prontuario prontuario = asoFacade.findProntuarioByAso(atendimento);

                if (prontuario != null)
                {
                    frmProntuario formProntuario = new frmProntuario(prontuario, true);
                    formProntuario.ShowDialog();
                }
                else
                {
                    /* procurando o último prontuário do colaborador */

                    Prontuario lasProntuario = asoFacade.findLastProntuarioByFuncionario(atendimento.ClienteFuncaoFuncionario.Funcionario);
                
                    if (lasProntuario != null)
                    {
                        if (MessageBox.Show("Deseja carregar o último prontuário do colaborador?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            lasProntuario.Aso = atendimento;
                            frmProntuario formProntuario = new frmProntuario(lasProntuario, false);
                            formProntuario.ShowDialog();
                        }
                        else
                        {
                            prontuario = new Prontuario();
                            prontuario.Aso = atendimento;

                            frmProntuario formProntuario = new frmProntuario(prontuario, false);
                            formProntuario.ShowDialog();
                        }
                    }
                    else
                    {
                        prontuario = new Prontuario();
                        prontuario.Aso = atendimento;

                        frmProntuario formProntuario = new frmProntuario(prontuario, false);
                        formProntuario.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_pesquisar_Click(object sender, EventArgs e)
        {
            try
            {

                /*fazendo as validações de tela para a pesquisa.
                 *se o usuario selecionou o cliente, ou o cliente e a função, ou não
                 *preencheu nenhum atributo do funcionário ou do aso então será obrigatório
                 *usar o período para o filtro.
                 */

                if (!this.dataEmissaoFinal.Checked && !this.dataEmissaoInicial.Checked && !this.dataGravacaoInicial.Checked && !this.dataGravacaoFinal.Checked)
                {
                    if (clienteFuncao == null)
                    {
                        if (String.IsNullOrWhiteSpace(textCpf.Text.Replace("-", "").Replace(".", "")))
                        {
                            if (String.IsNullOrEmpty(textRg.Text))
                            {
                                if (String.IsNullOrWhiteSpace((textCodigo.Text.Replace(".", ""))))
                                {
                                    if (String.IsNullOrEmpty(textNome.Text.Trim()))
                                    {
                                        throw new AsoException(AsoException.msg1);
                                    }
                                }

                            }
                        }

                    }

                }

                /* verificando para saber se as datas estão em formato crescente e com lógica */

                if (dataEmissaoInicial.Checked)
                {
                    if (!dataEmissaoFinal.Checked)
                        throw new Exception("Você deve marcar também a data de emissão final para a pesquisa");

                    if (Convert.ToDateTime(dataEmissaoInicial.Text) > Convert.ToDateTime(dataEmissaoFinal.Text))
                        throw new Exception("A data de emissão final não pode ser maior que a data de emissão final.");
                }

                if (this.dataGravacaoInicial.Checked)
                {
                    if (!this.dataGravacaoFinal.Checked)
                        throw new Exception("Você deve marcar também a data de gravação final para a pesquisa");

                    if (Convert.ToDateTime(dataGravacaoInicial.Text) > Convert.ToDateTime(dataGravacaoFinal.Text))
                        throw new Exception("A data de emissão final não pode ser maior que a data de emissão final.");
                }

                montaDataGrid();
                textCodigo.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_cliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar formCliente = new frmClienteSelecionar(null, null, false, null, null, null, null, null, null);
                formCliente.ShowDialog();

                if (formCliente.Cliente != null)
                {
                    cliente = formCliente.Cliente;
                    textCliente.Text = cliente.RazaoSocial;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_funcao_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new ClienteException(ClienteException.msg2);

                frmClienteFuncaoSeleciona incluirClienteFuncao = new frmClienteFuncaoSeleciona(cliente);
                incluirClienteFuncao.ShowDialog();

                if (incluirClienteFuncao.ClienteFuncao != null)
                {
                    clienteFuncao = incluirClienteFuncao.ClienteFuncao;
                    textFuncao.Text = clienteFuncao.Funcao.Descricao;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void montaDataGrid()
        {
            try
            {

                this.Cursor = Cursors.WaitCursor;

                this.dgvAtendimento.Columns.Clear();
                textTotal.Text = String.Empty;

                AsoFacade asoFacade = AsoFacade.getInstance();

                DataSet ds = asoFacade.findAsoByFilter(new Aso(null, null, null, textCodigo.Text, dataEmissaoInicial.Checked ? Convert.ToDateTime(dataEmissaoInicial.Text + " 00:00:00") : (DateTime?)null, new Funcionario(textNome.Text.Trim(), textRg.Text, DateTime.Now, textCpf.Text, string.Empty, string.Empty), cliente, cbTipo.SelectedIndex != 0 ? new Periodicidade(Convert.ToInt64(((SelectItem)cbTipo.SelectedItem).Valor), ((SelectItem)cbTipo.SelectedItem).Nome.ToString(), null) : null, cbSituacao.SelectedIndex != 0 ? ((SelectItem)cbSituacao.SelectedItem).Valor : string.Empty, null, null, null, null, null, null, null, null, dataGravacaoInicial.Checked ? Convert.ToDateTime(dataGravacaoInicial.Text + " 00:00:00") : (DateTime?)null, string.Empty, String.Empty, String.Empty, null, null, string.Empty, null, null, null, string.Empty, false, string.Empty, string.Empty, string.Empty), clienteFuncao, this.dataEmissaoFinal.Checked ? Convert.ToDateTime(dataEmissaoFinal.Text + " 23:59:59") : (DateTime?)null, this.dataGravacaoFinal.Checked ? Convert.ToDateTime(dataGravacaoFinal.Text + " 23:59:59") : (DateTime?)null);

                dgvAtendimento.DataSource = ds.Tables["Asos"].DefaultView; 

                // montando ids das colunas

                dgvAtendimento.Columns[0].HeaderText = "id_aso";
                dgvAtendimento.Columns[0].Visible = false;

                dgvAtendimento.Columns[1].HeaderText = "id_cliente_funcao_funcionario";
                dgvAtendimento.Columns[1].Visible = false;

                dgvAtendimento.Columns[2].HeaderText = "Código Atendimento";
                dgvAtendimento.Columns[3].HeaderText = "Data";
                dgvAtendimento.Columns[4].HeaderText = "Nome Funcionário";
                dgvAtendimento.Columns[5].HeaderText = "CPF";
                dgvAtendimento.Columns[6].HeaderText = "Matrícula";
                dgvAtendimento.Columns[7].HeaderText = "Razão Social";
                dgvAtendimento.Columns[8].HeaderText = "CNPJ";

                dgvAtendimento.Columns[9].HeaderText = "id_periodicidade";
                dgvAtendimento.Columns[9].Visible = false;

                dgvAtendimento.Columns[10].HeaderText = "Periodicidade";
                dgvAtendimento.Columns[11].HeaderText = "Situação";
                dgvAtendimento.Columns[12].HeaderText = "Criado Por";
                dgvAtendimento.Columns[13].HeaderText = "Data Finalização";
                dgvAtendimento.Columns[14].HeaderText = "Laudo";
                dgvAtendimento.Columns[15].HeaderText = "Finalizado Por";
                dgvAtendimento.Columns[16].HeaderText = "Cancelado Por";
                dgvAtendimento.Columns[17].HeaderText = "Prioridade";
                dgvAtendimento.Columns[17].Visible = false;
                dgvAtendimento.Columns[18].HeaderText = "PIS";
                dgvAtendimento.Columns[18].DisplayIndex = 6;
                dgvAtendimento.Columns[19].HeaderText = "CTPS";
                dgvAtendimento.Columns[19].DisplayIndex = 7;
                dgvAtendimento.Columns[20].HeaderText = "RG";
                dgvAtendimento.Columns[20].DisplayIndex = 8;
                dgvAtendimento.Columns[21].HeaderText = "UF_RG";
                dgvAtendimento.Columns[21].DisplayIndex = 9;

                textTotal.Text = dgvAtendimento.RowCount.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void grd_aso_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (string.Equals(dgv.Rows[e.RowIndex].Cells[11].Value.ToString(), ApplicationConstants.DESATIVADO))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;

            else if (String.Equals(dgv.Rows[e.RowIndex].Cells[11].Value.ToString(), ApplicationConstants.FECHADO))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.DarkGreen;

            else if (Convert.ToBoolean(dgv.Rows[e.RowIndex].Cells[17].Value))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.RoyalBlue;

            else if (String.Equals(dgv.Rows[e.RowIndex].Cells[14].Value.ToString(), ApplicationConstants.LAUDOINAPTO))
                dgv.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Gray;

            else if (String.Equals(dgv.Rows[e.RowIndex].Cells[11].Value.ToString(), ApplicationConstants.BLOQUEADO))
                dgv.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Orange;

            
        }

        private void grd_aso_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                if (dgvAtendimento.CurrentRow == null)
                    throw new Exception("Selecione uma linha");

                this.Cursor = Cursors.WaitCursor;

                AsoFacade asoFacade = AsoFacade.getInstance();
                Aso atendimento = asoFacade.findAtendimentoById((Int64)dgvAtendimento.CurrentRow.Cells[0].Value);

                if (!String.Equals(atendimento.Situacao, ApplicationConstants.DESATIVADO))
                {
                    frmAtendimentoCheck formAsoCheckExame = new frmAtendimentoCheck(atendimento);
                    formAsoCheckExame.ShowDialog();
                    btPesquisa.PerformClick();
                    
                }
                else
                {
                    if (MessageBox.Show("Atendimento cancelado. Deseja ver os exames?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        frmAtendimentoExibirCancelado formAsoExibirCancelado = new frmAtendimentoExibirCancelado(atendimento);
                        formAsoExibirCancelado.ShowDialog();
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } 
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void dgvAtendimento_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.ClearSelection();
        }

        private void btnExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new Exception("Selecione primeiro um cliente.");

                cliente = null;
                textCliente.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
              
        }

        private void btnExcluirFuncao_Click(object sender, EventArgs e)
        {
            try
            {
                if (clienteFuncao == null)
                    throw new Exception("Selecione primeiro uma função.");

                clienteFuncao = null;
                textFuncao.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnDesbloquear_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvAtendimento.CurrentRow == null)
                    throw new Exception("Selecione uma linha");

                this.Cursor = Cursors.WaitCursor;

                if (!String.Equals(dgvAtendimento.CurrentRow.Cells[11].Value, ApplicationConstants.BLOQUEADO))
                    throw new Exception("Somente atendimentos bloqueados podem ser desbloqueados");

                AsoFacade asoFacade = AsoFacade.getInstance();
                Aso atendimento = asoFacade.findAtendimentoById((Int64)dgvAtendimento.CurrentRow.Cells[0].Value);

                atendimento.Situacao = ApplicationConstants.CONSTRUCAO;

                asoFacade.updateSituacaoAso(atendimento);

                MessageBox.Show("Atendimento desbloqueado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.btPesquisa.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
    }
}
