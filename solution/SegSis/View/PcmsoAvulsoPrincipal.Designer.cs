﻿namespace SegSis.View
{
    partial class frm_PcmsoAvulsoPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_PcmsoAvulsoPrincipal));
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btn_alterar = new System.Windows.Forms.Button();
            this.btn_incluir = new System.Windows.Forms.Button();
            this.grb_pcmso = new System.Windows.Forms.GroupBox();
            this.grd_pcmso = new System.Windows.Forms.DataGridView();
            this.grb_filtro = new System.Windows.Forms.GroupBox();
            this.grb_contrato = new System.Windows.Forms.GroupBox();
            this.text_contratada = new System.Windows.Forms.TextBox();
            this.btn_contrato = new System.Windows.Forms.Button();
            this.grb_cliente = new System.Windows.Forms.GroupBox();
            this.text_cliente = new System.Windows.Forms.TextBox();
            this.btn_cliente = new System.Windows.Forms.Button();
            this.btn_limpar = new System.Windows.Forms.Button();
            this.btn_pesquisar = new System.Windows.Forms.Button();
            this.text_codPcmso = new System.Windows.Forms.MaskedTextBox();
            this.lbl_codigo = new System.Windows.Forms.Label();
            this.panel_banner.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_banner)).BeginInit();
            this.panel.SuspendLayout();
            this.grb_paginacao.SuspendLayout();
            this.grb_pcmso.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_pcmso)).BeginInit();
            this.grb_filtro.SuspendLayout();
            this.grb_contrato.SuspendLayout();
            this.grb_cliente.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.Controls.Add(this.grb_paginacao);
            this.panel.Controls.Add(this.grb_pcmso);
            this.panel.Controls.Add(this.grb_filtro);
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.Controls.Add(this.btn_fechar);
            this.grb_paginacao.Controls.Add(this.btn_alterar);
            this.grb_paginacao.Controls.Add(this.btn_incluir);
            this.grb_paginacao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grb_paginacao.Location = new System.Drawing.Point(4, 457);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(780, 55);
            this.grb_paginacao.TabIndex = 17;
            this.grb_paginacao.TabStop = false;
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SegSis.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(170, 19);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 22;
            this.btn_fechar.Text = "Fec&har";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btn_alterar
            // 
            this.btn_alterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_alterar.Image = global::SegSis.Properties.Resources.Alterar;
            this.btn_alterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_alterar.Location = new System.Drawing.Point(89, 19);
            this.btn_alterar.Name = "btn_alterar";
            this.btn_alterar.Size = new System.Drawing.Size(75, 23);
            this.btn_alterar.TabIndex = 16;
            this.btn_alterar.Text = "&Alterar";
            this.btn_alterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_alterar.UseVisualStyleBackColor = true;
            this.btn_alterar.Click += new System.EventHandler(this.btn_alterar_Click);
            // 
            // btn_incluir
            // 
            this.btn_incluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluir.Image = global::SegSis.Properties.Resources.icone_mais;
            this.btn_incluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluir.Location = new System.Drawing.Point(8, 19);
            this.btn_incluir.Name = "btn_incluir";
            this.btn_incluir.Size = new System.Drawing.Size(75, 23);
            this.btn_incluir.TabIndex = 15;
            this.btn_incluir.Text = "&Incluir";
            this.btn_incluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluir.UseVisualStyleBackColor = true;
            this.btn_incluir.Click += new System.EventHandler(this.btn_incluir_Click);
            // 
            // grb_pcmso
            // 
            this.grb_pcmso.Controls.Add(this.grd_pcmso);
            this.grb_pcmso.Location = new System.Drawing.Point(4, 155);
            this.grb_pcmso.Name = "grb_pcmso";
            this.grb_pcmso.Size = new System.Drawing.Size(780, 296);
            this.grb_pcmso.TabIndex = 16;
            this.grb_pcmso.TabStop = false;
            this.grb_pcmso.Text = "PCMSO";
            // 
            // grd_pcmso
            // 
            this.grd_pcmso.AllowUserToAddRows = false;
            this.grd_pcmso.AllowUserToDeleteRows = false;
            this.grd_pcmso.AllowUserToOrderColumns = true;
            this.grd_pcmso.AllowUserToResizeRows = false;
            this.grd_pcmso.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_pcmso.BackgroundColor = System.Drawing.Color.White;
            this.grd_pcmso.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_pcmso.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd_pcmso.Location = new System.Drawing.Point(3, 16);
            this.grd_pcmso.MultiSelect = false;
            this.grd_pcmso.Name = "grd_pcmso";
            this.grd_pcmso.ReadOnly = true;
            this.grd_pcmso.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_pcmso.Size = new System.Drawing.Size(774, 277);
            this.grd_pcmso.TabIndex = 14;
            this.grd_pcmso.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grd_pcmso_CellClick);
            // 
            // grb_filtro
            // 
            this.grb_filtro.Controls.Add(this.grb_contrato);
            this.grb_filtro.Controls.Add(this.grb_cliente);
            this.grb_filtro.Controls.Add(this.btn_limpar);
            this.grb_filtro.Controls.Add(this.btn_pesquisar);
            this.grb_filtro.Controls.Add(this.text_codPcmso);
            this.grb_filtro.Controls.Add(this.lbl_codigo);
            this.grb_filtro.Location = new System.Drawing.Point(3, 6);
            this.grb_filtro.Name = "grb_filtro";
            this.grb_filtro.Size = new System.Drawing.Size(781, 143);
            this.grb_filtro.TabIndex = 15;
            this.grb_filtro.TabStop = false;
            // 
            // grb_contrato
            // 
            this.grb_contrato.Controls.Add(this.text_contratada);
            this.grb_contrato.Controls.Add(this.btn_contrato);
            this.grb_contrato.Location = new System.Drawing.Point(128, 56);
            this.grb_contrato.Name = "grb_contrato";
            this.grb_contrato.Size = new System.Drawing.Size(648, 40);
            this.grb_contrato.TabIndex = 15;
            this.grb_contrato.TabStop = false;
            this.grb_contrato.Text = "Cliente Contratado";
            // 
            // text_contratada
            // 
            this.text_contratada.BackColor = System.Drawing.SystemColors.Info;
            this.text_contratada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_contratada.Location = new System.Drawing.Point(6, 14);
            this.text_contratada.MaxLength = 100;
            this.text_contratada.Name = "text_contratada";
            this.text_contratada.ReadOnly = true;
            this.text_contratada.Size = new System.Drawing.Size(576, 20);
            this.text_contratada.TabIndex = 2;
            this.text_contratada.TabStop = false;
            // 
            // btn_contrato
            // 
            this.btn_contrato.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_contrato.Location = new System.Drawing.Point(588, 11);
            this.btn_contrato.Name = "btn_contrato";
            this.btn_contrato.Size = new System.Drawing.Size(52, 23);
            this.btn_contrato.TabIndex = 2;
            this.btn_contrato.Text = "&Contrato";
            this.btn_contrato.UseVisualStyleBackColor = true;
            this.btn_contrato.Click += new System.EventHandler(this.btn_contrato_Click);
            // 
            // grb_cliente
            // 
            this.grb_cliente.Controls.Add(this.text_cliente);
            this.grb_cliente.Controls.Add(this.btn_cliente);
            this.grb_cliente.Location = new System.Drawing.Point(128, 10);
            this.grb_cliente.Name = "grb_cliente";
            this.grb_cliente.Size = new System.Drawing.Size(647, 40);
            this.grb_cliente.TabIndex = 14;
            this.grb_cliente.TabStop = false;
            this.grb_cliente.Text = "Cliente";
            // 
            // text_cliente
            // 
            this.text_cliente.BackColor = System.Drawing.SystemColors.Info;
            this.text_cliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cliente.Location = new System.Drawing.Point(6, 14);
            this.text_cliente.MaxLength = 100;
            this.text_cliente.Name = "text_cliente";
            this.text_cliente.ReadOnly = true;
            this.text_cliente.Size = new System.Drawing.Size(576, 20);
            this.text_cliente.TabIndex = 2;
            this.text_cliente.TabStop = false;
            // 
            // btn_cliente
            // 
            this.btn_cliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_cliente.Location = new System.Drawing.Point(588, 11);
            this.btn_cliente.Name = "btn_cliente";
            this.btn_cliente.Size = new System.Drawing.Size(52, 23);
            this.btn_cliente.TabIndex = 2;
            this.btn_cliente.Text = "&Cliente";
            this.btn_cliente.UseVisualStyleBackColor = true;
            this.btn_cliente.Click += new System.EventHandler(this.btn_cliente_Click);
            // 
            // btn_limpar
            // 
            this.btn_limpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_limpar.Image = global::SegSis.Properties.Resources.vassoura;
            this.btn_limpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_limpar.Location = new System.Drawing.Point(204, 102);
            this.btn_limpar.Name = "btn_limpar";
            this.btn_limpar.Size = new System.Drawing.Size(64, 23);
            this.btn_limpar.TabIndex = 13;
            this.btn_limpar.Text = "&Limpar";
            this.btn_limpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_limpar.UseVisualStyleBackColor = true;
            this.btn_limpar.Click += new System.EventHandler(this.btn_limpar_Click);
            // 
            // btn_pesquisar
            // 
            this.btn_pesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_pesquisar.Image = global::SegSis.Properties.Resources.lupa;
            this.btn_pesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_pesquisar.Location = new System.Drawing.Point(134, 102);
            this.btn_pesquisar.Name = "btn_pesquisar";
            this.btn_pesquisar.Size = new System.Drawing.Size(64, 23);
            this.btn_pesquisar.TabIndex = 12;
            this.btn_pesquisar.Text = "&Pesquisar";
            this.btn_pesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_pesquisar.UseVisualStyleBackColor = true;
            this.btn_pesquisar.Click += new System.EventHandler(this.btn_pesquisar_Click);
            // 
            // text_codPcmso
            // 
            this.text_codPcmso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_codPcmso.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_codPcmso.ForeColor = System.Drawing.Color.Blue;
            this.text_codPcmso.Location = new System.Drawing.Point(15, 24);
            this.text_codPcmso.Mask = "0000,00,000000/00";
            this.text_codPcmso.Name = "text_codPcmso";
            this.text_codPcmso.PromptChar = ' ';
            this.text_codPcmso.Size = new System.Drawing.Size(107, 20);
            this.text_codPcmso.TabIndex = 1;
            this.text_codPcmso.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_codigo
            // 
            this.lbl_codigo.AutoSize = true;
            this.lbl_codigo.Location = new System.Drawing.Point(12, 10);
            this.lbl_codigo.Name = "lbl_codigo";
            this.lbl_codigo.Size = new System.Drawing.Size(96, 13);
            this.lbl_codigo.TabIndex = 0;
            this.lbl_codigo.Text = "Código do PCMSO";
            // 
            // frm_PcmsoAvulsoPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 568);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_PcmsoAvulsoPrincipal";
            this.Text = "PESQUISAR PCMSO AVULSO";
            this.panel_banner.ResumeLayout(false);
            this.panel_banner.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_banner)).EndInit();
            this.panel.ResumeLayout(false);
            this.grb_paginacao.ResumeLayout(false);
            this.grb_pcmso.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_pcmso)).EndInit();
            this.grb_filtro.ResumeLayout(false);
            this.grb_filtro.PerformLayout();
            this.grb_contrato.ResumeLayout(false);
            this.grb_contrato.PerformLayout();
            this.grb_cliente.ResumeLayout(false);
            this.grb_cliente.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.Button btn_alterar;
        private System.Windows.Forms.Button btn_incluir;
        private System.Windows.Forms.GroupBox grb_pcmso;
        private System.Windows.Forms.DataGridView grd_pcmso;
        private System.Windows.Forms.GroupBox grb_filtro;
        private System.Windows.Forms.GroupBox grb_cliente;
        public System.Windows.Forms.TextBox text_cliente;
        private System.Windows.Forms.Button btn_cliente;
        private System.Windows.Forms.Button btn_limpar;
        private System.Windows.Forms.Button btn_pesquisar;
        private System.Windows.Forms.MaskedTextBox text_codPcmso;
        private System.Windows.Forms.Label lbl_codigo;
        private System.Windows.Forms.GroupBox grb_contrato;
        public System.Windows.Forms.TextBox text_contratada;
        private System.Windows.Forms.Button btn_contrato;
    }
}