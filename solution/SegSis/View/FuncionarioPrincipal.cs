﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.Resources;
using SWS.View.ViewHelper;
using SWS.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmFuncionarioPrincipal : frmTemplate
    {
        private Funcionario funcionario;
        private ClienteFuncao clienteFuncao;
        private Cliente cliente;
        private int numFiltro = 0;
        
        public frmFuncionarioPrincipal()
        {
            InitializeComponent();
            validaPermissoes();
            ComboHelper.comboSituacao(cbSituacao);
            ActiveControl = textNome;
        }

        private void btn_incluir_Click(object sender, EventArgs e)
        {
            try
            {
                frmFuncionarioIncluir incluirFuncionario = new frmFuncionarioIncluir();
                incluirFuncionario.ShowDialog();

                if (incluirFuncionario.Funcionario != null)
                {
                    funcionario = new Funcionario();
                    btnLimpar.PerformClick();
                    MontaDataGrid();
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_alterar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (dgvFuncionario.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                FuncionarioFacade funcionarioFacade = FuncionarioFacade.getInstance();

                Funcionario funcionarioAlterar = funcionarioFacade.findFuncionarioById((Int64)dgvFuncionario.CurrentRow.Cells[0].Value);

                if (!String.Equals(funcionarioAlterar.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente funcionários ativos podem ser alterados.");

                frmFuncionarioAlterar formFuncionarioAlterar = new frmFuncionarioAlterar(funcionarioAlterar);
                formFuncionarioAlterar.ShowDialog();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_pesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (!string.IsNullOrEmpty(textNome.Text.Trim()))
                    numFiltro++;

                if (!string.IsNullOrEmpty(textCpf.Text.Replace(".", "").Replace("-", "").Trim()))
                    numFiltro++;

                if (!string.IsNullOrEmpty(textRg.Text.Trim()))
                    numFiltro++;

                textNome.Focus();

                if (numFiltro == 0)
                {
                    if (MessageBox.Show("Você não selecionou nenhum filtro. Sua pesquisa poderá demorar um pouco. Deseja continuar?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        funcionario = new Funcionario(null, textNome.Text, textCpf.Text, textRg.Text, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, DateTime.Now, null, null, ((SelectItem)cbSituacao.SelectedItem).Valor, string.Empty, string.Empty, string.Empty, string.Empty, false, string.Empty, string.Empty, string.Empty, string.Empty);
                        textNome.Focus();
                        MontaDataGrid();
                    }
                }
                else
                {
                    funcionario = new Funcionario(null, textNome.Text, textCpf.Text, textRg.Text, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, DateTime.Now, null, null, ((SelectItem)cbSituacao.SelectedItem).Valor, string.Empty, string.Empty, string.Empty, string.Empty, false, string.Empty, string.Empty, string.Empty, string.Empty);
                    textNome.Focus();
                    MontaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_detalhar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvFuncionario.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");

                FuncionarioFacade funcionarioFacade = FuncionarioFacade.getInstance();
                
                Funcionario funcionarioDetalhar = funcionarioFacade.findFuncionarioById((long)dgvFuncionario.CurrentRow.Cells[0].Value);

                frmFuncionarioDetalhar formFuncionarioDetalhar = new frmFuncionarioDetalhar(funcionarioDetalhar);
                formFuncionarioDetalhar.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_limpar_Click(object sender, EventArgs e)
        {
            textNome.Text = string.Empty;
            textCpf.Text = string.Empty;
            textRg.Text = string.Empty;
            dgvFuncionario.Columns.Clear();
            numFiltro = 0;
            ComboHelper.comboSituacao(cbSituacao);
        }

        private void btn_reativar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvFuncionario.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");
                 
                FuncionarioFacade funcionarioFacade = FuncionarioFacade.getInstance();
                
                Funcionario funcionarioReativar = funcionarioFacade.findFuncionarioById((long)dgvFuncionario.CurrentRow.Cells[0].Value);
 
                if (!String.Equals(funcionarioReativar.Situacao, ApplicationConstants.DESATIVADO))
                    throw new Exception("Somente funcionários desativados podem ser reativados.");

                if (MessageBox.Show("Deseja reativar o funcionário selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    funcionarioReativar.Situacao = ApplicationConstants.ATIVO;
                    funcionarioFacade.updateFuncionario(funcionarioReativar);
                    
                    MessageBox.Show("Funcionário reativado com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    funcionario = new Funcionario();
                    btnLimpar.PerformClick();
                    MontaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_excluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvFuncionario.CurrentRow == null)
                    throw new Exception("Selecione uma linha.");
                    
                FuncionarioFacade funcionarioFacade = FuncionarioFacade.getInstance();

                Funcionario funcionarioExcluir = funcionarioFacade.findFuncionarioById((Int64)dgvFuncionario.CurrentRow.Cells[0].Value);

                if (!String.Equals(funcionarioExcluir.Situacao, ApplicationConstants.ATIVO))
                    throw new Exception("Somente funcionários ativos podem ser excluídos.");
                    
                if (MessageBox.Show("Deseja excluir o funcionário selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    funcionarioExcluir.Situacao = ApplicationConstants.DESATIVADO;
                    funcionarioFacade.excluiFuncionario(funcionarioExcluir);

                    MessageBox.Show("Funcionário excluído com sucesso.", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    funcionario = new Funcionario();
                    btnLimpar.PerformClick();
                    MontaDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void validaPermissoes()
        {
            PermissionamentoFacade permissionamentoFacade = PermissionamentoFacade.getInstance();

            btnAlterar.Enabled = permissionamentoFacade.hasPermission("FUNCIONARIO", "ALTERAR");
            btnIncluir.Enabled = permissionamentoFacade.hasPermission("FUNCIONARIO", "INCLUIR");
            btnExcluir.Enabled = permissionamentoFacade.hasPermission("FUNCIONARIO", "EXCLUIR");
            btnDetalhar.Enabled = permissionamentoFacade.hasPermission("FUNCIONARIO", "DETALHAR");
            btnReativar.Enabled = permissionamentoFacade.hasPermission("FUNCIONARIO", "REATIVAR");
        }

        private void btn_cliente_Click(object sender, EventArgs e)
        {
            try
            {
                frmClienteSelecionar formCliente = new frmClienteSelecionar(null, null, false, null, null, null, null, null, null);
                formCliente.ShowDialog();

                if (formCliente.Cliente != null)
                {
                    cliente = formCliente.Cliente;
                    textCliente.Text = cliente.RazaoSocial.ToUpper();
                    numFiltro++;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnFuncao_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (cliente == null)
                    throw new Exception("Selecione primeiro um cliente.");

                frmClienteFuncaoSeleciona formFuncaoPesquisarFuncionario = new frmClienteFuncaoSeleciona(cliente);
                formFuncaoPesquisarFuncionario.ShowDialog();
                    
                if (formFuncaoPesquisarFuncionario.ClienteFuncao != null)
                {
                    clienteFuncao = formFuncaoPesquisarFuncionario.ClienteFuncao;
                    textFuncao.Text = clienteFuncao.Funcao.Descricao;
                    numFiltro++;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void MontaDataGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                dgvFuncionario.Columns.Clear();

                FuncionarioFacade funcionarioFacade = FuncionarioFacade.getInstance();

                DataSet ds = funcionarioFacade.findFuncionarioByFilter(funcionario, clienteFuncao, cliente);

                dgvFuncionario.DataSource = ds.Tables["Funcionarios"].DefaultView;

                dgvFuncionario.Columns[0].HeaderText = "id_funcionario";
                dgvFuncionario.Columns[0].Visible = false;

                dgvFuncionario.Columns[1].HeaderText = "Nome";
                dgvFuncionario.Columns[2].HeaderText = "CPF";
                dgvFuncionario.Columns[3].HeaderText = "RG";
                dgvFuncionario.Columns[4].HeaderText = "Endereço";
                dgvFuncionario.Columns[5].HeaderText = "Número";
                dgvFuncionario.Columns[6].HeaderText = "Complemento";
                dgvFuncionario.Columns[7].HeaderText = "Bairro";
                dgvFuncionario.Columns[8].HeaderText = "Cidade";
                dgvFuncionario.Columns[9].HeaderText = "UF";
                dgvFuncionario.Columns[10].HeaderText = "Telefone";
                dgvFuncionario.Columns[11].HeaderText = "Celular";
                dgvFuncionario.Columns[12].HeaderText = "E-mail";

                dgvFuncionario.Columns[13].HeaderText = "Tipo Sanguíneo";
                dgvFuncionario.Columns[13].Visible = false;
                
                dgvFuncionario.Columns[14].HeaderText = "Fator RH";
                dgvFuncionario.Columns[14].Visible = false;
                
                dgvFuncionario.Columns[15].HeaderText = "Data Nascimento";
                dgvFuncionario.Columns[16].HeaderText = "Peso";
                dgvFuncionario.Columns[16].Visible = false;

                dgvFuncionario.Columns[17].HeaderText = "Altura";
                dgvFuncionario.Columns[17].Visible = false;
                
                dgvFuncionario.Columns[18].HeaderText = "Situação";
                dgvFuncionario.Columns[18].Visible = false;
                
                dgvFuncionario.Columns[19].HeaderText = "CEP";
                dgvFuncionario.Columns[19].Visible = false;
                
                dgvFuncionario.Columns[20].HeaderText = "Sexo";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void dgvFuncionario_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (String.Equals(dgv.Rows[e.RowIndex].Cells[18].Value.ToString(), ApplicationConstants.DESATIVADO))
                dgv.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
        }

        private void cbSituacao_SelectedValueChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(((SelectItem)cbSituacao.SelectedItem).Valor))
                numFiltro++;
        }

        private void btnExcluirCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (cliente == null)
                    throw new Exception("Você não selecionou o cliente ainda.");

                cliente = null;
                textCliente.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExcluirFuncao_Click(object sender, EventArgs e)
        {
            try
            {
                if (clienteFuncao == null)
                    throw new Exception("Você não selecionou a função ainda.");

                clienteFuncao = null;
                textFuncao.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvFuncionario_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btnAlterar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnImportar_Click(object sender, EventArgs e)
        {
            try
            {

                throw new NotImplementedException("Ainda não disponível");

                /* iniciando a leitura do arquivo */

                OpenFileDialog openFile = new OpenFileDialog();
                openFile.Multiselect = false;
                openFile.Title = "Selecione o arquivo csv para importação";
                openFile.Filter = "File (*.CSV) |*.CSV";
                openFile.CheckFileExists = true;
                openFile.ReadOnlyChecked = true;
                
                if (openFile.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        var filePath = openFile.FileName;
                        using (Stream str = openFile.OpenFile())
                        {
                            
                        }

                    }
                    catch (SecurityException ex)
                    {
                        MessageBox.Show("Erro de segurança Contate o administrador de segurança da rede.\n\n" +
                                               "Mensagem : " + ex.Message + "\n\n" +
                                               "Detalhes (enviar ao suporte):\n\n" + ex.StackTrace);

                    }
                        
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
