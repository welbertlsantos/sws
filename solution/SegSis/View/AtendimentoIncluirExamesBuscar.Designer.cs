﻿namespace SWS.View
{
    partial class frmAtendimentoIncluirExamesBuscar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_incluir = new System.Windows.Forms.Button();
            this.btn_novo = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.btn_pesquisar = new System.Windows.Forms.Button();
            this.text_descricao = new System.Windows.Forms.TextBox();
            this.lblExame = new System.Windows.Forms.TextBox();
            this.grb_exame = new System.Windows.Forms.GroupBox();
            this.dvg_exame = new System.Windows.Forms.DataGridView();
            this.chk_marcaDesmarcaExame = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).BeginInit();
            this.scForm.Panel1.SuspendLayout();
            this.scForm.Panel2.SuspendLayout();
            this.scForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grb_exame.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvg_exame)).BeginInit();
            this.SuspendLayout();
            // 
            // scForm
            // 
            // 
            // scForm.Panel1
            // 
            this.scForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.chk_marcaDesmarcaExame);
            this.pnlForm.Controls.Add(this.grb_exame);
            this.pnlForm.Controls.Add(this.lblExame);
            this.pnlForm.Controls.Add(this.text_descricao);
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btn_incluir);
            this.flpAcao.Controls.Add(this.btn_novo);
            this.flpAcao.Controls.Add(this.btn_pesquisar);
            this.flpAcao.Controls.Add(this.btn_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(534, 30);
            this.flpAcao.TabIndex = 0;
            // 
            // btn_incluir
            // 
            this.btn_incluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluir.Image = global::SWS.Properties.Resources.Gravar;
            this.btn_incluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluir.Location = new System.Drawing.Point(3, 3);
            this.btn_incluir.Name = "btn_incluir";
            this.btn_incluir.Size = new System.Drawing.Size(75, 23);
            this.btn_incluir.TabIndex = 7;
            this.btn_incluir.Text = "&Gravar";
            this.btn_incluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluir.UseVisualStyleBackColor = true;
            this.btn_incluir.Click += new System.EventHandler(this.btn_incluir_Click);
            // 
            // btn_novo
            // 
            this.btn_novo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_novo.Image = global::SWS.Properties.Resources.icone_mais;
            this.btn_novo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_novo.Location = new System.Drawing.Point(84, 3);
            this.btn_novo.Name = "btn_novo";
            this.btn_novo.Size = new System.Drawing.Size(75, 23);
            this.btn_novo.TabIndex = 8;
            this.btn_novo.Text = "&Novo";
            this.btn_novo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_novo.UseVisualStyleBackColor = true;
            this.btn_novo.Click += new System.EventHandler(this.btn_novo_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(246, 3);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 9;
            this.btn_fechar.Text = "Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // btn_pesquisar
            // 
            this.btn_pesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_pesquisar.Image = global::SWS.Properties.Resources.busca;
            this.btn_pesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_pesquisar.Location = new System.Drawing.Point(165, 3);
            this.btn_pesquisar.Name = "btn_pesquisar";
            this.btn_pesquisar.Size = new System.Drawing.Size(75, 23);
            this.btn_pesquisar.TabIndex = 2;
            this.btn_pesquisar.Text = "Buscar";
            this.btn_pesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_pesquisar.UseVisualStyleBackColor = true;
            this.btn_pesquisar.Click += new System.EventHandler(this.btn_pesquisar_Click);
            // 
            // text_descricao
            // 
            this.text_descricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_descricao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.text_descricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_descricao.Location = new System.Drawing.Point(91, 15);
            this.text_descricao.MaxLength = 100;
            this.text_descricao.Name = "text_descricao";
            this.text_descricao.Size = new System.Drawing.Size(413, 21);
            this.text_descricao.TabIndex = 1;
            // 
            // lblExame
            // 
            this.lblExame.BackColor = System.Drawing.Color.LightGray;
            this.lblExame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblExame.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExame.Location = new System.Drawing.Point(12, 15);
            this.lblExame.MaxLength = 100;
            this.lblExame.Name = "lblExame";
            this.lblExame.ReadOnly = true;
            this.lblExame.Size = new System.Drawing.Size(80, 21);
            this.lblExame.TabIndex = 5;
            this.lblExame.TabStop = false;
            this.lblExame.Text = "Exame";
            // 
            // grb_exame
            // 
            this.grb_exame.Controls.Add(this.dvg_exame);
            this.grb_exame.Location = new System.Drawing.Point(12, 65);
            this.grb_exame.Name = "grb_exame";
            this.grb_exame.Size = new System.Drawing.Size(492, 211);
            this.grb_exame.TabIndex = 3;
            this.grb_exame.TabStop = false;
            this.grb_exame.Text = "Exames";
            // 
            // dvg_exame
            // 
            this.dvg_exame.AllowUserToAddRows = false;
            this.dvg_exame.AllowUserToDeleteRows = false;
            this.dvg_exame.AllowUserToOrderColumns = true;
            this.dvg_exame.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dvg_exame.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dvg_exame.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dvg_exame.BackgroundColor = System.Drawing.Color.White;
            this.dvg_exame.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dvg_exame.DefaultCellStyle = dataGridViewCellStyle2;
            this.dvg_exame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvg_exame.Location = new System.Drawing.Point(3, 16);
            this.dvg_exame.MultiSelect = false;
            this.dvg_exame.Name = "dvg_exame";
            this.dvg_exame.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dvg_exame.Size = new System.Drawing.Size(486, 192);
            this.dvg_exame.TabIndex = 3;
            // 
            // chk_marcaDesmarcaExame
            // 
            this.chk_marcaDesmarcaExame.AutoSize = true;
            this.chk_marcaDesmarcaExame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chk_marcaDesmarcaExame.Location = new System.Drawing.Point(12, 42);
            this.chk_marcaDesmarcaExame.Name = "chk_marcaDesmarcaExame";
            this.chk_marcaDesmarcaExame.Size = new System.Drawing.Size(135, 17);
            this.chk_marcaDesmarcaExame.TabIndex = 2;
            this.chk_marcaDesmarcaExame.Text = "Marca/Desmarca todos";
            this.chk_marcaDesmarcaExame.UseVisualStyleBackColor = true;
            this.chk_marcaDesmarcaExame.CheckedChanged += new System.EventHandler(this.chk_marcaDesmarcaExame_CheckedChanged);
            // 
            // frmAtendimentoIncluirExamesBuscar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Name = "frmAtendimentoIncluirExamesBuscar";
            this.Text = "INCLUIR EXAMES";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmAtendimentoIncluirExamesBuscar_KeyDown);
            this.scForm.Panel1.ResumeLayout(false);
            this.scForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scForm)).EndInit();
            this.scForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grb_exame.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dvg_exame)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btn_incluir;
        private System.Windows.Forms.Button btn_novo;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.TextBox lblExame;
        private System.Windows.Forms.Button btn_pesquisar;
        private System.Windows.Forms.TextBox text_descricao;
        private System.Windows.Forms.CheckBox chk_marcaDesmarcaExame;
        private System.Windows.Forms.GroupBox grb_exame;
        private System.Windows.Forms.DataGridView dvg_exame;
    }
}