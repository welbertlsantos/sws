﻿namespace SWS.View
{
    partial class frm_ppraAgenteIncluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_ppraAgenteIncluir));
            this.grb_gradacao = new System.Windows.Forms.GroupBox();
            this.grb_tempo = new System.Windows.Forms.GroupBox();
            this.cb_tempoExposicao = new System.Windows.Forms.ComboBox();
            this.grb_medidaControle = new System.Windows.Forms.GroupBox();
            this.text_soma = new System.Windows.Forms.TextBox();
            this.grd_soma = new System.Windows.Forms.DataGridView();
            this.grb_efeito = new System.Windows.Forms.GroupBox();
            this.cb_efeito = new System.Windows.Forms.ComboBox();
            this.grb_exposicao = new System.Windows.Forms.GroupBox();
            this.cb_exposicao = new System.Windows.Forms.ComboBox();
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.btn_limpar = new System.Windows.Forms.Button();
            this.btn_incluir = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.grb_agente = new System.Windows.Forms.GroupBox();
            this.btn_agente = new System.Windows.Forms.Button();
            this.text_agente = new System.Windows.Forms.TextBox();
            this.grb_gradacao.SuspendLayout();
            this.grb_tempo.SuspendLayout();
            this.grb_medidaControle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_soma)).BeginInit();
            this.grb_efeito.SuspendLayout();
            this.grb_exposicao.SuspendLayout();
            this.grb_paginacao.SuspendLayout();
            this.grb_agente.SuspendLayout();
            this.SuspendLayout();
            // 
            // grb_gradacao
            // 
            this.grb_gradacao.Controls.Add(this.grb_tempo);
            this.grb_gradacao.Controls.Add(this.grb_medidaControle);
            this.grb_gradacao.Controls.Add(this.grb_efeito);
            this.grb_gradacao.Controls.Add(this.grb_exposicao);
            this.grb_gradacao.Location = new System.Drawing.Point(5, 55);
            this.grb_gradacao.Name = "grb_gradacao";
            this.grb_gradacao.Size = new System.Drawing.Size(590, 282);
            this.grb_gradacao.TabIndex = 5;
            this.grb_gradacao.TabStop = false;
            this.grb_gradacao.Text = "Gradações";
            // 
            // grb_tempo
            // 
            this.grb_tempo.Controls.Add(this.cb_tempoExposicao);
            this.grb_tempo.Enabled = false;
            this.grb_tempo.Location = new System.Drawing.Point(371, 15);
            this.grb_tempo.Name = "grb_tempo";
            this.grb_tempo.Size = new System.Drawing.Size(191, 55);
            this.grb_tempo.TabIndex = 8;
            this.grb_tempo.TabStop = false;
            this.grb_tempo.Text = "Tempo de Exposição";
            // 
            // cb_tempoExposicao
            // 
            this.cb_tempoExposicao.BackColor = System.Drawing.Color.LightGray;
            this.cb_tempoExposicao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_tempoExposicao.FormattingEnabled = true;
            this.cb_tempoExposicao.Location = new System.Drawing.Point(6, 19);
            this.cb_tempoExposicao.Name = "cb_tempoExposicao";
            this.cb_tempoExposicao.Size = new System.Drawing.Size(179, 21);
            this.cb_tempoExposicao.TabIndex = 0;
            // 
            // grb_medidaControle
            // 
            this.grb_medidaControle.Controls.Add(this.text_soma);
            this.grb_medidaControle.Controls.Add(this.grd_soma);
            this.grb_medidaControle.Location = new System.Drawing.Point(6, 75);
            this.grb_medidaControle.Name = "grb_medidaControle";
            this.grb_medidaControle.Size = new System.Drawing.Size(578, 201);
            this.grb_medidaControle.TabIndex = 7;
            this.grb_medidaControle.TabStop = false;
            this.grb_medidaControle.Text = "Medida de Controle";
            // 
            // text_soma
            // 
            this.text_soma.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_soma.ForeColor = System.Drawing.Color.Red;
            this.text_soma.Location = new System.Drawing.Point(6, 18);
            this.text_soma.MaxLength = 2;
            this.text_soma.Multiline = true;
            this.text_soma.Name = "text_soma";
            this.text_soma.ReadOnly = true;
            this.text_soma.Size = new System.Drawing.Size(51, 29);
            this.text_soma.TabIndex = 0;
            this.text_soma.TabStop = false;
            this.text_soma.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // grd_soma
            // 
            this.grd_soma.AllowUserToAddRows = false;
            this.grd_soma.AllowUserToDeleteRows = false;
            this.grd_soma.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_soma.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.grd_soma.BackgroundColor = System.Drawing.Color.White;
            this.grd_soma.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.grd_soma.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_soma.Location = new System.Drawing.Point(63, 18);
            this.grd_soma.Name = "grd_soma";
            this.grd_soma.ReadOnly = true;
            this.grd_soma.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_soma.Size = new System.Drawing.Size(509, 177);
            this.grd_soma.TabIndex = 4;
            // 
            // grb_efeito
            // 
            this.grb_efeito.Controls.Add(this.cb_efeito);
            this.grb_efeito.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grb_efeito.Location = new System.Drawing.Point(190, 15);
            this.grb_efeito.Name = "grb_efeito";
            this.grb_efeito.Size = new System.Drawing.Size(175, 54);
            this.grb_efeito.TabIndex = 5;
            this.grb_efeito.TabStop = false;
            this.grb_efeito.Text = "Efeito";
            // 
            // cb_efeito
            // 
            this.cb_efeito.BackColor = System.Drawing.Color.LightGray;
            this.cb_efeito.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_efeito.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_efeito.FormattingEnabled = true;
            this.cb_efeito.Location = new System.Drawing.Point(6, 19);
            this.cb_efeito.Name = "cb_efeito";
            this.cb_efeito.Size = new System.Drawing.Size(162, 21);
            this.cb_efeito.TabIndex = 3;
            this.cb_efeito.SelectedIndexChanged += new System.EventHandler(this.cb_efeito_SelectedIndexChanged);
            // 
            // grb_exposicao
            // 
            this.grb_exposicao.Controls.Add(this.cb_exposicao);
            this.grb_exposicao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grb_exposicao.Location = new System.Drawing.Point(6, 15);
            this.grb_exposicao.Name = "grb_exposicao";
            this.grb_exposicao.Size = new System.Drawing.Size(177, 54);
            this.grb_exposicao.TabIndex = 4;
            this.grb_exposicao.TabStop = false;
            this.grb_exposicao.Text = "Exposição";
            // 
            // cb_exposicao
            // 
            this.cb_exposicao.BackColor = System.Drawing.Color.LightGray;
            this.cb_exposicao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_exposicao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_exposicao.FormattingEnabled = true;
            this.cb_exposicao.Location = new System.Drawing.Point(6, 19);
            this.cb_exposicao.Name = "cb_exposicao";
            this.cb_exposicao.Size = new System.Drawing.Size(163, 21);
            this.cb_exposicao.TabIndex = 2;
            this.cb_exposicao.SelectedIndexChanged += new System.EventHandler(this.cb_exposicao_SelectedIndexChanged);
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.Controls.Add(this.btn_limpar);
            this.grb_paginacao.Controls.Add(this.btn_incluir);
            this.grb_paginacao.Controls.Add(this.btn_fechar);
            this.grb_paginacao.Location = new System.Drawing.Point(5, 343);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(590, 55);
            this.grb_paginacao.TabIndex = 6;
            this.grb_paginacao.TabStop = false;
            // 
            // btn_limpar
            // 
            this.btn_limpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_limpar.Image = global::SWS.Properties.Resources.vassoura;
            this.btn_limpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_limpar.Location = new System.Drawing.Point(87, 22);
            this.btn_limpar.Name = "btn_limpar";
            this.btn_limpar.Size = new System.Drawing.Size(75, 23);
            this.btn_limpar.TabIndex = 6;
            this.btn_limpar.Text = "&Limpar";
            this.btn_limpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_limpar.UseVisualStyleBackColor = true;
            this.btn_limpar.Click += new System.EventHandler(this.btn_limpar_Click);
            // 
            // btn_incluir
            // 
            this.btn_incluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluir.Image = global::SWS.Properties.Resources.Gravar;
            this.btn_incluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluir.Location = new System.Drawing.Point(6, 22);
            this.btn_incluir.Name = "btn_incluir";
            this.btn_incluir.Size = new System.Drawing.Size(75, 23);
            this.btn_incluir.TabIndex = 5;
            this.btn_incluir.Text = "&Gravar";
            this.btn_incluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluir.UseVisualStyleBackColor = true;
            this.btn_incluir.Click += new System.EventHandler(this.btn_incluir_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(168, 22);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 7;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // grb_agente
            // 
            this.grb_agente.Controls.Add(this.btn_agente);
            this.grb_agente.Controls.Add(this.text_agente);
            this.grb_agente.Location = new System.Drawing.Point(5, 1);
            this.grb_agente.Name = "grb_agente";
            this.grb_agente.Size = new System.Drawing.Size(590, 48);
            this.grb_agente.TabIndex = 0;
            this.grb_agente.TabStop = false;
            this.grb_agente.Text = "Agente";
            // 
            // btn_agente
            // 
            this.btn_agente.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_agente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_agente.Image = global::SWS.Properties.Resources.busca;
            this.btn_agente.Location = new System.Drawing.Point(553, 15);
            this.btn_agente.Name = "btn_agente";
            this.btn_agente.Size = new System.Drawing.Size(30, 20);
            this.btn_agente.TabIndex = 1;
            this.btn_agente.UseVisualStyleBackColor = true;
            this.btn_agente.Click += new System.EventHandler(this.btn_agente_Click);
            // 
            // text_agente
            // 
            this.text_agente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_agente.Location = new System.Drawing.Point(7, 15);
            this.text_agente.MaxLength = 100;
            this.text_agente.Name = "text_agente";
            this.text_agente.ReadOnly = true;
            this.text_agente.Size = new System.Drawing.Size(549, 20);
            this.text_agente.TabIndex = 0;
            this.text_agente.TabStop = false;
            // 
            // frm_ppraAgenteIncluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 400);
            this.ControlBox = false;
            this.Controls.Add(this.grb_gradacao);
            this.Controls.Add(this.grb_paginacao);
            this.Controls.Add(this.grb_agente);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_ppraAgenteIncluir";
            this.Text = "";
            this.grb_gradacao.ResumeLayout(false);
            this.grb_tempo.ResumeLayout(false);
            this.grb_medidaControle.ResumeLayout(false);
            this.grb_medidaControle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_soma)).EndInit();
            this.grb_efeito.ResumeLayout(false);
            this.grb_exposicao.ResumeLayout(false);
            this.grb_paginacao.ResumeLayout(false);
            this.grb_agente.ResumeLayout(false);
            this.grb_agente.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grb_agente;
        public System.Windows.Forms.TextBox text_agente;
        private System.Windows.Forms.Button btn_agente;
        private System.Windows.Forms.GroupBox grb_efeito;
        public System.Windows.Forms.ComboBox cb_efeito;
        private System.Windows.Forms.GroupBox grb_gradacao;
        private System.Windows.Forms.GroupBox grb_exposicao;
        public System.Windows.Forms.ComboBox cb_exposicao;
        private System.Windows.Forms.TextBox text_soma;
        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.Button btn_limpar;
        private System.Windows.Forms.Button btn_incluir;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.GroupBox grb_medidaControle;
        private System.Windows.Forms.DataGridView grd_soma;
        public System.Windows.Forms.GroupBox grb_tempo;
        public System.Windows.Forms.ComboBox cb_tempoExposicao;
    }
}