﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SegSis.Facade;
using SegSis.Entidade;
using SegSis.Excecao;
using SegSis.View.Resources;

namespace SegSis.View
{
    public partial class frm_FuncionarioClienteAtivoIncluir : BaseFormConsulta
    {
        private static String Msg01 = " Selecione um linha";
        private static string Msg02 = " Atenção";

        private List<ClienteFuncaoFuncionario> clienteFuncaoFuncionarioSet = null;
               
        Cliente cliente = null;
        ClienteFuncao clienteFuncao = null;

        public frm_FuncionarioClienteAtivoIncluir(List<ClienteFuncaoFuncionario> clienteFuncaoFuncionarioSet)
        {
            InitializeComponent();
            this.clienteFuncaoFuncionarioSet = clienteFuncaoFuncionarioSet;
        }

        public void setClienteFuncao(ClienteFuncao clienteFuncao)
        {
            this.clienteFuncao = clienteFuncao;
        }

        public List<ClienteFuncaoFuncionario> getClienteFuncaoFuncionarios()
        {
            return this.clienteFuncaoFuncionarioSet;
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.grd_cliente.Columns.Clear();
                montaDataGrid();
                text_razaoSocial.Focus();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (grd_cliente.CurrentRow != null)
                {
                    
                    ClienteFacade clienteFacade = ClienteFacade.getInstance();

                    Int64 id = (Int64)grd_cliente.CurrentRow.Cells[0].Value;

                    cliente = clienteFacade.findClienteById(id);

                    frm_FuncionarioIncluirClienteFuncao formFuncionarioClienteFuncaoIncluir = new frm_FuncionarioIncluirClienteFuncao(cliente);
                    formFuncionarioClienteFuncaoIncluir.ShowDialog();

                    if (formFuncionarioClienteFuncaoIncluir.getClienteFuncaoFuncionario() != null)
                    {
                        clienteFuncaoFuncionarioSet.Add(formFuncionarioClienteFuncaoIncluir.getClienteFuncaoFuncionario());
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show(Msg01, Msg02);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void montaDataGrid()
        {
            ClienteFacade clienteFacade = ClienteFacade.getInstance();

            cliente = new Cliente();

            cliente.setRazaoSocial(text_razaoSocial.Text.Trim().ToUpper());
            cliente.setFantasia(text_fantasia.Text.Trim().ToUpper());
            cliente.setSituacao(ApplicationConstants.ATIVO);
            
            DataSet ds = clienteFacade.findClienteAtivoByFilterNotInClienteFuncaoFuncionarioSet(cliente, clienteFuncaoFuncionarioSet);

            grd_cliente.DataSource = ds.Tables["Clientes"].DefaultView;
            
            grd_cliente.Columns[0].HeaderText = "ID";
            grd_cliente.Columns[0].Visible = false;

            grd_cliente.Columns[1].HeaderText = "Razão Social";
        }

        private void btn_novo_Click(object sender, EventArgs e)
        {
            frm_cliente_inclui formClienteIncluir = new frm_cliente_inclui();
            formClienteIncluir.ShowDialog();

            if (formClienteIncluir.getCliente() != null)
            {
                cliente = formClienteIncluir.getCliente();
                
                frm_FuncionarioIncluirClienteFuncao formFuncionarioClienteFuncao = new frm_FuncionarioIncluirClienteFuncao(cliente);
                formFuncionarioClienteFuncao.ShowDialog();

                if (formFuncionarioClienteFuncao.getClienteFuncaoFuncionario() != null)
                {
                    clienteFuncaoFuncionarioSet.Add(formFuncionarioClienteFuncao.getClienteFuncaoFuncionario());
                    text_fantasia.Text = cliente.getFantasia();
                    text_razaoSocial.Text = cliente.getRazaoSocial();
                    montaDataGrid();
                    btn_ok.PerformClick();
                    
                }

            }
        }

    }
}
