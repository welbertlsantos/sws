﻿using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmFuncaoDetalhar : frmFuncaoIncluir
    {
        public frmFuncaoDetalhar(Funcao funcao) :base()
        {
            InitializeComponent();
            textDescricao.Text = funcao.Descricao;
            textCodioCbo.Text = funcao.CodCbo;
            Comentarios = funcao.Atividades;
            montaDataGrid();
            btGravar.Enabled = false;
            btIncluirComentario.Enabled = false;
            btExcluirAtividade.Enabled = false;
            dgvComentario.Enabled = false;
        }
    }
}
