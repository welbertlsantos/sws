﻿using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmPcmsoIncluirTipo : Form
    {
        private Estudo pcmso;

        public Estudo Pcmso
        {
            get { return pcmso; }
            set { pcmso = value; }
        }
        
        public frmPcmsoIncluirTipo()
        {
            InitializeComponent();
        }

        private void btPcmsoAvulso_Click(object sender, EventArgs e)
        {
            frmPcmsoAvulsoIncluir incluirPcmsoAvulso = new frmPcmsoAvulsoIncluir();
            incluirPcmsoAvulso.ShowDialog();

            if (incluirPcmsoAvulso.Estudo != null)
            {
                this.Pcmso = incluirPcmsoAvulso.Estudo;
                this.Close();
            }
        }

        private void btIncluirPcmso_Click(object sender, EventArgs e)
        {
            frmPcmsoIncluir incluirPcmso = new frmPcmsoIncluir();
            incluirPcmso.ShowDialog();

            if (incluirPcmso.Pcmso != null)
            {
                this.Pcmso = incluirPcmso.Pcmso;
                this.Close();
            }
                
        }
    }
}
