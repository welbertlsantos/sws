﻿using SWS.Entidade;
using SWS.Facade;
using SWS.View.ViewHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmContratosSelecionar : frmTemplateConsulta
    {
        private Contrato contrato;

        public Contrato Contrato
        {
            get { return contrato; }
            set { contrato = value; }
        }

        private List<Contrato> contratos;
        
        public frmContratosSelecionar(List<Contrato> contratos)
        {
            InitializeComponent();
            this.contratos = contratos;
            montaGridContrato();

        }

        private void btn_confirma_Click(object sender, EventArgs e)
        {
            try
            {
                if (dg_contrato.CurrentRow == null)
                    throw new Exception("Selecione um contrato.");
                 
                FinanceiroFacade financeiroFacade = FinanceiroFacade.getInstance();

                contrato = financeiroFacade.findContratoById((Int64)dg_contrato.CurrentRow.Cells[0].Value);
                
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btn_fechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void montaGridContrato()
        {
            try
            {
                dg_contrato.Columns.Clear();
                dg_contrato.ColumnCount = 4;

                dg_contrato.Columns[0].HeaderText = "IdContrato";
                dg_contrato.Columns[0].Visible = false;

                dg_contrato.Columns[1].HeaderText = "Cliente";

                dg_contrato.Columns[2].HeaderText = "Prestador";

                dg_contrato.Columns[3].HeaderText = "Código Contrato";
                dg_contrato.Columns[3].DisplayIndex = 1;

                foreach (Contrato contrato in contratos)
                    dg_contrato.Rows.Add(contrato.Id, contrato.Cliente.RazaoSocial, contrato.Prestador != null ? contrato.Prestador.RazaoSocial : String.Empty, !String.IsNullOrEmpty(contrato.CodigoContrato) ? ValidaCampoHelper.RetornaCodigoEstudoFormatado(contrato.CodigoContrato) : String.Empty);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dg_contrato_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btn_confirma.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
