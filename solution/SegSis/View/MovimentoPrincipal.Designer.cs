﻿namespace SWS.View
{
    partial class frmMovimentoPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flpAcao = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_faturamento = new System.Windows.Forms.Button();
            this.btn_incluir = new System.Windows.Forms.Button();
            this.btn_alterar = new System.Windows.Forms.Button();
            this.btn_cancelar = new System.Windows.Forms.Button();
            this.btn_pesquisar = new System.Windows.Forms.Button();
            this.btn_limpar = new System.Windows.Forms.Button();
            this.btn_fechar = new System.Windows.Forms.Button();
            this.lblCliente = new System.Windows.Forms.TextBox();
            this.lblSituacao = new System.Windows.Forms.TextBox();
            this.lblLancamento = new System.Windows.Forms.TextBox();
            this.lblFaturamento = new System.Windows.Forms.TextBox();
            this.lblNotaFiscal = new System.Windows.Forms.TextBox();
            this.lblClienteParticular = new System.Windows.Forms.TextBox();
            this.lblProduto = new System.Windows.Forms.TextBox();
            this.lblExame = new System.Windows.Forms.TextBox();
            this.btn_cliente = new System.Windows.Forms.Button();
            this.text_cliente = new System.Windows.Forms.TextBox();
            this.cb_situacao = new SWS.ComboBoxWithBorder();
            this.dt_finalLancamento = new System.Windows.Forms.DateTimePicker();
            this.dt_inicialLancamento = new System.Windows.Forms.DateTimePicker();
            this.dt_finalFaturamento = new System.Windows.Forms.DateTimePicker();
            this.dt_inicialFaturamento = new System.Windows.Forms.DateTimePicker();
            this.text_nfe = new System.Windows.Forms.TextBox();
            this.cbClienteParticular = new SWS.ComboBoxWithBorder();
            this.btn_produto = new System.Windows.Forms.Button();
            this.text_produto = new System.Windows.Forms.TextBox();
            this.btn_exame = new System.Windows.Forms.Button();
            this.text_exame = new System.Windows.Forms.TextBox();
            this.chk_marcarTodos = new System.Windows.Forms.CheckBox();
            this.grb_movimento = new System.Windows.Forms.GroupBox();
            this.dg_movimento = new System.Windows.Forms.DataGridView();
            this.grb_totais = new System.Windows.Forms.GroupBox();
            this.lblQuantExames = new System.Windows.Forms.TextBox();
            this.lblQuantProduto = new System.Windows.Forms.TextBox();
            this.lblQuantidadeItem = new System.Windows.Forms.TextBox();
            this.lblTotalValor = new System.Windows.Forms.TextBox();
            this.textQuantidadeExame = new System.Windows.Forms.TextBox();
            this.textQuantidadeProduto = new System.Windows.Forms.TextBox();
            this.textValor = new System.Windows.Forms.TextBox();
            this.textQuantidade = new System.Windows.Forms.TextBox();
            this.lblCorFaturado = new System.Windows.Forms.Label();
            this.lblCorCancelado = new System.Windows.Forms.Label();
            this.lblCorPendente = new System.Windows.Forms.Label();
            this.lblFaturado = new System.Windows.Forms.Label();
            this.lblCancelado = new System.Windows.Forms.Label();
            this.lblPendente = new System.Windows.Forms.Label();
            this.grbLegenda = new System.Windows.Forms.GroupBox();
            this.lblCentroCusto = new System.Windows.Forms.TextBox();
            this.textCentroCusto = new System.Windows.Forms.TextBox();
            this.btnCentroCusto = new System.Windows.Forms.Button();
            this.btnExcluirCliente = new System.Windows.Forms.Button();
            this.btnExcluirCentroCusto = new System.Windows.Forms.Button();
            this.btnExcluirProduto = new System.Windows.Forms.Button();
            this.btnExcluirExame = new System.Windows.Forms.Button();
            this.lblAtendimento = new System.Windows.Forms.TextBox();
            this.dataFinalAtendimento = new System.Windows.Forms.DateTimePicker();
            this.dataInicialAtendimento = new System.Windows.Forms.DateTimePicker();
            this.btnExcluirCredenciada = new System.Windows.Forms.Button();
            this.btnCredenciada = new System.Windows.Forms.Button();
            this.textCredenciada = new System.Windows.Forms.TextBox();
            this.lblCredenciada = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).BeginInit();
            this.spForm.Panel1.SuspendLayout();
            this.spForm.Panel2.SuspendLayout();
            this.spForm.SuspendLayout();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).BeginInit();
            this.flpAcao.SuspendLayout();
            this.grb_movimento.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_movimento)).BeginInit();
            this.grb_totais.SuspendLayout();
            this.grbLegenda.SuspendLayout();
            this.SuspendLayout();
            // 
            // spForm
            // 
            this.spForm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // spForm.Panel1
            // 
            this.spForm.Panel1.Controls.Add(this.flpAcao);
            // 
            // pnlForm
            // 
            this.pnlForm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlForm.Controls.Add(this.btnExcluirCredenciada);
            this.pnlForm.Controls.Add(this.btnCredenciada);
            this.pnlForm.Controls.Add(this.textCredenciada);
            this.pnlForm.Controls.Add(this.lblCredenciada);
            this.pnlForm.Controls.Add(this.lblFaturamento);
            this.pnlForm.Controls.Add(this.lblAtendimento);
            this.pnlForm.Controls.Add(this.dataFinalAtendimento);
            this.pnlForm.Controls.Add(this.dataInicialAtendimento);
            this.pnlForm.Controls.Add(this.btnExcluirExame);
            this.pnlForm.Controls.Add(this.btnExcluirProduto);
            this.pnlForm.Controls.Add(this.btnExcluirCentroCusto);
            this.pnlForm.Controls.Add(this.btnExcluirCliente);
            this.pnlForm.Controls.Add(this.btnCentroCusto);
            this.pnlForm.Controls.Add(this.textCentroCusto);
            this.pnlForm.Controls.Add(this.lblCentroCusto);
            this.pnlForm.Controls.Add(this.lblLancamento);
            this.pnlForm.Controls.Add(this.grbLegenda);
            this.pnlForm.Controls.Add(this.grb_totais);
            this.pnlForm.Controls.Add(this.grb_movimento);
            this.pnlForm.Controls.Add(this.chk_marcarTodos);
            this.pnlForm.Controls.Add(this.btn_exame);
            this.pnlForm.Controls.Add(this.text_exame);
            this.pnlForm.Controls.Add(this.btn_produto);
            this.pnlForm.Controls.Add(this.text_produto);
            this.pnlForm.Controls.Add(this.cbClienteParticular);
            this.pnlForm.Controls.Add(this.text_nfe);
            this.pnlForm.Controls.Add(this.dt_finalFaturamento);
            this.pnlForm.Controls.Add(this.dt_inicialFaturamento);
            this.pnlForm.Controls.Add(this.dt_finalLancamento);
            this.pnlForm.Controls.Add(this.dt_inicialLancamento);
            this.pnlForm.Controls.Add(this.cb_situacao);
            this.pnlForm.Controls.Add(this.btn_cliente);
            this.pnlForm.Controls.Add(this.text_cliente);
            this.pnlForm.Controls.Add(this.lblExame);
            this.pnlForm.Controls.Add(this.lblProduto);
            this.pnlForm.Controls.Add(this.lblClienteParticular);
            this.pnlForm.Controls.Add(this.lblNotaFiscal);
            this.pnlForm.Controls.Add(this.lblSituacao);
            this.pnlForm.Controls.Add(this.lblCliente);
            this.pnlForm.Size = new System.Drawing.Size(781, 464);
            // 
            // banner
            // 
            this.banner.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // flpAcao
            // 
            this.flpAcao.Controls.Add(this.btn_faturamento);
            this.flpAcao.Controls.Add(this.btn_incluir);
            this.flpAcao.Controls.Add(this.btn_alterar);
            this.flpAcao.Controls.Add(this.btn_cancelar);
            this.flpAcao.Controls.Add(this.btn_pesquisar);
            this.flpAcao.Controls.Add(this.btn_limpar);
            this.flpAcao.Controls.Add(this.btn_fechar);
            this.flpAcao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAcao.Location = new System.Drawing.Point(0, 0);
            this.flpAcao.Name = "flpAcao";
            this.flpAcao.Size = new System.Drawing.Size(784, 35);
            this.flpAcao.TabIndex = 0;
            // 
            // btn_faturamento
            // 
            this.btn_faturamento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_faturamento.Image = global::SWS.Properties.Resources.Icone_faturamento;
            this.btn_faturamento.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_faturamento.Location = new System.Drawing.Point(3, 3);
            this.btn_faturamento.Name = "btn_faturamento";
            this.btn_faturamento.Size = new System.Drawing.Size(75, 23);
            this.btn_faturamento.TabIndex = 7;
            this.btn_faturamento.TabStop = false;
            this.btn_faturamento.Text = "F&aturar";
            this.btn_faturamento.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_faturamento.UseVisualStyleBackColor = true;
            this.btn_faturamento.Click += new System.EventHandler(this.btn_faturamento_Click);
            // 
            // btn_incluir
            // 
            this.btn_incluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_incluir.Image = global::SWS.Properties.Resources.icone_mais1;
            this.btn_incluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_incluir.Location = new System.Drawing.Point(84, 3);
            this.btn_incluir.Name = "btn_incluir";
            this.btn_incluir.Size = new System.Drawing.Size(75, 23);
            this.btn_incluir.TabIndex = 9;
            this.btn_incluir.TabStop = false;
            this.btn_incluir.Text = "&Avulso";
            this.btn_incluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_incluir.UseVisualStyleBackColor = true;
            this.btn_incluir.Click += new System.EventHandler(this.btn_incluir_Click);
            // 
            // btn_alterar
            // 
            this.btn_alterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_alterar.Image = global::SWS.Properties.Resources.Alterar;
            this.btn_alterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_alterar.Location = new System.Drawing.Point(165, 3);
            this.btn_alterar.Name = "btn_alterar";
            this.btn_alterar.Size = new System.Drawing.Size(75, 23);
            this.btn_alterar.TabIndex = 10;
            this.btn_alterar.TabStop = false;
            this.btn_alterar.Text = "&Alterar";
            this.btn_alterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_alterar.UseVisualStyleBackColor = true;
            this.btn_alterar.Click += new System.EventHandler(this.btn_alterar_Click);
            // 
            // btn_cancelar
            // 
            this.btn_cancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_cancelar.Image = global::SWS.Properties.Resources.Icone_cancelar;
            this.btn_cancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_cancelar.Location = new System.Drawing.Point(246, 3);
            this.btn_cancelar.Name = "btn_cancelar";
            this.btn_cancelar.Size = new System.Drawing.Size(75, 23);
            this.btn_cancelar.TabIndex = 8;
            this.btn_cancelar.TabStop = false;
            this.btn_cancelar.Text = "&Cancelar";
            this.btn_cancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_cancelar.UseVisualStyleBackColor = true;
            this.btn_cancelar.Click += new System.EventHandler(this.btn_cancelar_Click);
            // 
            // btn_pesquisar
            // 
            this.btn_pesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_pesquisar.Image = global::SWS.Properties.Resources.busca;
            this.btn_pesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_pesquisar.Location = new System.Drawing.Point(327, 3);
            this.btn_pesquisar.Name = "btn_pesquisar";
            this.btn_pesquisar.Size = new System.Drawing.Size(75, 23);
            this.btn_pesquisar.TabIndex = 11;
            this.btn_pesquisar.TabStop = false;
            this.btn_pesquisar.Text = "&Pesquisar";
            this.btn_pesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_pesquisar.UseVisualStyleBackColor = true;
            this.btn_pesquisar.Click += new System.EventHandler(this.btn_pesquisar_Click);
            // 
            // btn_limpar
            // 
            this.btn_limpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_limpar.Image = global::SWS.Properties.Resources.vassoura;
            this.btn_limpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_limpar.Location = new System.Drawing.Point(408, 3);
            this.btn_limpar.Name = "btn_limpar";
            this.btn_limpar.Size = new System.Drawing.Size(75, 23);
            this.btn_limpar.TabIndex = 12;
            this.btn_limpar.TabStop = false;
            this.btn_limpar.Text = "&Limpar";
            this.btn_limpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_limpar.UseVisualStyleBackColor = true;
            this.btn_limpar.Click += new System.EventHandler(this.btn_limpar_Click);
            // 
            // btn_fechar
            // 
            this.btn_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fechar.Image = global::SWS.Properties.Resources.close;
            this.btn_fechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_fechar.Location = new System.Drawing.Point(489, 3);
            this.btn_fechar.Name = "btn_fechar";
            this.btn_fechar.Size = new System.Drawing.Size(75, 23);
            this.btn_fechar.TabIndex = 6;
            this.btn_fechar.TabStop = false;
            this.btn_fechar.Text = "&Fechar";
            this.btn_fechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_fechar.UseVisualStyleBackColor = true;
            this.btn_fechar.Click += new System.EventHandler(this.btn_fechar_Click);
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.LightGray;
            this.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(6, 7);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.ReadOnly = true;
            this.lblCliente.Size = new System.Drawing.Size(108, 21);
            this.lblCliente.TabIndex = 0;
            this.lblCliente.TabStop = false;
            this.lblCliente.Text = "Cliente";
            // 
            // lblSituacao
            // 
            this.lblSituacao.BackColor = System.Drawing.Color.LightGray;
            this.lblSituacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSituacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSituacao.Location = new System.Drawing.Point(6, 67);
            this.lblSituacao.Name = "lblSituacao";
            this.lblSituacao.ReadOnly = true;
            this.lblSituacao.Size = new System.Drawing.Size(108, 21);
            this.lblSituacao.TabIndex = 1;
            this.lblSituacao.TabStop = false;
            this.lblSituacao.Text = "Situação";
            // 
            // lblLancamento
            // 
            this.lblLancamento.BackColor = System.Drawing.Color.LightGray;
            this.lblLancamento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblLancamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLancamento.Location = new System.Drawing.Point(375, 7);
            this.lblLancamento.Name = "lblLancamento";
            this.lblLancamento.ReadOnly = true;
            this.lblLancamento.Size = new System.Drawing.Size(108, 21);
            this.lblLancamento.TabIndex = 2;
            this.lblLancamento.TabStop = false;
            this.lblLancamento.Text = "Lançamento";
            // 
            // lblFaturamento
            // 
            this.lblFaturamento.BackColor = System.Drawing.Color.LightGray;
            this.lblFaturamento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFaturamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFaturamento.Location = new System.Drawing.Point(375, 27);
            this.lblFaturamento.Name = "lblFaturamento";
            this.lblFaturamento.ReadOnly = true;
            this.lblFaturamento.Size = new System.Drawing.Size(108, 21);
            this.lblFaturamento.TabIndex = 3;
            this.lblFaturamento.TabStop = false;
            this.lblFaturamento.Text = "Faturamento";
            // 
            // lblNotaFiscal
            // 
            this.lblNotaFiscal.BackColor = System.Drawing.Color.LightGray;
            this.lblNotaFiscal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNotaFiscal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNotaFiscal.Location = new System.Drawing.Point(6, 106);
            this.lblNotaFiscal.Name = "lblNotaFiscal";
            this.lblNotaFiscal.ReadOnly = true;
            this.lblNotaFiscal.Size = new System.Drawing.Size(108, 21);
            this.lblNotaFiscal.TabIndex = 4;
            this.lblNotaFiscal.TabStop = false;
            this.lblNotaFiscal.Text = "Nota fiscal";
            // 
            // lblClienteParticular
            // 
            this.lblClienteParticular.BackColor = System.Drawing.Color.LightGray;
            this.lblClienteParticular.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblClienteParticular.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClienteParticular.Location = new System.Drawing.Point(6, 87);
            this.lblClienteParticular.Name = "lblClienteParticular";
            this.lblClienteParticular.ReadOnly = true;
            this.lblClienteParticular.Size = new System.Drawing.Size(108, 21);
            this.lblClienteParticular.TabIndex = 5;
            this.lblClienteParticular.TabStop = false;
            this.lblClienteParticular.Text = "Cliente Particular";
            // 
            // lblProduto
            // 
            this.lblProduto.BackColor = System.Drawing.Color.LightGray;
            this.lblProduto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblProduto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProduto.Location = new System.Drawing.Point(375, 67);
            this.lblProduto.Name = "lblProduto";
            this.lblProduto.ReadOnly = true;
            this.lblProduto.Size = new System.Drawing.Size(108, 21);
            this.lblProduto.TabIndex = 6;
            this.lblProduto.TabStop = false;
            this.lblProduto.Text = "Produto";
            // 
            // lblExame
            // 
            this.lblExame.BackColor = System.Drawing.Color.LightGray;
            this.lblExame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblExame.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExame.Location = new System.Drawing.Point(375, 87);
            this.lblExame.Name = "lblExame";
            this.lblExame.ReadOnly = true;
            this.lblExame.Size = new System.Drawing.Size(108, 21);
            this.lblExame.TabIndex = 7;
            this.lblExame.TabStop = false;
            this.lblExame.Text = "Exame";
            // 
            // btn_cliente
            // 
            this.btn_cliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_cliente.Image = global::SWS.Properties.Resources.busca;
            this.btn_cliente.Location = new System.Drawing.Point(287, 7);
            this.btn_cliente.Name = "btn_cliente";
            this.btn_cliente.Size = new System.Drawing.Size(33, 21);
            this.btn_cliente.TabIndex = 1;
            this.btn_cliente.UseVisualStyleBackColor = true;
            this.btn_cliente.Click += new System.EventHandler(this.btn_cliente_Click);
            // 
            // text_cliente
            // 
            this.text_cliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_cliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_cliente.Location = new System.Drawing.Point(113, 7);
            this.text_cliente.MaxLength = 100;
            this.text_cliente.Name = "text_cliente";
            this.text_cliente.ReadOnly = true;
            this.text_cliente.Size = new System.Drawing.Size(176, 21);
            this.text_cliente.TabIndex = 8;
            this.text_cliente.TabStop = false;
            // 
            // cb_situacao
            // 
            this.cb_situacao.BackColor = System.Drawing.Color.LightGray;
            this.cb_situacao.BorderColor = System.Drawing.Color.DimGray;
            this.cb_situacao.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cb_situacao.FormattingEnabled = true;
            this.cb_situacao.Location = new System.Drawing.Point(113, 67);
            this.cb_situacao.Name = "cb_situacao";
            this.cb_situacao.Size = new System.Drawing.Size(239, 21);
            this.cb_situacao.TabIndex = 2;
            // 
            // dt_finalLancamento
            // 
            this.dt_finalLancamento.Checked = false;
            this.dt_finalLancamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dt_finalLancamento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dt_finalLancamento.Location = new System.Drawing.Point(617, 7);
            this.dt_finalLancamento.Name = "dt_finalLancamento";
            this.dt_finalLancamento.ShowCheckBox = true;
            this.dt_finalLancamento.Size = new System.Drawing.Size(133, 21);
            this.dt_finalLancamento.TabIndex = 4;
            // 
            // dt_inicialLancamento
            // 
            this.dt_inicialLancamento.Checked = false;
            this.dt_inicialLancamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dt_inicialLancamento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dt_inicialLancamento.Location = new System.Drawing.Point(482, 7);
            this.dt_inicialLancamento.Name = "dt_inicialLancamento";
            this.dt_inicialLancamento.ShowCheckBox = true;
            this.dt_inicialLancamento.Size = new System.Drawing.Size(133, 21);
            this.dt_inicialLancamento.TabIndex = 3;
            // 
            // dt_finalFaturamento
            // 
            this.dt_finalFaturamento.Checked = false;
            this.dt_finalFaturamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dt_finalFaturamento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dt_finalFaturamento.Location = new System.Drawing.Point(617, 27);
            this.dt_finalFaturamento.Name = "dt_finalFaturamento";
            this.dt_finalFaturamento.ShowCheckBox = true;
            this.dt_finalFaturamento.Size = new System.Drawing.Size(133, 21);
            this.dt_finalFaturamento.TabIndex = 6;
            // 
            // dt_inicialFaturamento
            // 
            this.dt_inicialFaturamento.Checked = false;
            this.dt_inicialFaturamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dt_inicialFaturamento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dt_inicialFaturamento.Location = new System.Drawing.Point(482, 27);
            this.dt_inicialFaturamento.Name = "dt_inicialFaturamento";
            this.dt_inicialFaturamento.ShowCheckBox = true;
            this.dt_inicialFaturamento.Size = new System.Drawing.Size(133, 21);
            this.dt_inicialFaturamento.TabIndex = 5;
            // 
            // text_nfe
            // 
            this.text_nfe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_nfe.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_nfe.Location = new System.Drawing.Point(113, 106);
            this.text_nfe.MaxLength = 20;
            this.text_nfe.Name = "text_nfe";
            this.text_nfe.Size = new System.Drawing.Size(239, 21);
            this.text_nfe.TabIndex = 7;
            this.text_nfe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.text_nfe_KeyPress);
            // 
            // cbClienteParticular
            // 
            this.cbClienteParticular.BackColor = System.Drawing.Color.LightGray;
            this.cbClienteParticular.BorderColor = System.Drawing.Color.DimGray;
            this.cbClienteParticular.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cbClienteParticular.FormattingEnabled = true;
            this.cbClienteParticular.Location = new System.Drawing.Point(113, 87);
            this.cbClienteParticular.Name = "cbClienteParticular";
            this.cbClienteParticular.Size = new System.Drawing.Size(239, 21);
            this.cbClienteParticular.TabIndex = 8;
            // 
            // btn_produto
            // 
            this.btn_produto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_produto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_produto.Image = global::SWS.Properties.Resources.busca;
            this.btn_produto.Location = new System.Drawing.Point(685, 67);
            this.btn_produto.Name = "btn_produto";
            this.btn_produto.Size = new System.Drawing.Size(33, 21);
            this.btn_produto.TabIndex = 9;
            this.btn_produto.UseVisualStyleBackColor = true;
            this.btn_produto.Click += new System.EventHandler(this.btn_produto_Click);
            // 
            // text_produto
            // 
            this.text_produto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_produto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_produto.Location = new System.Drawing.Point(482, 67);
            this.text_produto.MaxLength = 100;
            this.text_produto.Multiline = true;
            this.text_produto.Name = "text_produto";
            this.text_produto.ReadOnly = true;
            this.text_produto.Size = new System.Drawing.Size(204, 21);
            this.text_produto.TabIndex = 18;
            this.text_produto.TabStop = false;
            // 
            // btn_exame
            // 
            this.btn_exame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_exame.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_exame.Image = global::SWS.Properties.Resources.busca;
            this.btn_exame.Location = new System.Drawing.Point(685, 87);
            this.btn_exame.Name = "btn_exame";
            this.btn_exame.Size = new System.Drawing.Size(33, 21);
            this.btn_exame.TabIndex = 10;
            this.btn_exame.UseVisualStyleBackColor = true;
            this.btn_exame.Click += new System.EventHandler(this.btn_exame_Click);
            // 
            // text_exame
            // 
            this.text_exame.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.text_exame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_exame.Location = new System.Drawing.Point(482, 87);
            this.text_exame.MaxLength = 100;
            this.text_exame.Multiline = true;
            this.text_exame.Name = "text_exame";
            this.text_exame.ReadOnly = true;
            this.text_exame.Size = new System.Drawing.Size(204, 21);
            this.text_exame.TabIndex = 20;
            this.text_exame.TabStop = false;
            // 
            // chk_marcarTodos
            // 
            this.chk_marcarTodos.AutoSize = true;
            this.chk_marcarTodos.Checked = true;
            this.chk_marcarTodos.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_marcarTodos.Enabled = false;
            this.chk_marcarTodos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chk_marcarTodos.Location = new System.Drawing.Point(616, 112);
            this.chk_marcarTodos.Name = "chk_marcarTodos";
            this.chk_marcarTodos.Size = new System.Drawing.Size(145, 17);
            this.chk_marcarTodos.TabIndex = 11;
            this.chk_marcarTodos.Text = "Marcar / desmarcar todos";
            this.chk_marcarTodos.UseVisualStyleBackColor = true;
            this.chk_marcarTodos.CheckedChanged += new System.EventHandler(this.chk_marcarTodos_CheckedChanged);
            // 
            // grb_movimento
            // 
            this.grb_movimento.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grb_movimento.Controls.Add(this.dg_movimento);
            this.grb_movimento.Location = new System.Drawing.Point(3, 135);
            this.grb_movimento.Name = "grb_movimento";
            this.grb_movimento.Size = new System.Drawing.Size(775, 231);
            this.grb_movimento.TabIndex = 23;
            this.grb_movimento.TabStop = false;
            this.grb_movimento.Text = "Movimento";
            // 
            // dg_movimento
            // 
            this.dg_movimento.AllowUserToAddRows = false;
            this.dg_movimento.AllowUserToDeleteRows = false;
            this.dg_movimento.AllowUserToResizeRows = false;
            this.dg_movimento.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dg_movimento.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dg_movimento.BackgroundColor = System.Drawing.Color.White;
            this.dg_movimento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dg_movimento.DefaultCellStyle = dataGridViewCellStyle2;
            this.dg_movimento.Location = new System.Drawing.Point(3, 16);
            this.dg_movimento.Name = "dg_movimento";
            this.dg_movimento.RowHeadersVisible = false;
            this.dg_movimento.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_movimento.Size = new System.Drawing.Size(769, 210);
            this.dg_movimento.TabIndex = 12;
            this.dg_movimento.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dg_movimento_ColumnAdded);
            this.dg_movimento.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dg_movimento_RowPrePaint);
            // 
            // grb_totais
            // 
            this.grb_totais.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.grb_totais.Controls.Add(this.lblQuantExames);
            this.grb_totais.Controls.Add(this.lblQuantProduto);
            this.grb_totais.Controls.Add(this.lblQuantidadeItem);
            this.grb_totais.Controls.Add(this.lblTotalValor);
            this.grb_totais.Controls.Add(this.textQuantidadeExame);
            this.grb_totais.Controls.Add(this.textQuantidadeProduto);
            this.grb_totais.Controls.Add(this.textValor);
            this.grb_totais.Controls.Add(this.textQuantidade);
            this.grb_totais.Location = new System.Drawing.Point(3, 372);
            this.grb_totais.Name = "grb_totais";
            this.grb_totais.Size = new System.Drawing.Size(415, 64);
            this.grb_totais.TabIndex = 24;
            this.grb_totais.TabStop = false;
            this.grb_totais.Text = "Totais";
            // 
            // lblQuantExames
            // 
            this.lblQuantExames.BackColor = System.Drawing.Color.LightGray;
            this.lblQuantExames.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblQuantExames.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantExames.Location = new System.Drawing.Point(201, 36);
            this.lblQuantExames.Name = "lblQuantExames";
            this.lblQuantExames.ReadOnly = true;
            this.lblQuantExames.Size = new System.Drawing.Size(119, 21);
            this.lblQuantExames.TabIndex = 11;
            this.lblQuantExames.TabStop = false;
            this.lblQuantExames.Text = "Quant. de exames";
            // 
            // lblQuantProduto
            // 
            this.lblQuantProduto.BackColor = System.Drawing.Color.LightGray;
            this.lblQuantProduto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblQuantProduto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantProduto.Location = new System.Drawing.Point(201, 16);
            this.lblQuantProduto.Name = "lblQuantProduto";
            this.lblQuantProduto.ReadOnly = true;
            this.lblQuantProduto.Size = new System.Drawing.Size(119, 21);
            this.lblQuantProduto.TabIndex = 10;
            this.lblQuantProduto.TabStop = false;
            this.lblQuantProduto.Text = "Quant. de Produtos";
            // 
            // lblQuantidadeItem
            // 
            this.lblQuantidadeItem.BackColor = System.Drawing.Color.LightGray;
            this.lblQuantidadeItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblQuantidadeItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantidadeItem.Location = new System.Drawing.Point(9, 36);
            this.lblQuantidadeItem.Name = "lblQuantidadeItem";
            this.lblQuantidadeItem.ReadOnly = true;
            this.lblQuantidadeItem.Size = new System.Drawing.Size(108, 21);
            this.lblQuantidadeItem.TabIndex = 9;
            this.lblQuantidadeItem.TabStop = false;
            this.lblQuantidadeItem.Text = "Quantidade Item";
            // 
            // lblTotalValor
            // 
            this.lblTotalValor.BackColor = System.Drawing.Color.LightGray;
            this.lblTotalValor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTotalValor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalValor.Location = new System.Drawing.Point(9, 16);
            this.lblTotalValor.Name = "lblTotalValor";
            this.lblTotalValor.ReadOnly = true;
            this.lblTotalValor.Size = new System.Drawing.Size(108, 21);
            this.lblTotalValor.TabIndex = 8;
            this.lblTotalValor.TabStop = false;
            this.lblTotalValor.Text = "Total R$";
            // 
            // textQuantidadeExame
            // 
            this.textQuantidadeExame.BackColor = System.Drawing.Color.LightSteelBlue;
            this.textQuantidadeExame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textQuantidadeExame.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textQuantidadeExame.Location = new System.Drawing.Point(319, 36);
            this.textQuantidadeExame.MaxLength = 10;
            this.textQuantidadeExame.Name = "textQuantidadeExame";
            this.textQuantidadeExame.ReadOnly = true;
            this.textQuantidadeExame.Size = new System.Drawing.Size(77, 21);
            this.textQuantidadeExame.TabIndex = 7;
            this.textQuantidadeExame.TabStop = false;
            // 
            // textQuantidadeProduto
            // 
            this.textQuantidadeProduto.BackColor = System.Drawing.Color.LightSteelBlue;
            this.textQuantidadeProduto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textQuantidadeProduto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textQuantidadeProduto.Location = new System.Drawing.Point(319, 16);
            this.textQuantidadeProduto.MaxLength = 10;
            this.textQuantidadeProduto.Name = "textQuantidadeProduto";
            this.textQuantidadeProduto.ReadOnly = true;
            this.textQuantidadeProduto.Size = new System.Drawing.Size(77, 21);
            this.textQuantidadeProduto.TabIndex = 5;
            this.textQuantidadeProduto.TabStop = false;
            // 
            // textValor
            // 
            this.textValor.BackColor = System.Drawing.Color.LightSteelBlue;
            this.textValor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textValor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textValor.Location = new System.Drawing.Point(116, 16);
            this.textValor.MaxLength = 10;
            this.textValor.Name = "textValor";
            this.textValor.ReadOnly = true;
            this.textValor.Size = new System.Drawing.Size(77, 21);
            this.textValor.TabIndex = 3;
            this.textValor.TabStop = false;
            // 
            // textQuantidade
            // 
            this.textQuantidade.BackColor = System.Drawing.Color.LightSteelBlue;
            this.textQuantidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textQuantidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textQuantidade.Location = new System.Drawing.Point(116, 36);
            this.textQuantidade.MaxLength = 10;
            this.textQuantidade.Name = "textQuantidade";
            this.textQuantidade.ReadOnly = true;
            this.textQuantidade.Size = new System.Drawing.Size(77, 21);
            this.textQuantidade.TabIndex = 2;
            this.textQuantidade.TabStop = false;
            // 
            // lblCorFaturado
            // 
            this.lblCorFaturado.AutoSize = true;
            this.lblCorFaturado.BackColor = System.Drawing.Color.Green;
            this.lblCorFaturado.Location = new System.Drawing.Point(12, 20);
            this.lblCorFaturado.Name = "lblCorFaturado";
            this.lblCorFaturado.Size = new System.Drawing.Size(13, 13);
            this.lblCorFaturado.TabIndex = 12;
            this.lblCorFaturado.Text = "  ";
            // 
            // lblCorCancelado
            // 
            this.lblCorCancelado.AutoSize = true;
            this.lblCorCancelado.BackColor = System.Drawing.Color.Red;
            this.lblCorCancelado.Location = new System.Drawing.Point(163, 20);
            this.lblCorCancelado.Name = "lblCorCancelado";
            this.lblCorCancelado.Size = new System.Drawing.Size(13, 13);
            this.lblCorCancelado.TabIndex = 13;
            this.lblCorCancelado.Text = "  ";
            // 
            // lblCorPendente
            // 
            this.lblCorPendente.AutoSize = true;
            this.lblCorPendente.BackColor = System.Drawing.Color.Black;
            this.lblCorPendente.Location = new System.Drawing.Point(89, 20);
            this.lblCorPendente.Name = "lblCorPendente";
            this.lblCorPendente.Size = new System.Drawing.Size(13, 13);
            this.lblCorPendente.TabIndex = 14;
            this.lblCorPendente.Text = "  ";
            // 
            // lblFaturado
            // 
            this.lblFaturado.AutoSize = true;
            this.lblFaturado.Location = new System.Drawing.Point(31, 20);
            this.lblFaturado.Name = "lblFaturado";
            this.lblFaturado.Size = new System.Drawing.Size(49, 13);
            this.lblFaturado.TabIndex = 15;
            this.lblFaturado.Text = "Faturado";
            // 
            // lblCancelado
            // 
            this.lblCancelado.AutoSize = true;
            this.lblCancelado.Location = new System.Drawing.Point(182, 20);
            this.lblCancelado.Name = "lblCancelado";
            this.lblCancelado.Size = new System.Drawing.Size(58, 13);
            this.lblCancelado.TabIndex = 16;
            this.lblCancelado.Text = "Cancelado";
            // 
            // lblPendente
            // 
            this.lblPendente.AutoSize = true;
            this.lblPendente.Location = new System.Drawing.Point(108, 20);
            this.lblPendente.Name = "lblPendente";
            this.lblPendente.Size = new System.Drawing.Size(53, 13);
            this.lblPendente.TabIndex = 17;
            this.lblPendente.Text = "Pendente";
            // 
            // grbLegenda
            // 
            this.grbLegenda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.grbLegenda.Controls.Add(this.lblPendente);
            this.grbLegenda.Controls.Add(this.lblCorFaturado);
            this.grbLegenda.Controls.Add(this.lblCorCancelado);
            this.grbLegenda.Controls.Add(this.lblCancelado);
            this.grbLegenda.Controls.Add(this.lblCorPendente);
            this.grbLegenda.Controls.Add(this.lblFaturado);
            this.grbLegenda.Location = new System.Drawing.Point(425, 372);
            this.grbLegenda.Name = "grbLegenda";
            this.grbLegenda.Size = new System.Drawing.Size(333, 64);
            this.grbLegenda.TabIndex = 25;
            this.grbLegenda.TabStop = false;
            this.grbLegenda.Text = "Legenda";
            // 
            // lblCentroCusto
            // 
            this.lblCentroCusto.BackColor = System.Drawing.Color.LightGray;
            this.lblCentroCusto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCentroCusto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCentroCusto.Location = new System.Drawing.Point(6, 27);
            this.lblCentroCusto.Name = "lblCentroCusto";
            this.lblCentroCusto.ReadOnly = true;
            this.lblCentroCusto.Size = new System.Drawing.Size(108, 21);
            this.lblCentroCusto.TabIndex = 27;
            this.lblCentroCusto.TabStop = false;
            this.lblCentroCusto.Text = "Centro de Custo";
            // 
            // textCentroCusto
            // 
            this.textCentroCusto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCentroCusto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCentroCusto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCentroCusto.Location = new System.Drawing.Point(113, 27);
            this.textCentroCusto.MaxLength = 100;
            this.textCentroCusto.Name = "textCentroCusto";
            this.textCentroCusto.ReadOnly = true;
            this.textCentroCusto.Size = new System.Drawing.Size(175, 21);
            this.textCentroCusto.TabIndex = 28;
            this.textCentroCusto.TabStop = false;
            // 
            // btnCentroCusto
            // 
            this.btnCentroCusto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCentroCusto.Image = global::SWS.Properties.Resources.busca;
            this.btnCentroCusto.Location = new System.Drawing.Point(287, 27);
            this.btnCentroCusto.Name = "btnCentroCusto";
            this.btnCentroCusto.Size = new System.Drawing.Size(33, 21);
            this.btnCentroCusto.TabIndex = 29;
            this.btnCentroCusto.UseVisualStyleBackColor = true;
            this.btnCentroCusto.Click += new System.EventHandler(this.btnCentroCusto_Click);
            // 
            // btnExcluirCliente
            // 
            this.btnExcluirCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCliente.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirCliente.Location = new System.Drawing.Point(319, 7);
            this.btnExcluirCliente.Name = "btnExcluirCliente";
            this.btnExcluirCliente.Size = new System.Drawing.Size(33, 21);
            this.btnExcluirCliente.TabIndex = 30;
            this.btnExcluirCliente.UseVisualStyleBackColor = true;
            this.btnExcluirCliente.Click += new System.EventHandler(this.btnExcluirCliente_Click);
            // 
            // btnExcluirCentroCusto
            // 
            this.btnExcluirCentroCusto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCentroCusto.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirCentroCusto.Location = new System.Drawing.Point(319, 27);
            this.btnExcluirCentroCusto.Name = "btnExcluirCentroCusto";
            this.btnExcluirCentroCusto.Size = new System.Drawing.Size(33, 21);
            this.btnExcluirCentroCusto.TabIndex = 31;
            this.btnExcluirCentroCusto.UseVisualStyleBackColor = true;
            this.btnExcluirCentroCusto.Click += new System.EventHandler(this.btnExcluirCentroCusto_Click);
            // 
            // btnExcluirProduto
            // 
            this.btnExcluirProduto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirProduto.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirProduto.Location = new System.Drawing.Point(717, 67);
            this.btnExcluirProduto.Name = "btnExcluirProduto";
            this.btnExcluirProduto.Size = new System.Drawing.Size(33, 21);
            this.btnExcluirProduto.TabIndex = 32;
            this.btnExcluirProduto.UseVisualStyleBackColor = true;
            this.btnExcluirProduto.Click += new System.EventHandler(this.btnExcluirProduto_Click);
            // 
            // btnExcluirExame
            // 
            this.btnExcluirExame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirExame.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirExame.Location = new System.Drawing.Point(717, 87);
            this.btnExcluirExame.Name = "btnExcluirExame";
            this.btnExcluirExame.Size = new System.Drawing.Size(33, 21);
            this.btnExcluirExame.TabIndex = 33;
            this.btnExcluirExame.UseVisualStyleBackColor = true;
            this.btnExcluirExame.Click += new System.EventHandler(this.btnExcluirExame_Click);
            // 
            // lblAtendimento
            // 
            this.lblAtendimento.BackColor = System.Drawing.Color.LightGray;
            this.lblAtendimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAtendimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAtendimento.Location = new System.Drawing.Point(375, 47);
            this.lblAtendimento.Name = "lblAtendimento";
            this.lblAtendimento.ReadOnly = true;
            this.lblAtendimento.Size = new System.Drawing.Size(108, 21);
            this.lblAtendimento.TabIndex = 34;
            this.lblAtendimento.TabStop = false;
            this.lblAtendimento.Text = "Atendimento";
            // 
            // dataFinalAtendimento
            // 
            this.dataFinalAtendimento.Checked = false;
            this.dataFinalAtendimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataFinalAtendimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataFinalAtendimento.Location = new System.Drawing.Point(617, 47);
            this.dataFinalAtendimento.Name = "dataFinalAtendimento";
            this.dataFinalAtendimento.ShowCheckBox = true;
            this.dataFinalAtendimento.Size = new System.Drawing.Size(133, 21);
            this.dataFinalAtendimento.TabIndex = 36;
            // 
            // dataInicialAtendimento
            // 
            this.dataInicialAtendimento.Checked = false;
            this.dataInicialAtendimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataInicialAtendimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataInicialAtendimento.Location = new System.Drawing.Point(482, 47);
            this.dataInicialAtendimento.Name = "dataInicialAtendimento";
            this.dataInicialAtendimento.ShowCheckBox = true;
            this.dataInicialAtendimento.Size = new System.Drawing.Size(133, 21);
            this.dataInicialAtendimento.TabIndex = 35;
            // 
            // btnExcluirCredenciada
            // 
            this.btnExcluirCredenciada.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluirCredenciada.Image = global::SWS.Properties.Resources.close;
            this.btnExcluirCredenciada.Location = new System.Drawing.Point(319, 47);
            this.btnExcluirCredenciada.Name = "btnExcluirCredenciada";
            this.btnExcluirCredenciada.Size = new System.Drawing.Size(33, 21);
            this.btnExcluirCredenciada.TabIndex = 40;
            this.btnExcluirCredenciada.UseVisualStyleBackColor = true;
            this.btnExcluirCredenciada.Click += new System.EventHandler(this.btnExcluirCredenciada_Click);
            // 
            // btnCredenciada
            // 
            this.btnCredenciada.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCredenciada.Image = global::SWS.Properties.Resources.busca;
            this.btnCredenciada.Location = new System.Drawing.Point(287, 47);
            this.btnCredenciada.Name = "btnCredenciada";
            this.btnCredenciada.Size = new System.Drawing.Size(33, 21);
            this.btnCredenciada.TabIndex = 39;
            this.btnCredenciada.UseVisualStyleBackColor = true;
            this.btnCredenciada.Click += new System.EventHandler(this.btnCredenciada_Click);
            // 
            // textCredenciada
            // 
            this.textCredenciada.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textCredenciada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCredenciada.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCredenciada.Location = new System.Drawing.Point(113, 47);
            this.textCredenciada.MaxLength = 100;
            this.textCredenciada.Name = "textCredenciada";
            this.textCredenciada.ReadOnly = true;
            this.textCredenciada.Size = new System.Drawing.Size(175, 21);
            this.textCredenciada.TabIndex = 38;
            this.textCredenciada.TabStop = false;
            // 
            // lblCredenciada
            // 
            this.lblCredenciada.BackColor = System.Drawing.Color.LightGray;
            this.lblCredenciada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCredenciada.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCredenciada.Location = new System.Drawing.Point(6, 47);
            this.lblCredenciada.Name = "lblCredenciada";
            this.lblCredenciada.ReadOnly = true;
            this.lblCredenciada.Size = new System.Drawing.Size(108, 21);
            this.lblCredenciada.TabIndex = 37;
            this.lblCredenciada.TabStop = false;
            this.lblCredenciada.Text = "Credenciada";
            // 
            // frmMovimentoPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.MaximizeBox = true;
            this.Name = "frmMovimentoPrincipal";
            this.Text = "GERENCIAR MOVIMENTO";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmMovimentoPrincipal_KeyDown);
            this.spForm.Panel1.ResumeLayout(false);
            this.spForm.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spForm)).EndInit();
            this.spForm.ResumeLayout(false);
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.banner)).EndInit();
            this.flpAcao.ResumeLayout(false);
            this.grb_movimento.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_movimento)).EndInit();
            this.grb_totais.ResumeLayout(false);
            this.grb_totais.PerformLayout();
            this.grbLegenda.ResumeLayout(false);
            this.grbLegenda.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpAcao;
        private System.Windows.Forms.Button btn_faturamento;
        private System.Windows.Forms.Button btn_incluir;
        private System.Windows.Forms.Button btn_alterar;
        private System.Windows.Forms.Button btn_cancelar;
        private System.Windows.Forms.Button btn_fechar;
        private System.Windows.Forms.TextBox lblCliente;
        private System.Windows.Forms.TextBox lblSituacao;
        private System.Windows.Forms.TextBox lblLancamento;
        private System.Windows.Forms.TextBox lblFaturamento;
        private System.Windows.Forms.TextBox lblNotaFiscal;
        private System.Windows.Forms.TextBox lblClienteParticular;
        private System.Windows.Forms.TextBox lblExame;
        private System.Windows.Forms.TextBox lblProduto;
        public System.Windows.Forms.Button btn_limpar;
        public System.Windows.Forms.Button btn_pesquisar;
        public System.Windows.Forms.Button btn_cliente;
        public System.Windows.Forms.TextBox text_cliente;
        private ComboBoxWithBorder cb_situacao;
        public System.Windows.Forms.DateTimePicker dt_finalLancamento;
        public System.Windows.Forms.DateTimePicker dt_inicialLancamento;
        private System.Windows.Forms.DateTimePicker dt_finalFaturamento;
        private System.Windows.Forms.DateTimePicker dt_inicialFaturamento;
        private System.Windows.Forms.TextBox text_nfe;
        private ComboBoxWithBorder cbClienteParticular;
        private System.Windows.Forms.Button btn_produto;
        private System.Windows.Forms.TextBox text_produto;
        private System.Windows.Forms.Button btn_exame;
        private System.Windows.Forms.TextBox text_exame;
        private System.Windows.Forms.CheckBox chk_marcarTodos;
        private System.Windows.Forms.GroupBox grb_movimento;
        private System.Windows.Forms.DataGridView dg_movimento;
        private System.Windows.Forms.GroupBox grb_totais;
        private System.Windows.Forms.TextBox textQuantidadeExame;
        private System.Windows.Forms.TextBox textQuantidadeProduto;
        private System.Windows.Forms.TextBox textValor;
        private System.Windows.Forms.TextBox textQuantidade;
        private System.Windows.Forms.TextBox lblQuantExames;
        private System.Windows.Forms.TextBox lblQuantProduto;
        private System.Windows.Forms.TextBox lblQuantidadeItem;
        private System.Windows.Forms.TextBox lblTotalValor;
        private System.Windows.Forms.Label lblCorPendente;
        private System.Windows.Forms.Label lblCorCancelado;
        private System.Windows.Forms.Label lblCorFaturado;
        private System.Windows.Forms.GroupBox grbLegenda;
        private System.Windows.Forms.Label lblPendente;
        private System.Windows.Forms.Label lblCancelado;
        private System.Windows.Forms.Label lblFaturado;
        public System.Windows.Forms.Button btnCentroCusto;
        public System.Windows.Forms.TextBox textCentroCusto;
        private System.Windows.Forms.TextBox lblCentroCusto;
        public System.Windows.Forms.Button btnExcluirExame;
        public System.Windows.Forms.Button btnExcluirProduto;
        public System.Windows.Forms.Button btnExcluirCentroCusto;
        public System.Windows.Forms.Button btnExcluirCliente;
        private System.Windows.Forms.DateTimePicker dataFinalAtendimento;
        private System.Windows.Forms.DateTimePicker dataInicialAtendimento;
        private System.Windows.Forms.TextBox lblAtendimento;
        public System.Windows.Forms.Button btnExcluirCredenciada;
        public System.Windows.Forms.Button btnCredenciada;
        public System.Windows.Forms.TextBox textCredenciada;
        private System.Windows.Forms.TextBox lblCredenciada;
    }
}