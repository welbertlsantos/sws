﻿namespace SegSis.View
{
    partial class frm_PcmsoSemPpraPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.text_engenheiro = new System.Windows.Forms.TextBox();
            this.text_codPpra = new System.Windows.Forms.MaskedTextBox();
            this.grb_engenheiro = new System.Windows.Forms.GroupBox();
            this.bt_engenheiro = new System.Windows.Forms.Button();
            this.text_tecno = new System.Windows.Forms.TextBox();
            this.grb_tecnico = new System.Windows.Forms.GroupBox();
            this.bt_tecnico = new System.Windows.Forms.Button();
            this.rb_fechado = new System.Windows.Forms.RadioButton();
            this.bt_limpar = new System.Windows.Forms.Button();
            this.bt_pesquisar = new System.Windows.Forms.Button();
            this.rb_todos = new System.Windows.Forms.RadioButton();
            this.rb_construcao = new System.Windows.Forms.RadioButton();
            this.btn_clienteLocalizar = new System.Windows.Forms.Button();
            this.ck_dataCriacao = new System.Windows.Forms.CheckBox();
            this.grb_situacao = new System.Windows.Forms.GroupBox();
            this.rb_aguardando = new System.Windows.Forms.RadioButton();
            this.grb_data = new System.Windows.Forms.GroupBox();
            this.dtCriacao_final = new System.Windows.Forms.DateTimePicker();
            this.dtCriacao_inicial = new System.Windows.Forms.DateTimePicker();
            this.text_cliente = new System.Windows.Forms.TextBox();
            this.lbl_codPcmso = new System.Windows.Forms.Label();
            this.grb_filtro = new System.Windows.Forms.GroupBox();
            this.lbl_cliente = new System.Windows.Forms.Label();
            this.grb_gride = new System.Windows.Forms.GroupBox();
            this.grd_pcmso = new System.Windows.Forms.DataGridView();
            this.btn_corrigir = new System.Windows.Forms.Button();
            this.btn_revisao = new System.Windows.Forms.Button();
            this.btn_finalizar = new System.Windows.Forms.Button();
            this.btn_enviarAnalise = new System.Windows.Forms.Button();
            this.grb_paginacao = new System.Windows.Forms.GroupBox();
            this.bt_imprimir = new System.Windows.Forms.Button();
            this.bt_fechar = new System.Windows.Forms.Button();
            this.bt_incluir = new System.Windows.Forms.Button();
            this.bt_excluir = new System.Windows.Forms.Button();
            this.bt_alterar = new System.Windows.Forms.Button();
            this.pcmsoErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel.SuspendLayout();
            this.grb_engenheiro.SuspendLayout();
            this.grb_tecnico.SuspendLayout();
            this.grb_situacao.SuspendLayout();
            this.grb_data.SuspendLayout();
            this.grb_filtro.SuspendLayout();
            this.grb_gride.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_pcmso)).BeginInit();
            this.grb_paginacao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcmsoErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.Controls.Add(this.grb_paginacao);
            this.panel.Controls.Add(this.grb_gride);
            this.panel.Controls.Add(this.grb_filtro);
            // 
            // text_engenheiro
            // 
            this.text_engenheiro.BackColor = System.Drawing.SystemColors.Info;
            this.text_engenheiro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_engenheiro.Location = new System.Drawing.Point(6, 16);
            this.text_engenheiro.MaxLength = 100;
            this.text_engenheiro.Name = "text_engenheiro";
            this.text_engenheiro.ReadOnly = true;
            this.text_engenheiro.Size = new System.Drawing.Size(204, 20);
            this.text_engenheiro.TabIndex = 100;
            this.text_engenheiro.TabStop = false;
            // 
            // text_codPpra
            // 
            this.text_codPpra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_codPpra.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_codPpra.Location = new System.Drawing.Point(6, 32);
            this.text_codPpra.Mask = "0000,00,000000/00";
            this.text_codPpra.Name = "text_codPpra";
            this.text_codPpra.PromptChar = ' ';
            this.text_codPpra.Size = new System.Drawing.Size(136, 20);
            this.text_codPpra.TabIndex = 1;
            this.text_codPpra.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.text_codPpra.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePrompt;
            // 
            // grb_engenheiro
            // 
            this.grb_engenheiro.Controls.Add(this.text_engenheiro);
            this.grb_engenheiro.Controls.Add(this.bt_engenheiro);
            this.grb_engenheiro.Location = new System.Drawing.Point(294, 58);
            this.grb_engenheiro.Name = "grb_engenheiro";
            this.grb_engenheiro.Size = new System.Drawing.Size(285, 42);
            this.grb_engenheiro.TabIndex = 100;
            this.grb_engenheiro.TabStop = false;
            this.grb_engenheiro.Text = "Médico";
            // 
            // bt_engenheiro
            // 
            this.bt_engenheiro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_engenheiro.Location = new System.Drawing.Point(216, 14);
            this.bt_engenheiro.Name = "bt_engenheiro";
            this.bt_engenheiro.Size = new System.Drawing.Size(63, 22);
            this.bt_engenheiro.TabIndex = 7;
            this.bt_engenheiro.Text = "Médico";
            this.bt_engenheiro.UseVisualStyleBackColor = true;
            this.bt_engenheiro.Click += new System.EventHandler(this.bt_engenheiro_Click);
            // 
            // text_tecno
            // 
            this.text_tecno.BackColor = System.Drawing.SystemColors.Info;
            this.text_tecno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_tecno.Location = new System.Drawing.Point(9, 16);
            this.text_tecno.MaxLength = 100;
            this.text_tecno.Name = "text_tecno";
            this.text_tecno.ReadOnly = true;
            this.text_tecno.Size = new System.Drawing.Size(190, 20);
            this.text_tecno.TabIndex = 100;
            this.text_tecno.TabStop = false;
            // 
            // grb_tecnico
            // 
            this.grb_tecnico.Controls.Add(this.text_tecno);
            this.grb_tecnico.Controls.Add(this.bt_tecnico);
            this.grb_tecnico.Location = new System.Drawing.Point(6, 58);
            this.grb_tecnico.Name = "grb_tecnico";
            this.grb_tecnico.Size = new System.Drawing.Size(281, 42);
            this.grb_tecnico.TabIndex = 100;
            this.grb_tecnico.TabStop = false;
            this.grb_tecnico.Text = "Técnico";
            // 
            // bt_tecnico
            // 
            this.bt_tecnico.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_tecnico.Location = new System.Drawing.Point(205, 14);
            this.bt_tecnico.Name = "bt_tecnico";
            this.bt_tecnico.Size = new System.Drawing.Size(63, 22);
            this.bt_tecnico.TabIndex = 6;
            this.bt_tecnico.Text = "Técnico";
            this.bt_tecnico.UseVisualStyleBackColor = true;
            this.bt_tecnico.Click += new System.EventHandler(this.bt_tecnico_Click);
            // 
            // rb_fechado
            // 
            this.rb_fechado.AutoSize = true;
            this.rb_fechado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rb_fechado.Location = new System.Drawing.Point(96, 33);
            this.rb_fechado.Name = "rb_fechado";
            this.rb_fechado.Size = new System.Drawing.Size(66, 17);
            this.rb_fechado.TabIndex = 11;
            this.rb_fechado.TabStop = true;
            this.rb_fechado.Text = "Fechado";
            this.rb_fechado.UseVisualStyleBackColor = true;
            // 
            // bt_limpar
            // 
            this.bt_limpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_limpar.Location = new System.Drawing.Point(83, 115);
            this.bt_limpar.Name = "bt_limpar";
            this.bt_limpar.Size = new System.Drawing.Size(71, 22);
            this.bt_limpar.TabIndex = 12;
            this.bt_limpar.Text = "Limpar";
            this.bt_limpar.UseVisualStyleBackColor = true;
            this.bt_limpar.Click += new System.EventHandler(this.bt_limpar_Click);
            // 
            // bt_pesquisar
            // 
            this.bt_pesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_pesquisar.Location = new System.Drawing.Point(6, 115);
            this.bt_pesquisar.Name = "bt_pesquisar";
            this.bt_pesquisar.Size = new System.Drawing.Size(71, 22);
            this.bt_pesquisar.TabIndex = 1;
            this.bt_pesquisar.Text = "Pesquisar";
            this.bt_pesquisar.UseVisualStyleBackColor = true;
            this.bt_pesquisar.Click += new System.EventHandler(this.bt_pesquisar_Click);
            // 
            // rb_todos
            // 
            this.rb_todos.AutoSize = true;
            this.rb_todos.Checked = true;
            this.rb_todos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rb_todos.Location = new System.Drawing.Point(96, 15);
            this.rb_todos.Name = "rb_todos";
            this.rb_todos.Size = new System.Drawing.Size(54, 17);
            this.rb_todos.TabIndex = 10;
            this.rb_todos.TabStop = true;
            this.rb_todos.Text = "Todos";
            this.rb_todos.UseVisualStyleBackColor = true;
            // 
            // rb_construcao
            // 
            this.rb_construcao.AutoSize = true;
            this.rb_construcao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rb_construcao.Location = new System.Drawing.Point(7, 33);
            this.rb_construcao.Name = "rb_construcao";
            this.rb_construcao.Size = new System.Drawing.Size(78, 17);
            this.rb_construcao.TabIndex = 9;
            this.rb_construcao.TabStop = true;
            this.rb_construcao.Text = "Construção";
            this.rb_construcao.UseVisualStyleBackColor = true;
            // 
            // btn_clienteLocalizar
            // 
            this.btn_clienteLocalizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_clienteLocalizar.Location = new System.Drawing.Point(480, 30);
            this.btn_clienteLocalizar.Name = "btn_clienteLocalizar";
            this.btn_clienteLocalizar.Size = new System.Drawing.Size(63, 22);
            this.btn_clienteLocalizar.TabIndex = 2;
            this.btn_clienteLocalizar.Text = "Cliente";
            this.btn_clienteLocalizar.UseVisualStyleBackColor = true;
            this.btn_clienteLocalizar.Click += new System.EventHandler(this.btn_clienteLocalizar_Click);
            // 
            // ck_dataCriacao
            // 
            this.ck_dataCriacao.AutoSize = true;
            this.ck_dataCriacao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ck_dataCriacao.Location = new System.Drawing.Point(15, 16);
            this.ck_dataCriacao.Name = "ck_dataCriacao";
            this.ck_dataCriacao.Size = new System.Drawing.Size(12, 11);
            this.ck_dataCriacao.TabIndex = 3;
            this.ck_dataCriacao.UseVisualStyleBackColor = true;
            // 
            // grb_situacao
            // 
            this.grb_situacao.Controls.Add(this.rb_fechado);
            this.grb_situacao.Controls.Add(this.rb_todos);
            this.grb_situacao.Controls.Add(this.rb_construcao);
            this.grb_situacao.Controls.Add(this.rb_aguardando);
            this.grb_situacao.Location = new System.Drawing.Point(585, 58);
            this.grb_situacao.Name = "grb_situacao";
            this.grb_situacao.Size = new System.Drawing.Size(179, 61);
            this.grb_situacao.TabIndex = 100;
            this.grb_situacao.TabStop = false;
            this.grb_situacao.Text = "Situação";
            // 
            // rb_aguardando
            // 
            this.rb_aguardando.AutoSize = true;
            this.rb_aguardando.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rb_aguardando.Location = new System.Drawing.Point(7, 15);
            this.rb_aguardando.Name = "rb_aguardando";
            this.rb_aguardando.Size = new System.Drawing.Size(82, 17);
            this.rb_aguardando.TabIndex = 8;
            this.rb_aguardando.TabStop = true;
            this.rb_aguardando.Text = "Aguardando";
            this.rb_aguardando.UseVisualStyleBackColor = true;
            // 
            // grb_data
            // 
            this.grb_data.Controls.Add(this.ck_dataCriacao);
            this.grb_data.Controls.Add(this.dtCriacao_final);
            this.grb_data.Controls.Add(this.dtCriacao_inicial);
            this.grb_data.Location = new System.Drawing.Point(549, 16);
            this.grb_data.Name = "grb_data";
            this.grb_data.Size = new System.Drawing.Size(215, 36);
            this.grb_data.TabIndex = 100;
            this.grb_data.TabStop = false;
            this.grb_data.Text = "Data de Elaboração";
            // 
            // dtCriacao_final
            // 
            this.dtCriacao_final.Enabled = false;
            this.dtCriacao_final.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtCriacao_final.Location = new System.Drawing.Point(120, 13);
            this.dtCriacao_final.Name = "dtCriacao_final";
            this.dtCriacao_final.Size = new System.Drawing.Size(81, 20);
            this.dtCriacao_final.TabIndex = 5;
            // 
            // dtCriacao_inicial
            // 
            this.dtCriacao_inicial.Enabled = false;
            this.dtCriacao_inicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtCriacao_inicial.Location = new System.Drawing.Point(33, 13);
            this.dtCriacao_inicial.Name = "dtCriacao_inicial";
            this.dtCriacao_inicial.Size = new System.Drawing.Size(81, 20);
            this.dtCriacao_inicial.TabIndex = 4;
            this.dtCriacao_inicial.Value = new System.DateTime(2011, 10, 27, 21, 57, 46, 0);
            // 
            // text_cliente
            // 
            this.text_cliente.BackColor = System.Drawing.SystemColors.Info;
            this.text_cliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_cliente.Location = new System.Drawing.Point(148, 32);
            this.text_cliente.MaxLength = 100;
            this.text_cliente.Name = "text_cliente";
            this.text_cliente.ReadOnly = true;
            this.text_cliente.Size = new System.Drawing.Size(326, 20);
            this.text_cliente.TabIndex = 100;
            this.text_cliente.TabStop = false;
            // 
            // lbl_codPcmso
            // 
            this.lbl_codPcmso.AutoSize = true;
            this.lbl_codPcmso.Location = new System.Drawing.Point(6, 16);
            this.lbl_codPcmso.Name = "lbl_codPcmso";
            this.lbl_codPcmso.Size = new System.Drawing.Size(96, 13);
            this.lbl_codPcmso.TabIndex = 100;
            this.lbl_codPcmso.Text = "Código do PCMSO";
            // 
            // grb_filtro
            // 
            this.grb_filtro.BackColor = System.Drawing.Color.White;
            this.grb_filtro.Controls.Add(this.grb_engenheiro);
            this.grb_filtro.Controls.Add(this.grb_tecnico);
            this.grb_filtro.Controls.Add(this.btn_clienteLocalizar);
            this.grb_filtro.Controls.Add(this.bt_limpar);
            this.grb_filtro.Controls.Add(this.bt_pesquisar);
            this.grb_filtro.Controls.Add(this.grb_situacao);
            this.grb_filtro.Controls.Add(this.grb_data);
            this.grb_filtro.Controls.Add(this.text_codPpra);
            this.grb_filtro.Controls.Add(this.text_cliente);
            this.grb_filtro.Controls.Add(this.lbl_cliente);
            this.grb_filtro.Controls.Add(this.lbl_codPcmso);
            this.grb_filtro.Location = new System.Drawing.Point(3, 3);
            this.grb_filtro.Name = "grb_filtro";
            this.grb_filtro.Size = new System.Drawing.Size(776, 148);
            this.grb_filtro.TabIndex = 2;
            this.grb_filtro.TabStop = false;
            this.grb_filtro.Text = "Filtro";
            // 
            // lbl_cliente
            // 
            this.lbl_cliente.AutoSize = true;
            this.lbl_cliente.Location = new System.Drawing.Point(145, 16);
            this.lbl_cliente.Name = "lbl_cliente";
            this.lbl_cliente.Size = new System.Drawing.Size(39, 13);
            this.lbl_cliente.TabIndex = 100;
            this.lbl_cliente.Text = "Cliente";
            // 
            // grb_gride
            // 
            this.grb_gride.BackColor = System.Drawing.Color.White;
            this.grb_gride.Controls.Add(this.grd_pcmso);
            this.grb_gride.Location = new System.Drawing.Point(3, 157);
            this.grb_gride.Name = "grb_gride";
            this.grb_gride.Size = new System.Drawing.Size(781, 301);
            this.grb_gride.TabIndex = 101;
            this.grb_gride.TabStop = false;
            this.grb_gride.Text = "PCMSOs";
            // 
            // grd_pcmso
            // 
            this.grd_pcmso.AllowUserToAddRows = false;
            this.grd_pcmso.AllowUserToDeleteRows = false;
            this.grd_pcmso.AllowUserToOrderColumns = true;
            this.grd_pcmso.AllowUserToResizeRows = false;
            this.grd_pcmso.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grd_pcmso.BackgroundColor = System.Drawing.Color.White;
            this.grd_pcmso.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_pcmso.Location = new System.Drawing.Point(6, 14);
            this.grd_pcmso.MultiSelect = false;
            this.grd_pcmso.Name = "grd_pcmso";
            this.grd_pcmso.ReadOnly = true;
            this.grd_pcmso.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grd_pcmso.Size = new System.Drawing.Size(769, 281);
            this.grd_pcmso.TabIndex = 12;
            // 
            // btn_corrigir
            // 
            this.btn_corrigir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_corrigir.Location = new System.Drawing.Point(545, 17);
            this.btn_corrigir.Name = "btn_corrigir";
            this.btn_corrigir.Size = new System.Drawing.Size(71, 22);
            this.btn_corrigir.TabIndex = 101;
            this.btn_corrigir.Text = "&Corrigir";
            this.btn_corrigir.UseVisualStyleBackColor = true;
            // 
            // btn_revisao
            // 
            this.btn_revisao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_revisao.Location = new System.Drawing.Point(468, 17);
            this.btn_revisao.Name = "btn_revisao";
            this.btn_revisao.Size = new System.Drawing.Size(71, 22);
            this.btn_revisao.TabIndex = 19;
            this.btn_revisao.Text = "&Revisão";
            this.btn_revisao.UseVisualStyleBackColor = true;
            this.btn_revisao.Click += new System.EventHandler(this.btn_revisao_Click);
            // 
            // btn_finalizar
            // 
            this.btn_finalizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_finalizar.Location = new System.Drawing.Point(391, 17);
            this.btn_finalizar.Name = "btn_finalizar";
            this.btn_finalizar.Size = new System.Drawing.Size(71, 22);
            this.btn_finalizar.TabIndex = 18;
            this.btn_finalizar.Text = "&Finalizar";
            this.btn_finalizar.UseVisualStyleBackColor = true;
            this.btn_finalizar.Click += new System.EventHandler(this.btn_finalizar_Click);
            // 
            // btn_enviarAnalise
            // 
            this.btn_enviarAnalise.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_enviarAnalise.Location = new System.Drawing.Point(314, 17);
            this.btn_enviarAnalise.Name = "btn_enviarAnalise";
            this.btn_enviarAnalise.Size = new System.Drawing.Size(71, 22);
            this.btn_enviarAnalise.TabIndex = 17;
            this.btn_enviarAnalise.Text = "&Enviar";
            this.btn_enviarAnalise.UseVisualStyleBackColor = true;
            this.btn_enviarAnalise.Click += new System.EventHandler(this.btn_enviarAnalise_Click);
            // 
            // grb_paginacao
            // 
            this.grb_paginacao.BackColor = System.Drawing.Color.White;
            this.grb_paginacao.Controls.Add(this.btn_corrigir);
            this.grb_paginacao.Controls.Add(this.btn_revisao);
            this.grb_paginacao.Controls.Add(this.btn_finalizar);
            this.grb_paginacao.Controls.Add(this.btn_enviarAnalise);
            this.grb_paginacao.Controls.Add(this.bt_imprimir);
            this.grb_paginacao.Controls.Add(this.bt_fechar);
            this.grb_paginacao.Controls.Add(this.bt_incluir);
            this.grb_paginacao.Controls.Add(this.bt_excluir);
            this.grb_paginacao.Controls.Add(this.bt_alterar);
            this.grb_paginacao.Location = new System.Drawing.Point(3, 464);
            this.grb_paginacao.Name = "grb_paginacao";
            this.grb_paginacao.Size = new System.Drawing.Size(781, 51);
            this.grb_paginacao.TabIndex = 102;
            this.grb_paginacao.TabStop = false;
            // 
            // bt_imprimir
            // 
            this.bt_imprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_imprimir.Location = new System.Drawing.Point(237, 17);
            this.bt_imprimir.Name = "bt_imprimir";
            this.bt_imprimir.Size = new System.Drawing.Size(71, 22);
            this.bt_imprimir.TabIndex = 16;
            this.bt_imprimir.Text = "Imprimir";
            this.bt_imprimir.UseVisualStyleBackColor = true;
            this.bt_imprimir.Click += new System.EventHandler(this.bt_imprimir_Click);
            // 
            // bt_fechar
            // 
            this.bt_fechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_fechar.Location = new System.Drawing.Point(622, 17);
            this.bt_fechar.Name = "bt_fechar";
            this.bt_fechar.Size = new System.Drawing.Size(71, 22);
            this.bt_fechar.TabIndex = 20;
            this.bt_fechar.Text = "&Fechar";
            this.bt_fechar.UseVisualStyleBackColor = true;
            this.bt_fechar.Click += new System.EventHandler(this.bt_fechar_Click);
            // 
            // bt_incluir
            // 
            this.bt_incluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_incluir.Location = new System.Drawing.Point(6, 17);
            this.bt_incluir.Name = "bt_incluir";
            this.bt_incluir.Size = new System.Drawing.Size(71, 22);
            this.bt_incluir.TabIndex = 13;
            this.bt_incluir.Text = "&Incluir";
            this.bt_incluir.UseVisualStyleBackColor = true;
            this.bt_incluir.Click += new System.EventHandler(this.bt_incluir_Click);
            // 
            // bt_excluir
            // 
            this.bt_excluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_excluir.Location = new System.Drawing.Point(160, 17);
            this.bt_excluir.Name = "bt_excluir";
            this.bt_excluir.Size = new System.Drawing.Size(71, 22);
            this.bt_excluir.TabIndex = 15;
            this.bt_excluir.Text = "&Excluir";
            this.bt_excluir.UseVisualStyleBackColor = true;
            this.bt_excluir.Click += new System.EventHandler(this.bt_excluir_Click);
            // 
            // bt_alterar
            // 
            this.bt_alterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_alterar.Location = new System.Drawing.Point(83, 17);
            this.bt_alterar.Name = "bt_alterar";
            this.bt_alterar.Size = new System.Drawing.Size(71, 22);
            this.bt_alterar.TabIndex = 14;
            this.bt_alterar.Text = "&Alterar";
            this.bt_alterar.UseVisualStyleBackColor = true;
            this.bt_alterar.Click += new System.EventHandler(this.bt_alterar_Click);
            // 
            // pcmsoErrorProvider
            // 
            this.pcmsoErrorProvider.ContainerControl = this;
            // 
            // frm_PcmsoSemPpraPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 568);
            this.Name = "frm_PcmsoSemPpraPrincipal";
            this.Text = "PESQUISA PCMSO";
            this.panel.ResumeLayout(false);
            this.grb_engenheiro.ResumeLayout(false);
            this.grb_engenheiro.PerformLayout();
            this.grb_tecnico.ResumeLayout(false);
            this.grb_tecnico.PerformLayout();
            this.grb_situacao.ResumeLayout(false);
            this.grb_situacao.PerformLayout();
            this.grb_data.ResumeLayout(false);
            this.grb_data.PerformLayout();
            this.grb_filtro.ResumeLayout(false);
            this.grb_filtro.PerformLayout();
            this.grb_gride.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_pcmso)).EndInit();
            this.grb_paginacao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pcmsoErrorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grb_filtro;
        private System.Windows.Forms.GroupBox grb_engenheiro;
        public System.Windows.Forms.TextBox text_engenheiro;
        private System.Windows.Forms.Button bt_engenheiro;
        private System.Windows.Forms.GroupBox grb_tecnico;
        public System.Windows.Forms.TextBox text_tecno;
        private System.Windows.Forms.Button bt_tecnico;
        private System.Windows.Forms.Button btn_clienteLocalizar;
        private System.Windows.Forms.Button bt_limpar;
        private System.Windows.Forms.Button bt_pesquisar;
        private System.Windows.Forms.GroupBox grb_situacao;
        private System.Windows.Forms.RadioButton rb_fechado;
        private System.Windows.Forms.RadioButton rb_todos;
        private System.Windows.Forms.RadioButton rb_construcao;
        private System.Windows.Forms.RadioButton rb_aguardando;
        private System.Windows.Forms.GroupBox grb_data;
        private System.Windows.Forms.CheckBox ck_dataCriacao;
        private System.Windows.Forms.DateTimePicker dtCriacao_final;
        private System.Windows.Forms.DateTimePicker dtCriacao_inicial;
        private System.Windows.Forms.MaskedTextBox text_codPpra;
        public System.Windows.Forms.TextBox text_cliente;
        private System.Windows.Forms.Label lbl_cliente;
        private System.Windows.Forms.Label lbl_codPcmso;
        private System.Windows.Forms.GroupBox grb_gride;
        private System.Windows.Forms.DataGridView grd_pcmso;
        private System.Windows.Forms.GroupBox grb_paginacao;
        private System.Windows.Forms.Button btn_corrigir;
        private System.Windows.Forms.Button btn_revisao;
        private System.Windows.Forms.Button btn_finalizar;
        private System.Windows.Forms.Button btn_enviarAnalise;
        private System.Windows.Forms.Button bt_imprimir;
        private System.Windows.Forms.Button bt_fechar;
        private System.Windows.Forms.Button bt_incluir;
        private System.Windows.Forms.Button bt_excluir;
        private System.Windows.Forms.Button bt_alterar;
        private System.Windows.Forms.ErrorProvider pcmsoErrorProvider;
    }
}