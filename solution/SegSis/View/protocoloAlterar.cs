﻿using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmprotocoloAlterar : frmProtocoloIncluir
    {
        public frmprotocoloAlterar(Protocolo protocolo) :base()
        {
            InitializeComponent();
            this.Protocolo = protocolo;
            textCodigo.Text = protocolo.Numero;
            textDataGravacao.Text = Convert.ToDateTime(protocolo.DataGravacao).ToShortDateString();
            this.cliente = protocolo.Cliente;
            this.textCliente.Text = protocolo.Cliente.RazaoSocial;
            this.montaDataGrid();
            this.btn_novo.Enabled = false;
        }
    }
}
