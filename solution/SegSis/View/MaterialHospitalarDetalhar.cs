﻿using SWS.Entidade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SWS.View
{
    public partial class frmMaterialHospitalarDetalhar : frmMaterialHospitalarIncluir
    {
        public frmMaterialHospitalarDetalhar(MaterialHospitalar materialHospitalar) :base()
        {
            InitializeComponent();

            textNome.Text = materialHospitalar.Descricao;
            textUnidade.Text = materialHospitalar.Unidade;
            btnGravar.Enabled = false;
            btnLimpar.Enabled = false;
        }
    }
}
