﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Entidade;
using SWS.Facade;

namespace SWS.View
{
    public partial class frmClienteCentroCustoSelecionar : frmTemplateConsulta
    {
        private Cliente cliente;

        private CentroCusto centroCustoSelecionado;

        public CentroCusto CentroCustoSelecionado
        {
            get { return centroCustoSelecionado; }
            set { centroCustoSelecionado = value; }
        }

        List<CentroCusto> centroCustoCliente = new List<CentroCusto>();

        public frmClienteCentroCustoSelecionar(Cliente cliente)
        {
            InitializeComponent();
            this.cliente = cliente;
            montaGrid();
        }

        public void montaGrid()
        {
            try
            {
                dgvCentroCusto.Columns.Clear();

                dgvCentroCusto.ColumnCount = 2;

                dgvCentroCusto.Columns[0].HeaderText = "id";
                dgvCentroCusto.Columns[0].Visible = false;

                dgvCentroCusto.Columns[1].HeaderText = "Centro Custo";

                ClienteFacade ClienteFacade = ClienteFacade.getInstance();

                centroCustoCliente = ClienteFacade.findCentroCustoAtivoByCliente(cliente);

                centroCustoCliente.ForEach(delegate(CentroCusto centroCusto)
                {
                    dgvCentroCusto.Rows.Add(centroCusto.Id, centroCusto.Descricao);
                });


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSelecionar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCentroCusto.CurrentRow == null)
                    throw new Exception("Selecione uma linha");

                centroCustoSelecionado = centroCustoCliente.Find(x => x.Id == ((long)dgvCentroCusto.CurrentRow.Cells[0].Value));

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvCentroCusto_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btnSelecionar.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

    }
}
