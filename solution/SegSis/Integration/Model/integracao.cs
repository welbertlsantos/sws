﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SWS.Integration.Model
{
    public class ContaMedicaField
    {
        public string codigoExternoContaMedica { get; set; }
        public string codigoExternoPaciente { get; set; }
        public string origemPaciente { get; set; }
        public string tipoAtendimento { get; set; }
        public string nome { get; set; }
        public string cpf { get; set; }
        public string dataNascimento { get; set; }
        public string sexo { get; set; }
        public string idade { get; set; }
        public string nomeMedico { get; set; }
        public string crm { get; set; }
        public string registroANS { get; set; }
        public string convenio { get; set; }
        public string numeroConvenio { get; set; }
        public string numeroPlano { get; set; }
        public string nomePlano { get; set; }
        public string guia { get; set; }
        public string matricula { get; set; }
        public List<Exames> exames { get; set; }
    }

    public class Exames
    {
        public string codigoExternoMovimento { get; set; }
        public string codigoExternoExame { get; set; }
        public string codigoTUSS { get; set; }
        public string nomeExame { get; set; }
    }
}
