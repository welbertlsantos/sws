﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace SWS.Modelo
{
    public class modelo3000
    {
        [XmlRoot(ElementName = "infoExclusao")]
        public class InfoExclusao
        {
            [XmlElement(ElementName = "tpEvento")]
            public string tpEvento { get; set; }
            [XmlElement(ElementName = "nrRecEvt")]
            public string nrRecEvt { get; set; }
            [XmlElement(ElementName = "ideTrabalhador")]
            public IdeTrabalhador ideTrabalhador { get; set; }
        }
        
        [XmlRoot(ElementName = "ideEmpregador")]
        public class IdeEmpregador
        {
            [XmlElement(ElementName = "tpInsc")]
            public string TpInsc { get; set; }
            [XmlElement(ElementName = "nrInsc")]
            public string NrInsc { get; set; }
        }

        [XmlRoot(ElementName = "ideTrabalhador")]
        public class IdeTrabalhador
        {
            [XmlElement(ElementName = "cpfTrab")]
            public string cpfTrab { get; set; }
        }

        [XmlRoot(ElementName = "ideEvento")]
        public class IdeEvento
        {
            [XmlElement(ElementName = "tpAmb")]
            public string tpAmb { get; set; }
            [XmlElement(ElementName = "procEmi")]
            public string procEmi { get; set; }
            [XmlElement(ElementName = "verProc")]
            public string verProc { get; set; }
        }

        [XmlRoot(ElementName = "evtExclusao")]
        public class EvtExclusao
        {
            [XmlAttribute(AttributeName = "Id")]
            public string Id { get; set; }
            [XmlElement(ElementName = "ideEvento")]
            public IdeEvento ideEvento { get; set; }
            [XmlElement(ElementName = "ideEmpregador")]
            public IdeEmpregador ideEmpregador { get; set; }
            [XmlElement(ElementName = "infoExclusao")]
            public InfoExclusao infoExclusao { get; set; }
        }

        [XmlRoot(ElementName = "eSocial", Namespace = "http://www.esocial.gov.br/schema/evt/evtExclusao/v_S_01_02_00")]
        public class ESocial
        {
            [XmlElement(ElementName = "evtExclusao")]
            public EvtExclusao evtExclusao { get; set; }
        }
    }
}


