﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace SWS.Esocial
{
    public class modelo2240
    {
        [XmlRoot(ElementName = "ideEmpregador")]
        public class IdeEmpregador
        {
            [XmlElement(ElementName = "tpInsc")]
            public string tpInsc { get; set; }
            [XmlElement(ElementName = "nrInsc")]
            public string nrInsc { get; set; }
        }

        [XmlRoot(ElementName = "ideVinculo")]
        public class IdeVinculo
        {
            [XmlElement(ElementName = "cpfTrab")]
            public string cpfTrab { get; set; }
            [XmlElement(ElementName = "matricula")]
            public string matricula { get; set; }
            [XmlElement(ElementName = "codCateg")]
            public string codCateg { get; set; }
        }

        [XmlRoot(ElementName = "respReg")]
        public class RespReg
        {
            [XmlElement(ElementName = "cpfResp")]
            public string cpfResp { get; set; }
            [XmlElement(ElementName = "ideOC")]
            public string ideOC { get; set; }
            [XmlElement(ElementName = "dscOC")]
            public string dscOC { get; set; }
            [XmlElement(ElementName = "nrOC")]
            public string nrOC { get; set; }
            [XmlElement(ElementName = "ufOC")]
            public string ufOC { get; set; }
        }

        [XmlRoot(ElementName = "ideEvento")]
        public class IdeEvento
        {
            [XmlElement(ElementName = "indRetif")]
            public string indRetif { get; set; }
            [XmlElement(ElementName = "nrRecibo")]
            public string nrRecibo { get; set; }
            [XmlElement(ElementName = "tpAmb")]
            public string tpAmb { get; set; }
            [XmlElement(ElementName = "procEmi")]
            public string procEmi { get; set; }
            [XmlElement(ElementName = "verProc")]
            public string verProc { get; set; }
        }

        [XmlRoot(ElementName = "infoAmb")]
        public class InfoAmb
        {
            [XmlElement(ElementName = "localAmb")]
            public string localAmb { get; set; }
            [XmlElement(ElementName = "dscSetor")]
            public string dscSetor { get; set; }
            [XmlElement(ElementName = "tpInsc")]
            public string tpInsc { get; set; }
            [XmlElement(ElementName = "nrInsc")]
            public string nrInsc { get; set; }
        }

        [XmlRoot(ElementName = "infoAtiv")]
        public class InfoAtiv
        {
            [XmlElement(ElementName = "dscAtivDes")]
            public string dscAtivDes { get; set; }
        }

        [XmlRoot(ElementName = "epiCompl")]
        public class EpiCompl
        {
            [XmlElement(ElementName = "medProtecao")]
            public string medProtecao { get; set; }
            [XmlElement(ElementName = "condFuncto")]
            public string condFuncto { get; set; }
            [XmlElement(ElementName = "usoInint")]
            public string usoInint { get; set; }
            [XmlElement(ElementName = "przValid")]
            public string przValid { get; set; }
            [XmlElement(ElementName = "periodicTroca")]
            public string periodicTroca { get; set; }
            [XmlElement(ElementName = "higienizacao")]
            public string higienizacao { get; set; }

        }

        [XmlRoot(ElementName = "obs")]
        public class Obs
        {
            [XmlElement(ElementName = "obsCompl")]
            public string obsCompl { get; set; }
        }

        [XmlRoot(ElementName = "epi")]
        public class Epi
        {
            [XmlElement(ElementName = "docAval")]
            public string docAval { get; set; }
        }

        [XmlRoot(ElementName = "epcEpi")]
        public class EpcEpi
        {
            [XmlElement(ElementName = "utilizEPC")]
            public string utilizEPC { get; set; }
            [XmlElement(ElementName = "eficEpc")]
            public string eficEpc { get; set; }
            [XmlElement(ElementName = "utilizEPI")]
            public string utilizEPI { get; set; }
            [XmlElement(ElementName = "eficEpi")]
            public string eficEpi { get; set; }
            [XmlElement(ElementName = "epi")]
            public List<Epi> epi { get; set; }
            [XmlElement(ElementName = "epiCompl")]
            public EpiCompl epiCompl { get; set; }

        }

        [XmlRoot(ElementName = "agNoc")]
        public class AgNoc
        {
            [XmlElement(ElementName = "codAgNoc")]
            public string codAgNoc { get; set; }
            
            [XmlElement(ElementName = "dscAgNoc")]
            public string dscAgNoc { get; set; }
            
            [XmlElement(ElementName = "tpAval")]
            public string tpAval { get; set; }
            
            [XmlElement(ElementName = "intConc")]
            public string intConc { get; set; }
            
            [XmlElement(ElementName = "limTol")]
            public string limTol { get; set; }
            
            [XmlElement(ElementName = "unMed")]
            public string unMed { get; set; }
            
            [XmlElement(ElementName = "tecMedicao")]
            public string tecMedicao { get; set; }
            
            [XmlElement(ElementName = "nrProcJud")]
            public string nrProcJud { get; set; }

            [XmlElement(ElementName = "epcEpi")]
            public EpcEpi epcEpi { get; set; }
            

        }

        [XmlRoot(ElementName = "infoExpRisco")]
        public class InfoExpRisco
        {
            [XmlElement(ElementName = "dtIniCondicao")]
            public string dtIniCondicao { get; set; }
            [XmlElement(ElementName = "dtFimCondicao")]
            public string dtFimCondicao { get; set; }
            [XmlElement(ElementName = "infoAmb")]
            public InfoAmb infoAmb { get; set; }
            [XmlElement(ElementName = "infoAtiv")]
            public InfoAtiv infoAtiv { get; set; }
            [XmlElement(ElementName = "agNoc")]
            public List<AgNoc> agNoc { get; set; }
            [XmlElement(ElementName = "respReg")]
            public RespReg respReg { get; set; }
            [XmlElement(ElementName = "obs")]
            public Obs obs { get; set; }
        }

        [XmlRoot(ElementName = "evtExpRisco")]
        public class EvtExpRisco
        {
            [XmlAttribute(AttributeName = "Id")]
            public string Id { get; set; }
            [XmlElement(ElementName = "ideEvento")]
            public IdeEvento ideEvento { get; set; }
            [XmlElement(ElementName = "ideEmpregador")]
            public IdeEmpregador ideEmpregador { get; set; }
            [XmlElement(ElementName = "ideVinculo")]
            public IdeVinculo ideVinculo { get; set; }
            [XmlElement(ElementName = "infoExpRisco")]
            public InfoExpRisco infoExpRisco { get; set; }
        }

        [XmlRoot(ElementName = "eSocial", Namespace = "http://www.esocial.gov.br/schema/evt/evtExpRisco/v_S_01_02_00")]
        public class ESocial
        {
            [XmlElement(ElementName = "evtExpRisco")]
            public EvtExpRisco evtExpRisco { get; set; }
        }



    }
}
