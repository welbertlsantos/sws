﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Documento
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private String descricao;

        public String Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }

        public Documento() { }

        public Documento(Int64? id, String descricao, String situacao)
        {
            this.Id = id;
            this.Descricao  = descricao;
            this.Situacao = situacao;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Documento p = obj as Documento;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

        public Boolean isNullForFilter()
        {
            Boolean retorno = false;

            if (String.IsNullOrEmpty(this.descricao) && String.IsNullOrEmpty(situacao))
            {
                retorno = true;

            }

            return retorno;
        }

    }
}
