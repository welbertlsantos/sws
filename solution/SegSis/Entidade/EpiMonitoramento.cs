﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class EpiMonitoramento
    {
        private long? id;

        public long? Id
        {
            get { return id; }
            set { id = value; }
        }

        private Epi epi;

        public Epi Epi
        {
            get { return epi; }
            set { epi = value; }
        }

        private MonitoramentoGheFonteAgente monitoramentoGheFonteAgente;

        public MonitoramentoGheFonteAgente MonitoramentoGheFonteAgente
        {
            get { return monitoramentoGheFonteAgente; }
            set { monitoramentoGheFonteAgente = value; }
        }

        public EpiMonitoramento(long? id, Epi epi, MonitoramentoGheFonteAgente monitoramentoGheFonteAgente)
        {
            this.Id = id;
            this.Epi = epi;
            this.MonitoramentoGheFonteAgente = monitoramentoGheFonteAgente;
        }

        public EpiMonitoramento() { }

        public EpiMonitoramento copy()
        {
            EpiMonitoramento epiMonitoramentoCopy = (EpiMonitoramento)this.MemberwiseClone();
            return epiMonitoramentoCopy;
        }
    }

}
