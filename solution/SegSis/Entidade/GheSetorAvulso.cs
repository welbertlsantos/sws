﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class GheSetorAvulso
    {
        private string ghe;

        public string Ghe
        {
            get { return ghe; }
            set { ghe = value; }
        }

        private string setor;

        public string Setor
        {
            get { return setor; }
            set { setor = value; }
        }

        public GheSetorAvulso(string ghe, string setor)
        {
            this.Ghe = ghe;
            this.Setor = setor;
        }

    }
}
