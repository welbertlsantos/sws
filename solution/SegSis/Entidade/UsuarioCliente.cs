﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class UsuarioCliente
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }

        private Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }

        private Usuario usuario;

        public Usuario Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }

        private DateTime? dataInclusao;

        public DateTime? DataInclusao
        {
            get { return dataInclusao; }
            set { dataInclusao = value; }
        }

        public UsuarioCliente() { }

        public UsuarioCliente(Int64? id, Cliente cliente, Usuario usuario, DateTime? dataInclusao)
        {
            this.Id = id;
            this.Cliente = cliente;
            this.Usuario = usuario;
            this.DataInclusao = dataInclusao;
        }
    }
}
