﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class ContratoProduto
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private Produto produto;

        public Produto Produto
        {
            get { return produto; }
            set { produto = value; }
        }
        private Contrato contrato;

        public Contrato Contrato
        {
            get { return contrato; }
            set { contrato = value; }
        }
        private Decimal? precoContratado;

        public Decimal? PrecoContratado
        {
            get { return precoContratado; }
            set { precoContratado = value; }
        }
        private Usuario usuario;

        public Usuario Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }
        private Decimal? custoContrato;

        public Decimal? CustoContrato
        {
            get { return custoContrato; }
            set { custoContrato = value; }
        }
        private DateTime? dataInclusao;

        public DateTime? DataInclusao
        {
            get { return dataInclusao; }
            set { dataInclusao = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        private Boolean altera;

        public Boolean Altera
        {
            get { return altera; }
            set { altera = value; }
        }
        
        public ContratoProduto() { }

        
        public ContratoProduto(Int64? id, Produto produto, Contrato contrato, Decimal? precoContratado,
            Usuario usuario, Decimal? custoContrato,
            DateTime? dataInclusao,
            String situacao, Boolean altera)
        {
            this.Id = id;
            this.Produto = produto;
            this.Contrato = contrato;
            this.PrecoContratado = precoContratado;
            this.Usuario = usuario;
            this.CustoContrato = custoContrato;
            this.DataInclusao = dataInclusao;
            this.Situacao = situacao;
            this.Altera = altera;
        }
        
        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            ContratoProduto  p = obj as ContratoProduto;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

    }
}
