﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class LoteEsocialAso
    {
        public long? Id { get; set; }
        public long? idAso { get; set; }
        public long? idLoteEsocialAso { get; set; }
        public string situacao { get; set; }
        public string nrebido { get; set; }
        public DateTime? dataCancelamento { get; set; }
        public string usuarioCancelou { get; set; }
        public Aso aso { get; set; }
        public LoteEsocial loteEsocial { get; set; }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            LoteEsocialAso p = obj as LoteEsocialAso;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.Id == p.Id);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }


    }
}
