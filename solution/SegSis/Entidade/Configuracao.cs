﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Configuracao
    {
        private long? id;

        public long? Id
        {
            get { return id; }
            set { id = value; }
        }
        private string descricao;

        public string Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }
        private string valor;

        public string Valor
        {
            get { return valor; }
            set { valor = value; }
        }
        
        public Configuracao() { }

        public Configuracao(Int64? id, string descricao, string valor)
        {
            this.Id = id;
            this.Descricao = descricao;
            this.Valor = valor;
            
        }

    
    }
}
