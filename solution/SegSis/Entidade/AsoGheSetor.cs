﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    class AsoGheSetor
    {
        private Int64? idAso;

        public Int64? IdAso
        {
            get { return idAso; }
            set { idAso = value; }
        }
        private Int64? idGhe;

        public Int64? IdGhe
        {
            get { return idGhe; }
            set { idGhe = value; }
        }
        private String descricaoGhe;

        public String DescricaoGhe
        {
            get { return descricaoGhe; }
            set { descricaoGhe = value; }
        }
        private Int64? idSetor;

        public Int64? IdSetor
        {
            get { return idSetor; }
            set { idSetor = value; }
        }
        private String descricaoSetor;

        public String DescricaoSetor
        {
            get { return descricaoSetor; }
            set { descricaoSetor = value; }
        }

        private string numeroPo;

        public string NumeroPo
        {
            get { return numeroPo; }
            set { numeroPo = value; }
        }

        public AsoGheSetor() { }

        public AsoGheSetor(Int64? idAso, Int64? idGhe, String descricaoGhe, Int64? idSetor, String descricaoSetor, string numeroPo)
        {
            this.IdAso = idAso;
            this.idGhe = idGhe;
            this.DescricaoGhe = descricaoGhe;
            this.idSetor = idSetor;
            this.DescricaoSetor = descricaoSetor;
            this.NumeroPo = numeroPo;
        }

        
        
    }
}
