﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS.Entidade
{
    public class DocumentoEstudo
    {
        private long id;

        public long Id
        {
            get { return id; }
            set { id = value; }
        }
        private Documento documento;

        public Documento Documento
        {
            get { return documento; }
            set { documento = value; }
        }
        private Estudo estudo;

        public Estudo Estudo
        {
            get { return estudo; }
            set { estudo = value; }
        }

        public DocumentoEstudo() { }


    }
}
