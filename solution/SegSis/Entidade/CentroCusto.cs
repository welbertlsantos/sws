﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class CentroCusto
    {
        private long? id;

        public long? Id
        {
            get { return id; }
            set { id = value; }
        }
        private Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }
        private string descricao;

        public string Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }
        private string situacao;

        public string Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }

        public CentroCusto() { }

        public CentroCusto(long? id) { this.Id = id; }

        public CentroCusto(long? id, Cliente cliente, string descricao, string situacao)
        {
            this.Id = id;
            this.Cliente = cliente;
            this.Descricao = descricao;
            this.Situacao = situacao;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            CentroCusto p = obj as CentroCusto;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.Id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
    }
}