﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class UsuarioPerfil
    {
        private Usuario usuario;

        public Usuario Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }
        private Perfil perfil;

        public Perfil Perfil
        {
            get { return perfil; }
            set { perfil = value; }
        }
        private DateTime dataInicio;

        public DateTime DataInicio
        {
            get { return dataInicio; }
            set { dataInicio = value; }
        }

        public UsuarioPerfil(Usuario usuario, Perfil perfil, DateTime dataInicio)
        {
            this.Usuario = usuario;
            this.Perfil = perfil;
            this.DataInicio = dataInicio;
        }

        public UsuarioPerfil() { }
    }
}
