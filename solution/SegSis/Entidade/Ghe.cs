﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using SWS.View;
using System.Windows.Forms;


namespace SWS.Entidade
{
    public class Ghe
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private Estudo estudo;

        public Estudo Estudo
        {
            get { return estudo; }
            set { estudo = value; }
        }
        private String descricao;

        public String Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }
        private Int32 nexp;

        public Int32 Nexp
        {
            get { return nexp; }
            set { nexp = value; }
        }

        private string numeroPo;

        public string NumeroPo
        {
            get { return numeroPo; }
            set { numeroPo = value; }
        }

        private HashSet<GheSetor> gheSetor;

        public HashSet<GheSetor> GheSetor
        {
            get { return gheSetor; }
            set { gheSetor = value; }
        }
        private HashSet<GheFonte> gheFonte;

        public HashSet<GheFonte> GheFonte
        {
            get { return gheFonte; }
            set { gheFonte = value; }
        }
        private HashSet<Nota> norma;

        public HashSet<Nota> Norma
        {
            get { return norma; }
            set { norma = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }

        public Ghe() { }

        public Ghe(Int64? id, Estudo estudo, String descricao, Int32 nexp, String situacao, string numeroPo)
        {
            this.Id = id;
            this.Estudo = estudo;
            this.Descricao = descricao;
            this.Nexp = nexp;
            this.Situacao = situacao;
            this.NumeroPo = numeroPo;
        }

        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
         public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Ghe p = obj as Ghe;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
         
    }
}
