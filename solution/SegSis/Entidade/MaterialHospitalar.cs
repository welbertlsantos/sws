﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SWS.Entidade
{
    public class MaterialHospitalar
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private String descricao;

        public String Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }
        private String unidade;

        public String Unidade
        {
            get { return unidade; }
            set { unidade = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }

        public MaterialHospitalar(){}

        public MaterialHospitalar(Int64? id, String descricao, String unidade, String situacao)
        {
            this.Id = id;
            this.Descricao = descricao;
            this.Unidade = unidade;
            this.Situacao = situacao;
        }

        public Boolean isNullForFilter()
        {
            Boolean retorno = Convert.ToBoolean(false);

            if (String.IsNullOrWhiteSpace(this.descricao) && String.IsNullOrWhiteSpace(this.unidade) && String.IsNullOrWhiteSpace(situacao))
            {
                retorno = Convert.ToBoolean(true);
            }

            return retorno;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            MaterialHospitalar p = obj as MaterialHospitalar;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

    }
}
