﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Relatorio
    {
        private Int64 id;

        public Int64 Id
        {
            get { return id; }
            set { id = value; }
        }
        private String nomeRelatorio;

        public String NomeRelatorio
        {
            get { return nomeRelatorio; }
            set { nomeRelatorio = value; }
        }
        private String tipoRelatorio;

        public String TipoRelatorio
        {
            get { return tipoRelatorio; }
            set { tipoRelatorio = value; }
        }
        private DateTime dataCriacao;

        public DateTime DataCriacao
        {
            get { return dataCriacao; }
            set { dataCriacao = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }

        private string nmRelatorio;

        public string NmRelatorio
        {
            get { return nmRelatorio; }
            set { nmRelatorio = value; }
        }

        public Relatorio() { }

        public Relatorio(Int64 id, String nomeRelatorio, String tipoRelatorio, DateTime dataCriacao, String situacao, string nmRelatorio)
        {
            this.Id = id;
            this.NomeRelatorio = nomeRelatorio;
            this.TipoRelatorio = tipoRelatorio;
            this.DataCriacao = dataCriacao;
            this.Situacao = situacao;
            this.NmRelatorio = nmRelatorio;
        }
    }
}
