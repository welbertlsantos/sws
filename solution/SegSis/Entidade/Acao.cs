﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade 
{
    public class Acao
    {
        private Int64 id;
        private String descricao;
        private String situacao;

        public Acao(Int64 id, String descricao, String situacao) 
        {
            this.setId(id);
            this.setDescricao(descricao);
            this.setSituacao(situacao);
        }
        
        public Int64 getId()
        {
            return this.id;
        }
        public void setId(Int64 id)
        {
            this.id = id;
        }

        public String getDescricao()
        {
            return this.descricao;
        }
        public void setDescricao(String descricao)
        {
            this.descricao = descricao;
        }
        
        public String getSituacao()
        {
            return this.situacao;
        }
        public void setSituacao(String situacao)
        {
            this.situacao = situacao;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Acao p = obj as Acao;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

        public Boolean isNullForFilter()
        {
            Boolean retorno = Convert.ToBoolean(false);

            if (String.IsNullOrWhiteSpace(this.descricao.Trim()))
            {
                retorno = Convert.ToBoolean(true);
            }

            return retorno;
        }
    }
}
