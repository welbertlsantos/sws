﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class ClienteFuncaoFuncionario
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private ClienteFuncao clienteFuncao;

        public ClienteFuncao ClienteFuncao
        {
            get { return clienteFuncao; }
            set { clienteFuncao = value; }
        }
        private Funcionario funcionario;

        public Funcionario Funcionario
        {
            get { return funcionario; }
            set { funcionario = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        private DateTime? dataCadastro;

        public DateTime? DataCadastro
        {
            get { return dataCadastro; }
            set { dataCadastro = value; }
        }
        private DateTime? dataDesligamento;

        public DateTime? DataDesligamento
        {
            get { return dataDesligamento; }
            set { dataDesligamento = value; }
        }
        private String matricula;

        public String Matricula
        {
            get { return matricula; }
            set { matricula = value; }
        }
        private bool vip;

        public bool Vip
        {
            get { return vip; }
            set { vip = value; }
        }

        private string brPdh;

        public string BrPdh
        {
            get { return brPdh; }
            set { brPdh = value; }
        }

        private string regimeRevezamento;

        public string RegimeRevezamento
        {
            get { return regimeRevezamento; }
            set { regimeRevezamento = value; }
        }

        private bool estagiario;

        public bool Estagiario
        {
            get { return estagiario; }
            set { estagiario = value; }
        }

        private bool vinculo;

        public bool Vinculo
        {
            get { return vinculo; }
            set { vinculo = value; }
        }

        public ClienteFuncaoFuncionario() { }

        public ClienteFuncaoFuncionario(Int64? id) 
        {
            this.Id = id;
        }

        public ClienteFuncaoFuncionario(Int64? id, ClienteFuncao clienteFuncao, Funcionario funcionario, String situacao,
            DateTime? data_cadastro, DateTime? data_desligamento, String matricula, bool vip, string brPdh, string regimeRevezamento, bool estagiario, bool vinculo)
        {
            this.Id = id;
            this.ClienteFuncao = clienteFuncao;
            this.Funcionario = funcionario;
            this.Situacao = situacao;
            this.DataCadastro = data_cadastro;
            this.DataDesligamento = data_desligamento;
            this.Matricula = matricula;
            this.Vip = vip;
            this.BrPdh = brPdh;
            this.RegimeRevezamento = regimeRevezamento;
            this.Estagiario = estagiario;
            this.Vinculo = vinculo;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            ClienteFuncaoFuncionario p = obj as ClienteFuncaoFuncionario;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
    }
}
