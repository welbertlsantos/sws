﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class CorrecaoEstudo
    {

        private long? id;

        public long? Id
        {
            get { return id; }
            set { id = value; }
        }
        private Usuario usuario;

        public Usuario Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }
        private Estudo estudo;

        public Estudo Estudo
        {
            get { return estudo; }
            set { estudo = value; }
        }
        private String historico;

        public String Historico
        {
            get { return historico; }
            set { historico = value; }
        }
        private DateTime dataCorrecao;

        public DateTime DataCorrecao
        {
            get { return dataCorrecao; }
            set { dataCorrecao = value; }
        } 
        
        public CorrecaoEstudo() { }

        public CorrecaoEstudo(long? id, Usuario usuario, Estudo estudo, String historico, DateTime dataCorrecao)
        {
            this.Id = id;
            this.Usuario = usuario;
            this.Estudo = estudo;
            this.Historico = historico;
            this.DataCorrecao = dataCorrecao;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            CorrecaoEstudo p = obj as CorrecaoEstudo;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
        
    }
}
