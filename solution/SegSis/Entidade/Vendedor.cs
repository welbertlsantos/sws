﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;

namespace SWS.Entidade
{
    public class Vendedor
    {

        private long? id;

        public long? Id
        {
            get { return id; }
            set { id = value; }
        }
        private String nome;

        public String Nome
        {
            get { return nome; }
            set { nome = value; }
        }
        private String cpf;

        public String Cpf
        {
            get { return cpf; }
            set { cpf = value.Replace(".", "").Replace(",", "").Replace("-", "").Trim(); }
        }
        private String rg;

        public String Rg
        {
            get { return rg; }
            set { rg = value; }
        }
        private String endereco;

        public String Endereco
        {
            get { return endereco; }
            set { endereco = value; }
        }
        private String numero;

        public String Numero
        {
            get { return numero; }
            set { numero = value; }
        }
        private String complemento;

        public String Complemento
        {
            get { return complemento; }
            set { complemento = value; }
        }
        private String bairro;

        public String Bairro
        {
            get { return bairro; }
            set { bairro = value; }
        }
        private String cidade;

        public String Cidade
        {
            get { return cidade; }
            set { cidade = value; }
        }
        private String uf;

        public String Uf
        {
            get { return uf; }
            set { uf = value; }
        }
        private String cep;

        public String Cep
        {
            get { return cep; }
            set { cep = value.Replace(".", "").Replace(",", "").Replace("-", "").Trim(); }
        }
        private String telefone1;

        public String Telefone1
        {
            get { return telefone1; }
            set { telefone1 = value; }
        }
        private String telefone2;

        public String Telefone2
        {
            get { return telefone2; }
            set { telefone2 = value; }
        }
        private String email;

        public String Email
        {
            get { return email; }
            set { email = value; }
        }
        private Decimal? comissao;

        public Decimal? Comissao
        {
            get { return comissao; }
            set { comissao = value; }
        }
        private DateTime? dataCadastro;

        public DateTime? DataCadastro
        {
            get { return dataCadastro; }
            set { dataCadastro = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }

        private CidadeIbge cidadeIbge;

        public CidadeIbge CidadeIbge
        {
            get { return cidadeIbge; }
            set { cidadeIbge = value; }
        }
        
        public Vendedor(long? id, String nome, String cpf, String rg, String endereco, String numero,
            String complemento, String bairro, String cidade, String uf, String cep, String telefone1, String telefone2, String email,
            Decimal? comissao, DateTime? dataCadastro, String situacao, CidadeIbge cidadeIbge)
        {
            this.Id = id;
            this.Nome = nome;
            this.Cpf = cpf;
            this.Rg = rg;
            this.Endereco = endereco;
            this.Numero = numero;
            this.Complemento = complemento;
            this.Bairro = bairro;
            this.Cidade = cidade;
            this.Uf = uf;
            this.Cep = cep;
            this.Telefone1 = telefone1;
            this.Telefone2 = telefone2;
            this.Email = email;
            this.Comissao = comissao;
            this.DataCadastro = dataCadastro;
            this.Situacao = situacao;
            this.CidadeIbge = cidadeIbge;
            
        }

        public Vendedor(long? id)
        {
            this.Id = id;
        }
        
        public Boolean isNullForFilter()
        {
            Boolean retorno = Convert.ToBoolean(false);

            if (String.IsNullOrWhiteSpace(this.nome) && String.IsNullOrWhiteSpace(this.cpf) &&
                String.IsNullOrWhiteSpace(this.cidade) && String.IsNullOrWhiteSpace(this.uf) &&
                    String.IsNullOrWhiteSpace(this.situacao))
            {
                retorno = Convert.ToBoolean(true);
            }

            return retorno;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Vendedor p = obj as Vendedor;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

    }
}
