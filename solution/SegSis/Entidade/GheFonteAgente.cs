﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class GheFonteAgente
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private GradSoma gradSoma;

        public GradSoma GradSoma
        {
            get { return gradSoma; }
            set { gradSoma = value; }
        }
        private GradEfeito gradEfeito;

        public GradEfeito GradEfeito
        {
            get { return gradEfeito; }
            set { gradEfeito = value; }
        }
        private GradExposicao gradExposicao;

        public GradExposicao GradExposicao
        {
            get { return gradExposicao; }
            set { gradExposicao = value; }
        }
        private GheFonte gheFonte;

        public GheFonte GheFonte
        {
            get { return gheFonte; }
            set { gheFonte = value; }
        }
        private Agente agente;

        public Agente Agente
        {
            get { return agente; }
            set { agente = value; }
        }
        private String tempoExposicao;

        public String TempoExposicao
        {
            get { return tempoExposicao; }
            set { tempoExposicao = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        
        public GheFonteAgente() { }

        public GheFonteAgente(Int64 id)
        {
            this.Id = id;
        }

        public GheFonteAgente(Int64? id, GradSoma gradSoma, GradEfeito gradEfeito, GradExposicao gradExposicao, GheFonte gheFonte, Agente agente, string tempoExposicao, string situacao)
        {
            this.Id = id;
            this.GradSoma = gradSoma;
            this.GradEfeito = gradEfeito;
            this.GradExposicao = gradExposicao;
            this.GheFonte = gheFonte;
            this.Agente = agente;
            this.TempoExposicao = tempoExposicao;
            this.Situacao = situacao;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            GheFonteAgente p = obj as GheFonteAgente;
            if (p == null)
            {
                return false;
            }

            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

        public GheFonteAgente copy()
        {
            return (GheFonteAgente)this.MemberwiseClone();
        }
    
    }
}
