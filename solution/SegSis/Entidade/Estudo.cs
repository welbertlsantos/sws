﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using SWS.View;
using System.Windows.Forms;

namespace SWS.Entidade
{
    public class Estudo
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        
        private Int64? idRelatorio;

        public Int64? IdRelatorio
        {
            get { return idRelatorio; }
            set { idRelatorio = value; }
        }
        
        private String codigoEstudo;

        public String CodigoEstudo
        {
            get { return codigoEstudo; }
            set { codigoEstudo = value.Replace(",", "").Replace(".", "").Replace("/", "").Trim(); }
        }
        
        private Cronograma cronograma;

        public Cronograma Cronograma
        {
            get { return cronograma; }
            set { cronograma = value; }
        }
        
        private VendedorCliente vendedorCliente;

        public VendedorCliente VendedorCliente
        {
            get { return vendedorCliente; }
            set { vendedorCliente = value; }
        }
        
        private Usuario engenheiro;

        public Usuario Engenheiro
        {
            get { return engenheiro; }
            set { engenheiro = value; }
        }
        
        private Usuario tecno;

        public Usuario Tecno
        {
            get { return tecno; }
            set { tecno = value; }
        }
        
        private Cliente clienteContratante;

        public Cliente ClienteContratante
        {
            get { return clienteContratante; }
            set { clienteContratante = value; }
        }
        
        private DateTime? dataCriacao;

        public DateTime? DataCriacao
        {
            get { return dataCriacao; }
            set { dataCriacao = value; }
        }
        
        private DateTime? dataVencimento;

        public DateTime? DataVencimento
        {
            get { return dataVencimento; }
            set { dataVencimento = value; }
        }
        
        private DateTime? dataFechamento;

        public DateTime? DataFechamento
        {
            get { return dataFechamento; }
            set { dataFechamento = value; }
        }
        
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }

        private Boolean tipoEstudo;

        public Boolean TipoEstudo
        {
            get { return tipoEstudo; }
            set { tipoEstudo = value; }
        }
        private String dataFimContrato;

        public String DataFimContrato
        {
            get { return dataFimContrato; }
            set { dataFimContrato = value; }
        }

        private String obra;

        public String Obra
        {
            get { return obra; }
            set { obra = value; }
        }

        private String localObra;

        public String LocalObra
        {
            get { return localObra; }
            set { localObra = value; }
        }

        private String endereco;

        public String Endereco
        {
            get { return endereco; }
            set { endereco = value; }
        }

        private String numero;

        public String Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        private String complemento;

        public String Complemento
        {
            get { return complemento; }
            set { complemento = value; }
        }

        private String bairro;

        public String Bairro
        {
            get { return bairro; }
            set { bairro = value; }
        }

        private String cidade;

        public String Cidade
        {
            get { return cidade; }
            set { cidade = value; }
        }

        private String uf;

        public String Uf
        {
            get { return uf; }
            set { uf = value; }
        }

        private String numContrato;

        public String NumContrato
        {
            get { return numContrato; }
            set { numContrato = value; }
        }

        private String grauRisco;

        public String GrauRisco
        {
            get { return grauRisco; }
            set { grauRisco = value; }
        }

        private String cliRazaoSocial;

        public String CliRazaoSocial
        {
            get { return cliRazaoSocial; }
            set { cliRazaoSocial = value; }
        }

        private String cliCnpj;

        public String CliCnpj
        {
            get { return cliCnpj; }
            set { cliCnpj = value; }
        }

        private String cliInscricao;

        public String CliInscricao
        {
            get { return cliInscricao; }
            set { cliInscricao = value; }
        }

        private Int32 cliEstFem;

        public Int32 CliEstFem
        {
            get { return cliEstFem; }
            set { cliEstFem = value; }
        }

        private Int32 cliEstMasc;

        public Int32 CliEstMasc
        {
            get { return cliEstMasc; }
            set { cliEstMasc = value; }
        }

        private String cliJornada;

        public String CliJornada
        {
            get { return cliJornada; }
            set { cliJornada = value; }
        }

        private String cliEndereco;

        public String CliEndereco
        {
            get { return cliEndereco; }
            set { cliEndereco = value; }
        }

        private String cliNumero;

        public String CliNumero
        {
            get { return cliNumero; }
            set { cliNumero = value; }
        }

        private String cliComplemento;

        public String CliComplemento
        {
            get { return cliComplemento; }
            set { cliComplemento = value; }
        }

        private String cliBairro;

        public String CliBairro
        {
            get { return cliBairro; }
            set { cliBairro = value; }
        }

        private String cliCidade;

        public String CliCidade
        {
            get { return cliCidade; }
            set { cliCidade = value; }
        }

        private String cliEmail;

        public String CliEmail
        {
            get { return cliEmail; }
            set { cliEmail = value; }
        }

        private String cliCep;

        public String CliCep
        {
            get { return cliCep; }
            set { cliCep = value; }
        }

        private String cliUf;

        public String CliUf
        {
            get { return cliUf; }
            set { cliUf = value; }
        }

        private String cliTelefone1;

        public String CliTelefone1
        {
            get { return cliTelefone1; }
            set { cliTelefone1 = value; }
        }

        private String cliTelefone2;

        public String CliTelefone2
        {
            get { return cliTelefone2; }
            set { cliTelefone2 = value; }
        }

        private String contrRazaoSocial;

        public String ContrRazaoSocial
        {
            get { return contrRazaoSocial; }
            set { contrRazaoSocial = value; }
        }

        private String contrCnpj;

        public String ContrCnpj
        {
            get { return contrCnpj; }
            set { contrCnpj = value; }
        }

        private String contrEndereco;

        public String ContrEndereco
        {
            get { return contrEndereco; }
            set { contrEndereco = value; }
        }

        private String contrNumero;

        public String ContrNumero
        {
            get { return contrNumero; }
            set { contrNumero = value; }
        }

        private String contrComplemento;

        public String ContrComplemento
        {
            get { return contrComplemento; }
            set { contrComplemento = value; }
        }

        private String contrBairro;

        public String ContrBairro
        {
            get { return contrBairro; }
            set { contrBairro = value; }
        }

        private String contrCidade;

        public String ContrCidade
        {
            get { return contrCidade; }
            set { contrCidade = value; }
        }

        private String contrCep;

        public String ContrCep
        {
            get { return contrCep; }
            set { contrCep = value; }
        }

        private String contrUf;

        public String ContrUf
        {
            get { return contrUf; }
            set { contrUf = value; }
        }

        private String contrInscricao;

        public String ContrInscricao
        {
            get { return contrInscricao; }
            set { contrInscricao = value; }
        }

        private String dataInicioContrato;

        public String DataInicioContrato
        {
            get { return dataInicioContrato; }
            set { dataInicioContrato = value; }
        }

        private String cep;

        public String Cep
        {
            get { return cep; }
            set { cep = value.Replace(".", "").Replace(",", "").Replace("-", "").Trim(); }
        }

        private String cliFantasia;

        public String CliFantasia
        {
            get { return cliFantasia; }
            set { cliFantasia = value; }
        }

        private String comentario;

        public String Comentario
        {
            get { return comentario; }
            set { comentario = value; }
        }

        private Boolean avulso;

        public Boolean Avulso
        {
            get { return avulso; }
            set { avulso = value; }
        }

        private Protocolo protocolo;

        public Protocolo Protocolo
        {
            get { return protocolo; }
            set { protocolo = value; }
        }

        List<MedicoEstudo> medicos;

        public List<MedicoEstudo> Medicos
        {
            get { return medicos; }
            set { medicos = value; }
        }

        public Estudo() { }

        public Estudo(Int64? id)
        { 
            this.Id = id;
        }

        public Estudo(Int64? id, Int64? idRelatorio, String codigoEstudo, Cronograma cronograma, VendedorCliente vendedorCliente, 
            Usuario engenheiro, Usuario tecno, Cliente clienteContratante, DateTime? dataCriacao,
            DateTime? dataVencimento, DateTime? dataFechamento, String situacao, Boolean tipoEstudo, 
            String dataFimContrato, String obra, String localObra, String endereco, String numero, 
            String complemento, String bairro, String cidade, String uf,
            String numContrato, String grauRisco, String cliRazaoSocial, String cliCnpj, String cliInscricao,
            Int32 cliEstFem, Int32 cliEstMasc, String cliJornada, String cliEndereco, String cliNumero, 
            String cliComplemento, String cliBairro, String cliCidade, String cliEmail, String cliCep, 
            String cliUf, String cliTelefone1, String cliTelefone2, String contrRazaoSocial, 
            String contrCnpj, String contrEndereco, String contrNumero, String contrComplemento, String contrBairro,
            String contrCidade, String contrCep, String contrUf, String contrInscricao, String dataInicioContrato, String cep, String cliFantasia, String comentario, Boolean avulso,
             Protocolo protocolo)
        {
            this.Id = id;
            this.IdRelatorio = idRelatorio;
            this.CodigoEstudo = codigoEstudo;
            this.Cronograma = cronograma;
            this.VendedorCliente = vendedorCliente;
            this.Engenheiro = engenheiro;
            this.Tecno = tecno;
            this.ClienteContratante = clienteContratante;
            this.DataCriacao = dataCriacao;
            this.DataVencimento = dataVencimento;
            this.DataFechamento = dataFechamento;
            this.Situacao = situacao;
            this.TipoEstudo = tipoEstudo;
            this.DataFimContrato = dataFimContrato;
            this.Obra = obra;
            this.LocalObra = localObra;
            this.Endereco = endereco;
            this.Numero = numero;
            this.Complemento = complemento;
            this.Bairro = bairro;
            this.Cidade = cidade;
            this.Uf = uf;
            this.NumContrato = numContrato;
            this.GrauRisco = grauRisco;
            this.CliRazaoSocial = cliRazaoSocial;
            this.CliCnpj = cliCnpj;
            this.CliInscricao = cliInscricao;
            this.CliEstFem = cliEstFem;
            this.CliEstMasc = cliEstMasc;
            this.CliJornada = cliJornada;
            this.CliEndereco = cliEndereco;
            this.CliNumero = cliNumero;
            this.CliComplemento = cliComplemento;
            this.CliBairro = cliBairro;
            this.CliCidade = cliCidade;
            this.CliEmail = cliEmail;
            this.CliCep = cliCep;
            this.CliUf = cliUf;
            this.CliTelefone1 = cliTelefone1;
            this.CliTelefone2 = cliTelefone2;
            this.ContrRazaoSocial = contrRazaoSocial;
            this.ContrCnpj = contrCnpj;
            this.ContrEndereco = contrEndereco;
            this.ContrNumero = contrNumero;
            this.ContrComplemento = contrComplemento;
            this.ContrBairro = contrBairro;
            this.ContrCidade = contrCidade;
            this.ContrCep = contrCep;
            this.ContrUf = contrUf;
            this.ContrInscricao = contrInscricao;
            this.DataInicioContrato = dataInicioContrato;
            this.Cep = cep;
            this.CliFantasia = cliFantasia;
            this.Comentario = comentario;
            this.Avulso = avulso;
            this.Protocolo = protocolo;
            
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Estudo p = obj as Estudo;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

        public Boolean isNullForFilter()
        {
            Boolean retorno = false;

            if (String.IsNullOrWhiteSpace(this.codigoEstudo)
                && (vendedorCliente == null || vendedorCliente.Cliente == null || String.IsNullOrWhiteSpace(vendedorCliente.Cliente.RazaoSocial))
                && String.IsNullOrWhiteSpace(Convert.ToString(this.dataCriacao))
                && String.IsNullOrWhiteSpace(this.situacao)
                && (tecno == null || String.IsNullOrWhiteSpace(tecno.Nome))
                && (engenheiro == null || String.IsNullOrWhiteSpace(engenheiro.Nome))
                )
            {
                retorno = true;
            }

            return retorno;
        }

    }
}
