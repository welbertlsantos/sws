﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Contrato
    {

        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private String codigoContrato;

        public String CodigoContrato
        {
            get { return codigoContrato; }
            set { codigoContrato = value.Replace(".", "").Replace("/", "").Trim(); }
        }
        private Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }
        private Usuario usuarioCriador;

        public Usuario UsuarioCriador
        {
            get { return usuarioCriador; }
            set { usuarioCriador = value; }
        }
        private DateTime? dataElaboracao;

        public DateTime? DataElaboracao
        {
            get { return dataElaboracao; }
            set { dataElaboracao = value; }
        }
        private DateTime? dataFim;

        public DateTime? DataFim
        {
            get { return dataFim; }
            set { dataFim = value; }
        }
        private DateTime? dataInicio;

        public DateTime? DataInicio
        {
            get { return dataInicio; }
            set { dataInicio = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        private String observacao;

        public String Observacao
        {
            get { return observacao; }
            set { observacao = value; }
        }
        private Usuario usuarioFinalizou;

        public Usuario UsuarioFinalizou
        {
            get { return usuarioFinalizou; }
            set { usuarioFinalizou = value; }
        }
        private Usuario usuarioCancelamento;

        public Usuario UsuarioCancelamento
        {
            get { return usuarioCancelamento; }
            set { usuarioCancelamento = value; }
        }
        private String motivoCancelamento;

        public String MotivoCancelamento
        {
            get { return motivoCancelamento; }
            set { motivoCancelamento = value; }
        }
        private Cliente prestador;

        public Cliente Prestador
        {
            get { return prestador; }
            set { prestador = value; }
        }
        private Boolean contratoAutomatico;

        public Boolean ContratoAutomatico
        {
            get { return contratoAutomatico; }
            set { contratoAutomatico = value; }
        }
        private DateTime? dataCancelamento;

        public DateTime? DataCancelamento
        {
            get { return dataCancelamento; }
            set { dataCancelamento = value; }
        }
        private Contrato contratoPai;

        public Contrato ContratoPai
        {
            get { return contratoPai; }
            set { contratoPai = value; }
        }
        private Boolean bloqueiaInadimplencia;

        public Boolean BloqueiaInadimplencia
        {
            get { return bloqueiaInadimplencia; }
            set { bloqueiaInadimplencia = value; }
        }
        private Boolean geraMensalidade;

        public Boolean GeraMensalidade
        {
            get { return geraMensalidade; }
            set { geraMensalidade = value; }
        }
        private Boolean geraNumeroVidas;

        public Boolean GeraNumeroVidas
        {
            get { return geraNumeroVidas; }
            set { geraNumeroVidas = value; }
        }
        private int? dataCobranca;

        public int? DataCobranca
        {
            get { return dataCobranca; }
            set { dataCobranca = value; }
        }
        private Boolean cobrancaAutomatica;

        public Boolean CobrancaAutomatica
        {
            get { return cobrancaAutomatica; }
            set { cobrancaAutomatica = value; }
        }
        private Decimal? valorMensalidade;

        public Decimal? ValorMensalidade
        {
            get { return valorMensalidade; }
            set { valorMensalidade = value; }
        }
        private Decimal? valorNumeroVidas;

        public Decimal? ValorNumeroVidas
        {
            get { return valorNumeroVidas; }
            set { valorNumeroVidas = value; }
        }
        private int? diasInadimplenciaBloqueio;

        public int? DiasInadimplenciaBloqueio
        {
            get { return diasInadimplenciaBloqueio; }
            set { diasInadimplenciaBloqueio = value; }
        }
        private Contrato contratoRaiz;

        public Contrato ContratoRaiz
        {
            get { return contratoRaiz; }
            set { contratoRaiz = value; }
        }
        private ClienteProposta clienteProposta;

        public ClienteProposta ClienteProposta
        {
            get { return clienteProposta; }
            set { clienteProposta = value; }
        }
        private Boolean proposta;

        public Boolean Proposta
        {
            get { return proposta; }
            set { proposta = value; }
        }
        private String propostaFormaPagamento;

        public String PropostaFormaPagamento
        {
            get { return propostaFormaPagamento; }
            set { propostaFormaPagamento = value; }
        }
        private Usuario usuarioEncerrou;

        public Usuario UsuarioEncerrou
        {
            get { return usuarioEncerrou; }
            set { usuarioEncerrou = value; }
        }
        private DateTime? dataEncerramento;

        public DateTime? DataEncerramento
        {
            get { return dataEncerramento; }
            set { dataEncerramento = value; }
        }
        private String motivoEncerramento;

        public String MotivoEncerramento
        {
            get { return motivoEncerramento; }
            set { motivoEncerramento = value; }
        }

        private Empresa empresa;

        public Empresa Empresa
        {
            get { return empresa; }
            set { empresa = value; }
        }


        public Contrato() { }

        public Contrato(Int64? id, String codigoContrato, Cliente cliente,  Usuario usuarioCriador,  DateTime? dataElaboracao, DateTime? dataFim, DateTime? dataInicio, String situacao, String observacao, Usuario usuarioFinalizou, Usuario usuarioCancelamento, String motivoCancelamento, Cliente prestador, Boolean contratoAutomatico, DateTime? dataCancelamento, Contrato contratoPai, Boolean bloqueiaInadimplencia, Boolean geraMensalidade, Boolean geraNumeroVidas, int? dataCobranca, Boolean cobrancaAutomatica, Decimal? valorMensalidade, Decimal? valorNumeroVidas, int? diasInadimplenciaBloqueio, Contrato contratoRaiz, ClienteProposta clienteProposta, Boolean proposta, String propostaFormaPagamento, Usuario usuarioEncerrou, DateTime? dataEncerramento, String motivoEncerramento, Empresa empresa)
        {
            this.Id = id;
            this.CodigoContrato = codigoContrato;
            this.Cliente = cliente;
            this.UsuarioCriador = usuarioCriador;
            this.DataElaboracao = dataElaboracao;
            this.DataFim  = dataFim;
            this.DataInicio = dataInicio;
            this.Situacao = situacao;
            this.Observacao = observacao;
            this.UsuarioFinalizou = usuarioFinalizou;
            this.UsuarioCancelamento = usuarioCancelamento;
            this.MotivoCancelamento = motivoCancelamento;
            this.Prestador = prestador;
            this.ContratoAutomatico = contratoAutomatico;
            this.DataCancelamento = dataCancelamento;
            this.ContratoPai = contratoPai;
            this.BloqueiaInadimplencia = bloqueiaInadimplencia;
            this.GeraMensalidade = geraMensalidade;
            this.GeraNumeroVidas = geraNumeroVidas;
            this.DataCobranca = dataCobranca;
            this.CobrancaAutomatica = cobrancaAutomatica;
            this.ValorMensalidade = valorMensalidade;
            this.ValorNumeroVidas = valorNumeroVidas;
            this.DiasInadimplenciaBloqueio = diasInadimplenciaBloqueio;
            this.ContratoRaiz = contratoRaiz;
            this.ClienteProposta = clienteProposta;
            this.Proposta = proposta;
            this.PropostaFormaPagamento= propostaFormaPagamento;
            this.UsuarioEncerrou = usuarioEncerrou;
            this.DataEncerramento = dataEncerramento;
            this.Empresa = empresa;
        }

        public Contrato(Int64? id)
        {
            this.Id = id;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Contrato p = obj as Contrato;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

        public Boolean isNullForFilter()
        {
            Boolean retorno = Convert.ToBoolean(false);

            if (String.IsNullOrEmpty(this.codigoContrato) && String.IsNullOrEmpty(this.situacao) && 
                this.dataElaboracao == null && this.dataFim == null && this.dataInicio == null 
                && this.dataCancelamento == null &&  this.cliente == null)
            {
                retorno = Convert.ToBoolean(true);
            }

            return retorno;
        }


    }
}
