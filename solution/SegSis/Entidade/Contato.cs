﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Contato
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }

        private CidadeIbge cidade;

        public CidadeIbge Cidade
        {
            get { return cidade; }
            set { cidade = value; }
        }

        private TipoContato tipoContato;

        public TipoContato TipoContato
        {
            get { return tipoContato; }
            set { tipoContato = value; }
        }

        private Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }

        private String nome;

        public String Nome
        {
            get { return nome; }
            set { nome = value.Trim(); }
        }

        private String cpf;

        public String Cpf
        {
            get { return cpf; }
            set { cpf = value.Replace(".","").Replace("-","").Trim(); }
        }

        private String rg;

        public String Rg
        {
            get { return rg; }
            set { rg = value; }
        }

        private DateTime? dataNascimento;

        public DateTime? DataNascimento
        {
            get { return dataNascimento; }
            set { dataNascimento = value; }
        }

        private String endereco;

        public String Endereco
        {
            get { return endereco; }
            set { endereco = value.Trim(); }
        }

        private String numero;

        public String Numero
        {
            get { return numero; }
            set { numero = value.Trim(); }
        }

        private String complemento;

        public String Complemento
        {
            get { return complemento; }
            set { complemento = value.Trim(); }
        }

        private String bairro;

        public String Bairro
        {
            get { return bairro; }
            set { bairro = value.Trim() ; }
        }

        private String uf;

        public String Uf
        {
            get { return uf; }
            set { uf = value; }
        }

        private String cep;

        public String Cep
        {
            get { return cep; }
            set { cep = value.Replace("-","").Trim(); }
        }

        private String telefone;

        public String Telefone
        {
            get { return telefone; }
            set { telefone = value; }
        }

        private String celular;

        public String Celular
        {
            get { return celular; }
            set { celular = value; }
        }

        private String pisPasep;

        public String PisPasep
        {
            get { return pisPasep; }
            set { pisPasep = value.Replace(".","").Replace(",","").Replace("-","").Trim(); }
        }

        private String email;

        public String Email
        {
            get { return email; }
            set { email = value; }
        }

        private bool responsavelContrato;

        public bool ResponsavelContrato
        {
            get { return responsavelContrato; }
            set { responsavelContrato = value; }
        }

        private bool responsavelEstudo;

        public bool ResponsavelEstudo
        {
            get { return responsavelEstudo; }
            set { responsavelEstudo = value; }
        }

        public Contato(
            Int64? id,
            CidadeIbge cidade,
            TipoContato tipoContato,
            Cliente cliente,
            String nome,
            String cpf,
            String rg,
            DateTime? dataNascimento,
            String endereco,
            String numero,
            String complemento,
            String bairro,
            String uf,
            String cep,
            String telefone,
            String celular,
            String pisPasep,
            String email,
            bool responsavelContrato,
            bool responsavelEstudo
            )
        {
            this.Id = id;
            this.Cidade = cidade;
            this.TipoContato = tipoContato;
            this.Cliente = cliente;
            this.Nome = nome;
            this.Cpf = cpf;
            this.Rg = rg;
            this.DataNascimento = dataNascimento;
            this.Endereco = endereco;
            this.Numero = numero;
            this.Complemento = complemento;
            this.Bairro = bairro;
            this.Uf = uf;
            this.Cep = cep;
            this.Telefone = telefone;
            this.Celular = celular;
            this.PisPasep = pisPasep;
            this.Email = email;
            this.ResponsavelContrato = responsavelContrato;
            this.ResponsavelEstudo = responsavelEstudo;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Contato p = obj as Contato;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
           
            
            

    }
}
