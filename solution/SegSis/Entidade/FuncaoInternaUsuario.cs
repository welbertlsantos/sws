﻿using System;
using SWS.Entidade;

namespace SWS.View.Entidade
{
    public class FuncaoInternaUsuario
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private FuncaoInterna funcaoInterna;

        public FuncaoInterna FuncaoInterna
        {
            get { return funcaoInterna; }
            set { funcaoInterna = value; }
        }
        private Usuario usuario;

        public Usuario Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        private DateTime? dataInicio;

        public DateTime? DataInicio
        {
            get { return dataInicio; }
            set { dataInicio = value; }
        }
        private DateTime? dataTermino;

        public DateTime? DataTermino
        {
            get { return dataTermino; }
            set { dataTermino = value; }
        }
      
        public FuncaoInternaUsuario(Int64? id, FuncaoInterna funcaoInterna, Usuario usuario, String situacao, DateTime? dataInicio, DateTime? dataTermino)
        {
            this.Id = id;
            this.FuncaoInterna = funcaoInterna;
            this.Usuario = usuario;
            this.Situacao = situacao;
            this.DataInicio = dataInicio;
            this.DataTermino = dataTermino;

        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            FuncaoInternaUsuario p = obj as FuncaoInternaUsuario;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

    }
}
