﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Monitoramento
    {
        private long? id;

        public long? Id
        {
            get { return id; }
            set { id = value; }
        }
        private Monitoramento monitoramentoAnterior;

        internal Monitoramento MonitoramentoAnterior
        {
            get { return monitoramentoAnterior; }
            set { monitoramentoAnterior = value; }
        }

        private Usuario usuarioResponsavel;

        public Usuario UsuarioResponsavel
        {
            get { return usuarioResponsavel; }
            set { usuarioResponsavel = value; }
        }

        private DateTime dataInicioCondicao;

        public DateTime DataInicioCondicao
        {
            get { return dataInicioCondicao; }
            set { dataInicioCondicao = value; }
        }

        private string observacao;

        public string Observacao
        {
            get { return observacao; }
            set { observacao = value; }
        }

        private Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }

        private DateTime? periodo;

        public DateTime? Periodo
        {
            get { return periodo; }
            set { periodo = value; }
        }

        private Estudo estudo;

        public Estudo Estudo
        {
            get { return estudo; }
            set { estudo = value; }
        }

        private ClienteFuncaoFuncionario clienteFuncaoFuncionario;

        public ClienteFuncaoFuncionario ClienteFuncaoFuncionario
        {
            get { return clienteFuncaoFuncionario; }
            set { clienteFuncaoFuncionario = value; }
        }

        private ClienteFuncionario clienteFuncionario;

        public ClienteFuncionario ClienteFuncionario
        {
            get { return clienteFuncionario; }
            set { clienteFuncionario = value; }
        }

        private DateTime? dataFimCondicao;

        public DateTime? DataFimCondicao
        {
            get { return dataFimCondicao; }
            set { dataFimCondicao = value; }
        }

        public Monitoramento() { }
        
        public Monitoramento(long? id, Monitoramento monitoramentoAnterior, ClienteFuncaoFuncionario clienteFuncaoFuncionario, Usuario usuarioResponsavel, DateTime dataInicioCondicao, string observacao, Cliente cliente, DateTime? periodo, Estudo estudo)
        {
            this.Id = id;
            this.MonitoramentoAnterior = monitoramentoAnterior;
            this.ClienteFuncaoFuncionario = clienteFuncaoFuncionario;
            this.UsuarioResponsavel = usuarioResponsavel;
            this.DataInicioCondicao = dataInicioCondicao;
            this.Observacao = observacao;
            this.Cliente = cliente;
            this.Periodo = periodo;
            this.Estudo = estudo;
        }

        public Monitoramento(long id)
        {
            this.Id = id;
        }

        public bool isNullForFilter()
        {
            bool retorno = false;

            if (cliente == null && clienteFuncaoFuncionario == null & periodo == null)
                retorno = true;

            return retorno;
        }

        public Monitoramento copy()
        {
            Monitoramento monitoramentoCopy = (Monitoramento)this.MemberwiseClone();
            return monitoramentoCopy;
        }
    }
}
