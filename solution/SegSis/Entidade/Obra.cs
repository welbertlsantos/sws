﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS.Entidade
{
    public class Obra
    {
        private string localObra;

        public string LocalObra
        {
            get { return localObra; }
            set { localObra = value; }
        }

        private string enderecoObra;

        public string EnderecoObra
        {
            get { return enderecoObra; }
            set { enderecoObra = value; }
        }

        private string numeroObra;

        public string NumeroObra
        {
            get { return numeroObra; }
            set { numeroObra = value; }
        }

        private string complementoObra;

        public string ComplementoObra
        {
            get { return complementoObra; }
            set { complementoObra = value; }
        }

        private string bairroObra;

        public string BairroObra
        {
            get { return bairroObra; }
            set { bairroObra = value; }
        }

        private string unidadeFederativaObra;

        public string UnidadeFederativaObra
        {
            get { return unidadeFederativaObra; }
            set { unidadeFederativaObra = value; }
        }

        private CidadeIbge cidadeObra;

        public CidadeIbge CidadeObra
        {
            get { return cidadeObra; }
            set { cidadeObra = value; }
        }

        private string cepObra;

        public string CepObra
        {
            get { return cepObra; }
            set { cepObra = value.Replace(".","").Replace("-","").Trim(); }
        }

        private string descritivoObra;

        public string DescritivoObra
        {
            get { return descritivoObra; }
            set { descritivoObra = value; }
        }

        public Obra() { }
    }
}
