﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    
    public class ClienteProposta
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private String razaoSocial;

        public String RazaoSocial
        {
            get { return razaoSocial; }
            set { razaoSocial = value; }
        }
        private String nomeFantasia;

        public String NomeFantasia
        {
            get { return nomeFantasia; }
            set { nomeFantasia = value; }
        }
        private String cnpj;

        public String Cnpj
        {
            get { return cnpj; }
            set { cnpj = value.Replace(",", "").Replace(".", "").Replace("/", "").Trim(); }
        }
        private String inscricao;

        public String Inscricao
        {
            get { return inscricao; }
            set { inscricao = value; }
        }
        private String endereco;

        public String Endereco
        {
            get { return endereco; }
            set { endereco = value; }
        }
        private String numero;

        public String Numero
        {
            get { return numero; }
            set { numero = value; }
        }
        private String complemento;

        public String Complemento
        {
            get { return complemento; }
            set { complemento = value; }
        }
        private String bairro;

        public String Bairro
        {
            get { return bairro; }
            set { bairro = value; }
        }
        private String cep;

        public String Cep
        {
            get { return cep; }
            set { cep = value.Replace(",", "").Replace(".", "").Replace("-", "").Trim(); }
        }
        private CidadeIbge cidade;

        public CidadeIbge Cidade
        {
            get { return cidade; }
            set { cidade = value; }
        }
        private String uf;

        public String Uf
        {
            get { return uf; }
            set { uf = value; }
        }
        private String telefone;

        public String Telefone
        {
            get { return telefone; }
            set { telefone = value; }
        }
        private String celular;

        public String Celular
        {
            get { return celular; }
            set { celular = value; }
        }
        private DateTime? dataCadastro;

        public DateTime? DataCadastro
        {
            get { return dataCadastro; }
            set { dataCadastro = value; }
        }
        private String contato;

        public String Contato
        {
            get { return contato; }
            set { contato = value; }
        }
        private String email;

        public String Email
        {
            get { return email; }
            set { email = value; }
        }

        public ClienteProposta() { }

        public ClienteProposta(Int64? id)
        {
            this.Id = id;
        }

        public ClienteProposta(Int64? id,
            String razaoSocial,
            String nomeFantasia,
            String cnpj,
            String inscricao,
            String endereco,
            String numero,
            String complemento,
            String bairro,
            String cep,
            CidadeIbge cidade,
            String uf,
            String telefone,
            String celular,
            DateTime? dataCadastro,
            String contato,
            String email)
        {
            this.Id = id;
            this.RazaoSocial = razaoSocial;
            this.NomeFantasia = nomeFantasia;
            this.Cnpj = cnpj;
            this.Inscricao = inscricao;
            this.Endereco = endereco;
            this.Numero = numero;
            this.Complemento = complemento;
            this.Bairro = bairro;
            this.Cep = cep;
            this.Cidade = cidade;
            this.Uf = uf;
            this.Telefone = telefone;
            this.Celular = celular;
            this.DataCadastro = dataCadastro;
            this.Contato = contato;
            this.Email = email;
        }

    }
}
