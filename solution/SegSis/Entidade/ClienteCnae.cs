﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;

namespace SWS.Entidade
{
    public class ClienteCnae
    {
        private Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }

        private Cnae cnae;

        public Cnae Cnae
        {
            get { return cnae; }
            set { cnae = value; }
        }

        private Boolean flag_pr;

        public Boolean Flag_pr
        {
            get { return flag_pr; }
            set { flag_pr = value; }
        }

        public ClienteCnae() { }

        public ClienteCnae(Cliente cliente, Cnae cnae, Boolean flag_pr)
        {
            this.Cliente = cliente;
            this.Cnae = cnae;
            this.Flag_pr = flag_pr;
        }

    }
}
