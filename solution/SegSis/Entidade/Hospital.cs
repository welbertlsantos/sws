﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Hospital
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private String nome;

        public String Nome
        {
            get { return nome; }
            set { nome = value; }
        }
        private String endereco;

        public String Endereco
        {
            get { return endereco; }
            set { endereco = value; }
        }
        private String numero;

        public String Numero
        {
            get { return numero; }
            set { numero = value; }
        }
        private String complemento;

        public String Complemento
        {
            get { return complemento; }
            set { complemento = value; }
        }
        private String bairro;

        public String Bairro
        {
            get { return bairro; }
            set { bairro = value; }
        }
        
        private String cep;

        public String Cep
        {
            get { return cep; }
            set { cep = value.Replace(".","").Replace("-","").Trim(); }
        }
        private String uf;

        public String Uf
        {
            get { return uf; }
            set { uf = value; }
        }
        private String telefone1;

        public String Telefone1
        {
            get { return telefone1; }
            set { telefone1 = value; }
        }
        private String telefone2;

        public String Telefone2
        {
            get { return telefone2; }
            set { telefone2 = value; }
        }
        private String observacao;

        public String Observacao
        {
            get { return observacao; }
            set { observacao = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }

        private CidadeIbge cidade;

        public CidadeIbge Cidade
        {
            get { return cidade; }
            set { cidade = value; }
        }

        public Hospital() { }

        public Hospital(Int64? id, String descricao, String endereco, String numero, String complemento, String bairro, String cep, String uf, String telefone1, String telefone2, String observacao, String situacao, CidadeIbge cidade)
        {
            this.Id = id;
            this.Nome = descricao;
            this.Endereco = endereco;
            this.Numero = numero;
            this.Complemento = complemento;
            this.Bairro = bairro;
            this.Cep = cep;
            this.Uf = uf;
            this.Telefone1 = telefone1;
            this.Telefone2 = telefone2;
            this.Observacao = observacao;
            this.Situacao = situacao;
            this.Cidade = cidade;
        }

        public Boolean isNullForFilter()
        {
            Boolean retorno = Convert.ToBoolean(false);

            if (String.IsNullOrWhiteSpace(this.nome) && String.IsNullOrWhiteSpace(situacao))
            {
                retorno = Convert.ToBoolean(true);
            }

            return retorno;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Hospital p = obj as Hospital;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

    }
}
