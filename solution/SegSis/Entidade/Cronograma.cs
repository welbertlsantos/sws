﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;

namespace SWS.Entidade
{
    public class Cronograma
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private String descricao;

        public String Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }
        private HashSet<CronogramaAtividade> cronogramaAtividade;

        public HashSet<CronogramaAtividade> CronogramaAtividade
        {
            get { return cronogramaAtividade; }
            set { cronogramaAtividade = value; }
        }

        public Cronograma() { }

        public Cronograma(Int64? id, String descricao)
        {
            this.Id = id;
            this.Descricao = descricao;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Cronograma p = obj as Cronograma;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

    }
}
