﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class RelatorioAnual
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private Usuario usuario;

        public Usuario Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }
        private Medico medicoCoordenador;

        public Medico MedicoCoordenador
        {
            get { return medicoCoordenador; }
            set { medicoCoordenador = value; }
        }
        private Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }
        private DateTime dataInicial;

        public DateTime DataInicial
        {
            get { return dataInicial; }
            set { dataInicial = value; }
        }
        private DateTime dataFinal;

        public DateTime DataFinal
        {
            get { return dataFinal; }
            set { dataFinal = value; }
        }
        private DateTime dataGeracao;

        public DateTime DataGeracao
        {
            get { return dataGeracao; }
            set { dataGeracao = value; }
        }

        private LinkedList<RelatorioAnualItem> itemRelatorioAnual = new LinkedList<RelatorioAnualItem>();

        public LinkedList<RelatorioAnualItem> ItemRelatorioAnual
        {
            get { return itemRelatorioAnual; }
            set { itemRelatorioAnual = value; }
        }

        public RelatorioAnual() { }

        public RelatorioAnual(Int64? id,
            Usuario usuario,
            Medico coordenador,
            Cliente cliente,
            DateTime dataInicial,
            DateTime dataFinal,
            DateTime dataGeracao)
        {
            this.Id = id;
            this.Usuario = usuario;
            this.MedicoCoordenador = coordenador;
            this.Cliente = cliente;
            this.DataInicial = dataInicial;
            this.DataFinal = dataFinal;
            this.dataGeracao = dataGeracao;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            RelatorioAnual p = obj as RelatorioAnual;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

    }
}
