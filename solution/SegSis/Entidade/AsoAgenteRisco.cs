﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class AsoAgenteRisco
    {

        private Int64? idAso;

        public Int64? IdAso
        {
            get { return idAso; }
            set { idAso = value; }
        }
        private Int64? idAgente;

        public Int64? IdAgente
        {
            get { return idAgente; }
            set { idAgente = value; }
        }
        private String descricaoAgente;

        public String DescricaoAgente
        {
            get { return descricaoAgente; }
            set { descricaoAgente = value; }
        }
        private Int64? idRisco;

        public Int64? IdRisco
        {
            get { return idRisco; }
            set { idRisco = value; }
        }
        private String descricaoRisco;

        public String DescricaoRisco
        {
            get { return descricaoRisco; }
            set { descricaoRisco = value; }
        }

        public AsoAgenteRisco() { }

        public AsoAgenteRisco(Int64? idAso, Int64? idAgente, String descricaoAgente, Int64? idRisco, String descricaoRisco)
        {
            this.IdAso = idAso;
            this.IdAgente = idAgente;
            this.DescricaoAgente = descricaoAgente;
            this.IdRisco = idRisco;
            this.DescricaoRisco = descricaoRisco;
        }

    }
}
