﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Aso
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private Medico examinador;

        public Medico Examinador
        {
            get { return examinador; }
            set { examinador = value; }
        }
        private ClienteFuncaoFuncionario clienteFuncaoFuncionario;

        public ClienteFuncaoFuncionario ClienteFuncaoFuncionario
        {
            get { return clienteFuncaoFuncionario; }
            set { clienteFuncaoFuncionario = value; }
        }
        private String codigo; // codigo composto pelo ano + mes + id do aso com 6 digitos. exemplo: 2012.12.000001

        public String Codigo
        {
            get { return codigo; }
            set { codigo = value.Replace(".","").Replace("-","").Trim(); }
        }
        private DateTime? dataElaboracao;

        public DateTime? DataElaboracao
        {
            get { return dataElaboracao; }
            set { dataElaboracao = value; }
        }
        private Funcionario funcionario;

        public Funcionario Funcionario
        {
            get { return funcionario; }
            set { funcionario = value; }
        }
        private Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }
        private Periodicidade periodicidade;

        public Periodicidade Periodicidade
        {
            get { return periodicidade; }
            set { periodicidade = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        private Medico coordenador;

        public Medico Coordenador
        {
            get { return coordenador; }
            set { coordenador = value; }
        }
        private Usuario criador;

        public Usuario Criador
        {
            get { return criador; }
            set { criador = value; }
        }
        private Usuario finalizador;

        public Usuario Finalizador
        {
            get { return finalizador; }
            set { finalizador = value; }
        }
        private String rg;

        public String Rg
        {
            get { return rg; }
            set { rg = value; }
        }
        private Estudo estudo;

        public Estudo Estudo
        {
            get { return estudo; }
            set { estudo = value; }
        }
        private Usuario usuarioAlteracao;

        public Usuario UsuarioAlteracao
        {
            get { return usuarioAlteracao; }
            set { usuarioAlteracao = value; }
        }
        private String justificativaAlteracao;

        public String JustificativaAlteracao
        {
            get { return justificativaAlteracao; }
            set { justificativaAlteracao = value; }
        }
        private String matricula;

        public String Matricula
        {
            get { return matricula; }
            set { matricula = value; }
        }
        private Usuario usuarioCancelamento;

        public Usuario UsuarioCancelamento
        {
            get { return usuarioCancelamento; }
            set { usuarioCancelamento = value; }
        }
        private DateTime? dataFinalizacao;

        public DateTime? DataFinalizacao
        {
            get { return dataFinalizacao; }
            set { dataFinalizacao = value; }
        }
        private DateTime? dataCancelamento;

        public DateTime? DataCancelamento
        {
            get { return dataCancelamento; }
            set { dataCancelamento = value; }
        }
        private DateTime? dataGravacao;

        public DateTime? DataGravacao
        {
            get { return dataGravacao; }
            set { dataGravacao = value; }
        }
        private String senha;

        public String Senha
        {
            get { return senha; }
            set { senha = value; }
        }
        private String observacaoAso;

        public String ObservacaoAso
        {
            get { return observacaoAso; }
            set { observacaoAso = value; }
        }
        private String conclusao;

        public String Conclusao
        {
            get { return conclusao; }
            set { conclusao = value; }
        }
        private List<GheFonteAgenteExameAso> examePcmso = new List<GheFonteAgenteExameAso>();

        public List<GheFonteAgenteExameAso> ExamePcmso
        {
            get { return examePcmso; }
            set { examePcmso = value; }
        }
        private List<ClienteFuncaoExameASo> exameExtra = new List<ClienteFuncaoExameASo>();

        public List<ClienteFuncaoExameASo> ExameExtra
        {
            get { return exameExtra; }
            set { exameExtra = value; }
        }
        private Protocolo protocolo;

        public Protocolo Protocolo
        {
            get { return protocolo; }
            set { protocolo = value; }
        }
        private String justificativa;

        public String Justificativa
        {
            get { return justificativa; }
            set { justificativa = value; }
        }

        private Empresa empresa;

        public Empresa Empresa
        {
            get { return empresa; }
            set { empresa = value; }
        }

        private CentroCusto centroCusto;

        public CentroCusto CentroCusto
        {
            get { return centroCusto; }
            set { centroCusto = value; }
        }

        private DateTime? dataVencimento;

        public DateTime? DataVencimento
        {
            get { return dataVencimento; }
            set { dataVencimento = value; }
        }

        private string codigoPo;

        public string CodigoPo
        {
            get { return codigoPo; }
            set { codigoPo = value; }
        }

        private bool prioridade;

        public bool Prioridade
        {
            get { return prioridade; }
            set { prioridade = value; }
        }

        private string pis;

        public string Pis
        {
            get { return pis; }
            set { pis = value.Replace("-", "").Replace(".", "").Replace("/", "").Trim();}
        }
        
        private string listGheSetorAvulso;

        public string ListGheSetorAvulso
        {
            get { return listGheSetorAvulso; }
            set { listGheSetorAvulso = value; }
        }
        /* Não fazem parte do construtor do aso */

        private List<GheSetorAvulso> gheSetorAvulso;

        public List<GheSetorAvulso> GheSetorAvulso
        {
            get { return gheSetorAvulso; }
            set { gheSetorAvulso = value; }
        }

        private List<AsoAgenteRisco> asoAgenteRisco;

        public List<AsoAgenteRisco> AsoAgenteRisco
        {
            get { return asoAgenteRisco; }
            set { asoAgenteRisco = value; }
        }

        private string funcaoFuncionario;

        public string FuncaoFuncionario
        {
            get { return funcaoFuncionario; }
            set { funcaoFuncionario = value; }
        }

        private bool confinado;

        public bool Confinado
        {
            get { return confinado; }
            set { confinado = value; }
        }

        private bool altura;

        public bool Altura
        {
            get { return altura; }
            set { altura = value; }
        }

        private bool eletricidade;

        public bool Eletricidade
        {
            get { return eletricidade; }
            set { eletricidade = value; }
        }

        public Aso() { }

        public Aso(Int64? id) { this.Id = id; }

        public Aso(Int64? id, ClienteFuncaoFuncionario clienteFuncaoFuncionario, String rg)
        {
            this.Id = id;
            this.ClienteFuncaoFuncionario = clienteFuncaoFuncionario;
            this.Rg = rg;
        }

        public Aso(Int64? id, Medico examinador, ClienteFuncaoFuncionario clienteFuncaoFuncionario, String codigo, 
            DateTime? dataElaboracao, Funcionario funcionario, Cliente cliente, Periodicidade periodicidade, String situacao, Medico coordenador,
            Usuario criador, String justificativaAlteracao,  Estudo estudo, Usuario finalizador, Usuario usuarioCancelamento, DateTime? dataFinalizacao, DateTime? dataCancelamento, DateTime? dataGravacao,
            String observacaoAso, String conclusao, String senha, Protocolo protocolo, Empresa empresa, string justificativa, Usuario usuarioAlteracao, CentroCusto centroCusto, DateTime? dataVencimento, string codigoPo, bool prioridade, string pis, string colecaoGheSetorAvulso, string funcaoColaborador)
        {

            this.Id = id;
            this.Examinador = examinador;
            this.ClienteFuncaoFuncionario = clienteFuncaoFuncionario;
            this.Codigo = codigo;
            this.DataElaboracao = dataElaboracao;
            this.Funcionario = funcionario;
            this.Cliente = cliente;
            this.Periodicidade = periodicidade;
            this.Situacao = situacao;
            this.Examinador = examinador;
            this.Coordenador = coordenador;
            this.Criador = criador;
            this.Finalizador = finalizador;
            this.JustificativaAlteracao = justificativaAlteracao;
            this.Estudo = estudo;
            this.UsuarioCancelamento = usuarioCancelamento;
            this.DataFinalizacao = dataFinalizacao;
            this.DataCancelamento = dataCancelamento;
            this.DataGravacao = dataGravacao;
            this.ObservacaoAso = observacaoAso;
            this.Conclusao = conclusao;
            this.Senha = senha;
            this.Protocolo = protocolo;
            this.Empresa = empresa;
            this.Justificativa = justificativa;
            this.UsuarioAlteracao = usuarioAlteracao;
            this.CentroCusto = centroCusto;
            this.DataVencimento = dataVencimento;
            this.CodigoPo = codigoPo;
            this.Prioridade = prioridade;
            this.Pis = pis;
            this.ListGheSetorAvulso = colecaoGheSetorAvulso;
            this.FuncaoFuncionario = funcaoColaborador;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Aso p = obj as Aso;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

        public Boolean isNullForFilter()
        {
            Boolean retorno = Convert.ToBoolean(false);

            if (String.IsNullOrWhiteSpace(this.codigo.Replace(".", "").Trim()) &&
                this.periodicidade == null && String.IsNullOrWhiteSpace(this.situacao.Trim()))
                
                retorno = true;

            return retorno;

        }

    }
}

