﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class ItensCanceladosNf
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private GheFonteAgenteExameAso gheFonteAgenteExameAso;

        public GheFonteAgenteExameAso GheFonteAgenteExameAso
        {
            get { return gheFonteAgenteExameAso; }
            set { gheFonteAgenteExameAso = value; }
        }
        private ClienteFuncaoExameASo clienteFuncaoExameAso;

        public ClienteFuncaoExameASo ClienteFuncaoExameAso
        {
            get { return clienteFuncaoExameAso; }
            set { clienteFuncaoExameAso = value; }
        }
        private Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }
        private ContratoProduto contratoProduto;

        public ContratoProduto ContratoProduto
        {
            get { return contratoProduto; }
            set { contratoProduto = value; }
        }
        private Nfe nfe;

        public Nfe Nfe
        {
            get { return nfe; }
            set { nfe = value; }
        }
        private DateTime? dataInclusao;

        public DateTime? DataInclusao
        {
            get { return dataInclusao; }
            set { dataInclusao = value; }
        }
        private DateTime? dataFaturamento;

        public DateTime? DataFaturamento
        {
            get { return dataFaturamento; }
            set { dataFaturamento = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        private Decimal? precoUnit;

        public Decimal? PrecoUnit
        {
            get { return precoUnit; }
            set { precoUnit = value; }
        }
        private Decimal? valorComissao;

        public Decimal? ValorComissao
        {
            get { return valorComissao; }
            set { valorComissao = value; }
        }
        private Estudo estudo;

        public Estudo Estudo
        {
            get { return estudo; }
            set { estudo = value; }
        }
        private Aso aso;

        public Aso Aso
        {
            get { return aso; }
            set { aso = value; }
        }
        private Int32? quantidade;

        public Int32? Quantidade
        {
            get { return quantidade; }
            set { quantidade = value; }
        }
        private Usuario usuario;

        public Usuario Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }
        private Decimal? custoUnitario;

        public Decimal? CustoUnitario
        {
            get { return custoUnitario; }
            set { custoUnitario = value; }
        }

        public ItensCanceladosNf() { }

        public ItensCanceladosNf
            (
            Int64? id, 
            GheFonteAgenteExameAso gheFonteAgenteExameASo, 
            ClienteFuncaoExameASo clienteFuncaoExameAso,
            Cliente cliente, 
            ContratoProduto contratoProduto, 
            Nfe nfe, 
            DateTime? dataInclusao, 
            DateTime? dataFaturamento,
            String situacao, 
            Decimal? precoUnit, 
            Decimal? valorComissao, 
            Estudo estudo, 
            Aso aso, 
            Int32? quantidade,
            Usuario usuario,
            Decimal? custoUnitario
            )
        {
            this.Id = id;
            this.GheFonteAgenteExameAso = gheFonteAgenteExameAso;
            this.ClienteFuncaoExameAso = clienteFuncaoExameAso;
            this.Cliente = cliente;
            this.ContratoProduto = contratoProduto;
            this.Nfe = nfe;
            this.DataInclusao = dataInclusao;
            this.DataFaturamento = dataFaturamento;
            this.Situacao = situacao;
            this.PrecoUnit = precoUnit;
            this.ValorComissao = valorComissao;
            this.Estudo = estudo;
            this.Aso = aso;
            this.Quantidade = quantidade;
            this.Usuario = usuario;
            this.CustoUnitario = custoUnitario;
        }
        
        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            ItensCanceladosNf p = obj as ItensCanceladosNf;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

    }
}
