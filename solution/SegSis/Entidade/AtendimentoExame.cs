﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class AtendimentoExame
    {
        private Exame exame;

        public Exame Exame
        {
            get { return exame; }
            set { exame = value; }
        }

        private Sala sala;

        public Sala Sala
        {
            get { return sala; }
            set { sala = value; }
        }

        public AtendimentoExame() { }

        public AtendimentoExame(Exame exame, Sala sala)
        {
            this.Exame = exame;
            this.Sala = sala;
        }


    }
}
