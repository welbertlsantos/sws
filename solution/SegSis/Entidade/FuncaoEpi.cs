﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    class FuncaoEpi
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private Estudo estudo;

        public Estudo Estudo
        {
            get { return estudo; }
            set { estudo = value; }
        }
        private String nomeFuncao;

        public String NomeFuncao
        {
            get { return nomeFuncao; }
            set { nomeFuncao = value; }
        }
        private String nomeEpi;

        public String NomeEpi
        {
            get { return nomeEpi; }
            set { nomeEpi = value; }
        }
        private String tipo;

        public String Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }
        private String classificacao;

        public String Classificacao
        {
            get { return classificacao; }
            set { classificacao = value; }
        }


        public FuncaoEpi(Int64? id, Estudo estudo, String nomeFuncao, String nomeEpi, String tipo, String classificacao)
        {
            this.Id = id;
            this.Estudo = estudo;
            this.NomeFuncao = nomeFuncao;
            this.NomeEpi = nomeEpi;
            this.Tipo = tipo;
            this.Classificacao = classificacao;
        }

        public void setClassificacao(String classificacao)
        {
            this.classificacao = classificacao;
        }

    }
}
