﻿using System;

namespace SWS.Entidade
{
    public class MedicoExame
    {
        private long? id;

        public long? Id
        {
            get { return id; }
            set { id = value; }
        }

        private Exame exame;

        public Exame Exame
        {
            get { return exame; }
            set { exame = value; }
        }

        private Medico medico;

        public Medico Medico
        {
            get { return medico; }
            set { medico = value; }
        }

        private DateTime? dataGravacao;

        public DateTime? DataGravacao
        {
            get { return dataGravacao; }
            set { dataGravacao = value; }
        }
        
        public MedicoExame(){}

        public MedicoExame(long? id, Exame exame, Medico medico, DateTime? dataGravacao)
        {
            this.Id = id;
            this.Exame = exame;
            this.Medico = medico;
            this.DataGravacao = dataGravacao;
        }

    }
}
