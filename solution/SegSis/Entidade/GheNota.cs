﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class GheNota
    {

        private Ghe ghe;

        public Ghe Ghe
        {
            get { return ghe; }
            set { ghe = value; }
        }
        private Nota nota;

        public Nota Nota
        {
            get { return nota; }
            set { nota = value; }
        }
        
        public GheNota(){ }

        public GheNota(Ghe ghe, Nota nota)
        {
            this.Ghe = ghe;
            this.Nota = nota;
        }


    }
}
