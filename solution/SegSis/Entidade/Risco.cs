﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Risco
    {

        private Int64 id;

        public Int64 Id
        {
            get { return id; }
            set { id = value; }
        }
        private String descricao;

        public String Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }

        public Risco() { }

        public Risco(Int64 id, String descricao)
        {
            this.Id = id;
            this.Descricao = descricao;
        }


    }
}

