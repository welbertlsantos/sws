﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class ComentarioFuncao
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private Funcao funcao;

        public Funcao Funcao
        {
            get { return funcao; }
            set { funcao = value; }
        }
        private String atividade;

        public String Atividade
        {
            get { return atividade; }
            set { atividade = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }

        public ComentarioFuncao() { }

        public ComentarioFuncao(Int64? id, Funcao funcao, String atividade, String situacao)
        {
            this.Id = id;
            this.Funcao = funcao;
            this.Atividade = atividade;
            this.Situacao = situacao;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            ComentarioFuncao p = obj as ComentarioFuncao;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
        
    }
}
