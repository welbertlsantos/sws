﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Perfil
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private String descricao;

        public String Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }

        private List<Dominio> dominio;

        public List<Dominio> Dominio
        {
            get { return dominio; }
            set { dominio = value; }
        }

        public Perfil(){}
        
        public Perfil(Int64? id, String descricao, String situacao)
        {
            this.Id = id;
            this.Descricao = descricao;
            this.Situacao = situacao;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Perfil p = obj as Perfil;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

        public Boolean isNullForFilter()
        {
            Boolean retorno = Convert.ToBoolean(false);

            if (String.IsNullOrWhiteSpace(this.descricao) && String.IsNullOrWhiteSpace(this.situacao))
            {
                retorno = Convert.ToBoolean(true);
            }

            return retorno;
        }

    }
}
