﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class RelatorioAnualItem
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private RelatorioAnual relatorioAnual;

        public RelatorioAnual RelatorioAnual
        {
            get { return relatorioAnual; }
            set { relatorioAnual = value; }
        }
        private Setor setor;

        public Setor Setor
        {
            get { return setor; }
            set { setor = value; }
        }
        private Exame exame;

        public Exame Exame
        {
            get { return exame; }
            set { exame = value; }
        }
        private Int32 totalExameNormal;

        public Int32 TotalExameNormal
        {
            get { return totalExameNormal; }
            set { totalExameNormal = value; }
        }
        private Int32 totalExameAnormal;

        public Int32 TotalExameAnormal
        {
            get { return totalExameAnormal; }
            set { totalExameAnormal = value; }
        }
        private Int32 previsao;

        public Int32 Previsao
        {
            get { return previsao; }
            set { previsao = value; }
        }
        private Periodicidade tipo;

        public Periodicidade Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }


        public RelatorioAnualItem() { }

        public RelatorioAnualItem(Int64? id,
            RelatorioAnual relatorioAnual,
            Setor setor,
            Exame exame,
            Int32 totalExameNormal,
            Int32 totalExameAnormal,
            Int32 previsao,
            Periodicidade tipo)
        {
            this.Id = id;
            this.RelatorioAnual = relatorioAnual;
            this.Setor = setor;
            this.Exame = exame;
            this.TotalExameNormal = totalExameNormal;
            this.TotalExameAnormal = totalExameAnormal;
            this.Previsao = previsao;
            this.Tipo = tipo;
        }


        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            RelatorioAnualItem p = obj as RelatorioAnualItem;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
    }
}
