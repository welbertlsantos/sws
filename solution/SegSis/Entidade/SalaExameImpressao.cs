﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class SalaExameImpressao
    {
        private string sala;

        public string Sala
        {
            get { return sala; }
            set { sala = value; }
        }

        private string exame;

        public string Exame
        {
            get { return exame; }
            set { exame = value; }
        }

        public SalaExameImpressao(){}

        public SalaExameImpressao(string sala, string exame)
        {
            this.Sala = sala;
            this.Exame = exame;
        }
    }
}
