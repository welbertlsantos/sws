﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Sala
    {

        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private String descricao;

        public String Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        private Int32 andar;

        public Int32 Andar
        {
            get { return andar; }
            set { andar = value; }
        }
        private Boolean vip;

        public Boolean Vip
        {
            get { return vip; }
            set { vip = value; }
        }
        private List<SalaExame> exames = new List<SalaExame>() { };

        public List<SalaExame> Exames
        {
            get { return exames; }
            set { exames = value; }
        }

        private Empresa empresa;

        public Empresa Empresa
        {
            get { return empresa; }
            set { empresa = value; }
        }

        private string slugSala;

        public string SlugSala
        {
            get { return slugSala; }
            set { slugSala = value; }
        }

        private int qtdeChamada;

        public int QtdeChamada
        {
            get { return qtdeChamada; }
            set { qtdeChamada = value; }
        }

        public Sala() { }

        public Sala(Int64? id, String descricao, String situacao, Int32 andar, Boolean vip, Empresa empresa, string slugSala, int qtdeChamada )
        {
            this.Id = id;
            this.Descricao = descricao;
            this.Situacao = situacao;
            this.Andar = andar;
            this.Vip = vip;
            this.Empresa = empresa;
            this.SlugSala = slugSala;
            this.QtdeChamada = qtdeChamada;
        }

        public Boolean isNullForFilter()
        {
            Boolean retorno = Convert.ToBoolean(false);

            if (String.IsNullOrWhiteSpace(this.descricao) && String.IsNullOrWhiteSpace(this.situacao) && empresa == null)
                retorno = Convert.ToBoolean(true);

            return retorno;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.

            Sala p = obj as Sala;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }


    }
}