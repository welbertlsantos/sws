﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class LoteEsocial
    {
        public Int64? Id { get; set; }
        public Cliente cliente { get; set; }
        public string Tipo { get; set; }
        public DateTime? DataCriacao { get; set; }
        public string codigo { get; set; }
        public List<Aso> asos { get; set; }
        public DateTime? periodo { get; set; }

        public LoteEsocial(long? id, Cliente cliente, string tipo, DateTime? dataCriacao, string codigo, DateTime? periodo)
        {
            this.Id = id;
            this.cliente = cliente;
            this.Tipo = tipo;
            this.DataCriacao = dataCriacao;
            this.codigo = codigo;
            this.periodo = periodo;
        }

        public LoteEsocial() { }

        public LoteEsocial(long id) { this.Id = id; }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            LoteEsocial p = obj as LoteEsocial;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.Id == p.Id);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public bool isNullForFilter()
        {
            bool retorno = false;

            if (this.cliente == null)
            {
                retorno = true;
            }

            return retorno;
        }

        
    }
}
