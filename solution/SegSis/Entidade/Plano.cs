﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Plano
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private String descricao;

        public String Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        private Boolean carga;

        public Boolean Carga
        {
            get { return carga; }
            set { carga = value; }
        }
        private List<Parcela> parcelas = new List<Parcela>();

        public List<Parcela> Parcelas
        {
            get { return parcelas; }
            set { parcelas = value; }
        }
        private List<PlanoForma> formas = new List<PlanoForma>();

        public List<PlanoForma> Formas
        {
            get { return formas; }
            set { formas = value; }
        }

        public Plano() { }

        public Plano(Int64? id, String descricao, String situacao, Boolean carga)
        {
            this.Id = id;
            this.Descricao = descricao;
            this.Situacao = situacao;
            this.Carga = carga;
        }       

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Plano p = obj as Plano;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

        public Boolean isNullForFilter()
        {
            Boolean retorno = false;

            if (String.IsNullOrEmpty(descricao) && String.IsNullOrEmpty(situacao))
            {
                retorno = true;
            }

            return retorno;
        }

    }
}
