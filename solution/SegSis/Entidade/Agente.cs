﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Agente
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private Risco risco;

        public Risco Risco
        {
            get { return risco; }
            set { risco = value; }
        }
        private String descricao;

        public String Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }
        private String trajetoria;

        public String Trajetoria
        {
            get { return trajetoria; }
            set { trajetoria = value; }
        }
        private String dano;

        public String Dano
        {
            get { return dano; }
            set { dano = value; }
        }
        private String limite;

        public String Limite
        {
            get { return limite; }
            set { limite = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        private Boolean privado;

        public Boolean Privado
        {
            get { return privado; }
            set { privado = value; }
        }
        private string intensidade;

        public string Intensidade
        {
            get { return intensidade; }
            set { intensidade = value; }
        }
        private string tecnica;

        public string Tecnica
        {
            get { return tecnica; }
            set { tecnica = value; }
        }

        private string codigoEsocial;

        public string CodigoEsocial
        {
            get { return codigoEsocial; }
            set { codigoEsocial = value; }
        }

        public Agente()
        { }

        public Agente(Int64? id, Risco risco, String descricao, String trajetoria, String dano, String limite, String situacao, Boolean privado, string intensidade, string tecnica, string codigoEsocial)
        {
            this.Id = id;
            this.Risco = risco;
            this.Descricao = descricao;
            this.Trajetoria = trajetoria;
            this.Dano = dano;
            this.Limite = limite;
            this.Situacao = situacao;
            this.Privado  = privado;
            this.Intensidade = intensidade;
            this.Tecnica = tecnica;
            this.CodigoEsocial = codigoEsocial;
                
        }

        public Boolean isNullForFilter()
        {
            Boolean retorno = Convert.ToBoolean(false);

            if (String.IsNullOrWhiteSpace(this.descricao) && String.IsNullOrWhiteSpace(situacao))
            {
                if (this.risco == null)
                {
                    retorno = Convert.ToBoolean(true);
                }
            }

            return retorno;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Agente p = obj as Agente;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }
                
        public override int GetHashCode()
        {
            return id.GetHashCode();
        }


    }
}
