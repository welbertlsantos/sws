﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Produto
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private String nome;

        public String Nome
        {
            get { return nome; }
            set { nome = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        private Decimal? preco;

        public Decimal? Preco
        {
            get { return preco; }
            set { preco = value; }
        }
        
        private String tipo;

        public String Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }
        
        private Decimal? custo;

        public Decimal? Custo
        {
            get { return custo; }
            set { custo = value; }
        }

        private bool padraoContrato;

        public bool PadraoContrato
        {
            get { return padraoContrato; }
            set { padraoContrato = value; }
        }

        private string descricao;

        public string Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }

        public Produto() { }

        public Produto(Int64? id, String nome, String situacao, Decimal? preco, String tipo, Decimal? custo, bool padraoContrato, string descricao)
        {
            this.Id = id;
            this.Nome = nome;
            this.Situacao = situacao;
            this.Preco = preco;
            this.Tipo = tipo;
            this.Custo = custo;
            this.PadraoContrato = padraoContrato;
            this.Descricao = descricao;
        }

        public Boolean isNullForFilter()
        {
            Boolean retorno = false;

            if (String.IsNullOrEmpty(nome) && String.IsNullOrEmpty(situacao))
            {
                retorno = true;
            }

            return retorno;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Produto p = obj as Produto;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

    }
}
