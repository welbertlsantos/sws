﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;


namespace SWS.Entidade
{
    public class CronogramaAtividade
    {
        private Atividade atividade;

        public Atividade Atividade
        {
            get { return atividade; }
            set { atividade = value; }
        }
        private Cronograma cronograma;

        public Cronograma Cronograma
        {
            get { return cronograma; }
            set { cronograma = value; }
        }
        private String mesRealizado;

        public String MesRealizado
        {
            get { return mesRealizado; }
            set { mesRealizado = value; }
        }
        private String anoRealizado;

        public String AnoRealizado
        {
            get { return anoRealizado; }
            set { anoRealizado = value; }
        }
        private String publicoAlvo;

        public String PublicoAlvo
        {
            get { return publicoAlvo; }
            set { publicoAlvo = value; }
        }
        private String evidencia;

        public String Evidencia
        {
            get { return evidencia; }
            set { evidencia = value; }
        }

        public CronogramaAtividade() { }

        public CronogramaAtividade(Atividade atividade, Cronograma cronograma, String mesRealizado, String anoRealizado,
            String publicoAlvo, String evidencia)
        {
            this.Atividade = atividade;
            this.Cronograma = cronograma;
            this.MesRealizado = mesRealizado;
            this.AnoRealizado = anoRealizado;
            this.PublicoAlvo = publicoAlvo;
            this.Evidencia = evidencia;
        }
        
        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            CronogramaAtividade p = obj as CronogramaAtividade;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            if ((p.Cronograma == null))
            {
                return (this.atividade.Id == p.atividade.Id);
            }
            else
            {
                if ((p.Atividade == null))
                {
                    return (this.Cronograma.Id == p.Cronograma.Id);
                }
                else
                {
                    return (this.atividade.Id == p.atividade.Id && this.Cronograma.Id == p.Cronograma.Id);

                }
            }
        }

        public override int GetHashCode()
        {
            return atividade.Id.GetHashCode();
        }
    
    
    }
}
