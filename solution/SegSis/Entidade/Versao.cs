﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    class Versao
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private Estudo estudo;

        public Estudo Estudo
        {
            get { return estudo; }
            set { estudo = value; }
        }
        private String numeroRevisao;

        public String NumeroRevisao
        {
            get { return numeroRevisao; }
            set { numeroRevisao = value; }
        }
        private String comentario;

        public String Comentario
        {
            get { return comentario; }
            set { comentario = value; }
        }
        private DateTime? dataCriacao;

        public DateTime? DataCriacao
        {
            get { return dataCriacao; }
            set { dataCriacao = value; }
        }

        public Versao(Int64? id, Estudo estudo, String numeroRevisao, String comentario, DateTime? dataCriacao)
        {
            this.Id = id;
            this.Estudo = estudo;
            this.NumeroRevisao = numeroRevisao;
            this.Comentario = comentario;
            this.DataCriacao = dataCriacao;
        }
        
    }
}
