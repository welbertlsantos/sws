﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class ClienteArquivo
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }
        private Arquivo arquivo;

        public Arquivo Arquivo
        {
            get { return arquivo; }
            set { arquivo = value; }
        }
        private DateTime? dataCriacao;

        public DateTime? DataCriacao
        {
            get { return dataCriacao; }
            set { dataCriacao = value; }
        }
        private DateTime? dataFinalizacao;

        public DateTime? DataFinalizacao
        {
            get { return dataFinalizacao; }
            set { dataFinalizacao = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }

        public ClienteArquivo(Int64? id, Arquivo arquivo, Cliente cliente, DateTime? dataCriacao, DateTime? dataFinalizacao, String situacao)
        {
            this.Id = id;
            this.Cliente = cliente;
            this.Arquivo = arquivo;
            this.DataCriacao = dataCriacao;
            this.DataFinalizacao = dataFinalizacao;
            this.Situacao = situacao;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            ClienteArquivo p = obj as ClienteArquivo;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }
        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
    }
}
