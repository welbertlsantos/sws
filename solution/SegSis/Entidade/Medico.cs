﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Medico
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private String nome;

        public String Nome
        {
            get { return nome; }
            set { nome = value; }
        }
        private String crm;

        public String Crm
        {
            get { return crm; }
            set { crm = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        private String telefone1;

        public String Telefone1
        {
            get { return telefone1; }
            set { telefone1 = value; }
        }
        private String telefone2;

        public String Telefone2
        {
            get { return telefone2; }
            set { telefone2 = value; }
        }
        private String email;

        public String Email
        {
            get { return email; }
            set { email = value; }
        }
        private String endereco;

        public String Endereco
        {
            get { return endereco; }
            set { endereco = value; }
        }
        private String numero;

        public String Numero
        {
            get { return numero; }
            set { numero = value; }
        }
        private String complemento;

        public String Complemento
        {
            get { return complemento; }
            set { complemento = value; }
        }
        private String bairro;

        public String Bairro
        {
            get { return bairro; }
            set { bairro = value; }
        }
        private String cep;

        public String Cep
        {
            get { return cep; }
            set { cep = value.Replace(".", "").Replace("-", "").Trim(); }
        }
        
        private String uf;

        public String Uf
        {
            get { return uf; }
            set { uf = value; }
        }

        private CidadeIbge cidade;

        public CidadeIbge Cidade
        {
            get { return cidade; }
            set { cidade = value; }
        }

        private string pispasep;

        public string Pispasep
        {
            get { return pispasep; }
            set { pispasep = value.Replace(".","").Replace(",","").Replace("-","").Trim(); }
        }

        private string ufCrm;

        public string UfCrm
        {
            get { return ufCrm; }
            set { ufCrm = value; }
        }

        private string documentoExtra;

        public string DocumentoExtra
        {
            get { return documentoExtra; }
            set { documentoExtra = value.Trim(); }
        }

        private byte[] assinatura;

        public byte[] Assinatura
        {
            get { return assinatura; }
            set { assinatura = value; }
        }

        private string mimetype;

        public string Mimetype
        {
            get { return mimetype; }
            set { mimetype = value; }
        }

        private string cpf;

        public string Cpf
        {
            get { return cpf; }
            set { cpf = value.Replace(".","").Replace(",","").Replace("-","").Trim();}
        }

        private string rqe;

        public string Rqe
        {
            get { return rqe; }
            set { rqe = value; }
        }

        public Medico() { }

        public Medico(Int64? id)
        {
            this.Id = id;
        }


        public Medico(Int64? id, String nome, String crm, String situacao, String telefone1, String telefone2,
            String email, String endereco, String numero, String complemento, String bairro, String cep, 
            String uf, CidadeIbge cidadeIbge, string pisPasep, string ufCrm, string documentoExtra, byte[] assinatura, string mimetype, string cpf, string rqe )
        {
            this.Id = id;
            this.Nome = nome;
            this.Crm = crm; 
            this.Situacao = situacao;
            this.Telefone1 = telefone1;
            this.Telefone2 = telefone2;
            this.Email = email;
            this.Endereco = endereco;
            this.Numero= numero;
            this.Complemento = complemento;
            this.Bairro = bairro;
            this.Cep = cep;
            this.Uf = uf;
            this.Cidade = cidadeIbge;
            this.Pispasep = pisPasep;
            this.UfCrm = ufCrm;
            this.DocumentoExtra = documentoExtra;
            this.Assinatura = assinatura;
            this.Mimetype = mimetype;
            this.Cpf = cpf;
            this.Rqe = rqe;

        }

        public Boolean isNullForFilter()
        {
            Boolean retorno = Convert.ToBoolean(false);

            if (String.IsNullOrWhiteSpace(this.nome) && String.IsNullOrWhiteSpace(this.crm) && String.IsNullOrWhiteSpace(situacao))
            {
                retorno = Convert.ToBoolean(true);
            }

            return retorno;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Medico p = obj as Medico;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
    
    }
}
