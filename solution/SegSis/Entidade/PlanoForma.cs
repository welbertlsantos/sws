﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class PlanoForma
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private Forma forma;

        public Forma Forma
        {
            get { return forma; }
            set { forma = value; }
        }
        private Plano plano;

        public Plano Plano
        {
            get { return plano; }
            set { plano = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }

        public PlanoForma() { }

        public PlanoForma(Int64? id, Forma forma, Plano plano, String situacao)
        {
            this.Id = id;
            this.Forma = forma;
            this.Plano = plano;
            this.Situacao = situacao;

        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            PlanoForma p = obj as PlanoForma;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }


    }
}
