﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;

namespace SWS.Entidade
{
    public class Cnae
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private String codCnae;

        public String CodCnae
        {
            get { return codCnae; }
            set { codCnae = value.Replace("-", "").Replace(".", "").Replace("/", "").Trim(); }
        }
        private String atividade;

        public String Atividade
        {
            get { return atividade; }
            set { atividade = value; }
        }
        private String grauRisco;

        public String GrauRisco
        {
            get { return grauRisco; }
            set { grauRisco = value.Replace("-", "");}
        }
        private string grupo;

        public string Grupo
        {
            get { return grupo; }
            set { grupo = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }

        public Cnae() {}

        public Cnae(Int64? idCnae)
        {
            this.Id = idCnae;
        }

        public Cnae(Int64? id, String codCnae, String atividade,String grauRisco, String grupo, String situacao)
        {
            this.Id = id;
            this.CodCnae = codCnae;
            this.Atividade = atividade;
            this.GrauRisco = grauRisco;
            this.Grupo = grupo;
            this.Situacao = situacao;
        }

        public Boolean isNullForFilter()
        {
            Boolean retorno = Convert.ToBoolean(false);

            if (String.IsNullOrWhiteSpace(this.codCnae) && String.IsNullOrWhiteSpace(this.atividade) &&
                String.IsNullOrWhiteSpace(situacao))
            {
                retorno = Convert.ToBoolean(true);
            }

            return retorno;
        }
        
        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Cnae p = obj as Cnae;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
    }

}
