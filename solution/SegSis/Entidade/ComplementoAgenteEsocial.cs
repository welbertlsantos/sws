﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class ComplementoAgenteEsocial
    {
        private long? id;

        public long? Id
        {
            get { return id; }
            set { id = value; }
        }
        private string codigo;

        public string Codigo
        {
            get { return codigo; }
            set { codigo = value; }
        }
        private string descricao;

        public string Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }

        public ComplementoAgenteEsocial() { }
        public ComplementoAgenteEsocial(long? id, string codigo, string descricao)
        {
            this.Id = id;
            this.Codigo = codigo;
            this.Descricao = descricao;
        }
    }
}
