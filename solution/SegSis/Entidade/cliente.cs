﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.View.Entidade;

namespace SWS.Entidade
{
    public class Cliente
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private String razaoSocial;

        public String RazaoSocial
        {
            get { return razaoSocial; }
            set { razaoSocial = value.Trim(); }
        }
        private String fantasia;

        public String Fantasia
        {
            get { return fantasia; }
            set { fantasia = value.Trim(); }
        }
        private String cnpj;

        public String Cnpj
        {
            get { return cnpj; }
            set { cnpj = value.Replace(".", "").Replace(",", "").Replace("-", "").Replace("/", "").Trim(); }
        }
        private String inscricao;

        public String Inscricao
        {
            get { return inscricao; }
            set { inscricao = value; }
        }
        private String endereco;

        public String Endereco
        {
            get { return endereco; }
            set { endereco = value; }
        }
        private String numero;

        public String Numero
        {
            get { return numero; }
            set { numero = value; }
        }
        private String complemento;

        public String Complemento
        {
            get { return complemento; }
            set { complemento = value; }
        }
        private String bairro;

        public String Bairro
        {
            get { return bairro; }
            set { bairro = value; }
        }
        private String cep;

        public String Cep
        {
            get { return cep; }
            set { cep = value.Replace(".", "").Replace(",", "").Replace("-", "").Trim(); }
        }
        private String uf;

        public String Uf
        {
            get { return uf; }
            set { uf = value; }
        }
        private String telefone1;

        public String Telefone1
        {
            get { return telefone1; }
            set { telefone1 = value; }
        }
        private String telefone2;

        public String Telefone2
        {
            get { return telefone2; }
            set { telefone2 = value; }
        }
        private String email;

        public String Email
        {
            get { return email; }
            set { email = value; }
        }
        private String site;

        public String Site
        {
            get { return site; }
            set { site = value; }
        }
        private DateTime? dataCadastro;

        public DateTime? DataCadastro
        {
            get { return dataCadastro; }
            set { dataCadastro = value; }
        }
        private String responsavel;

        public String Responsavel
        {
            get { return responsavel; }
            set { responsavel = value; }
        }
        private String cargo;

        public String Cargo
        {
            get { return cargo; }
            set { cargo = value; }
        }
        private String documento;

        public String Documento
        {
            get { return documento; }
            set { documento = value; }
        }
        private String escopo;

        public String Escopo
        {
            get { return escopo; }
            set { escopo = value; }
        }
        private String jornada;

        public String Jornada
        {
            get { return jornada; }
            set { jornada = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        private Int32 estMasc;

        public Int32 EstMasc
        {
            get { return estMasc; }
            set { estMasc = value; }
        }
        private Int32 estFem;

        public Int32 EstFem
        {
            get { return estFem; }
            set { estFem = value; }
        }
        private String enderecoCob;

        public String EnderecoCob
        {
            get { return enderecoCob; }
            set { enderecoCob = value; }
        }
        private String numeroCob;

        public String NumeroCob
        {
            get { return numeroCob; }
            set { numeroCob = value; }
        }
        private String complementoCob;

        public String ComplementoCob
        {
            get { return complementoCob; }
            set { complementoCob = value; }
        }
        private String bairroCob;

        public String BairroCob
        {
            get { return bairroCob; }
            set { bairroCob = value; }
        }
        private String cepCob;

        public String CepCob
        {
            get { return cepCob; }
            set { cepCob = value.Replace(".", "").Replace(",", "").Replace("-", "").Trim(); }
        }
        private String ufCob;

        public String UfCob
        {
            get { return ufCob; }
            set { ufCob = value; }
        }
        private String telefoneCob;

        public String TelefoneCob
        {
            get { return telefoneCob; }
            set { telefoneCob = value; }
        }
        private Medico coordenador;

        public Medico Coordenador
        {
            get { return coordenador; }
            set { coordenador = value; }
        }
        private String telefone2Cob;

        public String Telefone2Cob
        {
            get { return telefone2Cob; }
            set { telefone2Cob = value; }
        }
        private Boolean vip;

        public Boolean Vip
        {
            get { return vip; }
            set { vip = value; }
        }
        private String telefoneContato;

        public String TelefoneContato
        {
            get { return telefoneContato; }
            set { telefoneContato = value; }
        }
        private Boolean usaContrato;

        public Boolean UsaContrato
        {
            get { return usaContrato; }
            set { usaContrato = value; }
        }
        private Cliente matriz;

        public Cliente Matriz
        {
            get { return matriz; }
            set { matriz = value; }
        }
        private Cliente credenciadaCliente;

        public Cliente CredenciadaCliente
        {
            get { return credenciadaCliente; }
            set { credenciadaCliente = value; }
        }
        private Boolean particular;

        public Boolean Particular
        {
            get { return particular; }
            set { particular = value; }
        }
        private Boolean unidade;

        public Boolean Unidade
        {
            get { return unidade; }
            set { unidade = value; }
        }
        private Boolean credenciada;

        public Boolean Credenciada
        {
            get { return credenciada; }
            set { credenciada = value; }
        }
        private Boolean prestador;

        public Boolean Prestador
        {
            get { return prestador; }
            set { prestador = value; }
        }
        private CidadeIbge cidadeIbge;

        public CidadeIbge CidadeIbge
        {
            get { return cidadeIbge; }
            set { cidadeIbge = value; }
        }
        private CidadeIbge cidadeIbgeCobranca;

        public CidadeIbge CidadeIbgeCobranca
        {
            get { return cidadeIbgeCobranca; }
            set { cidadeIbgeCobranca = value; }
        }
        private Boolean destacaIss;

        public Boolean DestacaIss
        {
            get { return destacaIss; }
            set { destacaIss = value; }
        }
        private Boolean geraCobrancaValorLiquido;

        public Boolean GeraCobrancaValorLiquido
        {
            get { return geraCobrancaValorLiquido; }
            set { geraCobrancaValorLiquido = value; }
        }
        private Decimal? aliquotaIss;

        public Decimal? AliquotaIss
        {
            get { return aliquotaIss; }
            set { aliquotaIss = value; }
        }
        private List<ClienteCnae> clienteCnae = new List<ClienteCnae>();

        public List<ClienteCnae> ClienteCnae
        {
            get { return clienteCnae; }
            set { clienteCnae = value; }
        }
        private List<VendedorCliente> vendedorCliente = new List<VendedorCliente>();

        public List<VendedorCliente> VendedorCliente
        {
            get { return vendedorCliente; }
            set { vendedorCliente = value; }
        }
        private List<ClienteFuncao> funcoes = new List<ClienteFuncao>() { };

        public List<ClienteFuncao> Funcoes
        {
            get { return funcoes; }
            set { funcoes = value; }
        }
        private List<Contato> contatos = new List<Contato>();

        public List<Contato> Contatos
        {
            get { return contatos; }
            set { contatos = value; }
        }
        
        private Ramo ramo;

        public Ramo Ramo
        {
            get { return ramo; }
            set { ramo = value; }
        }

        private string codigoCnes;

        public string CodigoCnes
        {
            get { return codigoCnes; }
            set { codigoCnes = value; }
        }

        private bool usaCentroCusto;

        public bool UsaCentroCusto
        {
            get { return usaCentroCusto; }
            set { usaCentroCusto = value; }
        }

        private bool bloqueado;

        public bool Bloqueado
        {
            get { return bloqueado; }
            set { bloqueado = value; }
        }

        private bool credenciadora;

        public bool Credenciadora
        {
            get { return credenciadora; }
            set { credenciadora = value; }
        }

        private List<CentroCusto> centroCusto = new List<CentroCusto>();

        public List<CentroCusto> CentroCusto
        {
            get { return centroCusto; }
            set { centroCusto = value; }
        }

        private bool fisica;

        public bool Fisica
        {
            get { return fisica; }
            set { fisica = value; }
        }

        private bool usaPo;

        public bool UsaPo
        {
            get { return usaPo; }
            set { usaPo = value; }
        }

        public Cliente() {}

        public Cliente(Int64? id)
        {
            this.Id = id;
        }

        private bool simplesNacional;

        public bool SimplesNacional
        {
            get { return simplesNacional; }
            set { simplesNacional = value; }
        }
                
        public Cliente(Int64? id, String razaoSocial, String fantasia, String cnpj, String inscricao, 
            String endereco, String numero, String complemento, String bairro, 
            String cep, String uf, String telefone1, String telefone2, String email, String site, 
            DateTime? dataCadastro, String responsavel, String cargo, String documento, String escopo, 
            String jornada, String situacao, Int32 estMasc, Int32 estFem, String enderecoCob, 
            String numeroCob, String complementoCob, String bairroCob, String cepCob, String ufCob,
            String telefoneCob, Medico coordenador, String FaxCob, Boolean vip, String telefoneContato, Boolean usaContrato,
            Cliente matriz, Cliente credenciadaCliente, Boolean particular, Boolean unidade, Boolean credenciada, Boolean prestador, 
            CidadeIbge cidadeIbge, CidadeIbge cidadeIbgeCobranca, Boolean destacaIss, Boolean geraCobVlrLiq, Decimal? aliquotaIss, string codigoCnes, 
            bool usaCentroCusto, bool bloqueado, bool credenciadora, bool fisica, bool usaPo, bool simplesNacional)
        {
            
            this.Id = id;
            this.RazaoSocial = razaoSocial;
            this.Fantasia = fantasia;
            this.Cnpj = cnpj;
            this.Inscricao = inscricao;
            this.Endereco = endereco;
            this.Numero = numero;
            this.Complemento = complemento;
            this.Bairro = bairro;
            this.Cep = cep;
            this.Uf = uf;
            this.Telefone1 = telefone1;
            this.Telefone2 = telefone2;
            this.Email = email;
            this.Site = site;
            this.DataCadastro = dataCadastro;
            this.Responsavel = responsavel;
            this.Cargo = cargo;
            this.Documento = documento;
            this.Escopo = escopo;
            this.Jornada = jornada;
            this.Situacao = situacao;
            this.EstMasc = estMasc;
            this.EstFem = estFem;
            this.EnderecoCob = enderecoCob;
            this.NumeroCob = numeroCob;
            this.ComplementoCob = complementoCob;
            this.BairroCob = bairroCob;
            this.CepCob = cepCob;
            this.UfCob = ufCob;
            this.TelefoneCob = telefoneCob;
            this.Coordenador = coordenador;
            this.Telefone2Cob = FaxCob;
            this.Vip = vip;
            this.TelefoneContato = telefoneContato;
            this.UsaContrato = usaContrato;
            this.Matriz = matriz;
            this.CredenciadaCliente = credenciadaCliente;
            this.Particular = particular;
            this.Unidade = unidade;
            this.Credenciada = credenciada;
            this.Prestador = prestador;
            this.CidadeIbge = cidadeIbge;
            this.CidadeIbgeCobranca = cidadeIbgeCobranca;
            this.DestacaIss = destacaIss;
            this.GeraCobrancaValorLiquido = geraCobVlrLiq;
            this.AliquotaIss = aliquotaIss;
            this.codigoCnes = codigoCnes;
            this.UsaCentroCusto = usaCentroCusto;
            this.Bloqueado = bloqueado;
            this.Credenciadora = credenciadora;
            this.Fisica = fisica;
            this.UsaPo = usaPo;
            this.SimplesNacional = simplesNacional;
        }

        public Boolean isNullForFilter()
        {
            Boolean retorno = Convert.ToBoolean(false);

            if (String.IsNullOrWhiteSpace(this.razaoSocial) && String.IsNullOrWhiteSpace(this.fantasia) &&
                String.IsNullOrWhiteSpace(this.cnpj) && this.cidadeIbge == null &&
                String.IsNullOrWhiteSpace(this.uf) && String.IsNullOrWhiteSpace(Convert.ToString(dataCadastro)) &&
                string.IsNullOrWhiteSpace(this.situacao))
            {
                retorno = Convert.ToBoolean(true);
            }

            return retorno;
        }

        public Boolean isNullForFilterSelect()
        {
            Boolean retorno = Convert.ToBoolean(false);

            if (String.IsNullOrWhiteSpace(this.razaoSocial) && String.IsNullOrWhiteSpace(this.fantasia))
            {
                retorno = Convert.ToBoolean(true);
            }

            return retorno;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Cliente p = obj as Cliente;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
        
        public int tipoCliente()
        {
            int tipoCliente = 0;
            if (this.Cnpj.Length == 14) tipoCliente = 1;
            else tipoCliente = 2;
            return tipoCliente;
        }
    }

}
