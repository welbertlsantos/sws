﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Banco
    {


        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private String nome;

        public String Nome
        {
            get { return nome; }
            set { nome = value; }
        }
        private String codigoBanco;

        public String CodigoBanco
        {
            get { return codigoBanco; }
            set { codigoBanco = value; }
        }
        private Boolean? caixa;

        public Boolean? Caixa
        {
            get { return caixa; }
            set { caixa = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        private Boolean privado;

        public Boolean Privado
        {
            get { return privado; }
            set { privado = value; }
        }
        private Boolean? boleto;

        public Boolean? Boleto
        {
            get { return boleto; }
            set { boleto = value; }
        }
        
        public Banco() { }

        public Banco(Int64? id, String nome, String codigoBanco, Boolean? caixa, String situacao, Boolean privado, Boolean? boleto)
        {
            this.Id = id;
            this.Nome = nome;
            this.CodigoBanco = codigoBanco;
            this.Caixa = caixa;
            this.Situacao = situacao;
            this.Privado = privado;
            this.Boleto = boleto;
        }
        
        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Banco p = obj as Banco;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.Id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

    }
}
