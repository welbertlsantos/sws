﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Log
    {

        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private String tabela;

        public String Tabela
        {
            get { return tabela; }
            set { tabela = value; }
        }
        private Int64? idTabela;

        public Int64? IdTabela
        {
            get { return idTabela; }
            set { idTabela = value; }
        }
        private DateTime? dataAlteracao;

        public DateTime? DataAlteracao
        {
            get { return dataAlteracao; }
            set { dataAlteracao = value; }
        }
        private String atributo;

        public String Atributo
        {
            get { return atributo; }
            set { atributo = value; }
        }
        private String valorAnterior;

        public String ValorAnterior
        {
            get { return valorAnterior; }
            set { valorAnterior = value; }
        }
        private String valorAlterado;

        public String ValorAlterado
        {
            get { return valorAlterado; }
            set { valorAlterado = value; }
        }
        private String usuario;

        public String Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }

        public Log() { }

        public Log(Int64? id,
            String tabela,
            Int64? idTabela,
            DateTime? dataAlteracao,
            String atributo,
            String valorAnterior,
            String valorAlterado,
            String usuario
        )
        {
            this.Id = id;
            this.Tabela = tabela;
            this.IdTabela = idTabela;
            this.DataAlteracao = dataAlteracao;
            this.Atributo = atributo;
            this.ValorAnterior = valorAnterior;
            this.ValorAlterado = valorAlterado;
            this.Usuario = usuario;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Log p = obj as Log;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

    }
}
