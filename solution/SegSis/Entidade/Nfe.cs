﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Nfe
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private Usuario usuarioCriador;

        public Usuario UsuarioCriador
        {
            get { return usuarioCriador; }
            set { usuarioCriador = value; }
        }
        private Empresa empresa;

        public Empresa Empresa
        {
            get { return empresa; }
            set { empresa = value; }
        }
        private long? numeroNfe;

        public long? NumeroNfe
        {
            get { return numeroNfe; }
            set { numeroNfe = value; }
        }
        private Decimal valorTotalNf;

        public Decimal ValorTotalNf
        {
            get { return valorTotalNf; }
            set { valorTotalNf = value; }
        }
        private DateTime dataEmissao;

        public DateTime DataEmissao
        {
            get { return dataEmissao; }
            set { dataEmissao = value; }
        }
        private Decimal? valorPis;

        public Decimal? ValorPis
        {
            get { return valorPis; }
            set { valorPis = value; }
        }
        private Decimal? valorCofins;

        public Decimal? ValorCofins
        {
            get { return valorCofins; }
            set { valorCofins = value; }
        }
        private Decimal baseCalculo;

        public Decimal BaseCalculo
        {
            get { return baseCalculo; }
            set { baseCalculo = value; }
        }
        private Decimal aliquotaIss;

        public Decimal AliquotaIss
        {
            get { return aliquotaIss; }
            set { aliquotaIss = value; }
        }
        private Decimal? valorIr;

        public Decimal? ValorIr
        {
            get { return valorIr; }
            set { valorIr = value; }
        }
        private Decimal? valorIss;

        public Decimal? ValorIss
        {
            get { return valorIss; }
            set { valorIss = value; }
        }
        private Decimal? valorCsll;

        public Decimal? ValorCsll
        {
            get { return valorCsll; }
            set { valorCsll = value; }
        }
        private Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }
        private DateTime dataGravacao;

        public DateTime DataGravacao
        {
            get { return dataGravacao; }
            set { dataGravacao = value; }
        }
        private DateTime? dataCancelamento;

        public DateTime? DataCancelamento
        {
            get { return dataCancelamento; }
            set { dataCancelamento = value; }
        }
        private String motivoCancelamento;

        public String MotivoCancelamento
        {
            get { return motivoCancelamento; }
            set { motivoCancelamento = value; }
        }
        private Usuario usuarioCancelamento;

        public Usuario UsuarioCancelamento
        {
            get { return usuarioCancelamento; }
            set { usuarioCancelamento = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        private PlanoForma planoForma;

        public PlanoForma PlanoForma
        {
            get { return planoForma; }
            set { planoForma = value; }
        }
        private Decimal? valorLiquido;

        public Decimal? ValorLiquido
        {
            get { return valorLiquido; }
            set { valorLiquido = value; }
        }
        private List<Movimento> listaMovimento = new List<Movimento>() { };

        public List<Movimento> ListaMovimento
        {
            get { return listaMovimento; }
            set { listaMovimento = value; }
        }
        private LinkedList<Cobranca> listaCobranca = new LinkedList<Cobranca>() { };

        public LinkedList<Cobranca> ListaCobranca
        {
            get { return listaCobranca; }
            set { listaCobranca = value; }
        }

        private CentroCusto centroCusto;

        public CentroCusto CentroCusto
        {
            get { return centroCusto; }
            set { centroCusto = value; }
        }
        
        public Nfe() { }

        public Nfe(Int64? id, Usuario usuarioCriador, Empresa empresa, long? numeroNfe,
            Decimal valorTotalNf, DateTime dataEmissao, Decimal? valorPis,
            Decimal? valorCofins, Decimal baseCalculo, Decimal aliquotaIss, Decimal? valorIr,
            Decimal? valorInss, Decimal? valorCsll, Cliente cliente, DateTime dataGravacao, DateTime? dataCancelamento, 
            String motivoCancelamento, Usuario usuarioCancelamento,
            String situacao, PlanoForma planoForma, Decimal? valorLiquido,  CentroCusto centroCusto)
        {
            this.Id = id;
            this.Empresa = empresa;
            this.UsuarioCriador = usuarioCriador;
            this.NumeroNfe = numeroNfe;
            this.ValorTotalNf = valorTotalNf;
            this.DataEmissao = dataEmissao;
            this.ValorPis = valorPis;
            this.ValorCofins = valorCofins;
            this.BaseCalculo = baseCalculo;
            this.AliquotaIss = aliquotaIss;
            this.ValorIr = valorIr;
            this.ValorIss = valorInss;
            this.ValorCsll = valorCsll;
            this.Cliente = cliente;
            this.DataGravacao = dataGravacao;
            this.DataCancelamento = dataCancelamento;
            this.UsuarioCancelamento = usuarioCancelamento;
            this.MotivoCancelamento = motivoCancelamento;
            this.Situacao = situacao;
            this.PlanoForma = planoForma;
            this.ValorLiquido = valorLiquido;
            this.CentroCusto = centroCusto;

        }

        public Nfe(Int64? id)
        {
            this.id = id;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Nfe p = obj as Nfe;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

        public Boolean isNullForFilter(DateTime? dataEmissaoInicial, DateTime? dataCancelamentoInicial)
        {
            Boolean retorno = false;

            // parametros de filtro.

            if (this.empresa == null && String.IsNullOrEmpty(this.situacao) && dataEmissaoInicial == null &&
                dataCancelamentoInicial == null && this.numeroNfe == null && this.cliente == null)
                retorno = true;

            return retorno;

        }

    }
}
