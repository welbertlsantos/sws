﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    class GheSetorClienteFuncaoExame
    {
        private GheSetorClienteFuncao gheSetorClienteFuncao;

        public GheSetorClienteFuncao GheSetorClienteFuncao
        {
            get { return gheSetorClienteFuncao; }
            set { gheSetorClienteFuncao = value; }
        }
        private Exame exame;

        public Exame Exame
        {
            get { return exame; }
            set { exame = value; }
        }
        private Int32 idade;

        public Int32 Idade
        {
            get { return idade; }
            set { idade = value; }
        }

        private GheFonteAgenteExame gheFonteAgenteExame;

        public GheFonteAgenteExame GheFonteAgenteExame
        {
            get { return gheFonteAgenteExame; }
            set { gheFonteAgenteExame = value; }
        }

        public GheSetorClienteFuncaoExame() { }

        public GheSetorClienteFuncaoExame(GheSetorClienteFuncao gheSetorClienteFuncao, Exame exame, Int32 idade, GheFonteAgenteExame gheFonteAgenteExame)
        {
            this.GheSetorClienteFuncao = gheSetorClienteFuncao;
            this.Exame = exame;
            this.Idade = idade;
            this.GheFonteAgenteExame = gheFonteAgenteExame;
        }
        
    }
}
