﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS.Entidade
{
    public class ClienteFuncionario
    {
        private long id;

        public long Id
        {
            get { return id; }
            set { id = value; }
        }
        private Funcionario funcionario;

        public Funcionario Funcionario
        {
            get { return funcionario; }
            set { funcionario = value; }
        }
        private Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }
        private string brpdh;

        public string Brpdh
        {
            get { return brpdh; }
            set { brpdh = value; }
        }
        private DateTime dataAdmissao;

        public DateTime DataAdmissao
        {
            get { return dataAdmissao; }
            set { dataAdmissao = value; }
        }
        private DateTime? dataDemissao;

        public DateTime? DataDemissao
        {
            get { return dataDemissao; }
            set { dataDemissao = value; }
        }
        private string regimeRevezamento;

        public string RegimeRevezamento
        {
            get { return regimeRevezamento; }
            set { regimeRevezamento = value; }
        }
        private string matricula;

        public string Matricula
        {
            get { return matricula; }
            set { matricula = value; }
        }
        private bool estagiario;

        public bool Estagiario
        {
            get { return estagiario; }
            set { estagiario = value; }
        }
        private bool vinculo;

        public bool Vinculo
        {
            get { return vinculo; }
            set { vinculo = value; }
        }

        public ClienteFuncionario(long id, Funcionario funcionario, Cliente cliente, string brpdh, DateTime dataAdmissao, DateTime? dataDemissao, string regimeRevezamento, string matricula, bool estagiario, bool vinculo)
        {
            this.Id = id;
            this.Funcionario = funcionario;
            this.Cliente = cliente;
            this.Brpdh = brpdh;
            this.DataAdmissao = dataAdmissao;
            this.DataDemissao = dataDemissao;
            this.RegimeRevezamento = regimeRevezamento;
            this.Matricula = matricula;
            this.Estagiario = estagiario;
            this.Vinculo = vinculo;
        }

    }
}
