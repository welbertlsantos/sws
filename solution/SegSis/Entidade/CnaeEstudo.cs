﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace SWS.Entidade
{
    class CnaeEstudo
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private Cnae cnae;

        public Cnae Cnae
        {
            get { return cnae; }
            set { cnae = value; }
        }
        private Estudo estudo;

        public Estudo Estudo
        {
            get { return estudo; }
            set { estudo = value; }
        }
        private bool flagCliente;

        public bool FlagCliente
        {
            get { return flagCliente; }
            set { flagCliente = value; }
        }
        
        public CnaeEstudo() { }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            CnaeEstudo p = obj as CnaeEstudo;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
    
    }
}
