﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class AcompanhamentoAtendimento
    {
        private long? id;

        public long? Id
        {
            get { return id; }
            set { id = value; }
        }
        private string nomeFuncionario;

        public string NomeFuncionario
        {
            get { return nomeFuncionario; }
            set { nomeFuncionario = value; }
        }
        private string sala;

        public string Sala
        {
            get { return sala; }
            set { sala = value; }
        }
        private int andar;

        public int Andar
        {
            get { return andar; }
            set { andar = value; }
        }
        private string razaoSocial;

        public string RazaoSocial
        {
            get { return razaoSocial; }
            set { razaoSocial = value; }
        }
        private string rg;

        public string Rg
        {
            get { return rg; }
            set { rg = value; }
        }
        private DateTime? dataInclusao;

        public DateTime? DataInclusao
        {
            get { return dataInclusao; }
            set { dataInclusao = value; }
        }
        private string senhaAtendimento;

        public string SenhaAtendimento
        {
            get { return senhaAtendimento; }
            set { senhaAtendimento = value; }
        }
        private bool atendido;

        public bool Atendido
        {
            get { return atendido; }
            set { atendido = value; }
        }
        private string cpf;

        public string Cpf
        {
            get { return cpf; }
            set { cpf = value; }
        }
        private DateTime? dataAtendido;

        public DateTime? DataAtendido
        {
            get { return dataAtendido; }
            set { dataAtendido = value; }
        }
        private long idAtendimento;

        public long IdAtendimento
        {
            get { return idAtendimento; }
            set { idAtendimento = value; }
        }

        private string codigoAtendimento;

        public string CodigoAtendimento
        {
            get { return codigoAtendimento; }
            set { codigoAtendimento = value; }
        }

        private string exame;

        public string Exame
        {
            get { return exame; }
            set { exame = value; }
        }

        private long idEmpresa;

        public long IdEmpresa
        {
            get { return idEmpresa; }
            set { idEmpresa = value; }
        }

        private string nomeEmpresa;

        public string NomeEmpresa
        {
            get { return nomeEmpresa; }
            set { nomeEmpresa = value; }
        }

        public AcompanhamentoAtendimento(long? id, string nomeFuncionario, 
            string sala, int andar, string razaoSocial, string rg, DateTime? dataInclusao, 
            string senha, bool flagAtendido, string cpf, DateTime? dataAtendido, 
            long idAtendimento, string codigoAtendimento, string nomeExame,
            long idEmpresa, string nomeEmpresa)
        {
            this.Id = id;
            this.NomeFuncionario = nomeFuncionario;
            this.Sala = sala;
            this.Andar = andar;
            this.RazaoSocial = razaoSocial;
            this.Rg = rg;
            this.DataInclusao = dataInclusao;
            this.SenhaAtendimento = senha;
            this.Atendido = flagAtendido;
            this.Cpf = cpf;
            this.DataAtendido = dataAtendido;
            this.IdAtendimento = idAtendimento;
            this.CodigoAtendimento = codigoAtendimento;
            this.Exame = nomeExame;
            this.IdEmpresa = idEmpresa;
            this.NomeEmpresa = nomeEmpresa;
        }
    }
}
