﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class RelatorioExame
    {
        private long? idRelatorioExame;

        public long? IdRelatorioExame
        {
            get { return idRelatorioExame; }
            set { idRelatorioExame = value; }
        }

        private Exame exame;

        public Exame Exame
        {
            get { return exame; }
            set { exame = value; }
        }

        private Relatorio relatorio;

        public Relatorio Relatorio
        {
            get { return relatorio; }
            set { relatorio = value; }
        }

        private string situacao;

        public string Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }

        private DateTime dataGravacao;

        public DateTime DataGravacao
        {
            get { return dataGravacao; }
            set { dataGravacao = value; }
        }

        public RelatorioExame() { }

        public RelatorioExame(long? id, Exame exame, Relatorio relatorio, string situacao, DateTime? dataGravacao)
        {
            this.IdRelatorioExame = id;
            this.Exame = exame;
            this.Relatorio = relatorio;
            this.Situacao = situacao;
            this.DataGravacao = (DateTime)dataGravacao;
        }


    }
}
