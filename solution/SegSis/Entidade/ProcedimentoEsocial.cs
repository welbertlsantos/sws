﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class ProcedimentoEsocial
    {
        private long? id;

        public long? Id
        {
            get { return id; }
            set { id = value; }
        }

        private string codigo;

        public string Codigo
        {
            get { return codigo; }
            set { codigo = value; }
        }

        private string procedimento;

        public string Procedimento
        {
            get { return procedimento; }
            set { procedimento = value; }
        }

        public ProcedimentoEsocial(long? id, string codigo, string procedimento)
        {
            this.Id = id;
            this.Codigo = codigo;
            this.Procedimento = procedimento;
        }
    }
}
