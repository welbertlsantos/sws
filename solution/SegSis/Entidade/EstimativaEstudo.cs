﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    class EstimativaEstudo
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private Estudo estudo;

        public Estudo Estudo
        {
            get { return estudo; }
            set { estudo = value; }
        }
        private Estimativa estimativa;

        internal Estimativa Estimativa
        {
            get { return estimativa; }
            set { estimativa = value; }
        }
        private Int32 quantidade;

        public Int32 Quantidade
        {
            get { return quantidade; }
            set { quantidade = value; }
        }

        public EstimativaEstudo() { }

        public EstimativaEstudo(Int64? id, Estudo estudo, Estimativa estimativa, Int32 quantidade)
        {
            this.Id = id;
            this.Estudo = estudo;
            this.Estimativa = estimativa;
            this.Quantidade = quantidade;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            EstimativaEstudo p = obj as EstimativaEstudo;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }


    }
}
