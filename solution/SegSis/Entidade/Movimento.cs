﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Movimento
    {

        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private GheFonteAgenteExameAso gheFonteAgenteExameAso;

        public GheFonteAgenteExameAso GheFonteAgenteExameAso
        {
            get { return gheFonteAgenteExameAso; }
            set { gheFonteAgenteExameAso = value; }
        }
        private ClienteFuncaoExameASo clienteFuncaoExameAso;

        public ClienteFuncaoExameASo ClienteFuncaoExameAso
        {
            get { return clienteFuncaoExameAso; }
            set { clienteFuncaoExameAso = value; }
        }
        private Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }
        private ContratoProduto contratoProduto;

        public ContratoProduto ContratoProduto
        {
            get { return contratoProduto; }
            set { contratoProduto = value; }
        }
        private Nfe nfe;

        public Nfe Nfe
        {
            get { return nfe; }
            set { nfe = value; }
        }
        private DateTime? dataInclusao;

        public DateTime? DataInclusao
        {
            get { return dataInclusao; }
            set { dataInclusao = value; }
        }
        private DateTime? dataFaturamento;

        public DateTime? DataFaturamento
        {
            get { return dataFaturamento; }
            set { dataFaturamento = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        private Decimal? precoUnit;

        public Decimal? PrecoUnit
        {
            get { return precoUnit; }
            set { precoUnit = value; }
        }
        private Decimal? valorComissao;

        public Decimal? ValorComissao
        {
            get { return valorComissao; }
            set { valorComissao = value; }
        }
        private Estudo estudo;

        public Estudo Estudo
        {
            get { return estudo; }
            set { estudo = value; }
        }
        private Aso aso;

        public Aso Aso
        {
            get { return aso; }
            set { aso = value; }
        }
        private Int32? quantidade;

        public Int32? Quantidade
        {
            get { return quantidade; }
            set { quantidade = value; }
        }
        private Usuario usuario;

        public Usuario Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }
        private Empresa empresa;

        public Empresa Empresa
        {
            get { return empresa; }
            set { empresa = value; }
        }
        private Decimal? custoUnitario;

        public Decimal? CustoUnitario
        {
            get { return custoUnitario; }
            set { custoUnitario = value; }
        }
        private Cliente unidade;

        public Cliente Unidade
        {
            get { return unidade; }
            set { unidade = value; }
        }
        private Protocolo protocolo;

        public Protocolo Protocolo
        {
            get { return protocolo; }
            set { protocolo = value; }
        }
        private DateTime dataGravacao;

        public DateTime DataGravacao
        {
            get { return dataGravacao; }
            set { dataGravacao = value; }
        }

        private CentroCusto centroCusto;

        public CentroCusto CentroCusto
        {
            get { return centroCusto; }
            set { centroCusto = value; }
        }

        private ContratoExame contratoExame;

        public ContratoExame ContratoExame
        {
            get { return contratoExame; }
            set { contratoExame = value; }
        }

        private Cliente clienteCredenciada;

        public Cliente ClienteCredenciada
        {
            get { return clienteCredenciada; }
            set { clienteCredenciada = value; }
        }

        public Movimento() { }

        public Movimento(Int64? id, GheFonteAgenteExameAso gheFonteAgenteExameASo, ClienteFuncaoExameASo clienteFuncaoExameAso, 
            Cliente cliente, ContratoProduto contratoProduto, Nfe nfe, DateTime? dataInclusao, DateTime? dataFaturamento, 
            String situacao, Decimal? precoUnit, Decimal? valorComissao, Estudo estudo, Aso aso, Int32? quantidade,
            Usuario usuario, Empresa empresa, Decimal? custoUnitario, Cliente unidade, 
            Protocolo protocolo,
            DateTime dataGravacao, CentroCusto centroCusto, ContratoExame contratoExame, Cliente clienteCredenciada)
        {
            this.Id = id;
            this.GheFonteAgenteExameAso = gheFonteAgenteExameASo;
            this.ClienteFuncaoExameAso = clienteFuncaoExameAso;
            this.Cliente = cliente;
            this.ContratoProduto = contratoProduto;
            this.Nfe = nfe;
            this.DataInclusao = dataInclusao;
            this.DataFaturamento = dataFaturamento;
            this.Situacao = situacao;
            this.PrecoUnit = precoUnit;
            this.ValorComissao = valorComissao;
            this.Estudo = estudo;
            this.Aso = aso;
            this.Quantidade = quantidade;
            this.Usuario = usuario;
            this.Empresa = empresa;
            this.CustoUnitario = custoUnitario;
            this.Unidade = unidade;
            this.Protocolo = protocolo;
            this.DataGravacao = dataGravacao;
            this.CentroCusto = centroCusto;
            this.ContratoExame = contratoExame;
            this.ClienteCredenciada = clienteCredenciada;
        }
        
        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Movimento p = obj as Movimento;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

        public Boolean isNullForFilter()
        {
            Boolean retorno = Convert.ToBoolean(false);

            if (this.cliente == null && this.dataFaturamento == null && this.dataInclusao == null && this.nfe == null && String.IsNullOrEmpty(situacao) && this.clienteCredenciada == null)
            {
                retorno = Convert.ToBoolean(true);
            }

            return retorno;
        }


    }
}
