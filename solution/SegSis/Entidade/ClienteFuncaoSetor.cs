﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class ClienteFuncaoSetor
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private Setor setor;

        public Setor Setor
        {
            get { return setor; }
            set { setor = value; }
        }
        private ClienteFuncao clienteFuncao;

        public ClienteFuncao ClienteFuncao
        {
            get { return clienteFuncao; }
            set { clienteFuncao = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        private DateTime dataExclusao;

        public DateTime DataExclusao
        {
            get { return dataExclusao; }
            set { dataExclusao = value; }
        }
        private Usuario usuario;

        public Usuario Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }
                
        public ClienteFuncaoSetor() { }

        public ClienteFuncaoSetor(Int64? id, Setor setor, ClienteFuncao clienteFuncao, String situacao)
        {
            this.Id = id;
            this.Setor = setor;
            this.ClienteFuncao = clienteFuncao;
            this.Situacao = situacao;
        }
                 
        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            ClienteFuncaoSetor p = obj as ClienteFuncaoSetor;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
        
    }
}


