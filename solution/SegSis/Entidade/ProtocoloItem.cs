﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class ProtocoloItem
    {
        private Int64 idItem;

        public Int64 IdItem
        {
            get { return idItem; }
            set { idItem = value; }
        }
        
        private Int64 idProtocolo;
        
        public Int64 IdProtocolo
        {
            get { return idProtocolo; }
            set { idProtocolo = value; }
        }
        
        private String codigoItem;
        public String CodigoItem
        {
            get { return codigoItem; }
            set { codigoItem = value; }
        }
        
        private String cliente;
        public String Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }

        private String colaborador;
        public String Colaborador
        {
            get { return colaborador; }
            set { colaborador = value; }
        }
        
        private String rg;
        public String Rg
        {
            get { return rg; }
            set { rg = value; }
        }
        
        private String cpf;
        public String Cpf
        {
            get { return cpf; }
            set { cpf = value; }
        }
        
        private String periodicidade;
        public String Periodicidade
        {
            get { return periodicidade; }
            set { periodicidade = value; }
        }
        
        private String descricaoItem;
        public String DescricaoItem
        {
            get { return descricaoItem; }
            set { descricaoItem = value; }
        }
        
        private DateTime dataItem;
        public DateTime DataItem
        {
            get { return dataItem; }
            set { dataItem = value; }
        }
        
        private String laudo;
        public String Laudo
        {
            get { return laudo; }
            set { laudo = value; }
        }
        
        private String tipo;
        public String Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }

        public ProtocoloItem() { }

    }
}
