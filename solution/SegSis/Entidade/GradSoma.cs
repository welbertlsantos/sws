﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class GradSoma
    {
        private Int64 id;

        public Int64 Id
        {
            get { return id; }
            set { id = value; }
        }
        private Int64 faixa;

        public Int64 Faixa
        {
            get { return faixa; }
            set { faixa = value; }
        }
        private String medControle;

        public String MedControle
        {
            get { return medControle; }
            set { medControle = value; }
        }
        private String descricao;

        public String Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }

        public GradSoma(){}
    
        public GradSoma(Int64 id)
        {
            this.Id = id;
        }

        public GradSoma(Int64 id, Int64 faixa, String medControle, String descricao)
        {
            this.Id = id;
            this.Faixa = faixa;
            this.MedControle = medControle;
            this.Descricao = descricao;
        }
    
    }
}
