﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class SalaExame
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private Exame exame;

        public Exame Exame
        {
            get { return exame; }
            set { exame = value; }
        }
        private Sala sala;

        public Sala Sala
        {
            get { return sala; }
            set { sala = value; }
        }
        private Int32? tempoMedioAtendimento;

        public Int32? TempoMedioAtendimento
        {
            get { return tempoMedioAtendimento; }
            set { tempoMedioAtendimento = value; }
        }
        private Int32? tempoMedioAtendimentoCalculado;

        public Int32? TempoMedioAtendimentoCalculado
        {
            get { return tempoMedioAtendimentoCalculado; }
            set { tempoMedioAtendimentoCalculado = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        private Int32 quantidadeAtendimentos = 0;

        public Int32 QuantidadeAtendimentos
        {
            get { return quantidadeAtendimentos; }
            set { quantidadeAtendimentos = value; }
        }
        
        public SalaExame() { }

        public SalaExame(Int64? id, Exame exame, Sala sala, Int32? tempoMedioAtendimento, Int32? tempoMedioAtendimentoCalculado, String situacao)
        {
            this.Id = id;
            this.Exame = exame;
            this.Sala = sala;
            this.TempoMedioAtendimento = tempoMedioAtendimento;
            this.TempoMedioAtendimentoCalculado = tempoMedioAtendimentoCalculado;
            this.Situacao = situacao;
        }
        
        public Int32 getPrevisaoDeAtendimento()
        {
            Int32 previsaoAtendimento = 0;

            if (tempoMedioAtendimentoCalculado != null && tempoMedioAtendimentoCalculado > 0)
                previsaoAtendimento = (Int32) tempoMedioAtendimentoCalculado * quantidadeAtendimentos;
            else
                previsaoAtendimento = (Int32)tempoMedioAtendimento * quantidadeAtendimentos;

            return previsaoAtendimento;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.

            SalaExame p = obj as SalaExame;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id && this.exame.Id == p.exame.Id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
    
    }
}
