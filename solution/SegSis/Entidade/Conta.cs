﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Conta
    {

        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private Banco banco;

        public Banco Banco
        {
            get { return banco; }
            set { banco = value; }
        }
        private String nome;

        public String Nome
        {
            get { return nome; }
            set { nome = value; }
        }
        private String agenciaNumero;

        public String AgenciaNumero
        {
            get { return agenciaNumero; }
            set { agenciaNumero = value; }
        }
        private String agenciaDigito;

        public String AgenciaDigito
        {
            get { return agenciaDigito; }
            set { agenciaDigito = value; }
        }
        private String contaNumero;

        public String ContaNumero
        {
            get { return contaNumero; }
            set { contaNumero = value; }
        }
        private String contaDigito;

        public String ContaDigito
        {
            get { return contaDigito; }
            set { contaDigito = value; }
        }
        private Int64? proximoNumero;

        public Int64? ProximoNumero
        {
            get { return proximoNumero; }
            set { proximoNumero = value; }
        }
        private String numeroRemessa;

        public String NumeroRemessa
        {
            get { return numeroRemessa; }
            set { numeroRemessa = value; }
        }
        private String carteira;

        public String Carteira
        {
            get { return carteira; }
            set { carteira = value; }
        }
        private String convenio;

        public String Convenio
        {
            get { return convenio; }
            set { convenio = value; }
        }
        private String variacaoCarteira;

        public String VariacaoCarteira
        {
            get { return variacaoCarteira; }
            set { variacaoCarteira = value; }
        }
        private String codigoCedenteBanco;

        public String CodigoCedenteBanco
        {
            get { return codigoCedenteBanco; }
            set { codigoCedenteBanco = value; }
        }
        private Boolean registro;

        public Boolean Registro
        {
            get { return registro; }
            set { registro = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        private Boolean privado;

        public Boolean Privado
        {
            get { return privado; }
            set { privado = value; }
        }
        
        public Conta() { }

        public Conta(Int64 id) 
        {
            this.Id = id;
        }

        public Conta(Int64? id, Banco banco, String nome, String agenciaNumero, String agenciaDigito, String contaNumero, String contaDigito, Int64? proximoNumero, String numeroRemessa, String carteira, String convenio, String variacaoCarteira, String codigoCedenteBanco, Boolean registro, String situacao, Boolean privado)
        {
            this.Id = id;
            this.Banco = banco;
            this.Nome = nome;
            this.AgenciaNumero = agenciaNumero;
            this.AgenciaDigito = agenciaDigito;
            this.ContaNumero = contaNumero;
            this.ContaDigito = contaDigito;
            this.ProximoNumero = proximoNumero;
            this.NumeroRemessa = numeroRemessa;
            this.Carteira = carteira;
            this.Convenio = convenio;
            this.VariacaoCarteira = variacaoCarteira;
            this.CodigoCedenteBanco = codigoCedenteBanco;
            this.Registro = registro;
            this.Situacao = situacao;
            this.Privado = privado;
          
        }
        
        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Conta p = obj as Conta;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
    
    }
}
