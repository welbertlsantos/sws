﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class TipoContato
    {
        private Int64 id;

        public Int64 Id
        {
            get { return id; }
            set { id = value; }
        }

        private String descricao;

        public String Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }

        public TipoContato(Int64 id, String descricao)
        {
            this.id = id;
            this.descricao = descricao;
        }

    }
}
