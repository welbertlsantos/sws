﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace SWS.Entidade
{
    public class HospitalEstudo
    {
        private Estudo pcmso;

        public Estudo Pcmso
        {
            get { return pcmso; }
            set { pcmso = value; }
        }
        private Hospital hospital;

        public Hospital Hospital
        {
            get { return hospital; }
            set { hospital = value; }
        }

        public HospitalEstudo() {}

        public HospitalEstudo(Estudo pcmso, Hospital hospital)
        {
            this.Pcmso = pcmso;
            this.Hospital = hospital;
        }
    }
}
