﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    class MaterialHospitalarEstudo
    {
        private Estudo estudo;

        public Estudo Estudo
        {
            get { return estudo; }
            set { estudo = value; }
        }
        private MaterialHospitalar materialHospitalar;

        public MaterialHospitalar MaterialHospitalar
        {
            get { return materialHospitalar; }
            set { materialHospitalar = value; }
        }
        private Int32 quantidade;

        public Int32 Quantidade
        {
            get { return quantidade; }
            set { quantidade = value; }
        }

        public MaterialHospitalarEstudo() { }

        public MaterialHospitalarEstudo(Estudo estudo, MaterialHospitalar materialHospitalar, Int32 quantidade)
        {
            this.Estudo = estudo;
            this.MaterialHospitalar = materialHospitalar;
            this.Quantidade = quantidade;
        }
    }
}
