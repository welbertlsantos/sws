﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;

namespace SWS.Entidade
{
    class Revisao
    {
        private Int64? id;
        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }

        private Estudo estudo;
        public Estudo Estudo
        {
            get { return estudo; }
            set { estudo = value; }
        }

        private String comentario;
        public String Comentario
        {
            get { return comentario; }
            set { comentario = value; }
        }

        private Estudo estudoRaiz;
        public Estudo EstudoRaiz
        {
            get { return estudoRaiz; }
            set { estudoRaiz = value; }
        }

        private Estudo estudoRevisao;
        public Estudo EstudoRevisao
        {
            get { return estudoRevisao; }
            set { estudoRevisao = value; }
        }

        private string tipoRevisao;
        public string TipoRevisao
        {
            get { return tipoRevisao; }
            set { tipoRevisao = value; }
        }
        
        private string versaoEstudo;
        public string VersaoEstudo
        {
            get { return versaoEstudo; }
            set { versaoEstudo = value; }
        }

        private DateTime? dataRevisao;
        public DateTime? DataRevisao
        {
            get { return dataRevisao; }
            set { dataRevisao = value; }
        }

        private Boolean indCorrecao;
        public Boolean IndCorrecao
        {
            get { return indCorrecao; }
            set { indCorrecao = value; }
        }

        public Revisao() { }

        public Revisao(Int64? id, Estudo estudo, String comentario, Estudo estudoRaiz, Estudo estudoRevisao, string tipoRevisao, string versaoEstudo, DateTime? dataRevisao, Boolean indCorrecao)
        {
            this.id = id;
            this.estudo = estudo;
            this.comentario = comentario;
            this.estudoRaiz = estudoRaiz;
            this.estudoRevisao = estudoRevisao;
            this.tipoRevisao = tipoRevisao;
            this.versaoEstudo = versaoEstudo;
            this.dataRevisao = dataRevisao;
            this.indCorrecao = indCorrecao;
        }

    }
}