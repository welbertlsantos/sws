﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class ClienteFuncaoExamePeriodicidade
    {
        private ClienteFuncaoExame clienteFuncaoExame;

        public ClienteFuncaoExame ClienteFuncaoExame
        {
            get { return clienteFuncaoExame; }
            set { clienteFuncaoExame = value; }
        }
        
        private Periodicidade periodicidade;

        public Periodicidade Periodicidade
        {
            get { return periodicidade; }
            set { periodicidade = value; }
        }
        
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }

        private int? periodoVencimento;

        public int? PeriodoVencimento
        {
            get { return periodoVencimento; }
            set { periodoVencimento = value; }
        }

        public ClienteFuncaoExamePeriodicidade(){}

        public ClienteFuncaoExamePeriodicidade(ClienteFuncaoExame clienteFuncaoExame, Periodicidade periodicidade, String situacao, int? periodoVencimento)
        {
            this.ClienteFuncaoExame = clienteFuncaoExame;
            this.Periodicidade = periodicidade;
            this.Situacao = situacao;
            this.PeriodoVencimento = periodoVencimento;
        }

    }
}
