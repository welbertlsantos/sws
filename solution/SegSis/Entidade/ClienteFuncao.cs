﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class ClienteFuncao
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }
        private Funcao funcao;

        public Funcao Funcao
        {
            get { return funcao; }
            set { funcao = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        private DateTime? dataCadastro;

        public DateTime? DataCadastro
        {
            get { return dataCadastro; }
            set { dataCadastro = value; }
        }
        private DateTime? dataExclusao;

        public DateTime? DataExclusao
        {
            get { return dataExclusao; }
            set { dataExclusao = value; }
        }
        
        public ClienteFuncao(Int64? id) 
        {
            this.Id = id;
        }

        public ClienteFuncao() { }

        public ClienteFuncao(Int64? id, Cliente cliente, Funcao funcao, String situacao, DateTime? dataCadastro, DateTime? dataExclusao)
        {
            this.Id = id;
            this.Cliente = cliente;
            this.Funcao = funcao;
            this.Situacao = situacao;
            this.DataCadastro = dataCadastro;
            this.DataExclusao = dataExclusao;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            ClienteFuncao p = obj as ClienteFuncao;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id && this.Funcao.Id == p.Funcao.Id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
        
    }
}
