﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class GradExposicao
    {
        private Int64 id;

        public Int64 Id
        {
            get { return id; }
            set { id = value; }
        }
        private String descricao;

        public String Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }
        private String categoria;

        public String Categoria
        {
            get { return categoria; }
            set { categoria = value; }
        }
        private Int64 valor;

        public Int64 Valor
        {
            get { return valor; }
            set { valor = value; }
        }

        public GradExposicao(){}

        public GradExposicao(Int64 id, String descricao, String categoria, Int64 valor)
        {
            this.Id = id;
            this.Descricao = descricao;
            this.Categoria = categoria;
            this.Valor = valor;
        }

    }
}