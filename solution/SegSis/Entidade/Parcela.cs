﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Parcela
    {

        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }

        private Plano plano;

        public Plano Plano
        {
            get { return plano; }
            set { plano = value; }
        }

        private int? dias;

        public int? Dias
        {
            get { return dias; }
            set { dias = value; }
        }

        public Parcela() { }

        public Parcela(Int64? id, Plano plano, int? dias)
        {
            this.Id = id;
            this.Plano = plano;
            this.Dias = dias;
        }
        
        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Parcela p = obj as Parcela;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

    }
}
