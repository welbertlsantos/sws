﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    class Anexo
    {
        
        private Int64? id;
        private String conteudo;
        private Estudo estudo;

        public Anexo() { }

        public Anexo(Int64? id, Estudo estudo, String conteudo)
        {
            this.setId(id);
            this.setConteudo(conteudo);
            this.setEstudo(estudo);
        }

        public Int64? getId ()
        {
            return this.id;
        }
        public void setId(Int64? id)
        {
            this.id = id;
        }

        public String getConteudo()
        {
            return this.conteudo;
        }
        public void setConteudo(String conteudo)
        {
            this.conteudo = conteudo;
        }

        public Estudo getEstudo()
        {
            return this.estudo;
        }
        public void setEstudo(Estudo estudo)
        {
            this.estudo = estudo;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Anexo p = obj as Anexo;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
    }
}
