﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using SWS.View;
using System.Windows.Forms;


namespace SWS.Entidade
{
    public class GheSetor
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private Setor setor;

        public Setor Setor
        {
            get { return setor; }
            set { setor = value; }
        }
        private Ghe ghe;

        public Ghe Ghe
        {
            get { return ghe; }
            set { ghe = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }

        public GheSetor() { }

        public GheSetor(Int64? id, Setor setor, Ghe ghe, String situacao)
        {
            this.Id = id;
            this.Setor = setor;
            this.Ghe = ghe;
            this.Situacao = situacao;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            GheSetor p = obj as GheSetor;

            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id );
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
        
    
    }
}
