﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Protocolo
    {

        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private String numero;

        public String Numero
        {
            get { return numero; }
            set { numero = value.Replace(".","").Replace(",","").Trim(); }
        }
        private String documentoResponsavel;

        public String DocumentoResponsavel
        {
            get { return documentoResponsavel; }
            set { documentoResponsavel = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        private DateTime? dataGravacao;

        public DateTime? DataGravacao
        {
            get { return dataGravacao; }
            set { dataGravacao = value; }
        }
        private Usuario usuarioGerou;

        public Usuario UsuarioGerou
        {
            get { return usuarioGerou; }
            set { usuarioGerou = value; }
        }
        private DateTime? dataCancelamento;

        public DateTime? DataCancelamento
        {
            get { return dataCancelamento; }
            set { dataCancelamento = value; }
        }
        private Usuario usuarioCancelou;

        public Usuario UsuarioCancelou
        {
            get { return usuarioCancelou; }
            set { usuarioCancelou = value; }
        }
        private String enviado;

        public String Enviado
        {
            get { return enviado; }
            set { enviado = value; }
        }
        private DateTime? dataFinalizacao;

        public DateTime? DataFinalizacao
        {
            get { return dataFinalizacao; }
            set { dataFinalizacao = value; }
        }
        private Usuario usuarioFinalizou;

        public Usuario UsuarioFinalizou
        {
            get { return usuarioFinalizou; }
            set { usuarioFinalizou = value; }
        }
        private String observacao;

        public String Observacao
        {
            get { return observacao; }
            set { observacao = value; }
        }
        private String motivoCancelamento;

        public String MotivoCancelamento
        {
            get { return motivoCancelamento; }
            set { motivoCancelamento = value; }
        }
        private Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }
        private List<ClienteFuncaoExameASo> clienteFuncaoExameAsos = new List<ClienteFuncaoExameASo>();

        public List<ClienteFuncaoExameASo> ClienteFuncaoExameAsos
        {
            get { return clienteFuncaoExameAsos; }
            set { clienteFuncaoExameAsos = value; }
        }
        private List<GheFonteAgenteExameAso> gheFonteAgenteExameAsos = new List<GheFonteAgenteExameAso>();

        public List<GheFonteAgenteExameAso> GheFonteAgenteExameAsos
        {
            get { return gheFonteAgenteExameAsos; }
            set { gheFonteAgenteExameAsos = value; }
        }
        private LinkedList<Aso> atendimentos = new LinkedList<Aso>();

        public LinkedList<Aso> Atendimentos
        {
            get { return atendimentos; }
            set { atendimentos = value; }
        }
        private Aso atendimento;

        public Aso Atendimento
        {
            get { return atendimento; }
            set { atendimento = value; }
        }
        private LinkedList<Estudo> pcmsos = new LinkedList<Estudo>();

        public LinkedList<Estudo> Pcmsos
        {
            get { return pcmsos; }
            set { pcmsos = value; }
        }
        private Estudo pcmso;

        public Estudo Pcmso
        {
            get { return pcmso; }
            set { pcmso = value; }
        }
        private LinkedList<Estudo> ppras = new LinkedList<Estudo>();

        public LinkedList<Estudo> Ppras
        {
            get { return ppras; }
            set { ppras = value; }
        }
        private Estudo ppra;

        public Estudo Ppra
        {
            get { return ppra; }
            set { ppra = value; }
        }
        private LinkedList<Movimento> movimentos = new LinkedList<Movimento>();

        public LinkedList<Movimento> Movimentos
        {
            get { return movimentos; }
            set { movimentos = value; }
        }
        private Movimento produtoMovimento;

        public Movimento ProdutoMovimento
        {
            get { return produtoMovimento; }
            set { produtoMovimento = value; }
        }
        private LinkedList<DocumentoProtocolo> documentos = new LinkedList<DocumentoProtocolo>();

        public LinkedList<DocumentoProtocolo> Documentos
        {
            get { return documentos; }
            set { documentos = value; }
        }

        public Protocolo(){}

        public Protocolo(Int64? id, String numero, String documentoResponsavel, 
            String situacao, DateTime? dataGravacao, Usuario usuarioGerou,
            DateTime? dataCancelamento, Usuario usuarioCancelou, String enviado,
            DateTime? dataFinalizacao, Usuario usuarioFinalizou,
            String observacao, String motivoCancelamento, Cliente cliente)
        {

            this.Id = id;
            this.Numero = numero;
            this.DocumentoResponsavel = documentoResponsavel;
            this.Situacao = situacao;
            this.DataGravacao = dataGravacao;
            this.UsuarioGerou = usuarioGerou;
            this.DataCancelamento = dataCancelamento;
            this.UsuarioCancelou = usuarioCancelou;
            this.Enviado = enviado;
            this.DataFinalizacao = dataFinalizacao;
            this.UsuarioFinalizou = usuarioFinalizou;
            this.Observacao = observacao;
            this.MotivoCancelamento = motivoCancelamento;
            this.Cliente = cliente;
        }

        public Protocolo(Int64 id)
        {
            this.Id = id;
        }

        public override bool Equals(System.Object obj)
        {
            if (obj == null)
            {
                return false;
            }

            Protocolo p = obj as Protocolo;
            if ((System.Object)p == null)
            {
                return false;
            }

            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

        public Boolean isNullForFilter()
        {
            Boolean retorno = false;

            if (String.IsNullOrEmpty(numero) && String.IsNullOrEmpty(situacao) && dataGravacao == null && dataCancelamento == null &&
                cliente == null)
            {
                retorno = true;
                
            }

            return retorno;
        }

    }
}
