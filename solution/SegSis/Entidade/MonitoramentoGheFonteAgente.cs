﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class MonitoramentoGheFonteAgente
    {
        private long? id;

        public long? Id
        {
            get { return id; }
            set { id = value; }
        }

        private GheFonteAgente gheFonteAgente;

        public GheFonteAgente GheFonteAgente
        {
            get { return gheFonteAgente; }
            set { gheFonteAgente = value; }
        }

        private Monitoramento monitoramento;

        public Monitoramento Monitoramento
        {
            get { return monitoramento; }
            set { monitoramento = value; }
        }

        private string tipoAvaliacao;

        public string TipoAvaliacao
        {
            get { return tipoAvaliacao; }
            set { tipoAvaliacao = value; }
        }

        private decimal? intensidadeConcentracao;

        public decimal? IntensidadeConcentracao
        {
            get { return intensidadeConcentracao; }
            set { intensidadeConcentracao = value; }
        }

        private decimal? limiteTolerancia;

        public decimal? LimiteTolerancia
        {
            get { return limiteTolerancia; }
            set { limiteTolerancia = value; }
        }

        private string unidadeMedicao;

        public string UnidadeMedicao
        {
            get { return unidadeMedicao; }
            set { unidadeMedicao = value; }
        }

        private string tecnicaMedicao;

        public string TecnicaMedicao
        {
            get { return tecnicaMedicao; }
            set { tecnicaMedicao = value; }
        }

        private string utilizaEpc;

        public string UtilizaEpc
        {
            get { return utilizaEpc; }
            set { utilizaEpc = value; }
        }

        private string eficaciaEpc;

        public string EficaciaEpc
        {
            get { return eficaciaEpc; }
            set { eficaciaEpc = value; }
        }

        private string utilizaEpi;

        public string UtilizaEpi
        {
            get { return utilizaEpi; }
            set { utilizaEpi = value; }
        }

        private string eficaciaEpi;

        public string EficaciaEpi
        {
            get { return eficaciaEpi; }
            set { eficaciaEpi = value; }
        }

        private string medidaProtecao;

        public string MedidaProtecao
        {
            get { return medidaProtecao; }
            set { medidaProtecao = value; }
        }

        private string condicaoFuncionamento;

        public string CondicaoFuncionamento
        {
            get { return condicaoFuncionamento; }
            set { condicaoFuncionamento = value; }
        }

        private string usoIniterrupto;

        public string UsoIniterrupto
        {
            get { return usoIniterrupto; }
            set { usoIniterrupto = value; }
        }

        private string prazoValidade;

        public string PrazoValidade
        {
            get { return prazoValidade; }
            set { prazoValidade = value; }
        }

        private string periodicidadeTroca;

        public string PeriodicidadeTroca
        {
            get { return periodicidadeTroca; }
            set { periodicidadeTroca = value; }
        }

        private string higienizacao;

        public string Higienizacao
        {
            get { return higienizacao; }
            set { higienizacao = value; }
        }

        private string numeroProcessoJudicial;

        public string NumeroProcessoJudicial
        {
            get { return numeroProcessoJudicial; }
            set { numeroProcessoJudicial = value; }
        }

        private List<EpiMonitoramento> monitoramentoEpis = new List<EpiMonitoramento>();

        public List<EpiMonitoramento> MonitoramentoEpis
        {
            get { return monitoramentoEpis; }
            set { monitoramentoEpis = value; }
        }


        public MonitoramentoGheFonteAgente() { }
        
        public MonitoramentoGheFonteAgente(long? id, GheFonteAgente gheFonteAgente, Monitoramento monitoramento, string tipoAvaliacao, decimal? intensidadeConcentracao,
            decimal? limiteTolerancia, string unidadeMedicao, string tecnicaMedicao, string utilizaEpc, string eficaciaEpc, string utilizaEpi, string eficaciaEpi, 
            string medidaProtecao, string condicaoFuncionamento, string usoIniterrupto, string prazoValidade, string periodicidadeTroca, string higienizacao, string numeroProcessoJudicial)
        {
            this.Id = id;
            this.GheFonteAgente = gheFonteAgente;
            this.Monitoramento = monitoramento;
            this.TipoAvaliacao = tipoAvaliacao;
            this.IntensidadeConcentracao = intensidadeConcentracao;
            this.LimiteTolerancia = limiteTolerancia;
            this.UnidadeMedicao = unidadeMedicao;
            this.TecnicaMedicao = tecnicaMedicao;
            this.UtilizaEpc = utilizaEpc;
            this.EficaciaEpc = eficaciaEpc;
            this.UtilizaEpi = utilizaEpi;
            this.EficaciaEpi = eficaciaEpi;
            this.MedidaProtecao = medidaProtecao;
            this.CondicaoFuncionamento = condicaoFuncionamento;
            this.UsoIniterrupto = usoIniterrupto;
            this.PrazoValidade = prazoValidade;
            this.PeriodicidadeTroca = periodicidadeTroca;
            this.Higienizacao = higienizacao;
            this.NumeroProcessoJudicial = numeroProcessoJudicial;
        }

        public MonitoramentoGheFonteAgente(long id)
        {
            this.Id = id;
        }

        public MonitoramentoGheFonteAgente copy()
        {
            MonitoramentoGheFonteAgente monitoramentoGheFonteAgenteCopy = (MonitoramentoGheFonteAgente)this.MemberwiseClone();
            return monitoramentoGheFonteAgenteCopy;
        }

    }
}
