﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Empresa
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }

        private String razaoSocial;

        public String RazaoSocial
        {
            get { return razaoSocial; }
            set { razaoSocial = value; }
        }
        
        private String fantasia;

        public String Fantasia
        {
            get { return fantasia; }
            set { fantasia = value; }
        }
        
        private String cnpj;

        public String Cnpj
        {
            get { return cnpj; }
            set { cnpj = value.Replace(".", "").Replace(",", "").Replace("-", "").Replace("/", "").Trim(); }
        }
        
        private String inscricao;

        public String Inscricao
        {
            get { return inscricao; }
            set { inscricao = value; }
        }
        
        private String endereco;

        public String Endereco
        {
            get { return endereco; }
            set { endereco = value; }
        }
        
        private String numero;

        public String Numero
        {
            get { return numero; }
            set { numero = value; }
        }
        
        private String complemento;

        public String Complemento
        {
            get { return complemento; }
            set { complemento = value; }
        }
        
        private String bairro;

        public String Bairro
        {
            get { return bairro; }
            set { bairro = value; }
        }
        
        private String cep;

        public String Cep
        {
            get { return cep; }
            set { cep = value.Replace(".", "").Replace(",", "").Replace("-", "").Trim(); }
        }
        
        private String uf;

        public String Uf
        {
            get { return uf; }
            set { uf = value; }
        }
        
        private String telefone1;

        public String Telefone1
        {
            get { return telefone1; }
            set { telefone1 = value; }
        }
        
        private String telefone2;

        public String Telefone2
        {
            get { return telefone2; }
            set { telefone2 = value; }
        }
        
        private String email;

        public String Email
        {
            get { return email; }
            set { email = value; }
        }
        
        private String site;

        public String Site
        {
            get { return site; }
            set { site = value; }
        }
        
        private DateTime? dataCadastro;

        public DateTime? DataCadastro
        {
            get { return dataCadastro; }
            set { dataCadastro = value; }
        }
        
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        
        private String mimetype;

        public String Mimetype
        {
            get { return mimetype; }
            set { mimetype = value; }
        }
        
        private byte[] logo;

        public byte[] Logo
        {
            get { return logo; }
            set { logo = value; }
        }
        
        private Boolean simplesNacional;

        public Boolean SimplesNacional
        {
            get { return simplesNacional; }
            set { simplesNacional = value; }
        }
        
        private CidadeIbge cidade;

        public CidadeIbge Cidade
        {
            get { return cidade; }
            set { cidade = value; }
        }

        private Empresa matriz;

        public Empresa Matriz
        {
            get { return matriz; }
            set { matriz = value; }
        }

        private string codigoCnes;

        public string CodigoCnes
        {
            get { return codigoCnes; }
            set { codigoCnes = value; }
        }

        private decimal valorMinimoImpostoFederal;

        public decimal ValorMinimoImpostoFederal
        {
            get { return valorMinimoImpostoFederal; }
            set { valorMinimoImpostoFederal = value; }
        }

        private decimal valorMinimoIR;

        public decimal ValorMinimoIR
        {
            get { return valorMinimoIR; }
            set { valorMinimoIR = value; }
        }

        private decimal aliquotaPIS;

        public decimal AliquotaPIS
        {
            get { return aliquotaPIS; }
            set { aliquotaPIS = value; }
        }

        private decimal aliquotaCOFINS;

        public decimal AliquotaCOFINS
        {
            get { return aliquotaCOFINS; }
            set { aliquotaCOFINS = value; }
        }

        private decimal aliquotaIR;

        public decimal AliquotaIR
        {
            get { return aliquotaIR; }
            set { aliquotaIR = value; }
        }

        private decimal aliquotaCSLL;

        public decimal AliquotaCSLL
        {
            get { return aliquotaCSLL; }
            set { aliquotaCSLL = value; }
        }

        private decimal aliquotaISS;

        public decimal AliquotaISS
        {
            get { return aliquotaISS; }
            set { aliquotaISS = value; }
        }

        public Empresa() {}

        public Empresa(
            Int64? id, 
            String razaoSocial, 
            String fantasia, 
            String cnpj, 
            String inscricao, 
            String endereco, 
            String numero, 
            String complemento, 
            String bairro, 
            String cep, 
            String uf, 
            String telefone1,
            String telefone2, 
            String email, 
            String site, 
            DateTime? dataCadastro, 
            String situacao, 
            String mimetype, 
            byte[] logo,
            CidadeIbge cidade, 
            Boolean simplesNacional, 
            Empresa matriz, 
            string codigoCnes,
            decimal valorMinimoImpostoFederal,
            decimal valorMinimoIR,
            decimal aliquotaPIS,
            decimal aliquotaCOFINS,
            decimal aliquotaIR,
            decimal aliquotaCSLL,
            decimal aliquotaISS
            )
        {
            this.Id = id; 
            this.RazaoSocial = razaoSocial;
            this.Fantasia = fantasia;
            this.Cnpj = cnpj;
            this.Inscricao = inscricao;
            this.Endereco = endereco;
            this.Numero = numero;
            this.Complemento = complemento;
            this.Bairro = bairro;
            this.Cep = cep;
            this.Uf = uf;
            this.Telefone1 = telefone1;
            this.Telefone2 = telefone2;
            this.Email = email;
            this.Site = site;
            this.DataCadastro = dataCadastro;
            this.Situacao = situacao;
            this.Logo = logo;
            this.Mimetype = mimetype;
            this.Cidade = cidade;
            this.SimplesNacional = simplesNacional;
            this.Matriz = matriz;
            this.CodigoCnes = codigoCnes;
            this.ValorMinimoImpostoFederal = valorMinimoImpostoFederal;
            this.ValorMinimoIR = valorMinimoIR;
            this.AliquotaPIS = aliquotaPIS;
            this.AliquotaCOFINS = aliquotaCOFINS;
            this.AliquotaIR = aliquotaIR;
            this.AliquotaCSLL = aliquotaCSLL;
            this.AliquotaISS = aliquotaISS;
        }

        public Empresa(Int64? id)
        {
            this.Id = id;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Empresa p = obj as Empresa;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

        public Boolean isNullForFilter()
        {
            Boolean retorno = Convert.ToBoolean(false);

            if (String.IsNullOrWhiteSpace(this.razaoSocial) && String.IsNullOrWhiteSpace(this.cnpj) && String.IsNullOrWhiteSpace(this.situacao))
            {
                retorno = Convert.ToBoolean(true);
            }

            return retorno;
        }
        
    }
}
