﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.View.Entidade;

namespace SWS.Entidade
{
    public class Epi
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private String descricao;

        public String Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }
        private String finalidade;

        public String Finalidade
        {
            get { return finalidade; }
            set { finalidade = value; }
        }
        private String ca;

        public String Ca
        {
            get { return ca; }
            set { ca = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }

        public Epi() { }

        public Epi(long id)
        {
            this.Id = id;
        }

        public Epi(Int64? id, String descricao, String finalidade, String ca, String situacao)
        {
            this.Id = id;
            this.Descricao = descricao;
            this.Finalidade = finalidade;
            this.Ca = ca;
            this.Situacao = situacao;
        }

        public Boolean isNullForFilter()
        {
            Boolean retorno = Convert.ToBoolean(false);

            if (String.IsNullOrWhiteSpace(this.descricao) && String.IsNullOrWhiteSpace(this.finalidade) && String.IsNullOrWhiteSpace(this.ca) &&
                String.IsNullOrWhiteSpace(this.situacao))
            {
                retorno = Convert.ToBoolean(true);
            }

            return retorno;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Epi p = obj as Epi;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
    }
}
