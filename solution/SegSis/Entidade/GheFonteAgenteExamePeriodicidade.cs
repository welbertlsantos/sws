﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class GheFonteAgenteExamePeriodicidade
    {

        private Periodicidade periodicidade;

        public Periodicidade Periodicidade
        {
            get { return periodicidade; }
            set { periodicidade = value; }
        }
        private GheFonteAgenteExame gheFonteAgenteExame;

        public GheFonteAgenteExame GheFonteAgenteExame
        {
            get { return gheFonteAgenteExame; }
            set { gheFonteAgenteExame = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }

        private int? periodoVencimento;

        public int? PeriodoVencimento
        {
            get { return periodoVencimento; }
            set { periodoVencimento = value; }
        }

        public GheFonteAgenteExamePeriodicidade(){}

        public GheFonteAgenteExamePeriodicidade(Periodicidade periodicidade, GheFonteAgenteExame gheFonteAgenteExame, String situacao, int? periodoVencimento)
        {
            this.Periodicidade = periodicidade;
            this.GheFonteAgenteExame = gheFonteAgenteExame;
            this.Situacao = situacao;
            this.PeriodoVencimento = periodoVencimento;
        }

        public GheFonteAgenteExamePeriodicidade copy()
        {
            return (GheFonteAgenteExamePeriodicidade)this.MemberwiseClone();
        }

    }
}
