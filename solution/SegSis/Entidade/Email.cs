﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Email
    {
        
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }

        private Prontuario prontuario;

        public Prontuario Prontuario
        {
            get { return prontuario; }
            set { prontuario = value; }
        }

        private String para;

        public String Para
        {
            get { return para; }
            set { para = value; }
        }

        private String assunto;

        public String Assunto
        {
            get { return assunto; }
            set { assunto = value; }
        }

        private String mensagem;

        public String Mensagem
        {
            get { return mensagem; }
            set { mensagem = value; }
        }

        private String anexo;

        public String Anexo
        {
            get { return anexo; }
            set { anexo = value; }
        }

        private String prioridade;

        public String Prioridade
        {
            get { return prioridade; }
            set { prioridade = value; }
        }

        private DateTime dataEnvio;

        public DateTime DataEnvio
        {
            get { return dataEnvio; }
            set { dataEnvio = value; }
        }

        private Usuario usuario;

        public Usuario Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }


        public Email(Int64? id,
            Prontuario prontuario,
            String para,
            String assunto,
            String mensagem,
            String anexo,
            String prioridade,
            DateTime dataEnvio, Usuario usuario)
        {
            this.Id = id;
            this.Prontuario = prontuario;
            this.Para = para;
            this.Assunto = assunto.Trim();
            this.Mensagem = mensagem.Trim();
            this.Anexo = anexo.Trim();
            this.Prioridade = prioridade;
            this.DataEnvio = dataEnvio;
            this.Usuario = usuario;
        }



    }
}
