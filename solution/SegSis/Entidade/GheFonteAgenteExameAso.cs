﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class GheFonteAgenteExameAso
    {
        private Int64? id;
        
        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private SalaExame salaExame;

        public SalaExame SalaExame
        {
            get { return salaExame; }
            set { salaExame = value; }
        }
        private Aso aso;

        public Aso Aso
        {
            get { return aso; }
            set { aso = value; }
        }
        private GheFonteAgenteExame gheFonteAgenteExame;

        public GheFonteAgenteExame GheFonteAgenteExame
        {
            get { return gheFonteAgenteExame; }
            set { gheFonteAgenteExame = value; }
        }
        private DateTime? dataExame;

        public DateTime? DataExame
        {
            get { return dataExame; }
            set { dataExame = value; }
        }
        private Boolean atendido;

        public Boolean Atendido
        {
            get { return atendido; }
            set { atendido = value; }
        }
        private Boolean finalizado;

        public Boolean Finalizado
        {
            get { return finalizado; }
            set { finalizado = value; }
        }
        private String login;

        public String Login
        {
            get { return login; }
            set { login = value; }
        }
        private Boolean devolvido;

        public Boolean Devolvido
        {
            get { return devolvido; }
            set { devolvido = value; }
        }
        private DateTime? dataAtendido;

        public DateTime? DataAtendido
        {
            get { return dataAtendido; }
            set { dataAtendido = value; }
        }
        private DateTime? dataDevolvido;

        public DateTime? DataDevolvido
        {
            get { return dataDevolvido; }
            set { dataDevolvido = value; }
        }
        private DateTime? dataFinalizado;

        public DateTime? DataFinalizado
        {
            get { return dataFinalizado; }
            set { dataFinalizado = value; }
        }
        private Usuario usuario;

        public Usuario Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }
        private Boolean cancelado;

        public Boolean Cancelado
        {
            get { return cancelado; }
            set { cancelado = value; }
        }
        private DateTime? dataCancelado;

        public DateTime? DataCancelado
        {
            get { return dataCancelado; }
            set { dataCancelado = value; }
        }
        private String resultado;

        public String Resultado
        {
            get { return resultado; }
            set { resultado = value; }
        }
        private String observacao;

        public String Observacao
        {
            get { return observacao; }
            set { observacao = value; }
        }
        private Boolean duplicado;

        public Boolean Duplicado
        {
            get { return duplicado; }
            set { duplicado = value; }
        }
        private Boolean transcrito;

        public Boolean Transcrito
        {
            get { return transcrito; }
            set { transcrito = value; }
        }
        private Cliente prestador;

        public Cliente Prestador
        {
            get { return prestador; }
            set { prestador = value; }
        }
        private Protocolo protocolo;

        public Protocolo Protocolo
        {
            get { return protocolo; }
            set { protocolo = value; }
        }
        private Boolean confinado;

        public Boolean Confinado
        {
            get { return confinado; }
            set { confinado = value; }
        }
        private Boolean altura;

        public Boolean Altura
        {
            get { return altura; }
            set { altura = value; }
        }
        private Medico medico;

        public Medico Medico
        {
            get { return medico; }
            set { medico = value; }
        }

        private DateTime? dataValidade;

        public DateTime? DataValidade
        {
            get { return dataValidade; }
            set { dataValidade = value; }
        }

        private bool eletricidade;

        public bool Eletricidade
        {
            get { return eletricidade; }
            set { eletricidade = value; }
        }

        private bool imprimeASO;

        public bool ImprimeASO
        {
            get { return imprimeASO; }
            set { imprimeASO = value; }
        }

        public GheFonteAgenteExameAso() { }


        public GheFonteAgenteExameAso(Int64? id, SalaExame salaExame, Aso aso, GheFonteAgenteExame gheFonteAgenteExame,
            DateTime? dataExame, Boolean atendido, Boolean finalizado, String login,
            Boolean devolvido, DateTime? dataAtendido, DateTime? dataDevolvido, DateTime? dataFinalizado,
            Usuario usuario, Boolean cancelado, DateTime? dataCancelado, String resultado,
            String observacao, Boolean duplicado, Boolean transcrito, Cliente prestador, Protocolo protocolo, 
            Boolean confinado, Boolean altura, Medico medico, DateTime? dataValidade, bool eletricidade, bool imprimeASO)
        {
            this.Id = id;
            this.SalaExame = salaExame;
            this.Aso = aso;
            this.GheFonteAgenteExame = gheFonteAgenteExame;
            this.DataExame = dataExame;
            this.Atendido = atendido;
            this.Finalizado = finalizado;
            this.Login = login;
            this.Devolvido = devolvido;
            this.DataAtendido = dataAtendido;
            this.DataDevolvido = dataDevolvido;
            this.DataFinalizado = dataFinalizado;
            this.Usuario = usuario;
            this.Cancelado = cancelado;
            this.DataCancelado = dataCancelado;
            this.Resultado = resultado;
            this.Observacao = observacao;
            this.Duplicado = duplicado;
            this.Transcrito = transcrito;
            this.Prestador = prestador;
            this.Protocolo = protocolo;
            this.Confinado = confinado;
            this.Altura = altura;
            this.Medico = medico;
            this.DataValidade = dataValidade;
            this.Eletricidade = eletricidade;
            this.ImprimeASO = imprimeASO;
        }

        public GheFonteAgenteExameAso(Int64? id)
        {
            this.Id = id;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            GheFonteAgenteExameAso p = obj as GheFonteAgenteExameAso;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.gheFonteAgenteExame.Id == p.gheFonteAgenteExame.Id && this.aso.Id == p.aso.Id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

    }
}