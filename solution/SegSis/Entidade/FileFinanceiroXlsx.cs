﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class FileFinanceiroXlsx
    {
        private List<object[]> listExames;

        public List<object[]> ListExames
        {
            get { return listExames; }
            set { listExames = value; }
        }

        /* list exames trará os seguintes campos para geração do arquivo xlsx:
         * Empresa do atendimento, CNPJ da empresa do atendimento, Centro de Custo, Nome, rg, funcao, data do aso, codigo do atendimento, tipo de atendimento, exame, preco unitario, quantidade e situacao do movimento. */

        private List<object[]> listProdutos;

        public List<object[]> ListProdutos
        {
            get { return listProdutos; }
            set { listProdutos = value; }
        }

        /* list produtos trará  os seguintes campos para geração de aquivo xlsx:
         * Empresa que foi feito o lançamento, cnpj, Centro de Custo, produto, preco unitário, quantidade, situacao do movimento e data de inclusão */
        public FileFinanceiroXlsx(){}

        public FileFinanceiroXlsx(List<object[]> exame, List<object[]> produto)
        {
            this.ListExames = exame;
            this.ListProdutos = produto;
        }


    }
}
