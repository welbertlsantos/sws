﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class ClienteFuncaoExameASo
    {

        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private SalaExame salaExame;

        public SalaExame SalaExame
        {
            get { return salaExame; }
            set { salaExame = value; }
        }
        private Aso aso;

        public Aso Aso
        {
            get { return aso; }
            set { aso = value; }
        }
        private ClienteFuncaoExame clienteFuncaoExame;

        public ClienteFuncaoExame ClienteFuncaoExame
        {
            get { return clienteFuncaoExame; }
            set { clienteFuncaoExame = value; }
        }
        private DateTime? dataExame;

        public DateTime? DataExame
        {
            get { return dataExame; }
            set { dataExame = value; }
        }
        private Boolean atendido;

        public Boolean Atendido
        {
            get { return atendido; }
            set { atendido = value; }
        }
        private Boolean finalizado;

        public Boolean Finalizado
        {
            get { return finalizado; }
            set { finalizado = value; }
        }
        private String login;

        public String Login
        {
            get { return login; }
            set { login = value; }
        }
        private Boolean devolvido;

        public Boolean Devolvido
        {
            get { return devolvido; }
            set { devolvido = value; }
        }
        private DateTime? dataAtendido;

        public DateTime? DataAtendido
        {
            get { return dataAtendido; }
            set { dataAtendido = value; }
        }
        private DateTime? dataDevolvido;

        public DateTime? DataDevolvido
        {
            get { return dataDevolvido; }
            set { dataDevolvido = value; }
        }
        private DateTime? dataFinalizado;

        public DateTime? DataFinalizado
        {
            get { return dataFinalizado; }
            set { dataFinalizado = value; }
        }
        private Usuario usuario;

        public Usuario Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }
        private Boolean cancelado;

        public Boolean Cancelado
        {
            get { return cancelado; }
            set { cancelado = value; }
        }
        private DateTime? dataCancelado;

        public DateTime? DataCancelado
        {
            get { return dataCancelado; }
            set { dataCancelado = value; }
        }
        private String resultado;

        public String Resultado
        {
            get { return resultado; }
            set { resultado = value; }
        }
        private String observacaoResultado;

        public String ObservacaoResultado
        {
            get { return observacaoResultado; }
            set { observacaoResultado = value; }
        }
        private Boolean transcrito;

        public Boolean Transcrito
        {
            get { return transcrito; }
            set { transcrito = value; }
        }
        private Cliente prestador;

        public Cliente Prestador
        {
            get { return prestador; }
            set { prestador = value; }
        }
        private Protocolo protocolo;

        public Protocolo Protocolo
        {
            get { return protocolo; }
            set { protocolo = value; }
        }
        private Medico medico;

        public Medico Medico
        {
            get { return medico; }
            set { medico = value; }
        }

        private DateTime? dataValidade;

        public DateTime? DataValidade
        {
            get { return dataValidade; }
            set { dataValidade = value; }
        }

        private bool imprimeASO;

        public bool ImprimeASO
        {
            get { return imprimeASO; }
            set { imprimeASO = value; }
        }

        public ClienteFuncaoExameASo() { }

        public ClienteFuncaoExameASo(Int64? id, SalaExame salaExame, Aso aso, ClienteFuncaoExame clienteFuncaoExame,
            DateTime? dataExame, Boolean atendido, Boolean finalizado, String login, Boolean devolvido,
            DateTime? dataAtendido, DateTime? dataDevolvido, DateTime? dataFinalizado, 
            Usuario usuario, Boolean cancelado, DateTime? dataCancelado, String resultado, String observacaoResultado, 
            Boolean transcrito, Cliente prestador, Protocolo protocolo, Medico medico, DateTime? dataValidade, bool imprimeASO)
        {
            this.Id = id;
            this.Aso = aso;
            this.ClienteFuncaoExame = clienteFuncaoExame;
            this.DataExame = dataExame;
            this.Atendido = atendido;
            this.Finalizado = finalizado;
            this.Login = login;
            this.Devolvido = devolvido;
            this.DataAtendido = dataAtendido;
            this.DataDevolvido = dataDevolvido;
            this.DataFinalizado = dataFinalizado;
            this.Usuario = usuario;
            this.Cancelado = cancelado;
            this.DataCancelado = dataCancelado;
            this.Resultado = resultado;
            this.ObservacaoResultado = observacaoResultado;
            this.SalaExame = salaExame;
            this.Transcrito = transcrito;
            this.Prestador = prestador;
            this.Protocolo = protocolo;
            this.Medico = medico;
            this.DataValidade = dataValidade;
            this.ImprimeASO = imprimeASO;
            
        }

        public ClienteFuncaoExameASo(Int64? id)
        {
            this.Id = id;
        }


        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            ClienteFuncaoExameASo p = obj as ClienteFuncaoExameASo;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.clienteFuncaoExame.Id == p.clienteFuncaoExame.Id && this.aso.Id == p.aso.Id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

    }
}
