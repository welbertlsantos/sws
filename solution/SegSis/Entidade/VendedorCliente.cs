﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;

namespace SWS.Entidade
{
    public class VendedorCliente
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private Cliente cliente;

        public Cliente Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }
        private Vendedor vendedor;

        public Vendedor Vendedor
        {
            get { return vendedor; }
            set { vendedor = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }

        public VendedorCliente(){}

        public VendedorCliente(Int64? id, Cliente cliente, Vendedor vendedor, String situacao)
        {
            this.Id = id;
            this.Cliente = cliente;
            this.Vendedor = vendedor;
            this.Situacao = situacao;
        }

        public VendedorCliente(Int64? id)
        {
            this.Id = id;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            VendedorCliente p = obj as VendedorCliente ;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

    }
}
