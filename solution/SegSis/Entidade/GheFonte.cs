﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using SWS.View;
using System.Windows.Forms;

namespace SWS.Entidade
{
    public class GheFonte
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private Fonte fonte;

        public Fonte Fonte
        {
            get { return fonte; }
            set { fonte = value; }
        }
        private Ghe ghe;

        public Ghe Ghe
        {
            get { return ghe; }
            set { ghe = value; }
        }
        private Boolean principal;

        public Boolean Principal
        {
            get { return principal; }
            set { principal = value; }
        }

        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }

        public GheFonte() { }
        
        public GheFonte(Int64? idGheFonte, Fonte fonte, Ghe ghe)
        {
            this.Id = idGheFonte;
            this.Fonte = fonte;
            this.Ghe = ghe;
        }

        public GheFonte(Int64? idGheFonte, Fonte fonte, Ghe ghe, Boolean principal, String situacao)
        {
            this.Id = idGheFonte;
            this.Fonte = fonte;
            this.Ghe = ghe;
            this.Principal = principal;
            this.Situacao = situacao;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            GheFonte p = obj as GheFonte;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            if ((p.Ghe == null))
            {
                return (this.fonte.Id == p.fonte.Id);
            }
            else
            {
                if ((p.Fonte == null))
                {
                    return (this.Ghe.Id == p.Ghe.Id);
                }
                else
                {
                    return (this.fonte.Id == p.fonte.Id && Ghe.Id == p.Ghe.Id);
                }
            }
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

    }
}
