﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class GheSetorClienteFuncao
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private ClienteFuncao clienteFuncao;

        public ClienteFuncao ClienteFuncao
        {
            get { return clienteFuncao; }
            set { clienteFuncao = value; }
        }
        private GheSetor gheSetor;

        public GheSetor GheSetor
        {
            get { return gheSetor; }
            set { gheSetor = value; }
        }
        private ComentarioFuncao comentarioFuncao;

        public ComentarioFuncao ComentarioFuncao
        {
            get { return comentarioFuncao; }
            set { comentarioFuncao = value; }
        }
        private Boolean confinado;

        public Boolean Confinado
        {
            get { return confinado; }
            set { confinado = value; }
        }
        private Boolean altura;

        public Boolean Altura
        {
            get { return altura; }
            set { altura = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }

        private bool eletricidade;

        public bool Eletricidade
        {
            get { return eletricidade; }
            set { eletricidade = value; }
        }

        public GheSetorClienteFuncao(Int64? id, ClienteFuncao clienteFuncao, GheSetor gheSetor, Boolean confinado, Boolean altura, String situacao, ComentarioFuncao comentarioFuncao, bool eletricidade)
        {
            this.Id = id;
            this.ClienteFuncao = clienteFuncao;
            this.GheSetor = gheSetor;
            this.Confinado = confinado;
            this.Altura = altura;
            this.Situacao = situacao;
            this.ComentarioFuncao = comentarioFuncao;
            this.Eletricidade = eletricidade;
        }

        public GheSetorClienteFuncao() { }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            GheSetorClienteFuncao p = obj as GheSetorClienteFuncao;
            if (p == null)
            {
                return false;
            }

            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

        public GheSetorClienteFuncao copy()
        {
            return (GheSetorClienteFuncao) this.MemberwiseClone();
        }
        



    }
}
