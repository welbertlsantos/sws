﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.View.Entidade;

namespace SWS.Entidade
{
    public class Usuario
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }

        private String nome;

        public String Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        private String cpf;

        public String Cpf
        {
            get { return cpf; }
            set { cpf = value.Replace(".", "").Replace(",", "").Replace("-", "").Trim(); }
        }

        private String rg;

        public String Rg
        {
            get { return rg; }
            set { rg = value; }
        }

        private String documentoExtra;

        public String DocumentoExtra
        {
            get { return documentoExtra; }
            set { documentoExtra = value; }
        }

        private DateTime? dataAdmissao;

        public DateTime? DataAdmissao
        {
            get { return dataAdmissao; }
            set { dataAdmissao = value; }
        }

        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }

        private String login;

        public String Login
        {
            get { return login; }
            set { login = value; }
        }
        private String senha;

        public String Senha
        {
            get { return senha; }
            set { senha = value; }
        }

        private String email;

        public String Email
        {
            get { return email; }
            set { email = value; }
        }

        private Medico medico;

        public Medico Medico
        {
            get { return medico; }
            set { medico = value; }
        }

        private Empresa empresa;

        public Empresa Empresa
        {
            get { return empresa; }
            set { empresa = value; }
        }

        private String mte; 

        public String Mte
        {
            get { return mte; }
            set { mte = value; }
        }

        private Boolean elaboraDocumento;

        public Boolean ElaboraDocumento
        {
            get { return elaboraDocumento; }
            set { elaboraDocumento = value; }
        }

        private byte[] assinatura;
        
        public byte[] Assinatura
        {
            get { return assinatura; }
            set { assinatura = value; }
        }

        private String mimetype;
        
        public String Mimetype
        {
            get { return mimetype; }
            set { mimetype = value; }
        }

        private bool externo;

        public bool Externo
        {
            get { return externo; }
            set { externo = value; }
        }

        private string orgaoEmissor;

        public string OrgaoEmissor
        {
            get { return orgaoEmissor; }
            set { orgaoEmissor = value; }
        }

        private string ufOrgaoEmissor;

        public string UfOrgaoEmissor
        {
            get { return ufOrgaoEmissor; }
            set { ufOrgaoEmissor = value; }
        }
       
        private HashSet<Perfil> perfil = new HashSet<Perfil>();

        public HashSet<Perfil> Perfil
        {
            get { return perfil; }
            set { perfil = value; }
        }
        private FuncaoInterna funcaoInterna = new FuncaoInterna();

        public FuncaoInterna FuncaoInterna
        {
            get { return funcaoInterna; }
            set { funcaoInterna = value; }
        }
        
        private List<UsuarioCliente> usuarioCliente;

        public List<UsuarioCliente> UsuarioCliente
        {
            get { return usuarioCliente; }
            set { usuarioCliente = value; }
        }

        public Usuario(Int64? id, String nome, String cpf, String rg, String documentoExtra, DateTime? dataAdmissao, String situacao, String login, String senha, String email, Medico coordenador, Empresa empresa, String mte, Boolean elaboraDocumento, byte[] assinatura, String mimetype, bool externo, string orgaoEmissor, string ufOrgaoEmissor)
        {
            this.Id = id;
            this.Nome = nome;
            this.Cpf = cpf;
            this.Rg = rg;
            this.DocumentoExtra = documentoExtra;
            this.DataAdmissao = dataAdmissao;
            this.Situacao = situacao;
            this.Login = login;
            this.Senha = senha;
            this.Email = email;
            this.Medico = coordenador;
            this.Empresa = empresa;
            this.Mte = mte;
            this.ElaboraDocumento = elaboraDocumento;
            this.assinatura = assinatura;
            this.Mimetype = mimetype;
            this.Externo = externo;
            this.OrgaoEmissor = orgaoEmissor;
            this.UfOrgaoEmissor = ufOrgaoEmissor;
        }

        public Usuario(String login, String senha)
        {
            this.Login = login;
            this.Senha = senha;
        }

        public Usuario(Int64? id)
        {
            this.Id = id;
        }

        public Usuario() { }

        public Boolean isNullForFilter()
        {
            Boolean retorno = Convert.ToBoolean(false);

            if (String.IsNullOrWhiteSpace(this.nome) && String.IsNullOrWhiteSpace(this.cpf) && String.IsNullOrWhiteSpace(this.login) && String.IsNullOrWhiteSpace(this.situacao))
            {
                retorno = Convert.ToBoolean(true);
            }

            return retorno;
        }

        public Boolean isNullForFilterFuncaoSeleciona()
        {
            Boolean retorno = Convert.ToBoolean(false);

            if (String.IsNullOrWhiteSpace(this.nome))
            {
                retorno = Convert.ToBoolean(true);
            }

            return retorno;
        }
        
        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Usuario p = obj as Usuario;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

    }

}
