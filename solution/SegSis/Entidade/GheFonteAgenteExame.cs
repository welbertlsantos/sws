﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class GheFonteAgenteExame
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private Exame exame;

        public Exame Exame
        {
            get { return exame; }
            set { exame = value; }
        }
        private GheFonteAgente gheFonteAgente;

        public GheFonteAgente GheFonteAgente
        {
            get { return gheFonteAgente; }
            set { gheFonteAgente = value; }
        }
        private Int32 idadeExame;

        public Int32 IdadeExame
        {
            get { return idadeExame; }
            set { idadeExame = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        private Boolean confinado;

        public Boolean Confinado
        {
            get { return confinado; }
            set { confinado = value; }
        }
        private Boolean altura;

        public Boolean Altura
        {
            get { return altura; }
            set { altura = value; }
        }

        private bool eletricidade;

        public bool Eletricidade
        {
            get { return eletricidade; }
            set { eletricidade = value; }
        }

        public GheFonteAgenteExame() { }

        public GheFonteAgenteExame(Int64 id)
        { 
            this.Id = id;
        }

        public GheFonteAgenteExame(Int64? id, Exame exame, GheFonteAgente gheFonteAgente, Int32 idadeExame, String situacao, Boolean confinado, Boolean altura, bool eletricidade)
        {
            this.Id = id;
            this.Exame = exame;
            this.GheFonteAgente = gheFonteAgente;
            this.IdadeExame = idadeExame;
            this.Situacao = situacao;
            this.Confinado = confinado;
            this.Altura = altura;
            this.Eletricidade = eletricidade;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            GheFonteAgenteExame p = obj as GheFonteAgenteExame;
            if (p == null)
            {
                return false;
            }

            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

        public GheFonteAgenteExame copy()
        {
            return (GheFonteAgenteExame)this.MemberwiseClone();
        }

    }
}
