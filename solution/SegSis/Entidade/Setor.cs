﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Setor
    {
        private long? id;

        public long? Id
        {
            get { return id; }
            set { id = value; }
        }

        private string descricao;

        public string Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }

        private string situacao;

        public string Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }

        public Setor() { }

        public Setor(long? id, string descricao, string situacao)
        {
            this.Id = id;
            this.Descricao = descricao;
            this.Situacao = situacao;
        }

        public Setor(long id)
        {
            this.Id = id;
        }

        public void setId(long? id)
        {
            this.Id = id;
        }

        public Boolean isNullForFilter()
        {
            Boolean retorno = Convert.ToBoolean(false);

            if (String.IsNullOrWhiteSpace(this.descricao) && String.IsNullOrWhiteSpace(this.situacao)) 
            {
                retorno = Convert.ToBoolean(true);
            }

            return retorno;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Setor p = obj as Setor;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

    }
}
