﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS.Entidade
{
    class Filial
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private string host;

        public string Host
        {
            get { return host; }
            set { host = value; }
        }

        private string port;

        public string Port
        {
            get { return port; }
            set { port = value; }
        }

        private string user;

        public string User
        {
            get { return user; }
            set { user = value; }
        }

        private string password;

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        private string database;

        public string Database
        {
            get { return database; }
            set { database = value; }
        }

        private string schema;

        public string Schema
        {
            get { return schema; }
            set { schema = value; }
        }

        public Filial() { }

    }
}
