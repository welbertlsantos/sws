﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class MedicoEstudo
    {
        private Estudo pcmso;

        public Estudo Pcmso
        {
            get { return pcmso; }
            set { pcmso = value; }
        }
        
        private Medico medico;

        public Medico Medico
        {
            get { return medico; }
            set { medico = value; }
        }
        
        private Boolean coordenador;

        public Boolean Coordenador
        {
            get { return coordenador; }
            set { coordenador = value; }
        }

        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }

        public MedicoEstudo(){}

        public MedicoEstudo(Estudo pcmso, Medico medico, Boolean coordenador, String situacao)
        {
            Pcmso = pcmso;
            Medico = medico;
            Coordenador = coordenador;
            Situacao = situacao;
            
        }
    }
}
