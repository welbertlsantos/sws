﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Exame : IComparable<Exame>
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private String descricao;

        public String Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        private Boolean? laboratorio;

        public Boolean? Laboratorio
        {
            get { return laboratorio; }
            set { laboratorio = value; }
        }
        private Decimal preco;

        public Decimal Preco
        {
            get { return preco; }
            set { preco = value; }
        }
        private Int32 prioridade;

        public Int32 Prioridade
        {
            get { return prioridade; }
            set { prioridade = value; }
        }
        private Decimal custo;

        public Decimal Custo
        {
            get { return custo; }
            set { custo = value; }
        }
        private Boolean externo;

        public Boolean Externo
        {
            get { return externo; }
            set { externo = value; }
        }
        private Boolean liberaDocumento;

        public Boolean LiberaDocumento
        {
            get { return liberaDocumento; }
            set { liberaDocumento = value; }
        }

        private bool padraoContrato;

        public bool PadraoContrato
        {
            get { return padraoContrato; }
            set { padraoContrato = value; }
        }

        private List<MedicoExame> medicoExame;

        public List<MedicoExame> MedicoExame
        {
            get { return medicoExame; }
            set { medicoExame = value; }
        }

        private string codigoTuss;

        public string CodigoTuss
        {
            get { return codigoTuss; }
            set { codigoTuss = value; }
        }

        private bool exameComplementar;

        public bool ExameComplementar
        {
            get { return exameComplementar; }
            set { exameComplementar = value; }
        }

        private RelatorioExame relatorioExame;

        public RelatorioExame RelatorioExame
        {
            get { return relatorioExame; }
            set { relatorioExame = value; }
        }

        private int? periodoVencimento;

        public int? PeriodoVencimento
        {
            get { return periodoVencimento; }
            set { periodoVencimento = value; }
        }

        public Exame() { }

        public Exame(Int64? id) 
        {
            this.Id = id;
        }

        public Exame(Int64? id, String nome)
        {
            this.Id = id;
            this.Descricao = nome;
        }

        public Exame(Int64? id, String descricao, String situacao, Boolean? laboratorio, 
            Decimal preco, Int32 prioridade, Decimal custo, Boolean externo,
            Boolean liberaDocumento, string codigoTuss, bool exameComplementar, int? periodoVencimento, bool padraoContrato)
        {
            this.Id = id;
            this.Descricao = descricao;
            this.Situacao = situacao;
            this.Laboratorio= laboratorio;
            this.Preco = preco;
            this.Prioridade = prioridade;
            this.Custo = custo;
            this.Externo = externo;
            this.LiberaDocumento = liberaDocumento;
            this.CodigoTuss = codigoTuss;
            this.ExameComplementar = exameComplementar;
            this.PeriodoVencimento = periodoVencimento;
            this.PadraoContrato = padraoContrato;
        }


        public Boolean isNullForFilter()
        {
            Boolean retorno = Convert.ToBoolean(false);

            if (String.IsNullOrWhiteSpace(this.descricao) && String.IsNullOrWhiteSpace(situacao) && laboratorio == null)
            {
                retorno = Convert.ToBoolean(true);
            }

            return retorno;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Exame p = obj as Exame;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

        public int CompareTo(Exame b)
        {
            // Alphabetic sort name[A to Z]
            return this.descricao.CompareTo(b.descricao);
        }
    }
}
