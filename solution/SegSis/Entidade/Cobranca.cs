﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Cobranca
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private Forma forma;

        public Forma Forma
        {
            get { return forma; }
            set { forma = value; }
        }
        private Usuario usuarioCancela;

        public Usuario UsuarioCancela
        {
            get { return usuarioCancela; }
            set { usuarioCancela = value; }
        }
        private Usuario usuarioEstorna;

        public Usuario UsuarioEstorna
        {
            get { return usuarioEstorna; }
            set { usuarioEstorna = value; }
        }
        private Usuario usuarioDesdobra;

        public Usuario UsuarioDesdobra
        {
            get { return usuarioDesdobra; }
            set { usuarioDesdobra = value; }
        }
        private Usuario usuarioCriador;

        public Usuario UsuarioCriador
        {
            get { return usuarioCriador; }
            set { usuarioCriador = value; }
        }
        private Usuario usuarioBaixa;

        public Usuario UsuarioBaixa
        {
            get { return usuarioBaixa; }
            set { usuarioBaixa = value; }
        }
        private Conta conta;

        public Conta Conta
        {
            get { return conta; }
            set { conta = value; }
        }
        private Nfe nota;

        public Nfe Nota
        {
            get { return nota; }
            set { nota = value; }
        }
        private Decimal valor;

        public Decimal Valor
        {
            get { return valor; }
            set { valor = value; }
        }
        private DateTime dataEmissao;

        public DateTime DataEmissao
        {
            get { return dataEmissao; }
            set { dataEmissao = value; }
        }
        private DateTime dataVencimento;

        public DateTime DataVencimento
        {
            get { return dataVencimento; }
            set { dataVencimento = value; }
        }
        private DateTime? dataPagamento;

        public DateTime? DataPagamento
        {
            get { return dataPagamento; }
            set { dataPagamento = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        private Int32 numeroParcela;

        public Int32 NumeroParcela
        {
            get { return numeroParcela; }
            set { numeroParcela = value; }
        }
        private String numeroDocumento;

        public String NumeroDocumento
        {
            get { return numeroDocumento; }
            set { numeroDocumento = value; }
        }
        private DateTime? dataAlteracao;

        public DateTime? DataAlteracao
        {
            get { return dataAlteracao; }
            set { dataAlteracao = value; }
        }
        private String observacaoCancelamento;

        public String ObservacaoCancelamento
        {
            get { return observacaoCancelamento; }
            set { observacaoCancelamento = value; }
        }
        private Cobranca cobrancaDesdobrada;

        public Cobranca CobrancaDesdobrada
        {
            get { return cobrancaDesdobrada; }
            set { cobrancaDesdobrada = value; }
        }
        private String observacaoCobranca;

        public String ObservacaoCobranca
        {
            get { return observacaoCobranca; }
            set { observacaoCobranca = value; }
        }
        private String observacaoBaixa;

        public String ObservacaoBaixa
        {
            get { return observacaoBaixa; }
            set { observacaoBaixa = value; }
        }
        private DateTime? dataBaixa;

        public DateTime? DataBaixa
        {
            get { return dataBaixa; }
            set { dataBaixa = value; }
        }
        private String observacaoEstorno;

        public String ObservacaoEstorno
        {
            get { return observacaoEstorno; }
            set { observacaoEstorno = value; }
        }
        private DateTime? dataEstorno;

        public DateTime? DataEstorno
        {
            get { return dataEstorno; }
            set { dataEstorno = value; }
        }
        private Usuario usuarioProrroga;

        public Usuario UsuarioProrroga
        {
            get { return usuarioProrroga; }
            set { usuarioProrroga = value; }
        }
        private String observacaoProrrogacao;

        public String ObservacaoProrrogacao
        {
            get { return observacaoProrrogacao; }
            set { observacaoProrrogacao = value; }
        }
        private Decimal? valorBaixado;

        public Decimal? ValorBaixado
        {
            get { return valorBaixado; }
            set { valorBaixado = value; }
        }
        private Decimal? valorAjustado;

        public Decimal? ValorAjustado
        {
            get { return valorAjustado; }
            set { valorAjustado = value; }
        }
        private String nossoNumero;

        public String NossoNumero
        {
            get { return nossoNumero; }
            set { nossoNumero = value; }
        }

        public Cobranca() { }

        public Cobranca( Int64? id, Forma forma, Usuario usuarioCancela, Usuario usuarioEstorna, Usuario usuarioDesdobra, Usuario usuarioCriador, Usuario usuarioBaixa, Conta conta, Nfe nota, Decimal valor, DateTime dataEmissao, DateTime dataVencimento, DateTime? dataPagamento, String situacao, Int32 numeroParcela, String numeroDocumento, DateTime? dataAlteracao, String observacaoCancelamento, Cobranca cobrancaDesdobrada, String observacaoCobranca, String observacaoBaixa, DateTime? dataBaixa, String observacaoEstorno, DateTime? dataEstorno, Usuario usuarioProrroga, String observacaoProrrogacao,  Decimal? valorBaixado, Decimal? valorAjustado, String nossoNumero)
        {
            this.Id = id;
            this.Forma = forma;
            this.UsuarioCancela = usuarioCancela;
            this.UsuarioEstorna = usuarioEstorna;
            this.UsuarioDesdobra = usuarioDesdobra;
            this.UsuarioCriador = usuarioCriador;
            this.UsuarioBaixa = usuarioBaixa;
            this.Conta = conta;
            this.Nota = nota;
            this.Valor = valor;
            this.DataEmissao = dataEmissao;
            this.DataPagamento = dataPagamento;
            this.DataVencimento = dataVencimento;
            this.DataPagamento = dataPagamento;
            this.Situacao = situacao;
            this.NumeroParcela = numeroParcela;
            this.NumeroDocumento = numeroDocumento;
            this.DataAlteracao = dataAlteracao;
            this.ObservacaoCancelamento = observacaoCancelamento;
            this.CobrancaDesdobrada = cobrancaDesdobrada;
            this.ObservacaoCobranca = observacaoCobranca;
            this.ObservacaoBaixa = observacaoBaixa;
            this.DataBaixa = dataBaixa;
            this.ObservacaoEstorno = observacaoEstorno;
            this.DataEstorno = dataEstorno;
            this.UsuarioProrroga = usuarioProrroga;
            this.ObservacaoProrrogacao = observacaoProrrogacao;
            this.ValorBaixado = valorBaixado;
            this.ValorAjustado = valorAjustado;
            this.NossoNumero = nossoNumero;
        }

        public Cobranca(Int64? id)
        {
            this.Id = id;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Cobranca p = obj as Cobranca;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

    }
}
