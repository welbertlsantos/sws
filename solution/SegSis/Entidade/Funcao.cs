﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.View.Entidade;

namespace SWS.Entidade
{
    
    public class Funcao
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private String descricao;

        public String Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }
        private String codCbo;

        public String CodCbo
        {
            get { return codCbo; }
            set { codCbo = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        private String comentario;

        public String Comentario
        {
            get { return comentario; }
            set { comentario = value; }
        }

        private List<ComentarioFuncao> atividades = new List<ComentarioFuncao>();

        public List<ComentarioFuncao> Atividades
        {
            get { return atividades; }
            set { atividades = value; }
        }

        public Funcao() { }

        public Funcao(Int64? id, String descricao, String codCbo, String situacao, String comentario)
        {
            this.Id = id;
            this.Descricao = descricao;
            this.CodCbo = codCbo;
            this.Situacao = situacao;
            this.Comentario = comentario;
        }

        public Boolean isNullForFilter()
        {
            Boolean retorno = Convert.ToBoolean(false);

            if (String.IsNullOrWhiteSpace(this.Descricao) && String.IsNullOrWhiteSpace(this.situacao))
            {
                retorno = Convert.ToBoolean(true);
            }
            
            return retorno;
                    
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Funcao p = obj as Funcao;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
    }
}
