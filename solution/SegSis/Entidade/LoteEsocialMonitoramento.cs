﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class LoteEsocialMonitoramento
    {
        private long? id;

        public long? Id
        {
            get { return id; }
            set { id = value; }
        }
        private LoteEsocial loteEsocial;

        public LoteEsocial LoteEsocial
        {
            get { return loteEsocial; }
            set { loteEsocial = value; }
        }
        private Monitoramento monitoramento;

        public Monitoramento Monitoramento
        {
            get { return monitoramento; }
            set { monitoramento = value; }
        }

        private string situacao;

        public string Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }

        private string nRecibo;

        public string NRecibo
        {
            get { return nRecibo; }
            set { nRecibo = value; }
        }

        private DateTime? dataCancelamento;

        public DateTime? DataCancelamento
        {
            get { return dataCancelamento; }
            set { dataCancelamento = value; }
        }

        private string usuarioCancelou;

        public string UsuarioCancelou
        {
            get { return usuarioCancelou; }
            set { usuarioCancelou = value; }
        }


        public LoteEsocialMonitoramento() { }

        public LoteEsocialMonitoramento(long id) { this.Id = id; }

        public LoteEsocialMonitoramento(long? id, LoteEsocial loteEsocial, Monitoramento monitoramento, string situacao, DateTime? dataCancelamento, string usuarioCancelou)
        {
            this.Id = id;
            this.LoteEsocial = loteEsocial;
            this.Monitoramento = monitoramento;
            this.Situacao = situacao;
            this.DataCancelamento = dataCancelamento;
            this.UsuarioCancelou = usuarioCancelou;
        }
    
    }
}
