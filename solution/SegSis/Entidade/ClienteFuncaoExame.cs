﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class ClienteFuncaoExame
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private ClienteFuncao clienteFuncao;

        public ClienteFuncao ClienteFuncao
        {
            get { return clienteFuncao; }
            set { clienteFuncao = value; }
        }
        private Exame exame;

        public Exame Exame
        {
            get { return exame; }
            set { exame = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        private Int32 idadeExame;

        public Int32 IdadeExame
        {
            get { return idadeExame; }
            set { idadeExame = value; }
        }

        public ClienteFuncaoExame(Int64 id)
        {
            this.Id = id;
        }

        public ClienteFuncaoExame() { }

        public ClienteFuncaoExame(Int64? id, ClienteFuncao clienteFuncao, Exame exame, String situacao, Int32 idadeExame)
        {
            this.Id = id;
            this.ClienteFuncao = clienteFuncao;
            this.Exame = exame;
            this.Situacao = situacao;
            this.IdadeExame = idadeExame;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            ClienteFuncaoExame p = obj as ClienteFuncaoExame;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
    
    }
}
