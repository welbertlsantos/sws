﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class ItemFilaAtendimento
    {
        private Int64? idItem;

        public Int64? IdItem
        {
            get { return idItem; }
            set { idItem = value; }
        }
        private String nome;

        public String Nome
        {
            get { return nome; }
            set { nome = value; }
        }
        private String dataNascimento;

        public String DataNascimento
        {
            get { return dataNascimento; }
            set { dataNascimento = value; }
        }
        private String cpf;

        public String Cpf
        {
            get { return cpf; }
            set { cpf = value; }
        }
        private String funcao;

        public String Funcao
        {
            get { return funcao; }
            set { funcao = value; }
        }
        private String empresa;

        public String Empresa
        {
            get { return empresa; }
            set { empresa = value; }
        }
        private String login;

        public String Login
        {
            get { return login; }
            set { login = value; }
        }
        private String tipoAtendimento;

        public String TipoAtendimento
        {
            get { return tipoAtendimento; }
            set { tipoAtendimento = value; }
        }
        private String tipoExame;

        public String TipoExame
        {
            get { return tipoExame; }
            set { tipoExame = value; }
        }
        private Exame exame;

        public Exame Exame
        {
            get { return exame; }
            set { exame = value; }
        }
        private Boolean devolvido;

        public Boolean Devolvido
        {
            get { return devolvido; }
            set { devolvido = value; }
        }
        private DateTime dataExame;

        public DateTime DataExame
        {
            get { return dataExame; }
            set { dataExame = value; }
        }
        private String rg;

        public String Rg
        {
            get { return rg; }
            set { rg = value; }
        }
        private String nomeSala;

        public String NomeSala
        {
            get { return nomeSala; }
            set { nomeSala = value; }
        }
        private Int32? andar;

        public Int32? Andar
        {
            get { return andar; }
            set { andar = value; }
        }
        private Int32? peso;

        public Int32? Peso
        {
            get { return peso; }
            set { peso = value; }
        }
        private Decimal? altura;

        public Decimal? Altura
        {
            get { return altura; }
            set { altura = value; }
        }
        private String periodicidade;

        public String Periodicidade
        {
            get { return periodicidade; }
            set { periodicidade = value; }
        }
        private String imc;

        public String Imc
        {
            get { return imc; }
            set { imc = value; }
        }
        private DateTime? dataAtendido;

        public DateTime? DataAtendido
        {
            get { return dataAtendido; }
            set { dataAtendido = value; }
        }
        private String codigoAtendimento;

        public String CodigoAtendimento
        {
            get { return codigoAtendimento; }
            set { codigoAtendimento = value; }
        }
        private String prestador;

        public String Prestador
        {
            get { return prestador; }
            set { prestador = value; }
        }

        private bool prioridade;

        public bool Prioridade
        {
            get { return prioridade; }
            set { prioridade = value; }
        }

        private string senhaAtendimento;

        public string SenhaAtendimento
        {
            get { return senhaAtendimento; }
            set { senhaAtendimento = value; }
        }


        public ItemFilaAtendimento(Int64? idItem, String nome, String dataNascimento, String cpf, String funcao, String empresa, String login, 
            String tipoAtendimento, String tipoExame, Exame exame, Boolean devolvido, DateTime dataExame, String rg,
            String nomeSala, Int32? andar, Int32? peso, Decimal? altura, String periodicidade, String imc, DateTime? dataAtendido, 
            String codigoAtendimento, String prestador, bool prioridade, string senhaAtendimento)
        {
            this.IdItem = idItem;
            this.Nome = nome;
            this.DataNascimento = dataNascimento;
            this.Cpf = cpf;
            this.Funcao = funcao;
            this.Empresa = empresa;
            this.Login = login;
            this.TipoAtendimento = tipoAtendimento;
            this.TipoExame = tipoExame;
            this.Exame = exame;
            this.Devolvido = devolvido;
            this.DataExame = dataExame;
            this.Rg = rg;
            this.NomeSala = nomeSala;
            this.Andar = andar;
            this.Peso = peso;
            this.Altura = altura;
            this.Periodicidade = periodicidade;
            this.Imc = imc;
            this.DataAtendido = dataAtendido;
            this.CodigoAtendimento = codigoAtendimento;
            this.Prestador = prestador;
            this.Prioridade = prioridade;
            this.SenhaAtendimento = senhaAtendimento;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            ItemFilaAtendimento p = obj as ItemFilaAtendimento;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.idItem == p.idItem && this.TipoExame.Equals(p.TipoExame));
        }

        public override int GetHashCode()
        {
            return idItem.GetHashCode();
        }
    
    }
}
