﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Periodicidade
    {
        Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        
        String descricao;

        public String Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }

        private int? dias;

        public int? Dias
        {
            get { return dias; }
            set { dias = value; }
        }
        
        public Periodicidade() { }

        public Periodicidade(Int64? id)
        { 
            this.Id = id;
        }

        public Periodicidade(Int64? id, String descricao, int? dias)
        {
            this.Id = id;
            this.Descricao = descricao;
            this.Dias = dias;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Periodicidade p = obj as Periodicidade;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.Id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

        public int tipoPeriodicidadeEsocial()
        {
            int tipo;
            switch (id)
            {
                case 4:
                    tipo = 0;
                    break;
                case 6:
                    tipo = 2;
                    break;
                case 7:
                    tipo = 3;
                    break;
                case 5:
                    tipo = 9;
                    break;
                case 3:
                    tipo = 4;
                    break;
                case 8:
                    tipo = 4;
                    break;
                default:
                    tipo = 1;
                    break;
            }
            return tipo;
        }

    }
}
