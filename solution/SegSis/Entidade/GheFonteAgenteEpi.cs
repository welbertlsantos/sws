﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    
    public class GheFonteAgenteEpi
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private Epi epi;

        public Epi Epi
        {
            get { return epi; }
            set { epi = value; }
        }
        private GheFonteAgente gheFonteAgente;

        public GheFonteAgente GheFonteAgente
        {
            get { return gheFonteAgente; }
            set { gheFonteAgente = value; }
        }
        private String tipo;

        public String Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }
        private String classificacao;

        public String Classificacao
        {
            get { return classificacao; }
            set { classificacao = value; }
        }
        

        public GheFonteAgenteEpi() { }

        public GheFonteAgenteEpi(Int64? id, GheFonteAgente gheFonteAgente, Epi epi, String tipo, String classificacao)
        {
            this.Id = id;
            this.Epi = epi;
            this.GheFonteAgente = gheFonteAgente;
            this.Tipo = tipo;
            this.Classificacao = classificacao;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            GheFonteAgenteEpi p = obj as GheFonteAgenteEpi;
            if (p == null)
            {
                return false;
            }

            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

    }
}
