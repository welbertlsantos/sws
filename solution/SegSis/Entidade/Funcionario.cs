﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Funcionario
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }

        private String nome;

        public String Nome
        {
            get { return nome; }
            set { nome = value.Trim(); }
        }

        private String cpf;

        public String Cpf
        {
            get { return cpf; }
            set { cpf = value.Replace(".", "").Replace("-", "").Replace("/","").Trim();}
        }
        
        private String rg;

        public String Rg
        {
            get { return rg; }
            set { rg = value; }
        }
        
        private String endereco;

        public String Endereco
        {
            get { return endereco; }
            set { endereco = value; }
        }
        
        private String numero;

        public String Numero
        {
            get { return numero; }
            set { numero = value; }
        }
        
        private String complemento;

        public String Complemento
        {
            get { return complemento; }
            set { complemento = value; }
        }
        
        private String bairro;

        public String Bairro
        {
            get { return bairro; }
            set { bairro = value; }
        }
        
        private CidadeIbge cidade;

        public CidadeIbge Cidade
        {
            get { return cidade; }
            set { cidade = value; }
        }
        
        private String uf;

        public String Uf
        {
            get { return uf; }
            set { uf = value; }
        }
        
        private String telefone1;

        public String Telefone1
        {
            get { return telefone1; }
            set { telefone1 = value; }
        }
        
        private String telefone2;

        public String Telefone2
        {
            get { return telefone2; }
            set { telefone2 = value; }
        }
        
        private String email;

        public String Email
        {
            get { return email; }
            set { email = value; }
        }
        
        private String tipoSangue;

        public String TipoSangue
        {
            get { return tipoSangue; }
            set { tipoSangue = value; }
        }
        
        private String fatorRh;

        public String FatorRh
        {
            get { return fatorRh; }
            set { fatorRh = value; }
        }
        
        private DateTime dataNascimento;

        public DateTime DataNascimento
        {
            get { return dataNascimento; }
            set { dataNascimento = value; }
        }
        
        private Int32? peso;

        public Int32? Peso
        {
            get { return peso; }
            set { peso = value; }
        }
        
        private Decimal? altura;

        public Decimal? Altura
        {
            get { return altura; }
            set { altura = value; }
        }
        
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        
        private String cep;

        public String Cep
        {
            get { return cep; }
            set { cep = value.Replace("-","").Replace(".","").Trim(); }
        }
        
        private String sexo;

        public String Sexo
        {
            get { return sexo; }
            set { sexo = value; }
        }
        
        private String ctps;

        public String Ctps
        {
            get { return ctps; }
            set { ctps = value.Trim(); }
        }
        
        private String pisPasep;

        public String PisPasep
        {
            get { return pisPasep; }
            set { pisPasep = value.Replace("-", "").Replace(".", "").Replace("/", "").Trim(); }
        }

        private bool pcd;

        public bool Pcd
        {
            get { return pcd; }
            set { pcd = value; }
        }

        private String orgaoEmissor;

        public String OrgaoEmissor
        {
            get { return orgaoEmissor; }
            set { orgaoEmissor = value; }
        }

        private String ufEmissor;

        public String UfEmissor
        {
            get { return ufEmissor; }
            set { ufEmissor = value; }
        }

        private String serie;

        public String Serie
        {
            get { return serie; }
            set { serie = value; }
        }

        private string ufEmissorCtps;

        public string UfEmissorCtps
        {
            get { return ufEmissorCtps; }
            set { ufEmissorCtps = value; }
        }

        private List<ClienteFuncaoFuncionario> clienteFuncaoFuncionario;

        public List<ClienteFuncaoFuncionario> ClienteFuncaoFuncionario
        {
            get { return clienteFuncaoFuncionario; }
            set { clienteFuncaoFuncionario = value; }
        }
        
        public Funcionario() { }

        public Funcionario(long id)
        {
            this.Id = id;
        }

        public Funcionario(Int64? id, String nome, String cpf, String rg, String endereco,
            String numero, String complemento, String bairro, CidadeIbge cidade, 
            String uf, String telefone1, String telefone2, String email,
            String tipoSangue, String fatorRh, DateTime dataNascimento, Int32? peso, Decimal? altura,
            String situacao, String cep, String sexo, String ctps, String pisPasep, bool pcd, String orgaoEmissor, String ufEmissor, String serie, String ufEmissorCtps)
        {

            this.id = id;
            this.Nome = nome;
            this.Cpf = cpf;
            this.Rg = rg;
            this.Endereco = endereco;
            this.Numero = numero;
            this.Complemento = complemento;
            this.Bairro = bairro;
            this.Cidade = cidade;
            this.Uf = uf;
            this.Telefone1 = telefone1;
            this.Telefone2 = telefone2;
            this.Email = email;
            this.TipoSangue = tipoSangue;
            this.FatorRh = fatorRh;
            this.DataNascimento = dataNascimento;
            this.Peso = peso;
            this.Altura = altura;
            this.Cep = cep;
            this.Situacao = situacao;
            this.Sexo = sexo;
            this.Ctps = ctps;
            this.PisPasep = pisPasep;
            this.Pcd = pcd;
            this.OrgaoEmissor = orgaoEmissor;
            this.UfEmissor = ufEmissor;
            this.Serie = serie;
            this.UfEmissorCtps = ufEmissorCtps;
        }

        public Funcionario(String nome, String rg, DateTime dataNascimento, String cpf, String tipoSanguineo, String fatorRh)
        {
            this.Nome = nome;
            this.Rg = rg;
            this.DataNascimento = dataNascimento;
            this.Cpf = cpf;
            this.TipoSangue = tipoSanguineo;
            this.FatorRh = fatorRh;
        }
        
        public String getImc()
        {
            String retorno = String.Empty;
            
            if (this.Peso != null && this.Altura != null)
            {
                if (this.Altura > 0)
                {
                    return Convert.ToString(Math.Round((decimal)(this.Peso / (this.Altura * this.Altura)), 2));
                }
            }

            return retorno;
        }
        
        public Boolean isNullForFilter()
        {
            Boolean retorno = Convert.ToBoolean(false);

            if (String.IsNullOrWhiteSpace(this.Nome) && String.IsNullOrWhiteSpace(this.Cpf) && String.IsNullOrWhiteSpace(this.Rg) && string.IsNullOrEmpty(this.situacao))
                retorno = Convert.ToBoolean(true);

            return retorno;

        }

        public Boolean isNullForFilterAso()
        {
            Boolean retorno = Convert.ToBoolean(false);

            if (String.IsNullOrWhiteSpace(this.Cpf) && String.IsNullOrWhiteSpace(Convert.ToString(this.Rg)) &&
                String.IsNullOrEmpty(this.Nome))
            {
                retorno = Convert.ToBoolean(true);
            }

            return retorno;

        }
        
        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Funcionario p = obj as Funcionario;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
        
    }
}
