﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class DocumentoProtocolo
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private Aso aso;

        public Aso Aso
        {
            get { return aso; }
            set { aso = value; }
        }
        private Documento documento;

        public Documento Documento
        {
            get { return documento; }
            set { documento = value; }
        }
        private Protocolo protocolo;

        public Protocolo Protocolo
        {
            get { return protocolo; }
            set { protocolo = value; }
        }
        private DateTime dataGravacao;

        public DateTime DataGravacao
        {
            get { return dataGravacao; }
            set { dataGravacao = value; }
        }

        public DocumentoProtocolo() { }

        public DocumentoProtocolo(Int64? id, Aso aso, Documento documento, Protocolo protocolo, DateTime dataGravacao)
        {
            this.Id = id;
            this.Aso = aso;
            this.Documento = documento;
            this.Protocolo = protocolo;
            this.dataGravacao = dataGravacao;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            DocumentoProtocolo p = obj as DocumentoProtocolo;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

    }
}
