﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Arquivo
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private String descricao;

        public String Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }
        private String mimetype;

        public String Mimetype
        {
            get { return mimetype; }
            set { mimetype = value; }
        }
        private Int32 size;

        public Int32 Size
        {
            get { return size; }
            set { size = value; }
        }
        private byte[] conteudo;

        public byte[] Conteudo
        {
            get { return conteudo; }
            set { conteudo = value; }
        }

        public Arquivo(Int64? id) { this.id = id; }

        public Arquivo(Int64? id, String descricao, String mimetype, Int32 size, byte[] conteudo)
        {
            this.Id = id;
            this.Descricao = descricao;
            this.Mimetype = mimetype;
            this.Size = size;
            this.Conteudo = conteudo;
        }
        
        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Arquivo p = obj as Arquivo;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
    }
}
