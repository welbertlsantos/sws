﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.View.Entidade;

namespace SWS.Entidade
{
    public class FuncaoInterna
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private String descricao;

        public String Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }
        private String situacao;

        public String Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        private String privado;

        public String Privado
        {
            get { return privado; }
            set { privado = value; }
        } 

        public FuncaoInterna() { }
        
        public FuncaoInterna(Int64? id, String descricao, String situacao, String privado)
        {
            this.Id= id;
            this.Descricao = descricao;
            this.Situacao = situacao;
            this.Privado = privado;
        }

        public Boolean isNullForFilter()
        {
            Boolean retorno = Convert.ToBoolean(false);

            if (String.IsNullOrWhiteSpace(this.Descricao) && String.IsNullOrWhiteSpace(this.situacao))
            {
                retorno = Convert.ToBoolean(true);
            }

            return retorno;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            FuncaoInterna p = obj as FuncaoInterna;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

    }
}
