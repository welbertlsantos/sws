﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Entidade
{
    public class Prontuario
    {
        private Int64? id;

        public Int64? Id
        {
            get { return id; }
            set { id = value; }
        }
        private Aso aso;

        public Aso Aso
        {
            get { return aso; }
            set { aso = value; }
        }
        private Usuario usuario;

        public Usuario Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }
        private String pressao;

        public String Pressao
        {
            get { return pressao; }
            set { pressao = value; }
        }
        private String frequencia;

        public String Frequencia
        {
            get { return frequencia; }
            set { frequencia = value; }
        }
        private Decimal? peso;

        public Decimal? Peso
        {
            get { return peso; }
            set { peso = value; }
        }
        private Decimal? altura;

        public Decimal? Altura
        {
            get { return altura; }
            set { altura = value; }
        }
        private String lassegue;

        public String Lassegue
        {
            get { return lassegue; }
            set { lassegue = value; }
        }
        private Boolean bebida;

        public Boolean Bebida
        {
            get { return bebida; }
            set { bebida = value; }
        }
        private Boolean fumo;

        public Boolean Fumo
        {
            get { return fumo; }
            set { fumo = value; }
        }
        private Boolean diarreia;

        public Boolean Diarreia
        {
            get { return diarreia; }
            set { diarreia = value; }
        }
        private Boolean tosse;

        public Boolean Tosse
        {
            get { return tosse; }
            set { tosse = value; }
        }
        private Boolean afastado;

        public Boolean Afastado
        {
            get { return afastado; }
            set { afastado = value; }
        }
        private Boolean convulsao;

        public Boolean Convulsao
        {
            get { return convulsao; }
            set { convulsao = value; }
        }
        private Boolean hepatite;

        public Boolean Hepatite
        {
            get { return hepatite; }
            set { hepatite = value; }
        }
        private Boolean acidente;

        public Boolean Acidente
        {
            get { return acidente; }
            set { acidente = value; }
        }
        private Boolean tuberculose;

        public Boolean Tuberculose
        {
            get { return tuberculose; }
            set { tuberculose = value; }
        }
        private Boolean antitetanica;

        public Boolean Antitetanica
        {
            get { return antitetanica; }
            set { antitetanica = value; }
        }
        private Boolean atividadeFisica;

        public Boolean AtividadeFisica
        {
            get { return atividadeFisica; }
            set { atividadeFisica = value; }
        }
        private Boolean medoAltura;

        public Boolean MedoAltura
        {
            get { return medoAltura; }
            set { medoAltura = value; }
        }
        private Boolean doresJuntas;

        public Boolean DoresJuntas
        {
            get { return doresJuntas; }
            set { doresJuntas = value; }
        }
        private Boolean palpitacao;

        public Boolean Palpitacao
        {
            get { return palpitacao; }
            set { palpitacao = value; }
        }
        private Boolean tontura;

        public Boolean Tontura
        {
            get { return tontura; }
            set { tontura = value; }
        }
        private Boolean azia;

        public Boolean Azia
        {
            get { return azia; }
            set { azia = value; }
        }
        private Boolean desmaio;

        public Boolean Desmaio
        {
            get { return desmaio; }
            set { desmaio = value; }
        }
        private Boolean doresCabeca;

        public Boolean DoresCabeca
        {
            get { return doresCabeca; }
            set { doresCabeca = value; }
        }
        private Boolean epilepsia;

        public Boolean Epilepsia
        {
            get { return epilepsia; }
            set { epilepsia = value; }
        }
        private Boolean insonia;

        public Boolean Insonia
        {
            get { return insonia; }
            set { insonia = value; }
        }
        private Boolean problemaAudicao;

        public Boolean ProblemaAudicao
        {
            get { return problemaAudicao; }
            set { problemaAudicao = value; }
        }
        private Boolean tendinite;

        public Boolean Tendinite
        {
            get { return tendinite; }
            set { tendinite = value; }
        }
        private Boolean fratura;

        public Boolean Fratura
        {
            get { return fratura; }
            set { fratura = value; }
        }
        private Boolean alergia;

        public Boolean Alergia
        {
            get { return alergia; }
            set { alergia = value; }
        }
        private Boolean asma;

        public Boolean Asma
        {
            get { return asma; }
            set { asma = value; }
        }
        private Boolean diabete;

        public Boolean Diabete
        {
            get { return diabete; }
            set { diabete = value; }
        }
        private Boolean hernia;

        public Boolean Hernia
        {
            get { return hernia; }
            set { hernia = value; }
        }
        private Boolean manchasPele;

        public Boolean ManchasPele
        {
            get { return manchasPele; }
            set { manchasPele = value; }
        }
        private Boolean pressaoAlta;

        public Boolean PressaoAlta
        {
            get { return pressaoAlta; }
            set { pressaoAlta = value; }
        }
        private Boolean problemaVisao;

        public Boolean ProblemaVisao
        {
            get { return problemaVisao; }
            set { problemaVisao = value; }
        }
        private Boolean problemaColuna;

        public Boolean ProblemaColuna
        {
            get { return problemaColuna; }
            set { problemaColuna = value; }
        }
        private Boolean rinite;

        public Boolean Rinite
        {
            get { return rinite; }
            set { rinite = value; }
        }
        private Boolean varize;

        public Boolean Varize
        {
            get { return varize; }
            set { varize = value; }
        }
        private Boolean oculos;

        public Boolean Oculos
        {
            get { return oculos; }
            set { oculos = value; }
        }
        private String aparelhoMucosa;

        public String AparelhoMucosa
        {
            get { return aparelhoMucosa; }
            set { aparelhoMucosa = value; }
        }
        private String aparelhoMucosaObs;

        public String AparelhoMucosaObs
        {
            get { return aparelhoMucosaObs; }
            set { aparelhoMucosaObs = value; }
        }
        private String aparelhoVascular;

        public String AparelhoVascular
        {
            get { return aparelhoVascular; }
            set { aparelhoVascular = value; }
        }
        private String aparelhoVascularObs;

        public String AparelhoVascularObs
        {
            get { return aparelhoVascularObs; }
            set { aparelhoVascularObs = value; }
        }
        private String aparelhoRespiratorio;

        public String AparelhoRespiratorio
        {
            get { return aparelhoRespiratorio; }
            set { aparelhoRespiratorio = value; }
        }
        private String aparelhoRespiratorioObs;

        public String AparelhoRespiratorioObs
        {
            get { return aparelhoRespiratorioObs; }
            set { aparelhoRespiratorioObs = value; }
        }
        private String aparelhoOlhoDir;

        public String AparelhoOlhoDir
        {
            get { return aparelhoOlhoDir; }
            set { aparelhoOlhoDir = value; }
        }
        private String aparelhoOlhoDirObs;

        public String AparelhoOlhoDirObs
        {
            get { return aparelhoOlhoDirObs; }
            set { aparelhoOlhoDirObs = value; }
        }
        private String aparelhoOlhoEsq;

        public String AparelhoOlhoEsq
        {
            get { return aparelhoOlhoEsq; }
            set { aparelhoOlhoEsq = value; }
        }
        private String aparelhoOlhoEsqObs;

        public String AparelhoOlhoEsqObs
        {
            get { return aparelhoOlhoEsqObs; }
            set { aparelhoOlhoEsqObs = value; }
        }
        private String aparelhoProtese;

        public String AparelhoProtese
        {
            get { return aparelhoProtese; }
            set { aparelhoProtese = value; }
        }
        private String aparelhoProteseObs;

        public String AparelhoProteseObs
        {
            get { return aparelhoProteseObs; }
            set { aparelhoProteseObs = value; }
        }
        private String aparelhoGanglio;

        public String AparelhoGanglio
        {
            get { return aparelhoGanglio; }
            set { aparelhoGanglio = value; }
        }
        private String aparelhoGanglioObs;

        public String AparelhoGanglioObs
        {
            get { return aparelhoGanglioObs; }
            set { aparelhoGanglioObs = value; }
        }
        private String aparelhoAbdomen;

        public String AparelhoAbdomen
        {
            get { return aparelhoAbdomen; }
            set { aparelhoAbdomen = value; }
        }
        private String aparelhoAbdomenObs;

        public String AparelhoAbdomenObs
        {
            get { return aparelhoAbdomenObs; }
            set { aparelhoAbdomenObs = value; }
        }
        private String aparelhoUrina;

        public String AparelhoUrina
        {
            get { return aparelhoUrina; }
            set { aparelhoUrina = value; }
        }
        private String aparelhoUrinaObs;

        public String AparelhoUrinaObs
        {
            get { return aparelhoUrinaObs; }
            set { aparelhoUrinaObs = value; }
        }
        private String aparelhoMembro;

        public String AparelhoMembro
        {
            get { return aparelhoMembro; }
            set { aparelhoMembro = value; }
        }
        private String aparelhoMembroObs;

        public String AparelhoMembroObs
        {
            get { return aparelhoMembroObs; }
            set { aparelhoMembroObs = value; }
        }
        private String aparelhoColuna;

        public String AparelhoColuna
        {
            get { return aparelhoColuna; }
            set { aparelhoColuna = value; }
        }
        private String aparelhoColunaObs;

        public String AparelhoColunaObs
        {
            get { return aparelhoColunaObs; }
            set { aparelhoColunaObs = value; }
        }
        private String aparelhoEquilibrio;

        public String AparelhoEquilibrio
        {
            get { return aparelhoEquilibrio; }
            set { aparelhoEquilibrio = value; }
        }
        private String aparelhoEquilibrioObs;

        public String AparelhoEquilibrioObs
        {
            get { return aparelhoEquilibrioObs; }
            set { aparelhoEquilibrioObs = value; }
        }
        private String aparelhoForca;

        public String AparelhoForca
        {
            get { return aparelhoForca; }
            set { aparelhoForca = value; }
        }
        private String aparelhoForcaObs;

        public String AparelhoForcaObs
        {
            get { return aparelhoForcaObs; }
            set { aparelhoForcaObs = value; }
        }
        private String aparelhoMotor;

        public String AparelhoMotor
        {
            get { return aparelhoMotor; }
            set { aparelhoMotor = value; }
        }
        private String aparelhoMotorObs;

        public String AparelhoMotorObs
        {
            get { return aparelhoMotorObs; }
            set { aparelhoMotorObs = value; }
        }
        private Int32? parto;

        public Int32? Parto
        {
            get { return parto; }
            set { parto = value; }
        }
        private String metodo;

        public String Metodo
        {
            get { return metodo; }
            set { metodo = value; }
        }
        private String metodoObs;

        public String MetodoObs
        {
            get { return metodoObs; }
            set { metodoObs = value; }
        }
        private DateTime? dataUltMenstruacao;

        public DateTime? DataUltMenstruacao
        {
            get { return dataUltMenstruacao; }
            set { dataUltMenstruacao = value; }
        }
        private Boolean previAfastado;

        public Boolean PreviAfastado
        {
            get { return previAfastado; }
            set { previAfastado = value; }
        }
        private Boolean previRecurso;

        public Boolean PreviRecurso
        {
            get { return previRecurso; }
            set { previRecurso = value; }
        }
        private DateTime? dataRecurso;

        public DateTime? DataRecurso
        {
            get { return dataRecurso; }
            set { dataRecurso = value; }
        }
        private String causa;

        public String Causa
        {
            get { return causa; }
            set { causa = value; }
        }
        private String laudo;

        public String Laudo
        {
            get { return laudo; }
            set { laudo = value; }
        }
        private String laudoComentario;

        public String LaudoComentario
        {
            get { return laudoComentario; }
            set { laudoComentario = value; }
        }
        private String textComentarioAparelho;

        public String TextComentarioAparelho
        {
            get { return textComentarioAparelho; }
            set { textComentarioAparelho = value; }
        }

        public Prontuario() { }

        public Prontuario(Int64? id)
        {
            this.id = id;
        }

        public Prontuario(
            Int64? id,
            Usuario usuario,
            Aso aso,
            String pressao,
            String frequencia,
            Decimal? peso,
            Decimal? altura,
            String lassegue,
            Boolean bebida,
            Boolean fumo,
            Boolean diarreia,
            Boolean tosse,
            Boolean afastado,
            Boolean convulsao,
            Boolean hepatite,
            Boolean acidente,
            Boolean tuberculose,
            Boolean antitetanica,
            Boolean atividadeFisica,
            Boolean medoAltura,
            Boolean doresJuntas,
            Boolean palpitacao,
            Boolean tonturas,
            Boolean azia,
            Boolean desmaio,
            Boolean doresCabeca,
            Boolean epilepsia,
            Boolean insonia,
            Boolean problemaAudicao,
            Boolean tendinite,
            Boolean fratura,
            Boolean alergia,
            Boolean asma,
            Boolean diabete,
            Boolean hernia,
            Boolean manchasPele,
            Boolean pressaoAlta,
            Boolean problemaVisao,
            Boolean problemaColuna,
            Boolean rinite,
            Boolean varize,
            Boolean oculos,
            String aparelhoMucosa,
            String aparelhoMucosaObs,
            String aparelhoVascular,
            String aparelhoVascularObs,
            String aparelhoRespiratorio,
            String aparelhoRespiratorioObs,
            String aparelhoOlhoDir,
            String aparelhoOlhoDirObs,
            String aparelhoOlhoEsq,
            String aparelhoOlhoEsqObs,
            String aparelhoProtese,
            String aparelhoProteseObs,
            String aparelhoGanglio,
            String aparelhoGanglioObs,
            String aparelhoAbdomen,
            String aparelhoAbdomenObs,
            String aparelhoUrina,
            String aparelhoUrinaObs,
            String aparelhoMembro,
            String aparelhoMembroObs,
            String aparelhoColuna,
            String aparelhoColunaObs,
            String aparelhoEquilibrio,
            String aparelhoEquilibrioObs,
            String aparelhoForca,
            String aparelhoForcaObs,
            String aparelhoMotor,
            String aparelhoMotorObs,
            Int32? parto,
            String metodo,
            DateTime? dataUltimaMenstruacao,
            Boolean previAfastado,
            Boolean previRecurso,
            DateTime? dataRecurso,
            String causa,
            String laudo,
            String laudoComentario,
            String metodoObs,
            String textComentarioAparelho
            )
        {
            Id = id;
            Usuario = usuario;
            Aso = aso;
            Pressao = pressao;
            Frequencia = frequencia;
            Peso = peso;
            Altura = altura;
            Lassegue = lassegue;
            Bebida = bebida;
            Fumo = fumo;
            Diarreia = diarreia;
            Tosse = tosse;
            Afastado = afastado;
            Convulsao = convulsao;
            Hepatite = hepatite;
            Acidente = acidente;
            Tuberculose = tuberculose;
            Antitetanica = antitetanica;
            AtividadeFisica = atividadeFisica;
            MedoAltura = medoAltura;
            DoresJuntas = doresJuntas;
            Palpitacao = palpitacao;
            Tontura = tontura;
            Azia = azia;
            Desmaio = desmaio;
            DoresCabeca = doresCabeca;
            Epilepsia = epilepsia;
            Insonia = insonia;
            ProblemaAudicao = problemaAudicao;
            Tendinite = tendinite;
            Fratura = fratura;
            Alergia = alergia;
            Asma = asma;
            Diabete = diabete;
            Hernia = hernia;
            ManchasPele = manchasPele;
            PressaoAlta = pressaoAlta;
            ProblemaVisao = problemaVisao;
            ProblemaColuna = problemaColuna;
            Rinite = rinite;
            Varize = varize;
            Oculos = oculos;
            AparelhoMucosa = aparelhoMucosa;
            AparelhoMucosaObs = aparelhoMucosaObs;
            AparelhoVascular = aparelhoVascular;
            AparelhoVascularObs = aparelhoVascularObs;
            AparelhoRespiratorio = aparelhoRespiratorio;
            AparelhoRespiratorioObs = aparelhoRespiratorioObs;
            AparelhoOlhoDir = aparelhoOlhoDir;
            AparelhoOlhoDirObs = aparelhoOlhoDirObs;
            AparelhoOlhoEsq = aparelhoOlhoEsq;
            AparelhoOlhoEsqObs = aparelhoOlhoEsqObs;
            AparelhoProtese = aparelhoProtese;
            AparelhoProteseObs = aparelhoProteseObs;
            AparelhoGanglio = aparelhoGanglio;
            AparelhoGanglioObs = aparelhoGanglioObs;
            AparelhoAbdomen = aparelhoAbdomen;
            AparelhoAbdomenObs = aparelhoAbdomenObs;
            AparelhoUrina = aparelhoUrina;
            AparelhoUrinaObs = aparelhoUrinaObs;
            AparelhoMembro = aparelhoMembro;
            AparelhoMembroObs = aparelhoMembroObs;
            AparelhoColuna = aparelhoColuna;
            AparelhoColunaObs = aparelhoColunaObs;
            AparelhoEquilibrio = aparelhoEquilibrio;
            AparelhoEquilibrioObs = aparelhoEquilibrioObs;
            AparelhoForca = aparelhoForca;
            AparelhoForcaObs = aparelhoForcaObs;
            AparelhoMotor = aparelhoMotor;
            AparelhoMotorObs = aparelhoMotorObs;
            Parto = parto;
            Metodo = metodo;
            DataUltMenstruacao = dataUltimaMenstruacao;
            PreviAfastado = previAfastado;
            PreviRecurso = previRecurso;
            DataRecurso = dataRecurso;
            Causa = causa;
            Laudo = laudo;
            LaudoComentario = laudoComentario;
            MetodoObs = metodoObs;
            TextComentarioAparelho = textComentarioAparelho;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Prontuario p = obj as Prontuario;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.id == p.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }


    }
}
