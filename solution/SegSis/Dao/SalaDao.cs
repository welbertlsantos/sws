﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using NpgsqlTypes;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;
using SWS.View.Resources;
using System.Data;

namespace SWS.Dao
{
    class SalaDao : ISalaDao
    {

        public Sala findByDescricao(String descricao, long idEmpresa, NpgsqlConnection dbConnection)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findSalaByDescricao");

            Sala salaRetorno = null;

            StringBuilder query = new StringBuilder();
            
            try
            {
                query.Append(" SELECT ID_SALA, DESCRICAO, SITUACAO, ANDAR, VIP, ID_EMPRESA, SLUG_SALA, QTDE_CHAMADA ");
                query.Append(" FROM SEG_SALA WHERE DESCRICAO = :VALUE1 AND ID_EMPRESA = :VALUE2 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = descricao.ToUpper();

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = idEmpresa;


                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    salaRetorno = new Sala(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetInt32(3), dr.GetBoolean(4), new Empresa(dr.GetInt64(5)), dr.GetString(6), dr.GetInt32(7));
                }

                return salaRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAgenteByDescricao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
            {
                LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

                Int64 proximoId = Convert.ToInt64(0);
                try
                {
                    NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_SALA_ID_SALA_SEQ'))", dbConnection);

                    NpgsqlDataReader dr = command.ExecuteReader();

                    while (dr.Read())
                    {
                        proximoId = dr.GetInt64(0);
                    }

                    return proximoId;
                }
                catch (NpgsqlException ex)
                {
                    LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoIdAgente", ex);
                    throw new DataBaseConectionException(ex.Message);
                }
            }

        public Sala insert(Sala sala, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertSala");

            StringBuilder query = new StringBuilder();

            try
            {
                sala.Id = recuperaProximoId(dbConnection);

                query.Append(" INSERT INTO SEG_SALA (ID_SALA, DESCRICAO, SITUACAO, ANDAR, VIP, ID_EMPRESA, SLUG_SALA, QTDE_CHAMADA ) VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6, :VALUE7, :VALUE8)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = sala.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = sala.Descricao;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = sala.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = sala.Andar;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Boolean));
                command.Parameters[4].Value = sala.Vip;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Integer));
                command.Parameters[5].Value = sala.Empresa.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = sala.SlugSala;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Integer));
                command.Parameters[7].Value = sala.QtdeChamada;
                
                
                command.ExecuteNonQuery();

                return sala;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertSala", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public DataSet findByFilter(Sala sala, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findSalaByFilter");
            
            try
            {
                StringBuilder query = new StringBuilder();

                if (sala.isNullForFilter())
                    query.Append(" SELECT ID_SALA, DESCRICAO, SITUACAO, ANDAR, FORMATA_VIP(VIP), SLUG_SALA FROM SEG_SALA ");
                else
                {
                    query.Append(" SELECT ID_SALA, DESCRICAO, SITUACAO, ANDAR, FORMATA_VIP(VIP), SLUG_SALA FROM SEG_SALA WHERE TRUE ");

                    if (!String.IsNullOrWhiteSpace(sala.Descricao))
                        query.Append(" AND DESCRICAO LIKE :VALUE1 ");

                    if (!String.IsNullOrWhiteSpace(sala.Situacao))
                        query.Append(" AND SITUACAO = :VALUE2 ");

                    query.Append(" AND ID_EMPRESA = :VALUE3 ");
                }
            
                query.Append(" ORDER BY DESCRICAO ASC");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = sala.Descricao + "%";
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = sala.Situacao;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[2].Value = sala.Empresa.Id;
                
                DataSet ds = new DataSet();
                adapter.Fill(ds, "Salas");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findSalaByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Sala findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findSalaById");

            Sala salaRetorno = null;
            
            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_SALA, DESCRICAO, SITUACAO, ANDAR, VIP, ID_EMPRESA, SLUG_SALA, QTDE_CHAMADA ");
                query.Append(" FROM SEG_SALA ");
                query.Append(" WHERE ID_SALA = :VALUE1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    salaRetorno = new Sala(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetInt32(3), dr.GetBoolean(4), new Empresa(dr.GetInt64(5)), dr.GetString(6), dr.GetInt32(7));
                }

                return salaRetorno;
            }
            catch (NpgsqlException ex)
            { 
                LogHelper.logger.Error(this.GetType().Name + " - findSalaById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Sala update(Sala sala, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateSala");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" UPDATE SEG_SALA SET DESCRICAO = :VALUE2, ANDAR = :VALUE3, VIP = :VALUE4, SITUACAO = :VALUE5, SLUG_SALA = :VALUE6, QTDE_CHAMADA = :VALUE7 WHERE ID_SALA = :VALUE1 ", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = sala.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = sala.Descricao;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = sala.Andar;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Boolean));
                command.Parameters[3].Value = sala.Vip;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = sala.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = sala.SlugSala;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Integer));
                command.Parameters[6].Value = sala.QtdeChamada;


                command.ExecuteNonQuery();

                return sala;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateSala", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<Sala> findallSalaByEmpresa(Empresa empresa, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findallSalaByEmpresa");

            List<Sala> salas = new List<Sala>();

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_SALA, DESCRICAO, SITUACAO, ANDAR, VIP, ID_EMPRESA, SLUG_SALA, QTDE_CHAMADA ");
                query.Append(" FROM SEG_SALA ");
                query.Append(" WHERE ID_EMPRESA = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = empresa.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    salas.Add(new Sala(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetInt32(3), dr.GetBoolean(4), new Empresa(dr.GetInt64(5)), dr.GetString(6), dr.GetInt32(7)));
                }

                return salas;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findallSalaByEmpresa", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

    }
}

