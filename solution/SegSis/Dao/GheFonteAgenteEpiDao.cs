﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using Npgsql;
using NpgsqlTypes;
using SWS.Excecao;
using SWS.Helper;
using System.Data;


namespace SWS.Dao
{
    class GheFonteAgenteEpiDao : IGheFonteAgenteEpiDao
    {
        public GheFonteAgenteEpi insertGheFonteAgenteEpi(GheFonteAgenteEpi gheFonteAgenteEpi, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertGheFonteAgenteEpi");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" insert into seg_ghe_fonte_agente_epi (id_ghe_fonte_agente_epi, id_ghe_fonte_agente, id_epi, tipo_epi, classificacao ) ");
                query.Append(" values (nextval('seg_ghe_fonte_agente_epi_id_ghe_fonte_agente_epi_seq'), :value1, :value2, :value3, :value4) "); 

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value2", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value3", NpgsqlDbType.Varchar));
                command.Parameters.Add(new NpgsqlParameter("value4", NpgsqlDbType.Varchar));

                command.Parameters[0].Value = gheFonteAgenteEpi.GheFonteAgente.Id;
                command.Parameters[1].Value = gheFonteAgenteEpi.Epi.Id;
                command.Parameters[2].Value = gheFonteAgenteEpi.Tipo;
                command.Parameters[3].Value = gheFonteAgenteEpi.Classificacao;
                
                command.ExecuteNonQuery();
                return gheFonteAgenteEpi;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertGheFonteAgenteEpi", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public GheFonteAgenteEpi findGheFonteAgenteEpiByGheFonteAgenteEpi(GheFonteAgenteEpi gheFonteAgenteEpi, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGheFonteAgenteEpiByGheFonteAgenteEpi");

            try
            {
                GheFonteAgenteEpi gheFonteAgenteEpiRetorno = null;
                
                StringBuilder query = new StringBuilder();

                query.Append(" select id_ghe_fonte_agente_epi, id_ghe_fonte_agente, id_epi, tipo_epi, classificacao ");
                query.Append(" from seg_ghe_fonte_agente_epi where ");
                query.Append(" id_epi = :value1 and id_ghe_fonte_agente = :value2 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value2", NpgsqlDbType.Integer));

                command.Parameters[0].Value = gheFonteAgenteEpi.Epi.Id;
                command.Parameters[1].Value = gheFonteAgenteEpi.GheFonteAgente.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gheFonteAgenteEpiRetorno = new GheFonteAgenteEpi(dr.GetInt64(0), gheFonteAgenteEpi.GheFonteAgente, 
                        gheFonteAgenteEpi.Epi, dr.GetString(3), dr.GetString(4));
                }

                return gheFonteAgenteEpiRetorno;
                
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheFonteAgenteEpiByGheFonteAgenteEpi", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void excluiGheFonteAgenteEpi(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método excluiGheFonteAgenteEpi");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" delete from seg_ghe_fonte_agente_epi where id_ghe_fonte_agente_epi = :value1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = id;
                
                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - excluiGheFonteAgenteEpi", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public HashSet<GheFonteAgenteEpi> findAllGheFonteAgenteEpi(GheFonteAgente gheFonteAgente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllGheFonteAgenteEpi");

            try
            {
                GheFonteAgenteEpi gheFonteAgenteEpiRetorno = null;
                HashSet<GheFonteAgenteEpi> gheFonteAgenteEpiSetRetorno = new HashSet<GheFonteAgenteEpi>();
                
                StringBuilder query = new StringBuilder();

                query.Append(" select a.id_ghe_fonte_agente_epi, a.id_ghe_fonte_agente, a.id_epi, a.tipo_epi, b.descricao, b.finalidade,");
                query.Append(" b.ca, b.situacao, a.classificacao from seg_ghe_fonte_agente_epi a, seg_epi b where ");
                query.Append(" a.id_epi = b.id_epi and a.id_ghe_fonte_agente = :value1 ");
                query.Append(" order by b.descricao ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = gheFonteAgente.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    Epi epi = new Epi(dr.GetInt64(2), dr.GetString(4), dr.GetString(5), dr.GetString(6),dr.GetString(7));
                    gheFonteAgenteEpiRetorno = new GheFonteAgenteEpi(dr.GetInt64(0), gheFonteAgente, epi, dr.GetString(3), dr.GetString(8));
                    gheFonteAgenteEpiSetRetorno.Add(gheFonteAgenteEpiRetorno);
                }

                return gheFonteAgenteEpiSetRetorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllGheFonteAgenteEpi", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean findGheFonteAgenteEpi(GheFonteAgenteEpi gheFonteAgenteEpi, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGheFonteAgenteEpi");

            try
            {
                Boolean retorno = Convert.ToBoolean(false);
                
                StringBuilder query = new StringBuilder();

                query.Append(" select id_ghe_fonte_agente_epi, id_ghe_fonte_agente, id_epi, tipo_epi, classificacao ");
                query.Append(" from seg_ghe_fonte_agente_epi where ");
                query.Append(" id_epi = :value1 and id_ghe_fonte_agente = :value2 and classificacao = :value3 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value2", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value3", NpgsqlDbType.Varchar));

                command.Parameters[0].Value = gheFonteAgenteEpi.Epi.Id;
                command.Parameters[1].Value = gheFonteAgenteEpi.GheFonteAgente.Id;
                command.Parameters[2].Value = gheFonteAgenteEpi.Classificacao;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    retorno = Convert.ToBoolean(true);
                      
                }

                return retorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheFonteAgenteEpi", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean findGheFonteAgenteEpiByEpi(Epi epi, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGheFonteAgenteEpiByEpi");

            Boolean isGheFonteAgenteEpiUsed = false;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" select count(*) from seg_ghe_fonte_agente_epi gfae ");
                query.Append(" where gfae.id_epi = :value1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = epi.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    if (dr.GetInt64(0) > 0)
                    {
                        isGheFonteAgenteEpiUsed = true;
                    }
                }

                return isGheFonteAgenteEpiUsed;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheFonteAgenteEpiByEpi", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public DataSet findAllGheFonteAgenteEpiByGHeFonteAgente(GheFonteAgente gheFonteAgente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllGheFonteAgenteEpiByGHeFonteAgente");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" select a.id_ghe_fonte_agente_epi, a.id_ghe_fonte_agente, a.id_epi, b.descricao, (CASE WHEN a.tipo_epi = '' THEN '' WHEN a.tipo_epi = 'O' THEN 'OBRIGATÓRIO' ELSE 'EVENTUAL' END) as tipo, b.finalidade,");
                query.Append(" b.ca, b.situacao, (CASE WHEN a.classificacao = 'MC' THEN 'MEDIDA DE CONTROLE' WHEN a.classificacao = 'EPI' Then 'PROTEÇÃO INDIVIDUAL' ELSE 'PROTEÇÃO COLETIVA' END ) as Classificacao from seg_ghe_fonte_agente_epi a, seg_epi b where ");
                query.Append(" a.id_epi = b.id_epi and a.id_ghe_fonte_agente = :value1 ");
                query.Append(" order by b.descricao ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                adapter.SelectCommand.Parameters[0].Value = gheFonteAgente.Id;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Epis");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllGheFonteAgenteEpiByGHeFonteAgente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }



    }
}
