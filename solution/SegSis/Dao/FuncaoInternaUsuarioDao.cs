﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using Npgsql;
using NpgsqlTypes;
using SWS.View.Resources;
using SWS.Excecao;
using SWS.Helper;

namespace SWS.Dao
{
    class FuncaoInternaUsuarioDao : IFuncaoInternaUsuarioDao
    {
        public void insert(Usuario usuario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertFuncaoInterna");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" INSERT INTO SEG_USUARIO_FUNCAO_INTERNA ");
                query.Append(" (ID_USUARIO_FUNCAO_INTERNA, ID_FUNCAO_INTERNA, ID_USUARIO, SITUACAO, DT_INICIO) ");
                query.Append(" VALUES (NEXTVAL('SEG_USUARIO_FUNCAO_INTERNA_ID_USUARIO_FUNCAO_INTERNA_SEQ'), ");
                query.Append(" :VALUE1, :VALUE2, 'A', NOW())");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = usuario.FuncaoInterna.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = usuario.Id;
                
                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertFuncaoInterna", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void changeStatus(Usuario usuario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método changeStatus");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_USUARIO_FUNCAO_INTERNA SET SITUACAO = 'I', ");
                query.Append(" DT_TERMINO = NOW() WHERE ID_FUNCAO_INTERNA = :VALUE1 AND ID_USUARIO = :VALUE2");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = usuario.FuncaoInterna.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = usuario.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - changeStatus", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
