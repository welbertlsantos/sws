﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using NpgsqlTypes;
using Npgsql;
using SWS.Entidade;
using SWS.Excecao;
using SWS.Helper;
using SWS.View.ViewHelper;
using System.Data;

namespace SWS.Dao
{
    class ContratoExameDao : IContratoExameDao
    {
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_CONTRATO_EXAME_ID_CONTRATO_EXAME_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void insert(ContratoExame contratoExame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            StringBuilder query = new StringBuilder();

            try
            {
                contratoExame.Id = recuperaProximoId(dbConnection);

                query.Append(" INSERT INTO SEG_CONTRATO_EXAME (ID_CONTRATO_EXAME, ID_EXAME, ID_CONTRATO, ");
                query.Append(" PRECO_CONTRATO, USUARIO, CUSTO_CONTRATO, DATA_INCLUSAO, SITUACAO, ALTERA ) VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6, :VALUE7, 'A', :VALUE8)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = contratoExame.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = contratoExame.Exame.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = contratoExame.Contrato.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Numeric));
                command.Parameters[3].Value = contratoExame.PrecoContratado;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = contratoExame.Usuario.Login;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Numeric));
                command.Parameters[5].Value = contratoExame.CustoContrato;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Timestamp));
                command.Parameters[6].Value = contratoExame.DataInclusao;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Boolean));
                command.Parameters[7].Value = contratoExame.Altera;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public HashSet<ContratoExame> findAtivosByContrato(Contrato contrato, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtivosByContrato");

            try
            {
                StringBuilder query = new StringBuilder();

                HashSet<ContratoExame> colecaoExame = new HashSet<ContratoExame>();

                query.Append(" SELECT CONTRATO_EXAME.ID_CONTRATO_EXAME, CONTRATO_EXAME.ID_EXAME, CONTRATO_EXAME.ID_CONTRATO, CONTRATO_EXAME.PRECO_CONTRATO, CONTRATO_EXAME.USUARIO, CONTRATO_EXAME.CUSTO_CONTRATO, ");
                query.Append(" EXAME.ID_EXAME, EXAME.DESCRICAO, EXAME.SITUACAO, EXAME.LABORATORIO, EXAME.PRECO, EXAME.PRIORIDADE, EXAME.CUSTO, EXAME.EXTERNO, EXAME.LIBERA_DOCUMENTO, CONTRATO_EXAME.DATA_INCLUSAO, CONTRATO_EXAME.SITUACAO, CONTRATO_EXAME.ALTERA, EXAME.CODIGO_TUSS, EXAME.EXAME_COMPLEMENTAR, EXAME.PERIODO_VENCIMENTO, EXAME.PADRAO_CONTRATO ");
                query.Append(" FROM SEG_CONTRATO_EXAME CONTRATO_EXAME ");
                query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = CONTRATO_EXAME.ID_EXAME ");
                query.Append(" WHERE CONTRATO_EXAME.ID_CONTRATO = :VALUE1 AND CONTRATO_EXAME.SITUACAO = 'A' ");
                query.Append(" ORDER BY EXAME.DESCRICAO ASC ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = contrato.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    colecaoExame.Add(new ContratoExame(dr.GetInt64(0) , new Exame(dr.GetInt64(6), dr.GetString(7), dr.GetString(8), dr.GetBoolean(9), dr.GetDecimal(10), dr.GetInt32(11),dr.GetDecimal(12), dr.GetBoolean(13), dr.GetBoolean(14), dr.IsDBNull(18) ? string.Empty : dr.GetString(18), dr.GetBoolean(19), dr.IsDBNull(20) ? (int?)null : dr.GetInt32(20), dr.GetBoolean(21)), contrato, dr.GetDecimal(3), contrato.UsuarioCriador, dr.GetDecimal(5), dr.IsDBNull(15) ? null : (DateTime?)dr.GetDateTime(15), dr.IsDBNull(16) ? String.Empty : dr.GetString(16), dr.GetBoolean(17)));
                }

                return colecaoExame;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtivosByContrato", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public void delete(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" DELETE FROM SEG_CONTRATO_EXAME WHERE ID_CONTRATO_EXAME = :VALUE1", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;
                
                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void update(ContratoExame contratoExame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_CONTRATO_EXAME SET PRECO_CONTRATO = :VALUE1, ");
                query.Append(" CUSTO_CONTRATO = :VALUE4, ALTERA = :VALUE6 AND SITUACAO = :VALUE5 WHERE ID_CONTRATO = :VALUE2 AND ID_EXAME = :VALUE3");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Numeric));
                command.Parameters[0].Value = contratoExame.PrecoContratado;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = contratoExame.Contrato.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = contratoExame.Exame.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Numeric));
                command.Parameters[3].Value = contratoExame.CustoContrato;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = contratoExame.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Boolean));
                command.Parameters[5].Value = contratoExame.Altera;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public ContratoExame findAllByContratoAndExameAndSituacao(Contrato contrato, Exame exame, String str, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByContratoAndExameAndSituacao");

            ContratoExame contratoExame = null;

            StringBuilder query = new StringBuilder();

            try
            {

                query.Append(" SELECT CONTRATO_EXAME.ID_CONTRATO_EXAME, CONTRATO_EXAME.PRECO_CONTRATO, USUARIO.ID_USUARIO, USUARIO.LOGIN, CONTRATO_EXAME.CUSTO_CONTRATO, CONTRATO_EXAME.DATA_INCLUSAO, CONTRATO_EXAME.SITUACAO, CONTRATO_EXAME.ALTERA ");
                query.Append(" FROM SEG_CONTRATO_EXAME CONTRATO_EXAME ");
                query.Append(" JOIN SEG_USUARIO USUARIO ON USUARIO.LOGIN = CONTRATO_EXAME.USUARIO ");
                query.Append(" WHERE  ");
                query.Append(" CONTRATO_EXAME.ID_CONTRATO = :VALUE1 AND CONTRATO_EXAME.ID_EXAME = :VALUE2 ");
                
                if (!string.IsNullOrEmpty(str))
                    query.Append(" AND CONTRATO_EXAME.SITUACAO = :VALUE3 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = contrato.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = exame.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = str;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    contratoExame = new ContratoExame(dr.GetInt64(0), exame, contrato, dr.GetDecimal(1), new Usuario(dr.GetInt64(2)), dr.GetDecimal(4), dr.IsDBNull(5) ? null : (DateTime?)dr.GetDateTime(5), dr.IsDBNull(6) ? String.Empty : dr.GetString(6), dr.GetBoolean(7));
                }

                return contratoExame;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByContratoAndExameAndSituacao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAtivosByContratoDataSet(Contrato contrato, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtivosByContratoDataSet");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT CONTRATO_EXAME.ID_CONTRATO_EXAME, CONTRATO_EXAME.ID_EXAME, EXAME.DESCRICAO, CONTRATO_EXAME.PRECO_CONTRATO, CONTRATO_EXAME.CUSTO_CONTRATO, CONTRATO_EXAME.DATA_INCLUSAO, CONTRATO_EXAME.SITUACAO, CONTRATO_EXAME.ALTERA, EXAME.SITUACAO AS SITUACAO_EXAME, EXAME.LABORATORIO, EXAME.PRIORIDADE, EXAME.EXTERNO, EXAME.LIBERA_DOCUMENTO, CAST((CASE WHEN CUSTO_CONTRATO = 0 THEN 0 ELSE (((PRECO_CONTRATO / CUSTO_CONTRATO)-1) * 100) END) AS NUMERIC (8,2)) AS LUCRO, EXAME.PADRAO_CONTRATO ");
                query.Append(" FROM SEG_CONTRATO_EXAME CONTRATO_EXAME ");
                query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = CONTRATO_EXAME.ID_EXAME ");
                query.Append(" WHERE CONTRATO_EXAME.ID_CONTRATO = :VALUE1 AND CONTRATO_EXAME.SITUACAO = 'A' ");
                query.Append(" ORDER BY DESCRICAO ASC ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = contrato.Id;
                
                DataSet ds = new DataSet();

                adapter.Fill(ds, "Exames");

                return ds;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtivosByContratoDataSet", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void reativated(ContratoExame contratoExame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método reativated");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" UPDATE SEG_CONTRATO_EXAME SET SITUACAO = 'A' WHERE ID_CONTRATO_EXAME = :VALUE1 ", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = contratoExame.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - reativated", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void desativated(ContratoExame contratoExame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método desativated");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" UPDATE SEG_CONTRATO_EXAME SET SITUACAO = 'I' WHERE ID_CONTRATO_EXAME = :VALUE1 ", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = contratoExame.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - desativated", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public ContratoExame findById(long id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            try
            {
                StringBuilder query = new StringBuilder();
                ContratoExame contratoExame = null;

                query.Append(" SELECT CONTRATO_EXAME.ID_CONTRATO_EXAME, CONTRATO_EXAME.ID_EXAME, CONTRATO_EXAME.ID_CONTRATO, CONTRATO_EXAME.PRECO_CONTRATO, CONTRATO_EXAME.USUARIO, CONTRATO_EXAME.CUSTO_CONTRATO, ");
                query.Append(" EXAME.ID_EXAME, EXAME.DESCRICAO, EXAME.SITUACAO, EXAME.LABORATORIO, EXAME.PRECO, EXAME.PRIORIDADE, EXAME.CUSTO, EXAME.EXTERNO, EXAME.LIBERA_DOCUMENTO, CONTRATO_EXAME.DATA_INCLUSAO, CONTRATO_EXAME.SITUACAO, CONTRATO_EXAME.ALTERA, EXAME.CODIGO_TUSS, EXAME.EXAME_COMPLEMENTAR, EXAME.PERIODO_VENCIMENTO, EXAME.PADRAO_CONTRATO ");
                query.Append(" FROM SEG_CONTRATO_EXAME CONTRATO_EXAME ");
                query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = CONTRATO_EXAME.ID_EXAME ");
                query.Append(" WHERE CONTRATO_EXAME.ID_CONTRATO_EXAME = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();
                

                while (dr.Read())
                {
                    contratoExame = new ContratoExame();
                    contratoExame.Id = dr.GetInt64(0);
                    contratoExame.Contrato = new Contrato(dr.GetInt64(2));
                    contratoExame.PrecoContratado = dr.IsDBNull(3) ? Convert.ToDecimal(0) : dr.GetDecimal(3);
                    contratoExame.Usuario = new Usuario(dr.GetString(4), string.Empty);
                    contratoExame.CustoContrato = dr.IsDBNull(5) ? Convert.ToDecimal(0) : dr.GetDecimal(5);
                    contratoExame.Exame = new Exame(dr.GetInt64(6), dr.GetString(7), dr.GetString(8), dr.GetBoolean(9), dr.GetDecimal(10), dr.GetInt32(11), dr.GetDecimal(12), dr.GetBoolean(13), dr.GetBoolean(14), dr.IsDBNull(18) ? string.Empty : dr.GetString(18), dr.GetBoolean(19), dr.IsDBNull(20) ? (int?)null : dr.GetInt32(20), dr.GetBoolean(21));
                    contratoExame.DataInclusao = dr.IsDBNull(15) ? (DateTime?)null : dr.GetDateTime(15);
                    contratoExame.Situacao = dr.IsDBNull(16) ? string.Empty : dr.GetString(16);
                    contratoExame.Altera = dr.GetBoolean(17);
                }

                return contratoExame;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtivosByContrato", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
