﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using NpgsqlTypes;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;
using System.Data;
using SWS.View.Resources;

namespace SWS.Dao
{
    class ContratoDao :IContratoDao
    {
        public Contrato findContratoSistemaByCliente(Cliente cliente, NpgsqlConnection dbConnection)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findContratoSistemaByCliente");

            Contrato contratoRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_CONTRATO, CODIGO_CONTRATO, ID_CLIENTE, DATA_ELABORACAO, DATA_FIM, DATA_INICIO, ");
                query.Append(" SITUACAO, OBSERVACAO, ID_USUARIO_CRIOU, ID_USUARIO_FINALIZOU, ID_USUARIO_CANCELOU, MOTIVO_CANCELAMENTO, "); 
                query.Append(" ID_CLIENTE_PRESTADOR, CONTRATO_AUTOMATICO, DATA_CANCELAMENTO, ID_CONTRATO_PAI, BLOQUEIA_INADIMPLENCIA, ");
                query.Append(" GERA_MENSALIDADE, GERA_NUMERO_VIDAS, DATA_COBRANCA, COBRANCA_AUTOMATICA, VALOR_MENSALIDADE, ");
                query.Append(" VALOR_NUMERO_VIDAS, DIAS_BLOQUEIO, ID_CONTRATO_RAIZ, ID_CLIENTE_PROPOSTA, PROPOSTA, PROPOSTA_FORMA_PAGAMENTO, ID_USUARIO_ENCERROU, DATA_ENCERRAMENTO, MOTIVO_ENCERRAMENTO, ID_EMPRESA "); 
                query.Append(" FROM SEG_CONTRATO WHERE ID_CLIENTE = :VALUE1 AND ID_CLIENTE_PRESTADOR IS NULL AND SITUACAO = 'F' AND CONTRATO_AUTOMATICO = TRUE");
                
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    contratoRetorno = new Contrato(dr.GetInt64(0), dr.IsDBNull(1) ? String.Empty : dr.GetString(1), cliente, dr.IsDBNull(8) ? null : new Usuario(dr.GetInt64(8)), dr.IsDBNull(3) ? (DateTime?)null : dr.GetDateTime(3), dr.IsDBNull(4) ? (DateTime?)null : dr.GetDateTime(4), dr.IsDBNull(5) ? (DateTime?)null : dr.GetDateTime(5), dr.IsDBNull(6) ? String.Empty : dr.GetString(6), dr.IsDBNull(7) ? String.Empty : dr.GetString(7), dr.IsDBNull(9) ? null : new Usuario(dr.GetInt64(9)), dr.IsDBNull(10) ? null : new Usuario(dr.GetInt64(10)), dr.IsDBNull(11) ? String.Empty : dr.GetString(11), dr.IsDBNull(12) ? null : new Cliente(dr.GetInt64(12)), dr.GetBoolean(13), dr.IsDBNull(14) ? (DateTime?)null : dr.GetDateTime(14), dr.IsDBNull(15) ? null : findById(dr.GetInt64(15), dbConnection), dr.GetBoolean(16), dr.GetBoolean(17), dr.GetBoolean(18), dr.IsDBNull(19) ? (int?)null : dr.GetInt32(19), dr.GetBoolean(20), dr.IsDBNull(21) ? null : (Decimal?)dr.GetDecimal(21), dr.IsDBNull(22) ? null : (Decimal?)dr.GetDecimal(22), dr.IsDBNull(23) ? (int?)null : dr.GetInt32(23), dr.IsDBNull(24) ? null : findById(dr.GetInt64(24), dbConnection), null,  dr.GetBoolean(26), dr.IsDBNull(27) ? String.Empty : dr.GetString(27), dr.IsDBNull(28)? null : new Usuario(dr.GetInt64(28)), dr.IsDBNull(29)? (DateTime?)null : dr.GetDateTime(29), dr.IsDBNull(30) ? String.Empty : dr.GetString(30), dr.IsDBNull(31) ? null : new Empresa(dr.GetInt64(31)));                            
                }

                return contratoRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findContratoSistemaByCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public Contrato insert(Contrato contrato, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            StringBuilder query = new StringBuilder();

            try
            {
                contrato.Id = recuperaProximoId(dbConnection);

                contrato.CodigoContrato = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + Convert.ToString(contrato.Id).PadLeft(6, '0') + "00";

                query.Append(" INSERT INTO SEG_CONTRATO (ID_CONTRATO, CODIGO_CONTRATO, ID_CLIENTE, DATA_ELABORACAO, ");
                query.Append(" SITUACAO, OBSERVACAO, ID_USUARIO_CRIOU, ID_CLIENTE_PRESTADOR, CONTRATO_AUTOMATICO, ");
                query.Append(" BLOQUEIA_INADIMPLENCIA, GERA_MENSALIDADE, GERA_NUMERO_VIDAS, DATA_COBRANCA, COBRANCA_AUTOMATICA, ");
                query.Append(" VALOR_MENSALIDADE, VALOR_NUMERO_VIDAS, DIAS_BLOQUEIO, PROPOSTA, ID_CLIENTE_PROPOSTA, DATA_FIM, PROPOSTA_FORMA_PAGAMENTO, DATA_INICIO, ID_EMPRESA) VALUES ");
                query.Append(" (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6, :VALUE7, :VALUE8, :VALUE9, ");
                query.Append(" :VALUE10, :VALUE11, :VALUE12, :VALUE13, :VALUE14, :VALUE15, :VALUE16, :VALUE17, :VALUE18, :VALUE19, :VALUE20, :VALUE21, :VALUE22, :VALUE23 )");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = contrato.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = String.IsNullOrEmpty(contrato.CodigoContrato) ? String.Empty : contrato.CodigoContrato;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = contrato.Cliente != null ? contrato.Cliente.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Timestamp));
                command.Parameters[3].Value = contrato.DataElaboracao != null ? contrato.DataElaboracao : (DateTime?)null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = String.IsNullOrEmpty(contrato.Situacao) ? String.Empty : contrato.Situacao;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Text));
                command.Parameters[5].Value = String.IsNullOrEmpty(contrato.Observacao) ? String.Empty : contrato.Observacao;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Integer));
                command.Parameters[6].Value = contrato.UsuarioCriador != null ? contrato.UsuarioCriador.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Integer));
                command.Parameters[7].Value = contrato.Prestador != null ? contrato.Prestador.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Boolean));
                command.Parameters[8].Value = contrato.ContratoAutomatico;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Boolean));
                command.Parameters[9].Value = contrato.BloqueiaInadimplencia;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Boolean));
                command.Parameters[10].Value = contrato.GeraMensalidade;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Boolean));
                command.Parameters[11].Value = contrato.GeraNumeroVidas;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Integer));
                command.Parameters[12].Value = contrato.DataCobranca;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Boolean));
                command.Parameters[13].Value = contrato.CobrancaAutomatica;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Numeric));
                command.Parameters[14].Value = contrato.ValorMensalidade == null ? 0 : contrato.ValorMensalidade;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Numeric));
                command.Parameters[15].Value = contrato.ValorNumeroVidas == null ? 0 : contrato.ValorNumeroVidas;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE17", NpgsqlDbType.Integer));
                command.Parameters[16].Value = contrato.DiasInadimplenciaBloqueio;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE18", NpgsqlDbType.Boolean));
                command.Parameters[17].Value = contrato.Proposta;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE19", NpgsqlDbType.Integer));
                command.Parameters[18].Value = contrato.ClienteProposta == null ? null : contrato.ClienteProposta.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE20", NpgsqlDbType.Timestamp));
                command.Parameters[19].Value = contrato.DataFim != null ? contrato.DataFim : (DateTime?)null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE21", NpgsqlDbType.Varchar));
                command.Parameters[20].Value = !String.IsNullOrEmpty(contrato.PropostaFormaPagamento) ? contrato.PropostaFormaPagamento : String.Empty;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE22", NpgsqlDbType.Timestamp));
                command.Parameters[21].Value = contrato.DataInicio;

                command.Parameters.Add(new NpgsqlParameter("VALUE23", NpgsqlDbType.Integer));
                command.Parameters[22].Value = contrato.Empresa.Id;
                                
                command.ExecuteNonQuery();


                return contrato;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_CONTRATO_ID_CONTRATO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public Contrato insertContratoPrestador(Contrato contrato, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertContratoPrestador");

            StringBuilder query = new StringBuilder();

            Contrato contratoPrestador = new Contrato();

            try
            {
                contratoPrestador.Id = recuperaProximoId(dbConnection);
                contratoPrestador.CodigoContrato = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + Convert.ToString(contratoPrestador.Id).PadLeft(6, '0') + "00";

                query.Append(" INSERT INTO SEG_CONTRATO (ID_CONTRATO, CODIGO_CONTRATO, ID_CLIENTE, DATA_ELABORACAO, DATA_FIM, DATA_INICIO, SITUACAO, OBSERVACAO, ID_USUARIO_CRIOU, CONTRATO_AUTOMATICO, BLOQUEIA_INADIMPLENCIA, GERA_MENSALIDADE, ID_CLIENTE_PRESTADOR, GERA_NUMERO_VIDAS, DATA_COBRANCA, COBRANCA_AUTOMATICA, VALOR_MENSALIDADE, VALOR_NUMERO_VIDAS, DIAS_BLOQUEIO, PROPOSTA, ID_EMPRESA) ");
                query.Append(" VALUES ( :VALUE1, :VALUE2, :VALUE3, NOW(), :VALUE4, :VALUE5, 'G', :VALUE6, :VALUE7, FALSE, :VALUE8, :VALUE9, :VALUE10, :VALUE11, :VALUE12, :VALUE13, :VALUE14, :VALUE15, :VALUE16, FALSE, :VALUE17 ) ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = contratoPrestador.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = contratoPrestador.CodigoContrato;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = contrato.Cliente.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Timestamp));
                command.Parameters[3].Value = contrato.DataFim == null ? (DateTime?)null : contrato.DataFim;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Timestamp));
                command.Parameters[4].Value = contrato.DataInicio;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Text));
                command.Parameters[5].Value = contrato.Observacao.ToUpper();
                
                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Integer));
                command.Parameters[6].Value = contrato.UsuarioCriador.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Boolean));
                command.Parameters[7].Value = contrato.BloqueiaInadimplencia;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Boolean));
                command.Parameters[8].Value = contrato.GeraMensalidade;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Integer));
                command.Parameters[9].Value = contrato.Prestador.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Boolean));
                command.Parameters[10].Value = contrato.GeraNumeroVidas;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Integer));
                command.Parameters[11].Value = contrato.DataCobranca;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Boolean));
                command.Parameters[12].Value = contrato.CobrancaAutomatica;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Numeric));
                command.Parameters[13].Value = contrato.ValorMensalidade;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Numeric));
                command.Parameters[14].Value = contrato.ValorNumeroVidas;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Integer));
                command.Parameters[15].Value = contrato.DiasInadimplenciaBloqueio;

                command.Parameters.Add(new NpgsqlParameter("VALUE17", NpgsqlDbType.Integer));
                command.Parameters[16].Value = contrato.Empresa.Id;
                
                command.ExecuteNonQuery();

                return contratoPrestador;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertContratoPrestador", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<Contrato> findAllByCliente(Cliente cliente, ClienteProposta clienteProposta, NpgsqlConnection dbConnection)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllContratoByCliente");

            List<Contrato> contratos = new List<Contrato>();

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT CONTRATO.ID_CONTRATO, CONTRATO.ID_CLIENTE, CONTRATO.ID_USUARIO_CRIOU, CONTRATO.ID_CLIENTE_PRESTADOR, ");
                query.Append(" C.RAZAO_SOCIAL, C.FANTASIA, C.CNPJ, C.INSCRICAO, C.ENDERECO,  ");
                query.Append(" C.NUMERO, C.COMPLEMENTO, C.BAIRRO, C.CEP, C.UF, C.TELEFONE1, C.TELEFONE2, C.EMAIL,  ");
                query.Append(" C.SITE, C.DATA_CADASTRO, C.RESPONSAVEL, C.CARGO, C.DOCUMENTO, C.ESCOPO, C.JORNADA, C.SITUACAO,  ");
                query.Append(" C.EST_MASC, C.EST_FEM, C.ENDERECOCOB, C.NUMEROCOB, C.COMPLEMENTOCOB, C.BAIRROCOB, C.CEPCOB, ");
                query.Append(" C.UFCOB, C.TELEFONECOB, C.ID_MEDICO, C.TELEFONE2COB, C.VIP, C.TELEFONE_CONTATO, C.USA_CONTRATO, C.ID_CLIENTE_MATRIZ,  ");
                query.Append(" C.ID_CLIENTE_CREDENCIADO, C.PARTICULAR, C.UNIDADE, C.CREDENCIADA, C.PRESTADOR, C.ID_CIDADE_IBGE, CIDADE.CODIGO, CIDADE.NOME, ");
                query.Append(" CIDADE.UF_CIDADE, C.ID_CIDADE_IBGE_COBRANCA, CIDADE_COBRANCA.CODIGO, CIDADE_COBRANCA.NOME, CIDADE_COBRANCA.UF_CIDADE, ");
                query.Append(" C.DESTACA_ISS, C.GERA_COBRANCA_VALOR_LIQUIDO, C.ALIQUOTA_ISS, CONTRATO.CODIGO_CONTRATO, CONTRATO.DATA_ELABORACAO, CONTRATO.DATA_FIM, CONTRATO.DATA_INICIO, ");
                query.Append(" CONTRATO.SITUACAO, CONTRATO.OBSERVACAO, CONTRATO.ID_USUARIO_FINALIZOU, CONTRATO.ID_USUARIO_CANCELOU, CONTRATO.MOTIVO_CANCELAMENTO, CONTRATO.CONTRATO_AUTOMATICO, ");
                query.Append(" CONTRATO.DATA_CANCELAMENTO, CONTRATO.ID_CONTRATO_PAI, CONTRATO.BLOQUEIA_INADIMPLENCIA, CONTRATO.GERA_MENSALIDADE, ");
                query.Append(" CONTRATO.GERA_NUMERO_VIDAS, CONTRATO.DATA_COBRANCA, CONTRATO.COBRANCA_AUTOMATICA, CONTRATO.VALOR_MENSALIDADE, ");
                query.Append(" CONTRATO.VALOR_NUMERO_VIDAS, CONTRATO.DIAS_BLOQUEIO, CONTRATO.ID_CONTRATO_RAIZ, CONTRATO.ID_CLIENTE_PROPOSTA, CONTRATO.PROPOSTA, ");
                query.Append(" CONTRATO.PROPOSTA_FORMA_PAGAMENTO, CONTRATO.ID_USUARIO_ENCERROU, CONTRATO.DATA_ENCERRAMENTO, CONTRATO.MOTIVO_ENCERRAMENTO, C.CODIGO_CNES, C.USA_CENTRO_CUSTO, C.BLOQUEADO, CONTRATO.ID_EMPRESA, C.CREDENCIADORA, C.FISICA, C.USA_PO, C.SIMPLES_NACIONAL ");
                query.Append(" FROM SEG_CONTRATO CONTRATO ");
                query.Append(" LEFT JOIN SEG_CLIENTE C ON C.ID_CLIENTE = CONTRATO.ID_CLIENTE_PRESTADOR ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = C.ID_CIDADE_IBGE ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE_COBRANCA ON CIDADE_COBRANCA.ID_CIDADE_IBGE = C.ID_CIDADE_IBGE_COBRANCA ");
                query.Append(" LEFT JOIN SEG_MEDICO M ON M.ID_MEDICO = C.ID_MEDICO");
                query.Append(" LEFT JOIN SEG_CLIENTE_PROPOSTA CLIENTE_PROPOSTA ON CLIENTE_PROPOSTA.ID_CLIENTE_PROPOSTA = CONTRATO.ID_CLIENTE_PROPOSTA ");

                if (clienteProposta == null)
                    query.Append(" WHERE CONTRATO.CONTRATO_AUTOMATICO = FALSE AND CONTRATO.ID_CLIENTE = :VALUE1");
                else
                    query.Append(" WHERE CONTRATO.CONTRATO_AUTOMATICO = FALSE AND CONTRATO.ID_CLIENTE_PROPOSTA = :VALUE2 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente != null ? cliente.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = clienteProposta != null ? clienteProposta.Id : null;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    Contrato contrato = new Contrato( dr.GetInt64(0),  dr.IsDBNull(56) ? string.Empty : dr.GetString(56),  cliente,  dr.IsDBNull(2) ? null : new Usuario(dr.GetInt64(2)),  dr.IsDBNull(57) ? null : (DateTime?)dr.GetDateTime(57),  dr.IsDBNull(58) ? null : (DateTime?)dr.GetDateTime(58),  dr.IsDBNull(59) ? null : (DateTime?)dr.GetDateTime(59),  dr.IsDBNull(60) ? String.Empty : dr.GetString(60),  dr.IsDBNull(61) ? String.Empty : dr.GetString(61),  dr.IsDBNull(62) ? null : new Usuario(dr.GetInt64(62)),  dr.IsDBNull(63) ? null : new Usuario(dr.GetInt64(63)),  dr.IsDBNull(64) ? String.Empty : dr.GetString(64),  dr.IsDBNull(3) ? null : new Cliente(dr.GetInt64(3), dr.GetString(4), dr.GetString(5), dr.GetString(6), dr.GetString(7), dr.GetString(8), dr.GetString(9), dr.GetString(10), dr.GetString(11), dr.GetString(12), dr.GetString(13), dr.GetString(14), dr.GetString(15), dr.GetString(16), dr.GetString(17), dr.GetDateTime(18), dr.GetString(19), dr.GetString(20), dr.GetString(21), dr.GetString(22), dr.GetString(23), dr.GetString(24), dr.GetInt32(25), dr.GetInt32(26), dr.GetString(27), dr.GetString(28), dr.GetString(29), dr.GetString(30), dr.GetString(31), dr.GetString(32), dr.GetString(33), dr.IsDBNull(34) ? null : new Medico(dr.GetInt64(34)), dr.GetString(35), dr.GetBoolean(36), dr.GetString(37), dr.GetBoolean(38), dr.IsDBNull(39) ? null : new Cliente(dr.GetInt64(39)), dr.IsDBNull(40) ? null : new Cliente(dr.GetInt64(40)), dr.GetBoolean(41), dr.GetBoolean(42), dr.GetBoolean(43), dr.GetBoolean(44), new CidadeIbge(dr.GetInt64(45), dr.GetString(46), dr.GetString(47), dr.GetString(48)), dr.IsDBNull(49) ? null : new CidadeIbge(dr.GetInt64(49), dr.GetString(50), dr.GetString(51), dr.GetString(52)), dr.GetBoolean(53), dr.GetBoolean(54), dr.GetDecimal(55), dr.IsDBNull(83) ? string.Empty : dr.GetString(83), dr.GetBoolean(84), dr.GetBoolean(85), dr.GetBoolean(87), dr.GetBoolean(88), dr.GetBoolean(89), dr.GetBoolean(90)),  dr.GetBoolean(65),  dr.IsDBNull(66) ? null : (DateTime?)dr.GetDateTime(66), dr.IsDBNull(67) ? null : findById((Int64)dr.GetInt64(67), dbConnection), dr.GetBoolean(68), dr.GetBoolean(69), dr.GetBoolean(70), dr.IsDBNull(71) ? (int?)null : dr.GetInt32(71), dr.GetBoolean(72), dr.IsDBNull(73) ? null : (Decimal?)dr.GetDecimal(73), dr.IsDBNull(74) ? null : (Decimal?)dr.GetDecimal(74), dr.IsDBNull(75) ? (int?)null : dr.GetInt32(75), dr.IsDBNull(76) ? null : findById(dr.GetInt64(76), dbConnection), null, dr.GetBoolean(78), dr.IsDBNull(79) ? String.Empty : dr.GetString(79), dr.IsDBNull(80) ? null : new Usuario(dr.GetInt64(80)), dr.IsDBNull(81) ? (DateTime?)null : dr.GetDateTime(81), dr.IsDBNull(82) ? String.Empty : dr.GetString(82), dr.IsDBNull(86) ? null : new Empresa(dr.GetInt64(86)));

                    if (dr.IsDBNull(1))
                    {
                        query.Clear();

                        query.Append(" SELECT CLIENTE.ID_CLIENTE_PROPOSTA, CLIENTE.ID_CIDADE_IBGE, CLIENTE.RAZAO_SOCIAL, CLIENTE.NOME_FANTASIA, CLIENTE.CNPJ, CLIENTE.INSCRICAO, CLIENTE.ENDERECO, ");
                        query.Append(" CLIENTE.NUMERO, CLIENTE.COMPLEMENTO, CLIENTE.BAIRRO, CLIENTE.CEP, CLIENTE.UF, CLIENTE.TELEFONE, CLIENTE.CELULAR, CLIENTE.DATA_CADASTRO, CLIENTE.CONTATO, CLIENTE.EMAIL, ");
                        query.Append(" CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE ");
                        query.Append(" FROM SEG_CLIENTE_PROPOSTA CLIENTE ");
                        query.Append(" JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = CLIENTE.ID_CIDADE_IBGE ");
                        query.Append(" WHERE CLIENTE.ID_CLIENTE_PROPOSTA = :VALUE1 ");

                        NpgsqlCommand commandCliente = new NpgsqlCommand(query.ToString(), dbConnection);

                        commandCliente.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                        commandCliente.Parameters[0].Value = (Int64)dr.GetInt64(77);

                        NpgsqlDataReader drCliente = command.ExecuteReader();

                        while (drCliente.Read())
                        {
                            contrato.ClienteProposta = new ClienteProposta(drCliente.GetInt64(0), drCliente.GetString(2), drCliente.IsDBNull(3) ? String.Empty : drCliente.GetString(3), drCliente.IsDBNull(4) ? String.Empty : drCliente.GetString(4), drCliente.IsDBNull(5) ? String.Empty : drCliente.GetString(5), drCliente.GetString(6), drCliente.GetString(7), drCliente.IsDBNull(8) ? String.Empty : drCliente.GetString(8), drCliente.IsDBNull(9) ? String.Empty : drCliente.GetString(9), drCliente.IsDBNull(10) ? String.Empty : drCliente.GetString(10), new CidadeIbge(drCliente.GetInt64(2), drCliente.GetString(17), drCliente.GetString(18), drCliente.GetString(19)), drCliente.GetString(11), drCliente.IsDBNull(12) ? String.Empty : drCliente.GetString(12), drCliente.IsDBNull(13) ? String.Empty : drCliente.GetString(13), drCliente.GetDateTime(14), drCliente.IsDBNull(15) ? String.Empty : drCliente.GetString(15), drCliente.IsDBNull(16) ? String.Empty : drCliente.GetString(16)); 
                        }

                    }


                    contratos.Add(contrato);
                }

                return contratos;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllContratoByCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Contrato findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            Contrato contrato = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_CONTRATO, CODIGO_CONTRATO, ID_CLIENTE, DATA_ELABORACAO, DATA_FIM, ");
                query.Append(" DATA_INICIO, SITUACAO, OBSERVACAO, ID_USUARIO_CRIOU, ID_USUARIO_FINALIZOU, ");
                query.Append(" ID_USUARIO_CANCELOU, MOTIVO_CANCELAMENTO, ID_CLIENTE_PRESTADOR, CONTRATO_AUTOMATICO, ");
                query.Append(" DATA_CANCELAMENTO, ID_CONTRATO_PAI, BLOQUEIA_INADIMPLENCIA, GERA_MENSALIDADE, GERA_NUMERO_VIDAS, ");
                query.Append(" DATA_COBRANCA, COBRANCA_AUTOMATICA, VALOR_MENSALIDADE, VALOR_NUMERO_VIDAS, DIAS_BLOQUEIO, ID_CONTRATO_RAIZ, ID_CLIENTE_PROPOSTA, PROPOSTA, PROPOSTA_FORMA_PAGAMENTO, ID_USUARIO_ENCERROU, DATA_ENCERRAMENTO, MOTIVO_ENCERRAMENTO, ID_EMPRESA ");
                query.Append(" FROM SEG_CONTRATO WHERE ID_CONTRATO = :VALUE1");

                NpgsqlCommand commandAuxiliar = new NpgsqlCommand(query.ToString(), dbConnection);
                
                commandAuxiliar.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                commandAuxiliar.Parameters[0].Value = id;

                NpgsqlDataReader drAuxiliar = commandAuxiliar.ExecuteReader();

                while (drAuxiliar.Read())
                {
                    contrato = new Contrato(drAuxiliar.GetInt64(0), drAuxiliar.IsDBNull(1) ? String.Empty : drAuxiliar.GetString(1), drAuxiliar.IsDBNull(2) ? null : new Cliente(drAuxiliar.GetInt64(2)), drAuxiliar.IsDBNull(8) ? null : new Usuario(drAuxiliar.GetInt64(8)), drAuxiliar.IsDBNull(3) ? null : (DateTime?)drAuxiliar.GetDateTime(3), drAuxiliar.IsDBNull(4) ? null : (DateTime?)drAuxiliar.GetDateTime(4), drAuxiliar.IsDBNull(5) ? null : (DateTime?)drAuxiliar.GetDateTime(5), drAuxiliar.IsDBNull(6) ? String.Empty : drAuxiliar.GetString(6), drAuxiliar.IsDBNull(7) ? String.Empty : drAuxiliar.GetString(7), drAuxiliar.IsDBNull(9) ? null : new Usuario(drAuxiliar.GetInt64(9)), drAuxiliar.IsDBNull(10) ? null : new Usuario(drAuxiliar.GetInt64(10)), drAuxiliar.IsDBNull(11) ? String.Empty : drAuxiliar.GetString(11), drAuxiliar.IsDBNull(12) ? null : new Cliente(drAuxiliar.GetInt64(12)), drAuxiliar.GetBoolean(13), drAuxiliar.IsDBNull(14) ? null : (DateTime?)drAuxiliar.GetDateTime(14), drAuxiliar.IsDBNull(15) ? null : new Contrato((Int64)drAuxiliar.GetInt64(15)), drAuxiliar.GetBoolean(16), drAuxiliar.GetBoolean(17), drAuxiliar.GetBoolean(18), drAuxiliar.IsDBNull(19) ? (int?)null : drAuxiliar.GetInt32(19), drAuxiliar.GetBoolean(20), drAuxiliar.IsDBNull(21) ? null : (Decimal?)drAuxiliar.GetDecimal(21), drAuxiliar.IsDBNull(22) ? null : (Decimal?)drAuxiliar.GetDecimal(22), drAuxiliar.IsDBNull(23) ? (int?)null : drAuxiliar.GetInt32(23), drAuxiliar.IsDBNull(24) ? null : new Contrato(drAuxiliar.GetInt64(24)), drAuxiliar.IsDBNull(25) ? null : new ClienteProposta(drAuxiliar.GetInt64(25)), drAuxiliar.GetBoolean(26), drAuxiliar.IsDBNull(27) ? String.Empty : drAuxiliar.GetString(27), drAuxiliar.IsDBNull(28) ? null : new Usuario(drAuxiliar.GetInt64(28)), drAuxiliar.IsDBNull(29) ? (DateTime?)null : drAuxiliar.GetDateTime(29), drAuxiliar.IsDBNull(30) ? String.Empty : drAuxiliar.GetString(30), drAuxiliar.IsDBNull(31) ? null : new Empresa(drAuxiliar.GetInt64(31)));
                }

                if (contrato.ClienteProposta != null)
                {
                        
                    query.Clear();
                    
                    query.Append(" SELECT CLIENTE.ID_CLIENTE_PROPOSTA, CLIENTE.ID_CIDADE_IBGE, CLIENTE.RAZAO_SOCIAL, CLIENTE.NOME_FANTASIA, CLIENTE.CNPJ, CLIENTE.INSCRICAO, CLIENTE.ENDERECO, ");
                    query.Append(" CLIENTE.NUMERO, CLIENTE.COMPLEMENTO, CLIENTE.BAIRRO, CLIENTE.CEP, CLIENTE.UF, CLIENTE.TELEFONE, CLIENTE.CELULAR, CLIENTE.DATA_CADASTRO, CLIENTE.CONTATO, CLIENTE.EMAIL, ");
                    query.Append(" CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE ");
                    query.Append(" FROM SEG_CLIENTE_PROPOSTA CLIENTE ");
                    query.Append(" JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = CLIENTE.ID_CIDADE_IBGE ");
                    query.Append(" WHERE CLIENTE.ID_CLIENTE_PROPOSTA = :VALUE1 ");
                    
                    NpgsqlCommand commandClienteProposta = new NpgsqlCommand(query.ToString(), dbConnection);
                    
                    commandClienteProposta.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                    commandClienteProposta.Parameters[0].Value = contrato.ClienteProposta.Id;
                        
                    NpgsqlDataReader drCliente = commandClienteProposta.ExecuteReader();
                    
                    while (drCliente.Read())
                    {
                            contrato.ClienteProposta = new ClienteProposta(drCliente.GetInt64(0), drCliente.GetString(2), drCliente.IsDBNull(3) ? String.Empty : drCliente.GetString(3), drCliente.IsDBNull(4) ? String.Empty : drCliente.GetString(4), drCliente.IsDBNull(5) ? String.Empty : drCliente.GetString(5), drCliente.GetString(6), drCliente.GetString(7), drCliente.IsDBNull(8) ? String.Empty : drCliente.GetString(8), drCliente.IsDBNull(9) ? String.Empty : drCliente.GetString(9), drCliente.IsDBNull(10) ? String.Empty : drCliente.GetString(10), new CidadeIbge(drCliente.GetInt64(1), drCliente.GetString(17), drCliente.GetString(18), drCliente.GetString(19)), drCliente.GetString(11), drCliente.IsDBNull(12) ? String.Empty : drCliente.GetString(12), drCliente.IsDBNull(13) ? String.Empty : drCliente.GetString(13), drCliente.GetDateTime(14), drCliente.IsDBNull(15) ? String.Empty : drCliente.GetString(15), drCliente.IsDBNull(16) ? String.Empty : drCliente.GetString(16));
                    }
                }

                return contrato;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findContratoByFilter(Contrato contrato, DateTime? dataElaboracaoFinal, DateTime? dataVencimentoFinal, 
            DateTime? dataFechamentoFinal, DateTime? dataCancelamentoFinal, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findContratoByFilter");
            try
            {
                StringBuilder query = new StringBuilder();

                if (contrato.isNullForFilter())
                {
                    
                    query.Append(" SELECT CONTRATO.ID_CONTRATO, FORMATA_CODIGO_CONTRATO(CONTRATO.CODIGO_CONTRATO) AS CODIGO_CONTRATO, ");
                    query.Append(" CONTRATO.ID_CLIENTE, CLIENTE.RAZAO_SOCIAL, CLIENTE.CNPJ, ");
                    query.Append(" CONTRATO.DATA_ELABORACAO, CONTRATO.DATA_FIM, CONTRATO.DATA_INICIO, CONTRATO.SITUACAO,  ");
                    query.Append(" CONTRATO.OBSERVACAO, CONTRATO.ID_USUARIO_CRIOU, USUARIO_CRIOU.LOGIN AS LOGIN_CRIOU, CONTRATO.ID_USUARIO_FINALIZOU, ");
                    query.Append(" USUARIO_FINALIZOU.LOGIN AS LOGIN_FINALIZOU, CONTRATO.ID_USUARIO_CANCELOU, USUARIO_CANCELOU.LOGIN AS LOGIN_CANCELOU, ");
                    query.Append(" CONTRATO.MOTIVO_CANCELAMENTO, CONTRATO.ID_CLIENTE_PRESTADOR, PRESTADOR.RAZAO_SOCIAL AS PRESTADOR, ");
                    query.Append(" CONTRATO.DATA_CANCELAMENTO, CONTRATO.ID_CONTRATO_PAI, CONTRATO.ID_CONTRATO_RAIZ, CONTRATO.ID_CLIENTE_PROPOSTA, CONTRATO.ID_USUARIO_ENCERROU, USUARIO_ENCERROU.LOGIN AS LOGIN_ENCERROU, CONTRATO.DATA_ENCERRAMENTO, CLIENTE_PROPOSTA.RAZAO_SOCIAL AS CLIENTE_PROPOSTA ");
                    query.Append(" FROM SEG_CONTRATO CONTRATO ");
                    query.Append(" LEFT JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = CONTRATO.ID_CLIENTE ");
                    query.Append(" LEFT JOIN SEG_USUARIO USUARIO_CRIOU ON USUARIO_CRIOU.ID_USUARIO = CONTRATO.ID_USUARIO_CRIOU ");
                    query.Append(" LEFT JOIN SEG_USUARIO USUARIO_FINALIZOU ON USUARIO_FINALIZOU.ID_USUARIO = CONTRATO.ID_USUARIO_FINALIZOU ");
                    query.Append(" LEFT JOIN SEG_USUARIO USUARIO_CANCELOU ON USUARIO_CANCELOU.ID_USUARIO = CONTRATO.ID_USUARIO_CANCELOU ");
                    query.Append(" LEFT JOIN SEG_USUARIO USUARIO_ENCERROU ON USUARIO_ENCERROU.ID_USUARIO = CONTRATO.ID_USUARIO_ENCERROU ");
                    query.Append(" LEFT JOIN SEG_CLIENTE PRESTADOR ON PRESTADOR.ID_CLIENTE = CONTRATO.ID_CLIENTE_PRESTADOR ");
                    query.Append(" LEFT JOIN SEG_CLIENTE_PROPOSTA CLIENTE_PROPOSTA ON CLIENTE_PROPOSTA.ID_CLIENTE_PROPOSTA = CONTRATO.ID_CLIENTE_PROPOSTA ");

                    if (contrato.Proposta)
                        query.Append(" WHERE CONTRATO.PROPOSTA = TRUE AND CONTRATO.CONTRATO_AUTOMATICO = FALSE ");
                    else
                        query.Append(" WHERE CONTRATO.PROPOSTA = FALSE AND CONTRATO.CONTRATO_AUTOMATICO = FALSE ");

                }
                else
                {
                    query.Append(" SELECT CONTRATO.ID_CONTRATO, FORMATA_CODIGO_CONTRATO(CONTRATO.CODIGO_CONTRATO) AS CODIGO_CONTRATO, ");
                    query.Append(" CONTRATO.ID_CLIENTE, CLIENTE.RAZAO_SOCIAL, CLIENTE.CNPJ, ");
                    query.Append(" CONTRATO.DATA_ELABORACAO, CONTRATO.DATA_FIM, CONTRATO.DATA_INICIO, CONTRATO.SITUACAO,  ");
                    query.Append(" CONTRATO.OBSERVACAO, CONTRATO.ID_USUARIO_CRIOU, USUARIO_CRIOU.LOGIN AS LOGIN_CRIOU, CONTRATO.ID_USUARIO_FINALIZOU, ");
                    query.Append(" USUARIO_FINALIZOU.LOGIN AS LOGIN_FINALIZOU, CONTRATO.ID_USUARIO_CANCELOU, USUARIO_CANCELOU.LOGIN AS LOGIN_CANCELOU, ");
                    query.Append(" CONTRATO.MOTIVO_CANCELAMENTO, CONTRATO.ID_CLIENTE_PRESTADOR, PRESTADOR.RAZAO_SOCIAL AS PRESTADOR, ");
                    query.Append(" CONTRATO.DATA_CANCELAMENTO, CONTRATO.ID_CONTRATO_PAI, CONTRATO.ID_CONTRATO_RAIZ, CONTRATO.ID_CLIENTE_PROPOSTA, CONTRATO.ID_USUARIO_ENCERROU, USUARIO_ENCERROU.LOGIN AS LOGIN_ENCERROU, CONTRATO.DATA_ENCERRAMENTO, CLIENTE_PROPOSTA.RAZAO_SOCIAL AS CLIENTE_PROPOSTA ");
                    query.Append(" FROM SEG_CONTRATO CONTRATO ");
                    query.Append(" LEFT JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = CONTRATO.ID_CLIENTE ");
                    query.Append(" LEFT JOIN SEG_USUARIO USUARIO_CRIOU ON USUARIO_CRIOU.ID_USUARIO = CONTRATO.ID_USUARIO_CRIOU ");
                    query.Append(" LEFT JOIN SEG_USUARIO USUARIO_FINALIZOU ON USUARIO_FINALIZOU.ID_USUARIO = CONTRATO.ID_USUARIO_FINALIZOU ");
                    query.Append(" LEFT JOIN SEG_USUARIO USUARIO_CANCELOU ON USUARIO_CANCELOU.ID_USUARIO = CONTRATO.ID_USUARIO_CANCELOU ");
                    query.Append(" LEFT JOIN SEG_USUARIO USUARIO_ENCERROU ON USUARIO_ENCERROU.ID_USUARIO = CONTRATO.ID_USUARIO_ENCERROU ");
                    query.Append(" LEFT JOIN SEG_CLIENTE PRESTADOR ON PRESTADOR.ID_CLIENTE = CONTRATO.ID_CLIENTE_PRESTADOR ");
                    query.Append(" LEFT JOIN SEG_CLIENTE_PROPOSTA CLIENTE_PROPOSTA ON CLIENTE_PROPOSTA.ID_CLIENTE_PROPOSTA = CONTRATO.ID_CLIENTE_PROPOSTA ");

                    query.Append(" WHERE CONTRATO.CONTRATO_AUTOMATICO = FALSE ");

                    if (!String.IsNullOrEmpty(contrato.CodigoContrato))
                        query.Append(" AND CONTRATO.CODIGO_CONTRATO = :VALUE1 ");

                    if (contrato.Cliente != null)
                        query.Append(" AND CONTRATO.ID_CLIENTE = :VALUE11 ");

                    if (!String.IsNullOrEmpty(contrato.Situacao))
                        query.Append(" AND CONTRATO.SITUACAO = :VALUE2 ");

                    if (contrato.DataElaboracao != null)
                        query.Append(" AND CONTRATO.DATA_ELABORACAO BETWEEN :VALUE3 AND :VALUE4 ");

                    if (contrato.DataFim != null)
                        query.Append(" AND CONTRATO.DATA_FIM BETWEEN :VALUE5 AND :VALUE6 ");

                    if (contrato.DataInicio != null)
                        query.Append(" AND CONTRATO.data_inicio BETWEEN :VALUE7 AND :VALUE8 ");

                    if (contrato.DataCancelamento != null)
                        query.Append(" AND CONTRATO.DATA_CANCELAMENTO BETWEEN :VALUE9 AND :VALUE10 ");

                    if (contrato.Proposta)
                        query.Append(" AND CONTRATO.PROPOSTA = TRUE ");
                    else
                        query.Append(" AND CONTRATO.PROPOSTA = FALSE ");


                    query.Append(" ORDER BY CODIGO_CONTRATO ASC ");


                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = contrato.CodigoContrato;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = contrato.Situacao;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[2].Value = contrato.DataElaboracao == null ? null : contrato.DataElaboracao;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[3].Value = dataElaboracaoFinal == null ? null : dataElaboracaoFinal;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[4].Value = contrato.DataFim == null ? null : contrato.DataFim;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[5].Value = dataVencimentoFinal == null ? null : dataVencimentoFinal;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[6].Value = contrato.DataInicio == null ? null : contrato.DataInicio;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[7].Value = dataFechamentoFinal == null ? null : dataFechamentoFinal;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[8].Value = contrato.DataCancelamento == null ? null : contrato.DataCancelamento;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[9].Value = dataCancelamentoFinal == null ? null : dataCancelamentoFinal;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[10].Value = contrato.Cliente == null ? null : contrato.Cliente.Id;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Contratos");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findContratoByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAditivoByContrato(Contrato contrato, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAditivoByContrato");
            try
            {
                StringBuilder query = new StringBuilder();

                query.Clear();
                
                query.Append(" SELECT CONTRATO.ID_CONTRATO, FORMATA_CODIGO_CONTRATO(CONTRATO.CODIGO_CONTRATO) AS CODIGO_CONTRATO, ");
                query.Append(" CONTRATO.ID_CLIENTE, CLIENTE.RAZAO_SOCIAL, (CASE WHEN CLIENTE.FISICA = TRUE THEN FORMATA_CPF(CLIENTE.CNPJ) ELSE FORMATA_CNPJ(CLIENTE.CNPJ) END) AS CNPJ, ");
                query.Append(" CONTRATO.DATA_ELABORACAO, CONTRATO.data_inicio, CONTRATO.DATA_FIM, CONTRATO.SITUACAO,  ");
                query.Append(" CONTRATO.OBSERVACAO, CONTRATO.ID_USUARIO_CRIOU, USUARIO_CRIOU.LOGIN, CONTRATO.ID_USUARIO_FINALIZOU, ");
                query.Append(" USUARIO_FINALIZOU.LOGIN, CONTRATO.ID_USUARIO_CANCELOU, USUARIO_CANCELOU.LOGIN, ");
                query.Append(" CONTRATO.MOTIVO_CANCELAMENTO, CONTRATO.ID_CLIENTE_PRESTADOR, PRESTADOR.RAZAO_SOCIAL, ");
                query.Append(" CONTRATO.DATA_CANCELAMENTO, CONTRATO.ID_CONTRATO_PAI, CONTRATO.ID_CONTRATO_RAIZ");
                query.Append(" FROM SEG_CONTRATO CONTRATO ");
                query.Append(" JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = CONTRATO.ID_CLIENTE ");
                query.Append(" LEFT JOIN SEG_USUARIO USUARIO_CRIOU ON USUARIO_CRIOU.ID_USUARIO = CONTRATO.ID_USUARIO_CRIOU ");
                query.Append(" LEFT JOIN SEG_USUARIO USUARIO_FINALIZOU ON USUARIO_FINALIZOU.ID_USUARIO = CONTRATO.ID_USUARIO_FINALIZOU ");
                query.Append(" LEFT JOIN SEG_USUARIO USUARIO_CANCELOU ON USUARIO_CANCELOU.ID_USUARIO = CONTRATO.ID_USUARIO_CANCELOU ");
                query.Append(" LEFT JOIN SEG_CLIENTE PRESTADOR ON PRESTADOR.ID_CLIENTE = CONTRATO.ID_CLIENTE_PRESTADOR ");
                query.Append(" WHERE CONTRATO.ID_CONTRATO_RAIZ = :VALUE1 ");
                query.Append(" ORDER BY CODIGO_CONTRATO ASC ");

                DataSet ds = new DataSet();
                
                NpgsqlDataAdapter aditivo = new NpgsqlDataAdapter(query.ToString(), dbConnection);
                
                aditivo.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                aditivo.SelectCommand.Parameters[0].Value = contrato.Id;
                
                aditivo.Fill(ds, "Aditivos");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAditivoByContrato", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Contrato update(Contrato contrato, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_CONTRATO SET  ");
                query.Append(" DATA_FIM = :VALUE2,  ");
                query.Append(" OBSERVACAO = :VALUE3, ");
                query.Append(" BLOQUEIA_INADIMPLENCIA = :VALUE4, ");
                query.Append(" GERA_NUMERO_VIDAS = :VALUE5,   ");
                query.Append(" DIAS_BLOQUEIO = :VALUE10, ");
                query.Append(" GERA_MENSALIDADE = :VALUE11, ");
                query.Append(" DATA_COBRANCA = :VALUE6, ");
                query.Append(" COBRANCA_AUTOMATICA = :VALUE7, ");
                query.Append(" VALOR_MENSALIDADE = :VALUE8,  ");
                query.Append(" VALOR_NUMERO_VIDAS = :VALUE9,  ");
                query.Append(" ID_CLIENTE = :VALUE12,");
                query.Append(" ID_CLIENTE_PROPOSTA = :VALUE13,  ");
                query.Append(" PROPOSTA_FORMA_PAGAMENTO = :VALUE14,   ");
                query.Append(" DATA_INICIO = :VALUE15, ");
                query.Append(" SITUACAO = :VALUE16");
                query.Append(" WHERE ID_CONTRATO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = contrato.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                command.Parameters[1].Value = contrato.DataFim;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Text));
                command.Parameters[2].Value = contrato.Observacao;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Boolean));
                command.Parameters[3].Value = contrato.BloqueiaInadimplencia;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Boolean));
                command.Parameters[4].Value = contrato.GeraNumeroVidas;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Integer));
                command.Parameters[5].Value = contrato.DataCobranca;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Boolean));
                command.Parameters[6].Value = contrato.CobrancaAutomatica;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Numeric));
                command.Parameters[7].Value = contrato.ValorMensalidade;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Numeric));
                command.Parameters[8].Value = contrato.ValorNumeroVidas;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Integer));
                command.Parameters[9].Value = contrato.DiasInadimplenciaBloqueio;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Boolean));
                command.Parameters[10].Value = contrato.GeraMensalidade;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Integer));
                command.Parameters[11].Value = contrato.Cliente != null ? contrato.Cliente.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Integer));
                command.Parameters[12].Value = contrato.ClienteProposta != null ? contrato.ClienteProposta.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Varchar));
                command.Parameters[13].Value = String.IsNullOrEmpty(contrato.PropostaFormaPagamento) ? String.Empty : contrato.PropostaFormaPagamento;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Timestamp));
                command.Parameters[14].Value = contrato.DataInicio;

                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Varchar));
                command.Parameters[15].Value = contrato.Situacao;

                command.ExecuteNonQuery();

                return contrato;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void cancela(Contrato contrato, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método cancela");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_CONTRATO SET SITUACAO = 'C',    ");
                query.Append(" ID_USUARIO_CANCELOU = :VALUE2,    ");
                query.Append(" MOTIVO_CANCELAMENTO = :VALUE3,   ");
                query.Append(" DATA_CANCELAMENTO = :VALUE4    ");
                query.Append(" WHERE ID_CONTRATO = :VALUE1   ");
                

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = contrato.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer ));
                command.Parameters[1].Value = contrato.UsuarioCancelamento.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Text));
                command.Parameters[2].Value = contrato.MotivoCancelamento;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Timestamp));
                command.Parameters[3].Value = contrato.DataCancelamento;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - cancela", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void validaContrato(Contrato contrato, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método validaContrato");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_CONTRATO SET SITUACAO = 'F',    ");
                query.Append(" ID_USUARIO_FINALIZOU = :VALUE2, DATA_INICIO = :VALUE3, DATA_FIM = :VALUE4 ");
                query.Append(" WHERE ID_CONTRATO = :VALUE1   ");


                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = contrato.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = contrato.UsuarioFinalizou.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Timestamp));
                command.Parameters[2].Value = contrato.DataInicio;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Timestamp));
                command.Parameters[3].Value = contrato.DataFim;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - validaContrato", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void validaProposta(Contrato contrato, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método validaProposta");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_CONTRATO SET SITUACAO = 'G',    ");
                query.Append(" PROPOSTA = FALSE    ");
                query.Append(" WHERE ID_CONTRATO = :VALUE1   ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = contrato.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - validaProposta", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Contrato insertAditivo(Contrato contrato, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertAditivo");

            StringBuilder query = new StringBuilder();

            Contrato aditivo = new Contrato();

            try
            {
                aditivo.Id = recuperaProximoId(dbConnection);

                aditivo.CodigoContrato = contrato.CodigoContrato.Substring(0, 12) + (Convert.ToInt32(contrato.CodigoContrato.Substring(12, 2)) + 1).ToString().PadLeft(2,'0');

                query.Append(" INSERT INTO SEG_CONTRATO (ID_CONTRATO, CODIGO_CONTRATO, ID_CLIENTE, DATA_ELABORACAO, SITUACAO, OBSERVACAO, ID_USUARIO_CRIOU, CONTRATO_AUTOMATICO, ID_CONTRATO_PAI, BLOQUEIA_INADIMPLENCIA, GERA_MENSALIDADE, ID_CLIENTE_PRESTADOR, GERA_NUMERO_VIDAS, DATA_COBRANCA, COBRANCA_AUTOMATICA, VALOR_MENSALIDADE, VALOR_NUMERO_VIDAS, DIAS_BLOQUEIO, ID_CONTRATO_RAIZ, PROPOSTA, ID_EMPRESA ) ");
                query.Append(" VALUES ");
                query.Append(" (:VALUE1, :VALUE2, :VALUE3, NOW(), 'G', :VALUE4, :VALUE5, FALSE, :VALUE6, :VALUE7, :VALUE8, :VALUE9, :VALUE10, :VALUE11, :VALUE12, :VALUE13, :VALUE14, :VALUE15, :VALUE16, FALSE, :VALUE17 )");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aditivo.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = aditivo.CodigoContrato;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = contrato.Cliente != null ? contrato.Cliente.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Text));
                command.Parameters[3].Value = contrato.Observacao;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Integer));
                command.Parameters[4].Value = SWS.Facade.PermissionamentoFacade.usuarioAutenticado.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Integer));
                command.Parameters[5].Value = contrato.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Boolean));
                command.Parameters[6].Value = contrato.BloqueiaInadimplencia;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Boolean));
                command.Parameters[7].Value = contrato.GeraMensalidade;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Integer));
                command.Parameters[8].Value = contrato.Prestador != null ? contrato.Prestador.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Boolean));
                command.Parameters[9].Value = contrato.GeraNumeroVidas;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Integer));
                command.Parameters[10].Value = contrato.DataCobranca;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Boolean));
                command.Parameters[11].Value = contrato.CobrancaAutomatica;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Numeric));
                command.Parameters[12].Value = contrato.ValorMensalidade;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Numeric));
                command.Parameters[13].Value = contrato.ValorNumeroVidas;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Integer));
                command.Parameters[14].Value = contrato.DiasInadimplenciaBloqueio;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Integer));
                command.Parameters[15].Value = contrato.ContratoRaiz != null ? contrato.ContratoRaiz.Id : contrato.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE17", NpgsqlDbType.Integer));
                command.Parameters[16].Value = contrato.Empresa.Id;


                command.ExecuteNonQuery();

                return aditivo;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertAditivo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void changeStatus(Contrato contrato, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método changeStatus");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_CONTRATO SET SITUACAO = :VALUE2 WHERE ID_CONTRATO = :VALUE1 ");


                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = contrato.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = contrato.Situacao;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - changeStatus", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean verificaUltimoContrato(Contrato contrato, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaUltimoContrato");

            StringBuilder query = new StringBuilder();

            Boolean status = true;

            try
            {
                query.Append(" SELECT * FROM SEG_CONTRATO   ");
                query.Append(" WHERE ID_CONTRATO_PAI = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = contrato.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    status = false;
                }

                return status;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaUltimoContrato", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean verificaIsAditivo(Contrato contrato, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaIsAditivo");

            StringBuilder query = new StringBuilder();

            Boolean status = false;

            try
            {
                query.Append(" SELECT * FROM SEG_CONTRATO   ");
                query.Append(" WHERE ID_CONTRATO_PAI IS NOT NULL AND ID_CONTRATO_RAIZ IS NOT NULL AND ID_CONTRATO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = contrato.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    status = true;
                }

                return status;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaIsAditivo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void delete(Contrato contrato, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" DELETE FROM SEG_CONTRATO WHERE ID_CONTRATO = :VALUE1    ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = contrato.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Contrato findContratoVigenteByData(DateTime dataInicio, Cliente cliente, Cliente prestador, NpgsqlConnection dbConnection)
        {   

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findContratoVigenteByData");

            Contrato contratoVigente = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ");
                query.Append(" CONTRATO.ID_CONTRATO, CONTRATO.ID_CLIENTE, CONTRATO.ID_USUARIO_CRIOU, CONTRATO.ID_CLIENTE_PRESTADOR, ");
                query.Append(" CONTRATO.CODIGO_CONTRATO, CONTRATO.DATA_ELABORACAO, CONTRATO.DATA_FIM, CONTRATO.DATA_INICIO,  ");
                query.Append(" CONTRATO.SITUACAO, CONTRATO.OBSERVACAO, CONTRATO.ID_USUARIO_FINALIZOU, CONTRATO.ID_USUARIO_CANCELOU, CONTRATO.MOTIVO_CANCELAMENTO, CONTRATO.CONTRATO_AUTOMATICO,  ");
                query.Append(" CONTRATO.DATA_CANCELAMENTO, CONTRATO.ID_CONTRATO_PAI, CONTRATO.BLOQUEIA_INADIMPLENCIA, CONTRATO.GERA_MENSALIDADE, ");
                query.Append(" CONTRATO.GERA_NUMERO_VIDAS, CONTRATO.DATA_COBRANCA, CONTRATO.COBRANCA_AUTOMATICA, CONTRATO.VALOR_MENSALIDADE, ");
                query.Append(" CONTRATO.VALOR_NUMERO_VIDAS, CONTRATO.DIAS_BLOQUEIO, CONTRATO.ID_CONTRATO_RAIZ, CONTRATO.ID_CLIENTE_PROPOSTA, CONTRATO.PROPOSTA, ");
                query.Append(" CONTRATO.PROPOSTA_FORMA_PAGAMENTO, CONTRATO.ID_USUARIO_ENCERROU, CONTRATO.DATA_ENCERRAMENTO, CONTRATO.MOTIVO_ENCERRAMENTO, CONTRATO.ID_EMPRESA ");
                query.Append(" FROM SEG_CONTRATO CONTRATO ");
                query.Append(" WHERE CONTRATO.CONTRATO_AUTOMATICO = FALSE AND ");
                query.Append(" CONTRATO.ID_CLIENTE = :VALUE1 ");
                query.Append(" AND (:VALUE2 BETWEEN CONTRATO.DATA_INICIO AND CONTRATO.DATA_FIM OR CONTRATO.DATA_FIM IS NULL) ");
                query.Append(" AND CONTRATO.SITUACAO = 'F'");

                if (prestador != null)
                    query.Append(" AND CONTRATO.ID_CLIENTE_PRESTADOR = :VALUE3 ");
                else
                    query.Append(" AND CONTRATO.ID_CLIENTE_PRESTADOR IS NULL");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                command.Parameters[1].Value = dataInicio;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = prestador != null ? prestador.Id : null;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    contratoVigente = new Contrato( dr.GetInt64(0), dr.IsDBNull(4) ? string.Empty : dr.GetString(4), cliente, dr.IsDBNull(2) ? null : new Usuario(dr.GetInt64(2)), dr.IsDBNull(5) ? null : (DateTime?)dr.GetDateTime(5), dr.IsDBNull(6) ? null : (DateTime?)dr.GetDateTime(6), dr.IsDBNull(7) ? null : (DateTime?)dr.GetDateTime(7), dr.IsDBNull(8) ? String.Empty : dr.GetString(8), dr.IsDBNull(9) ? String.Empty : dr.GetString(9), dr.IsDBNull(10) ? null : new Usuario(dr.GetInt64(10)), dr.IsDBNull(11) ? null : new Usuario(dr.GetInt64(11)), dr.IsDBNull(12) ? String.Empty : dr.GetString(12), prestador, dr.GetBoolean(13), dr.IsDBNull(14) ? null : (DateTime?)dr.GetDateTime(14), dr.IsDBNull(15) ? null : findById((Int64)dr.GetInt64(15), dbConnection), dr.GetBoolean(16), dr.GetBoolean(17), dr.GetBoolean(18), dr.IsDBNull(19) ? (int?)null : dr.GetInt32(19), dr.GetBoolean(20), dr.IsDBNull(21) ? null : (Decimal?)dr.GetDecimal(21), dr.IsDBNull(22) ? null : (Decimal?)dr.GetDecimal(22), dr.IsDBNull(23) ? (int?)null : dr.GetInt32(23), dr.IsDBNull(24) ? null : findById(dr.GetInt64(24), dbConnection), null,  dr.GetBoolean(26), dr.IsDBNull(27) ? String.Empty : dr.GetString(27), dr.IsDBNull(28) ? null : new Usuario(dr.GetInt64(28)), dr.IsDBNull(29) ? (DateTime?)null : dr.GetDateTime(29), dr.IsDBNull(30) ? String.Empty : dr.GetString(30), dr.IsDBNull(31) ? null : new Empresa(dr.GetInt64(31))); 
                }

                return contratoVigente;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findContratoVigenteByData", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void encerra(Contrato contrato, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método encerra");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_CONTRATO SET DATA_FIM = :VALUE3, ID_USUARIO_ENCERROU = :VALUE2, DATA_ENCERRAMENTO = :VALUE5, MOTIVO_ENCERRAMENTO = :VALUE4 WHERE ID_CONTRATO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = contrato.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = contrato.UsuarioEncerrou.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Timestamp));
                command.Parameters[2].Value = contrato.DataFim;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Text));
                command.Parameters[3].Value = contrato.MotivoEncerramento;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Timestamp));
                command.Parameters[4].Value = contrato.DataEncerramento;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - encerra", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Contrato verificaContratoVigente(DateTime dataInicio, DateTime? dataFim, Cliente cliente, Cliente prestador,  NpgsqlConnection dbConnection)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaContratoVigente");

            Contrato contratoVigente = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ");
                query.Append(" CONTRATO.ID_CONTRATO, CONTRATO.ID_CLIENTE, CONTRATO.ID_USUARIO_CRIOU, CONTRATO.ID_CLIENTE_PRESTADOR,   ");
                query.Append(" CONTRATO.CODIGO_CONTRATO, CONTRATO.DATA_ELABORACAO, CONTRATO.DATA_FIM, CONTRATO.DATA_INICIO, ");
                query.Append(" CONTRATO.SITUACAO, CONTRATO.OBSERVACAO, CONTRATO.ID_USUARIO_FINALIZOU, CONTRATO.ID_USUARIO_CANCELOU, CONTRATO.MOTIVO_CANCELAMENTO, CONTRATO.CONTRATO_AUTOMATICO, ");
                query.Append(" CONTRATO.DATA_CANCELAMENTO, CONTRATO.ID_CONTRATO_PAI, CONTRATO.BLOQUEIA_INADIMPLENCIA, CONTRATO.GERA_MENSALIDADE, ");
                query.Append(" CONTRATO.GERA_NUMERO_VIDAS, CONTRATO.DATA_COBRANCA, CONTRATO.COBRANCA_AUTOMATICA, CONTRATO.VALOR_MENSALIDADE, ");
                query.Append(" CONTRATO.VALOR_NUMERO_VIDAS, CONTRATO.DIAS_BLOQUEIO, CONTRATO.ID_CONTRATO_RAIZ, CONTRATO.ID_CLIENTE_PROPOSTA, CONTRATO.PROPOSTA, ");
                query.Append(" CONTRATO.PROPOSTA_FORMA_PAGAMENTO, CONTRATO.ID_USUARIO_ENCERROU, CONTRATO.DATA_ENCERRAMENTO, CONTRATO.MOTIVO_ENCERRAMENTO, CONTRATO.ID_EMPRESA ");
                query.Append(" FROM SEG_CONTRATO CONTRATO ");
                query.Append(" WHERE ");
                query.Append(" CONTRATO.CONTRATO_AUTOMATICO = FALSE");
                query.Append(" AND CONTRATO.ID_CLIENTE = :VALUE1 ");
                query.Append(" AND ((:VALUE2 BETWEEN CONTRATO.DATA_INICIO AND CONTRATO.DATA_FIM OR CONTRATO.DATA_FIM IS NULL)  ");
                query.Append(" OR (:VALUE3 BETWEEN CONTRATO.DATA_INICIO  AND CONTRATO.DATA_FIM OR CONTRATO.DATA_FIM IS NULL OR :VALUE3 <= CONTRATO.DATA_FIM))  ");
                query.Append(" AND CONTRATO.SITUACAO = 'F'");

                if (prestador != null)
                    query.Append(" AND CONTRATO.ID_CLIENTE_PRESTADOR = :VALUE4 ");
                else
                    query.Append(" AND CONTRATO.ID_CLIENTE_PRESTADOR IS NULL");


                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente != null ? cliente.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                command.Parameters[1].Value = dataInicio;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Timestamp));
                command.Parameters[2].Value = dataFim == null ? Convert.ToDateTime("31/12/9998 23:59:59") : dataFim;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = prestador == null ? null : prestador.Id;

                NpgsqlDataReader dr = command.ExecuteReader();
                
                while (dr.Read())
                {
                    /* verificando se o valor data final do contrato a validar é maior do que a data atual o contrato que apareceu vigente. 
                     * caso seja maior, então a parte OR DATA_FIM >= CONTRATO.DATA_FIM SERÁ EXCLUÍDA.*/

                    if (!dr.IsDBNull(6) && (DateTime)dr.GetDateTime(6) < dataFim)
                    {
                        dr.Close();
                        query.Clear();
                        /* recriando a query para retirar o parametro informado */

                        query.Append(" SELECT ");
                        query.Append(" CONTRATO.ID_CONTRATO, CONTRATO.ID_CLIENTE, CONTRATO.ID_USUARIO_CRIOU, CONTRATO.ID_CLIENTE_PRESTADOR,   ");
                        query.Append(" CONTRATO.CODIGO_CONTRATO, CONTRATO.DATA_ELABORACAO, CONTRATO.DATA_FIM, CONTRATO.DATA_INICIO, ");
                        query.Append(" CONTRATO.SITUACAO, CONTRATO.OBSERVACAO, CONTRATO.ID_USUARIO_FINALIZOU, CONTRATO.ID_USUARIO_CANCELOU, CONTRATO.MOTIVO_CANCELAMENTO, CONTRATO.CONTRATO_AUTOMATICO, ");
                        query.Append(" CONTRATO.DATA_CANCELAMENTO, CONTRATO.ID_CONTRATO_PAI, CONTRATO.BLOQUEIA_INADIMPLENCIA, CONTRATO.GERA_MENSALIDADE, ");
                        query.Append(" CONTRATO.GERA_NUMERO_VIDAS, CONTRATO.DATA_COBRANCA, CONTRATO.COBRANCA_AUTOMATICA, CONTRATO.VALOR_MENSALIDADE, ");
                        query.Append(" CONTRATO.VALOR_NUMERO_VIDAS, CONTRATO.DIAS_BLOQUEIO, CONTRATO.ID_CONTRATO_RAIZ, CONTRATO.ID_CLIENTE_PROPOSTA, CONTRATO.PROPOSTA, ");
                        query.Append(" CONTRATO.PROPOSTA_FORMA_PAGAMENTO, CONTRATO.ID_USUARIO_ENCERROU, CONTRATO.DATA_ENCERRAMENTO, CONTRATO.MOTIVO_ENCERRAMENTO, CONTRATO.ID_EMPRESA ");
                        query.Append(" FROM SEG_CONTRATO CONTRATO ");
                        query.Append(" WHERE ");
                        query.Append(" CONTRATO.CONTRATO_AUTOMATICO = FALSE ");
                        query.Append(" AND CONTRATO.ID_CLIENTE = :VALUE1 ");
                        query.Append(" AND ((:VALUE2 BETWEEN CONTRATO.DATA_INICIO AND CONTRATO.DATA_FIM OR CONTRATO.DATA_FIM IS NULL)  ");
                        query.Append(" OR (:VALUE3 BETWEEN CONTRATO.DATA_INICIO  AND CONTRATO.DATA_FIM OR CONTRATO.DATA_FIM IS NULL))  ");
                        query.Append(" AND CONTRATO.SITUACAO = 'F'");

                        if (prestador != null)
                            query.Append(" AND CONTRATO.ID_CLIENTE_PRESTADOR = :VALUE4 ");
                        else
                            query.Append(" AND CONTRATO.ID_CLIENTE_PRESTADOR IS NULL");

                        NpgsqlCommand commandAlterado = new NpgsqlCommand(query.ToString(), dbConnection);

                        commandAlterado.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                        commandAlterado.Parameters[0].Value = cliente != null ? cliente.Id : null;

                        commandAlterado.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                        commandAlterado.Parameters[1].Value = dataInicio;

                        commandAlterado.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Timestamp));
                        commandAlterado.Parameters[2].Value = dataFim == null ? Convert.ToDateTime("31/12/9998 23:59:59") : dataFim;

                        commandAlterado.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                        commandAlterado.Parameters[3].Value = prestador == null ? null : prestador.Id;

                        NpgsqlDataReader drAlterado = commandAlterado.ExecuteReader();

                        while (drAlterado.Read())
                        {
                            contratoVigente = new Contrato( drAlterado.GetInt64(0), drAlterado.IsDBNull(4) ? string.Empty : drAlterado.GetString(4), cliente,  drAlterado.IsDBNull(2) ? null : new Usuario(drAlterado.GetInt64(2)), drAlterado.IsDBNull(5) ? null : (DateTime?)drAlterado.GetDateTime(5), drAlterado.IsDBNull(6) ? null : (DateTime?)drAlterado.GetDateTime(6), drAlterado.IsDBNull(7) ? null : (DateTime?)drAlterado.GetDateTime(7), drAlterado.IsDBNull(8) ? String.Empty : drAlterado.GetString(8), drAlterado.IsDBNull(9) ? String.Empty : drAlterado.GetString(9), drAlterado.IsDBNull(10) ? null : new Usuario(drAlterado.GetInt64(10)), drAlterado.IsDBNull(11) ? null : new Usuario(drAlterado.GetInt64(11)), drAlterado.IsDBNull(12) ? String.Empty : drAlterado.GetString(12), prestador, drAlterado.GetBoolean(13), drAlterado.IsDBNull(14) ? null : (DateTime?)drAlterado.GetDateTime(14), drAlterado.IsDBNull(15) ? null : findById((Int64)drAlterado.GetInt64(15), dbConnection), drAlterado.GetBoolean(16), drAlterado.GetBoolean(17), drAlterado.GetBoolean(18), drAlterado.IsDBNull(19) ? (int?)null : drAlterado.GetInt32(19),  drAlterado.GetBoolean(20), drAlterado.IsDBNull(21) ? null : (Decimal?)drAlterado.GetDecimal(21), drAlterado.IsDBNull(22) ? null : (Decimal?)drAlterado.GetDecimal(22), drAlterado.IsDBNull(23) ? (int?)null : drAlterado.GetInt32(23), drAlterado.IsDBNull(24) ? null : findById(drAlterado.GetInt64(24), dbConnection), null, drAlterado.GetBoolean(26), drAlterado.IsDBNull(27) ? String.Empty : drAlterado.GetString(27), drAlterado.IsDBNull(28) ? null : new Usuario(drAlterado.GetInt64(28)), drAlterado.IsDBNull(29) ? (DateTime?)null : drAlterado.GetDateTime(29), drAlterado.IsDBNull(30) ? String.Empty : drAlterado.GetString(30), dr.IsDBNull(31) ? null : new Empresa(dr.GetInt64(31))); 
                            return contratoVigente;
                        }

                        /* retornando o contratoVigente como nulo porque foi permitido a inclusão de um novo contrato *
                        return contratoVigente; */

                        return contratoVigente;
                    }
                    
                    contratoVigente = new Contrato( dr.GetInt64(0), dr.IsDBNull(4) ? string.Empty : dr.GetString(4), cliente,  dr.IsDBNull(2) ? null : new Usuario(dr.GetInt64(2)), dr.IsDBNull(5) ? null : (DateTime?)dr.GetDateTime(5), dr.IsDBNull(6) ? null : (DateTime?)dr.GetDateTime(6), dr.IsDBNull(7) ? null : (DateTime?)dr.GetDateTime(7), dr.IsDBNull(8) ? String.Empty : dr.GetString(8), dr.IsDBNull(9) ? String.Empty : dr.GetString(9), dr.IsDBNull(10) ? null : new Usuario(dr.GetInt64(10)), dr.IsDBNull(11) ? null : new Usuario(dr.GetInt64(11)), dr.IsDBNull(12) ? String.Empty : dr.GetString(12), prestador, dr.GetBoolean(13), dr.IsDBNull(14) ? null : (DateTime?)dr.GetDateTime(14), dr.IsDBNull(15) ? null : findById((Int64)dr.GetInt64(15), dbConnection), dr.GetBoolean(16), dr.GetBoolean(17), dr.GetBoolean(18), dr.IsDBNull(19) ? (int?)null : dr.GetInt32(19), dr.GetBoolean(20), dr.IsDBNull(21) ? null : (Decimal?)dr.GetDecimal(21), dr.IsDBNull(22) ? null : (Decimal?)dr.GetDecimal(22), dr.IsDBNull(23) ? (int?)null : dr.GetInt32(23), dr.IsDBNull(24) ? null : findById(dr.GetInt64(24), dbConnection), null, dr.GetBoolean(26), dr.IsDBNull(27) ? String.Empty : dr.GetString(27), dr.IsDBNull(28) ? null : new Usuario(dr.GetInt64(28)), dr.IsDBNull(29) ? (DateTime?)null : dr.GetDateTime(29), dr.IsDBNull(30) ? String.Empty : dr.GetString(30), dr.IsDBNull(31) ? null : new Empresa(dr.GetInt64(31))); 
                }
                
                return contratoVigente;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaContratoVigente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Contrato findLastContratoByCliente(Cliente cliente, NpgsqlConnection dbConnection)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findLastContratoByCliente");

            Contrato contratoVigente = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ");
                query.Append(" CONTRATO.ID_CONTRATO, CONTRATO.ID_CLIENTE, CONTRATO.ID_USUARIO_CRIOU, CONTRATO.ID_CLIENTE_PRESTADOR, ");
                query.Append(" CONTRATO.CODIGO_CONTRATO, CONTRATO.DATA_ELABORACAO, CONTRATO.DATA_FIM, CONTRATO.DATA_INICIO,  ");
                query.Append(" CONTRATO.SITUACAO, CONTRATO.OBSERVACAO, CONTRATO.ID_USUARIO_FINALIZOU, CONTRATO.ID_USUARIO_CANCELOU, CONTRATO.MOTIVO_CANCELAMENTO, CONTRATO.CONTRATO_AUTOMATICO,  ");
                query.Append(" CONTRATO.DATA_CANCELAMENTO, CONTRATO.ID_CONTRATO_PAI, CONTRATO.BLOQUEIA_INADIMPLENCIA, CONTRATO.GERA_MENSALIDADE, ");
                query.Append(" CONTRATO.GERA_NUMERO_VIDAS, CONTRATO.DATA_COBRANCA, CONTRATO.COBRANCA_AUTOMATICA, CONTRATO.VALOR_MENSALIDADE, ");
                query.Append(" CONTRATO.VALOR_NUMERO_VIDAS, CONTRATO.DIAS_BLOQUEIO, CONTRATO.ID_CONTRATO_RAIZ, CONTRATO.ID_CLIENTE_PROPOSTA, CONTRATO.PROPOSTA, ");
                query.Append(" CONTRATO.PROPOSTA_FORMA_PAGAMENTO, CONTRATO.ID_USUARIO_ENCERROU, CONTRATO.DATA_ENCERRAMENTO, CONTRATO.MOTIVO_ENCERRAMENTO, CONTRATO.ID_EMPRESA ");
                query.Append(" FROM SEG_CONTRATO CONTRATO ");
                query.Append(" WHERE ");
                query.Append(" CONTRATO.CONTRATO_AUTOMATICO = FALSE ");
                query.Append(" AND CONTRATO.ID_CLIENTE = :VALUE1 ");
                query.Append(" AND SITUACAO = 'F'  ");
                query.Append(" AND ID_CONTRATO = (SELECT MAX(ID_CONTRATO) FROM SEG_CONTRATO WHERE ID_CLIENTE = :VALUE1 AND SITUACAO = 'F') ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = cliente.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    contratoVigente = new Contrato( dr.GetInt64(0), dr.IsDBNull(4) ? string.Empty : dr.GetString(4), cliente, dr.IsDBNull(2) ? null : new Usuario(dr.GetInt64(2)), dr.IsDBNull(5) ? null : (DateTime?)dr.GetDateTime(5), dr.IsDBNull(6) ? null : (DateTime?)dr.GetDateTime(6), dr.IsDBNull(7) ? null : (DateTime?)dr.GetDateTime(7), dr.IsDBNull(8) ? String.Empty : dr.GetString(8), dr.IsDBNull(9) ? String.Empty : dr.GetString(9), dr.IsDBNull(10) ? null : new Usuario(dr.GetInt64(10)), dr.IsDBNull(11) ? null : new Usuario(dr.GetInt64(11)), dr.IsDBNull(12) ? String.Empty : dr.GetString(12), dr.IsDBNull(3) ? null : new Cliente(dr.GetInt64(3)), dr.GetBoolean(13), dr.IsDBNull(14) ? null : (DateTime?)dr.GetDateTime(14), dr.IsDBNull(15) ? null : findById((Int64)dr.GetInt64(15), dbConnection), dr.GetBoolean(16), dr.GetBoolean(17), dr.GetBoolean(18), dr.IsDBNull(19) ? (int?)null : dr.GetInt32(19), dr.GetBoolean(20), dr.IsDBNull(21) ? null : (Decimal?)dr.GetDecimal(21), dr.IsDBNull(22) ? null : (Decimal?)dr.GetDecimal(22), dr.IsDBNull(23) ? (int?)null : dr.GetInt32(23), dr.IsDBNull(24) ? null : findById(dr.GetInt64(24), dbConnection), null, dr.GetBoolean(26), dr.IsDBNull(27) ? String.Empty : dr.GetString(27), dr.IsDBNull(28) ? null : new Usuario(dr.GetInt64(28)), dr.IsDBNull(29) ? (DateTime?)null : dr.GetDateTime(29), dr.IsDBNull(30) ? String.Empty : dr.GetString(30), dr.IsDBNull(31) ? null : new Empresa(dr.GetInt64(31)));
                }

                return contratoVigente;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findLastContratoByCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        
    }
}
