﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using System.Data;
using Npgsql;
using SWS.Excecao;
using NpgsqlTypes;
using SWS.Entidade;
using SWS.Helper;
using SWS.View.Resources;

namespace SWS.Dao
{
    class GheFonteDao : IGheFonteDao
    {
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_GHE_FONTE_ID_GHE_FONTE_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoIdGheFonte", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAllByGhe(Ghe ghe, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllFonteByGhe");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE_FONTE.ID_GHE_FONTE, GHE_FONTE.ID_FONTE, FONTE.DESCRICAO, GHE_FONTE.ID_GHE, GHE.DESCRICAO, GHE_FONTE.PRINCIPAL ");
                query.Append(" FROM SEG_GHE_FONTE GHE_FONTE ");
                query.Append(" JOIN SEG_FONTE FONTE ON FONTE.ID_FONTE = GHE_FONTE.ID_FONTE ");
                query.Append(" JOIN SEG_GHE GHE ON GHE.ID_GHE = GHE_FONTE.ID_GHE ");
                query.Append(" WHERE GHE_FONTE.ID_GHE = :VALUE1 ");
                query.Append(" ORDER BY GHE_FONTE.ID_GHE_FONTE ASC");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = ghe.Id;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Fontes");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllFonteByGhe", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public HashSet<GheFonte> findAllAtivoByGhe(Ghe ghe, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByGhe");

            HashSet<GheFonte> ghesFontes = new HashSet<GheFonte>();
            
            try
            {

                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE_FONTE.ID_GHE_FONTE, FONTE.ID_FONTE, FONTE.DESCRICAO, FONTE.SITUACAO, GHE.ID_GHE, GHE.DESCRICAO, ");
                query.Append(" GHE_FONTE.PRINCIPAL, FONTE.PRIVADO, GHE_FONTE.SITUACAO, GHE.NUMERO_PO ");
                query.Append(" FROM SEG_GHE_FONTE GHE_FONTE ");
                query.Append(" JOIN SEG_FONTE FONTE ON FONTE.ID_FONTE = GHE_FONTE.ID_FONTE ");
                query.Append(" JOIN SEG_GHE GHE ON GHE.ID_GHE = GHE_FONTE.ID_GHE");
                query.Append(" WHERE GHE.SITUACAO = 'A' AND GHE_FONTE.SITUACAO = 'A' AND GHE_FONTE.ID_GHE = :VALUE1 ");
                query.Append(" ORDER BY GHE_FONTE.ID_GHE_FONTE ASC ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = ghe.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    ghesFontes.Add(new GheFonte(dr.GetInt64(0), dr.IsDBNull(1) ? null : new Fonte(dr.GetInt64(1), dr.GetString(2), dr.GetString(3), dr.GetBoolean(7)), dr.IsDBNull(4) ? null : new Ghe(dr.GetInt64(4), null, dr.GetString(5), 0, string.Empty, dr.IsDBNull(9) ? string.Empty : dr.GetString(9)), dr.GetBoolean(6), dr.IsDBNull(8) ? String.Empty : dr.GetString(8)));
                }

                return ghesFontes;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByGhe", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public GheFonte insert(GheFonte gheFonte, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            StringBuilder query = new StringBuilder();

            try
            {
                gheFonte.Id = recuperaProximoId(dbConnection);

                query.Append(" INSERT INTO SEG_GHE_FONTE (ID_GHE_FONTE, ID_FONTE, ID_GHE, PRINCIPAL, SITUACAO ) ");
                query.Append(" VALUES ( :VALUE1, :VALUE2, :VALUE3, :VALUE4, 'A' ) ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonte.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = gheFonte.Fonte.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = gheFonte.Ghe.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Boolean));
                command.Parameters[3].Value = gheFonte.Principal;

                command.ExecuteNonQuery();

                return gheFonte;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public void deleteGheFonte(GheFonte gheFonte, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteGheFonte");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("DELETE FROM SEG_GHE_FONTE WHERE ID_GHE_FONTE = :VALUE1", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonte.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteGheFonte", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public GheFonte findByGheFonte(GheFonte gheFonte, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByGheFonte");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE_FONTE.ID_GHE_FONTE, FONTE.ID_FONTE, FONTE.DESCRICAO, FONTE.SITUACAO, GHE.ID_GHE, ");
                query.Append(" GHE.DESCRICAO, FONTE.PRIVADO, GHE.NEXP, GHE.NUMERO_PO ");
                query.Append(" FROM SEG_GHE_FONTE GHE_FONTE ");
                query.Append(" JOIN SEG_FONTE FONTE ON FONTE.ID_FONTE = GHE_FONTE.ID_FONTE ");
                query.Append(" JOIN SEG_GHE GHE ON GHE.ID_GHE = GHE_FONTE.ID_GHE ");
                query.Append(" WHERE ");
                query.Append(" FONTE.ID_FONTE = :VALUE1 AND GHE.ID_GHE = :VALUE2 ");
                query.Append(" ORDER BY FONTE.DESCRICAO");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonte.Fonte.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = gheFonte.Ghe.Id;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gheFonte = new GheFonte(dr.GetInt64(0), dr.IsDBNull(1)? null : new Fonte(dr.GetInt64(1), dr.GetString(2), dr.GetString(3), dr.GetBoolean(6)), dr.IsDBNull(4) ? null : new Ghe(dr.GetInt64(4), null, dr.GetString(5), dr.IsDBNull(7) ? 0 : dr.GetInt32(7), string.Empty, dr.IsDBNull(8) ? string.Empty : dr.GetString(8)));
                }

                return gheFonte;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByGheFonte", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public GheFonte findGheFonteInEpi(GheFonte gheFonte, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGheFonteInEpi");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE_FONTE.ID_GHE_FONTE, FONTE.ID_FONTE, FONTE.DESCRICAO, FONTE.SITUACAO, GHE.ID_GHE, ");
                query.Append(" GHE.DESCRICAO, FONTE.PRIVADO, GHE.NEXP, GHE.NUMERO_PO ");
                query.Append(" FROM SEG_GHE_FONTE GHE_FONTE ");
                query.Append(" JOIN SEG_FONTE FONTE ON FONTE.ID_FONTE = GHE_FONTE.ID_FONTE ");
                query.Append(" JOIN SEG_GHE GHE ON GHE.ID_GHE = GHE_FONTE.ID_GHE ");
                query.Append(" WHERE ");
                query.Append(" FONTE.ID_FONTE = :VALUE1 ");
                query.Append(" AND GHE.ID_GHE = :VALUE2 AND GHE_FONTE.PRINCIPAL <> 'T' ");
                query.Append(" ORDER BY FONTE.DESCRICAO");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonte.Fonte.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = gheFonte.Ghe.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {

                    gheFonte = new GheFonte(dr.GetInt64(0), dr.IsDBNull(1) ? null : new Fonte(dr.GetInt64(1), dr.GetString(2), dr.GetString(3), dr.GetBoolean(6)), 
                        dr.IsDBNull(4) ? null : new Ghe(dr.GetInt64(4), null, dr.GetString(5), dr.IsDBNull(7) ? 0 : dr.GetInt32(7), string.Empty, dr.IsDBNull(8) ? string.Empty : dr.GetString(8)));

                }

                return gheFonte;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheFonteInEpi", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAllByGheInEpi(Ghe ghe, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByGheInEpi");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE_FONTE.ID_GHE_FONTE, GHE_FONTE.ID_FONTE, FONTE.DESCRICAO, GHE_FONTE.ID_GHE, GHE.DESCRICAO, GHE_FONTE.PRINCIPAL ");
                query.Append(" FROM ");
                query.Append(" SEG_GHE_FONTE GHE_FONTE ");
                query.Append(" JOIN SEG_FONTE FONTE ON FONTE.ID_FONTE = GHE_FONTE.ID_FONTE ");
                query.Append(" JOIN SEG_GHE GHE ON GHE.ID_GHE = GHE_FONTE.ID_GHE ");
                query.Append(" WHERE GHE_FONTE.ID_GHE = :VALUE1 ");
                query.Append(" AND GHE_FONTE.PRINCIPAL <> 'T' ");
                query.Append(" ORDER BY GHE_FONTE.ID_GHE_FONTE ASC");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = ghe.Id;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Fontes");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByGheInEpi", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean verificaPodeAlterarFonte(Fonte fonte, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método finGheFonteByFonte");

            Boolean isGheFonteInUsed = false;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT COUNT(*) FROM SEG_GHE_FONTE GHE_FONTE ");
                query.Append(" WHERE GHE_FONTE.ID_FONTE = :VALUE1 AND GHE_FONTE.SITUACAO = 'A' ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = fonte.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    if (dr.GetInt64(0) > 0)
                    {
                        isGheFonteInUsed = true;
                    }
                }

                return isGheFonteInUsed;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - finGheFonteByFonte", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public GheFonte update(GheFonte gheFonte, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - update");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_GHE_FONTE SET PRINCIPAL = :VALUE2, SITUACAO = :VALUE3 WHERE ID_GHE_FONTE = :VALUE1 ");
                query.Append("  ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonte.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Boolean));
                command.Parameters[1].Value = gheFonte.Principal;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = gheFonte.Situacao;

                command.ExecuteNonQuery();

                return gheFonte;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public List<GheFonte> findAllByGheList(Ghe ghe, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByGhe");

            List<GheFonte> ghesFontes = new List<GheFonte>();

            try
            {

                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE_FONTE.ID_GHE_FONTE, FONTE.ID_FONTE, FONTE.DESCRICAO, FONTE.SITUACAO, GHE.ID_GHE, GHE.DESCRICAO, ");
                query.Append(" GHE_FONTE.PRINCIPAL, FONTE.PRIVADO, GHE_FONTE.SITUACAO, GHE.NUMERO_PO ");
                query.Append(" FROM SEG_GHE_FONTE GHE_FONTE ");
                query.Append(" JOIN SEG_FONTE FONTE ON FONTE.ID_FONTE = GHE_FONTE.ID_FONTE ");
                query.Append(" JOIN SEG_GHE GHE ON GHE.ID_GHE = GHE_FONTE.ID_GHE");
                query.Append(" WHERE GHE_FONTE.ID_GHE = :VALUE1 ");
                query.Append(" ORDER BY GHE_FONTE.ID_GHE_FONTE ASC ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = ghe.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    ghesFontes.Add(new GheFonte(dr.GetInt64(0), dr.IsDBNull(1) ? null : new Fonte(dr.GetInt64(1), dr.GetString(2), dr.GetString(3), dr.GetBoolean(7)), dr.IsDBNull(4) ? null : new Ghe(dr.GetInt64(4), null, dr.GetString(5), 0, string.Empty, dr.IsDBNull(9) ? string.Empty : dr.GetString(9)), dr.GetBoolean(6), dr.IsDBNull(8) ? String.Empty : dr.GetString(8)));
                }

                return ghesFontes;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByGhe", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public bool validaExclusaoGheFonte(GheFonte ghefonte, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método validaExclusaoGheFonte");

            Boolean isGheFonteInUsed = true;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT * FROM SEG_GHE_FONTE SGF  ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE SGFA ON SGFA .ID_GHE_FONTE = SGF.ID_GHE_FONTE  ");
                query.Append(" LEFT JOIN SEG_MONITORAMENTO_GHEFONTEAGENTE SMG ON SMG.ID_GHE_FONTE_AGENTE = SGFA.ID_GHE_FONTE_AGENTE ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME SGFAE ON SGFAE.ID_GHE_FONTE_AGENTE = SGFA.ID_GHE_FONTE_AGENTE  ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO SGFAEA ON SGFAEA.ID_GHE_FONTE_AGENTE_EXAME = SGFAE.ID_GHE_FONTE_AGENTE_EXAME  ");
                query.Append(" JOIN SEG_ASO SA ON SA.ID_ASO = SGFAEA.ID_ASO  ");
                query.Append(" WHERE SGF.ID_GHE_FONTE = :VALUE1  ");


                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = ghefonte.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    if (dr.GetInt64(0) > 0)
                    {
                        isGheFonteInUsed = false;
                    }
                }

                return isGheFonteInUsed;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - validaExclusaoGheFonte", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }
    }
}
