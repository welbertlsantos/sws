﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using SWS.Excecao;
using Npgsql;
using NpgsqlTypes;
using SWS.IDao;
using System.Data;
using SWS.Helper;
using SWS.View.Resources;

namespace SWS.Dao
{
    class MaterialHospitalarEstudoDao :IMaterialHospitalarEstudoDao
    {
        public void insertMaterialHospitalarEstudo(MaterialHospitalarEstudo materialHospitalarEstudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertMaterialHospitalarEstudo");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" insert into seg_material_hospitalar_estudo (id_estudo, id_material_hospitalar, quantidade ) ");
                query.Append(" values (:value1, :value2, :value3)");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value2", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value3", NpgsqlDbType.Integer));

                command.Parameters[0].Value = materialHospitalarEstudo.Estudo.Id;
                command.Parameters[1].Value = materialHospitalarEstudo.MaterialHospitalar.Id;
                command.Parameters[2].Value = materialHospitalarEstudo.Quantidade;
                
                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertMaterialHospitalarEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public DataSet findAllMaterialHospitalarEstudo(Estudo pcmso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllMaterialHospitalarEstudo");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT MHE.ID_ESTUDO, MHE.ID_MATERIAL_HOSPITALAR, MH.DESCRICAO, MHE.QUANTIDADE, MH.UNIDADE ");
                query.Append(" FROM SEG_MATERIAL_HOSPITALAR_ESTUDO MHE, SEG_MATERIAL_HOSPITALAR MH ");
                query.Append(" WHERE MHE.ID_MATERIAL_HOSPITALAR = MH.ID_MATERIAL_HOSPITALAR ");
                query.Append(" AND MHE.ID_ESTUDO = :VALUE1 ORDER BY MH.DESCRICAO ASC ");
                
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = pcmso.Id;
                

                DataSet ds = new DataSet();

                adapter.Fill(ds, "MateriaisHospitalaresEstudos");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllMaterialHospitalarEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public HashSet<MaterialHospitalarEstudo> findAllByEstudo(Estudo pcmso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllMaterialHospitalarEstudo");

            HashSet<MaterialHospitalarEstudo> materiaisHospitalaresEstudo = new HashSet<MaterialHospitalarEstudo>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" select mhe.id_estudo, mhe.id_material_hospitalar, mh.descricao, mhe.quantidade ");
                query.Append(" from seg_material_hospitalar_estudo mhe, seg_material_hospitalar mh ");
                query.Append(" where mhe.id_material_hospitalar = mh.id_material_hospitalar ");
                query.Append(" and mhe.id_estudo = :value1 order by mh.descricao asc ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = pcmso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    materiaisHospitalaresEstudo.Add(new MaterialHospitalarEstudo(new Estudo(dr.GetInt64(0)), new MaterialHospitalar(dr.GetInt64(1), string.Empty, string.Empty, string.Empty), dr.GetInt32(3)));
                }

                return materiaisHospitalaresEstudo;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllMaterialHospitalarEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void updateMaterialHospitalarEstudo(MaterialHospitalarEstudo materialHospitalarEstudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alteraMaterialHospitalarEstudo");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_MATERIAL_HOSPITALAR_ESTUDO SET QUANTIDADE = :VALUE1 "); 
                query.Append(" WHERE ID_ESTUDO = :VALUE2 AND ID_MATERIAL_HOSPITALAR = :VALUE3 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = materialHospitalarEstudo.Quantidade;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = materialHospitalarEstudo.Estudo.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = materialHospitalarEstudo.MaterialHospitalar.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alteraMaterialHospitalarEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public void deleteMaterialHospitalarEstudo(MaterialHospitalarEstudo materialHospitalarEstudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteMaterialHospitalarEstudo");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" DELETE FROM SEG_MATERIAL_HOSPITALAR_ESTUDO WHERE ");
                query.Append(" ID_ESTUDO = :VALUE1 AND ID_MATERIAL_HOSPITALAR = :VALUE2" );

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = materialHospitalarEstudo.Estudo.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = materialHospitalarEstudo.MaterialHospitalar.Id;
                

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteMaterialHospitalarEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean findMaterialHospitalarEstudoByMaterialHospitalar(MaterialHospitalar materialHospitalar, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMaterialHospitalarEstudoByMaterialHospitalar");

            Boolean isMaterialInUsed = false;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" select count(*) from seg_material_hospitalar_estudo mhe, seg_estudo e ");
                query.Append(" where mhe.id_estudo = e.id_estudo and mhe.id_material_hospitalar = :value1 and e.situacao = :value2 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value2", NpgsqlDbType.Varchar));

                command.Parameters[0].Value = materialHospitalar.Id;
                command.Parameters[1].Value = ApplicationConstants.FECHADO;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    if (dr.GetInt64(0) > 0)
                    {
                        isMaterialInUsed = true;
                    }
                }

                return isMaterialInUsed;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMaterialHospitalarEstudoByMaterialHospitalar", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }
    }
}
