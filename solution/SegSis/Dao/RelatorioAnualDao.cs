﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;
using SWS.Entidade;
using NpgsqlTypes;

namespace SWS.Dao
{
    class RelatorioAnualDao : IRelatorioAnualDao
    {
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("select nextval(('seg_relanual_id_relanual_seq'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoIdItem(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoIdItem");

            Int64 proximoId = Convert.ToInt64(0);

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("select nextval(('seg_relanual_item_id_relanual_item_seq'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoIdItem", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public RelatorioAnual insertRelatorioAnual(RelatorioAnual relatorioAnual, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertRelatorioAnual");

            try
            {
                relatorioAnual.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" insert into seg_relanual( id_relanual, id_usuario, id_medico_coordenador, id_cliente, data_inicial, data_final, data_geracao)");
                query.Append(" values (:value1, :value2, :value3, :value4, :value5, :value6, :value7)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value2", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value3", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value4", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value5", NpgsqlDbType.Date));
                command.Parameters.Add(new NpgsqlParameter("value6", NpgsqlDbType.Date));
                command.Parameters.Add(new NpgsqlParameter("value7", NpgsqlDbType.Timestamp));
                
                command.Parameters[0].Value = relatorioAnual.Id;
                command.Parameters[1].Value = relatorioAnual.Usuario.Id;
                command.Parameters[2].Value = relatorioAnual.MedicoCoordenador.Id;
                command.Parameters[3].Value = relatorioAnual.Cliente.Id;
                command.Parameters[4].Value = relatorioAnual.DataInicial;
                command.Parameters[5].Value = relatorioAnual.DataFinal;
                command.Parameters[6].Value = relatorioAnual.DataGeracao;

                command.ExecuteNonQuery();
                
                /* criacao dos itens do relatorio */

                foreach(RelatorioAnualItem itemRelatorio in relatorioAnual.ItemRelatorioAnual)
                {
                    
                    query.Clear();
                    
                    itemRelatorio.Id = recuperaProximoIdItem(dbConnection);

                    query.Append(" insert into seg_relanual_item (id_relanual_item, id_relanual, id_setor, descricao_setor, id_exame, descricao_exame, total_exame_normal, total_exame_anormal, previsao, id_periodicidade, descricao_periodicidade )");
                    query.Append(" values (:value1, :value2, :value3, :value4, :value5, :value6, :value7, :value8, :value9, :value10, :value11)");

                    NpgsqlCommand commandItem = new NpgsqlCommand(query.ToString(), dbConnection);

                    commandItem.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));
                    commandItem.Parameters.Add(new NpgsqlParameter("value2", NpgsqlDbType.Integer));
                    commandItem.Parameters.Add(new NpgsqlParameter("value3", NpgsqlDbType.Integer));
                    commandItem.Parameters.Add(new NpgsqlParameter("value4", NpgsqlDbType.Varchar));
                    commandItem.Parameters.Add(new NpgsqlParameter("value5", NpgsqlDbType.Integer));
                    commandItem.Parameters.Add(new NpgsqlParameter("value6", NpgsqlDbType.Varchar));
                    commandItem.Parameters.Add(new NpgsqlParameter("value7", NpgsqlDbType.Integer));
                    commandItem.Parameters.Add(new NpgsqlParameter("value8", NpgsqlDbType.Integer));
                    commandItem.Parameters.Add(new NpgsqlParameter("value9", NpgsqlDbType.Integer));
                    commandItem.Parameters.Add(new NpgsqlParameter("value10", NpgsqlDbType.Integer));
                    commandItem.Parameters.Add(new NpgsqlParameter("value11", NpgsqlDbType.Varchar));

                    commandItem.Parameters[0].Value = itemRelatorio.Id;
                    commandItem.Parameters[1].Value = relatorioAnual.Id;
                    commandItem.Parameters[2].Value = itemRelatorio.Setor.Id;
                    commandItem.Parameters[3].Value = itemRelatorio.Setor.Descricao;
                    commandItem.Parameters[4].Value = itemRelatorio.Exame.Id;
                    commandItem.Parameters[5].Value = itemRelatorio.Exame.Descricao;
                    commandItem.Parameters[6].Value = itemRelatorio.TotalExameNormal;
                    commandItem.Parameters[7].Value = itemRelatorio.TotalExameAnormal;
                    commandItem.Parameters[8].Value = itemRelatorio.Previsao;
                    commandItem.Parameters[9].Value = itemRelatorio.Tipo.Id;
                    commandItem.Parameters[10].Value = itemRelatorio.Tipo.Descricao;

                    commandItem.ExecuteNonQuery();

                }
                
                return relatorioAnual;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertRelatorioAnual", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public LinkedList<Setor> verificaSetoresInRelatorio(Cliente cliente, DateTime dataInicial, DateTime dataFinal,
            Cliente unidade, Boolean matriz, CentroCusto centroCusto, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaSetoresInRelatorio");

            try
            {
                StringBuilder query = new StringBuilder();
                LinkedList<Setor> setorInRelatorio = new LinkedList<Setor>();

                query.Append(" SELECT DISTINCT ");
                query.Append(" AGS.ID_SETOR, AGS.DESCRICAO_SETOR");
                query.Append(" FROM SEG_ASO_GHE_SETOR AGS ");
                query.Append(" JOIN SEG_ASO ATENDIMENTO ON ATENDIMENTO.ID_ASO = AGS.ID_ASO ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CFF ON CFF.ID_CLIENTE_FUNCAO_FUNCIONARIO = ATENDIMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO CF ON CF.ID_SEG_CLIENTE_FUNCAO = CFF.ID_SEG_CLIENTE_FUNCAO ");
                query.Append(" JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = CF.ID_CLIENTE ");
                query.Append(" JOIN SEG_PERIODICIDADE TIPO ON TIPO.ID_PERIODICIDADE = ATENDIMENTO.ID_PERIODICIDADE ");

                if (matriz)
                {
                    query.Append(" JOIN SEG_ESTUDO ESTUDO ON ESTUDO.ID_ESTUDO = ATENDIMENTO.ID_ESTUDO AND ESTUDO.ID_CLIENTE_CONTRATADO IS NULL ");
                }


                if (unidade != null)
                {
                    query.Append(" JOIN SEG_ESTUDO ESTUDO ON ESTUDO.ID_ESTUDO = ATENDIMENTO.ID_ESTUDO AND ESTUDO.ID_CLIENTE_CONTRATADO = :VALUE4 ");
                }

                query.Append(" WHERE ");
                query.Append(" CLIENTE.ID_CLIENTE = :VALUE1 AND ");
                query.Append(" ATENDIMENTO.DATA_ASO BETWEEN :VALUE2 AND :VALUE3 AND ");
                query.Append(" ATENDIMENTO.SITUACAO = 'F' AND ");
                query.Append(" ATENDIMENTO.ID_ESTUDO IS NOT NULL AND ");
                query.Append(" TIPO.ID_PERIODICIDADE <> 8 "); // PERIODICIDADE OUTROS. NÃO DEVE SER LEVADO EM CONTA NO RELATÓRIO.
                
                if (centroCusto != null)
                {
                    query.Append(" AND ATENDIMENTO.ID_CENTROCUSTO = :VALUE5 ");
                }
                
                query.Append(" ORDER BY DESCRICAO_SETOR ASC ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                command.Parameters[1].Value = dataInicial;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Timestamp));
                command.Parameters[2].Value = dataFinal;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = unidade != null ? unidade.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Integer));
                command.Parameters[4].Value = centroCusto != null ? centroCusto.Id : null;


                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    setorInRelatorio.AddFirst(new Setor(dr.GetInt64(0), dr.GetString(1), String.Empty));
                }

                return setorInRelatorio;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaSetoresInRelatorio", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public LinkedList<Exame> findExameInSetor(Setor setor, RelatorioAnual relatorioAnual, 
            Cliente unidade, Boolean matriz, CentroCusto centroCusto, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método montaItemRelatorio");

            try
            {
                StringBuilder query = new StringBuilder();

                LinkedList<Exame> exameInRelatorio = new LinkedList<Exame>();

                /* apurando exames para cada setor */
                
                query.Append(" SELECT DISTINCT " );
                query.Append(" EXAME.ID_EXAME, EXAME.DESCRICAO ");
                query.Append(" FROM SEG_ASO ATENDIMENTO");
                query.Append(" JOIN SEG_ASO_GHE_SETOR AGS ON AGS.ID_ASO = ATENDIMENTO.ID_ASO ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CFF ON CFF.ID_CLIENTE_FUNCAO_FUNCIONARIO = ATENDIMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO CF ON CF.ID_SEG_CLIENTE_FUNCAO = CFF.ID_SEG_CLIENTE_FUNCAO ");
                query.Append(" JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = CF.ID_CLIENTE ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA ON GFAEA.ID_ASO = ATENDIMENTO.ID_ASO ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GFAE ON GFAE.ID_GHE_FONTE_AGENTE_EXAME = GFAEA.ID_GHE_FONTE_AGENTE_EXAME ");
                query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = GFAE.ID_EXAME ");
                query.Append(" JOIN SEG_PERIODICIDADE TIPO ON TIPO.ID_PERIODICIDADE = ATENDIMENTO.ID_PERIODICIDADE ");

                if (matriz)
                {
                    query.Append(" JOIN SEG_ESTUDO ESTUDO ON ESTUDO.ID_ESTUDO = ATENDIMENTO.ID_ESTUDO AND ESTUDO.ID_CLIENTE_CONTRATADO IS NULL ");
                }

                if (unidade != null)
                {
                    query.Append(" JOIN SEG_ESTUDO ESTUDO ON ESTUDO.ID_ESTUDO = ATENDIMENTO.ID_ESTUDO AND ESTUDO.ID_CLIENTE_CONTRATADO = :VALUE5 ");
                }

                query.Append(" WHERE ");
                query.Append(" CLIENTE.ID_CLIENTE = :VALUE1 AND ");
                query.Append(" ATENDIMENTO.DATA_ASO BETWEEN :VALUE2 AND :VALUE3 AND ");
                query.Append(" AGS.ID_SETOR = :VALUE4 AND  ");
                query.Append(" ATENDIMENTO.SITUACAO = 'F' AND ");
                query.Append(" GFAEA.RESULTADO IS NOT NULL AND ");
                query.Append(" TIPO.ID_PERIODICIDADE <> 8 "); // PERIODICIDADE OUTROS. NÃO DEVE SER LEVADO EM CONTA NO RELATÓRIO

                if (centroCusto != null)
                    query.Append(" AND ATENDIMENTO.ID_CENTROCUSTO = :VALUE6 ");
                
                
                query.Append(" ORDER BY DESCRICAO ASC");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = relatorioAnual.Cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                command.Parameters[1].Value = relatorioAnual.DataInicial;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Timestamp));
                command.Parameters[2].Value = relatorioAnual.DataFinal;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = setor.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Integer));
                command.Parameters[4].Value = unidade != null ? unidade.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Integer));
                command.Parameters[5].Value = centroCusto != null ? centroCusto.Id : null;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    exameInRelatorio.AddFirst(new Exame(dr.GetInt64(0), dr.GetString(1), String.Empty, null, 0, 0, 0, false, false, string.Empty, false, null, false));
                }

                return exameInRelatorio;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - montaItemRelatorio", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int32 findTotalInSituacao(Setor setor, Exame exame, RelatorioAnual relatorioAnual, 
            String situacao, Periodicidade tipo, Cliente unidade, Boolean matriz, CentroCusto centroCusto, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findTotalInSituacao");

            try
            {
                StringBuilder query = new StringBuilder();

                Int32 total = 0;

                /* apurando exames para cada setor */

                query.Append(" SELECT CAST(COUNT(*) AS INTEGER) FROM SEG_ASO ATENDIMENTO ");
                query.Append(" JOIN SEG_ASO_GHE_SETOR AGS ON AGS.ID_ASO = ATENDIMENTO.ID_ASO ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CFF ON CFF.ID_CLIENTE_FUNCAO_FUNCIONARIO = ATENDIMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO CF ON CF.ID_SEG_CLIENTE_FUNCAO = CFF.ID_SEG_CLIENTE_FUNCAO ");
                query.Append(" JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = CF.ID_CLIENTE ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA ON GFAEA.ID_ASO = ATENDIMENTO.ID_ASO ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GFAE ON GFAE.ID_GHE_FONTE_AGENTE_EXAME = GFAEA.ID_GHE_FONTE_AGENTE_EXAME ");
                query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = GFAE.ID_EXAME ");
                query.Append(" JOIN SEG_PERIODICIDADE TIPO ON TIPO.ID_PERIODICIDADE = ATENDIMENTO.ID_PERIODICIDADE ");

                if (matriz)
                {
                    query.Append(" JOIN SEG_ESTUDO ESTUDO ON ESTUDO.ID_ESTUDO = ATENDIMENTO.ID_ESTUDO AND ESTUDO.ID_CLIENTE_CONTRATADO IS NULL ");
                }


                if (unidade != null)
                {
                    query.Append(" JOIN SEG_ESTUDO ESTUDO ON ESTUDO.ID_ESTUDO = ATENDIMENTO.ID_ESTUDO AND ESTUDO.ID_CLIENTE_CONTRATADO = :VALUE8 ");
                }

                query.Append(" WHERE ");
                query.Append(" ATENDIMENTO.SITUACAO = 'F' AND ");
                query.Append(" ATENDIMENTO.DATA_ASO BETWEEN :VALUE2 AND :VALUE3 AND  ");
                query.Append(" CLIENTE.ID_CLIENTE = :VALUE1 AND ");
                query.Append(" GFAEA.DUPLICADO = FALSE AND ");
                query.Append(" AGS.ID_SETOR = :VALUE4 AND ");
                query.Append(" EXAME.ID_EXAME = :VALUE5 AND ");
                query.Append(" GFAEA.RESULTADO = :VALUE6 AND ");
                query.Append(" TIPO.ID_PERIODICIDADE = :VALUE7 ");

                if (centroCusto != null)
                    query.Append(" AND ATENDIMENTO.ID_CENTROCUSTO = :VALUE8 ");
                

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = relatorioAnual.Cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                command.Parameters[1].Value = relatorioAnual.DataInicial;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Timestamp));
                command.Parameters[2].Value = relatorioAnual.DataFinal;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = setor.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Integer));
                command.Parameters[4].Value = exame.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Integer));
                command.Parameters[6].Value = tipo.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Integer));
                command.Parameters[7].Value = unidade != null ? unidade.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Integer));
                command.Parameters[8].Value = centroCusto != null ? centroCusto.Id : null;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    total = dr.GetInt32(0);
                }

                return total;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findTotalInSituacao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }


        public Dictionary<String, Int32> findTipoAtendimentoInSetorInExame(Setor setor, Exame exame, 
            RelatorioAnual relatorioAnual, Cliente unidade, Boolean matriz, CentroCusto centroCusto, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findTipoAtendimentoInSetorInExame");

            try
            {
                StringBuilder query = new StringBuilder();

                Dictionary<String, Int32> tipoAtendimento = new Dictionary<string, int>();


                query.Append(" SELECT (CASE WHEN TIPO.DESCRICAO = 'ANUAL' OR TIPO.DESCRICAO = 'BI-ANUAL' THEN 'PERIODICO' ELSE TIPO.DESCRICAO END) AS TIPO, ");
                query.Append(" CAST(COUNT(*) AS INTEGER) AS TOTAL FROM SEG_ASO ATENDIMENTO ");
                query.Append(" JOIN SEG_ASO_GHE_SETOR AGS ON AGS.ID_ASO = ATENDIMENTO.ID_ASO   ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CFF ON CFF.ID_CLIENTE_FUNCAO_FUNCIONARIO = ATENDIMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO CF ON CF.ID_SEG_CLIENTE_FUNCAO = CFF.ID_SEG_CLIENTE_FUNCAO  ");
                query.Append(" JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = CF.ID_CLIENTE  ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA ON GFAEA.ID_ASO = ATENDIMENTO.ID_ASO   ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GFAE ON GFAE.ID_GHE_FONTE_AGENTE_EXAME = GFAEA.ID_GHE_FONTE_AGENTE_EXAME   ");
                query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = GFAE.ID_EXAME   ");
                query.Append(" JOIN SEG_PERIODICIDADE TIPO ON  TIPO.ID_PERIODICIDADE = ATENDIMENTO.ID_PERIODICIDADE ");

                if (matriz)
                {
                    query.Append(" JOIN SEG_ESTUDO ESTUDO ON ESTUDO.ID_ESTUDO = ATENDIMENTO.ID_ESTUDO AND ESTUDO.ID_CLIENTE_CONTRATADO IS NULL ");
                }

                if (unidade != null)
                {
                    query.Append(" JOIN SEG_ESTUDO ESTUDO ON ESTUDO.ID_ESTUDO = ATENDIMENTO.ID_ESTUDO AND ESTUDO.ID_CLIENTE_CONTRATADO = :VALUE6 ");
                }

                query.Append(" WHERE   ");
                query.Append(" ATENDIMENTO.SITUACAO = 'F' AND  ");
                query.Append(" ATENDIMENTO.DATA_ASO BETWEEN :VALUE2 AND :VALUE3 AND    ");
                query.Append(" CLIENTE.ID_CLIENTE = :VALUE1 AND   ");
                query.Append(" GFAEA.DUPLICADO = FALSE AND  ");
                query.Append(" AGS.ID_SETOR = :VALUE4 AND ");
                query.Append(" EXAME.ID_EXAME = :VALUE5  ");

                if (centroCusto != null)
                    query.Append(" AND ATENDIMENTO.ID_CENTROCUSTO = :VALUE7 ");

                query.Append(" GROUP BY TIPO ");
                query.Append(" ORDER BY TIPO ASC ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = relatorioAnual.Cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                command.Parameters[1].Value = relatorioAnual.DataInicial;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Timestamp));
                command.Parameters[2].Value = relatorioAnual.DataFinal;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = setor.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Integer));
                command.Parameters[4].Value = exame.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Integer));
                command.Parameters[5].Value = unidade != null ? unidade.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Integer));
                command.Parameters[6].Value = centroCusto != null ? centroCusto.Id : null;

                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    tipoAtendimento.Add(dr.GetString(0), dr.GetInt32(1));
                }

                return tipoAtendimento;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - montaItemRelatorio", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public LinkedList<Periodicidade> findTipoInExame(Setor setor, Exame exame,
            RelatorioAnual relatorioAnual, Cliente unidade, Boolean matriz, CentroCusto centroCusto, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findTipoInExame");

            try
            {
                StringBuilder query = new StringBuilder();

                LinkedList<Periodicidade> tipo = new LinkedList<Periodicidade>();
                
                query.Append(" SELECT DISTINCT ");
                query.Append(" TIPO.ID_PERIODICIDADE,");
                query.Append(" (CASE WHEN TIPO.DESCRICAO = 'ANUAL' OR TIPO.DESCRICAO = 'BI-ANUAL' THEN 'PERIODICO' ELSE TIPO.DESCRICAO END) AS TIPO, TIPO.DIAS");
                query.Append(" FROM SEG_ASO ATENDIMENTO");
                query.Append(" JOIN SEG_ASO_GHE_SETOR AGS ON AGS.ID_ASO = ATENDIMENTO.ID_ASO   ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CFF ON CFF.ID_CLIENTE_FUNCAO_FUNCIONARIO = ATENDIMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO CF ON CF.ID_SEG_CLIENTE_FUNCAO = CFF.ID_SEG_CLIENTE_FUNCAO  ");
                query.Append(" JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = CF.ID_CLIENTE  ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA ON GFAEA.ID_ASO = ATENDIMENTO.ID_ASO   ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GFAE ON GFAE.ID_GHE_FONTE_AGENTE_EXAME = GFAEA.ID_GHE_FONTE_AGENTE_EXAME   ");
                query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = GFAE.ID_EXAME   ");
                query.Append(" JOIN SEG_PERIODICIDADE TIPO ON  TIPO.ID_PERIODICIDADE = ATENDIMENTO.ID_PERIODICIDADE ");

                if (matriz)
                {
                    query.Append(" JOIN SEG_ESTUDO ESTUDO ON ESTUDO.ID_ESTUDO = ATENDIMENTO.ID_ESTUDO AND ESTUDO.ID_CLIENTE_CONTRATADO IS NULL ");
                }

                if (unidade != null)
                {
                    query.Append(" JOIN SEG_ESTUDO ESTUDO ON ESTUDO.ID_ESTUDO = ATENDIMENTO.ID_ESTUDO AND ESTUDO.ID_CLIENTE_CONTRATADO = :VALUE6 ");
                }
                
                query.Append(" WHERE");
                query.Append(" ATENDIMENTO.SITUACAO = 'F' AND  ");
                query.Append(" CLIENTE.ID_CLIENTE = :VALUE1 AND   ");
                query.Append(" ATENDIMENTO.DATA_ASO BETWEEN :VALUE2 AND :VALUE3 AND    ");
                query.Append(" GFAEA.DUPLICADO = FALSE AND  ");
                query.Append(" AGS.ID_SETOR = :VALUE4 AND ");
                query.Append(" EXAME.ID_EXAME = :VALUE5 AND ");
                query.Append(" TIPO.ID_PERIODICIDADE <> '8'"); // PERIODICIDADE OUTROS NÃO DEVE SER LEVADO EM CONTA NO RELATÓRIO.

                if (centroCusto != null)
                {
                    query.Append(" AND ATENDIMENTO.ID_CENTROCUSTO = :VALUE7 ");
                }
                
                query.Append(" ORDER BY TIPO ASC ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = relatorioAnual.Cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("value2", NpgsqlDbType.Timestamp));
                command.Parameters[1].Value = relatorioAnual.DataInicial;

                command.Parameters.Add(new NpgsqlParameter("value3", NpgsqlDbType.Timestamp));
                command.Parameters[2].Value = relatorioAnual.DataFinal;

                command.Parameters.Add(new NpgsqlParameter("value4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = setor.Id;

                command.Parameters.Add(new NpgsqlParameter("value5", NpgsqlDbType.Integer));
                command.Parameters[4].Value = exame.Id;

                command.Parameters.Add(new NpgsqlParameter("value6", NpgsqlDbType.Integer));
                command.Parameters[5].Value = unidade != null ? unidade.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Integer));
                command.Parameters[6].Value = centroCusto != null ? centroCusto.Id : null;


                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    tipo.AddFirst(new Periodicidade(dr.GetInt64(0), dr.GetString(1), dr.IsDBNull(2) ? null : (int?)dr.GetInt32(2)));
                }

                return tipo;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findTipoInExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

    }
}
