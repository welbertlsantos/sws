﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using Npgsql;
using NpgsqlTypes;
using SWS.Excecao;
using System.Data;
using SWS.Helper;
using SWS.View.Resources;

namespace SWS.Dao
{
    class GheSetorDao : IGheSetorDao
    {

        public HashSet<GheSetor> findAtivosByGhe(Ghe ghe, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllGheSetor");

            HashSet<GheSetor> gheSetorRetorno = new HashSet<GheSetor>();
            
            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE_SETOR.ID_GHE_SETOR, GHE_SETOR.ID_SETOR, SETOR.DESCRICAO, SETOR.SITUACAO, GHE_SETOR.ID_GHE, GHE.DESCRICAO, GHE_SETOR.SITUACAO FROM SEG_GHE_SETOR GHE_SETOR ");
                query.Append(" JOIN SEG_SETOR SETOR ON SETOR.ID_SETOR = GHE_SETOR.ID_SETOR ");
                query.Append(" JOIN SEG_GHE GHE ON GHE.ID_GHE = GHE_SETOR.ID_GHE ");
                query.Append(" WHERE GHE_SETOR.ID_GHE = :VALUE1 AND GHE_SETOR.SITUACAO = 'A' ORDER BY SETOR.DESCRICAO ");
                                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = ghe.Id;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gheSetorRetorno.Add(new GheSetor(dr.GetInt64(0), dr.IsDBNull(1) ? null : new Setor(dr.GetInt64(1),dr.GetString(2),dr.GetString(3)), ghe, dr.GetString(6)));
                }
                
                return gheSetorRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtivoByGhe", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        private Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_GHE_SETOR_ID_GHE_SETOR_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoIdGheFonte", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public GheSetor insert(GheSetor gheSetor, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            try
            {
                gheSetor.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_GHE_SETOR (ID_GHE_SETOR, ID_SETOR, ID_GHE, SITUACAO ) VALUES ( :VALUE1, :VALUE2, :VALUE3, 'A') ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheSetor.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = gheSetor.Setor.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = gheSetor.Ghe.Id;
                

                command.ExecuteNonQuery();

                return gheSetor;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void delete(GheSetor gheSetor, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" DELETE FROM SEG_GHE_SETOR WHERE ID_GHE_SETOR = :VALUE1", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheSetor.Id;
                
                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAllByGhe(Ghe ghe, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllSetorByGhe");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE_SETOR.ID_GHE_SETOR, GHE_SETOR.ID_SETOR, SETOR.DESCRICAO AS SETOR, GHE_SETOR.ID_GHE, GHE.DESCRICAO AS GHE ");
                query.Append(" FROM SEG_GHE_SETOR GHE_SETOR ");
                query.Append(" JOIN SEG_SETOR SETOR ON SETOR.ID_SETOR = GHE_SETOR.ID_SETOR ");
                query.Append(" JOIN SEG_GHE GHE ON GHE.ID_GHE = GHE_SETOR.ID_GHE ");
                query.Append(" WHERE GHE_SETOR.ID_GHE = :VALUE1 AND GHE_SETOR.SITUACAO = 'A' ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = ghe.Id;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Setores");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllSetorByGhe", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public GheSetor findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            GheSetor gheSetor = new GheSetor();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE_SETOR.ID_GHE_SETOR, GHE_SETOR.ID_SETOR, SETOR.DESCRICAO, SETOR.SITUACAO, GHE_SETOR.ID_GHE, ");
                query.Append(" GHE.DESCRICAO, GHE_SETOR.SITUACAO, GHE.NEXP, GHE.NUMERO_PO FROM SEG_GHE_SETOR GHE_SETOR");
                query.Append(" JOIN SEG_SETOR SETOR ON SETOR.ID_SETOR = GHE_SETOR.ID_SETOR ");
                query.Append(" JOIN SEG_GHE GHE ON GHE.ID_GHE = GHE_SETOR.ID_GHE ");
                query.Append(" WHERE GHE_SETOR.ID_GHE_SETOR = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;
             
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gheSetor = new GheSetor(dr.GetInt64(0), dr.IsDBNull(1) ? null : new Setor(dr.GetInt64(1), dr.GetString(2), dr.GetString(3)), dr.IsDBNull(4) ? null : new Ghe(dr.GetInt64(4), null, dr.GetString(5), dr.IsDBNull(7) ? 0 : dr.GetInt32(7), string.Empty, dr.IsDBNull(8) ? string.Empty : dr.GetString(8)), dr.GetString(6));
                }
                
                return gheSetor;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int32 buscaQuantidadeByGhe(Ghe ghe, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método buscaQuantidadeByGhe");

            Int32 quantidadeSetor = 0;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT CAST(COUNT(*) AS INTEGER)");
                query.Append(" FROM SEG_GHE_SETOR");
                query.Append(" WHERE ID_GHE = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = ghe.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    quantidadeSetor = dr.GetInt32(0);
                }

                return quantidadeSetor;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - buscaQuantidadeByGhe", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void update(GheSetor gheSetor, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_GHE_SETOR SET SITUACAO = :VALUE1 WHERE ID_GHE_SETOR = :VALUE2");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = gheSetor.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = gheSetor.Id;

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean gheSetorIsUsedBySetor(Setor setor, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGheSetorBySetor");

            Boolean isSetorInUsed = false;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT COUNT(*) FROM SEG_GHE_SETOR ");
                query.Append(" WHERE ID_SETOR = :VALUE1 AND SITUACAO = 'A' ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = setor.Id;
                

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    if (dr.GetInt64(0) > 0)
                    {
                        isSetorInUsed = true;
                    }
                }

                return isSetorInUsed;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheSetorBySetor", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public LinkedList<GheSetor> findAllBySetorAndGhe(Setor setor, Ghe ghe, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllBySetorAndGhe");

            LinkedList<GheSetor> gheSetorList = new LinkedList<GheSetor>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE_SETOR.ID_GHE_SETOR, GHE_SETOR.SITUACAO ");
                query.Append(" FROM SEG_GHE_SETOR GHE_SETOR ");
                query.Append(" JOIN SEG_SETOR SETOR ON SETOR.ID_SETOR = GHE_SETOR.ID_SETOR");
                query.Append(" JOIN SEG_GHE GHE ON GHE.ID_GHE = GHE_SETOR.ID_GHE ");
                query.Append(" WHERE");
                query.Append(" GHE_SETOR.ID_SETOR = :VALUE1 AND GHE_SETOR.ID_GHE = :VALUE2 ");
                query.Append(" ORDER BY ID_GHE_SETOR DESC ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = setor.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = ghe.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gheSetorList.AddFirst(new GheSetor(dr.GetInt64(0), setor, ghe, dr.GetString(1)));
                }

                return gheSetorList;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllBySetorAndGhe", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public bool IsUsedInService(GheSetor gheSetor, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método gheSetorIsUsedInService");

            bool isSetorInUsed = false;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT DISTINCT GHE_SETOR.* FROM SEG_ASO ATENDIMENTO ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTE_FUNCAO_FUNCIONARIO ON CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO = ATENDIMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTE_FUNCAO_FUNCIONARIO.ID_SEG_CLIENTE_FUNCAO ");
                query.Append(" JOIN SEG_GHE_SETOR_CLIENTE_FUNCAO GHE_SETOR_CLIENTE_FUNCAO ON GHE_SETOR_CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO");
                query.Append(" JOIN SEG_GHE_SETOR GHE_SETOR ON GHE_SETOR.ID_GHE_SETOR = GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR");
                query.Append(" WHERE GHE_SETOR.ID_GHE_SETOR = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheSetor.Id;


                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    if (dr.GetInt64(0) > 0)
                    {
                        isSetorInUsed = true;
                    }
                }

                return isSetorInUsed;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - gheSetorIsUsedInService", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public List<GheSetor> findAllByGheList(Ghe ghe, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByGhe");

            List<GheSetor> gheSetorRetorno = new List<GheSetor>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE_SETOR.ID_GHE_SETOR, GHE_SETOR.ID_SETOR, SETOR.DESCRICAO, SETOR.SITUACAO, GHE_SETOR.ID_GHE, GHE.DESCRICAO, GHE_SETOR.SITUACAO FROM SEG_GHE_SETOR GHE_SETOR ");
                query.Append(" JOIN SEG_SETOR SETOR ON SETOR.ID_SETOR = GHE_SETOR.ID_SETOR ");
                query.Append(" JOIN SEG_GHE GHE ON GHE.ID_GHE = GHE_SETOR.ID_GHE ");
                query.Append(" WHERE GHE_SETOR.ID_GHE = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = ghe.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gheSetorRetorno.Add(new GheSetor(dr.GetInt64(0), dr.IsDBNull(1) ? null : new Setor(dr.GetInt64(1), dr.GetString(2), dr.GetString(3)), ghe, dr.GetString(6)));
                }

                return gheSetorRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByGhe", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
