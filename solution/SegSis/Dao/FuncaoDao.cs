﻿using SWS.Entidade;
using Npgsql;
using SWS.Excecao;
using System;
using NpgsqlTypes;
using System.Data;
using System.Text;
using SWS.IDao;
using SWS.View.Resources;
using System.Collections.Generic;
using SWS.Helper;

namespace SWS.Dao
{
    class FuncaoDao : IFuncaoDao
    {

        public DataSet findByFilter(Funcao funcao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByFilter");

            try
            {
                StringBuilder query = new StringBuilder();

                if (funcao.isNullForFilter())
                {
                    query.Append(" SELECT ID_FUNCAO, DESCRICAO, COD_CBO, SITUACAO FROM SEG_FUNCAO ORDER BY DESCRICAO ASC ");
                }
                else
                {
                    query.Append(" SELECT ID_FUNCAO,DESCRICAO, COD_CBO, SITUACAO FROM SEG_FUNCAO WHERE TRUE ");

                    if (!String.IsNullOrEmpty(funcao.Descricao))
                        query.Append(" AND DESCRICAO LIKE :VALUE1 ");

                    if (!String.IsNullOrEmpty(funcao.Situacao))
                        query.Append(" AND SITUACAO = :VALUE2 ");

                    if (!String.IsNullOrEmpty(funcao.CodCbo))
                        query.Append(" AND COD_CBO = :VALUE3 ");

                    query.Append(" ORDER BY DESCRICAO ASC");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = funcao.Descricao + "%";
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = funcao.Situacao;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[2].Value = funcao.CodCbo;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Funcoes");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Funcao insert(Funcao funcao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            StringBuilder query = new StringBuilder();

            try
            {
                funcao.Id = recuperaProximoId(dbConnection);

                query.Append(" INSERT INTO SEG_FUNCAO (ID_FUNCAO, DESCRICAO, COD_CBO, SITUACAO, COMENTARIO ) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, 'A', :VALUE4)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = funcao.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = funcao.Descricao.ToUpper();

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = funcao.CodCbo.ToUpper();

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = funcao.Comentario.ToUpper();
                
                command.ExecuteNonQuery();

                return funcao;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_FUNCAO_ID_FUNCAO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoIdFuncao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Funcao findByDescricao(String str, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByDescricao");

            Funcao funcaoRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_FUNCAO, DESCRICAO, COD_CBO, SITUACAO, COMENTARIO ");
                query.Append(" FROM SEG_FUNCAO WHERE DESCRICAO = :VALUE1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = str.ToUpper();

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    funcaoRetorno = new Funcao(dr.GetInt64(0), dr.GetString(1), dr.IsDBNull(2) ? string.Empty : dr.GetString(2), dr.GetString(3), dr.GetString(4));
                }

                return funcaoRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByDescricao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Funcao verificaPodeAlterarFuncao(Funcao funcao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaPodeAlterarFuncao");

            Funcao funcaoRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_FUNCAO, DESCRICAO, COD_CBO, SITUACAO, COMENTARIO ");
                query.Append(" FROM SEG_FUNCAO WHERE DESCRICAO = :VALUE1 AND SITUACAO = 'A' ");
                query.Append(" AND ID_FUNCAO <> :VALUE2 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = funcao.Descricao.ToUpper();
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = funcao.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    funcaoRetorno = new Funcao(dr.GetInt64(0), dr.GetString(1), dr.IsDBNull(2) ? string.Empty : dr.GetString(2), dr.GetString(3), dr.GetString(4));
                }

                return funcaoRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaPodeAlterarFuncao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Funcao findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            Funcao funcao = null;
            
            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_FUNCAO, DESCRICAO, COD_CBO, SITUACAO, COMENTARIO ");
                query.Append(" FROM SEG_FUNCAO WHERE ID_FUNCAO = :VALUE1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    funcao = new Funcao(dr.GetInt64(0), dr.GetString(1), dr.IsDBNull(2) ? string.Empty : dr.GetString(2), dr.GetString(3), dr.GetString(4));

                }
                return funcao;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public Funcao update(Funcao funcao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_FUNCAO SET DESCRICAO = :VALUE2, COD_CBO = :VALUE3, ");
                query.Append(" SITUACAO = :VALUE4, COMENTARIO = :VALUE5 WHERE ID_FUNCAO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = funcao.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = funcao.Descricao.ToUpper();
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = funcao.CodCbo.ToUpper();
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = funcao.Situacao;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = funcao.Comentario;

                command.ExecuteNonQuery();

                return funcao;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAtivaNotInClient(Funcao funcao, Cliente cliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtivaNotInClient");

            try
            {
                StringBuilder query = new StringBuilder();

                if (String.IsNullOrWhiteSpace(funcao.Descricao) && String.IsNullOrWhiteSpace(funcao.CodCbo))
                {
                    query.Append(" SELECT ID_FUNCAO, DESCRICAO, COD_CBO, SITUACAO, COMENTARIO, FALSE FROM SEG_FUNCAO WHERE SITUACAO = 'A' AND ");
                    query.Append(" ID_FUNCAO NOT IN (SELECT ID_FUNCAO FROM SEG_CLIENTE_FUNCAO WHERE ");
                    query.Append(" ID_CLIENTE = :VALUE1 AND SITUACAO = 'A') ORDER BY DESCRICAO ASC");
                }
                else
                {
                    query.Append(" SELECT ID_FUNCAO, DESCRICAO, COD_CBO, SITUACAO, COMENTARIO, FALSE FROM SEG_FUNCAO WHERE SITUACAO = 'A' AND ");
                    query.Append(" ID_FUNCAO NOT IN (SELECT ID_FUNCAO FROM SEG_CLIENTE_FUNCAO WHERE ");
                    query.Append(" ID_CLIENTE = :VALUE1 AND SITUACAO = 'A') ");


                    if (!String.IsNullOrWhiteSpace(funcao.Descricao))
                        query.Append(" AND DESCRICAO LIKE :VALUE2 ");

                    if (!String.IsNullOrWhiteSpace(funcao.CodCbo))
                        query.Append(" AND COD_CBO = :VALUE3 ");

                    query.Append(" ORDER BY DESCRICAO ASC");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = cliente.Id;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = funcao.Descricao + "%";
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[2].Value = funcao.CodCbo;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Funcoes");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtivaNotInClient", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public HashSet<Funcao> findByGhe(Ghe ghe, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByGhe");

            HashSet<Funcao> funcoes = new HashSet<Funcao>();
            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT FUNCAO.ID_FUNCAO, FUNCAO.DESCRICAO, FUNCAO.COD_CBO, FUNCAO.SITUACAO, FUNCAO.COMENTARIO ");
                query.Append(" FROM SEG_GHE GHE ");
                query.Append(" JOIN SEG_GHE_SETOR GHE_SETOR ON GHE_SETOR.ID_GHE = GHE.ID_GHE");
                query.Append(" JOIN SEG_GHE_SETOR_CLIENTE_FUNCAO GHE_SETOR_CLIENTE_FUNCAO ON GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR = GHE_SETOR.ID_GHE_SETOR ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = GHE_SETOR_CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO ");
                query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTE_FUNCAO.ID_FUNCAO ");
                query.Append(" WHERE GHE.ID_GHE = :VALUE1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = ghe.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    funcoes.Add(new Funcao(dr.GetInt64(0), dr.GetString(1), dr.IsDBNull(2) ? string.Empty : dr.GetString(2), dr.GetString(3), dr.GetString(4)));

                }
                return funcoes;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByGhe", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAllByGhe(Ghe ghe, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllFuncao");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT DISTINCT FUNCAO.ID_FUNCAO, FUNCAO.DESCRICAO, FUNCAO.COD_CBO, FUNCAO.SITUACAO, FUNCAO.COMENTARIO ");
                query.Append(" FROM SEG_GHE GHE "); 
                query.Append(" JOIN SEG_GHE_SETOR GHE_SETOR ON GHE_SETOR.ID_GHE = GHE.ID_GHE");
                query.Append(" JOIN SEG_GHE_SETOR_CLIENTE_FUNCAO GHE_SETOR_CLIENTE_FUNCAO ON GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR = GHE_SETOR.ID_GHE_SETOR ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = GHE_SETOR_CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO ");
                query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTE_FUNCAO.ID_FUNCAO ");
                query.Append(" WHERE GHE.ID_GHE = :VALUE1 ORDER BY FUNCAO.DESCRICAO ");
                
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = ghe.Id;
                
                DataSet dsFuncao = new DataSet();

                adapter.Fill(dsFuncao, "Funcoes");

                return dsFuncao;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllFuncao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAtivaNotInClient(Funcao funcao, Boolean select, List<ClienteFuncao> funcoes, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtivaNotInClient");

            try
            {
                StringBuilder query = new StringBuilder();

                int i = 0;

                if (String.IsNullOrWhiteSpace(funcao.Descricao) && String.IsNullOrWhiteSpace(funcao.CodCbo))
                {
                    query.Append("SELECT ID_FUNCAO, DESCRICAO, COD_CBO, SITUACAO, COMENTARIO, :VALUE4 FROM SEG_FUNCAO ");

                    if (funcoes.Count > 0)
                    {
                        query.Append(" WHERE ID_FUNCAO NOT IN (");

                        foreach (ClienteFuncao clienteFuncao in funcoes)
                        {
                            query.Append(clienteFuncao.Funcao.Id);
                            i++;

                            if (i < funcoes.Count)
                                query.Append(",");
                        }

                        query.Append(")");
                    }
                    
                    query.Append(" ORDER BY DESCRICAO ASC");
                }
                else
                {
                    query.Append(" SELECT ID_FUNCAO,DESCRICAO, COD_CBO, SITUACAO, COMENTARIO, :VALUE4 FROM SEG_FUNCAO WHERE TRUE");

                    if (!String.IsNullOrWhiteSpace(funcao.Descricao))
                        query.Append(" AND DESCRICAO LIKE :VALUE1 ");

                    if (!String.IsNullOrWhiteSpace(funcao.Situacao))
                        query.Append(" AND SITUACAO = :VALUE2 ");

                    if (!String.IsNullOrWhiteSpace(funcao.CodCbo))
                        query.Append(" AND COD_CBO = :VALUE3 ");

                    if (funcoes.Count > 0)
                    {
                        query.Append(" AND ID_FUNCAO NOT IN (");

                        foreach (ClienteFuncao clienteFuncao in funcoes)
                        {
                            query.Append(clienteFuncao.Funcao.Id);
                            i++;

                            if (i < funcoes.Count)
                                query.Append(",");
                        }

                        query.Append(")");
                    }

                    query.Append(" ORDER BY DESCRICAO ASC");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = funcao.Descricao.ToUpper() + "%";
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = funcao.Situacao;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[2].Value = funcao.CodCbo;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Boolean));
                adapter.SelectCommand.Parameters[3].Value = select;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Funcoes");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtivaNotInClient", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }


    }
}
