﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Helper;
using SWS.Entidade;
using NpgsqlTypes;
using Npgsql;
using SWS.Excecao;
using System.Data;
using SWS.View.Resources;

namespace SWS.Dao
{
    class ContratoProdutoDao : IContratoProdutoDao
    {

        public HashSet<ContratoProduto> findAtivosByContratoAndProduto(Contrato contrato, Produto produto, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtivosByContratoAndProduto");

            try
            {
                StringBuilder query = new StringBuilder();

                HashSet<ContratoProduto> colecaoProduto = new HashSet<ContratoProduto>();

                query.Append(" SELECT PRODUTO_CONTRATO.ID_PRODUTO_CONTRATO, PRODUTO_CONTRATO.ID_PRODUTO, PRODUTO_CONTRATO.ID_CONTRATO, PRODUTO_CONTRATO.PRECO_CONTRATADO, ");
                query.Append(" PRODUTO_CONTRATO.USUARIO, PRODUTO_CONTRATO.CUSTO_CONTRATO, ");
                query.Append(" PRODUTO.ID_PRODUTO, PRODUTO.NOME, PRODUTO.SITUACAO, PRODUTO.PRECO, PRODUTO.TIPO, PRODUTO.CUSTO, PRODUTO_CONTRATO.DATA_INCLUSAO, PRODUTO_CONTRATO.SITUACAO, PRODUTO_CONTRATO.ALTERA, PRODUTO.PADRAO_CONTRATO, PRODUTO.DESCRICAO ");
                query.Append(" FROM SEG_PRODUTO_CONTRATO PRODUTO_CONTRATO ");
                query.Append(" JOIN SEG_PRODUTO PRODUTO ON PRODUTO.ID_PRODUTO = PRODUTO_CONTRATO.ID_PRODUTO ");
                query.Append(" WHERE PRODUTO_CONTRATO.ID_CONTRATO = :VALUE1 AND PRODUTO_CONTRATO.SITUACAO = 'A' ");

                if (produto != null)
                    query.Append(" AND PRODUTO.DESCRICAO LIKE :VALUE2 ");
                                
                query.Append(" ORDER BY PRODUTO.DESCRICAO ASC ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = contrato.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = produto != null ? produto.Nome + "%" : null;

                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    colecaoProduto.Add(new ContratoProduto(dr.GetInt64(0), dr.IsDBNull(6) ? null : new Produto(dr.GetInt64(6), dr.GetString(7), dr.GetString(8), dr.GetDecimal(9), dr.GetString(10), dr.GetDecimal(11), dr.GetBoolean(15), dr.IsDBNull(16) ? string.Empty : dr.GetString(16)), contrato, dr.GetDecimal(3), contrato.UsuarioCriador, dr.GetDecimal(5), dr.IsDBNull(12) ? null : (DateTime?)dr.GetDateTime(12), dr.IsDBNull(13) ? String.Empty : dr.GetString(13), dr.GetBoolean(14)));
                }

                return colecaoProduto;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtivosByContratoAndProduto", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public ContratoProduto insert(ContratoProduto contratoProduto, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertContratoProduto");

            StringBuilder query = new StringBuilder();

            try
            {
                contratoProduto.Id = recuperaProximoId(dbConnection);

                query.Append(" INSERT INTO SEG_PRODUTO_CONTRATO ");
                query.Append(" (ID_PRODUTO_CONTRATO, ID_PRODUTO, ID_CONTRATO, PRECO_CONTRATADO, USUARIO, CUSTO_CONTRATO, DATA_INCLUSAO, SITUACAO, ALTERA ) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6, :VALUE7, 'A', :VALUE8)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = contratoProduto.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = contratoProduto.Produto.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = contratoProduto.Contrato.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Numeric));
                command.Parameters[3].Value = contratoProduto.PrecoContratado;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = contratoProduto.Usuario.Login;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Numeric));
                command.Parameters[5].Value = contratoProduto.Produto.Custo;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Timestamp));
                command.Parameters[6].Value = contratoProduto.DataInclusao;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Boolean)); 
                command.Parameters[7].Value = contratoProduto.Altera;

                command.ExecuteNonQuery();

                return contratoProduto;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertContratoProduto", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_PRODUTO_CONTRATO_ID_PRODUTO_CONTRATO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void delete(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" DELETE FROM SEG_PRODUTO_CONTRATO WHERE ID_PRODUTO_CONTRATO = :VALUE1", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public ContratoProduto update(ContratoProduto contratoProduto, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_PRODUTO_CONTRATO SET PRECO_CONTRATADO = :VALUE1, ");
                query.Append(" CUSTO_CONTRATO = :VALUE4, ALTERA = :VALUE5 WHERE ID_CONTRATO = :VALUE2 AND ID_PRODUTO = :VALUE3 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Numeric));
                command.Parameters[0].Value = contratoProduto.PrecoContratado;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = contratoProduto.Contrato.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = contratoProduto.Produto.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Numeric));
                command.Parameters[3].Value = contratoProduto.CustoContrato;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Boolean));
                command.Parameters[4].Value = contratoProduto.Altera;

                command.ExecuteNonQuery();

                return contratoProduto;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public ContratoProduto findByContratoAndProduto(Contrato contrato, Produto produto, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByContratoAndProduto");

            ContratoProduto contratoProduto = null;

            StringBuilder query = new StringBuilder();

            try
            {

                query.Append(" SELECT PRODUTO_CONTRATO.ID_PRODUTO_CONTRATO, PRODUTO_CONTRATO.PRECO_CONTRATADO, USUARIO.ID_USUARIO, USUARIO.LOGIN, PRODUTO_CONTRATO.CUSTO_CONTRATO, PRODUTO_CONTRATO.DATA_INCLUSAO, PRODUTO_CONTRATO.SITUACAO, PRODUTO_CONTRATO.ALTERA ");
                query.Append(" FROM SEG_PRODUTO_CONTRATO PRODUTO_CONTRATO ");
                query.Append(" JOIN SEG_USUARIO USUARIO ON USUARIO.LOGIN = PRODUTO_CONTRATO.USUARIO ");
                query.Append(" WHERE ");
                query.Append(" USUARIO.LOGIN = PRODUTO_CONTRATO.USUARIO AND  ");
                query.Append(" PRODUTO_CONTRATO.ID_CONTRATO = :VALUE1 AND PRODUTO_CONTRATO.ID_PRODUTO = :VALUE2 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = contrato.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = produto.Id;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    contratoProduto = new ContratoProduto(dr.GetInt64(0), produto, contrato, dr.GetDecimal(1), new Usuario(dr.GetInt64(2)), dr.GetDecimal(4), dr.IsDBNull(5) ? null : (DateTime?)dr.GetDateTime(5), dr.IsDBNull(6) ? String.Empty : dr.GetString(6), dr.GetBoolean(7));
                }

                return contratoProduto;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByContratoAndProduto", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public ContratoProduto findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            try
            {
                StringBuilder query = new StringBuilder();

                ContratoProduto contratoProduto = null;

                query.Append(" SELECT PRODUTO_CONTRATO.ID_PRODUTO_CONTRATO, PRODUTO_CONTRATO.ID_PRODUTO, PRODUTO_CONTRATO.ID_CONTRATO, PRODUTO_CONTRATO.PRECO_CONTRATADO, PRODUTO_CONTRATO.USUARIO, PRODUTO_CONTRATO.CUSTO_CONTRATO, PRODUTO_CONTRATO.DATA_INCLUSAO, ");
                query.Append(" PRODUTO.ID_PRODUTO, PRODUTO.NOME, PRODUTO.SITUACAO, PRODUTO.PRECO, PRODUTO.TIPO, PRODUTO.CUSTO, PRODUTO.TIPO_ESTUDO,  ");
                query.Append(" CONTRATO.ID_CONTRATO, CONTRATO.CODIGO_CONTRATO, CONTRATO.ID_CLIENTE, CONTRATO.DATA_ELABORACAO, CONTRATO.DATA_FIM, CONTRATO.DATA_INICIO, CONTRATO.SITUACAO, CONTRATO.OBSERVACAO, CONTRATO.ID_USUARIO_CRIOU, CONTRATO.ID_USUARIO_FINALIZOU, CONTRATO.ID_USUARIO_CANCELOU, CONTRATO.MOTIVO_CANCELAMENTO, CONTRATO.ID_CLIENTE_PRESTADOR, PRODUTO_CONTRATO.DATA_INCLUSAO, PRODUTO_CONTRATO.SITUACAO, CONTRATO.PROPOSTA_FORMA_PAGAMENTO, CONTRATO.DATA_ENCERRAMENTO, CONTRATO.ID_USUARIO_ENCERROU, CONTRATO.CONTRATO_AUTOMATICO, CONTRATO.DATA_CANCELAMENTO, CONTRATO.ID_CONTRATO_PAI, CONTRATO.BLOQUEIA_INADIMPLENCIA, CONTRATO.GERA_MENSALIDADE, CONTRATO.GERA_NUMERO_VIDAS, CONTRATO.DATA_COBRANCA, CONTRATO.COBRANCA_AUTOMATICA, CONTRATO.VALOR_MENSALIDADE, CONTRATO.VALOR_NUMERO_VIDAS, CONTRATO.DIAS_BLOQUEIO, CONTRATO.ID_CONTRATO_RAIZ, CONTRATO.ID_CLIENTE_PROPOSTA, CONTRATO.PROPOSTA, PRODUTO_CONTRATO.SITUACAO, PRODUTO_CONTRATO.ALTERA, CONTRATO.MOTIVO_ENCERRAMENTO, CONTRATO.ID_EMPRESA, PRODUTO.PADRAO_CONTRATO, PRODUTO.DESCRICAO ");
                query.Append(" FROM SEG_PRODUTO_CONTRATO PRODUTO_CONTRATO ");
                query.Append(" JOIN SEG_PRODUTO PRODUTO ON PRODUTO.ID_PRODUTO = PRODUTO_CONTRATO.ID_PRODUTO ");
                query.Append(" JOIN SEG_CONTRATO CONTRATO ON CONTRATO.ID_CONTRATO = PRODUTO_CONTRATO.ID_CONTRATO ");
                query.Append(" WHERE PRODUTO_CONTRATO.ID_PRODUTO_CONTRATO = :VALUE1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    contratoProduto = new ContratoProduto( dr.GetInt64(0), dr.IsDBNull(7) ? null : new Produto(dr.GetInt64(7), dr.GetString(8), dr.GetString(9), dr.GetDecimal(10), dr.GetString(11), dr.GetDecimal(12), dr.GetBoolean(50), dr.IsDBNull(51) ? string.Empty : dr.GetString(51)), dr.IsDBNull(14) ? null : new Contrato(dr.GetInt64(14), dr.IsDBNull(15) ? String.Empty : dr.GetString(15), new Cliente(dr.GetInt64(16)), dr.IsDBNull(22) ? null : new Usuario(dr.GetInt64(22)), dr.IsDBNull(17) ? (DateTime?)null : dr.GetDateTime(17), dr.IsDBNull(18) ? (DateTime?)null : dr.GetDateTime(18), dr.IsDBNull(19) ? (DateTime?)null : dr.GetDateTime(19), dr.GetString(20), dr.IsDBNull(21) ? String.Empty : dr.GetString(21), dr.IsDBNull(23) ? null : new Usuario(dr.GetInt64(23)), dr.IsDBNull(24) ? null : new Usuario(dr.GetInt64(24)), dr.IsDBNull(25) ? String.Empty : dr.GetString(25), dr.IsDBNull(26) ? null : new Cliente(dr.GetInt64(26)), dr.GetBoolean(32), dr.IsDBNull(33) ? (DateTime?)null : dr.GetDateTime(33), dr.IsDBNull(34) ? null : new Contrato(dr.GetInt64(34)), dr.GetBoolean(35), dr.GetBoolean(36), dr.GetBoolean(37), dr.IsDBNull(38) ? (int?)null : dr.GetInt32(38), dr.GetBoolean(39), dr.IsDBNull(40) ? (Decimal?)null : dr.GetDecimal(40), dr.IsDBNull(41) ? (Decimal?)null : dr.GetDecimal(41), dr.IsDBNull(42) ? (int?)null : dr.GetInt32(42), dr.IsDBNull(43) ? null : new Contrato(dr.GetInt64(43)), dr.IsDBNull(44) ? null : new ClienteProposta(dr.GetInt64(44)), dr.GetBoolean(45), dr.IsDBNull(29) ? String.Empty : dr.GetString(29), dr.IsDBNull(31) ? null : new Usuario(dr.GetInt64(31)), dr.IsDBNull(30) ? (DateTime?)null : dr.GetDateTime(30), dr.IsDBNull(48) ? String.Empty : dr.GetString(48), dr.IsDBNull(49) ? null : new Empresa(dr.GetInt64(49))), dr.IsDBNull(3) ? (Decimal?)null : dr.GetDecimal(3), new Usuario(dr.GetInt64(22)), dr.IsDBNull(5) ? (Decimal?)null : dr.GetDecimal(5), dr.IsDBNull(6) ? (DateTime?)null : dr.GetDateTime(6), dr.GetString(46), dr.GetBoolean(47));
                }

                return contratoProduto;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAtivosByContrato(Contrato contrato, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtivosByContrato");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT PRODUTO_CONTRATO.ID_PRODUTO_CONTRATO, PRODUTO_CONTRATO.ID_PRODUTO, PRODUTO.NOME, PRODUTO_CONTRATO.PRECO_CONTRATADO, PRODUTO_CONTRATO.CUSTO_CONTRATO, PRODUTO_CONTRATO.DATA_INCLUSAO, PRODUTO_CONTRATO.SITUACAO, PRODUTO_CONTRATO.ALTERA,  PRODUTO.SITUACAO AS SITUACAO_PRODUTO, PRODUTO.TIPO, PRODUTO.TIPO_ESTUDO, CAST((CASE WHEN CUSTO_CONTRATO = 0 THEN 0 ELSE (((PRECO_CONTRATADO / CUSTO_CONTRATO)-1) * 100) END) AS NUMERIC (8,2)) AS LUCRO, PRODUTO.PADRAO_CONTRATO ");
                query.Append(" FROM SEG_PRODUTO_CONTRATO PRODUTO_CONTRATO ");
                query.Append(" JOIN SEG_PRODUTO PRODUTO ON PRODUTO.ID_PRODUTO = PRODUTO_CONTRATO.ID_PRODUTO ");
                query.Append(" WHERE PRODUTO_CONTRATO.ID_CONTRATO = :VALUE1 AND PRODUTO_CONTRATO.SITUACAO = 'A' ");
                query.Append(" ORDER BY DESCRICAO ASC ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
               adapter.SelectCommand.Parameters[0].Value = contrato.Id;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Produtos");

                return ds;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtivosByContrato", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void reativated(ContratoProduto contratoProduto, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método reativated");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" UPDATE SEG_PRODUTO_CONTRATO SET SITUACAO = 'A' WHERE ID_PRODUTO_CONTRATO = :VALUE1 ", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = contratoProduto.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - reativated", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void desativated(ContratoProduto contratoProduto, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método desativated");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" UPDATE SEG_PRODUTO_CONTRATO SET SITUACAO = 'I' WHERE ID_PRODUTO_CONTRATO = :VALUE1 ", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = contratoProduto.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - desativated", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean contratoProdutoIsUsed(ContratoProduto contratoProduto, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método contratoProdutoIsUsed");

            Boolean retorno = false;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT * FROM SEG_MOVIMENTO ");
                query.Append(" WHERE ID_PRODUTO_CONTRATO = :VALUE1  ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = contratoProduto.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    retorno = true;
                }

                return retorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - contratoProdutoIsUsed", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
