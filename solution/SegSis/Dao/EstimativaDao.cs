﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using System.Data;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;
using SWS.Entidade;
using NpgsqlTypes;

namespace SWS.Dao
{
    class EstimativaDao : IEstimativaDao
    {
        public DataSet findAll(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAll");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID_ESTIMATIVA, DESCRICAO, SITUACAO   ");
                query.Append(" FROM SEG_ESTIMATIVA ");
                query.Append(" WHERE SITUACAO = 'A' ");
                query.Append(" ORDER BY DESCRICAO ASC  ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);
                DataSet ds = new DataSet();

                adapter.Fill(ds, "Estimativa");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAll", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAllNotInEstudo(Estudo estudo, Estimativa estimativa, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllNotInEstudo");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT FALSE, ID_ESTIMATIVA, DESCRICAO, SITUACAO  ");
                query.Append(" FROM SEG_ESTIMATIVA  ");
                query.Append(" WHERE ID_ESTIMATIVA NOT IN (SELECT ID_ESTIMATIVA FROM SEG_ESTIMATIVA_ESTUDO WHERE ID_ESTUDO = :VALUE1 ) " );

                if (!String.IsNullOrEmpty(estimativa.Descricao.Trim()))
                    query.Append(" AND DESCRICAO LIKE :VALUE2 ");
                
                query.Append(" ORDER BY DESCRICAO ASC  ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = estudo.Id;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = estimativa.Descricao + "%";

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Estimativa");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllNotInEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
