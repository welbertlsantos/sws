﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;
using SWS.Entidade;
using NpgsqlTypes;

namespace SWS.Dao
{
    class MedicoExameDao : IMedicoExameDao
    {

        public long recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            long proximoId = 0;

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_MEDICO_EXAME_ID_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoIdCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public MedicoExame insert(MedicoExame medicoExame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            try
            {
                medicoExame.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_MEDICO_EXAME ( ");
                query.Append(" ID, ID_EXAME, ID_MEDICO, DATA_GRAVACAO ) VALUES ");
                query.Append(" (:VALUE1, :VALUE2, :VALUE3, NOW())");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = medicoExame.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = medicoExame.Exame.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = medicoExame.Medico.Id;

                command.ExecuteNonQuery();

                return medicoExame;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public MedicoExame update(MedicoExame medicoExame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_MEDICO_EXAME SET ID_EXAME = :VALUE2, ID_MEDICO :VALUE3, DATA_GRAVACAO :VALUE4 ");
                query.Append(" WHERE ID = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = medicoExame.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = medicoExame.Exame.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = medicoExame.Medico.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Date));
                command.Parameters[3].Value = medicoExame.DataGravacao;

                command.ExecuteNonQuery();

                return medicoExame;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public MedicoExame findById(long id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            MedicoExame medicoExame = null;

            StringBuilder query = new StringBuilder();

            try
            {

                query.Append(" SELECT MEDICO_EXAME.ID, MEDICO_EXAME.ID_EXAME, MEDICO_EXAME.ID_MEDICO, MEDICO_EXAME.DATA_GRAVACAO ");
                query.Append(" FROM SEG_MEDICO_EXAME MEDICO_EXAME ");
                query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = MEDICO_EXAME.ID_EXAME ");
                query.Append(" JOIN SEG_MEDICO MEDICO ON MEDICO.ID_MEDICO = MEDICO_EXAME.ID_MEDICO");
                query.Append(" WHERE MEDICO_EXAME.ID = :VALUE1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    medicoExame = new MedicoExame(dr.GetInt64(0), new Exame(dr.GetInt64(1)), new Medico(dr.GetInt64(2)), dr.GetDateTime(3));
                }

                return medicoExame;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<MedicoExame> findAllByExame(Exame exame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByExame");

            List<MedicoExame> medicosExame = new List<MedicoExame>();

            StringBuilder query = new StringBuilder();

            try
            {

                query.Append(" SELECT MEDICO_EXAME.ID, MEDICO_EXAME.ID_EXAME, MEDICO_EXAME.ID_MEDICO, MEDICO_EXAME.DATA_GRAVACAO ");
                query.Append(" FROM SEG_MEDICO_EXAME MEDICO_EXAME ");
                query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = MEDICO_EXAME.ID_EXAME ");
                query.Append(" JOIN SEG_MEDICO MEDICO ON MEDICO.ID_MEDICO = MEDICO_EXAME.ID_MEDICO");
                query.Append(" WHERE MEDICO_EXAME.ID_EXAME = :VALUE1");
                query.Append(" ORDER BY ID_EXAME ASC ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = exame.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    medicosExame.Add(new MedicoExame(dr.GetInt64(0), exame, new Medico(dr.GetInt64(2)), dr.GetDateTime(3)));
                }

                return medicosExame;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void delete(MedicoExame medicoExame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" DELETE FROM SEG_MEDICO_EXAME WHERE ID = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = medicoExame.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

    }
}
