﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using Npgsql;
using NpgsqlTypes;
using System.Data;
using SWS.Excecao;
using SWS.Helper;
using SWS.Entidade;
using SWS.View.Resources;

namespace SWS.Dao
{
    class PeriodicidadeDao :IPeriodicidadeDao
    {
        public DataSet findAll(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllPeriodicidade");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_PERIODICIDADE, DESCRICAO, FALSE FROM SEG_PERIODICIDADE ORDER BY DESCRICAO ASC");
                
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Periodicidades");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllPeriodicidade", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findByGheFonteAgenteExame(Int64 idGheFonteAgenteExame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllPeriodicidadeByGheFonteAgenteExame");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT PERIODICIDADE.ID_PERIODICIDADE, PERIODICIDADE.DESCRICAO, GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.PERIODO_VENCIMENTO FROM SEG_GHE_FONTE_AGENTE_EXAME GHE_FONTE_AGENTE_EXAME ");
                query.Append(" LEFT JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = GHE_FONTE_AGENTE_EXAME.ID_EXAME ");
                query.Append(" LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME_PERIODICIDADE GHE_FONTE_AGENTE_EXAME_PERIODICIDADE ON ");
                query.Append(" GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME = GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.ID_GHE_FONTE_AGENTE_EXAME AND GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.SITUACAO = 'A' ");
                query.Append(" LEFT JOIN SEG_PERIODICIDADE PERIODICIDADE ON PERIODICIDADE.ID_PERIODICIDADE = GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.ID_PERIODICIDADE ");
                query.Append(" WHERE GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME = :VALUE1 ");
                query.Append(" ORDER BY PERIODICIDADE.DESCRICAO ");


                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = idGheFonteAgenteExame;
                
                DataSet ds = new DataSet();

                adapter.Fill(ds, "Periodicidades");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllPeriodicidade", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Periodicidade findById(Int64 idPeriodicidade, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findPeriodicidadeById");

            Periodicidade periodicidadeRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_PERIODICIDADE, DESCRICAO, DIAS FROM SEG_PERIODICIDADE WHERE ID_PERIODICIDADE = :VALUE1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = idPeriodicidade;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    periodicidadeRetorno = new Periodicidade(dr.GetInt64(0), dr.GetString(1), dr.IsDBNull(2) ? null : (int?)dr.GetInt32(2));
                }

                return periodicidadeRetorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findPeriodicidadeById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findByFilter(Periodicidade periodicidade, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findPeriodicidadeByFilter");

            try
            {
                StringBuilder query = new StringBuilder();

                if (!String.IsNullOrWhiteSpace(periodicidade.Descricao))
                    query.Append("SELECT ID_PERIODICIDADE, DESCRICAO FROM SEG_PERIODICIDADE WHERE DESCRICAO LIKE :VALUE1");
                else
                    query.Append("SELECT ID_PERIODICIDADE, DESCRICAO FROM SEG_PERIODICIDADE");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = String.IsNullOrEmpty(periodicidade.Descricao) ? String.Empty : periodicidade.Descricao + "%";

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Periodicidades");

                return ds;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findPeriodicidadeByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAllNotInClienteFuncaoExame(ClienteFuncaoExame clienteFuncaoExame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllPeriodicidadesNotInClienteFuncaoExame");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID_PERIODICIDADE, DESCRICAO, FALSE ");
                query.Append(" FROM SEG_PERIODICIDADE WHERE ");
                query.Append(" ID_PERIODICIDADE NOT IN (SELECT ID_PERIODICIDADE FROM SEG_CLIENTE_FUNCAO_EXAME_PERIODICIDADE WHERE ID_CLIENTE_FUNCAO_EXAME = :VALUE1 AND SITUACAO = 'A' ) ");
                query.Append(" ORDER BY DESCRICAO ASC ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = clienteFuncaoExame.Id;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Periodicidades");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllPeriodicidadesNotInClienteFuncaoExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAllNotInGheFonteAgenteExame(Periodicidade periodicidade, GheFonteAgenteExame gheFonteAgenteExame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllPeriodicidadesNotInGheFonteAgenteExame");

            try
            {
                StringBuilder query = new StringBuilder();

                if (String.IsNullOrEmpty(periodicidade.Descricao))
                {
                    query.Append(" SELECT ID_PERIODICIDADE, DESCRICAO ");
                    query.Append(" FROM SEG_PERIODICIDADE WHERE ");
                    query.Append(" ID_PERIODICIDADE NOT IN (SELECT ID_PERIODICIDADE FROM SEG_GHE_FONTE_AGENTE_EXAME_PERIODICIDADE WHERE ID_GHE_FONTE_AGENTE_EXAME = :VALUE1 AND SITUACAO = 'A' ) ");
                    query.Append(" ORDER BY DESCRICAO ASC ");
                }
                else
                {
                    query.Append(" SELECT ID_PERIODICIDADE, DESCRICAO, FALSE ");
                    query.Append(" FROM SEG_PERIODICIDADE WHERE ");
                    query.Append(" ID_PERIODICIDADE NOT IN (SELECT ID_PERIODICIDADE FROM SEG_GHE_FONTE_AGENTE_EXAME_PERIODICIDADE WHERE ID_GHE_FONTE_AGENTE_EXAME = :VALUE1 AND SITUACAO = 'A' ) ");
                    query.Append(" AND DESCRICAO LIKE :VALUE2 ORDER BY DESCRICAO ASC ");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = gheFonteAgenteExame.Id;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = periodicidade.Descricao;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Periodicidades");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllPeriodicidadesNotInGheFonteAgenteExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}