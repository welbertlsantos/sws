﻿using SWS.Entidade;
using Npgsql;
using SWS.Excecao;
using System;
using NpgsqlTypes;
using System.Data;
using System.Text;
using SWS.IDao;
using SWS.View.Resources;
using System.Collections.Generic;
using SWS.Helper;

namespace SWS.Dao
{
    class CronogramaAtividadeDao : ICronogramaAtividadeDao
    {
        public CronogramaAtividade insert(CronogramaAtividade cronogramaAtividade, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            StringBuilder query = new StringBuilder();

            try
            {

                query.Append(" INSERT INTO SEG_ATIVIDADE_CRONOGRAMA ");
                query.Append(" (ID_ATIVIDADE, ID_CRONOGRAMA, MES_REALIZADO, ANO_REALIZADO, PUBLICO, EVIDENCIA ) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6)");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cronogramaAtividade.Atividade.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = cronogramaAtividade.Cronograma.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = cronogramaAtividade.MesRealizado;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = cronogramaAtividade.AnoRealizado;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = cronogramaAtividade.PublicoAlvo.ToUpper();

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = cronogramaAtividade.Evidencia.ToUpper();

                command.ExecuteNonQuery();

                return cronogramaAtividade;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public HashSet<CronogramaAtividade> findByCronograma(Cronograma cronograma, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByCronograma");

            HashSet<CronogramaAtividade> cronogramaAtividade = new HashSet<CronogramaAtividade>();

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ATIVIDADE.ID_ATIVIDADE, ATIVIDADE.DESCRICAO, ATIVIDADE.SITUACAO, ATIVIDADE_CRONOGRAMA.ID_ATIVIDADE, ");
                query.Append(" ATIVIDADE_CRONOGRAMA.ID_CRONOGRAMA, ATIVIDADE_CRONOGRAMA.MES_REALIZADO, ATIVIDADE_CRONOGRAMA.ANO_REALIZADO, ATIVIDADE_CRONOGRAMA.PUBLICO, ");
                query.Append(" ATIVIDADE_CRONOGRAMA.EVIDENCIA FROM SEG_ATIVIDADE ATIVIDADE ");
                query.Append(" JOIN SEG_ATIVIDADE_CRONOGRAMA ATIVIDADE_CRONOGRAMA ON ATIVIDADE_CRONOGRAMA.ID_ATIVIDADE = ATIVIDADE.ID_ATIVIDADE");
                query.Append(" WHERE ATIVIDADE_CRONOGRAMA.ID_CRONOGRAMA = :VALUE1");
                
                NpgsqlCommand commandCronograma = new NpgsqlCommand(query.ToString(), dbConnection);

                commandCronograma.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                commandCronograma.Parameters[0].Value = cronograma.Id;

                NpgsqlDataReader drCronograma = commandCronograma.ExecuteReader();
                
                while (drCronograma.Read())
                {
                    cronogramaAtividade.Add(new CronogramaAtividade(new Atividade(drCronograma.GetInt64(0), drCronograma.GetString(1), drCronograma.GetString(2)), cronograma, drCronograma.GetString(5), drCronograma.GetString(6), drCronograma.GetString(7), drCronograma.GetString(8)));
                }

                return cronogramaAtividade;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByCronograma", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void update(CronogramaAtividade cronogramaAtividade, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateCronogramaAtividade");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_ATIVIDADE_CRONOGRAMA SET MES_REALIZADO = :VALUE1, ANO_REALIZADO = :VALUE2, ");
                query.Append(" PUBLICO = :VALUE3, EVIDENCIA = :VALUE4 WHERE ID_ATIVIDADE = :VALUE5 AND ");
                query.Append(" ID_CRONOGRAMA = :VALUE6 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = cronogramaAtividade.MesRealizado;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = cronogramaAtividade.AnoRealizado;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = cronogramaAtividade.PublicoAlvo;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = cronogramaAtividade.Evidencia;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Integer));
                command.Parameters[4].Value = cronogramaAtividade.Atividade.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Integer));
                command.Parameters[5].Value = cronogramaAtividade.Cronograma.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void delete(CronogramaAtividade cronogramaAtividade, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            try
            {

                NpgsqlCommand command = new NpgsqlCommand("DELETE FROM SEG_ATIVIDADE_CRONOGRAMA WHERE ID_CRONOGRAMA = :VALUE1 AND ID_ATIVIDADE = :VALUE2" , dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cronogramaAtividade.Cronograma.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = cronogramaAtividade.Atividade.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean isAtividadeCronogramaIsUsedByAtividade(Atividade atividade, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método isAtividadeCronogramaIsUsedByAtividade");

            Boolean isAtividadeIsUsed = false;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT COUNT(*) FROM SEG_ATIVIDADE_CRONOGRAMA ");
                query.Append(" WHERE ID_ATIVIDADE = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = atividade.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    if (dr.GetInt64(0) > 0)
                    {
                        isAtividadeIsUsed = true;
                    }
                }

                return isAtividadeIsUsed;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - isAtividadeCronogramaIsUsedByAtividade", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Dictionary<Int64, CronogramaAtividade> findByCronogramaDic(Cronograma cronograma, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByCronogramaDic");

            Dictionary<Int64, CronogramaAtividade> mapaCronogramaAtividade = new Dictionary<long, CronogramaAtividade>();

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ATIVIDADE_CRONOGRAMA.ID_ATIVIDADE, ATIVIDADE_CRONOGRAMA.ID_CRONOGRAMA, ATIVIDADE_CRONOGRAMA.MES_REALIZADO, ATIVIDADE_CRONOGRAMA.ANO_REALIZADO, ATIVIDADE_CRONOGRAMA.PUBLICO, ATIVIDADE_CRONOGRAMA.EVIDENCIA, ");
                query.Append(" ATIVIDADE.ID_ATIVIDADE, ATIVIDADE.DESCRICAO, ATIVIDADE.SITUACAO FROM SEG_ATIVIDADE_CRONOGRAMA ATIVIDADE_CRONOGRAMA ");
                query.Append(" JOIN SEG_ATIVIDADE ATIVIDADE ON ATIVIDADE.ID_ATIVIDADE = ATIVIDADE_CRONOGRAMA.ID_ATIVIDADE ");
                query.Append(" WHERE ATIVIDADE_CRONOGRAMA.ID_CRONOGRAMA = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = cronograma.Id;

                NpgsqlDataReader dr = command.ExecuteReader();


                while (dr.Read())
                {
                    mapaCronogramaAtividade.Add(dr.GetInt64(0), new CronogramaAtividade(new Atividade(dr.GetInt64(6), dr.GetString(7), dr.GetString(8)), cronograma, dr.GetString(2), dr.GetString(3), dr.GetString(4), dr.GetString(5)));
                }

                return mapaCronogramaAtividade;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByCronogramaDic", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

    }
}

