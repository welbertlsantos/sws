﻿using SWS.Entidade;
using Npgsql;
using SWS.Excecao;
using System;
using NpgsqlTypes;
using System.Data;
using System.Text;
using SWS.IDao;
using SWS.View.Resources;
using SWS.Helper;

namespace SWS.Dao
{
    class AgenteDao : IAgenteDao
    {
        public DataSet findByFilter(Agente agente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByFilter");

            try
            {
                StringBuilder query = new StringBuilder();

                if (agente.isNullForFilter())
                {
                    query.Append(" SELECT AGENTE.ID_AGENTE, AGENTE.ID_RISCO, AGENTE.DESCRICAO, AGENTE.TRAJETORIA, AGENTE.DANOS, AGENTE.LIMITE, AGENTE.SITUACAO, RISCO.DESCRICAO FROM SEG_AGENTE AGENTE ");
                    query.Append(" JOIN SEG_RISCO RISCO ON RISCO.ID_RISCO = AGENTE.ID_RISCO ");
                    query.Append(" ORDER BY AGENTE.DESCRICAO ASC");
                }
                else
                {
                    query.Append(" SELECT AGENTE.ID_AGENTE, AGENTE.ID_RISCO, AGENTE.DESCRICAO, AGENTE.TRAJETORIA, AGENTE.DANOS, AGENTE.LIMITE, AGENTE.SITUACAO, RISCO.DESCRICAO FROM SEG_AGENTE AGENTE ");
                    query.Append(" JOIN SEG_RISCO RISCO ON RISCO.ID_RISCO = AGENTE.ID_RISCO WHERE TRUE ");

                    if (!String.IsNullOrWhiteSpace(agente.Descricao))
                        query.Append(" AND AGENTE.DESCRICAO LIKE :VALUE1");
                    
                    if (agente.Risco != null)
                        query.Append(" AND AGENTE.ID_RISCO = :VALUE2 ");

                    if (!String.IsNullOrWhiteSpace(agente.Situacao))
                        query.Append(" AND AGENTE.SITUACAO = :VALUE3 ");
                    
                    query.Append(" ORDER BY AGENTE.DESCRICAO ASC ");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[0].Value = agente.Descricao + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[1].Value = agente.Risco != null ? agente.Risco.Id : (Int64?)null;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[2].Value = agente.Situacao;
                
                DataSet ds = new DataSet();
                adapter.Fill(ds, "Agentes");
                
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Agente insert(Agente agente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            try
            {
                agente.Id= recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_AGENTE( ID_AGENTE, ID_RISCO, DESCRICAO, TRAJETORIA, DANOS, LIMITE, SITUACAO, PRIVADO, INTENSIDADE, TECNICA, CODIGO_ESOCIAL )");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6, 'A', :VALUE7, :VALUE8, :VALUE9, :VALUE10 )");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = agente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = agente.Risco.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = agente.Descricao;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = agente.Trajetoria;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = agente.Dano;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = agente.Limite;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Boolean));
                command.Parameters[6].Value = agente.Privado;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Varchar));
                command.Parameters[7].Value = agente.Intensidade;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                command.Parameters[8].Value = agente.Tecnica;

                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                command.Parameters[9].Value = agente.CodigoEsocial;
                
                command.ExecuteNonQuery();
                
                return agente;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_AGENTE_ID_AGENTE_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Agente findByDescricao(String str, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByDescricao");

            Agente agenteRetorno = null;

            StringBuilder query = new StringBuilder();
            
            try
            {

                query.Append(" SELECT AGENTE.ID_AGENTE, AGENTE.ID_RISCO, AGENTE.DESCRICAO, AGENTE.TRAJETORIA, AGENTE.DANOS, AGENTE.LIMITE, AGENTE.SITUACAO, RISCO.DESCRICAO, AGENTE.PRIVADO, AGENTE.INTENSIDADE, AGENTE.TECNICA, AGENTE.CODIGO_ESOCIAL ");
                query.Append(" FROM SEG_AGENTE AGENTE JOIN SEG_RISCO RISCO ON RISCO.ID_RISCO = AGENTE.ID_RISCO WHERE ");
                query.Append(" AGENTE.DESCRICAO = :VALUE1 AND AGENTE.SITUACAO = 'A' ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = str.ToUpper();


                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    agenteRetorno = new Agente(dr.GetInt64(0), new Risco(dr.GetInt64(1), dr.GetString(7)), dr.GetString(2), dr.GetString(3), 
                        dr.GetString(4), dr.GetString(5), dr.GetString(6), dr.GetBoolean(8), dr.IsDBNull(9) ? string.Empty : dr.GetString(9), dr.IsDBNull(10) ? string.Empty : dr.GetString(10), dr.IsDBNull(11) ? string.Empty : dr.GetString(11));
                }

                return agenteRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByDescricao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Agente findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            Agente agenteRetorno = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT AGENTE.ID_AGENTE, AGENTE.ID_RISCO, AGENTE.DESCRICAO, AGENTE.TRAJETORIA, AGENTE.DANOS, AGENTE.LIMITE, AGENTE.SITUACAO, RISCO.DESCRICAO, AGENTE.PRIVADO, AGENTE.INTENSIDADE, AGENTE.TECNICA, AGENTE.CODIGO_ESOCIAL");
                query.Append(" FROM SEG_AGENTE AGENTE JOIN SEG_RISCO RISCO ON RISCO.ID_RISCO = AGENTE.ID_RISCO WHERE ");
                query.Append(" AGENTE.ID_AGENTE = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    agenteRetorno = new Agente(dr.GetInt64(0), new Risco(dr.GetInt64(1), dr.GetString(7)), dr.GetString(2), dr.GetString(3), dr.GetString(4),
                        dr.GetString(5), dr.GetString(6), dr.GetBoolean(8), dr.IsDBNull(9) ? string.Empty : dr.GetString(9), dr.IsDBNull(10) ? string.Empty : dr.GetString(10), dr.IsDBNull(11) ? string.Empty : dr.GetString(11));

                }
                
                return agenteRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Agente update(Agente agente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateAgente");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_AGENTE SET DESCRICAO = :VALUE2, ID_RISCO = :VALUE3, TRAJETORIA = :VALUE4, ");
                query.Append(" DANOS = :VALUE5, LIMITE = :VALUE6, SITUACAO = :VALUE7, INTENSIDADE = :VALUE8, TECNICA = :VALUE9, CODIGO_ESOCIAL = :VALUE10 WHERE ID_AGENTE = :VALUE1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = agente.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = agente.Descricao;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = agente.Risco.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = agente.Trajetoria;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = agente.Dano;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = agente.Limite;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = agente.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Varchar));
                command.Parameters[7].Value = agente.Intensidade;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                command.Parameters[8].Value = agente.Tecnica;

                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                command.Parameters[9].Value = agente.CodigoEsocial;

                command.ExecuteNonQuery();

                return agente;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAllNotInGheFonte(Agente agente, GheFonte gheFonte, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllNotInGheFonte");

            try
            {
                StringBuilder query = new StringBuilder();

                if (String.IsNullOrEmpty(agente.Descricao))
                {
                    query.Append(" SELECT AGENTE.ID_AGENTE, AGENTE.ID_RISCO, AGENTE.DESCRICAO, AGENTE.TRAJETORIA, AGENTE.DANOS, AGENTE.LIMITE, AGENTE.SITUACAO, RISCO.DESCRICAO, FALSE ");
                    query.Append(" FROM SEG_AGENTE AGENTE JOIN SEG_RISCO RISCO ON RISCO.ID_RISCO = AGENTE.ID_RISCO WHERE AGENTE.SITUACAO = 'A' AND ");
                    query.Append(" AGENTE.ID_AGENTE NOT IN (SELECT ID_AGENTE FROM SEG_GHE_FONTE GHE_FONTE, SEG_GHE_FONTE_AGENTE GHE_FONTE_AGENTE WHERE GHE_FONTE_AGENTE.ID_GHE_FONTE = GHE_FONTE.ID_GHE_FONTE AND GHE_FONTE.ID_GHE_FONTE = :VALUE1 AND GHE_FONTE.SITUACAO = 'A' AND GHE_FONTE_AGENTE.SITUACAO = 'A') ");
                    query.Append(" ORDER BY RISCO.ID_RISCO ");
                }
                else
                {
                    query.Append(" SELECT AGENTE.ID_AGENTE, AGENTE.ID_RISCO, AGENTE.DESCRICAO, AGENTE.TRAJETORIA, AGENTE.DANOS, AGENTE.LIMITE, AGENTE.SITUACAO, RISCO.DESCRICAO, FALSE ");
                    query.Append(" FROM SEG_AGENTE AGENTE JOIN SEG_RISCO RISCO ON RISCO.ID_RISCO = AGENTE.ID_RISCO WHERE AGENTE.SITUACAO = 'A' AND ");
                    query.Append(" AGENTE.ID_AGENTE NOT IN (SELECT ID_AGENTE FROM SEG_GHE_FONTE GHE_FONTE, SEG_GHE_FONTE_AGENTE GHE_FONTE_AGENTE WHERE GHE_FONTE_AGENTE.ID_GHE_FONTE = GHE_FONTE.ID_GHE_FONTE AND GHE_FONTE.ID_GHE_FONTE = :VALUE1 AND GHE_FONTE.SITUACAO = 'A' AND GHE_FONTE_AGENTE.SITUACAO = 'A') ");
                    query.Append(" AND AGENTE.DESCRICAO LIKE :VALUE2 ORDER BY RISCO.ID_RISCO ");
                }
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = gheFonte.Id;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = agente.Descricao + "%";

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Agentes");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllNotInGheFonte", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

    }
}

