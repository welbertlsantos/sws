﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using Npgsql;
using NpgsqlTypes;
using SWS.Excecao;
using SWS.Helper;
using System.Data;

namespace SWS.Dao
{
    class UsuarioPerfilDao : IUsuarioPerfilDao
    {
        public UsuarioPerfil incluir(UsuarioPerfil usuarioPerfil, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluir");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" INSERT INTO SEG_USUARIO_PERFIL ( ID_USUARIO, ID_PERFIL, DATA_INICIO ) VALUES(:VALUE1, :VALUE2, :VALUE3)");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = usuarioPerfil.Usuario.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = usuarioPerfil.Perfil.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Date));
                command.Parameters[2].Value = usuarioPerfil.DataInicio;

                command.ExecuteNonQuery();

                return usuarioPerfil;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluir", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void excluir(UsuarioPerfil usuarioPerfil, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método excluir");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("DELETE FROM SEG_USUARIO_PERFIL WHERE ID_USUARIO = :VALUE1 AND ID_PERFIL = :VALUE2", dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = usuarioPerfil.Usuario.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = usuarioPerfil.Perfil.Id;
                
                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - excluir", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void excluirPerfisDeUmUsuario(Int64? idUsuario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método excluirPerfisDeUmUsuario");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("DELETE FROM SEG_USUARIO_PERFIL WHERE ID_USUARIO = :VALUE1", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = idUsuario;
                
                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - excluirPerfisDeUmUsuario", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAllPerfilByUsuario(Usuario usuario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + "findAllPerfilByUsuario");

            try
            {

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter("SELECT USUARIO_PERFIL.ID_PERFIL, USUARIO_PERFIL.ID_USUARIO, USUARIO_PERFIL.DATA_INICIO, PERFIL.DESCRICAO FROM SEG_USUARIO_PERFIL USUARIO_PERFIL JOIN SEG_PERFIL PERFIL ON PERFIL.ID_PERFIL = USUARIO_PERFIL.ID_PERFIL WHERE ID_USUARIO = :VALUE1 ORDER BY DESCRICAO ASC", dbConnection);
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                adapter.SelectCommand.Parameters[0].Value = usuario.Id;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Perfis");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + "findAllPerfilByUsuario", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
