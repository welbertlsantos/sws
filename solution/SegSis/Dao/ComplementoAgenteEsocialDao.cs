﻿using Npgsql;
using NpgsqlTypes;
using SWS.Entidade;
using SWS.Excecao;
using SWS.Helper;
using SWS.IDao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Dao
{
    class ComplementoAgenteEsocialDao : IComplementoAgenteEsocialDao
    {
        public List<ComplementoAgenteEsocial> findAllByFilter(ComplementoAgenteEsocial complementoAgenteEsocial, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByFilter");

            List<ComplementoAgenteEsocial> complementoAgenteEsocialList = new List<ComplementoAgenteEsocial>();

            StringBuilder query = new StringBuilder();

            try
            {
                if (string.IsNullOrEmpty(complementoAgenteEsocial.Descricao))
                {
                    query.Append(" SELECT ID, CODIGO, DESCRICAO ");
                    query.Append(" FROM SEG_ESOCIAL_24 ");
                    query.Append(" ORDER BY DESCRICAO ASC");
                }
                else
                {
                    query.Append(" SELECT ID, CODIGO, DESCRICAO ");
                    query.Append(" FROM SEG_ESOCIAL_24 ");
                    query.Append(" WHERE DESCRICAO LIKE :VALUE1 ");
                    query.Append(" ORDER BY DESCRICAO ASC");

                }

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = "%" + complementoAgenteEsocial.Descricao + "%";

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    complementoAgenteEsocialList.Add(new ComplementoAgenteEsocial(dr.GetInt64(0), dr.GetString(1), dr.GetString(2)));
                }

                return complementoAgenteEsocialList;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

    }
}
