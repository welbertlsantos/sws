﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;
using System.Data;
using NpgsqlTypes;

namespace SWS.Dao
{
    class ConfiguracaoDao : IConfiguracaoDao
    {
        public Dictionary<String, String> findAllConfiguracao(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllConfiguracao");

            StringBuilder query = new StringBuilder();

            Dictionary<String, String> mapaConfiguracao = new Dictionary<string, string>();

            try
            {
                query.Append(" SELECT * FROM SEG_CONFIGURACAO");
                query.Append(" ORDER BY ID_CONFIGURACAO ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    mapaConfiguracao.Add(dr.GetString(1), dr.GetString(2));
                }

                return mapaConfiguracao;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllConfiguracao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Configuracao update(Configuracao configuracao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_CONFIGURACAO SET ");
                query.Append(" VALOR = :VALUE2 ");
                query.Append(" WHERE ID_CONFIGURACAO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = configuracao.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = configuracao.Valor;

                command.ExecuteNonQuery();

                return configuracao;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<Configuracao> findAll(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAll");

            try
            {
                StringBuilder query = new StringBuilder();

                List<Configuracao> configuracoes = new List<Configuracao>();

                query.Append(" SELECT ID_CONFIGURACAO, DESCRICAO, VALOR FROM SEG_CONFIGURACAO ORDER BY ID_CONFIGURACAO ASC ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    configuracoes.Add(new Configuracao(dr.GetInt64(0), dr.GetString(1), dr.GetString(2)));
                }

                return configuracoes;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAll", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
