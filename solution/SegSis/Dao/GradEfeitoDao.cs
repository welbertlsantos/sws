﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Dao;
using SWS.Entidade;
using SWS.View.Properties;
using System.Data;
using Npgsql;
using NpgsqlTypes;
using SWS.Excecao;
using SWS.IDao;
using SWS.Helper;

namespace SWS.Dao
{
    class GradEfeitoDao : IGradEfeitoDao
    {
        public DataSet findAllGradEfeito(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllGradEfeito");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_GRAD_EFEITO, DESCRICAO, CATEGORIA, VALOR FROM SEG_GRAD_EFEITO ORDER BY ID_GRAD_EFEITO");
                
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                DataSet ds = new DataSet();
                adapter.Fill(ds, "Efeitos");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllGradExposicao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public GradEfeito findGradEfeitoByGradEfeito(GradEfeito gradEfeito, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGradEfeitoByGradEfeito");

            GradEfeito gradEfeitoRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" select id_grad_efeito, descricao, categoria, valor from seg_grad_efeito where id_grad_efeito = :value1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                if (gradEfeito != null)
                {
                    command.Parameters[0].Value = gradEfeito.Id;
                }
                else
                {
                    command.Parameters[0].Value = null;
                }

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gradEfeitoRetorno = new GradEfeito(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetInt64(3));

                }

                return gradEfeitoRetorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGradEfeitoByGradEefeito", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}