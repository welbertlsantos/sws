﻿using SWS.Entidade;
using Npgsql;
using SWS.Excecao;
using System;
using NpgsqlTypes;
using System.Data;
using System.Text;
using SWS.IDao;
using SWS.View.Resources;
using SWS.Helper;

namespace SWS.Dao
{
    class SetorDao : ISetorDao 
    {

        public Setor insert(Setor setor, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirSetor");

            StringBuilder query = new StringBuilder();

            try
            {
                setor.Id = recuperaProximoId(dbConnection);

                query.Append(" INSERT INTO SEG_SETOR (ID_SETOR, DESCRICAO, SITUACAO ) VALUES ( :VALUE1, :VALUE2, :VALUE3 ) ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));

                command.Parameters[0].Value = setor.Id;
                command.Parameters[1].Value = setor.Descricao;
                command.Parameters[2].Value = setor.Situacao;

                command.ExecuteNonQuery();

                return setor;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirSetor", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Setor update(Setor setor, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateSetor");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("UPDATE SEG_SETOR SET DESCRICAO = :VALUE2, SITUACAO = :VALUE3 WHERE ID_SETOR = :VALUE1", dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));

                command.Parameters[0].Value = setor.Id;
                command.Parameters[1].Value = setor.Descricao;
                command.Parameters[2].Value = setor.Situacao;
                
                command.ExecuteNonQuery();

                return setor;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateSetor", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_SETOR_ID_SETOR_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Setor findSetorByDescricao(String descricao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findSetorByDescricao");

            Setor setorRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append("SELECT ID_SETOR, DESCRICAO, SITUACAO FROM SEG_SETOR WHERE DESCRICAO = :VALUE1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));

                command.Parameters[0].Value = descricao;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    setorRetorno = new Setor(dr.GetInt64(0), dr.GetString(1), dr.GetString(2));
                }

                return setorRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findSetorByDescricao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findSetorByFilter(Setor setor, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findSetorByFilter");

            try
            {
                StringBuilder query = new StringBuilder();

                if (setor.isNullForFilter())
                    query.Append(" SELECT ID_SETOR, DESCRICAO, SITUACAO FROM SEG_SETOR ORDER BY DESCRICAO ASC ");
                else
                {
                    query.Append(" SELECT ID_SETOR,DESCRICAO,SITUACAO FROM SEG_SETOR WHERE TRUE ");

                    if (!String.IsNullOrWhiteSpace(setor.Descricao))
                        query.Append(" AND DESCRICAO LIKE :VALUE1 ");

                    if (!String.IsNullOrWhiteSpace(setor.Situacao))
                        query.Append(" AND SITUACAO = :VALUE2 ");

                    query.Append(" ORDER BY DESCRICAO ASC");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[0].Value = "%" + setor.Descricao.ToUpper() + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[1].Value = setor.Situacao;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Setores");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findSetorByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Setor findSetorById(Int64 idSetor, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findSetorById");

            Setor setorRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_SETOR, DESCRICAO, SITUACAO FROM SEG_SETOR WHERE ID_SETOR = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = idSetor;
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    setorRetorno = new Setor(dr.GetInt64(0), dr.GetString(1), dr.GetString(2));

                }
                return setorRetorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findSetorById", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public DataSet findAllSetorAtivo(Setor setor, Ghe ghe, ClienteFuncao clienteFuncao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllSetorAtivo");

            StringBuilder query = new StringBuilder();

            try
            {
                if (ghe == null)
                {
                    if (String.IsNullOrWhiteSpace(setor.Descricao))
                    {
                        query.Append(" SELECT ID_SETOR, DESCRICAO, SITUACAO, FALSE FROM SEG_SETOR WHERE ");
                        query.Append(" SITUACAO = :VALUE1 AND ID_SETOR NOT IN (SELECT ID_SETOR FROM SEG_CLIENTE_FUNCAO_SETOR WHERE ID_CLIENTE_FUNCAO = :VALUE4 AND SITUACAO = :VALUE5 ) ");
                        query.Append(" ORDER BY DESCRICAO ");
                    }
                    else
                    {
                        query.Append(" SELECT ID_SETOR, DESCRICAO, SITUACAO, FALSE FROM SEG_SETOR WHERE ");
                        query.Append(" SITUACAO = :VALUE1 AND ID_SETOR NOT IN (SELECT ID_SETOR FROM SEG_CLIENTE_FUNCAO_SETOR WHERE ID_CLIENTE_FUNCAO = :VALUE4 AND SITUACAO = :VALUE5 ) ");
                        query.Append(" AND DESCRICAO LIKE :VALUE2 ");
                        query.Append(" ORDER BY DESCRICAO ");
                    }
                }
                else
                {

                    if (String.IsNullOrWhiteSpace(setor.Descricao))
                    {
                        query.Append(" SELECT ID_SETOR, DESCRICAO, SITUACAO, FALSE FROM SEG_SETOR WHERE ");
                        query.Append(" SITUACAO = :VALUE1 AND ID_SETOR NOT IN (SELECT ID_SETOR FROM SEG_GHE_SETOR WHERE ");
                        query.Append(" ID_GHE = :VALUE3 AND SITUACAO = :VALUE5) ORDER BY DESCRICAO ");
                    }
                    else
                    {
                        query.Append(" SELECT ID_SETOR, DESCRICAO, SITUACAO, FALSE FROM SEG_SETOR WHERE ");
                        query.Append(" SITUACAO = :VALUE1 AND ID_SETOR NOT IN (SELECT ID_SETOR FROM SEG_GHE_SETOR WHERE ");
                        query.Append(" ID_GHE = :VALUE3 AND SITUACAO = :VALUE5) AND DESCRICAO LIKE :VALUE2 ORDER BY DESCRICAO ");
                    }
                }
                
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = ApplicationConstants.ATIVO;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = "%" + setor.Descricao + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[2].Value = ghe != null ? ghe.Id : null;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[3].Value = clienteFuncao != null ? clienteFuncao.Id : null;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[4].Value = ApplicationConstants.ATIVO;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Setores");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllSetorAtivo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
