﻿using SWS.Entidade;
using Npgsql;
using SWS.Excecao;
using System;
using NpgsqlTypes;
using System.Data;
using System.Text;
using SWS.IDao;
using System.Collections.Generic;
using SWS.View.Resources;
using SWS.Helper;

namespace SWS.Dao
{
    class ClienteCnaeDao : IClienteCnaeDao
    {

        public ClienteCnae incluirClienteCnae(ClienteCnae clienteCnae, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirClienteCnae");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" INSERT INTO SEG_CLI_CNAE (ID_CLIENTE, ID_CNAE, FLAG_PR) VALUES (:VALUE1, :VALUE2, :VALUE3) ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteCnae.Cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = clienteCnae.Cnae.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Boolean));
                command.Parameters[2].Value = clienteCnae.Flag_pr;
                
                command.ExecuteNonQuery();

                return clienteCnae;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirClienteCnae", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void updateClienteCnae(ClienteCnae clienteCnae, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirCnaePrincipal");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" UPDATE SEG_CLI_CNAE SET FLAG_PR = :VALUE1 WHERE ID_CNAE = :VALUE2 AND ID_CLIENTE = :VALUE3 ", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Boolean));
                command.Parameters[0].Value = clienteCnae.Flag_pr;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = clienteCnae.Cnae.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = clienteCnae.Cliente.Id;
                
                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirCnaePrincipal", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<ClienteCnae> findClienteCnaeByCliente(Cliente cliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteCnaeByCliente");

            List<ClienteCnae> clienteCnae = new List<ClienteCnae>();

            StringBuilder query = new StringBuilder();
            
            try
            {
                query.Append(" SELECT CNAE.ID_CNAE, CNAE.COD_CNAE, CNAE.ATIVIDADE, CNAE.GRAURISCO, CNAE.GRUPO, CNAE.SITUACAO, CLI_CNAE.FLAG_PR ");
                query.Append(" FROM SEG_CNAE CNAE ");
                query.Append(" JOIN SEG_CLI_CNAE CLI_CNAE ON CLI_CNAE.ID_CNAE = CNAE.ID_CNAE ");
                query.Append(" WHERE CLI_CNAE.ID_CLIENTE = :VALUE1 ");
                query.Append(" ORDER BY COD_CNAE ASC ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    clienteCnae.Add(new ClienteCnae(cliente, new Cnae(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.GetString(4), dr.GetString(5)), dr.GetBoolean(6)));
                }

                return clienteCnae;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteCnaeByCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void deleteClienteCnae(Int64 idCliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteClienteCnae");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("DELETE FROM SEG_CLI_CNAE WHERE ID_CLIENTE = :VALUE1", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = idCliente;
                
                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteClienteCnae", ex);
                throw new DataBaseConectionException(ex.Message);
            }  
        }

        public DataSet findAllCnaeByCliente(Cliente cliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllCnaeByCliente");

            StringBuilder query = new StringBuilder();
            
            try
            {
                query.Append(" SELECT ID_CNAE, COD_CNAE CODIGO, ATIVIDADE ATIVIDADE, GRAURISCO GRAU_DE_RISCO, GRUPO GRUPO_CIPA, SITUACAO SITUACAO  ");
                query.Append(" FROM SEG_CNAE WHERE ID_CNAE IN ");
                query.Append(" (SELECT ID_CNAE FROM SEG_CLI_CNAE WHERE ID_CLIENTE = :VALUE1)");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                
                adapter.SelectCommand.Parameters[0].Value = cliente.Id;
                
                DataSet ds = new DataSet();
                adapter.Fill(ds, "Cnaes");
                return ds;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllCnaeByCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Cnae findCnaeById(Int64 idCnae, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findCnaeById");

            Cnae cnaeRetorno = null;

            try
            {
                
                NpgsqlCommand command = new NpgsqlCommand("SELECT ID_CNAE FROM SEG_CLI_CNAE WHERE ID_CNAE = :VALUE1", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = idCnae;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    cnaeRetorno = new Cnae(dr.GetInt64(0));
                }

                return cnaeRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findCnaeById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Cnae findCnaePrincipalCliente(Int64 idCliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findCnaePrincipalCliente");

            Cnae cnaeRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_CNAE, COD_CNAE, ATIVIDADE, GRAURISCO, GRUPO, SITUACAO FROM SEG_CNAE ");
                query.Append(" WHERE ID_CNAE IN (SELECT ID_CNAE FROM SEG_CLI_CNAE WHERE ID_CLIENTE = :VALUE1 ");
                query.Append(" AND FLAG_PR = TRUE)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = idCliente;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    cnaeRetorno = new Cnae(dr.GetInt64(0), dr.GetString(1), dr.GetString(2),dr.GetString(3),dr.GetString(4), dr.GetString(5));
                }

                return cnaeRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findCnaePrincipalCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public HashSet<ClienteCnae> findAllCnaeClienteByClienteCnae(Cliente cliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllCnaeClienteByClienteCnae");

            HashSet<ClienteCnae> listaClienteCnae = new HashSet<ClienteCnae>();
            
            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT CLIENTE_CNAE.ID_CLIENTE, CLIENTE_CNAE.ID_CNAE, CNAE.COD_CNAE, CNAE.ATIVIDADE, CNAE.GRAURISCO,");
                query.Append(" CNAE.GRUPO, CLIENTE_CNAE.FLAG_PR, CNAE.SITUACAO ");
                query.Append(" FROM SEG_CLI_CNAE CLIENTE_CNAE ");
                query.Append(" JOIN SEG_CNAE CNAE ON CNAE.ID_CNAE = CLIENTE_CNAE.ID_CNAE ");
                query.Append(" WHERE CLIENTE_CNAE.ID_CLIENTE = :VALUE1");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    listaClienteCnae.Add(new ClienteCnae(cliente, new Cnae(dr.GetInt64(1), dr.GetString(2), dr.GetString(3), dr.GetString(4),dr.GetString(5), dr.GetString(7)), dr.GetBoolean(6)));
                }

                return listaClienteCnae;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllCnaeClienteByClienteCnae", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Cnae findCnaePrincipalByCliente(Cliente cliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findCnaePrincipalByCliente");

            Cnae cnaeRetorno = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT CLIENTE_CNAE.ID_CNAE, CNAE.COD_CNAE, CNAE.ATIVIDADE, CNAE.GRAURISCO, CNAE.GRUPO, CNAE.SITUACAO ");
                query.Append(" FROM SEG_CLI_CNAE CLIENTE_CNAE ");
                query.Append(" JOIN SEG_CNAE CNAE ON CNAE.ID_CNAE = CLIENTE_CNAE.ID_CNAE ");
                query.Append(" WHERE CLIENTE_CNAE.ID_CLIENTE = :VALUE1 AND CLIENTE_CNAE.FLAG_PR = TRUE");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    cnaeRetorno = new Cnae(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.GetString(4), dr.GetString(5));
                }

                return cnaeRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findCnaePrincipalByCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public void deleteClienteCnaeInCliente(ClienteCnae clienteCnae, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteClienteCnaeInCliente");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" DELETE FROM SEG_CLI_CNAE WHERE ID_CLIENTE = :VALUE1 AND ID_CNAE = :VALUE2 ", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));

                command.Parameters[0].Value = clienteCnae.Cliente.Id;
                command.Parameters[1].Value = clienteCnae.Cnae.Id;

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteClienteCnaeInCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }


    }
}