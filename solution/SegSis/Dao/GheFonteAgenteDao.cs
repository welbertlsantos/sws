﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using System.Data;
using SWS.Entidade;
using Npgsql;
using SWS.Excecao;
using NpgsqlTypes;
using SWS.Facade;
using SWS.View.Resources;
using SWS.Helper;

namespace SWS.Dao
{
    class GheFonteAgenteDao : IGheFonteAgenteDao
    {
        public GheFonteAgente insert(GheFonteAgente gheFonteAgente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            try
            {
                StringBuilder query = new StringBuilder();
                
                gheFonteAgente.Id = (recuperaProximoId(dbConnection));

                query.Append(" INSERT INTO SEG_GHE_FONTE_AGENTE (ID_GHE_FONTE_AGENTE, ID_GRAD_SOMA, ID_GRAD_EFEITO, ");
                query.Append(" ID_GRAD_EXPOSICAO, ID_GHE_FONTE, ID_AGENTE, TEMPO_EXPOSICAO, SITUACAO ) " );
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6, :VALUE7, 'A') ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);


                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonteAgente.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = gheFonteAgente.GradSoma != null ? gheFonteAgente.GradSoma.Id : (Int64?)null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = gheFonteAgente.GradEfeito != null ? gheFonteAgente.GradEfeito.Id : (Int64?)null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = gheFonteAgente.GradExposicao != null ? gheFonteAgente.GradExposicao.Id : (Int64?)null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Integer));
                command.Parameters[4].Value = gheFonteAgente.GheFonte.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Integer));
                command.Parameters[5].Value = gheFonteAgente.Agente.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = gheFonteAgente.TempoExposicao;
                
                command.ExecuteNonQuery();

                return gheFonteAgente;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_GHE_FONTE_AGENTE_ID_GHE_FONTE_AGENTE_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoIdGheFonteAgente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void delete(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteGheFonteAgenteById");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" DELETE FROM SEG_GHE_FONTE_AGENTE WHERE ID_GHE_FONTE_AGENTE = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteGheFonteAgenteById", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public GheFonteAgente findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            GheFonteAgente gheFonteAgente = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE_FONTE_AGENTE.ID_GHE_FONTE_AGENTE, GHE_FONTE_AGENTE.ID_GRAD_SOMA, GHE_FONTE_AGENTE.ID_GRAD_EFEITO, GHE_FONTE_AGENTE.ID_GRAD_EXPOSICAO, ");
                query.Append(" GHE_FONTE_AGENTE.ID_GHE_FONTE, GHE_FONTE_AGENTE.ID_AGENTE, FONTE.ID_FONTE, FONTE.DESCRICAO, FONTE.SITUACAO, FONTE.PRIVADO, GHE.ID_GHE, GHE.ID_ESTUDO, GHE.DESCRICAO, GHE.NEXP, GHE.SITUACAO, GHE_FONTE.SITUACAO, GHE_FONTE_AGENTE.TEMPO_EXPOSICAO, GHE_FONTE_AGENTE.SITUACAO, GHE.NUMERO_PO ");
                query.Append(" FROM SEG_GHE_FONTE_AGENTE GHE_FONTE_AGENTE ");
                query.Append(" JOIN SEG_GHE_FONTE GHE_FONTE ON GHE_FONTE.ID_GHE_FONTE = GHE_FONTE_AGENTE.ID_GHE_FONTE ");
                query.Append(" JOIN SEG_FONTE FONTE ON FONTE.ID_FONTE = GHE_FONTE.ID_FONTE ");
                query.Append(" JOIN SEG_GHE GHE ON GHE.ID_GHE = GHE_FONTE.ID_GHE ");
                query.Append(" WHERE GHE_FONTE_AGENTE.ID_GHE_FONTE_AGENTE = :VALUE1  ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gheFonteAgente = new GheFonteAgente(dr.GetInt64(0), dr.IsDBNull(1) ? null : new GradSoma(dr.GetInt64(1), 0, String.Empty, String.Empty),  dr.IsDBNull(2) ? null : new GradEfeito(dr.GetInt64(2), String.Empty, String.Empty, 0), dr.IsDBNull(3) ? null : new GradExposicao(dr.GetInt64(3), String.Empty, String.Empty, 0),  dr.IsDBNull(4) ? null : new GheFonte(dr.GetInt64(4), new Fonte(dr.GetInt64(6), dr.GetString(7), dr.GetString(8), dr.GetBoolean(9)), new Ghe(dr.GetInt64(10), new Estudo(dr.GetInt64(11)), dr.GetString(12), dr.GetInt32(13), dr.IsDBNull(14) ? String.Empty : dr.GetString(14), dr.IsDBNull(18) ? string.Empty : dr.GetString(18)), false, dr.IsDBNull(15) ? String.Empty : dr.GetString(15) ), dr.IsDBNull(5) ? null : new Agente(dr.GetInt64(5), null, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, false, string.Empty, string.Empty, string.Empty), dr.IsDBNull(16) ? string.Empty : dr.GetString(16), dr.IsDBNull(17) ? string.Empty : dr.GetString(17));
                }

                return gheFonteAgente;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public HashSet<GheFonteAgente> findAtivosByGheFonteHash(GheFonte gheFonte, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByGheFonteHash");
            
            HashSet<GheFonteAgente> gheFonteAgente = new HashSet<GheFonteAgente>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE_FONTE_AGENTE.ID_GHE_FONTE_AGENTE, GHE_FONTE_AGENTE.ID_GRAD_SOMA, GHE_FONTE_AGENTE.ID_GRAD_EFEITO, GHE_FONTE_AGENTE.ID_GRAD_EXPOSICAO, ");
                query.Append(" GHE_FONTE_AGENTE.ID_GHE_FONTE, GHE_FONTE_AGENTE.ID_AGENTE, AGENTE.DESCRICAO AS AGENTE, GHE.ID_GHE, ");
                query.Append(" GHE.DESCRICAO AS GHE, FONTE.ID_FONTE, FONTE.DESCRICAO AS FONTE, RISCO.ID_RISCO, RISCO.DESCRICAO AS RISCO, GHE_FONTE_AGENTE.TEMPO_EXPOSICAO, ");
                query.Append(" GRAD_SOMA.DESCRICAO AS SOMA, GRAD_SOMA.FAIXA SOMA_FAIXA, GRAD_SOMA.MED_CONTROLE AS SOMA_MEDICA_CONTROLE,");
                query.Append(" GRAD_EFEITO.DESCRICAO AS EFEITO, GRAD_EFEITO.VALOR AS EFEITO_VALOR, GRAD_EFEITO.CATEGORIA AS EFEITO_CATEGORIA,");
                query.Append(" GRAD_EXPOSICAO.DESCRICAO AS EXPOSICAO, GRAD_EXPOSICAO.CATEGORIA AS EXPOSICAO_CATEGORIA, GRAD_EXPOSICAO.VALOR AS EXPOSICAO_VALOR,");
                query.Append(" FONTE.SITUACAO AS FONTE_SITUACAO, FONTE.PRIVADO AS FONTE_PRIVADO, ");
                query.Append(" GHE.NEXP, ");
                query.Append(" AGENTE.TRAJETORIA, AGENTE.DANOS, AGENTE.LIMITE, AGENTE.SITUACAO AS AGENTE_SITUACAO, AGENTE.PRIVADO AS AGENTE_PRIVADO, AGENTE.INTENSIDADE, AGENTE.TECNICA, GHE_FONTE_AGENTE.SITUACAO, GHE.NUMERO_PO, AGENTE.CODIGO_ESOCIAL  ");
                query.Append(" FROM SEG_GHE_FONTE_AGENTE GHE_FONTE_AGENTE");
                query.Append(" JOIN SEG_AGENTE AGENTE ON AGENTE.ID_AGENTE = GHE_FONTE_AGENTE.ID_AGENTE ");
                query.Append(" JOIN SEG_GHE_FONTE GHE_FONTE ON GHE_FONTE.ID_GHE_FONTE = GHE_FONTE_AGENTE.ID_GHE_FONTE");
                query.Append(" JOIN SEG_GHE GHE ON GHE.ID_GHE = GHE_FONTE.ID_GHE");
                query.Append(" JOIN SEG_FONTE FONTE ON FONTE.ID_FONTE = GHE_FONTE.ID_FONTE ");
                query.Append(" JOIN SEG_RISCO RISCO ON RISCO.ID_RISCO = AGENTE.ID_RISCO ");
                query.Append(" LEFT JOIN SEG_GRAD_SOMA GRAD_SOMA ON GRAD_SOMA.ID_GRAD_SOMA = GHE_FONTE_AGENTE.ID_GRAD_SOMA");
                query.Append(" LEFT JOIN SEG_GRAD_EXPOSICAO GRAD_EXPOSICAO ON GRAD_EXPOSICAO.ID_GRAD_EXPOSICAO = GHE_FONTE_AGENTE.ID_GRAD_EXPOSICAO");
                query.Append(" LEFT JOIN SEG_GRAD_EFEITO GRAD_EFEITO ON GRAD_EFEITO.ID_GRAD_EFEITO = GHE_FONTE_AGENTE.ID_GRAD_EFEITO");
                query.Append(" WHERE GHE_FONTE_AGENTE.ID_GHE_FONTE = :VALUE1 ");
                query.Append(" AND GHE_FONTE_AGENTE.SITUACAO = 'A' AND GHE_FONTE.SITUACAO = 'A' ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonte.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gheFonteAgente.Add(new GheFonteAgente(dr.GetInt64(0), dr.IsDBNull(1) ? null : new GradSoma(dr.GetInt64(1), dr.IsDBNull(15) ? 0 : dr.GetInt64(15) , dr.IsDBNull(16) ? String.Empty : dr.GetString(16), dr.IsDBNull(14) ? String.Empty : dr.GetString(14)),  dr.IsDBNull(2) ? null : new GradEfeito(dr.GetInt64(2), dr.IsDBNull(17) ? String.Empty : dr.GetString(17), dr.IsDBNull(19) ? String.Empty : dr.GetString(19), dr.IsDBNull(18) ? 0 : dr.GetInt64(18)),  dr.IsDBNull(3) ? null : new GradExposicao(dr.GetInt64(3), dr.IsDBNull(20) ? String.Empty : dr.GetString(20), dr.IsDBNull(21) ? String.Empty : dr.GetString(21), dr.IsDBNull(22) ? 0 : dr.GetInt64(22)),  dr.IsDBNull(4) ? null : new GheFonte(dr.GetInt64(4), new Fonte(dr.GetInt64(9), dr.GetString(10), dr.GetString(23), dr.GetBoolean(24)), new Ghe(dr.GetInt64(7), null, dr.GetString(8), dr.IsDBNull(25) ? 0 : dr.GetInt32(25), string.Empty, dr.IsDBNull(34) ? string.Empty : dr.GetString(34))), dr.IsDBNull(5) ? null : new Agente(dr.GetInt64(5), new Risco(dr.GetInt64(11),dr.GetString(12)), dr.GetString(6), dr.IsDBNull(26) ? String.Empty : dr.GetString(26), dr.IsDBNull(27) ? String.Empty : dr.GetString(27), dr.IsDBNull(28) ? String.Empty : dr.GetString(28), dr.IsDBNull(29) ? String.Empty : dr.GetString(29), dr.IsDBNull(30) ? false : dr.GetBoolean(30), dr.IsDBNull(31) ? String.Empty : dr.GetString(31), dr.IsDBNull(32) ? string.Empty : dr.GetString(32), dr.IsDBNull(35) ? string.Empty : dr.GetString(35)), dr.IsDBNull(13) ? string.Empty : dr.GetString(13), dr.IsDBNull(33) ? string.Empty : dr.GetString(33))); 
                }

                return gheFonteAgente;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByGheFonteHash", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAllByGheFonte(GheFonte gheFonte, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllAgenteByGheFonte");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE_FONTE_AGENTE.ID_GHE_FONTE_AGENTE, GHE_FONTE_AGENTE.ID_AGENTE, AGENTE.DESCRICAO AS AGENTE, GHE_FONTE_AGENTE.ID_GHE_FONTE, RISCO.DESCRICAO AS RISCO ");
                query.Append(" FROM SEG_GHE_FONTE_AGENTE GHE_FONTE_AGENTE ");
                query.Append(" JOIN SEG_AGENTE AGENTE ON AGENTE.ID_AGENTE = GHE_FONTE_AGENTE.ID_AGENTE ");
                query.Append(" JOIN SEG_RISCO RISCO ON RISCO.ID_RISCO = AGENTE.ID_RISCO ");
                query.Append(" WHERE GHE_FONTE_AGENTE.ID_GHE_FONTE = :VALUE1 AND GHE_FONTE_AGENTE.SITUACAO = 'A' ");
                query.Append(" ORDER BY GHE_FONTE_AGENTE.ID_GHE_FONTE ASC");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = gheFonte.Id;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Agentes");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllFonteByGhe", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAllRiscosByGhe(Ghe ghe, NpgsqlConnection dbConnection)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllRiscosByGhe");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT DISTINCT GHE_FONTE_AGENTE.ID_GHE_FONTE_AGENTE, AGENTE.ID_RISCO, AGENTE.DESCRICAO AS AGENTE, AGENTE.DANOS, AGENTE.LIMITE, RISCO.DESCRICAO");
                query.Append(" FROM SEG_GHE GHE ");
                query.Append(" JOIN SEG_GHE_FONTE GHE_FONTE ON GHE.ID_GHE = GHE_FONTE.ID_GHE AND GHE_FONTE.PRINCIPAL <> TRUE ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE GHE_FONTE_AGENTE ON GHE_FONTE_AGENTE.ID_GHE_FONTE = GHE_FONTE.ID_GHE_FONTE ");
                query.Append(" LEFT JOIN SEG_GHE_FONTE_AGENTE_EPI GHE_FONTE_AGENTE_EPI ON GHE_FONTE_AGENTE_EPI.ID_GHE_FONTE_AGENTE = GHE_FONTE_AGENTE.ID_GHE_FONTE_AGENTE AND GHE_FONTE_AGENTE_EPI.CLASSIFICACAO = 'MC' ");
                query.Append(" JOIN SEG_AGENTE AGENTE ON AGENTE.ID_AGENTE = GHE_FONTE_AGENTE.ID_AGENTE ");
                query.Append(" JOIN SEG_RISCO RISCO ON RISCO.ID_RISCO = AGENTE.ID_RISCO ");
                query.Append(" WHERE GHE.ID_GHE = :VALUE1 ORDER BY AGENTE.ID_RISCO ASC");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = ghe.Id;
                                
                DataSet dsRisco = new DataSet();

                adapter.Fill(dsRisco, "Riscos");

                return dsRisco;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllRiscosByGhe", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean isGheFonteAgenteUsed(GheFonteAgente gheFonteAgente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método isGheFonteAgenteUsed");

            Boolean isGheFonteAgenteUsed = false;
            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT COUNT(*) ");
                query.Append(" FROM SEG_GHE_FONTE_AGENTE_EXAME_ASO GHE_FONTE_AGENTE_EXAME_ASO ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GHE_FONTE_AGENTE_EXAME ON GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME = GHE_FONTE_AGENTE_EXAME_ASO.ID_GHE_FONTE_AGENTE_EXAME ");
                query.Append(" WHERE GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonteAgente.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    if (dr.GetInt64(0) > 0)
                    {
                        isGheFonteAgenteUsed = true;
                    }
                }

                return isGheFonteAgenteUsed;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - isGheFonteAgenteUsed", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public GheFonteAgente update(GheFonteAgente gheFonteAgente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_GHE_FONTE_AGENTE SET ID_GRAD_SOMA = :VALUE2, ID_GRAD_EFEITO = :VALUE3, ID_GRAD_EXPOSICAO = :VALUE4, TEMPO_EXPOSICAO = :VALUE5, SITUACAO = :VALUE6 WHERE ID_GHE_FONTE_AGENTE = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonteAgente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = gheFonteAgente.GradSoma == null ? null : (Int64?)gheFonteAgente.GradSoma.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = gheFonteAgente.GradEfeito == null ? null : (Int64?)gheFonteAgente.GradEfeito.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = gheFonteAgente.GradExposicao == null ? null : (Int64?)gheFonteAgente.GradExposicao.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = gheFonteAgente.TempoExposicao.ToUpper();

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = gheFonteAgente.Situacao;

                command.ExecuteNonQuery();

                return gheFonteAgente;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean isUsedByAgente(Agente agente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método desativaGheFonteAgente");
            
            Boolean isGheFonteAgenteUsedAgente = false;
            
            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT COUNT(*) FROM SEG_GHE_FONTE_AGENTE GHE_FONTE_AGENTE ");
                query.Append(" JOIN SEG_GHE_FONTE GHE_FONTE ON GHE_FONTE.ID_GHE_FONTE = GHE_FONTE_AGENTE.ID_GHE_FONTE ");
                query.Append(" JOIN SEG_GHE GHE ON GHE.ID_GHE = GHE_FONTE.ID_GHE ");
                query.Append(" JOIN SEG_ESTUDO ESTUDO ON ESTUDO.ID_ESTUDO = GHE.ID_ESTUDO AND ESTUDO.AVULSO IS FALSE AND ESTUDO.SITUACAO = 'C' ");
                query.Append(" WHERE GHE_FONTE_AGENTE.ID_AGENTE = :VALUE1 AND GHE_FONTE_AGENTE.SITUACAO = 'A' ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = agente.Id;
          

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    if (dr.GetInt64(0) > 0)
                    {
                        isGheFonteAgenteUsedAgente = true;
                    }
                }

                return isGheFonteAgenteUsedAgente;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheFonteAgenteByAgente", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public GheFonteAgente findByGheFonteAndAgente(GheFonte gheFonte, Agente agente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByGheFonteAndAgente");

            GheFonteAgente gheFonteAgente = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE_FONTE_AGENTE.ID_GHE_FONTE_AGENTE, GHE_FONTE_AGENTE.ID_GRAD_SOMA, GHE_FONTE_AGENTE.ID_GRAD_EFEITO, GHE_FONTE_AGENTE.ID_GRAD_EXPOSICAO, ");
                query.Append(" GHE_FONTE_AGENTE.ID_GHE_FONTE, GHE_FONTE_AGENTE.ID_AGENTE, FONTE.ID_FONTE, FONTE.DESCRICAO, FONTE.SITUACAO, FONTE.PRIVADO, GHE.ID_GHE, GHE.ID_ESTUDO, GHE.DESCRICAO, GHE.NEXP, GHE.SITUACAO, GHE_FONTE.SITUACAO, GHE_FONTE_AGENTE.TEMPO_EXPOSICAO, GHE_FONTE_AGENTE.SITUACAO, GHE.NUMERO_PO");
                query.Append(" FROM SEG_GHE_FONTE_AGENTE GHE_FONTE_AGENTE ");
                query.Append(" JOIN SEG_GHE_FONTE GHE_FONTE ON GHE_FONTE.ID_GHE_FONTE = GHE_FONTE_AGENTE.ID_GHE_FONTE ");
                query.Append(" JOIN SEG_FONTE FONTE ON FONTE.ID_FONTE = GHE_FONTE.ID_FONTE ");
                query.Append(" JOIN SEG_GHE GHE ON GHE.ID_GHE = GHE_FONTE.ID_GHE ");
                query.Append(" WHERE GHE_FONTE_AGENTE.ID_GHE_FONTE = :VALUE1 AND GHE_FONTE_AGENTE.ID_AGENTE = :VALUE2 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonte.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = agente.Id;


                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gheFonteAgente = new GheFonteAgente(dr.GetInt64(0), dr.IsDBNull(1) ? null : new GradSoma(dr.GetInt64(1), 0, String.Empty, String.Empty), dr.IsDBNull(2) ? null : new GradEfeito(dr.GetInt64(2), String.Empty, String.Empty, 0), dr.IsDBNull(3) ? null : new GradExposicao(dr.GetInt64(3), String.Empty, String.Empty, 0), dr.IsDBNull(4) ? null : new GheFonte(dr.GetInt64(4), new Fonte(dr.GetInt64(6), dr.GetString(7), dr.GetString(8), dr.GetBoolean(9)), new Ghe(dr.GetInt64(10), new Estudo(dr.GetInt64(11)), dr.GetString(12), dr.GetInt32(13), dr.IsDBNull(14) ? String.Empty : dr.GetString(14), dr.IsDBNull(18) ? string.Empty : dr.GetString(18)), false, dr.IsDBNull(15) ? String.Empty : dr.GetString(15)), dr.IsDBNull(5) ? null : new Agente(dr.GetInt64(5), null, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, false, string.Empty, string.Empty, string.Empty), dr.IsDBNull(16) ? string.Empty : dr.GetString(16), dr.IsDBNull(17) ? string.Empty : dr.GetString(17));
                }

                return gheFonteAgente;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByGheFonteAndAgente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public bool isUsedInService(GheFonteAgente gheFonteAgente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método idUsedInService");

            bool result = false;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT DISTINCT GHE_FONTE_AGENTE.* FROM SEG_ASO ATENDIMENTO ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GHE_FONTE_AGENTE_EXAME_ASO ON GHE_FONTE_AGENTE_EXAME_ASO.ID_ASO = ATENDIMENTO.ID_ASO ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GHE_FONTE_AGENTE_EXAME ON GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME = GHE_FONTE_AGENTE_EXAME_ASO.ID_GHE_FONTE_AGENTE_EXAME ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE GHE_FONTE_AGENTE ON GHE_FONTE_AGENTE.ID_GHE_FONTE_AGENTE = GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE  ");
                query.Append(" WHERE GHE_FONTE_AGENTE.ID_GHE_FONTE_AGENTE = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonteAgente.Id;


                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    if (dr.GetInt64(0) > 0)
                    {
                        result = true;
                    }
                }

                return result;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - idUsedInService", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public List<GheFonteAgente> findAllByGheFonteList(GheFonte gheFonte, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByGheFonteHash");

            List<GheFonteAgente> gheFonteAgente = new List<GheFonteAgente>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE_FONTE_AGENTE.ID_GHE_FONTE_AGENTE, GHE_FONTE_AGENTE.ID_GRAD_SOMA, GHE_FONTE_AGENTE.ID_GRAD_EFEITO, GHE_FONTE_AGENTE.ID_GRAD_EXPOSICAO, ");
                query.Append(" GHE_FONTE_AGENTE.ID_GHE_FONTE, GHE_FONTE_AGENTE.ID_AGENTE, AGENTE.DESCRICAO AS AGENTE, GHE.ID_GHE, ");
                query.Append(" GHE.DESCRICAO AS GHE, FONTE.ID_FONTE, FONTE.DESCRICAO AS FONTE, RISCO.ID_RISCO, RISCO.DESCRICAO AS RISCO, GHE_FONTE_AGENTE.TEMPO_EXPOSICAO, ");
                query.Append(" GRAD_SOMA.DESCRICAO AS SOMA, GRAD_SOMA.FAIXA SOMA_FAIXA, GRAD_SOMA.MED_CONTROLE AS SOMA_MEDICA_CONTROLE,");
                query.Append(" GRAD_EFEITO.DESCRICAO AS EFEITO, GRAD_EFEITO.VALOR AS EFEITO_VALOR, GRAD_EFEITO.CATEGORIA AS EFEITO_CATEGORIA,");
                query.Append(" GRAD_EXPOSICAO.DESCRICAO AS EXPOSICAO, GRAD_EXPOSICAO.CATEGORIA AS EXPOSICAO_CATEGORIA, GRAD_EXPOSICAO.VALOR AS EXPOSICAO_VALOR,");
                query.Append(" FONTE.SITUACAO AS FONTE_SITUACAO, FONTE.PRIVADO AS FONTE_PRIVADO, ");
                query.Append(" GHE.NEXP, ");
                query.Append(" AGENTE.TRAJETORIA, AGENTE.DANOS, AGENTE.LIMITE, AGENTE.SITUACAO AS AGENTE_SITUACAO, AGENTE.PRIVADO AS AGENTE_PRIVADO, AGENTE.INTENSIDADE, AGENTE.TECNICA, GHE_FONTE_AGENTE.SITUACAO, GHE.NUMERO_PO, AGENTE.CODIGO_ESOCIAL ");
                query.Append(" FROM SEG_GHE_FONTE_AGENTE GHE_FONTE_AGENTE");
                query.Append(" JOIN SEG_AGENTE AGENTE ON AGENTE.ID_AGENTE = GHE_FONTE_AGENTE.ID_AGENTE ");
                query.Append(" JOIN SEG_GHE_FONTE GHE_FONTE ON GHE_FONTE.ID_GHE_FONTE = GHE_FONTE_AGENTE.ID_GHE_FONTE");
                query.Append(" JOIN SEG_GHE GHE ON GHE.ID_GHE = GHE_FONTE.ID_GHE");
                query.Append(" JOIN SEG_FONTE FONTE ON FONTE.ID_FONTE = GHE_FONTE.ID_FONTE ");
                query.Append(" JOIN SEG_RISCO RISCO ON RISCO.ID_RISCO = AGENTE.ID_RISCO ");
                query.Append(" LEFT JOIN SEG_GRAD_SOMA GRAD_SOMA ON GRAD_SOMA.ID_GRAD_SOMA = GHE_FONTE_AGENTE.ID_GRAD_SOMA");
                query.Append(" LEFT JOIN SEG_GRAD_EXPOSICAO GRAD_EXPOSICAO ON GRAD_EXPOSICAO.ID_GRAD_EXPOSICAO = GHE_FONTE_AGENTE.ID_GRAD_EXPOSICAO");
                query.Append(" LEFT JOIN SEG_GRAD_EFEITO GRAD_EFEITO ON GRAD_EFEITO.ID_GRAD_EFEITO = GHE_FONTE_AGENTE.ID_GRAD_EFEITO");
                query.Append(" WHERE GHE_FONTE_AGENTE.ID_GHE_FONTE = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonte.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gheFonteAgente.Add(new GheFonteAgente(dr.GetInt64(0), dr.IsDBNull(1) ? null : new GradSoma(dr.GetInt64(1), dr.IsDBNull(15) ? 0 : dr.GetInt64(15), dr.IsDBNull(16) ? String.Empty : dr.GetString(16), dr.IsDBNull(14) ? String.Empty : dr.GetString(14)), dr.IsDBNull(2) ? null : new GradEfeito(dr.GetInt64(2), dr.IsDBNull(17) ? String.Empty : dr.GetString(17), dr.IsDBNull(19) ? String.Empty : dr.GetString(19), dr.IsDBNull(18) ? 0 : dr.GetInt64(18)), dr.IsDBNull(3) ? null : new GradExposicao(dr.GetInt64(3), dr.IsDBNull(20) ? String.Empty : dr.GetString(20), dr.IsDBNull(21) ? String.Empty : dr.GetString(21), dr.IsDBNull(22) ? 0 : dr.GetInt64(22)), dr.IsDBNull(4) ? null : new GheFonte(dr.GetInt64(4), new Fonte(dr.GetInt64(9), dr.GetString(10), dr.GetString(23), dr.GetBoolean(24)), new Ghe(dr.GetInt64(7), null, dr.GetString(8), dr.IsDBNull(25) ? 0 : dr.GetInt32(25), string.Empty, dr.IsDBNull(34) ? string.Empty : dr.GetString(34))), dr.IsDBNull(5) ? null : new Agente(dr.GetInt64(5), new Risco(dr.GetInt64(11), dr.GetString(12)), dr.GetString(6), dr.IsDBNull(26) ? String.Empty : dr.GetString(26), dr.IsDBNull(27) ? String.Empty : dr.GetString(27), dr.IsDBNull(28) ? String.Empty : dr.GetString(28), dr.IsDBNull(29) ? String.Empty : dr.GetString(29), dr.IsDBNull(30) ? false : dr.GetBoolean(30), dr.IsDBNull(31) ? String.Empty : dr.GetString(31), dr.IsDBNull(32) ? string.Empty : dr.GetString(32), dr.IsDBNull(35) ? string.Empty : dr.GetString(35)), dr.IsDBNull(13) ? string.Empty : dr.GetString(13), dr.IsDBNull(33) ? string.Empty : dr.GetString(33)));
                }

                return gheFonteAgente;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByGheFonteHash", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

    }
}


