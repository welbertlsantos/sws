﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using NpgsqlTypes;
using SWS.View.Resources;
using SWS.Excecao;
using System.Data;
using SWS.IDao;
using SWS.Helper;

namespace SWS.Dao
{
    class MaterialHospitalarDao : IMaterialHospitalarDao
    {
        public MaterialHospitalar insert(MaterialHospitalar materialHospitalar, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            StringBuilder query = new StringBuilder();

            try
            {
                materialHospitalar.Id = recuperaProximoId(dbConnection);

                query.Append(" INSERT INTO SEG_MATERIAL_HOSPITALAR (ID_MATERIAL_HOSPITALAR, DESCRICAO, UNIDADE, SITUACAO ) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, 'A')");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = materialHospitalar.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = materialHospitalar.Descricao;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = materialHospitalar.Unidade;

                command.ExecuteNonQuery();

                return materialHospitalar;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirMaterialHospitalar", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public MaterialHospitalar update(MaterialHospitalar materialHospitalar, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateMaterialHospitalar");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_MATERIAL_HOSPITALAR SET DESCRICAO = :VALUE2, UNIDADE = :VALUE3, SITUACAO = :VALUE4 ");
                query.Append(" WHERE ID_MATERIAL_HOSPITALAR = :VALUE1");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = materialHospitalar.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = materialHospitalar.Descricao;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = materialHospitalar.Unidade.ToUpper();

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = materialHospitalar.Situacao;
               
                command.ExecuteNonQuery();

                return materialHospitalar;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateMaterialHospitalar", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_MATERIAL_HOSPITALAR_ID_MATERIAL_HOSPITALAR_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public MaterialHospitalar findByDescricao(String descricao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMaterialHospitalarByDescricao");

            MaterialHospitalar materialHospitalarRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {

                query.Append(" SELECT ID_MATERIAL_HOSPITALAR, DESCRICAO, UNIDADE, SITUACAO ");
                query.Append(" FROM SEG_MATERIAL_HOSPITALAR WHERE DESCRICAO = :VALUE1");
            
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = descricao;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    materialHospitalarRetorno = new MaterialHospitalar(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3));
                }

                return materialHospitalarRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMaterialHospitalarByDescricao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public MaterialHospitalar findById(Int64 idMaterialHospitalar, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            MaterialHospitalar materialHospitalarRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_MATERIAL_HOSPITALAR, DESCRICAO, UNIDADE, SITUACAO FROM SEG_MATERIAL_HOSPITALAR "); 
                query.Append(" WHERE ID_MATERIAL_HOSPITALAR = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = idMaterialHospitalar;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    materialHospitalarRetorno = new MaterialHospitalar(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3));

                }
                return materialHospitalarRetorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMaterialHospitalarById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findByFilter(MaterialHospitalar materialHospitalar, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByFilter");

            try
            {
                StringBuilder query = new StringBuilder();

                if (materialHospitalar.isNullForFilter())
                {
                    query.Append(" SELECT ID_MATERIAL_HOSPITALAR, DESCRICAO, UNIDADE, SITUACAO ");
                    query.Append(" FROM SEG_MATERIAL_HOSPITALAR");
                }
                else
                {
                    query.Append(" SELECT ID_MATERIAL_HOSPITALAR, DESCRICAO, UNIDADE, SITUACAO ");
                    query.Append(" FROM SEG_MATERIAL_HOSPITALAR WHERE TRUE ");

                    if (!String.IsNullOrWhiteSpace(materialHospitalar.Descricao))
                        query.Append(" AND DESCRICAO LIKE :VALUE1 ");

                    if (!String.IsNullOrWhiteSpace(materialHospitalar.Situacao))
                        query.Append(" AND SITUACAO = :VALUE2 ");

                    if (!String.IsNullOrWhiteSpace(materialHospitalar.Unidade))
                        query.Append(" AND UNIDADE = :VALUE3 ");
                
                }
            
                query.Append(" ORDER BY DESCRICAO ASC ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = materialHospitalar.Descricao + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = materialHospitalar.Situacao;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[2].Value = materialHospitalar.Unidade;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "MateriaisHospitalares");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMaterialHospitalarByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }


        public Boolean findInEstudo(MaterialHospitalar materialHospitalar, NpgsqlConnection dbConnection)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMaterialHospitalarInEstudo");

            StringBuilder query = new StringBuilder();

            try
            {
                Boolean retorno = Convert.ToBoolean(false);

                query.Append(" SELECT ID_ESTUDO, ID_MATERIAL_HOSPITALAR, QUANTIDADE FROM ");
                query.Append(" SEG_MATERIAL_HOSPITALAR_ESTUDO WHERE ID_MATERIAL_HOSPITALAR = :VALUE1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = materialHospitalar.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    retorno = Convert.ToBoolean(true);
                }

                return retorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMaterialHospitalarInEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public List<MaterialHospitalar> findByPcmsoAndMateriaHospitalar(MaterialHospitalar materialHospitalar, Estudo pcmso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllMaterialHospitalarAtivoByPcmso");

            List<MaterialHospitalar> listaMaterialHospitalar = new List<MaterialHospitalar>();
            
            try
            {
                StringBuilder query = new StringBuilder();

                if (String.IsNullOrWhiteSpace(materialHospitalar.Descricao))
                {
                    query.Append(" SELECT ID_MATERIAL_HOSPITALAR, DESCRICAO, UNIDADE, SITUACAO ");
                    query.Append(" FROM SEG_MATERIAL_HOSPITALAR WHERE SITUACAO = 'A' AND ");
                    query.Append(" ID_MATERIAL_HOSPITALAR NOT IN (SELECT ID_MATERIAL_HOSPITALAR ");
                    query.Append(" FROM SEG_MATERIAL_HOSPITALAR_ESTUDO WHERE ID_ESTUDO = :VALUE2 )");

                }
                else
                {
                    query.Append(" SELECT ID_MATERIAL_HOSPITALAR, DESCRICAO, UNIDADE, SITUACAO ");
                    query.Append(" FROM SEG_MATERIAL_HOSPITALAR WHERE SITUACAO = 'A' AND DESCRICAO LIKE :VALUE1 AND ");
                    query.Append(" ID_MATERIAL_HOSPITALAR NOT IN (SELECT ID_MATERIAL_HOSPITALAR ");
                    query.Append(" FROM SEG_MATERIAL_HOSPITALAR_ESTUDO WHERE ID_ESTUDO = :VALUE2 )");
                    
                }

                query.Append(" ORDER BY DESCRICAO ASC ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = materialHospitalar.Descricao + "%";
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = pcmso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    listaMaterialHospitalar.Add(new MaterialHospitalar(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3)));
                
                }
                return listaMaterialHospitalar;
                
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllMaterialHospitalarAtivoByPcmso", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

    }
}
