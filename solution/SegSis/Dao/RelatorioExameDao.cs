﻿using Npgsql;
using NpgsqlTypes;
using SWS.Entidade;
using SWS.Excecao;
using SWS.Helper;
using SWS.IDao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Dao
{
    class RelatorioExameDao : IRelatorioExameDao
    {
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            long proximoId = Convert.ToInt64(0);

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_RELATORIO_EXAME_ID_RELATORIO_EXAME_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public RelatorioExame insert(RelatorioExame relatorioExame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            try
            {
                relatorioExame.IdRelatorioExame = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_RELATORIO_EXAME( ID_RELATORIO_EXAME, ID_EXAME, ID_RELATORIO, SITUACAO, DATA_GRAVACAO )");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, 'A', NOW())");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = relatorioExame.IdRelatorioExame;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = relatorioExame.Exame.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = relatorioExame.Relatorio.Id;

                command.ExecuteNonQuery();

                return relatorioExame;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public RelatorioExame findById(long id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            RelatorioExame relatorioExameReturn = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT RELATORIO_EXAME.ID_RELATORIO_EXAME, RELATORIO_EXAME.SITUACAO, RELATORIO_EXAME.DATA_GRAVACAO, EXAME.ID_EXAME, EXAME.DESCRICAO, EXAME.SITUACAO, EXAME.LABORATORIO, EXAME.PRECO, EXAME.PRIORIDADE, EXAME.CUSTO, EXAME.EXTERNO, EXAME.LIBERA_DOCUMENTO, EXAME.CODIGO_TUSS, EXAME.EXAME_COMPLEMENTAR,");
                query.Append(" RELATORIO.ID_RELATORIO, RELATORIO.NOME_RELATORIO, RELATORIO.TIPO, RELATORIO.DATA_CRIACAO, RELATORIO.SITUACAO, RELATORIO.NMRELATORIO, EXAME.PERIODO_VENCIMENTO, EXAME.PADRAO_CONTRATO ");
                query.Append(" FROM SEG_RELATORIO_EXAME RELATORIO_EXAME "); 
                query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = RELATORIO_EXAME.ID_EXAME ");
                query.Append(" JOIN SEG_RELATORIO RELATORIO ON RELATORIO.ID_RELATORIO = RELATORIO_EXAME.ID_RELATORIO ");
                query.Append(" WHERE RELATORIO_EXAME.ID_RELATORIO_EXAME = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    relatorioExameReturn = new RelatorioExame(dr.GetInt64(0), new Exame(dr.GetInt64(3), dr.GetString(4), dr.GetString(5), dr.GetBoolean(6), dr.GetDecimal(7), dr.GetInt32(8), dr.GetDecimal(9), dr.GetBoolean(10), dr.GetBoolean(11), dr.IsDBNull(12) ? string.Empty : dr.GetString(12), dr.GetBoolean(13), dr.IsDBNull(22) ? (int?)null : dr.GetInt32(22), dr.GetBoolean(23)), new Relatorio(dr.GetInt64(14), dr.GetString(15), dr.GetString(16), dr.GetDateTime(17), dr.GetString(18), dr.GetString(19)), dr.GetString(20), dr.GetDateTime(21));
                }

                return relatorioExameReturn;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<RelatorioExame> findAllByExame(Exame exame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByExame");

            List<RelatorioExame> relatoriosExame = new List<RelatorioExame>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT RELATORIO_EXAME.ID_RELATORIO_EXAME, RELATORIO_EXAME.SITUACAO, RELATORIO_EXAME.DATA_GRAVACAO, EXAME.ID_EXAME, EXAME.DESCRICAO, EXAME.SITUACAO, EXAME.LABORATORIO, EXAME.PRECO, EXAME.PRIORIDADE, EXAME.CUSTO, EXAME.EXTERNO, EXAME.LIBERA_DOCUMENTO, EXAME.CODIGO_TUSS, EXAME.EXAME_COMPLEMENTAR,");
                query.Append(" RELATORIO.ID_RELATORIO, RELATORIO.NOME_RELATORIO, RELATORIO.TIPO, RELATORIO.DATA_CRIACAO, RELATORIO.SITUACAO, RELATORIO.NMRELATORIO, EXAME.PERIODO_VENCIMENTO, EXAME.PADRAO_CONTRATO ");
                query.Append(" FROM SEG_RELATORIO_EXAME RELATORIO_EXAME ");
                query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = RELATORIO_EXAME.ID_EXAME ");
                query.Append(" JOIN SEG_RELATORIO RELATORIO ON RELATORIO.ID_RELATORIO = RELATORIO_EXAME.ID_RELATORIO ");
                query.Append(" WHERE RELATORIO_EXAME.ID_EXAME = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = exame.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    relatoriosExame.Add(new RelatorioExame(dr.GetInt64(0), new Exame(dr.GetInt64(3), dr.GetString(4), dr.GetString(5), dr.GetBoolean(6), dr.GetDecimal(7), dr.GetInt32(8), dr.GetDecimal(9), dr.GetBoolean(10), dr.GetBoolean(11), dr.IsDBNull(12) ? string.Empty : dr.GetString(12), dr.GetBoolean(13), dr.IsDBNull(20) ? (int?)null : dr.GetInt32(20), dr.GetBoolean(21)), new Relatorio(dr.GetInt64(14), dr.GetString(15), dr.GetString(16), dr.GetDateTime(17), dr.GetString(18), dr.GetString(19)), dr.GetString(1), dr.GetDateTime(2)));
                }

                return relatoriosExame;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public RelatorioExame update(RelatorioExame relatorioExame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_RELATORIO_EXAME SET SITUACAO = :VALUE2, DATA_GRAVACAO = :VALUE3 WHERE ID_RELATORIO_EXAME = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = relatorioExame.IdRelatorioExame;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = relatorioExame.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Date));
                command.Parameters[2].Value = relatorioExame.DataGravacao;

                command.ExecuteNonQuery();

                return relatorioExame;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void delete(RelatorioExame relatorioExame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" DELETE FROM SEG_RELATORIO_EXAME WHERE ID_RELATORIO_EXAME = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = relatorioExame.IdRelatorioExame;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

    }
}
