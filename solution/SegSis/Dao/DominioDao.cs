﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using System.Data;
using Npgsql;
using SWS.Excecao;
using SWS.Entidade;
using NpgsqlTypes;
using SWS.Helper;

namespace SWS.Dao
{
    class DominioDao : IDominioDao
    {
        public DataSet listaTodos(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método listaTodos");

            try
            {
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter("select id_dominio, descricao from seg_dominio order by descricao asc", dbConnection);

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Dominios");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - listaTodos", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public HashSet<Dominio> findByPerfil(Int64? idDominio, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByPerfil");

            HashSet<Dominio> dominios = new HashSet<Dominio>();

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" select d.id_dominio, d.descricao, d.situacao from seg_dominio d, ");
                query.Append(" seg_perfil_dominio_acao pda where d.id_dominio = pda.id_dominio ");
                query.Append(" and pda.id_perfil = :value1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = idDominio;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    dominios.Add(new Dominio(dr.GetInt64(0), dr.GetString(1), dr.GetString(2)));
                }

                return dominios;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByPerfil", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
