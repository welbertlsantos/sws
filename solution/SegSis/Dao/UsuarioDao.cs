﻿using SWS.Entidade;
using Npgsql;
using SWS.Excecao;
using System;
using NpgsqlTypes;
using System.Data;
using System.Text;
using SWS.IDao;
using SWS.View.Resources;
using SWS.Helper;
using System.Collections.Generic;

namespace SWS.Dao
{
    class UsuarioDao : IUsuarioDao
    {
        public Usuario autentica(Usuario usuario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método autentica");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_USUARIO, NOME, CPF, RG, DOC_EXTRA, DATA_ADMISSAO, SITUACAO, LOGIN, ");
                query.Append(" SENHA, EMAIL, ID_MEDICO, ID_EMPRESA, MTE, ELABORA_DOCUMENTO, ASSINATURA, MIMETYPE, EXTERNO, ORGAO_EMISSOR, UF_ORGAO_EMISSOR ");
                query.Append(" FROM SEG_USUARIO ");
                query.Append(" WHERE LOGIN = :VALUE1 AND SENHA = :VALUE2 AND SITUACAO = 'A'");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = usuario.Login;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = usuario.Senha;

                NpgsqlDataReader dr = command.ExecuteReader();

                if (!dr.HasRows)
                {
                    throw new UsuarioNaoAutenticadoException("Usuário inexistente.");
                }
                else
                {
                    while (dr.Read())
                    {
                        usuario = new Usuario(dr.GetInt64(0), dr.GetString(1), dr.IsDBNull(2) ? String.Empty : dr.GetString(2), dr.IsDBNull(3) ? String.Empty : dr.GetString(3), dr.IsDBNull(4) ? String.Empty : dr.GetString(4), dr.IsDBNull(5) ? (DateTime?)null : dr.GetDateTime(5), dr.IsDBNull(6) ? String.Empty : dr.GetString(6), dr.GetString(7), dr.IsDBNull(8) ? String.Empty : dr.GetString(8), dr.IsDBNull(9) ? String.Empty : dr.GetString(9), dr.IsDBNull(10) ? null : new Medico(dr.GetInt64(10)), dr.IsDBNull(11) ? null : new Empresa(dr.GetInt64(11)), dr.IsDBNull(12) ? String.Empty : dr.GetString(12), dr.GetBoolean(13), dr.IsDBNull(14) ? null : (Byte[])dr.GetValue(14), dr.IsDBNull(15) ? String.Empty : dr.GetString(15), dr.GetBoolean(16), dr.IsDBNull(17) ? string.Empty : dr.GetString(17), dr.IsDBNull(18) ? string.Empty : dr.GetString(18));
                    }
                }

                return usuario;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - autentica", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public Usuario insert(Usuario usuario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluir");

            StringBuilder query = new StringBuilder();

            try
            {
                usuario.Id = recuperaProximoId(dbConnection);

                query.Append(" INSERT INTO SEG_USUARIO ( ID_USUARIO, NOME, CPF, RG, DOC_EXTRA, DATA_ADMISSAO, ");
                query.Append(" SITUACAO, LOGIN, SENHA, EMAIL, ID_MEDICO, ID_EMPRESA, MTE, ELABORA_DOCUMENTO, ASSINATURA, MIMETYPE, EXTERNO, ORGAO_EMISSOR, UF_ORGAO_EMISSOR) VALUES ");
                query.Append(" (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6, :VALUE7, :VALUE8, ");
                query.Append(" :VALUE9, :VALUE10, :VALUE11, :VALUE12, :VALUE13, :VALUE14, :VALUE15, :VALUE16, :VALUE17, :VALUE18, :VALUE19)");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = usuario.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = usuario.Nome.ToUpper();
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = usuario.Cpf;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = usuario.Rg;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = usuario.DocumentoExtra.ToUpper();
                
                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Date));
                command.Parameters[5].Value = usuario.DataAdmissao;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = usuario.Situacao.ToUpper();
                
                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Varchar));
                command.Parameters[7].Value = usuario.Login.ToUpper();
                
                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                command.Parameters[8].Value = usuario.Senha;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                command.Parameters[9].Value = usuario.Email;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Integer));
                command.Parameters[10].Value = usuario.Medico == null ? null : usuario.Medico.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Integer));
                command.Parameters[11].Value = usuario.Empresa != null ? usuario.Empresa.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Varchar));
                command.Parameters[12].Value = usuario.Mte.ToUpper();
                
                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Boolean));
                command.Parameters[13].Value = usuario.ElaboraDocumento;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Bytea));
                command.Parameters[14].Value = usuario.Assinatura != null ? usuario.Assinatura : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Varchar));
                command.Parameters[15].Value = usuario.Mimetype;

                command.Parameters.Add(new NpgsqlParameter("VALUE17", NpgsqlDbType.Boolean));
                command.Parameters[16].Value = usuario.Externo;

                command.Parameters.Add(new NpgsqlParameter("VALUE18", NpgsqlDbType.Varchar));
                command.Parameters[17].Value = usuario.OrgaoEmissor;

                command.Parameters.Add(new NpgsqlParameter("VALUE19", NpgsqlDbType.Varchar));
                command.Parameters[18].Value = usuario.UfOrgaoEmissor;
                
                command.ExecuteNonQuery();

                return usuario;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirUsuario", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_USUARIO_ID_USUARIO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet listaTodos(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " listaTodos");

            try
            {
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter("SELECT ID_USUARIO, NOME, CPF, RG, DATA_ADMISSAO, LOGIN FROM SEG_USUARIO ORDER BY NOME ASC", dbConnection);

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Usuarios");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - listaTodos", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet listaTodosAtivos(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " listaTodosAtivos");

            try
            {
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter("SELECT ID_USUARIO, NOME, CPF, RG, DATA_ADMISSAO, LOGIN FROM SEG_USUARIO WHERE SITUACAO = 'A' ORDER BY NOME ASC", dbConnection);

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Usuarios");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - listaTodosAtivos", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findByFilter(Usuario usuario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + "findByFilter");

            try
            {
                StringBuilder query = new StringBuilder();

                if (usuario.isNullForFilter())
                {
                    query.Append(" SELECT USUARIO.ID_USUARIO, USUARIO.NOME, USUARIO.CPF, USUARIO.RG, USUARIO.DATA_ADMISSAO, USUARIO.LOGIN, USUARIO.SITUACAO, EMPRESA.FANTASIA ");
                    query.Append(" FROM SEG_USUARIO USUARIO ");
                    query.Append(" LEFT JOIN SEG_EMPRESA EMPRESA ON EMPRESA.ID_EMPRESA = USUARIO.ID_EMPRESA ");
                    query.Append(" WHERE 1 = 1 ");

                    if (usuario.Externo)
                        query.Append(" AND USUARIO.EXTERNO = :VALUE5 ");

                    query.Append(" ORDER BY NOME ASC");
                }
                else
                {
                    query.Append(" SELECT USUARIO.ID_USUARIO, USUARIO.NOME, USUARIO.CPF, USUARIO.RG, USUARIO.DATA_ADMISSAO, USUARIO.LOGIN, USUARIO.SITUACAO, EMPRESA.FANTASIA ");
                    query.Append(" FROM SEG_USUARIO USUARIO ");
                    query.Append(" LEFT JOIN SEG_EMPRESA EMPRESA ON EMPRESA.ID_EMPRESA = USUARIO.ID_EMPRESA ");
                    query.Append(" WHERE 1 = 1 ");
                    
                    if (!String.IsNullOrWhiteSpace(usuario.Nome))
                        query.Append(" AND USUARIO.NOME LIKE :VALUE1 ");

                    if (!String.IsNullOrWhiteSpace(usuario.Cpf))
                        query.Append(" AND USUARIO.CPF = :VALUE2 ");
                
                    if (!String.IsNullOrWhiteSpace(usuario.Login))
                        query.Append(" AND USUARIO.LOGIN LIKE :VALUE3 ");

                    if (!String.IsNullOrWhiteSpace(usuario.Situacao))
                        query.Append(" AND USUARIO.SITUACAO = :VALUE4 ");

                    if (usuario.Externo)
                        query.Append(" AND USUARIO.EXTERNO = :VALUE5 ");

                    query.Append(" ORDER BY NOME ASC ");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[0].Value = String.IsNullOrEmpty(usuario.Nome) ? String.Empty : usuario.Nome.ToUpper() + "%";
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[1].Value = usuario.Cpf;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[2].Value = String.IsNullOrEmpty(usuario.Login) ? String.Empty : usuario.Login.ToUpper() + "%";
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[3].Value = usuario.Situacao;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Boolean));
                adapter.SelectCommand.Parameters[4].Value = usuario.Externo;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Usuarios");

                return ds;
             }
             catch (NpgsqlException ex)
             {
                 LogHelper.logger.Error(this.GetType().Name + " - findByFilter", ex);
                 throw new DataBaseConectionException(ex.Message);
             }
        }

        public Usuario findById(Int64 idUsuario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            Usuario usuarioRetorno = null;

            StringBuilder query = new StringBuilder();
            try
            {
                query.Append(" SELECT USUARIO.ID_USUARIO, USUARIO.NOME, USUARIO.CPF, USUARIO.RG, USUARIO.DOC_EXTRA, USUARIO.DATA_ADMISSAO, ");
                query.Append(" USUARIO.SITUACAO, USUARIO.LOGIN, USUARIO.SENHA, USUARIO.EMAIL, USUARIO.ID_MEDICO, USUARIO.ID_EMPRESA, USUARIO.MTE, USUARIO.ELABORA_DOCUMENTO, USUARIO.ASSINATURA, USUARIO.MIMETYPE, USUARIO.EXTERNO, USUARIO.ORGAO_EMISSOR, USUARIO.UF_ORGAO_EMISSOR");
                query.Append(" FROM SEG_USUARIO USUARIO ");
                query.Append(" WHERE USUARIO.ID_USUARIO = :VALUE1 ");
                                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = idUsuario;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {

                    usuarioRetorno = new Usuario(dr.GetInt64(0), dr.GetString(1), dr.IsDBNull(2) ? String.Empty : dr.GetString(2), dr.IsDBNull(3) ? String.Empty : dr.GetString(3), dr.IsDBNull(4) ? String.Empty : dr.GetString(4), dr.IsDBNull(5) ? (DateTime?)null : dr.GetDateTime(5), dr.IsDBNull(6) ? String.Empty : dr.GetString(6), dr.GetString(7), dr.IsDBNull(8) ? String.Empty : dr.GetString(8), dr.IsDBNull(9) ? String.Empty : dr.GetString(9), dr.IsDBNull(10) ? null : new Medico(dr.GetInt64(10)), dr.IsDBNull(11) ? null : new Empresa(dr.GetInt64(11)),  dr.IsDBNull(12) ? String.Empty : dr.GetString(12), dr.GetBoolean(13), dr.IsDBNull(14) ? null : (Byte[])dr.GetValue(14), dr.IsDBNull(15) ? String.Empty : dr.GetString(15), dr.GetBoolean(16), dr.IsDBNull(17) ? string.Empty : dr.GetString(17), dr.IsDBNull(18) ? string.Empty : dr.GetString(18));
                }

                return usuarioRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Usuario update(Usuario usuario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_USUARIO SET NOME = :VALUE2, CPF = :VALUE3, RG = :VALUE4, ");
                query.Append(" DOC_EXTRA = :VALUE5, DATA_ADMISSAO = :VALUE6, SITUACAO = :VALUE7, ");
                query.Append(" LOGIN = :VALUE8, EMAIL = :VALUE9, MTE = :VALUE10, ELABORA_DOCUMENTO = :VALUE11, ASSINATURA = :VALUE12, MIMETYPE = :VALUE13, EXTERNO = :VALUE14, ORGAO_EMISSOR = :VALUE15, UF_ORGAO_EMISSOR = :VALUE16 WHERE ID_USUARIO = :VALUE1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = usuario.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = usuario.Nome.ToUpper();
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = usuario.Cpf;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = usuario.Rg;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = usuario.DocumentoExtra.ToUpper();
                
                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Date));
                command.Parameters[5].Value = usuario.DataAdmissao;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = usuario.Situacao.ToUpper();
                
                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Varchar));
                command.Parameters[7].Value = usuario.Login.ToUpper();
                
                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                command.Parameters[8].Value = usuario.Email;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                command.Parameters[9].Value = usuario.Mte.ToUpper();
                
                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Boolean));
                command.Parameters[10].Value = usuario.ElaboraDocumento;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Bytea));
                command.Parameters[11].Value = usuario.Assinatura;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Varchar));
                command.Parameters[12].Value = usuario.Mimetype;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Boolean));
                command.Parameters[13].Value = usuario.Externo;

                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Varchar));
                command.Parameters[14].Value = usuario.OrgaoEmissor;

                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Varchar));
                command.Parameters[15].Value = usuario.UfOrgaoEmissor;

                command.ExecuteNonQuery();

                return usuario;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void updateSenha(Usuario usuario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateSenha");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("UPDATE SEG_USUARIO SET SENHA = :VALUE2 WHERE ID_USUARIO = :VALUE1", dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = usuario.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = usuario.Senha.ToUpper();
                
                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateSenha", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public List<Usuario> findByCpf(string cpf, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByCpf");

            List<Usuario> usuarios = new List<Usuario>();

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT USUARIO.ID_USUARIO, USUARIO.NOME, USUARIO.CPF, USUARIO.RG, USUARIO.DOC_EXTRA, USUARIO.DATA_ADMISSAO, ");
                query.Append(" USUARIO.SITUACAO, USUARIO.LOGIN, USUARIO.SENHA, USUARIO.EMAIL, USUARIO.ID_MEDICO, USUARIO.ID_EMPRESA, ");
                query.Append(" USUARIO.MTE, USUARIO.ELABORA_DOCUMENTO, USUARIO.ASSINATURA, USUARIO.MIMETYPE, USUARIO.EXTERNO, USUARIO.ORGAO_EMISSOR, USUARIO.UF_ORGAO_EMISSOR ");
                query.Append(" FROM SEG_USUARIO USUARIO ");
                query.Append(" WHERE USUARIO.CPF = :VALUE1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = cpf;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    usuarios.Add(new Usuario(dr.GetInt64(0), dr.GetString(1), dr.IsDBNull(2) ? String.Empty : dr.GetString(2), dr.IsDBNull(3) ? String.Empty : dr.GetString(3), dr.IsDBNull(4) ? String.Empty : dr.GetString(4), dr.IsDBNull(5) ? (DateTime?)null : dr.GetDateTime(5), dr.IsDBNull(6) ? String.Empty : dr.GetString(6), dr.GetString(7), dr.IsDBNull(8) ? String.Empty : dr.GetString(8), dr.IsDBNull(9) ? String.Empty : dr.GetString(9), dr.IsDBNull(10) ? null : new Medico(dr.GetInt64(10)), dr.IsDBNull(11) ? null : new Empresa(dr.GetInt64(11)), dr.IsDBNull(12) ? String.Empty : dr.GetString(12), dr.GetBoolean(13), dr.IsDBNull(14) ? null : (Byte[])dr.GetValue(14), dr.IsDBNull(15) ? String.Empty : dr.GetString(15), dr.GetBoolean(16), dr.IsDBNull(17) ? string.Empty : dr.GetString(17), dr.IsDBNull(18) ? string.Empty : dr.GetString(18)));
                    
                }

                return usuarios;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByCPF", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Usuario findUsuarioByLogin(String login, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findUsuarioByLogin");

            Usuario usuarioRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT USUARIO.ID_USUARIO, USUARIO.NOME, USUARIO.CPF, USUARIO.RG, USUARIO.DOC_EXTRA, USUARIO.DATA_ADMISSAO, ");
                query.Append(" USUARIO.SITUACAO, USUARIO.LOGIN, USUARIO.SENHA, USUARIO.EMAIL, USUARIO.ID_MEDICO, USUARIO.ID_EMPRESA, ");
                query.Append(" USUARIO.MTE, USUARIO.ELABORA_DOCUMENTO, USUARIO.ASSINATURA, USUARIO.MIMETYPE, USUARIO.EXTERNO, USUARIO.ORGAO_EMISSOR, USUARIO.UF_ORGAO_EMISSOR ");
                query.Append(" FROM SEG_USUARIO USUARIO ");
                query.Append(" WHERE USUARIO.LOGIN = :VALUE1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = login.ToUpper();

                NpgsqlDataReader dr = command.ExecuteReader();
                
                while (dr.Read())
                {
                    usuarioRetorno = new Usuario(dr.GetInt64(0),dr.GetString(1), dr.IsDBNull(2) ? String.Empty : dr.GetString(2), dr.IsDBNull(3) ? String.Empty : dr.GetString(3), dr.IsDBNull(4) ? String.Empty : dr.GetString(4), dr.IsDBNull(5) ? (DateTime?)null : dr.GetDateTime(5), dr.IsDBNull(6) ? String.Empty : dr.GetString(6), dr.GetString(7), dr.IsDBNull(8) ? String.Empty : dr.GetString(8), dr.IsDBNull(9) ? String.Empty : dr.GetString(9), dr.IsDBNull(10) ? null : new Medico(dr.GetInt64(10)), dr.IsDBNull(11) ? null : new Empresa(dr.GetInt64(11)), dr.IsDBNull(12) ? String.Empty : dr.GetString(12), dr.GetBoolean(13), dr.IsDBNull(14) ? null : (Byte[])dr.GetValue(14), dr.IsDBNull(15) ? String.Empty : dr.GetString(15), dr.GetBoolean(16), dr.IsDBNull(17) ? string.Empty : dr.GetString(17), dr.IsDBNull(18) ? string.Empty : dr.GetString(18));
                    
                }
                
                return usuarioRetorno;    
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findUsuarioByLogin", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public DataSet findUsuarioFuncaoAtivoByFilter(Usuario usuario, Int64 idFuncaoProcurada, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findUsuarioFuncaoAtivoByFilter");

            try
            {
                StringBuilder query = new StringBuilder();

                if (usuario.isNullForFilterFuncaoSeleciona())
                {
                    query.Append(" SELECT ID_USUARIO, NOME FROM SEG_USUARIO WHERE ID_USUARIO IN (SELECT ID_USUARIO FROM SEG_USUARIO_FUNCAO_INTERNA WHERE ID_FUNCAO_INTERNA = :VALUE1 AND SITUACAO = 'A') ORDER BY NOME ASC");
                }
                else
                {
                    query.Append(" SELECT ID_USUARIO, NOME FROM SEG_USUARIO WHERE ID_USUARIO IN (SELECT ID_USUARIO FROM SEG_USUARIO_FUNCAO_INTERNA WHERE ID_FUNCAO_INTERNA = :VALUE1 AND SITUACAO = 'A' ) AND ");

                    if (!String.IsNullOrWhiteSpace(usuario.Nome))
                        query.Append(" NOME LIKE :VALUE2 ORDER BY NOME ASC ");

                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = idFuncaoProcurada;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = usuario != null ? usuario.Nome : null;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Usuarios");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public LinkedList<Usuario> findUsuarioByFuncaoInterna(Int64 idFuncaoInterna, Int64? idFuncaoInternaFinal, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findUsuarioByFuncaoInterna");

            try
            {
                StringBuilder query = new StringBuilder();

                LinkedList<Usuario> usuarios = new LinkedList<Usuario>() { };

                query.Append(" SELECT USUARIO.ID_USUARIO, USUARIO.NOME, USUARIO.CPF, USUARIO.RG, USUARIO.DOC_EXTRA, USUARIO.DATA_ADMISSAO, USUARIO.SITUACAO, USUARIO.LOGIN, USUARIO.SENHA, ");
                query.Append(" USUARIO.EMAIL, USUARIO.ID_MEDICO, USUARIO.ID_EMPRESA, USUARIO.MTE, USUARIO.ELABORA_DOCUMENTO, USUARIO.ASSINATURA, USUARIO.MIMETYPE, USUARIO.EXTERNO, USUARIO.ORGAO_EMISSOR, USUARIO.UF_ORGAO_EMISSOR ");
                query.Append(" FROM SEG_USUARIO USUARIO ");
                query.Append(" JOIN SEG_USUARIO_FUNCAO_INTERNA UFI ON USUARIO.ID_USUARIO = UFI.ID_USUARIO ");

                if (idFuncaoInternaFinal == null)
                    query.Append(" WHERE USUARIO.SITUACAO = 'A' AND UFI.SITUACAO = 'A' AND UFI.ID_FUNCAO_INTERNA  = :VALUE1 ");
                else
                    query.Append(" WHERE USUARIO.SITUACAO = 'A' AND UFI.SITUACAO = 'A' AND UFI.ID_FUNCAO_INTERNA IN (:VALUE1, :VALUE2) ");
                                
                query.Append(" ORDER BY USUARIO.NOME ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = idFuncaoInterna;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = idFuncaoInternaFinal;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    usuarios.AddLast(new Usuario(dr.GetInt64(0), dr.GetString(1), dr.IsDBNull(2) ? String.Empty : dr.GetString(2), dr.IsDBNull(3) ? String.Empty : dr.GetString(3), dr.IsDBNull(4) ? String.Empty : dr.GetString(4), dr.IsDBNull(5) ? (DateTime?)null : dr.GetDateTime(5), dr.IsDBNull(6) ? String.Empty : dr.GetString(6), dr.GetString(7), dr.IsDBNull(8) ? String.Empty : dr.GetString(8), dr.IsDBNull(9) ? String.Empty : dr.GetString(9), dr.IsDBNull(10) ? null : new Medico(dr.GetInt64(10)), dr.IsDBNull(11) ? null : new Empresa(dr.GetInt64(11)), dr.IsDBNull(12) ? String.Empty : dr.GetString(12), dr.GetBoolean(13), dr.IsDBNull(14) ? null : (Byte[])dr.GetValue(14), dr.IsDBNull(15) ? String.Empty : dr.GetString(15), dr.GetBoolean(16), dr.IsDBNull(17) ? string.Empty : dr.GetString(17), dr.IsDBNull(18) ? string.Empty : dr.GetString(18)));
                }

                return usuarios;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findUsuarioByFuncaoInterna", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findUsuarioElaboraDocumento(Usuario usuario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findUsuarioElaboraDocumento");

            try
            {
                StringBuilder query = new StringBuilder();

                if (usuario.isNullForFilter())
                {
                    query.Append(" SELECT ID_USUARIO, NOME FROM SEG_USUARIO WHERE ELABORA_DOCUMENTO = TRUE AND SITUACAO = 'A' ");
                }
                else
                {
                    query.Append(" SELECT ID_USUARIO, NOME FROM SEG_USUARIO WHERE ELABORA_DOCUMENTO = TRUE AND SITUACAO = 'A' AND ");

                    if (!String.IsNullOrWhiteSpace(usuario.Nome))
                        query.Append(" NOME LIKE :VALUE1 ");

                }

                query.Append(" ORDER BY NOME ASC ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = string.IsNullOrEmpty(usuario.Nome) ? string.Empty : "%" + usuario.Nome.ToUpper() + "%";

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Usuarios");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findUsuarioElaboraDocumento", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }


    }
}