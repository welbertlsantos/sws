﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;
using NpgsqlTypes;

namespace SWS.Dao
{
    class ArquivoDao : IArquivoDao
    {
        public Arquivo insertArquivo(Arquivo arquivo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertArquivo");

            try
            {
                arquivo.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_ARQUIVO (ID_ARQUIVO, DESCRICAO, MIMETYPE, SIZE, CONTEUDO) VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = arquivo.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = arquivo.Descricao.ToUpper();

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = arquivo.Mimetype.ToUpper();

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = arquivo.Size;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Bytea));
                command.Parameters[4].Value = arquivo.Conteudo;
               
                command.ExecuteNonQuery();

                return arquivo;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertArquivo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Arquivo findArquivoById(Int64? id, NpgsqlConnection dbConnection)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findArquivoById");

            Arquivo arquivoRetorno = null;

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT ID_ARQUIVO, DESCRICAO, MIMETYPE, SIZE, CONTEUDO FROM SEG_ARQUIVO WHERE ID_ARQUIVO = :VALUE1", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                byte[] conteudo;

                while (dr.Read())
                {
                    conteudo = (byte[]) dr.GetValue(4);
                    arquivoRetorno = new Arquivo(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetInt32(3), conteudo);
                }

                return arquivoRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findArquivoById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        private Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_ARQUIVO_ID_ARQUIVO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}