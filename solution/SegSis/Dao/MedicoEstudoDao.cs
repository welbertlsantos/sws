﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using SWS.Excecao;
using System.Data;
using Npgsql;
using NpgsqlTypes;
using SWS.Helper;
using SWS.View.Resources;


namespace SWS.Dao
{
    class MedicoEstudoDao :IMedicoEstudoDao
    {

        public DataSet findAllByEstudo(Estudo pcmso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByEstudo");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT MEDICO.ID_MEDICO, MEDICO.NOME, MEDICO.CRM, MEDICO.SITUACAO, MEDICO.TELEFONE1, MEDICO.TELEFONE2, MEDICO.EMAIL, MEDICO.ENDERECO, ");
                query.Append(" MEDICO.NUMERO, MEDICO.COMPLEMENTO, MEDICO.BAIRRO, MEDICO.CEP, CIDADE.NOME AS CIDADE, MEDICO.UF ");
                query.Append(" FROM SEG_MEDICO MEDICO " );
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = MEDICO.ID_CIDADE ");
                query.Append(" JOIN SEG_ESTUDO_MEDICO ESTUDO_MEDICO ON ESTUDO_MEDICO.ID_MEDICO = MEDICO.ID_MEDICO");
                query.Append(" WHERE ESTUDO_MEDICO.ID_ESTUDO = :VALUE1 ORDER BY MEDICO.NOME ASC ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = pcmso.Id;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Medicos");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public MedicoEstudo insert(MedicoEstudo medicoEstudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" INSERT INTO SEG_ESTUDO_MEDICO (ID_ESTUDO, ID_MEDICO, COORDENADOR, SITUACAO) VALUES ( :VALUE1, :VALUE2, :VALUE3, 'A' ) ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = medicoEstudo.Pcmso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = medicoEstudo.Medico.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Boolean));
                command.Parameters[2].Value = medicoEstudo.Coordenador;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }

            return medicoEstudo;
        }

        public void delete(MedicoEstudo medicoEstudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" DELETE FROM SEG_ESTUDO_MEDICO WHERE ID_MEDICO = :VALUE1 AND ID_ESTUDO = :VALUE2", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = medicoEstudo.Medico.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = medicoEstudo.Pcmso.Id;
                
                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean medicoIsUsed(Medico medico, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método medicoIsUsed");

            Boolean isMedicoInUsed = false;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT COUNT(*) ");
                query.Append(" FROM SEG_ESTUDO_MEDICO ESTUDO_MEDICO ");
                query.Append(" JOIN SEG_ESTUDO ESTUDO ON ESTUDO.ID_ESTUDO = ESTUDO_MEDICO.ID_ESTUDO ");
                query.Append(" WHERE ESTUDO_MEDICO.ID_MEDICO = :VALUE1 AND ESTUDO.SITUACAO = 'F' AND ESTUDO.AVULSO = FALSE ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = medico.Id;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    if (dr.GetInt64(0) > 0)
                    {
                        isMedicoInUsed = true;
                    }
                }

                return isMedicoInUsed;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - medicoIsUsed", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public List<MedicoEstudo> findAllByPcmsoList(Estudo pcmso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByPcmsoList");

            try
            {
                StringBuilder query = new StringBuilder();

                List<MedicoEstudo> medicosEstudo = new List<MedicoEstudo>();

                query.Append(" SELECT MEDICO.ID_MEDICO, MEDICO.NOME, MEDICO.CRM, MEDICO.SITUACAO, MEDICO.TELEFONE1, MEDICO.TELEFONE2, MEDICO.EMAIL, MEDICO.ENDERECO, ");
                query.Append(" MEDICO.NUMERO, MEDICO.COMPLEMENTO, MEDICO.BAIRRO, MEDICO.CEP, MEDICO.UF, MEDICO.ID_CIDADE, MEDICO.PIS_PASEP, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, ESTUDO_MEDICO.COORDENADOR, ESTUDO_MEDICO.SITUACAO, MEDICO.UF_CRM, MEDICO.DOCUMENTO_EXTRA, MEDICO.ASSINATURA, MEDICO.MIMETYPE, MEDICO.CPF, MEDICO.RQE ");
                query.Append(" FROM SEG_MEDICO MEDICO " );
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = MEDICO.ID_CIDADE ");
                query.Append(" JOIN SEG_ESTUDO_MEDICO ESTUDO_MEDICO ON ESTUDO_MEDICO.ID_MEDICO = MEDICO.ID_MEDICO ");
                query.Append(" WHERE ESTUDO_MEDICO.ID_ESTUDO = :VALUE1 ORDER BY MEDICO.NOME ASC ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = pcmso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    medicosEstudo.Add(new MedicoEstudo(pcmso, new Medico(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.IsDBNull(4) ? string.Empty : dr.GetString(4), dr.IsDBNull(5) ? string.Empty : dr.GetString(5), dr.IsDBNull(6) ? string.Empty : dr.GetString(6), dr.IsDBNull(7) ? string.Empty : dr.GetString(7), dr.IsDBNull(8) ? string.Empty : dr.GetString(8), dr.IsDBNull(9) ? string.Empty : dr.GetString(9), dr.IsDBNull(10) ? string.Empty : dr.GetString(10), dr.IsDBNull(11) ? string.Empty : dr.GetString(11), dr.IsDBNull(12) ? string.Empty : dr.GetString(12), dr.IsDBNull(13) ? null : new CidadeIbge(dr.GetInt64(13), dr.GetString(15), dr.GetString(16), dr.GetString(17)), dr.IsDBNull(14) ? String.Empty : dr.GetString(14), dr.IsDBNull(20) ? string.Empty : dr.GetString(20), dr.IsDBNull(21) ? string.Empty : dr.GetString(21), dr.IsDBNull(22) ? null : (byte[])dr.GetValue(22), dr.IsDBNull(23) ? string.Empty : dr.GetString(23), dr.IsDBNull(24) ? string.Empty : dr.GetString(24), dr.IsDBNull(25) ? string.Empty : dr.GetString(25)), dr.GetBoolean(18), dr.IsDBNull(19) ? String.Empty : dr.GetString(19)));
                }

                return medicosEstudo;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByPcmsoList", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public MedicoEstudo update(MedicoEstudo medicoEstudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_ESTUDO_MEDICO SET COORDENADOR = :VALUE3, SITUACAO = :VALUE4 WHERE ID_ESTUDO = :VALUE1 AND ID_MEDICO = :VALUE2 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = medicoEstudo.Pcmso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = medicoEstudo.Medico.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Boolean));
                command.Parameters[2].Value = medicoEstudo.Coordenador;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = medicoEstudo.Situacao;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }

            return medicoEstudo;
        }

    }
}
