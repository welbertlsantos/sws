﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using SWS.Excecao;
using Npgsql;
using NpgsqlTypes;
using System.Data;
using SWS.Helper;
using SWS.View.Resources;

namespace SWS.Dao
{
    class AsoDao : IAsoDao
    {
        public Boolean findAsoByClienteFuncaoFuncionario(ClienteFuncaoFuncionario clienteFuncaoFuncionario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAsoByClienteFuncaoFuncionario");

            try
            {
                StringBuilder query = new StringBuilder();

                Boolean retorno = Convert.ToBoolean(false);

                query.Append(" SELECT A.ID_ASO, A.ID_MEDICO_EXAMINADOR, A.CODIGO_ASO, A.DATA_ASO, A.NOME_FUNCIONARIO, ");
                query.Append(" A.RG_FUNCIONARIO, A.DATANASC_FUNCIONARIO, A.CPF_FUNCIONARIO, A.TIPOSAN_FUNCIONARIO, ");
                query.Append(" A.FTRH_FUNCIONARIO, A.MATRICULA_FUNCIONARIO, A.RAZAO_EMPRESA, A.CNPJ_EMPRESA, ");
                query.Append(" A.ENDERECO_EMPRESA, A.NUMERO_EMPRESA, A.COMPLEMENTO_EMPRESA, A.BAIRRO_EMPRESA, ");
                query.Append(" A.CEP_EMPRESA, A.CIDADE_EMPRESA, A.UF_EMPRESA, A.ID_PERIODICIDADE, A.SITUACAO, ");
                query.Append(" A.ID_MEDICO_COORDENADOR, A.CRM_COORDENADOR, A.TELEFONE1_COORDENADOR, A.TELEFONE2_COORDENADOR, ");
                query.Append(" A.NOME_COORDENADOR, A.CRM_EXAMINADOR, A.NOME_EXAMINADOR, A.TELEFONE1_EXAMINADOR, ");
                query.Append(" A.TELEFONE2_EXAMINADOR, A.ID_USUARIO_CRIADOR, A.ID_USUARIO_FINALIZADOR FROM SEG_ASO A WHERE ");
                query.Append(" A.ID_CLIENTE_FUNCAO_FUNCIONARIO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = clienteFuncaoFuncionario.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    retorno = Convert.ToBoolean(true);
                }

                return retorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAsoByClienteFuncaoFuncionario", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAsoByFilter(Aso atendimento, ClienteFuncao clienteFuncao, DateTime? dataAsoFinal, DateTime? dataCriacaoFinal, Usuario usuarioLogado, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAsoByFilter");

            try
            {
                StringBuilder query = new StringBuilder();
                StringBuilder join = new StringBuilder();
                StringBuilder where = new StringBuilder();

                // chaves para verificar se já foi utilizado as ligações
                Boolean bolJoin = false;
                
                if (atendimento.isNullForFilter() && clienteFuncao == null && atendimento.Cliente == null && atendimento.Funcionario.isNullForFilterAso() && atendimento.DataGravacao == null)
                {
                    query.Append(" SELECT ASO.ID_ASO, ASO.ID_CLIENTE_FUNCAO_FUNCIONARIO, ASO.CODIGO_ASO, ASO.DATA_ASO, ");
                    query.Append(" ASO.NOME_FUNCIONARIO, ASO.CPF_FUNCIONARIO, ASO.MATRICULA_FUNCIONARIO, ASO.RAZAO_EMPRESA, ");
                    query.Append(" ASO.CNPJ_EMPRESA, ASO.ID_PERIODICIDADE, PERIODICIDADE.DESCRICAO, ASO.SITUACAO, CRIADOR.LOGIN AS USUARIO_CRIADOR, ASO.DATA_FINALIZACAO, ASO.CONCLUSAO, FINALIZADOR.LOGIN AS USUARIO_FINALIZOU, CANCELADOR.LOGIN AS USUARIO_CANCELOU, ASO.PRIORIDADE, FUNCIONARIO.PIS_PASEP, FUNCIONARIO.CTPS, FUNCIONARIO.RG, FUNCIONARIO.UF_EMISSOR ");
                    query.Append(" FROM SEG_ASO ASO ");
                    query.Append(" INNER JOIN SEG_PERIODICIDADE PERIODICIDADE ON PERIODICIDADE.ID_PERIODICIDADE = ASO.ID_PERIODICIDADE ");
                    query.Append(" JOIN SEG_USUARIO CRIADOR ON CRIADOR.ID_USUARIO = ASO.ID_USUARIO_CRIADOR");
                    query.Append(" LEFT JOIN SEG_USUARIO FINALIZADOR ON FINALIZADOR.ID_USUARIO = ASO.ID_USUARIO_FINALIZADOR ");
                    query.Append(" LEFT JOIN SEG_USUARIO CANCELADOR ON CANCELADOR.ID_USUARIO = ASO.ID_USUARIO_CANCELAMENTO ");
                    query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTE_FUNCAO_FUNCIONARIO ON CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO = ASO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                    query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTE_FUNCAO_FUNCIONARIO.ID_SEG_CLIENTE_FUNCAO ");
                    query.Append(" JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = CLIENTE_FUNCAO.ID_CLIENTE");
                    query.Append(" LEFT JOIN SEG_FUNCIONARIO FUNCIONARIO ON FUNCIONARIO.ID_FUNCIONARIO = CLIENTE_FUNCAO_FUNCIONARIO.ID_FUNCIONARIO ");
                    query.Append(" WHERE ASO.DATA_ASO BETWEEN :VALUE1 AND :VALUE2 ");
                    query.Append(" AND ASO.ID_EMPRESA = :VALUE13 ");


                    /* verificando se o usuário tem restrinção para pesquisa */
                    if (usuarioLogado.UsuarioCliente.Count > 1)
                    {
                        query.Append(" AND CLIENTE.ID_CLIENTE IN ( ");

                        int index = 0;
                        foreach (UsuarioCliente usuarioCliente in usuarioLogado.UsuarioCliente)
                        {
                            query.Append(usuarioCliente.Cliente.Id);
                            index++;
                            if (usuarioLogado.UsuarioCliente.Count == index)
                                query.Append(")");
                            else
                                query.Append(",");
                        }
                    }
                    
                }
                else
                {
                    query.Append(" SELECT ASO.ID_ASO, ASO.ID_CLIENTE_FUNCAO_FUNCIONARIO, ASO.CODIGO_ASO, ASO.DATA_ASO, ");
                    query.Append(" ASO.NOME_FUNCIONARIO, ASO.CPF_FUNCIONARIO, ASO.MATRICULA_FUNCIONARIO, ASO.RAZAO_EMPRESA, ");
                    query.Append(" ASO.CNPJ_EMPRESA, ASO.ID_PERIODICIDADE, PERIODICIDADE.DESCRICAO, ASO.SITUACAO, CRIADOR.LOGIN AS USUARIO_CRIADOR, ASO.DATA_FINALIZACAO, ASO.CONCLUSAO, FINALIZADOR.LOGIN AS USUARIO_FINALIZOU, CANCELADOR.LOGIN AS USUARIO_CANCELOU, ASO.PRIORIDADE, FUNCIONARIO.PIS_PASEP, FUNCIONARIO.CTPS, FUNCIONARIO.RG, FUNCIONARIO.UF_EMISSOR ");
                    query.Append(" FROM SEG_ASO ASO ");
                    query.Append(" INNER JOIN SEG_PERIODICIDADE PERIODICIDADE ON PERIODICIDADE.ID_PERIODICIDADE = ASO.ID_PERIODICIDADE ");
                    query.Append(" JOIN SEG_USUARIO CRIADOR ON CRIADOR.ID_USUARIO = ASO.ID_USUARIO_CRIADOR");
                    query.Append(" LEFT JOIN SEG_USUARIO FINALIZADOR ON FINALIZADOR.ID_USUARIO = ASO.ID_USUARIO_FINALIZADOR ");
                    query.Append(" LEFT JOIN SEG_USUARIO CANCELADOR ON CANCELADOR.ID_USUARIO = ASO.ID_USUARIO_CANCELAMENTO ");
                    query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTE_FUNCAO_FUNCIONARIO ON CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO = ASO.ID_CLIENTE_FUNCAO_FUNCIONARIO");
                    query.Append(" LEFT JOIN SEG_FUNCIONARIO FUNCIONARIO ON FUNCIONARIO.ID_FUNCIONARIO = CLIENTE_FUNCAO_FUNCIONARIO.ID_FUNCIONARIO ");
                    query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTE_FUNCAO_FUNCIONARIO.ID_SEG_CLIENTE_FUNCAO ");
                    where.Append(" WHERE TRUE ");
                    where.Append(" AND ASO.ID_EMPRESA = :VALUE13 ");

                    // verificando se foi passado o parâmetro do codigo do aso.
                    if (!String.IsNullOrWhiteSpace(atendimento.Codigo))
                        where.Append(" AND ASO.CODIGO_ASO = :VALUE3 ");

                    // verificando se foi informado o período de pesquisa
                    if (dataAsoFinal != null)
                        where.Append(" AND ASO.DATA_ASO BETWEEN :VALUE1 AND :VALUE2 ");

                    // verificando se foi informado o período de pesquisa de gravação
                    if (atendimento.DataGravacao != null && dataCriacaoFinal != null)
                        where.Append(" AND ASO.DATA_GRAVACAO BETWEEN :VALUE11 AND :VALUE12 ");

                    // verificando se foi informado a periodicidade na pesquisa
                    if (atendimento.Periodicidade != null)
                        where.Append(" AND ASO.ID_PERIODICIDADE  = :VALUE4 ");

                    //verificando se foi informado a situação do aso.
                    if(!String.IsNullOrEmpty(atendimento.Situacao))
                        where.Append(" AND ASO.SITUACAO = :VALUE9 ");

                    // verificando dados do funcionario.
                    if (atendimento.Funcionario != null && !String.IsNullOrWhiteSpace(atendimento.Funcionario.Cpf))
                    {
                        //join.Append(" INNER JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CFF ON CFF.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                        //join.Append(" = ASO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                        //join.Append(" INNER JOIN SEG_FUNCIONARIO F ON F.ID_FUNCIONARIO = CFF.ID_FUNCIONARIO ");

                        where.Append(" AND FUNCIONARIO.CPF = :VALUE5 ");
                        bolJoin = true;
                    }

                    if (atendimento.Funcionario != null && !String.IsNullOrEmpty(atendimento.Funcionario.Rg))
                    {
                        if (!bolJoin)
                        {
                            //join.Append(" INNER JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CFF ON CFF.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                            //join.Append(" = ASO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                            //join.Append(" INNER JOIN SEG_FUNCIONARIO F ON F.ID_FUNCIONARIO = CFF.ID_FUNCIONARIO ");
                        }

                        where.Append(" AND FUNCIONARIO.RG = :VALUE6 ");
                        bolJoin = true;
                        
                    }

                    if (atendimento.Funcionario != null && !String.IsNullOrEmpty(atendimento.Funcionario.Nome))
                    {
                        if (!bolJoin)
                        {
                            //join.Append(" INNER JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CFF ON CFF.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                            //join.Append(" = ASO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                            //join.Append(" INNER JOIN SEG_FUNCIONARIO F ON F.ID_FUNCIONARIO = CFF.ID_FUNCIONARIO ");
                        }

                        where.Append(" AND ASO.NOME_FUNCIONARIO LIKE :VALUE10 ");
                        bolJoin = true;

                    }

                    // analisando dados do cliente funcao
                    if (clienteFuncao !=null )
                    {
                        if (!bolJoin)
                        {
                            //join.Append(" INNER JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CFF ON CFF.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                            //join.Append(" = ASO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                            //join.Append(" INNER JOIN SEG_FUNCIONARIO F ON F.ID_FUNCIONARIO = CFF.ID_FUNCIONARIO ");
                        }

                        where.Append(" AND CLIENTE_FUNCAO_FUNCIONARIO.ID_SEG_CLIENTE_FUNCAO = :VALUE7 ");
                        bolJoin = true;
                    }

                    //analisar somente o cliente.
                    if (atendimento.Cliente != null)
                    {
                        if (!bolJoin)
                        {
                            //join.Append(" INNER JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CFF ON CFF.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                            //join.Append(" = ASO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                            //join.Append(" INNER JOIN SEG_FUNCIONARIO F ON F.ID_FUNCIONARIO = CFF.ID_FUNCIONARIO ");
                            //join.Append(" INNER JOIN SEG_CLIENTE_FUNCAO CF ON CF.ID_SEG_CLIENTE_FUNCAO = CFF.ID_SEG_CLIENTE_FUNCAO ");
                            
                        }
                        else
                        {
                            //join.Append(" INNER JOIN SEG_CLIENTE_FUNCAO CF ON CF.ID_SEG_CLIENTE_FUNCAO = CFF.ID_SEG_CLIENTE_FUNCAO ");
                        }

                        where.Append(" AND CLIENTE_FUNCAO.ID_CLIENTE = :VALUE8 ");
                    }
                }

                // montando a consulta final.

                query.Append(join.ToString() + where.ToString());
                query.Append(" ORDER BY ASO.CODIGO_ASO ASC ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[0].Value = atendimento.DataElaboracao;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[1].Value = dataAsoFinal;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[2].Value = atendimento.Codigo;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[3].Value = atendimento.Periodicidade != null ? atendimento.Periodicidade.Id : null;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[4].Value = atendimento.Funcionario != null ? atendimento.Funcionario.Cpf : string.Empty;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[5].Value = atendimento.Funcionario != null ? atendimento.Funcionario.Rg : string.Empty;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[6].Value = clienteFuncao != null ? clienteFuncao.Id : null;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[7].Value = atendimento.Cliente != null ? atendimento.Cliente.Id : null;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[8].Value = atendimento.Situacao;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[9].Value = atendimento.Funcionario != null ? "%" + atendimento.Funcionario.Nome + "%" : string.Empty;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[10].Value = atendimento.DataGravacao;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[11].Value = dataCriacaoFinal;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[12].Value = usuarioLogado.Empresa.Id;

                DataSet ds = new DataSet();
                adapter.Fill(ds, "Asos");
                return ds;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAsoByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAsoByFilter(Cliente cliente, DateTime dataPeriodoInicial, DateTime dataPeriodoFinal, Boolean check, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAsoByFilter");

            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" SELECT ASO.ID_ASO, ASO.ID_CLIENTE_FUNCAO_FUNCIONARIO, ASO.CODIGO_ASO, ASO.DATA_ASO, ");
                query.Append(" ASO.NOME_FUNCIONARIO, ASO.CPF_FUNCIONARIO, ASO.MATRICULA_FUNCIONARIO, ASO.RAZAO_EMPRESA, ");
                query.Append(" ASO.CNPJ_EMPRESA, ASO.ID_PERIODICIDADE, PERIODICIDADE.DESCRICAO, ASO.SITUACAO, ASO.DATA_FINALIZACAO, ASO.CONCLUSAO, :VALUE4 FROM SEG_ASO ASO ");
                query.Append(" INNER JOIN SEG_PERIODICIDADE PERIODICIDADE ON PERIODICIDADE.ID_PERIODICIDADE = ASO.ID_PERIODICIDADE ");
                query.Append(" INNER JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTEFUNCAOFUNCIONARIO ON CLIENTEFUNCAOFUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO = ASO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                query.Append(" INNER JOIN SEG_CLIENTE_FUNCAO CLIENTEFUNCAO ON CLIENTEFUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTEFUNCAOFUNCIONARIO.ID_SEG_CLIENTE_FUNCAO ");
                query.Append(" INNER JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = CLIENTEFUNCAO.ID_CLIENTE ");
                query.Append(" WHERE TRUE ");
                query.Append(" AND ASO.DATA_ASO BETWEEN :VALUE1 AND :VALUE2 ");
                query.Append(" AND CLIENTE.ID_CLIENTE = :VALUE3 ");
                query.Append(" AND ASO.SITUACAO = 'F' ");
                query.Append(" AND ASO.ID_ASO NOT IN (SELECT ID_ASO FROM SEG_ESOCIAL_LOTE_ASO WHERE SITUACAO = 'E') ");
                query.Append(" ORDER BY ASO.CODIGO_ASO ASC ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[0].Value = dataPeriodoInicial;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[1].Value = dataPeriodoFinal;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[2].Value = cliente.Id;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Boolean));
                adapter.SelectCommand.Parameters[3].Value = check;
                    
                DataSet ds = new DataSet();
                adapter.Fill(ds, "Asos");
                return ds;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAsoByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
            
        public HashSet<GheFonteAgenteExame> findAllExamesByPcmsoByASOByIdadeByTipoByAlturaByConfinado(Estudo estudo, Int32 idade, Periodicidade periodicidade, List<Ghe> ghesInPcmso, Boolean? altura, Boolean? confinado, bool? eletricidade, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllExamesByPcmsoByASO");

            try
            {
                StringBuilder query = new StringBuilder();

                HashSet<GheFonteAgenteExame> examesInPcmso = new HashSet<GheFonteAgenteExame>();

                if (ghesInPcmso.Count > 0)
                {
                    query.Append(" SELECT GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME, EXAME.ID_EXAME, EXAME.DESCRICAO, EXAME.SITUACAO, EXAME.LABORATORIO, EXAME.PRECO, ");
                    query.Append(" EXAME.PRIORIDADE, EXAME.CUSTO, GHE_FONTE_AGENTE_EXAME.IDADE_EXAME, GHE_FONTE_AGENTE.ID_GHE_FONTE_AGENTE, AGENTE.ID_AGENTE, AGENTE.DESCRICAO, ");
                    query.Append(" RISCO.ID_RISCO,RISCO.DESCRICAO, GHE_FONTE_AGENTE_EXAME.IDADE_EXAME, GHE_FONTE_AGENTE_EXAME.SITUACAO, EXAME.PRECO, EXAME.PRIORIDADE, EXAME.CUSTO, EXAME.EXTERNO, EXAME.LIBERA_DOCUMENTO, GHE_FONTE_AGENTE_EXAME.CONFINADO, GHE_FONTE_AGENTE_EXAME.ALTURA, EXAME.CODIGO_TUSS, EXAME.EXAME_COMPLEMENTAR, EXAME.PERIODO_VENCIMENTO, EXAME.PADRAO_CONTRATO,  GHE_FONTE_AGENTE_EXAME.ELETRICIDADE "); 
                    query.Append(" FROM SEG_ESTUDO ESTUDO  ");
                    query.Append(" JOIN SEG_GHE GHE ON GHE.ID_ESTUDO = ESTUDO.ID_ESTUDO");
                    query.Append(" JOIN SEG_GHE_FONTE GHE_FONTE ON GHE_FONTE.ID_GHE = GHE.ID_GHE AND GHE_FONTE.SITUACAO = 'A' ");
                    query.Append(" JOIN SEG_GHE_FONTE_AGENTE GHE_FONTE_AGENTE ON GHE_FONTE_AGENTE.ID_GHE_FONTE = GHE_FONTE.ID_GHE_FONTE AND GHE_FONTE_AGENTE.SITUACAO = 'A' ");
                    query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GHE_FONTE_AGENTE_EXAME ON GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE = GHE_FONTE_AGENTE.ID_GHE_FONTE_AGENTE AND GHE_FONTE_AGENTE_EXAME.SITUACAO = 'A' ");
                    query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME_PERIODICIDADE GHE_FONTE_AGENTE_EXAME_PERIODICIDADE ON GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.ID_GHE_FONTE_AGENTE_EXAME = GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME AND GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.SITUACAO = 'A' ");
                    query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = GHE_FONTE_AGENTE_EXAME.ID_EXAME ");
                    query.Append(" JOIN SEG_AGENTE AGENTE ON AGENTE.ID_AGENTE = GHE_FONTE_AGENTE.ID_AGENTE ");
                    query.Append(" JOIN SEG_RISCO RISCO ON RISCO.ID_RISCO = AGENTE.ID_RISCO ");
                    query.Append(" WHERE ESTUDO.ID_ESTUDO = :VALUE1 ");

                    //Usando periódico como anual e bianual
                    if (periodicidade.Id == 2)
                        query.Append(" AND GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.ID_PERIODICIDADE IN (1,2) ");
                    else
                        query.Append(" AND GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.ID_PERIODICIDADE = :VALUE2 ");

                    query.Append(" AND GHE_FONTE_AGENTE_EXAME.IDADE_EXAME <= :VALUE3 ");
                    query.Append(" AND GHE.ID_GHE IN (");

                    int contador = ghesInPcmso.Count + 1;
                    foreach (Ghe ghe in ghesInPcmso)
                    {
                        contador --;
                        query.Append(ghe.Id);

                        if (ghesInPcmso.Count > 1)
                            if (contador > 1)
                                query.Append(",");
                    }

                    query.Append(")");
                    query.Append(" AND (GHE_FONTE_AGENTE_EXAME.ALTURA = FALSE AND GHE_FONTE_AGENTE_EXAME.CONFINADO = FALSE AND GHE_FONTE_AGENTE_EXAME.ELETRICIDADE = FALSE) ");
                    
                    
                    /* testando para saber se foi passado algum parametro de
                     * altura ou espaço confinado. */

                    if (altura != null || confinado != null || eletricidade != null)
                    {
                        /* nesse caso o colaborador tem pelo menos alguma atividade
                         * especial selecionada pelo usuario, será necessário
                         * usar o union para trazer além dos exames normais os exames com as determinadas caracteristicas*/

                        if (altura == true && confinado == true && eletricidade == true)
                        {


                            query.Append(" UNION ( ");
                            query.Append(" SELECT GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME, EXAME.ID_EXAME, EXAME.DESCRICAO, EXAME.SITUACAO, EXAME.LABORATORIO, EXAME.PRECO, ");
                            query.Append(" EXAME.PRIORIDADE, EXAME.CUSTO, GHE_FONTE_AGENTE_EXAME.IDADE_EXAME, GHE_FONTE_AGENTE.ID_GHE_FONTE_AGENTE, AGENTE.ID_AGENTE, AGENTE.DESCRICAO, ");
                            query.Append(" RISCO.ID_RISCO, RISCO.DESCRICAO, GHE_FONTE_AGENTE_EXAME.IDADE_EXAME, GHE_FONTE_AGENTE_EXAME.SITUACAO, EXAME.PRECO, EXAME.PRIORIDADE, EXAME.CUSTO, EXAME.EXTERNO, EXAME.LIBERA_DOCUMENTO, GHE_FONTE_AGENTE_EXAME.CONFINADO, GHE_FONTE_AGENTE_EXAME.ALTURA, EXAME.CODIGO_TUSS, EXAME.EXAME_COMPLEMENTAR, EXAME.PERIODO_VENCIMENTO, EXAME.PADRAO_CONTRATO, GHE_FONTE_AGENTE_EXAME.ELETRICIDADE ");
                            query.Append(" FROM SEG_ESTUDO ESTUDO  ");
                            query.Append(" JOIN SEG_GHE GHE ON GHE.ID_ESTUDO = ESTUDO.ID_ESTUDO ");
                            query.Append(" JOIN SEG_GHE_FONTE GHE_FONTE ON GHE_FONTE.ID_GHE = GHE.ID_GHE AND GHE_FONTE.SITUACAO = 'A' ");
                            query.Append(" JOIN SEG_GHE_FONTE_AGENTE GHE_FONTE_AGENTE ON GHE_FONTE_AGENTE.ID_GHE_FONTE = GHE_FONTE.ID_GHE_FONTE AND GHE_FONTE_AGENTE.SITUACAO = 'A' ");
                            query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GHE_FONTE_AGENTE_EXAME ON GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE = GHE_FONTE_AGENTE.ID_GHE_FONTE_AGENTE AND GHE_FONTE_AGENTE_EXAME.SITUACAO = 'A' ");
                            query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME_PERIODICIDADE GHE_FONTE_AGENTE_EXAME_PERIODICIDADE ON GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.ID_GHE_FONTE_AGENTE_EXAME = GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME AND GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.SITUACAO = 'A' ");
                            query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = GHE_FONTE_AGENTE_EXAME.ID_EXAME ");
                            query.Append(" JOIN SEG_AGENTE AGENTE ON AGENTE.ID_AGENTE = GHE_FONTE_AGENTE.ID_AGENTE ");
                            query.Append(" JOIN SEG_RISCO RISCO ON RISCO.ID_RISCO = AGENTE.ID_RISCO ");
                            query.Append(" WHERE ESTUDO.ID_ESTUDO = :VALUE1 ");

                            //Usando periódico como anual e bianual
                            if (periodicidade.Id == 2)
                                query.Append(" AND GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.ID_PERIODICIDADE IN (1,2) ");
                            else
                                query.Append(" AND GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.ID_PERIODICIDADE = :VALUE2 ");
                            
                            query.Append(" AND GHE_FONTE_AGENTE_EXAME.IDADE_EXAME <= :VALUE3 ");
                            query.Append(" AND GHE.ID_GHE IN (");

                            int contadorAtividade = ghesInPcmso.Count + 1;
                            foreach (Ghe ghe in ghesInPcmso)
                            {
                                contadorAtividade--;
                                query.Append(ghe.Id);

                                if (ghesInPcmso.Count > 1)
                                    if (contadorAtividade > 1)
                                        query.Append(",");
                            }

                            query.Append(")");
                            query.Append(" AND (GHE_FONTE_AGENTE_EXAME.ALTURA = :VALUE4 OR GHE_FONTE_AGENTE_EXAME.CONFINADO = :VALUE5 OR GHE_FONTE_AGENTE_EXAME.ELETRICIDADE = :VALUE6) ");
                            query.Append(") ");
                        }
                        else
                        {
                            query.Append(" UNION ( ");
                            query.Append(" SELECT GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME, EXAME.ID_EXAME, EXAME.DESCRICAO, EXAME.SITUACAO, EXAME.LABORATORIO, EXAME.PRECO, ");
                            query.Append(" EXAME.PRIORIDADE, EXAME.CUSTO, GHE_FONTE_AGENTE_EXAME.IDADE_EXAME, GHE_FONTE_AGENTE.ID_GHE_FONTE_AGENTE, AGENTE.ID_AGENTE, AGENTE.DESCRICAO, ");
                            query.Append(" RISCO.ID_RISCO, RISCO.DESCRICAO, GHE_FONTE_AGENTE_EXAME.IDADE_EXAME, GHE_FONTE_AGENTE_EXAME.SITUACAO, EXAME.PRECO, EXAME.PRIORIDADE, EXAME.CUSTO, EXAME.EXTERNO,");
                            query.Append(" EXAME.LIBERA_DOCUMENTO, GHE_FONTE_AGENTE_EXAME.CONFINADO, GHE_FONTE_AGENTE_EXAME.ALTURA, EXAME.CODIGO_TUSS, EXAME.EXAME_COMPLEMENTAR, EXAME.PERIODO_VENCIMENTO, EXAME.PADRAO_CONTRATO, GHE_FONTE_AGENTE_EXAME.ELETRICIDADE ");
                            query.Append(" FROM SEG_ESTUDO ESTUDO ");
                            query.Append(" JOIN SEG_GHE GHE ON GHE.ID_ESTUDO = ESTUDO.ID_ESTUDO ");
                            query.Append(" JOIN SEG_GHE_FONTE GHE_FONTE ON GHE_FONTE.ID_GHE = GHE.ID_GHE AND GHE_FONTE.SITUACAO = 'A' ");
                            query.Append(" JOIN SEG_GHE_FONTE_AGENTE GHE_FONTE_AGENTE ON GHE_FONTE_AGENTE.ID_GHE_FONTE = GHE_FONTE.ID_GHE_FONTE AND GHE_FONTE_AGENTE.SITUACAO = 'A' ");
                            query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GHE_FONTE_AGENTE_EXAME ON GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE = GHE_FONTE_AGENTE.ID_GHE_FONTE_AGENTE AND GHE_FONTE_AGENTE_EXAME.SITUACAO = 'A' ");
                            query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME_PERIODICIDADE GHE_FONTE_AGENTE_EXAME_PERIODICIDADE ON GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.ID_GHE_FONTE_AGENTE_EXAME = GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME AND GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.SITUACAO = 'A' ");
                            query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = GHE_FONTE_AGENTE_EXAME.ID_EXAME ");
                            query.Append(" JOIN SEG_AGENTE AGENTE ON AGENTE.ID_AGENTE = GHE_FONTE_AGENTE.ID_AGENTE ");
                            query.Append(" JOIN SEG_RISCO RISCO ON RISCO.ID_RISCO = AGENTE.ID_RISCO ");
                            query.Append(" WHERE ESTUDO.ID_ESTUDO = :VALUE1 ");

                            //Usando periódico como anual e bianual
                            if (periodicidade.Id == 2)
                                query.Append(" AND GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.ID_PERIODICIDADE IN (1,2) ");
                            else
                                query.Append(" AND GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.ID_PERIODICIDADE = :VALUE2 ");

                            query.Append(" AND GHE_FONTE_AGENTE_EXAME.IDADE_EXAME <= :VALUE3 ");
                            query.Append(" AND GHE.ID_GHE IN (");

                            int contadorAtividade = ghesInPcmso.Count + 1;
                            foreach (Ghe ghe in ghesInPcmso)
                            {
                                contadorAtividade--;
                                query.Append(ghe.Id);

                                if (ghesInPcmso.Count > 1)
                                    if (contadorAtividade > 1)
                                        query.Append(",");
                            }

                            query.Append(")");
                            query.Append(" AND (GHE_FONTE_AGENTE_EXAME.ALTURA = :VALUE4 AND GHE_FONTE_AGENTE_EXAME.CONFINADO = :VALUE5 AND GHE_FONTE_AGENTE_EXAME.ELETRICIDADE = :VALUE6) ");
                            query.Append(") ");
                            
                        }
                    }

                    NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                    command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                    command.Parameters[0].Value = estudo.Id;

                    command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                    command.Parameters[1].Value = periodicidade.Id;

                    command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                    command.Parameters[2].Value = idade;
                    
                    command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Boolean));
                    command.Parameters[3].Value = altura != null ? altura : null;

                    command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Boolean));
                    command.Parameters[4].Value = confinado != null ? confinado : null;

                    command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Boolean));
                    command.Parameters[5].Value = eletricidade != null ? eletricidade : null;
                    
                    NpgsqlDataReader dr = command.ExecuteReader();

                    while (dr.Read())
                    {
                        examesInPcmso.Add(new GheFonteAgenteExame(dr.GetInt64(0), new Exame(dr.GetInt64(1), dr.GetString(2), dr.GetString(3), dr.GetBoolean(4),dr.GetDecimal(16), dr.GetInt32(17),dr.GetDecimal(18),dr.GetBoolean(19), dr.GetBoolean(20), dr.IsDBNull(23) ? string.Empty : dr.GetString(23), dr.GetBoolean(24), dr.IsDBNull(25) ? (int?)null : dr.GetInt32(25), dr.GetBoolean(26)), new GheFonteAgente(dr.GetInt64(9), null, null, null, null, new Agente(dr.GetInt64(10), new Risco(dr.GetInt64(12),dr.GetString(13)), dr.GetString(11), null,null,null,null,false, string.Empty, string.Empty, string.Empty), string.Empty, string.Empty), dr.GetInt32(14),dr.GetString(15), dr.GetBoolean(21), dr.GetBoolean(22), dr.GetBoolean(27)));
                    }
                }

                return examesInPcmso;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllExamesByPcmsoByASO", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Aso insert(Aso aso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            try
            {
                StringBuilder query = new StringBuilder();
                
                aso.Id = recuperaProximoId(dbConnection);

                aso.Codigo = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + Convert.ToString(aso.Id).PadLeft(6, '0');

              
                query.Append(" INSERT INTO SEG_ASO( ID_ASO, ID_MEDICO_EXAMINADOR, ID_CLIENTE_FUNCAO_FUNCIONARIO, CODIGO_ASO, DATA_ASO, ");
                query.Append(" NOME_FUNCIONARIO, RG_FUNCIONARIO, DATANASC_FUNCIONARIO, CPF_FUNCIONARIO, ");
                query.Append(" TIPOSAN_FUNCIONARIO, FTRH_FUNCIONARIO, MATRICULA_FUNCIONARIO, RAZAO_EMPRESA, ");
                query.Append(" CNPJ_EMPRESA, ENDERECO_EMPRESA, NUMERO_EMPRESA, COMPLEMENTO_EMPRESA, BAIRRO_EMPRESA, ");
                query.Append(" CEP_EMPRESA, CIDADE_EMPRESA, UF_EMPRESA, ID_PERIODICIDADE, SITUACAO, ID_MEDICO_COORDENADOR, ");
                query.Append(" CRM_COORDENADOR, TELEFONE1_COORDENADOR, TELEFONE2_COORDENADOR, NOME_COORDENADOR, ");
                query.Append(" CRM_EXAMINADOR, NOME_EXAMINADOR, TELEFONE1_EXAMINADOR, TELEFONE2_EXAMINADOR, ");
                query.Append(" ID_USUARIO_CRIADOR, ID_ESTUDO, OBS_ASO, DATA_GRAVACAO, SENHA, ID_EMPRESA, ID_CENTROCUSTO, DATA_VENCIMENTO, NUMERO_PO, PRIORIDADE, PIS, FUNCAO_FUNCIONARIO, ALTURA, CONFINADO, ELETRICIDADE) VALUES (:VALUE1, :VALUE2, :VALUE3, ");
                query.Append(" :VALUE4, :VALUE5, :VALUE6, :VALUE7, :VALUE8, ");
                query.Append(" :VALUE9, :VALUE10, :VALUE11, :VALUE12, :VALUE13, :VALUE14, :VALUE15, :VALUE16, :VALUE17, ");
                query.Append(" :VALUE18, :VALUE19, :VALUE20, :VALUE21, :VALUE22, :VALUE23, :VALUE24, :VALUE25, ");
                query.Append(" :VALUE26, :VALUE27, ");
                query.Append(" :VALUE28, :VALUE29, :VALUE30, :VALUE31, :VALUE32, :VALUE33, :VALUE34, :VALUE35, NOW(), :VALUE36, :VALUE37, :VALUE38, :VALUE39, :VALUE40, :VALUE41, :VALUE42, :VALUE43, :VALUE44, :VALUE45, :VALUE46 )");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = aso.Examinador != null ? aso.Examinador.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = aso.ClienteFuncaoFuncionario.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = aso.Codigo;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Timestamp));
                command.Parameters[4].Value = aso.DataElaboracao;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = aso.Funcionario.Nome;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = aso.Funcionario.Rg;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Date));
                command.Parameters[7].Value = aso.Funcionario.DataNascimento;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                command.Parameters[8].Value = aso.Funcionario.Cpf;

                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                command.Parameters[9].Value = aso.Funcionario.TipoSangue;

                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Varchar));
                command.Parameters[10].Value = aso.Funcionario.FatorRh;

                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Varchar));
                command.Parameters[11].Value = aso.ClienteFuncaoFuncionario.Matricula;

                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Varchar));
                command.Parameters[12].Value = aso.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.RazaoSocial.ToUpper();

                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Varchar));
                command.Parameters[13].Value = aso.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Cnpj;

                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Varchar));
                command.Parameters[14].Value = aso.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Endereco.ToUpper();

                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Varchar));
                command.Parameters[15].Value = aso.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Numero.ToUpper();

                command.Parameters.Add(new NpgsqlParameter("VALUE17", NpgsqlDbType.Varchar));
                command.Parameters[16].Value = aso.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Complemento.ToUpper();

                command.Parameters.Add(new NpgsqlParameter("VALUE18", NpgsqlDbType.Varchar));
                command.Parameters[17].Value = aso.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Bairro.ToUpper();

                command.Parameters.Add(new NpgsqlParameter("VALUE19", NpgsqlDbType.Varchar));
                command.Parameters[18].Value = aso.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Cep;

                command.Parameters.Add(new NpgsqlParameter("VALUE20", NpgsqlDbType.Varchar));
                command.Parameters[19].Value = aso.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.CidadeIbge.Nome.ToUpper();

                command.Parameters.Add(new NpgsqlParameter("VALUE21", NpgsqlDbType.Varchar));
                command.Parameters[20].Value = aso.ClienteFuncaoFuncionario.ClienteFuncao.Cliente.Uf;

                command.Parameters.Add(new NpgsqlParameter("VALUE22", NpgsqlDbType.Integer));
                command.Parameters[21].Value = aso.Periodicidade.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE23", NpgsqlDbType.Varchar));
                command.Parameters[22].Value = aso.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE24", NpgsqlDbType.Integer));
                command.Parameters[23].Value = aso.Coordenador != null ? aso.Coordenador.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE25", NpgsqlDbType.Varchar));
                command.Parameters[24].Value = aso.Coordenador != null ? aso.Coordenador.Crm : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE26", NpgsqlDbType.Varchar));
                command.Parameters[25].Value = aso.Coordenador != null ? aso.Coordenador.Telefone1.ToUpper() : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE27", NpgsqlDbType.Varchar));
                command.Parameters[26].Value = aso.Coordenador != null ? aso.Coordenador.Telefone2.ToUpper() : String.Empty;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE28", NpgsqlDbType.Varchar));
                command.Parameters[27].Value = aso.Coordenador != null ? aso.Coordenador.Nome.ToUpper() : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE29", NpgsqlDbType.Varchar));
                command.Parameters[28].Value = aso.Examinador != null ? aso.Examinador.Crm : String.Empty;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE30", NpgsqlDbType.Varchar));
                command.Parameters[29].Value = aso.Examinador != null ? aso.Examinador.Nome.ToUpper() : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE31", NpgsqlDbType.Varchar));
                command.Parameters[30].Value = aso.Examinador != null ? aso.Examinador.Telefone1.ToUpper() : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE32", NpgsqlDbType.Varchar));
                command.Parameters[31].Value = aso.Examinador != null ? aso.Examinador.Telefone2.ToUpper() : String.Empty;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE33", NpgsqlDbType.Integer));
                command.Parameters[32].Value = aso.Criador.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE34", NpgsqlDbType.Integer));
                command.Parameters[33].Value = aso.Estudo != null ? aso.Estudo.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE35", NpgsqlDbType.Varchar));
                command.Parameters[34].Value = aso.ObservacaoAso;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE36", NpgsqlDbType.Varchar));
                command.Parameters[35].Value = aso.Senha;

                command.Parameters.Add(new NpgsqlParameter("VALUE37", NpgsqlDbType.Integer));
                command.Parameters[36].Value = aso.Empresa.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE38", NpgsqlDbType.Integer));
                command.Parameters[37].Value = aso.CentroCusto == null ? null : aso.CentroCusto.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE39", NpgsqlDbType.Date));
                command.Parameters[38].Value = aso.DataVencimento;

                command.Parameters.Add(new NpgsqlParameter("VALUE40", NpgsqlDbType.Varchar));
                command.Parameters[39].Value = aso.CodigoPo;

                command.Parameters.Add(new NpgsqlParameter("VALUE41", NpgsqlDbType.Boolean));
                command.Parameters[40].Value = aso.Prioridade;

                command.Parameters.Add(new NpgsqlParameter("VALUE42", NpgsqlDbType.Varchar));
                command.Parameters[41].Value = aso.Pis;

                command.Parameters.Add(new NpgsqlParameter("VALUE43", NpgsqlDbType.Varchar));
                command.Parameters[42].Value = aso.FuncaoFuncionario;

                command.Parameters.Add(new NpgsqlParameter("VALUE44", NpgsqlDbType.Boolean));
                command.Parameters[43].Value = aso.Altura;

                command.Parameters.Add(new NpgsqlParameter("VALUE45", NpgsqlDbType.Boolean));
                command.Parameters[44].Value = aso.Confinado;

                command.Parameters.Add(new NpgsqlParameter("VALUE46", NpgsqlDbType.Boolean));
                command.Parameters[45].Value = aso.Eletricidade;

                command.ExecuteNonQuery();

                return aso;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert ", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoIdASo");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_ASO_ID_ASO_SEQ'))", dbConnection);
                                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoIdASo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Aso findAsoByCodigo(String codigo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAsoByCodigo");

            try
            {
                StringBuilder query = new StringBuilder();

                Aso aso = null;

                query.Append(" SELECT ATENDIMENTO.ID_ASO, ATENDIMENTO.ID_MEDICO_EXAMINADOR, ATENDIMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO, ATENDIMENTO.CODIGO_ASO, ");
                query.Append(" ATENDIMENTO.DATA_ASO, ATENDIMENTO.NOME_FUNCIONARIO, ATENDIMENTO.RG_FUNCIONARIO, ATENDIMENTO.DATANASC_FUNCIONARIO, ATENDIMENTO.CPF_FUNCIONARIO, ");
                query.Append(" ATENDIMENTO.TIPOSAN_FUNCIONARIO, ATENDIMENTO.FTRH_FUNCIONARIO, ATENDIMENTO.MATRICULA_FUNCIONARIO, ATENDIMENTO.RAZAO_EMPRESA, ATENDIMENTO.CNPJ_EMPRESA, ");
                query.Append(" ATENDIMENTO.ENDERECO_EMPRESA, ATENDIMENTO.NUMERO_EMPRESA, ATENDIMENTO.COMPLEMENTO_EMPRESA, ATENDIMENTO.BAIRRO_EMPRESA, ATENDIMENTO.CEP_EMPRESA, ");
                query.Append(" ATENDIMENTO.CIDADE_EMPRESA, ATENDIMENTO.UF_EMPRESA, ATENDIMENTO.ID_PERIODICIDADE, ATENDIMENTO.SITUACAO, ATENDIMENTO.ID_MEDICO_COORDENADOR, ");
                query.Append(" ATENDIMENTO.CRM_COORDENADOR, ATENDIMENTO.TELEFONE1_COORDENADOR, ATENDIMENTO.TELEFONE2_COORDENADOR, ATENDIMENTO.NOME_COORDENADOR, ");
                query.Append(" ATENDIMENTO.CRM_EXAMINADOR, ATENDIMENTO.NOME_EXAMINADOR, ATENDIMENTO.TELEFONE1_EXAMINADOR, ATENDIMENTO.TELEFONE2_EXAMINADOR, ");
                query.Append(" ATENDIMENTO.ID_USUARIO_CRIADOR, ATENDIMENTO.ID_USUARIO_FINALIZADOR, ");
                query.Append(" ATENDIMENTO.ID_ULT_USUARIO_ALTERACAO, ATENDIMENTO.ID_ESTUDO, ATENDIMENTO.OBS_ALTERACAO, ATENDIMENTO.OBS_ASO, ATENDIMENTO.CONCLUSAO, ATENDIMENTO.DATA_GRAVACAO, ATENDIMENTO.DATA_CANCELAMENTO, ");
                query.Append(" ATENDIMENTO.ID_USUARIO_CANCELAMENTO, ATENDIMENTO.DATA_FINALIZACAO, ATENDIMENTO.ID_EMPRESA, ATENDIMENTO.OBS_CANCELAMENTO, ATENDIMENTO.ID_CENTROCUSTO, CLIENTE.ID_CLIENTE, ATENDIMENTO.DATA_VENCIMENTO, ATENDIMENTO.NUMERO_PO, ATENDIMENTO.PRIORIDADE, ATENDIMENTO.PIS, ATENDIMENTO.GHE_SETOR_AVULSO, ATENDIMENTO.FUNCAO_FUNCIONARIO ");
                query.Append(" FROM SEG_ASO ATENDIMENTO ");
                query.Append(" LEFT JOIN SEG_CENTROCUSTO CENTROCUSTO ON CENTROCUSTO.ID = ATENDIMENTO.ID_CENTROCUSTO ");
                query.Append(" LEFT JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = CENTROCUSTO.ID_CLIENTE ");
                query.Append(" WHERE ATENDIMENTO.CODIGO_ASO = :VALUE1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar)); 
                command.Parameters[0].Value = codigo;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    aso = new Aso(
                        dr.GetInt64(0), 
                        dr.IsDBNull(1) ? null : new Medico(dr.GetInt64(1), dr.GetString(29), dr.GetString(28), ApplicationConstants.ATIVO, dr.GetString(30), dr.GetString(31), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty), 
                        dr.IsDBNull(2) ? null : new ClienteFuncaoFuncionario(dr.GetInt64(2), null, null, string.Empty, null, null, dr.IsDBNull(11) ? string.Empty : dr.GetString(11), false, string.Empty, string.Empty, false, true), 
                        dr.GetString(3), 
                        dr.GetDateTime(4), 
                        new Funcionario(dr.GetString(5), dr.GetString(6), dr.GetDateTime(7), dr.GetString(8), dr.GetString(9), dr.GetString(10)), 
                        new Cliente(null, dr.GetString(12), String.Empty, dr.GetString(13), String.Empty, dr.GetString(14), dr.GetString(15), dr.GetString(16), dr.GetString(17), dr.GetString(18), dr.GetString(20), String.Empty, String.Empty, String.Empty, String.Empty, null, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, 0, 0, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, null, String.Empty, false, String.Empty, false, null, null, false, false, false, false, new CidadeIbge(null, String.Empty, dr.GetString(19), String.Empty), null, false, false, null, string.Empty, false, false, false, false, false, false), 
                        new Periodicidade(dr.GetInt64(21), String.Empty, null), 
                        dr.GetString(22), 
                        dr.IsDBNull(23) ? null : new Medico(dr.GetInt64(23), dr.GetString(27), dr.GetString(24), ApplicationConstants.ATIVO, dr.GetString(25), dr.GetString(26), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty), 
                        new Usuario(dr.GetInt64(32)), 
                        dr.IsDBNull(36) ? string.Empty : dr.GetString(36), 
                        dr.IsDBNull(35) ? null : new Estudo(dr.GetInt64(35)), 
                        dr.IsDBNull(33) ? null : new Usuario(dr.GetInt64(33)), 
                        dr.IsDBNull(41) ? null : new Usuario(dr.GetInt64(41)), 
                        dr.IsDBNull(42) ? (DateTime?)null : dr.GetDateTime(42), 
                        dr.IsDBNull(40) ? (DateTime?)null : dr.GetDateTime(40), 
                        dr.IsDBNull(39) ? (DateTime?)null : dr.GetDateTime(39), 
                        dr.IsDBNull(37) ? string.Empty : dr.GetString(37), 
                        dr.IsDBNull(38) ? string.Empty : dr.GetString(38), 
                        null, 
                        null, 
                        dr.IsDBNull(43) ? null : new Empresa(dr.GetInt64(43)), 
                        dr.IsDBNull(44) ? string.Empty : dr.GetString(44), 
                        dr.IsDBNull(34) ? null : new Usuario(dr.GetInt64(34)), 
                        dr.IsDBNull(45) ? null : new CentroCusto(dr.GetInt64(45), new Cliente(dr.GetInt64(46)), string.Empty, string.Empty), 
                        dr.IsDBNull(47) ? null : (DateTime?)dr.GetDateTime(47), 
                        dr.IsDBNull(48) ? string.Empty : dr.GetString(48), 
                        dr.GetBoolean(49), 
                        dr.IsDBNull(50) ? string.Empty : dr.GetString(50), 
                        dr.IsDBNull(51) ? string.Empty : dr.GetString(51), 
                        dr.IsDBNull(52) ? string.Empty : dr.GetString(52));
                }

                return aso;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAsoByCodigo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Aso findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAsoById");

            try
            {
                StringBuilder query = new StringBuilder();

                Aso aso = null;

                query.Append(" SELECT ATENDIMENTO.ID_ASO, ATENDIMENTO.ID_MEDICO_EXAMINADOR, ATENDIMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO, ATENDIMENTO.CODIGO_ASO, ");
                query.Append(" ATENDIMENTO.DATA_ASO, ATENDIMENTO.NOME_FUNCIONARIO, ATENDIMENTO.RG_FUNCIONARIO, ATENDIMENTO.DATANASC_FUNCIONARIO, ATENDIMENTO.CPF_FUNCIONARIO, ");
                query.Append(" ATENDIMENTO.TIPOSAN_FUNCIONARIO, ATENDIMENTO.FTRH_FUNCIONARIO, ATENDIMENTO.MATRICULA_FUNCIONARIO, ATENDIMENTO.RAZAO_EMPRESA, ATENDIMENTO.CNPJ_EMPRESA, ");
                query.Append(" ATENDIMENTO.ENDERECO_EMPRESA, ATENDIMENTO.NUMERO_EMPRESA, ATENDIMENTO.COMPLEMENTO_EMPRESA, ATENDIMENTO.BAIRRO_EMPRESA, ATENDIMENTO.CEP_EMPRESA, ");
                query.Append(" ATENDIMENTO.CIDADE_EMPRESA, ATENDIMENTO.UF_EMPRESA, ATENDIMENTO.ID_PERIODICIDADE, ATENDIMENTO.SITUACAO, ATENDIMENTO.ID_MEDICO_COORDENADOR, ");
                query.Append(" ATENDIMENTO.CRM_COORDENADOR, ATENDIMENTO.TELEFONE1_COORDENADOR, ATENDIMENTO.TELEFONE2_COORDENADOR, ATENDIMENTO.NOME_COORDENADOR, ");
                query.Append(" ATENDIMENTO.CRM_EXAMINADOR, ATENDIMENTO.NOME_EXAMINADOR, ATENDIMENTO.TELEFONE1_EXAMINADOR, ATENDIMENTO.TELEFONE2_EXAMINADOR, ");
                query.Append(" ATENDIMENTO.ID_USUARIO_CRIADOR, ATENDIMENTO.ID_USUARIO_FINALIZADOR, ");
                query.Append(" ATENDIMENTO.ID_ULT_USUARIO_ALTERACAO, ATENDIMENTO.ID_ESTUDO, ATENDIMENTO.OBS_ALTERACAO, ATENDIMENTO.OBS_ASO, ATENDIMENTO.CONCLUSAO, ATENDIMENTO.DATA_GRAVACAO, ATENDIMENTO.DATA_CANCELAMENTO, ");
                query.Append(" ATENDIMENTO.ID_USUARIO_CANCELAMENTO, ATENDIMENTO.DATA_FINALIZACAO, ATENDIMENTO.ID_EMPRESA, ATENDIMENTO.OBS_CANCELAMENTO, ATENDIMENTO.ID_CENTROCUSTO, CLIENTE_IN_CENTROCUSTO.ID_CLIENTE, ATENDIMENTO.DATA_VENCIMENTO, ATENDIMENTO.NUMERO_PO, ATENDIMENTO.PRIORIDADE, ATENDIMENTO.PIS, ATENDIMENTO.SENHA, ATENDIMENTO.GHE_SETOR_AVULSO, ATENDIMENTO.FUNCAO_FUNCIONARIO, ATENDIMENTO.ALTURA, ATENDIMENTO.CONFINADO, CLIENTE_IN_ATENDIMENTO.ID_CLIENTE, ATENDIMENTO.ELETRICIDADE, TIPO.DESCRICAO AS TIPO, CLIENTE_IN_ATENDIMENTO.SIMPLES_NACIONAL ");
                query.Append(" FROM SEG_ASO ATENDIMENTO ");
                query.Append(" LEFT JOIN SEG_CENTROCUSTO CENTROCUSTO ON CENTROCUSTO.ID = ATENDIMENTO.ID_CENTROCUSTO ");
                query.Append(" LEFT JOIN SEG_CLIENTE CLIENTE_IN_CENTROCUSTO ON CLIENTE_IN_CENTROCUSTO.ID_CLIENTE = CENTROCUSTO.ID_CLIENTE ");
                query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTE_FUNCAO_FUNCIONARIO ON CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO = ATENDIMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTE_FUNCAO_FUNCIONARIO.ID_SEG_CLIENTE_FUNCAO ");
                query.Append(" LEFT JOIN SEG_CLIENTE CLIENTE_IN_ATENDIMENTO ON CLIENTE_IN_ATENDIMENTO.ID_CLIENTE = CLIENTE_FUNCAO.ID_CLIENTE ");
                query.Append(" LEFT JOIN SEG_PERIODICIDADE TIPO ON TIPO.ID_PERIODICIDADE = ATENDIMENTO.ID_PERIODICIDADE ");
                query.Append(" WHERE ATENDIMENTO.ID_ASO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer)); // id_aso

                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    aso = new Aso(
                        dr.GetInt64(0),
                        dr.IsDBNull(1) ? null : new Medico(dr.GetInt64(1), dr.GetString(29), dr.GetString(28), ApplicationConstants.ATIVO, dr.GetString(30), dr.GetString(31), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty),
                        new ClienteFuncaoFuncionario(dr.GetInt64(2), null, null, string.Empty, null, null, dr.IsDBNull(11) ? string.Empty : dr.GetString(11), false, string.Empty, string.Empty, false, true),
                        dr.GetString(3), 
                        dr.GetDateTime(4), 
                        new Funcionario(dr.GetString(5), dr.GetString(6), dr.GetDateTime(7), dr.GetString(8), dr.GetString(9), dr.GetString(10)),
                        new Cliente(dr.GetInt64(56), dr.GetString(12), String.Empty, dr.GetString(13), String.Empty, dr.GetString(14), dr.GetString(15), dr.GetString(16), dr.GetString(17), dr.GetString(18), dr.GetString(20), String.Empty, String.Empty, String.Empty, String.Empty, null, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, 0, 0, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, null, String.Empty, false, String.Empty, false, null, null, false, false, false, false, new CidadeIbge(null, String.Empty, dr.GetString(19), String.Empty), null, false, false, null, string.Empty, false, false, false, false, false, dr.GetBoolean(59)),
                        new Periodicidade(dr.GetInt64(21), dr.GetString(58), null), 
                        dr.GetString(22), 
                        dr.IsDBNull(23) ? null : new Medico(dr.GetInt64(23), dr.GetString(27), dr.GetString(24), ApplicationConstants.ATIVO, dr.GetString(25), dr.GetString(26), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty), 
                        new Usuario(dr.GetInt64(32)), 
                        dr.IsDBNull(36) ? string.Empty : dr.GetString(36), 
                        dr.IsDBNull(35) ? null : new Estudo(dr.GetInt64(35)), 
                        dr.IsDBNull(33) ? null : new Usuario(dr.GetInt64(33)), 
                        dr.IsDBNull(41) ? null : new Usuario(dr.GetInt64(41)), 
                        dr.IsDBNull(42) ? (DateTime?)null : dr.GetDateTime(42), 
                        dr.IsDBNull(40) ? (DateTime?)null : dr.GetDateTime(40), 
                        dr.IsDBNull(39) ? (DateTime?)null : dr.GetDateTime(39), 
                        dr.IsDBNull(37) ? string.Empty : dr.GetString(37), 
                        dr.IsDBNull(38) ? string.Empty : dr.GetString(38), 
                        dr.IsDBNull(51) ? string.Empty : dr.GetString(51), 
                        null, 
                        dr.IsDBNull(43) ? null : new Empresa(dr.GetInt64(43)), 
                        dr.IsDBNull(44) ? string.Empty : dr.GetString(44), 
                        dr.IsDBNull(34) ? null : new Usuario(dr.GetInt64(34)), 
                        dr.IsDBNull(45) ? null : new CentroCusto(dr.GetInt64(45), new Cliente(dr.GetInt64(46)), string.Empty, string.Empty), 
                        dr.IsDBNull(47) ? null : (DateTime?)dr.GetDateTime(47), 
                        dr.IsDBNull(48) ? string.Empty : dr.GetString(48), 
                        dr.GetBoolean(49), 
                        dr.IsDBNull(50) ? string.Empty : dr.GetString(50), 
                        dr.IsDBNull(52) ? string.Empty : dr.GetString(52), 
                        dr.IsDBNull(53) ? string.Empty : dr.GetString(53));
                    
                    aso.Altura = dr.GetBoolean(54);
                    aso.Confinado = dr.GetBoolean(55);
                    aso.Eletricidade = dr.GetBoolean(57);
                }

                return aso;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAsoById", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public Aso findAsoByGheFonteAgenteExameAsoId(Int64? id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAsoByGheFonteAgenteExameAsoId");

            try
            {
                StringBuilder query = new StringBuilder();

                Aso aso = null;

                query.Append(" SELECT ATENDIMENTO.ID_ASO, ATENDIMENTO.ID_MEDICO_EXAMINADOR, ATENDIMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO, ATENDIMENTO.CODIGO_ASO, ");
                query.Append(" ATENDIMENTO.DATA_ASO, ATENDIMENTO.NOME_FUNCIONARIO, ATENDIMENTO.RG_FUNCIONARIO, ATENDIMENTO.DATANASC_FUNCIONARIO, ATENDIMENTO.CPF_FUNCIONARIO, ");
                query.Append(" ATENDIMENTO.TIPOSAN_FUNCIONARIO, ATENDIMENTO.FTRH_FUNCIONARIO, ATENDIMENTO.MATRICULA_FUNCIONARIO, ATENDIMENTO.RAZAO_EMPRESA, ATENDIMENTO.CNPJ_EMPRESA, ");
                query.Append(" ATENDIMENTO.ENDERECO_EMPRESA, ATENDIMENTO.NUMERO_EMPRESA, ATENDIMENTO.COMPLEMENTO_EMPRESA, ATENDIMENTO.BAIRRO_EMPRESA, ATENDIMENTO.CEP_EMPRESA, ");
                query.Append(" ATENDIMENTO.CIDADE_EMPRESA, ATENDIMENTO.UF_EMPRESA, ATENDIMENTO.ID_PERIODICIDADE, ATENDIMENTO.SITUACAO, ATENDIMENTO.ID_MEDICO_COORDENADOR, ");
                query.Append(" ATENDIMENTO.CRM_COORDENADOR, ATENDIMENTO.TELEFONE1_COORDENADOR, ATENDIMENTO.TELEFONE2_COORDENADOR, ATENDIMENTO.NOME_COORDENADOR, ");
                query.Append(" ATENDIMENTO.CRM_EXAMINADOR, ATENDIMENTO.NOME_EXAMINADOR, ATENDIMENTO.TELEFONE1_EXAMINADOR, ATENDIMENTO.TELEFONE2_EXAMINADOR, ");
                query.Append(" ATENDIMENTO.ID_USUARIO_CRIADOR, ATENDIMENTO.ID_USUARIO_FINALIZADOR, ");
                query.Append(" ATENDIMENTO.ID_ULT_USUARIO_ALTERACAO, ATENDIMENTO.ID_ESTUDO, ATENDIMENTO.OBS_ALTERACAO, ATENDIMENTO.OBS_ASO, ATENDIMENTO.CONCLUSAO, ATENDIMENTO.DATA_GRAVACAO, ATENDIMENTO.DATA_CANCELAMENTO, ");
                query.Append(" ATENDIMENTO.ID_USUARIO_CANCELAMENTO, ATENDIMENTO.DATA_FINALIZACAO, ATENDIMENTO.ID_EMPRESA, ATENDIMENTO.OBS_CANCELAMENTO, ATENDIMENTO.ID_CENTROCUSTO, CLIENTE.ID_CLIENTE, ATENDIMENTO.DATA_VENCIMENTO, ATENDIMENTO.NUMERO_PO, ATENDIMENTO.PRIORIDADE, ATENDIMENTO.PIS, ATENDIMENTO.GHE_SETOR_AVULSO, ATENDIMENTO.FUNCAO_FUNCIONARIO ");
                query.Append(" FROM SEG_ASO ATENDIMENTO ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA ON GFAEA.ID_ASO = ATENDIMENTO.ID_ASO ");
                query.Append(" LEFT JOIN SEG_CENTROCUSTO CENTROCUSTO ON CENTROCUSTO.ID = ATENDIMENTO.ID_CENTROCUSTO ");
                query.Append(" LEFT JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = CENTROCUSTO.ID_CLIENTE ");
                query.Append(" WHERE GFAEA.ID_GHE_FONTE_AGENTE_EXAME_ASO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer)); 
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    aso = new Aso(
                        dr.GetInt64(0), 
                        dr.IsDBNull(1) ? null : new Medico(dr.GetInt64(1)), 
                        new ClienteFuncaoFuncionario(dr.GetInt64(2), null, null, string.Empty, null, null, dr.IsDBNull(11) ? string.Empty : dr.GetString(11), false, string.Empty, string.Empty, false, true), 
                        dr.GetString(3), 
                        dr.GetDateTime(4), 
                        new Funcionario(dr.GetString(5), dr.GetString(6), dr.GetDateTime(7), dr.GetString(8), dr.GetString(9), dr.GetString(10)), 
                        new Cliente(null, dr.GetString(13), String.Empty, dr.GetString(13), String.Empty, dr.GetString(14), dr.GetString(15), dr.GetString(16), dr.GetString(17), dr.GetString(18), dr.GetString(20), String.Empty, String.Empty, String.Empty, String.Empty, null, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, 0, 0, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, null, String.Empty, false, String.Empty, false, null, null, false, false, false, false, null, null, false, false, null, string.Empty, false, false, false, false, false, false), 
                        new Periodicidade(dr.GetInt64(21), String.Empty, null), 
                        dr.GetString(22), 
                        dr.IsDBNull(23) ? null : new Medico(dr.GetInt64(23)), 
                        new Usuario(dr.GetInt64(32)), 
                        dr.IsDBNull(36) ? string.Empty : dr.GetString(36), 
                        dr.IsDBNull(35) ? null : new Estudo(dr.GetInt64(35)), 
                        dr.IsDBNull(33) ? null : new Usuario(dr.GetInt64(33)), 
                        dr.IsDBNull(41) ? null : new Usuario(dr.GetInt64(41)), 
                        dr.IsDBNull(42) ? (DateTime?)null : dr.GetDateTime(42), 
                        dr.IsDBNull(40) ? (DateTime?)null : dr.GetDateTime(40), 
                        dr.IsDBNull(39) ? (DateTime?)null : dr.GetDateTime(39), 
                        dr.IsDBNull(37) ? string.Empty : dr.GetString(37), 
                        dr.IsDBNull(38) ? string.Empty : dr.GetString(38), 
                        null, 
                        null, 
                        dr.IsDBNull(43) ? null : new Empresa(dr.GetInt64(43)), 
                        dr.IsDBNull(44) ? string.Empty : dr.GetString(44), 
                        dr.IsDBNull(34) ? null : new Usuario(dr.GetInt64(34)), 
                        dr.IsDBNull(45) ? null : new CentroCusto(dr.GetInt64(45), new Cliente(dr.GetInt64(46)), string.Empty, string.Empty), dr.IsDBNull(47) ? null : (DateTime?)dr.GetDateTime(47), dr.IsDBNull(48) ? string.Empty : dr.GetString(48), dr.GetBoolean(49), dr.IsDBNull(50) ? string.Empty : dr.GetString(50), dr.IsDBNull(51) ? string.Empty : dr.GetString(51), dr.IsDBNull(52) ? string.Empty : dr.GetString(52));
                }

                return aso;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAsoByGheFonteAgenteExameAsoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public Aso findAsoByClienteFuncaoeExameAsoId(Int64? id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAsoByClienteFuncaoeExameAsoId");

            try
            {
                StringBuilder query = new StringBuilder();

                Aso aso = null;

                query.Append(" SELECT ATENDIMENTO.ID_ASO, ATENDIMENTO.ID_MEDICO_EXAMINADOR, ATENDIMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO, ATENDIMENTO.CODIGO_ASO, ");
                query.Append(" ATENDIMENTO.DATA_ASO, ATENDIMENTO.NOME_FUNCIONARIO, ATENDIMENTO.RG_FUNCIONARIO, ATENDIMENTO.DATANASC_FUNCIONARIO, ATENDIMENTO.CPF_FUNCIONARIO, ");
                query.Append(" ATENDIMENTO.TIPOSAN_FUNCIONARIO, ATENDIMENTO.FTRH_FUNCIONARIO, ATENDIMENTO.MATRICULA_FUNCIONARIO, ATENDIMENTO.RAZAO_EMPRESA, ATENDIMENTO.CNPJ_EMPRESA, ");
                query.Append(" ATENDIMENTO.ENDERECO_EMPRESA, ATENDIMENTO.NUMERO_EMPRESA, ATENDIMENTO.COMPLEMENTO_EMPRESA, ATENDIMENTO.BAIRRO_EMPRESA, ATENDIMENTO.CEP_EMPRESA, ");
                query.Append(" ATENDIMENTO.CIDADE_EMPRESA, ATENDIMENTO.UF_EMPRESA, ATENDIMENTO.ID_PERIODICIDADE, ATENDIMENTO.SITUACAO, ATENDIMENTO.ID_MEDICO_COORDENADOR, ");
                query.Append(" ATENDIMENTO.CRM_COORDENADOR, ATENDIMENTO.TELEFONE1_COORDENADOR, ATENDIMENTO.TELEFONE2_COORDENADOR, ATENDIMENTO.NOME_COORDENADOR, ");
                query.Append(" ATENDIMENTO.CRM_EXAMINADOR, ATENDIMENTO.NOME_EXAMINADOR, ATENDIMENTO.TELEFONE1_EXAMINADOR, ATENDIMENTO.TELEFONE2_EXAMINADOR, ");
                query.Append(" ATENDIMENTO.ID_USUARIO_CRIADOR, ATENDIMENTO.ID_USUARIO_FINALIZADOR, ");
                query.Append(" ATENDIMENTO.ID_ULT_USUARIO_ALTERACAO, ATENDIMENTO.ID_ESTUDO, ATENDIMENTO.OBS_ALTERACAO, ATENDIMENTO.OBS_ASO, ATENDIMENTO.CONCLUSAO, ATENDIMENTO.DATA_GRAVACAO, ATENDIMENTO.DATA_CANCELAMENTO, ");
                query.Append(" ATENDIMENTO.ID_USUARIO_CANCELAMENTO, ATENDIMENTO.DATA_FINALIZACAO, ATENDIMENTO.ID_EMPRESA, ATENDIMENTO.OBS_CANCELAMENTO, ATENDIMENTO.ID_CENTROCUSTO, CLIENTE.ID_CLIENTE, ATENDIMENTO.DATA_VENCIMENTO, ATENDIMENTO.NUMERO_PO, ATENDIMENTO.PRIORIDADE, ATENDIMENTO.PIS, ATENDIMENTO.GHE_SETOR_AVULSO, ATENDIMENTO.FUNCAO_FUNCIONARIO ");
                query.Append(" FROM SEG_ASO ATENDIMENTO ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA ON CFEA.ID_ASO = ATENDIMENTO.ID_ASO ");
                query.Append(" LEFT JOIN SEG_CENTROCUSTO CENTROCUSTO ON CENTROCUSTO.ID = ATENDIMENTO.ID_CENTROCUSTO ");
                query.Append(" LEFT JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = CENTROCUSTO.ID_CLIENTE ");
                query.Append(" WHERE CFEA.ID_CLIENTE_FUNCAO_EXAME_ASO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer)); // id_ghe_fonte_agente_exame_aso
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    aso = new Aso(
                        dr.GetInt64(0), 
                        dr.IsDBNull(1) ? null : new Medico(dr.GetInt64(1)), 
                        new ClienteFuncaoFuncionario(dr.GetInt64(2), null, null, string.Empty, null, null, string.Empty, false, string.Empty, string.Empty, false, true), 
                        dr.GetString(3), 
                        dr.GetDateTime(4), 
                        new Funcionario(dr.GetString(5), dr.GetString(6), dr.GetDateTime(7), dr.GetString(8), dr.GetString(9), dr.GetString(10)), 
                        new Cliente(null, dr.GetString(13), String.Empty, dr.GetString(13), String.Empty, dr.GetString(14), dr.GetString(15), dr.GetString(16), dr.GetString(17), dr.GetString(18), dr.GetString(20), String.Empty, String.Empty, String.Empty, String.Empty, null, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, 0, 0, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, null, String.Empty, false, String.Empty, false, null, null, false, false, false, false, null, null, false, false, null, string.Empty, false, false, false, false, false, false), 
                        new Periodicidade(dr.GetInt64(21), String.Empty, null), 
                        dr.GetString(22), 
                        dr.IsDBNull(23) ? null : new Medico(dr.GetInt64(23)), 
                        dr.IsDBNull(32) ? null : new Usuario(dr.GetInt64(32)), 
                        dr.IsDBNull(36) ? string.Empty : dr.GetString(36), 
                        dr.IsDBNull(35) ? null : new Estudo(dr.GetInt64(35)), 
                        dr.IsDBNull(33) ? null : new Usuario(dr.GetInt64(33)), 
                        dr.IsDBNull(41) ? null : new Usuario(dr.GetInt64(41)), 
                        dr.IsDBNull(42) ? (DateTime?)null : dr.GetDateTime(42), 
                        dr.IsDBNull(40) ? (DateTime?)null : dr.GetDateTime(40), 
                        dr.IsDBNull(39) ? (DateTime?)null : dr.GetDateTime(39), 
                        dr.IsDBNull(37) ? string.Empty : dr.GetString(37), 
                        dr.IsDBNull(38) ? string.Empty : dr.GetString(38), 
                        null, 
                        null, 
                        dr.IsDBNull(43) ? null : new Empresa(dr.GetInt64(43)), 
                        dr.IsDBNull(44) ? string.Empty : dr.GetString(44), 
                        dr.IsDBNull(34) ? null : new Usuario(dr.GetInt64(34)), 
                        dr.IsDBNull(45) ? null : new CentroCusto(dr.GetInt64(45), new Cliente(dr.GetInt64(46)), string.Empty, string.Empty), 
                        dr.IsDBNull(47) ? null : (DateTime?)dr.GetDateTime(47), 
                        dr.IsDBNull(48) ? string.Empty : dr.GetString(48), 
                        dr.GetBoolean(49), 
                        dr.IsDBNull(50) ? string.Empty : dr.GetString(50), 
                        dr.IsDBNull(51) ? string.Empty : dr.GetString(51), 
                        dr.IsDBNull(52) ? string.Empty : dr.GetString(52));
                }

                return aso;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAsoByClienteFuncaoeExameAsoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public void updateStatusAso(Aso aso, String situacao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateStatusAso");

            try
            {
                StringBuilder query = new StringBuilder();

                if (situacao.Equals(ApplicationConstants.DESATIVADO))
                {
                    query.Append("UPDATE SEG_ASO SET SITUACAO = :VALUE2, DATA_CANCELAMENTO = NOW(), ID_USUARIO_CANCELAMENTO = :VALUE3, OBS_CANCELAMENTO = :VALUE4 WHERE ID_ASO = :VALUE1 ");
                }
                if (situacao.Equals(ApplicationConstants.FECHADO))
                {
                    query.Append("UPDATE SEG_ASO SET SITUACAO = :VALUE2, DATA_FINALIZACAO = NOW(), ID_USUARIO_FINALIZADOR = :VALUE3 WHERE ID_ASO = :VALUE1 ");
                }

                if (situacao.Equals(ApplicationConstants.CONSTRUCAO))
                {
                    query.Append("UPDATE SEG_ASO SET SITUACAO = :VALUE2 WHERE ID_ASO = :VALUE1");
                }
               

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer)); 
                command.Parameters[0].Value = aso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar)); 
                command.Parameters[1].Value = situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer)); 
                command.Parameters[2].Value = situacao.Equals(ApplicationConstants.DESATIVADO) ? aso.UsuarioCancelamento.Id : aso.Finalizador.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Text)); // situacao
                command.Parameters[3].Value = aso.Justificativa;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateStatusAso", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public LinkedList<Aso> findAsoByPeriodo(DateTime dataInicio, DateTime dataFim, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAsoByPeriodo");

            try
            {
                StringBuilder query = new StringBuilder();

                LinkedList<Aso> aso = new LinkedList<Aso>();

                query.Append(" SELECT A.ID_ASO, A.ID_MEDICO_EXAMINADOR, A.ID_CLIENTE_FUNCAO_FUNCIONARIO, A.CODIGO_ASO, A.DATA_ASO, A.NOME_FUNCIONARIO, ");
                query.Append(" A.RG_FUNCIONARIO, A.DATANASC_FUNCIONARIO, A.CPF_FUNCIONARIO, A.TIPOSAN_FUNCIONARIO, ");
                query.Append(" A.FTRH_FUNCIONARIO, A.MATRICULA_FUNCIONARIO, A.RAZAO_EMPRESA, A.CNPJ_EMPRESA, ");
                query.Append(" A.ENDERECO_EMPRESA, A.NUMERO_EMPRESA, A.COMPLEMENTO_EMPRESA, A.BAIRRO_EMPRESA, ");
                query.Append(" A.CEP_EMPRESA, A.CIDADE_EMPRESA, A.UF_EMPRESA, A.ID_PERIODICIDADE, A.SITUACAO, ");
                query.Append(" A.ID_MEDICO_COORDENADOR, A.CRM_COORDENADOR, A.TELEFONE1_COORDENADOR, ");
                query.Append(" A.TELEFONE2_COORDENADOR, A.NOME_COORDENADOR, A.CRM_EXAMINADOR, A.NOME_EXAMINADOR, ");
                query.Append(" A.TELEFONE1_EXAMINADOR, A.TELEFONE2_EXAMINADOR, A.ID_USUARIO_CRIADOR, ");
                query.Append(" A.ID_USUARIO_FINALIZADOR FROM SEG_ASO A WHERE");
                query.Append(" A.DATA_ASO BETWEEN :VALUE1 AND :VALUE2 AND A.SITUACAO <> 'I' AND A.SITUACAO <> 'C' ORDER BY A.DATA_ASO ASC");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Timestamp));
                command.Parameters[0].Value = dataInicio;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                command.Parameters[1].Value = dataFim;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    aso.AddLast(new Aso(dr.GetInt64(0), new ClienteFuncaoFuncionario(dr.GetInt64(2)), dr.GetString(6)));
                }

                return aso;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAsoByPeriodo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public LinkedList<Aso> findAsoNaoFinalizadoByPeriodo(DateTime dataInicio, DateTime dataFim, Empresa empresa, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAsoNaoFinalizadoByPeriodo");

            try
            {
                StringBuilder query = new StringBuilder();

                LinkedList<Aso> aso = new LinkedList<Aso>();

                query.Append(" SELECT ATENDIMENTO.ID_ASO, ATENDIMENTO.ID_MEDICO_EXAMINADOR, ATENDIMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO, ATENDIMENTO.CODIGO_ASO, ATENDIMENTO.DATA_ASO, ATENDIMENTO.NOME_FUNCIONARIO, ");
                query.Append(" ATENDIMENTO.RG_FUNCIONARIO, ATENDIMENTO.DATANASC_FUNCIONARIO, ATENDIMENTO.CPF_FUNCIONARIO, ATENDIMENTO.TIPOSAN_FUNCIONARIO, ");
                query.Append(" ATENDIMENTO.FTRH_FUNCIONARIO, ATENDIMENTO.MATRICULA_FUNCIONARIO, ATENDIMENTO.RAZAO_EMPRESA, ATENDIMENTO.CNPJ_EMPRESA, ");
                query.Append(" ATENDIMENTO.ENDERECO_EMPRESA, ATENDIMENTO.NUMERO_EMPRESA, ATENDIMENTO.COMPLEMENTO_EMPRESA, ATENDIMENTO.BAIRRO_EMPRESA, ");
                query.Append(" ATENDIMENTO.CEP_EMPRESA, ATENDIMENTO.CIDADE_EMPRESA, ATENDIMENTO.UF_EMPRESA, ATENDIMENTO.ID_PERIODICIDADE, ATENDIMENTO.SITUACAO, ");
                query.Append(" ATENDIMENTO.ID_MEDICO_COORDENADOR, ATENDIMENTO.CRM_COORDENADOR, ATENDIMENTO.TELEFONE1_COORDENADOR, ");
                query.Append(" ATENDIMENTO.TELEFONE2_COORDENADOR, ATENDIMENTO.NOME_COORDENADOR, ATENDIMENTO.CRM_EXAMINADOR, ATENDIMENTO.NOME_EXAMINADOR, ");
                query.Append(" ATENDIMENTO.TELEFONE1_EXAMINADOR, ATENDIMENTO.TELEFONE2_EXAMINADOR, ATENDIMENTO.ID_USUARIO_CRIADOR, ");
                query.Append(" ATENDIMENTO.ID_USUARIO_FINALIZADOR, ATENDIMENTO.ID_ESTUDO, ATENDIMENTO.OBS_ALTERACAO, ATENDIMENTO.OBS_ASO, ATENDIMENTO.CONCLUSAO, ");
                query.Append(" ATENDIMENTO.DATA_GRAVACAO, ATENDIMENTO.DATA_CANCELAMENTO, ATENDIMENTO.ID_USUARIO_CANCELAMENTO, ATENDIMENTO.DATA_FINALIZACAO, ");
                query.Append(" ATENDIMENTO.SENHA, ATENDIMENTO.ID_PROTOCOLO, ATENDIMENTO.OBS_CANCELAMENTO, ATENDIMENTO.ID_EMPRESA, ATENDIMENTO.ID_ULT_USUARIO_ALTERACAO, ATENDIMENTO.ID_CENTROCUSTO, CLIENTE.ID_CLIENTE, ATENDIMENTO.DATA_VENCIMENTO, ATENDIMENTO.NUMERO_PO, ATENDIMENTO.PRIORIDADE, ATENDIMENTO.PIS, ATENDIMENTO.GHE_SETOR_AVULSO, ATENDIMENTO.FUNCAO_FUNCIONARIO ");
                query.Append(" FROM SEG_ASO ATENDIMENTO ");
                query.Append(" LEFT JOIN SEG_CENTROCUSTO CENTROCUSTO ON CENTROCUSTO.ID = ATENDIMENTO.ID_CENTROCUSTO ");
                query.Append(" LEFT JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = CENTROCUSTO.ID_CLIENTE ");
                query.Append(" WHERE");
                query.Append(" ATENDIMENTO.DATA_ASO BETWEEN :VALUE1 AND :VALUE2 AND ATENDIMENTO.SITUACAO = 'C' AND ATENDIMENTO.ID_EMPRESA = :VALUE3 ORDER BY ATENDIMENTO.DATA_ASO ASC");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Timestamp));
                command.Parameters[0].Value = dataInicio;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                command.Parameters[1].Value = dataFim;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = empresa.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    aso.AddLast(new Aso(
                        dr.GetInt64(0), 
                        dr.IsDBNull(1) ? null : new Medico(dr.GetInt64(1), dr.IsDBNull(29) ? string.Empty : dr.GetString(29), dr.IsDBNull(28) ? string.Empty : dr.GetString(28), "A", dr.IsDBNull(30) ? string.Empty : dr.GetString(30), dr.IsDBNull(31) ? string.Empty : dr.GetString(31), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty),
                        new ClienteFuncaoFuncionario(dr.GetInt64(2), null, null, string.Empty, null, null, dr.IsDBNull(11) ? string.Empty : dr.GetString(11), false, string.Empty, string.Empty, false, false), 
                        dr.GetString(3), 
                        dr.GetDateTime(4), 
                        new Funcionario(dr.GetString(5), dr.GetString(6), dr.GetDateTime(7), dr.IsDBNull(8) ? string.Empty : dr.GetString(8), dr.IsDBNull(9) ? string.Empty : dr.GetString(9), dr.IsDBNull(10) ? string.Empty : dr.GetString(10)), 
                        new Cliente(null, dr.GetString(12), string.Empty, dr.GetString(13), string.Empty, dr.GetString(14), dr.GetString(15), dr.IsDBNull(16) ? string.Empty : dr.GetString(16), dr.GetString(17), dr.GetString(18), dr.GetString(20), string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, "A", 0, 0, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, false, string.Empty, false, null, null, false, false, false, false, dr.IsDBNull(19) ? null : new CidadeIbge(null, string.Empty, dr.GetString(19), dr.GetString(20)), null, false, false, 0, string.Empty, false, false, false, false, false, false),
                        new Periodicidade(dr.GetInt64(21), string.Empty, null),
                        dr.GetString(22), 
                        dr.IsDBNull(23) ? null : new Medico(dr.GetInt64(23), dr.IsDBNull(27) ? string.Empty : dr.GetString(27), dr.IsDBNull(24) ? string.Empty : dr.GetString(24), "A", dr.IsDBNull(25) ? string.Empty : dr.GetString(25), dr.IsDBNull(26) ? string.Empty : dr.GetString(26), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty),
                        dr.IsDBNull(32) ? null : new Usuario(dr.GetInt64(32)),
                        dr.IsDBNull(35) ? string.Empty : dr.GetString(35), 
                        dr.IsDBNull(34) ? null : new Estudo(dr.GetInt64(34)), 
                        dr.IsDBNull(33) ? null : new Usuario(dr.GetInt64(33)), 
                        dr.IsDBNull(40) ? null : new Usuario(dr.GetInt64(40)), 
                        dr.IsDBNull(41) ? (DateTime?)null : dr.GetDateTime(41), 
                        dr.IsDBNull(39) ? (DateTime?)null : dr.GetDateTime(39), 
                        dr.IsDBNull(38) ? (DateTime?)null : dr.GetDateTime(38), 
                        dr.IsDBNull(36) ? string.Empty : dr.GetString(36), 
                        dr.IsDBNull(37) ? string.Empty : dr.GetString(37), 
                        dr.IsDBNull(42) ? string.Empty : dr.GetString(42), 
                        dr.IsDBNull(43) ? null : new Protocolo(dr.GetInt64(43)), 
                        dr.IsDBNull(45) ? null : new Empresa(dr.GetInt64(45)), 
                        dr.IsDBNull(44) ? string.Empty : dr.GetString(44), 
                        dr.IsDBNull(46) ? null : new Usuario(dr.GetInt64(46)), 
                        dr.IsDBNull(47) ? null : new CentroCusto(dr.GetInt64(47), new Cliente(dr.GetInt64(48)), string.Empty, string.Empty), 
                        dr.IsDBNull(49) ? null : (DateTime?)dr.GetDateTime(49), 
                        dr.IsDBNull(50) ? string.Empty : dr.GetString(50), 
                        dr.GetBoolean(51), 
                        dr.IsDBNull(52) ? string.Empty : dr.GetString(52), 
                        dr.IsDBNull(53) ? string.Empty : dr.GetString(53), 
                        dr.IsDBNull(54) ? string.Empty : dr.GetString(54)));
                }

                return aso;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAsoNaoFinalizadoByPeriodo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public LinkedList<Aso> findAsoNotCanceladoByPeriodo(DateTime dataInicio, DateTime dataFim, Empresa empresa, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAsoNotCanceladoByPeriodo");

            try
            {
                StringBuilder query = new StringBuilder();

                LinkedList<Aso> aso = new LinkedList<Aso>();

                query.Append(" SELECT ATENDIMENTO.ID_ASO, ATENDIMENTO.ID_MEDICO_EXAMINADOR, ATENDIMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO, ATENDIMENTO.CODIGO_ASO, ATENDIMENTO.DATA_ASO, ATENDIMENTO.NOME_FUNCIONARIO, ");
                query.Append(" ATENDIMENTO.RG_FUNCIONARIO, ATENDIMENTO.DATANASC_FUNCIONARIO, ATENDIMENTO.CPF_FUNCIONARIO, ATENDIMENTO.TIPOSAN_FUNCIONARIO, ");
                query.Append(" ATENDIMENTO.FTRH_FUNCIONARIO, ATENDIMENTO.MATRICULA_FUNCIONARIO, ATENDIMENTO.RAZAO_EMPRESA, ATENDIMENTO.CNPJ_EMPRESA, ");
                query.Append(" ATENDIMENTO.ENDERECO_EMPRESA, ATENDIMENTO.NUMERO_EMPRESA, ATENDIMENTO.COMPLEMENTO_EMPRESA, ATENDIMENTO.BAIRRO_EMPRESA, ");
                query.Append(" ATENDIMENTO.CEP_EMPRESA, ATENDIMENTO.CIDADE_EMPRESA, ATENDIMENTO.UF_EMPRESA, ATENDIMENTO.ID_PERIODICIDADE, ATENDIMENTO.SITUACAO, ");
                query.Append(" ATENDIMENTO.ID_MEDICO_COORDENADOR, ATENDIMENTO.CRM_COORDENADOR, ATENDIMENTO.TELEFONE1_COORDENADOR, ");
                query.Append(" ATENDIMENTO.TELEFONE2_COORDENADOR, ATENDIMENTO.NOME_COORDENADOR, ATENDIMENTO.CRM_EXAMINADOR, ATENDIMENTO.NOME_EXAMINADOR, ");
                query.Append(" ATENDIMENTO.TELEFONE1_EXAMINADOR, ATENDIMENTO.TELEFONE2_EXAMINADOR, ATENDIMENTO.ID_USUARIO_CRIADOR, ");
                query.Append(" ATENDIMENTO.ID_USUARIO_FINALIZADOR, ATENDIMENTO.ID_ESTUDO, ATENDIMENTO.OBS_ALTERACAO, ATENDIMENTO.OBS_ASO, ATENDIMENTO.CONCLUSAO, ");
                query.Append(" ATENDIMENTO.DATA_GRAVACAO, ATENDIMENTO.DATA_CANCELAMENTO, ATENDIMENTO.ID_USUARIO_CANCELAMENTO, ATENDIMENTO.DATA_FINALIZACAO, ");
                query.Append(" ATENDIMENTO.SENHA, ATENDIMENTO.ID_PROTOCOLO, ATENDIMENTO.OBS_CANCELAMENTO, ATENDIMENTO.ID_EMPRESA, ATENDIMENTO.ID_ULT_USUARIO_ALTERACAO, ATENDIMENTO.ID_CENTROCUSTO, CLIENTE.ID_CLIENTE, ATENDIMENTO.DATA_VENCIMENTO, ATENDIMENTO.NUMERO_PO, ATENDIMENTO.PRIORIDADE, ATENDIMENTO.PIS, ATENDIMENTO.GHE_SETOR_AVULSO, ATENDIMENTO.FUNCAO_FUNCIONARIO ");
                query.Append(" FROM SEG_ASO ATENDIMENTO ");
                query.Append(" LEFT JOIN SEG_CENTROCUSTO CENTROCUSTO ON CENTROCUSTO.ID = ATENDIMENTO.ID_CENTROCUSTO ");
                query.Append(" LEFT JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = CENTROCUSTO.ID_CLIENTE ");
                query.Append(" WHERE");
                query.Append(" ATENDIMENTO.DATA_ASO BETWEEN :VALUE1 AND :VALUE2 AND ATENDIMENTO.SITUACAO <> 'I' AND ATENDIMENTO.ID_EMPRESA = :VALUE3 ORDER BY ATENDIMENTO.DATA_ASO ASC");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Timestamp));
                command.Parameters[0].Value = dataInicio;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                command.Parameters[1].Value = dataFim;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = empresa.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    aso.AddLast(new Aso(
                        dr.GetInt64(0), 
                        dr.IsDBNull(1) ? null : new Medico(dr.GetInt64(1), dr.IsDBNull(29) ? string.Empty : dr.GetString(29), dr.IsDBNull(28) ? string.Empty : dr.GetString(28), "A", dr.IsDBNull(30) ? string.Empty : dr.GetString(30), dr.IsDBNull(31) ? string.Empty : dr.GetString(31), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty), 
                        new ClienteFuncaoFuncionario(dr.GetInt64(2), null, null, string.Empty, null, null, dr.IsDBNull(11) ? string.Empty : dr.GetString(11), false, string.Empty, string.Empty, false, false), 
                        dr.GetString(3), 
                        dr.GetDateTime(4), 
                        new Funcionario(dr.GetString(5), dr.GetString(6), dr.GetDateTime(7), dr.IsDBNull(8) ? string.Empty : dr.GetString(8), dr.IsDBNull(9) ? string.Empty : dr.GetString(9), dr.IsDBNull(10) ? string.Empty : dr.GetString(10)), 
                        new Cliente(null, dr.GetString(12), string.Empty, dr.GetString(13), string.Empty, dr.GetString(14), dr.GetString(15), dr.IsDBNull(16) ? string.Empty : dr.GetString(16), dr.GetString(17), dr.GetString(18), dr.GetString(20), string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, "A", 0, 0, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, false, string.Empty, false, null, null, false, false, false, false, dr.IsDBNull(19) ? null : new CidadeIbge(null, string.Empty, dr.GetString(19), dr.GetString(20)), null, false, false, 0, string.Empty, false, false, false, false, false, false), 
                        new Periodicidade(dr.GetInt64(21), string.Empty, null), 
                        dr.GetString(22), 
                        dr.IsDBNull(23) ? null : new Medico(dr.GetInt64(23), dr.IsDBNull(27) ? string.Empty : dr.GetString(27), dr.IsDBNull(24) ? string.Empty : dr.GetString(24), "A", dr.IsDBNull(25) ? string.Empty : dr.GetString(25), dr.IsDBNull(26) ? string.Empty : dr.GetString(26), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty), 
                        dr.IsDBNull(32) ? null : new Usuario(dr.GetInt64(32)), 
                        dr.IsDBNull(35) ? string.Empty : dr.GetString(35), 
                        dr.IsDBNull(34) ? null : new Estudo(dr.GetInt64(34)), 
                        dr.IsDBNull(33) ? null : new Usuario(dr.GetInt64(33)), 
                        dr.IsDBNull(40) ? null : new Usuario(dr.GetInt64(40)), 
                        dr.IsDBNull(41) ? (DateTime?)null : dr.GetDateTime(41), 
                        dr.IsDBNull(39) ? (DateTime?)null : dr.GetDateTime(39), 
                        dr.IsDBNull(38) ? (DateTime?)null : dr.GetDateTime(38), 
                        dr.IsDBNull(36) ? string.Empty : dr.GetString(36), 
                        dr.IsDBNull(37) ? string.Empty : dr.GetString(37), 
                        dr.IsDBNull(42) ? string.Empty : dr.GetString(42), 
                        dr.IsDBNull(43) ? null : new Protocolo(dr.GetInt64(43)), 
                        dr.IsDBNull(45) ? null : new Empresa(dr.GetInt64(45)), 
                        dr.IsDBNull(44) ? string.Empty : dr.GetString(44), 
                        dr.IsDBNull(46) ? null : new Usuario(dr.GetInt64(46)), 
                        dr.IsDBNull(47) ? null : new CentroCusto(dr.GetInt64(47), new Cliente(dr.GetInt64(48)), string.Empty, string.Empty), 
                        dr.IsDBNull(49) ? null : (DateTime?)dr.GetDateTime(49), 
                        dr.IsDBNull(50) ? string.Empty : dr.GetString(50), 
                        dr.GetBoolean(51), 
                        dr.IsDBNull(52) ? string.Empty : dr.GetString(52), 
                        dr.IsDBNull(53) ? string.Empty : dr.GetString(53), 
                        dr.IsDBNull(54) ? string.Empty : dr.GetString(54)));
                }

                return aso;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAsoNotCanceladoByPeriodo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<ClienteFuncaoExame> findAllExamesByClienteFuncaoAndIdadeAndPeriodicidade(ClienteFuncao clienteFuncao, Int32 idade, Periodicidade periodicidade, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllExamesByClienteFuncaoAndIdadeAndPeriodicidade");

            try
            {
                StringBuilder query = new StringBuilder();

                List<ClienteFuncaoExame> clienteFuncaoExameList = new List<ClienteFuncaoExame>();

                query.Append(" SELECT DISTINCT CLIENTE_FUNCAO_EXAME.ID_CLIENTE_FUNCAO_EXAME, CLIENTE_FUNCAO_EXAME.ID_CLIENTE_FUNCAO, CLIENTE_FUNCAO_EXAME.ID_EXAME, ");
                query.Append(" EXAME.DESCRICAO, EXAME.SITUACAO, EXAME.LABORATORIO, EXAME.PRECO, EXAME.PRIORIDADE, EXAME.CUSTO,    ");
                query.Append(" CLIENTE_FUNCAO_EXAME.SITUACAO, CLIENTE_FUNCAO_EXAME.IDADE_EXAME, EXAME.EXTERNO, EXAME.LIBERA_DOCUMENTO, EXAME.CODIGO_TUSS, EXAME.EXAME_COMPLEMENTAR, EXAME.PERIODO_VENCIMENTO, EXAME.PADRAO_CONTRATO ");
                query.Append(" FROM SEG_CLIENTE_FUNCAO_EXAME CLIENTE_FUNCAO_EXAME  ");
                query.Append(" LEFT JOIN SEG_EXAME EXAME ON CLIENTE_FUNCAO_EXAME.ID_EXAME = EXAME.ID_EXAME  ");
                query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO_EXAME_PERIODICIDADE CLIENTE_FUNCAO_EXAME_PERIODICIDADE ON CLIENTE_FUNCAO_EXAME_PERIODICIDADE.ID_CLIENTE_FUNCAO_EXAME = CLIENTE_FUNCAO_EXAME.ID_CLIENTE_FUNCAO_EXAME ");
                query.Append(" WHERE CLIENTE_FUNCAO_EXAME.ID_CLIENTE_FUNCAO = :VALUE1 AND CLIENTE_FUNCAO_EXAME_PERIODICIDADE.SITUACAO = 'A' ");
                
                //Utilizando o valor para exames anual e bi-anual
                if (periodicidade.Id == 2)
                    query.Append(" AND CLIENTE_FUNCAO_EXAME_PERIODICIDADE.ID_PERIODICIDADE IN (1,2) ");
                else
                    query.Append(" AND CLIENTE_FUNCAO_EXAME_PERIODICIDADE.ID_PERIODICIDADE = :VALUE2 ");

                query.Append(" AND CLIENTE_FUNCAO_EXAME.SITUACAO = 'A' ");
                query.Append(" AND CLIENTE_FUNCAO_EXAME.IDADE_EXAME <= :VALUE3 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer)); 
                command.Parameters[0].Value = clienteFuncao.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer)); 
                command.Parameters[1].Value = periodicidade.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer)); 
                command.Parameters[2].Value = idade;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    clienteFuncaoExameList.Add(new ClienteFuncaoExame(dr.GetInt64(0), clienteFuncao, new Exame(dr.GetInt64(2), dr.GetString(3), dr.GetString(4), dr.GetBoolean(5),  dr.GetDecimal(6), dr.GetInt32(7), dr.GetDecimal(8),dr.GetBoolean(11), dr.GetBoolean(12), dr.IsDBNull(13) ? string.Empty : dr.GetString(13), dr.GetBoolean(14), dr.IsDBNull(15) ? (int?)null : dr.GetInt32(15), dr.GetBoolean(16)), dr.GetString(9),dr.GetInt32(10)));
                }

                return clienteFuncaoExameList;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllExamesByClienteFuncaoAndIdadeAndPeriodicidade", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public void update(Aso aso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alteraAso");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_ASO SET  ");
                query.Append(" DATA_ASO = :VALUE2, ");
                query.Append(" NOME_FUNCIONARIO = :VALUE3, ");
                query.Append(" RG_FUNCIONARIO = :VALUE4, ");
                query.Append(" DATANASC_FUNCIONARIO = :VALUE5, ");
                query.Append(" CPF_FUNCIONARIO = :VALUE6, ");
                query.Append(" TIPOSAN_FUNCIONARIO = :VALUE7, ");
                query.Append(" FTRH_FUNCIONARIO = :VALUE8, ");
                query.Append(" MATRICULA_FUNCIONARIO = :VALUE9, ");
                query.Append(" RAZAO_EMPRESA = :VALUE10, ");
                query.Append(" CNPJ_EMPRESA = :VALUE11, ");
                query.Append(" ENDERECO_EMPRESA = :VALUE12, ");
                query.Append(" NUMERO_EMPRESA = :VALUE13, ");
                query.Append(" COMPLEMENTO_EMPRESA = :VALUE14, ");
                query.Append(" BAIRRO_EMPRESA = :VALUE15, ");
                query.Append(" CIDADE_EMPRESA = :VALUE16, ");
                query.Append(" CEP_EMPRESA = :VALUE17, ");
                query.Append(" UF_EMPRESA = :VALUE18, ");
                query.Append(" NOME_COORDENADOR = :VALUE19, ");
                query.Append(" CRM_COORDENADOR = :VALUE20, ");
                query.Append(" TELEFONE1_COORDENADOR = :VALUE21, ");
                query.Append(" TELEFONE2_COORDENADOR = :VALUE22, ");
                query.Append(" NOME_EXAMINADOR = :VALUE23, ");
                query.Append(" CRM_EXAMINADOR = :VALUE24, ");
                query.Append(" TELEFONE1_EXAMINADOR = :VALUE25, ");
                query.Append(" TELEFONE2_EXAMINADOR = :VALUE26, ");
                query.Append(" ID_ULT_USUARIO_ALTERACAO = :VALUE27, ");
                query.Append(" OBS_ALTERACAO = :VALUE28, ");
                query.Append(" ID_CENTROCUSTO = :VALUE29, ");
                query.Append(" DATA_VENCIMENTO = :VALUE30, ");
                query.Append(" NUMERO_PO = :VALUE31, ");
                query.Append(" PRIORIDADE = :VALUE32, ");
                query.Append(" PIS = :VALUE33, ");
                query.Append(" SENHA = :VALUE34, ");
                query.Append(" GHE_SETOR_AVULSO = :VALUE35, ");
                query.Append(" FUNCAO_FUNCIONARIO = :VALUE36, ");
                query.Append(" OBS_ASO = :VALUE37, ");
                query.Append(" ID_MEDICO_COORDENADOR = :VALUE38,");
                query.Append(" ID_MEDICO_EXAMINADOR = :VALUE39, ");
                query.Append(" ALTURA = :VALUE40,");
                query.Append(" CONFINADO = :VALUE41,");
                query.Append(" ELETRICIDADE = :VALUE42");
                query.Append(" WHERE ID_ASO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                command.Parameters[1].Value = aso.DataElaboracao;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = aso.Funcionario.Nome;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = aso.Funcionario.Rg;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Date));
                command.Parameters[4].Value = aso.Funcionario.DataNascimento;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = aso.Funcionario.Cpf;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = aso.Funcionario.TipoSangue;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Varchar));
                command.Parameters[7].Value = aso.Funcionario.FatorRh;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                command.Parameters[8].Value = aso.ClienteFuncaoFuncionario.Matricula;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                command.Parameters[9].Value = aso.Cliente.RazaoSocial;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Varchar));
                command.Parameters[10].Value = aso.Cliente.Cnpj;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Varchar));
                command.Parameters[11].Value = aso.Cliente.Endereco;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Varchar));
                command.Parameters[12].Value = aso.Cliente.Numero;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Varchar));
                command.Parameters[13].Value = aso.Cliente.Complemento;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Varchar));
                command.Parameters[14].Value = aso.Cliente.Bairro;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Varchar));
                command.Parameters[15].Value = aso.Cliente.CidadeIbge.Nome;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE17", NpgsqlDbType.Varchar));
                command.Parameters[16].Value = aso.Cliente.Cep;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE18", NpgsqlDbType.Varchar));
                command.Parameters[17].Value = aso.Cliente.Uf;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE19", NpgsqlDbType.Varchar));
                command.Parameters[18].Value = aso.Coordenador != null ? aso.Coordenador.Nome : String.Empty;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE20", NpgsqlDbType.Varchar));
                command.Parameters[19].Value = aso.Coordenador != null ? aso.Coordenador.Crm + "-" + aso.Coordenador.UfCrm : String.Empty;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE21", NpgsqlDbType.Varchar));
                command.Parameters[20].Value = aso.Coordenador != null ? aso.Coordenador.Telefone1 : String.Empty;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE22", NpgsqlDbType.Varchar));
                command.Parameters[21].Value = aso.Coordenador != null ? aso.Coordenador.Telefone2 : String.Empty;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE23", NpgsqlDbType.Varchar));
                command.Parameters[22].Value = aso.Examinador != null ? aso.Examinador.Nome : string.Empty;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE24", NpgsqlDbType.Varchar));
                command.Parameters[23].Value = aso.Examinador != null ? aso.Examinador.Crm + "-" + aso.Examinador.UfCrm : string.Empty;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE25", NpgsqlDbType.Varchar));
                command.Parameters[24].Value = aso.Examinador != null ? aso.Examinador.Telefone1 : string.Empty;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE26", NpgsqlDbType.Varchar));
                command.Parameters[25].Value = aso.Examinador != null ? aso.Examinador.Telefone2 : string.Empty;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE27", NpgsqlDbType.Integer));
                command.Parameters[26].Value = aso.UsuarioAlteracao.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE28", NpgsqlDbType.Text));
                command.Parameters[27].Value = aso.JustificativaAlteracao;

                command.Parameters.Add(new NpgsqlParameter("VALUE29", NpgsqlDbType.Integer));
                command.Parameters[28].Value = aso.CentroCusto == null ? null : aso.CentroCusto.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE30", NpgsqlDbType.Date));
                command.Parameters[29].Value = aso.DataVencimento;

                command.Parameters.Add(new NpgsqlParameter("VALUE31", NpgsqlDbType.Varchar));
                command.Parameters[30].Value = aso.CodigoPo;

                command.Parameters.Add(new NpgsqlParameter("VALUE32", NpgsqlDbType.Boolean));
                command.Parameters[31].Value = aso.Prioridade;

                command.Parameters.Add(new NpgsqlParameter("VALUE33", NpgsqlDbType.Varchar));
                command.Parameters[32].Value = aso.Pis;

                command.Parameters.Add(new NpgsqlParameter("VALUE34", NpgsqlDbType.Varchar));
                command.Parameters[33].Value = aso.Senha;

                command.Parameters.Add(new NpgsqlParameter("VALUE35", NpgsqlDbType.Varchar));
                command.Parameters[34].Value = string.IsNullOrEmpty(aso.ListGheSetorAvulso) ? string.Empty : aso.ListGheSetorAvulso;

                command.Parameters.Add(new NpgsqlParameter("VALUE36", NpgsqlDbType.Varchar));
                command.Parameters[35].Value = aso.FuncaoFuncionario;

                command.Parameters.Add(new NpgsqlParameter("VALUE37", NpgsqlDbType.Varchar));
                command.Parameters[36].Value = aso.ObservacaoAso;

                command.Parameters.Add(new NpgsqlParameter("VALUE38", NpgsqlDbType.Integer));
                command.Parameters[37].Value = aso.Coordenador != null ? aso.Coordenador.Id : (long?)null;

                command.Parameters.Add(new NpgsqlParameter("VALUE39", NpgsqlDbType.Integer));
                command.Parameters[38].Value = aso.Examinador != null ? aso.Examinador.Id : (long?)null;

                command.Parameters.Add(new NpgsqlParameter("VALUE40", NpgsqlDbType.Boolean));
                command.Parameters[39].Value = aso.Altura;

                command.Parameters.Add(new NpgsqlParameter("VALUE41", NpgsqlDbType.Boolean));
                command.Parameters[40].Value = aso.Confinado;

                command.Parameters.Add(new NpgsqlParameter("VALUE42", NpgsqlDbType.Boolean));
                command.Parameters[41].Value = aso.Eletricidade;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void updateInformacaoLaudo(Aso atendimento, NpgsqlConnection dbconnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateInformacaoLaudo");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_ASO SET ");
                query.Append(" CONCLUSAO = :VALUE2, OBS_ASO = :VALUE3 WHERE ");
                query.Append(" ID_ASO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbconnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = atendimento.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = atendimento.Conclusao;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Text));
                command.Parameters[2].Value = atendimento.ObservacaoAso.ToUpper();

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateInformacaoLaudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public String findSenhaByAso(Aso aso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findSenhaByAso");

            try
            {
                StringBuilder query = new StringBuilder();

                String senha = String.Empty;

                query.Append(" SELECT SENHA FROM SEG_ASO WHERE ID_ASO = :VALUE1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer)); // id_aso
                command.Parameters[0].Value = aso.Id;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    senha = dr.GetString(0);
                }

                return senha;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findSenhaByAso", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public Boolean verificaExisteAtendimentoNoMesmoDia(Cliente cliente, Funcionario funcionario, Periodicidade periodicidade, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaExisteAtendimentoNaMesmaData");

            try
            {
                StringBuilder query = new StringBuilder();
                StringBuilder query2 = new StringBuilder();

                Boolean existeAtendimento = false;

                query.Append(" SELECT * FROM SEG_ASO ASO, SEG_CLIENTE_FUNCAO_FUNCIONARIO CFF ");
                query.Append(" WHERE ASO.DATA_GRAVACAO BETWEEN :VALUE4 AND :VALUE5 ");
                query.Append(" AND ASO.SITUACAO <> 'I' ");
                query.Append(" AND (CFF.ID_CLIENTE_FUNCAO_FUNCIONARIO = ASO.ID_CLIENTE_FUNCAO_FUNCIONARIO AND CFF.ID_FUNCIONARIO = :VALUE2) ");
                query.Append(" AND ASO.CNPJ_EMPRESA = :VALUE1 AND ASO.ID_PERIODICIDADE = :VALUE3 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = cliente.Cnpj;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = funcionario.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = periodicidade.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Timestamp));
                command.Parameters[3].Value = DateTime.Now.Date.Add(new TimeSpan(0, 0, 0));
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Timestamp));
                command.Parameters[4].Value = DateTime.Now.Date.Add(new TimeSpan(23, 59, 59));

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    existeAtendimento = true;
                    LogHelper.logger.Info(this.GetType().Name + " - verificaExisteAtendimentoNaMesmaData : Existe atendimento para o mesmo funcionário com mesmo id cnpj e periodicidade.");
                }

                query2.Append(" SELECT * FROM SEG_ASO ASO ");
                query2.Append(" WHERE ASO.DATA_GRAVACAO BETWEEN :VALUE4 AND :VALUE5 ");
                query2.Append(" AND ASO.SITUACAO <> 'I' ");
                query2.Append(" AND ASO.NOME_FUNCIONARIO = :VALUE2 ");
                query2.Append(" AND ASO.CNPJ_EMPRESA = :VALUE1 AND ASO.ID_PERIODICIDADE = :VALUE3 ");

                NpgsqlCommand command2 = new NpgsqlCommand(query2.ToString(), dbConnection);

                command2.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command2.Parameters[0].Value = cliente.Cnpj;
                
                command2.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command2.Parameters[1].Value = funcionario.Nome;
                
                command2.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command2.Parameters[2].Value = periodicidade.Id;
                
                command2.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Timestamp));
                command2.Parameters[3].Value = DateTime.Now.Date.Add(new TimeSpan(0, 0, 0));
                
                command2.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Timestamp));
                command2.Parameters[4].Value = DateTime.Now.Date.Add(new TimeSpan(23, 59, 59));

                NpgsqlDataReader dr2 = command2.ExecuteReader();

                while (dr2.Read())
                {
                    existeAtendimento = true;
                    LogHelper.logger.Info(this.GetType().Name + " - verificaExisteAtendimentoNaMesmaData : Existe atendimento para o mesmo funcionário com nome e cnpj e periodicidade.");
                }

                return existeAtendimento;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaExisteAtendimentoNaMesmaData", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }
        
        public List<Exame> FindAllExamesByAsoBySituacao(Aso aso, Boolean? atendido, Boolean? finalizado, Boolean? cancelado, Boolean? devolvido, Boolean? transcrito, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método FindAllExamesByAsoBySituacao");

            try
            {
                StringBuilder query = new StringBuilder();

                List<Exame> exames = new List<Exame>();
                
                query.Append(" SELECT E.ID_EXAME, E.DESCRICAO, E.SITUACAO, E.LABORATORIO, E.PRECO, E.PRIORIDADE, E.CUSTO, E.EXTERNO, E.LIBERA_DOCUMENTO, E.CODIGO_TUSS, E.EXAME_COMPLEMENTAR, E.PERIODO_VENCIMENTO, E.PADRAO_CONTRATO ");
                query.Append(" FROM SEG_ASO A  ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA ON CFEA.ID_ASO = A.ID_ASO ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME CFE ON CFE.ID_CLIENTE_FUNCAO_EXAME = CFEA.ID_CLIENTE_FUNCAO_EXAME ");
                query.Append(" JOIN SEG_EXAME E ON E.ID_EXAME = CFE.ID_EXAME ");
                query.Append(" WHERE A.ID_ASO = :VALUE1 ");

                if (atendido != null)
                    query.Append(" AND CFEA.ATENDIDO = :VALUE2 ");

                if (finalizado != null)
                    query.Append(" AND CFEA.FINALIZADO = :VALUE3 ");

                if (cancelado != null)
                    query.Append(" AND CFEA.CANCELADO = :VALUE4 ");

                if (devolvido != null)
                    query.Append(" AND CFEA.DEVOLVIDO = :VALUE5");

                if (transcrito != null)
                    query.Append(" AND CFEA.TRANSCRITO = :VALUE6");

                query.Append(" UNION ( ");
                query.Append(" SELECT E.ID_EXAME, E.DESCRICAO, E.SITUACAO, E.LABORATORIO, E.PRECO, E.PRIORIDADE, E.CUSTO, E.EXTERNO, E.LIBERA_DOCUMENTO, E.CODIGO_TUSS, E.EXAME_COMPLEMENTAR, E.PERIODO_VENCIMENTO, E.PADRAO_CONTRATO ");
                query.Append(" FROM SEG_ASO A ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA ON GFAEA.ID_ASO = A.ID_ASO AND GFAEA.DUPLICADO IS FALSE ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GFAE ON GFAE.ID_GHE_FONTE_AGENTE_EXAME = GFAEA.ID_GHE_FONTE_AGENTE_EXAME ");
                query.Append(" JOIN SEG_EXAME E ON E.ID_EXAME = GFAE.ID_EXAME ");
                query.Append(" WHERE A.ID_ASO = :VALUE1 ");

                if (atendido != null)
                    query.Append(" AND GFAEA.ATENDIDO = :VALUE2 ");

                if (finalizado != null)
                    query.Append(" AND GFAEA.FINALIZADO = :VALUE3 ");

                if (cancelado != null)
                    query.Append(" AND GFAEA.CANCELADO = :VALUE4 ");

                if (devolvido != null)
                    query.Append(" AND GFAEA.DEVOLVIDO = :VALUE5 ");

                if (transcrito != null)
                    query.Append(" AND GFAEA.TRANSCRITO = :VALUE6 ");

                query.Append(" )");
                query.Append(" ORDER BY DESCRICAO ASC ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Boolean));
                command.Parameters[1].Value = atendido;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Boolean));
                command.Parameters[2].Value = finalizado;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Boolean));
                command.Parameters[3].Value = cancelado;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Boolean));
                command.Parameters[4].Value = devolvido;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Boolean));
                command.Parameters[5].Value = transcrito;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    exames.Add(new Exame(dr.GetInt64(0),dr.GetString(1), dr.GetString(2), dr.IsDBNull(3) ? null : (Boolean?)dr.GetBoolean(3), dr.GetDecimal(4), dr.GetInt32(5), dr.GetDecimal(6), dr.GetBoolean(7), dr.GetBoolean(8), dr.IsDBNull(9) ? string.Empty : dr.GetString(9), dr.GetBoolean(10), dr.IsDBNull(11) ? (int?)null : dr.GetInt32(11), dr.GetBoolean(12)));
                }

                return exames;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - FindAllExamesByAsoBySituacao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public Boolean verificaAsoTranscrito(Aso aso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaAsoTranscrito");

            try
            {
                StringBuilder query = new StringBuilder();

                Boolean retorno = false;
                Int32 numeroExameTranscrito = 0;
                Int32 numeroRegistro = 0;

                query.Append(" SELECT E.ID_EXAME, CFEA.TRANSCRITO  ");
                query.Append(" FROM SEG_ASO A  ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA ON CFEA.ID_ASO = A.ID_ASO ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME CFE ON CFE.ID_CLIENTE_FUNCAO_EXAME = CFEA.ID_CLIENTE_FUNCAO_EXAME ");
                query.Append(" JOIN SEG_EXAME E ON E.ID_EXAME = CFE.ID_EXAME ");
                query.Append(" WHERE A.ID_ASO = :VALUE1 ");

                query.Append(" UNION ( ");
                query.Append(" SELECT E.ID_EXAME, GFAEA.TRANSCRITO ");
                query.Append(" FROM SEG_ASO A ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA ON GFAEA.ID_ASO = A.ID_ASO AND GFAEA.DUPLICADO IS FALSE ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GFAE ON GFAE.ID_GHE_FONTE_AGENTE_EXAME = GFAEA.ID_GHE_FONTE_AGENTE_EXAME ");
                query.Append(" JOIN SEG_EXAME E ON E.ID_EXAME = GFAE.ID_EXAME ");
                query.Append(" WHERE A.ID_ASO = :VALUE1 )");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    numeroRegistro++;

                    if (dr.GetBoolean(1))
                        numeroExameTranscrito++;
                }

                if (numeroRegistro == numeroExameTranscrito)
                    retorno = true;

                return retorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaAsoTranscrito", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean verificaAsoInclusaoExame(Aso aso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaAsoInclusaoExame");

            try
            {
                StringBuilder query = new StringBuilder();

                Boolean retorno = false;
                Boolean flagTrasncrito = false;
                Int32 numeroRegistro = 0;
                Int32 numeroTranscrito = 0;

                query.Append(" SELECT E.ID_EXAME, CFEA.TRANSCRITO  ");
                query.Append(" FROM SEG_ASO A  ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA ON CFEA.ID_ASO = A.ID_ASO ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME CFE ON CFE.ID_CLIENTE_FUNCAO_EXAME = CFEA.ID_CLIENTE_FUNCAO_EXAME ");
                query.Append(" JOIN SEG_EXAME E ON E.ID_EXAME = CFE.ID_EXAME ");
                query.Append(" WHERE A.ID_ASO = :VALUE1 ");

                query.Append(" UNION ( ");
                query.Append(" SELECT E.ID_EXAME, GFAEA.TRANSCRITO ");
                query.Append(" FROM SEG_ASO A ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA ON GFAEA.ID_ASO = A.ID_ASO AND GFAEA.DUPLICADO IS FALSE ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GFAE ON GFAE.ID_GHE_FONTE_AGENTE_EXAME = GFAEA.ID_GHE_FONTE_AGENTE_EXAME ");
                query.Append(" JOIN SEG_EXAME E ON E.ID_EXAME = GFAE.ID_EXAME ");
                query.Append(" WHERE A.ID_ASO = :VALUE1 ) ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    numeroRegistro++;
                    
                    if (dr.GetBoolean(1))
                    {
                        flagTrasncrito = true;
                        numeroTranscrito++;
                    }
                    
                }

                if (flagTrasncrito)
                {
                    if (numeroTranscrito < numeroRegistro)
                        retorno = true;
                }
                
                return retorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaAsoInclusaoExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void insertAsoInProtocolo(Protocolo protocolo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertAsoInProtocolo");

            try
            {

                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_ASO SET ID_PROTOCOLO = :VALUE1 WHERE ID_ASO = :VALUE2 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = protocolo.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = protocolo.Atendimento != null ? protocolo.Atendimento.Id : null;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertAsoInProtocolo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean findAsoInProtocolo(Aso atendimento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAsoInProtocolo");

            try
            {
                StringBuilder query = new StringBuilder();

                Boolean retorno = false;
                
                query.Append(" SELECT *  ");
                query.Append(" FROM SEG_ASO ASO  ");
                query.Append(" WHERE ASO.ID_ASO = :VALUE1 AND ASO.ID_PROTOCOLO IS NOT NULL ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = atendimento.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    retorno = true;
                }

                return retorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllAsoByProtocolo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean verificaAtendimentoUnidade(Cliente unidade, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaAtendimentoUnidade");

            try
            {
                StringBuilder query = new StringBuilder();

                Boolean retorno = false;

                query.Append(" SELECT *  ");
                query.Append(" FROM SEG_ASO ASO  ");
                query.Append(" JOIN SEG_ESTUDO ESTUDO ON ESTUDO.ID_ESTUDO = ASO.ID_ESTUDO ");
                query.Append(" JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = ESTUDO.ID_CLIENTE_CONTRATADO ");
                query.Append(" WHERE CLIENTE.ID_CLIENTE = :VALUE1 " );

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = unidade.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    retorno = true;
                }

                return retorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaAtendimentoUnidade", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<Aso> findAllByFuncionario(Funcionario funcionario, Int32 limitePesquisa, Empresa empresa, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByFuncionario");

            List<Aso> atendimentos = new List<Aso>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ATENDIMENTO.ID_ASO, ATENDIMENTO.CODIGO_ASO, ATENDIMENTO.ID_MEDICO_EXAMINADOR, ATENDIMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO, ATENDIMENTO.DATA_ASO, ATENDIMENTO.ID_PERIODICIDADE, ATENDIMENTO.SITUACAO, ATENDIMENTO.ID_MEDICO_COORDENADOR, ATENDIMENTO.ID_USUARIO_CRIADOR, ATENDIMENTO.ID_USUARIO_FINALIZADOR, ATENDIMENTO.ID_ESTUDO, ATENDIMENTO.OBS_ALTERACAO, ATENDIMENTO.OBS_ASO, ATENDIMENTO.CONCLUSAO, ATENDIMENTO.DATA_GRAVACAO, ATENDIMENTO.ID_ULT_USUARIO_ALTERACAO, ATENDIMENTO.DATA_FINALIZACAO, ATENDIMENTO.SENHA, ATENDIMENTO.ID_PROTOCOLO,  ");
                query.Append(" CLIENTE_FUNCAO_FUNCIONARIO.ID_SEG_CLIENTE_FUNCAO, CLIENTE_FUNCAO_FUNCIONARIO.SITUACAO, CLIENTE_FUNCAO_FUNCIONARIO.DATA_CADASTRO, CLIENTE_FUNCAO_FUNCIONARIO.DATA_DESLIGAMENTO, CLIENTE_FUNCAO_FUNCIONARIO.MATRICULA, CLIENTE_FUNCAO_FUNCIONARIO.VIP, ");
                query.Append(" CLIENTE_FUNCAO.ID_CLIENTE, CLIENTE_FUNCAO.SITUACAO, CLIENTE_FUNCAO.DATA_CADASTRO, CLIENTE_FUNCAO.DATA_DESLIGAMENTO, ");
                query.Append(" FUNCAO.DESCRICAO, FUNCAO.COD_CBO, FUNCAO.SITUACAO, FUNCAO.COMENTARIO, ATENDIMENTO.ID_EMPRESA, ATENDIMENTO.ID_CENTROCUSTO, ATENDIMENTO.DATA_VENCIMENTO, ATENDIMENTO.NUMERO_PO, ATENDIMENTO.PRIORIDADE, ATENDIMENTO.PIS, ATENDIMENTO.GHE_SETOR_AVULSO, ATENDIMENTO.FUNCAO_FUNCIONARIO, CLIENTE_FUNCAO_FUNCIONARIO.BR_PDH, CLIENTE_FUNCAO_FUNCIONARIO.REGIME_REVEZAMENTO, CLIENTE_FUNCAO_FUNCIONARIO.ESTAGIARIO, CLIENTE_FUNCAO_FUNCIONARIO.VINCULO  ");
                query.Append(" FROM SEG_ASO ATENDIMENTO  ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTE_FUNCAO_FUNCIONARIO ON CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO = ATENDIMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO  ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTE_FUNCAO_FUNCIONARIO.ID_SEG_CLIENTE_FUNCAO  ");
                query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTE_FUNCAO.ID_FUNCAO");
                query.Append(" JOIN SEG_FUNCIONARIO FUNCIONARIO ON FUNCIONARIO.ID_FUNCIONARIO = CLIENTE_FUNCAO_FUNCIONARIO.ID_FUNCIONARIO");
                query.Append(" JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = CLIENTE_FUNCAO.ID_CLIENTE ");
                query.Append(" WHERE FUNCIONARIO.ID_FUNCIONARIO = :VALUE1 AND ");
                query.Append(" ATENDIMENTO.SITUACAO <> 'I' AND ");
                query.Append(" ATENDIMENTO.DATA_ASO BETWEEN (CAST(NOW() AS DATE) - :VALUE2) AND (CAST(NOW() AS DATE) +1) ");
                query.Append(" AND ATENDIMENTO.ID_EMPRESA = :VALUE3 ");
                query.Append(" ORDER BY DATA_ASO ASC ");


                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = funcionario.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = limitePesquisa;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = empresa.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    atendimentos.Add(new Aso(dr.GetInt64(0), dr.IsDBNull(2) ? null : new Medico(dr.GetInt64(2)), new ClienteFuncaoFuncionario(dr.GetInt64(3), new ClienteFuncao(dr.GetInt64(19)), funcionario, dr.GetString(20), dr.GetDateTime(21), dr.IsDBNull(22) ? (DateTime?)null : dr.GetDateTime(22), dr.IsDBNull(23) ? string.Empty : dr.GetString(23), dr.GetBoolean(24), dr.IsDBNull(41) ? string.Empty : dr.GetString(41), dr.IsDBNull(42) ? string.Empty : dr.GetString(42), dr.GetBoolean(43), dr.GetBoolean(44)), dr.GetString(1), dr.GetDateTime(4), funcionario, new Cliente(dr.GetInt64(25)), new Periodicidade(dr.GetInt64(5)), dr.GetString(6), dr.IsDBNull(7) ? null : new Medico(dr.GetInt64(7)), new Usuario(dr.GetInt64(8)), dr.IsDBNull(11) ? string.Empty : dr.GetString(11), dr.IsDBNull(10) ? null : new Estudo(dr.GetInt64(10)), dr.IsDBNull(9) ? null : new Usuario(dr.GetInt64(9)), null, dr.IsDBNull(16) ? (DateTime?)null : dr.GetDateTime(16), null, dr.GetDateTime(14), dr.IsDBNull(12) ? string.Empty : dr.GetString(12), dr.IsDBNull(13) ? string.Empty : dr.GetString(13), dr.IsDBNull(17) ? string.Empty : dr.GetString(17), dr.IsDBNull(18) ? null : new Protocolo(dr.GetInt64(18)), new Empresa(dr.GetInt64(33)), dr.IsDBNull(11) ? string.Empty : dr.GetString(11), dr.IsDBNull(15) ? null : new Usuario(dr.GetInt64(15)), dr.IsDBNull(34) ? null : new CentroCusto(dr.GetInt64(34), null, string.Empty, string.Empty), dr.IsDBNull(35) ? null : (DateTime?)dr.GetDateTime(35), dr.IsDBNull(36) ? string.Empty : dr.GetString(36), dr.GetBoolean(37), dr.IsDBNull(38) ? string.Empty : dr.GetString(38), dr.IsDBNull(39) ? string.Empty : dr.GetString(39), dr.IsDBNull(40) ? string.Empty : dr.GetString(40)));
                }

                return atendimentos;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByFuncionario", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAtendimentosByLoteEsocial(LoteEsocial loteEsocial, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByFilter");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ASO.ID_ASO, ASO.ID_CLIENTE_FUNCAO_FUNCIONARIO, ASO.CODIGO_ASO, ASO.DATA_ASO, ");
                query.Append(" ASO.NOME_FUNCIONARIO, ASO.CPF_FUNCIONARIO, CLIENTE_FUNCAO_FUNCIONARIO.MATRICULA, ASO.RAZAO_EMPRESA, ");
                query.Append(" ASO.CNPJ_EMPRESA, ASO.ID_PERIODICIDADE, PERIODICIDADE.DESCRICAO, ASO.SITUACAO, ");
                query.Append(" ASO.DATA_FINALIZACAO FROM SEG_ASO ASO ");
                query.Append(" INNER JOIN SEG_PERIODICIDADE PERIODICIDADE ON PERIODICIDADE.ID_PERIODICIDADE = ASO.ID_PERIODICIDADE ");
                query.Append(" JOIN SEG_USUARIO CRIADOR ON CRIADOR.ID_USUARIO = ASO.ID_USUARIO_CRIADOR");
                query.Append(" LEFT JOIN SEG_USUARIO FINALIZADOR ON FINALIZADOR.ID_USUARIO = ASO.ID_USUARIO_FINALIZADOR ");
                query.Append(" LEFT JOIN SEG_USUARIO CANCELADOR ON CANCELADOR.ID_USUARIO = ASO.ID_USUARIO_CANCELAMENTO ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTE_FUNCAO_FUNCIONARIO ON CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO = ASO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTE_FUNCAO_FUNCIONARIO.ID_SEG_CLIENTE_FUNCAO ");
                query.Append(" JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = CLIENTE_FUNCAO.ID_CLIENTE");
                query.Append(" WHERE ASO.ID_ASO IN (SELECT ID_ASO FROM SEG_ESOCIAL_LOTE_ASO WHERE ID_ESOCIAL_LOTE = :VALUE1 AND SITUACAO = 'E' ) ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = loteEsocial.Id;

                DataSet ds = new DataSet();
                adapter.Fill(ds, "Asos");
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<Aso> findAtendimentosByLoteEsocialList(LoteEsocial loteEsocial, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtendimentosByLoteEsocialList");

            List<Aso> atendimentos = new List<Aso>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ATENDIMENTO.ID_ASO, ATENDIMENTO.ID_MEDICO_EXAMINADOR, ATENDIMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO, ATENDIMENTO.CODIGO_ASO, ");
                query.Append(" ATENDIMENTO.DATA_ASO, ATENDIMENTO.NOME_FUNCIONARIO, ATENDIMENTO.RG_FUNCIONARIO, ATENDIMENTO.DATANASC_FUNCIONARIO, ATENDIMENTO.CPF_FUNCIONARIO, ");
                query.Append(" ATENDIMENTO.TIPOSAN_FUNCIONARIO, ATENDIMENTO.FTRH_FUNCIONARIO, ATENDIMENTO.MATRICULA_FUNCIONARIO, ATENDIMENTO.RAZAO_EMPRESA, ATENDIMENTO.CNPJ_EMPRESA, ");
                query.Append(" ATENDIMENTO.ENDERECO_EMPRESA, ATENDIMENTO.NUMERO_EMPRESA, ATENDIMENTO.COMPLEMENTO_EMPRESA, ATENDIMENTO.BAIRRO_EMPRESA, ATENDIMENTO.CEP_EMPRESA, ");
                query.Append(" ATENDIMENTO.CIDADE_EMPRESA, ATENDIMENTO.UF_EMPRESA, ATENDIMENTO.ID_PERIODICIDADE, ATENDIMENTO.SITUACAO, ATENDIMENTO.ID_MEDICO_COORDENADOR, ");
                query.Append(" ATENDIMENTO.CRM_COORDENADOR, ATENDIMENTO.TELEFONE1_COORDENADOR, ATENDIMENTO.TELEFONE2_COORDENADOR, ATENDIMENTO.NOME_COORDENADOR, ");
                query.Append(" ATENDIMENTO.CRM_EXAMINADOR, ATENDIMENTO.NOME_EXAMINADOR, ATENDIMENTO.TELEFONE1_EXAMINADOR, ATENDIMENTO.TELEFONE2_EXAMINADOR, ");
                query.Append(" ATENDIMENTO.ID_USUARIO_CRIADOR, ATENDIMENTO.ID_USUARIO_FINALIZADOR, ");
                query.Append(" ATENDIMENTO.ID_ULT_USUARIO_ALTERACAO, ATENDIMENTO.ID_ESTUDO, ATENDIMENTO.OBS_ALTERACAO, ATENDIMENTO.OBS_ASO, ATENDIMENTO.CONCLUSAO, ATENDIMENTO.DATA_GRAVACAO, ATENDIMENTO.DATA_CANCELAMENTO, ");
                query.Append(" ATENDIMENTO.ID_USUARIO_CANCELAMENTO, ATENDIMENTO.DATA_FINALIZACAO, ATENDIMENTO.ID_EMPRESA, ATENDIMENTO.OBS_CANCELAMENTO, ATENDIMENTO.ID_CENTROCUSTO, CLIENTE.ID_CLIENTE, ATENDIMENTO.DATA_VENCIMENTO, ATENDIMENTO.NUMERO_PO, ATENDIMENTO.PRIORIDADE, ATENDIMENTO.PIS, ATENDIMENTO.GHE_SETOR_AVULSO, ATENDIMENTO.FUNCAO_FUNCIONARIO ");
                query.Append(" FROM SEG_ASO ATENDIMENTO ");
                query.Append(" LEFT JOIN SEG_CENTROCUSTO CENTROCUSTO ON CENTROCUSTO.ID = ATENDIMENTO.ID_CENTROCUSTO ");
                query.Append(" LEFT JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = CENTROCUSTO.ID_CLIENTE ");
                query.Append(" WHERE ATENDIMENTO.ID_ASO IN (SELECT ID_ASO FROM SEG_ESOCIAL_LOTE_ASO WHERE ID_ESOCIAL_LOTE = :VALUE1 AND SITUACAO = 'E' ) ");
                query.Append(" ORDER BY ATENDIMENTO.ID_ASO ASC");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = loteEsocial.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    atendimentos.Add(new Aso(
                        dr.GetInt64(0), 
                        dr.IsDBNull(1) ? null : new Medico(dr.GetInt64(1)), 
                        dr.IsDBNull(2) ? null : new ClienteFuncaoFuncionario(dr.GetInt64(2), null, null, string.Empty, null, null, dr.GetString(11), false, string.Empty, string.Empty, false, true), 
                        dr.GetString(3), 
                        dr.GetDateTime(4), 
                        new Funcionario(dr.GetString(5), dr.GetString(6), dr.GetDateTime(7), dr.GetString(8), dr.GetString(9), dr.GetString(10)), 
                        new Cliente(null, dr.GetString(12), String.Empty, dr.GetString(13), String.Empty, dr.GetString(14), dr.GetString(15), dr.GetString(16), dr.GetString(17), dr.GetString(18), dr.GetString(20), String.Empty, String.Empty, String.Empty, String.Empty, null, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, 0, 0, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, null, String.Empty, false, String.Empty, false, null, null, false, false, false, false, new CidadeIbge(null, String.Empty, dr.GetString(19), String.Empty), null, false, false, null, string.Empty, false, false, false, false, false, false),
                        new Periodicidade(dr.GetInt64(21)), 
                        dr.GetString(22), 
                        dr.IsDBNull(23) ? null : new Medico(dr.GetInt64(23)), 
                        new Usuario(dr.GetInt64(32)), 
                        dr.IsDBNull(36) ? string.Empty : dr.GetString(36), 
                        dr.IsDBNull(35) ? null : new Estudo(dr.GetInt64(35)), 
                        dr.IsDBNull(33) ? null : new Usuario(dr.GetInt64(33)), 
                        dr.IsDBNull(41) ? null : new Usuario(dr.GetInt64(41)), 
                        dr.IsDBNull(42) ? (DateTime?)null : dr.GetDateTime(42), 
                        dr.IsDBNull(40) ? (DateTime?)null : dr.GetDateTime(40), 
                        dr.IsDBNull(39) ? (DateTime?)null : dr.GetDateTime(39), 
                        dr.IsDBNull(37) ? string.Empty : dr.GetString(37), 
                        dr.IsDBNull(38) ? string.Empty : dr.GetString(38), 
                        null, 
                        null, 
                        dr.IsDBNull(43) ? null : new Empresa(dr.GetInt64(43)), 
                        dr.IsDBNull(44) ? string.Empty : dr.GetString(44), 
                        dr.IsDBNull(34) ? null : new Usuario(dr.GetInt64(34)), 
                        dr.IsDBNull(45) ? null : new CentroCusto(dr.GetInt64(45), new Cliente(dr.GetInt64(46)), string.Empty, string.Empty), 
                        dr.IsDBNull(47) ? null : (DateTime?)dr.GetDateTime(47), 
                        dr.IsDBNull(48) ? string.Empty : dr.GetString(48), 
                        dr.GetBoolean(49), 
                        dr.IsDBNull(50) ? string.Empty : dr.GetString(50), 
                        dr.IsDBNull(51) ? string.Empty : dr.GetString(51), 
                        dr.IsDBNull(52) ? string.Empty : dr.GetString(52)));
                }

                return atendimentos;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtendimentosByLoteEsocialList", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public List<AtendimentoExame> FindAllExamesByAsoByBySalaInService(Aso aso,  Sala sala, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método FindAllExamesByAsoByBySalaInService");

            try
            {
                StringBuilder query = new StringBuilder();

                List<AtendimentoExame> exames = new List<AtendimentoExame>();

                query.Append(" SELECT E.ID_EXAME, E.DESCRICAO, E.SITUACAO, E.LABORATORIO, E.PRECO, E.PRIORIDADE, E.CUSTO, E.EXTERNO, E.LIBERA_DOCUMENTO, E.CODIGO_TUSS, E.EXAME_COMPLEMENTAR, SALA.ID_SALA, SALA.DESCRICAO AS NOME_SALA, SALA.SITUACAO, SALA.ANDAR, SALA.VIP, E.PERIODO_VENCIMENTO, SALA.ID_EMPRESA, SALA.SLUG_SALA, E.PADRAO_CONTRATO, SALA.QTDE_CHAMADA ");
                query.Append(" FROM SEG_ASO A  ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA ON CFEA.ID_ASO = A.ID_ASO ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME CFE ON CFE.ID_CLIENTE_FUNCAO_EXAME = CFEA.ID_CLIENTE_FUNCAO_EXAME ");
                query.Append(" JOIN SEG_EXAME E ON E.ID_EXAME = CFE.ID_EXAME ");
                query.Append(" JOIN SEG_SALA_EXAME SALA_EXAME ON SALA_EXAME.ID_SALA_EXAME = CFEA.ID_SALA_EXAME ");
                query.Append(" JOIN SEG_SALA SALA ON SALA.ID_SALA = SALA_EXAME.ID_SALA ");
                query.Append(" WHERE A.ID_ASO = :VALUE1 ");
                query.Append(" AND CFEA.ATENDIDO = TRUE ");
                query.Append(" AND CFEA.FINALIZADO = FALSE ");
                query.Append(" AND SALA.ID_SALA <> :VALUE2 ");
                query.Append(" UNION ( ");
                query.Append(" SELECT E.ID_EXAME, E.DESCRICAO, E.SITUACAO, E.LABORATORIO, E.PRECO, E.PRIORIDADE, E.CUSTO, E.EXTERNO, E.LIBERA_DOCUMENTO, E.CODIGO_TUSS, E.EXAME_COMPLEMENTAR, SALA.ID_SALA, SALA.DESCRICAO AS NOME_SALA, SALA.SITUACAO, SALA.ANDAR, SALA.VIP, E.PERIODO_VENCIMENTO, SALA.ID_EMPRESA, SALA.SLUG_SALA, E.PADRAO_CONTRATO, SALA.QTDE_CHAMADA ");
                query.Append(" FROM SEG_ASO A ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA ON GFAEA.ID_ASO = A.ID_ASO AND GFAEA.DUPLICADO IS FALSE ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GFAE ON GFAE.ID_GHE_FONTE_AGENTE_EXAME = GFAEA.ID_GHE_FONTE_AGENTE_EXAME ");
                query.Append(" JOIN SEG_EXAME E ON E.ID_EXAME = GFAE.ID_EXAME ");
                query.Append(" JOIN SEG_SALA_EXAME SALA_EXAME ON SALA_EXAME.ID_SALA_EXAME = GFAEA.ID_SALA_EXAME ");
                query.Append(" JOIN SEG_SALA SALA ON SALA.ID_SALA = SALA_EXAME.ID_SALA ");
                query.Append(" WHERE A.ID_ASO = :VALUE1 ");
                query.Append(" AND GFAEA.ATENDIDO = TRUE ");
                query.Append(" AND GFAEA.FINALIZADO = FALSE ");
                query.Append(" AND SALA.ID_SALA <> :VALUE2 ");
                query.Append(" )");
                query.Append(" ORDER BY DESCRICAO ASC ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = sala.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    exames.Add(new AtendimentoExame(new Exame(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.IsDBNull(3) ? null : (Boolean?)dr.GetBoolean(3), dr.GetDecimal(4), dr.GetInt32(5), dr.GetDecimal(6), dr.GetBoolean(7), dr.GetBoolean(8), dr.IsDBNull(9) ? string.Empty : dr.GetString(9), dr.GetBoolean(10), dr.IsDBNull(16) ? (int?)null : dr.GetInt32(16), dr.GetBoolean(19)), new Sala(dr.GetInt64(11), dr.GetString(12), dr.GetString(13), dr.GetInt32(14), dr.GetBoolean(15), new Empresa(dr.GetInt64(17)), dr.GetString(18), dr.GetInt32(20))));
                }

                return exames;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - FindAllExamesByAsoByBySalaInService", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void updateGheSetorAvulso(Aso aso, List<GheSetorAvulso> gheSetorAvulso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateGheSetorAvulso");

            try
            {
                StringBuilder listaGheAvulso = new StringBuilder();
                int index = 0;

                gheSetorAvulso.ForEach(delegate(GheSetorAvulso gs)
                {
                    listaGheAvulso.Append(gs.Ghe);
                    if (!string.IsNullOrEmpty(gs.Setor)) listaGheAvulso.Append(" - " + gs.Setor);
                    if (gheSetorAvulso.Count -1 > index) listaGheAvulso.Append(", ");
                    index++;
                    
                });

                NpgsqlCommand command = new NpgsqlCommand("UPDATE SEG_ASO SET GHE_SETOR_AVULSO = :VALUE2 WHERE ID_ASO = :VALUE1", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = listaGheAvulso.ToString();

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateGheSetorAvulso", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void updateTipoAtendimento(Aso atendimento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateTipoAtendimento");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("UPDATE SEG_ASO SET ID_PERIODICIDADE = :VALUE2 WHERE ID_ASO = :VALUE1", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = atendimento.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = atendimento.Periodicidade.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateTipoAtendimento", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void updateSituacao(Aso aso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateSituacao");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append("UPDATE SEG_ASO SET SITUACAO = :VALUE2 WHERE ID_ASO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = aso.Situacao;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateSituacao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<Aso> findAllAtendimentoInPeriodoByFuncionarioInCliente(ClienteFuncaoFuncionario clienteFuncaoFuncionario, DateTime dataInicial, DateTime dataFinal, Empresa empresa, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllAtendimentoInPeriodoByFuncionarioInCliente");

            try
            {
                StringBuilder query = new StringBuilder();

                List<Aso> atendimentos = new List<Aso>();

                query.Append(" SELECT ATENDIMENTO.ID_ASO, ATENDIMENTO.CODIGO_ASO, ATENDIMENTO.ID_MEDICO_EXAMINADOR, ATENDIMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO, ATENDIMENTO.DATA_ASO, ATENDIMENTO.NOME_FUNCIONARIO, ATENDIMENTO.RG_FUNCIONARIO, ATENDIMENTO.DATANASC_FUNCIONARIO, ATENDIMENTO.CPF_FUNCIONARIO, ATENDIMENTO.TIPOSAN_FUNCIONARIO, ATENDIMENTO.FTRH_FUNCIONARIO, ATENDIMENTO.MATRICULA_FUNCIONARIO, ATENDIMENTO.RAZAO_EMPRESA, ATENDIMENTO.CNPJ_EMPRESA, ATENDIMENTO.ENDERECO_EMPRESA, ATENDIMENTO.NUMERO_EMPRESA, ATENDIMENTO.COMPLEMENTO_EMPRESA, ATENDIMENTO.BAIRRO_EMPRESA, ATENDIMENTO.CEP_EMPRESA, ATENDIMENTO.CIDADE_EMPRESA, ATENDIMENTO.UF_EMPRESA, ATENDIMENTO.ID_PERIODICIDADE, ATENDIMENTO.SITUACAO, ATENDIMENTO.ID_MEDICO_COORDENADOR, ATENDIMENTO.CRM_COORDENADOR, ATENDIMENTO.TELEFONE1_COORDENADOR, ATENDIMENTO.TELEFONE2_COORDENADOR, ATENDIMENTO.NOME_COORDENADOR, ATENDIMENTO.CRM_EXAMINADOR, ATENDIMENTO.NOME_EXAMINADOR, ATENDIMENTO.TELEFONE1_EXAMINADOR, ATENDIMENTO.TELEFONE2_EXAMINADOR, ATENDIMENTO.ID_USUARIO_CRIADOR, ATENDIMENTO.ID_USUARIO_FINALIZADOR, ATENDIMENTO.ID_ESTUDO, ATENDIMENTO.OBS_ALTERACAO, ATENDIMENTO.OBS_ASO, ATENDIMENTO.CONCLUSAO, ATENDIMENTO.ID_USUARIO_CANCELAMENTO, ATENDIMENTO.ID_ULT_USUARIO_ALTERACAO, ATENDIMENTO.DATA_FINALIZACAO, ATENDIMENTO.SENHA, ATENDIMENTO.ID_PROTOCOLO, ATENDIMENTO.OBS_CANCELAMENTO, ATENDIMENTO.ID_EMPRESA, ATENDIMENTO.ID_CENTROCUSTO, ATENDIMENTO.DATA_VENCIMENTO, ATENDIMENTO.NUMERO_PO, ATENDIMENTO.PRIORIDADE, ATENDIMENTO.GHE_SETOR_AVULSO, ATENDIMENTO.FUNCAO_FUNCIONARIO, ATENDIMENTO.PIS, ATENDIMENTO.CONFINADO, ATENDIMENTO.ALTURA, ATENDIMENTO.DATA_CANCELAMENTO, ATENDIMENTO.DATA_GRAVACAO, ");
                query.Append(" PERIODICIDADE.DESCRICAO, PERIODICIDADE.DIAS, PERIODICIDADE.IND_PERIODICO "); 
                query.Append(" FROM SEG_ASO ATENDIMENTO  ");
                query.Append(" JOIN SEG_PERIODICIDADE PERIODICIDADE ON PERIODICIDADE.ID_PERIODICIDADE = ATENDIMENTO.ID_PERIODICIDADE ");
                query.Append(" WHERE TRUE ");
                query.Append(" AND ATENDIMENTO.CPF_FUNCIONARIO = :VALUE1   ");
                query.Append(" AND ATENDIMENTO.DATA_ASO BETWEEN :VALUE2 AND :VALUE3 ");
                query.Append(" AND ATENDIMENTO.SITUACAO = 'F' ");
                query.Append(" AND PERIODICIDADE.ID_PERIODICIDADE IN (4, 7) ");
                query.Append(" AND ATENDIMENTO.ID_EMPRESA = :VALUE4 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = clienteFuncaoFuncionario.Funcionario.Cpf;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                command.Parameters[1].Value = dataInicial;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Timestamp));
                command.Parameters[2].Value = dataFinal;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = empresa.Id;


                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    atendimentos.Add(new Aso(dr.GetInt64(0), dr.IsDBNull(2) ? null : new Medico(dr.GetInt64(2)), new ClienteFuncaoFuncionario(dr.GetInt64(3)), dr.GetString(1), dr.GetDateTime(4), clienteFuncaoFuncionario.Funcionario, clienteFuncaoFuncionario.ClienteFuncao.Cliente, new Periodicidade(dr.GetInt64(21), dr.GetString(56), dr.GetInt32(57)), dr.GetString(22), dr.IsDBNull(23) ? null : new Medico(dr.GetInt64(23)), new Usuario(dr.GetInt64(32)), dr.IsDBNull(35) ? string.Empty : dr.GetString(35), dr.IsDBNull(34) ? null : new Estudo(dr.GetInt64(34)), dr.IsDBNull(33) ? null : new Usuario(dr.GetInt64(33)), dr.IsDBNull(38) ? null : new Usuario(dr.GetInt64(38)), dr.IsDBNull(40) ? (DateTime?)null : dr.GetDateTime(40), dr.IsDBNull(54) ? (DateTime?)null : dr.GetDateTime(54), dr.IsDBNull(55) ? (DateTime?)null : dr.GetDateTime(55), dr.IsDBNull(36) ? string.Empty : dr.GetString(36), dr.IsDBNull(37) ? string.Empty : dr.GetString(37), dr.IsDBNull(41) ? string.Empty : dr.GetString(41), dr.IsDBNull(42) ? null : new Protocolo(dr.GetInt64(42)), dr.IsDBNull(44) ? null : empresa, dr.IsDBNull(35) ? string.Empty : dr.GetString(35), dr.IsDBNull(39) ? null : new Usuario(dr.GetInt64(39)), dr.IsDBNull(45) ? null : new CentroCusto(dr.GetInt64(45)), dr.IsDBNull(46) ? (DateTime?)null : dr.GetDateTime(46), dr.IsDBNull(47) ? string.Empty : dr.GetString(47), dr.GetBoolean(48), dr.IsDBNull(51) ? string.Empty : dr.GetString(51), dr.IsDBNull(49) ? string.Empty : dr.GetString(49), dr.IsDBNull(50) ? string.Empty : dr.GetString(50)));
                    
                }

                return atendimentos;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllAtendimentoInPeriodoByFuncionarioInCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }


        public List<Aso> findAllAtendimentoInSituacaoByClienteAndEmpresaAndPeriodo(Cliente cliente, string situacao, DateTime dataInicial, DateTime dataFinal, Empresa empresa, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllAtendimentoInSituacaoByClienteAndEmpresaAndPeriodo");

            try
            {
                StringBuilder query = new StringBuilder();

                List<Aso> atendimentos = new List<Aso>();

                query.Append(" select atendimento.id_aso, atendimento.codigo_aso, atendimento.id_medico_examinador, atendimento.id_cliente_funcao_funcionario, atendimento.data_aso, atendimento.nome_funcionario, atendimento.rg_funcionario, atendimento.datanasc_funcionario, atendimento.cpf_funcionario, atendimento.tiposan_funcionario, atendimento.ftrh_funcionario, atendimento.matricula_funcionario, atendimento.razao_empresa, atendimento.cnpj_empresa, atendimento.endereco_empresa, atendimento.numero_empresa, atendimento.complemento_empresa, atendimento.bairro_empresa, atendimento.cep_empresa, atendimento.cidade_empresa, atendimento.uf_empresa, atendimento.id_periodicidade, atendimento.situacao, atendimento.id_medico_coordenador, atendimento.crm_coordenador, atendimento.telefone1_coordenador, atendimento.telefone2_coordenador, atendimento.nome_coordenador, atendimento.crm_examinador, atendimento.nome_examinador, atendimento.telefone1_examinador, atendimento.telefone2_examinador, atendimento.id_usuario_criador, atendimento.id_usuario_finalizador, atendimento.id_estudo, atendimento.obs_alteracao, atendimento.obs_aso,  atendimento.conclusao, atendimento.data_gravacao, atendimento.data_cancelamento, atendimento.id_usuario_cancelamento, atendimento.id_ult_usuario_alteracao, atendimento.data_finalizacao, atendimento.senha, atendimento.id_protocolo, atendimento.obs_cancelamento, atendimento.id_empresa, atendimento.id_centrocusto, atendimento.data_vencimento, atendimento.numero_po, atendimento.prioridade, atendimento.ghe_setor_avulso, atendimento.funcao_funcionario, atendimento.pis, atendimento.confinado, atendimento.altura, cliente_funcao_funcionario.id_funcionario, cliente.id_cliente ");
                query.Append(" from seg_aso atendimento ");
                query.Append(" join seg_cliente_funcao_funcionario cliente_funcao_funcionario on atendimento.id_cliente_funcao_funcionario = cliente_funcao_funcionario.id_cliente_funcao_funcionario ");
                query.Append(" join seg_cliente_funcao cliente_funcao on cliente_funcao.id_seg_cliente_funcao = cliente_funcao_funcionario.id_seg_cliente_funcao  ");
                query.Append(" join seg_cliente cliente on cliente.id_cliente = cliente_funcao.id_cliente ");
                query.Append(" where true ");
                query.Append(" and cliente.id_cliente = :value1 ");
                query.Append(" and atendimento.situacao = :value2 ");
                query.Append(" and atendimento.data_aso between :value3 and :value4 ");
                query.Append(" and atendimento.id_empresa = :value5 ");
                query.Append(" order by atendimento.codigo_aso asc ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Timestamp));
                command.Parameters[2].Value = dataInicial;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Timestamp));
                command.Parameters[3].Value = dataFinal;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Integer));
                command.Parameters[4].Value = empresa.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    Aso atendimento = new Aso();
                    atendimento.Id = (long)dr.GetInt64(0);
                    atendimento.Codigo = dr.GetString(1);
                    atendimento.Examinador = dr.IsDBNull(2) ? null : new Medico(dr.GetInt64(2));
                    atendimento.ClienteFuncaoFuncionario = new ClienteFuncaoFuncionario(dr.GetInt64(3));
                    atendimento.DataElaboracao = dr.GetDateTime(4);
                    atendimento.Funcionario = new Funcionario(dr.GetInt64(56));
                    atendimento.Cliente = new Cliente(dr.GetInt64(57));
                    atendimento.Periodicidade = new Periodicidade(dr.GetInt64(21));
                    atendimento.Situacao = dr.GetString(22);
                    atendimento.Coordenador = dr.IsDBNull(23) ? null : new Medico(dr.GetInt64(23));
                    atendimento.Criador = new Usuario(dr.GetInt64(32));
                    atendimento.Finalizador = new Usuario(dr.GetInt64(33));
                    atendimento.Estudo = dr.IsDBNull(34) ? null : new Estudo(dr.GetInt64(34));
                    atendimento.JustificativaAlteracao = dr.IsDBNull(35) ? string.Empty : dr.GetString(35);
                    atendimento.ObservacaoAso = dr.IsDBNull(36) ? string.Empty : dr.GetString(36);
                    atendimento.Conclusao = dr.IsDBNull(37) ? string.Empty : dr.GetString(37);
                    atendimento.DataGravacao = dr.GetDateTime(38);
                    atendimento.DataCancelamento = dr.IsDBNull(39) ? (DateTime?)null : dr.GetDateTime(39);
                    atendimento.UsuarioCancelamento = dr.IsDBNull(40) ? null : new Usuario(dr.GetInt64(40));
                    atendimento.UsuarioAlteracao = dr.IsDBNull(41) ? null : new Usuario(dr.GetInt64(41));
                    atendimento.DataFinalizacao = dr.IsDBNull(42) ? (DateTime?)null : dr.GetDateTime(42);
                    atendimento.Senha = dr.IsDBNull(43) ? string.Empty : dr.GetString(43);
                    atendimento.Protocolo = dr.IsDBNull(44) ? null : new Protocolo(dr.GetInt64(44));
                    atendimento.Justificativa = dr.IsDBNull(45) ? string.Empty : dr.GetString(45);
                    atendimento.Empresa = new Empresa(dr.GetInt64(46));
                    atendimento.CentroCusto = dr.IsDBNull(47) ? null : new CentroCusto(dr.GetInt64(47));
                    atendimento.DataVencimento = dr.IsDBNull(48) ? (DateTime?)null : dr.GetDateTime(48);
                    atendimento.CodigoPo = dr.IsDBNull(49) ? string.Empty : dr.GetString(49);
                    atendimento.Prioridade = dr.GetBoolean(50);
                    atendimento.Pis = dr.IsDBNull(53) ? string.Empty : dr.GetString(53);
                    atendimento.Confinado = dr.GetBoolean(54);
                    atendimento.Altura = dr.GetBoolean(55);
                    atendimentos.Add(atendimento);
                }

                return atendimentos;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllAtendimentoInSituacaoByClienteAndEmpresaAndPeriodo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
            


    }
}


