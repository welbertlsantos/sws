﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using SWS.Excecao;
using NpgsqlTypes;
using SWS.Entidade;
using SWS.Helper;

namespace SWS.Dao
{
    class RevisaoDao : IRevisaoDao
    {
        public void incluir(Revisao revisao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluir");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" insert into seg_revisao (id_revisao, id_estudo, comentario, id_estudo_raiz, ");
                query.Append(" id_estudo_revisao, tipo_revisao, versao_estudo, data_revisao, ind_correcao ) values ");
                query.Append(" (nextval('seg_revisao_id_revisao_seq'), :value1, :value2, :value3, :value4, :value5, :value6, NOW(), :value7)");

                NpgsqlCommand command = new NpgsqlCommand( query.ToString() , dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value2", NpgsqlDbType.Varchar));
                command.Parameters.Add(new NpgsqlParameter("value3", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value4", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value5", NpgsqlDbType.Varchar));
                command.Parameters.Add(new NpgsqlParameter("value6", NpgsqlDbType.Varchar));
                command.Parameters.Add(new NpgsqlParameter("value7", NpgsqlDbType.Boolean));

                command.Parameters[0].Value = revisao.Estudo.Id;
                command.Parameters[1].Value = revisao.Comentario;
                command.Parameters[2].Value = revisao.EstudoRaiz.Id;
                command.Parameters[3].Value = revisao.EstudoRevisao.Id;
                command.Parameters[4].Value = revisao.TipoRevisao;
                command.Parameters[5].Value = revisao.VersaoEstudo;
                command.Parameters[6].Value = revisao.IndCorrecao;

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluir", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public bool verificaPodeReplicar(Int64? idEstudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaPodeReplicar");
            bool podeAlterar = true;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_REVISAO, ID_ESTUDO, COMENTARIO, ID_ESTUDO_RAIZ, ");
                query.Append(" ID_ESTUDO_REVISAO, TIPO_REVISAO, VERSAO_ESTUDO, DATA_REVISAO FROM SEG_REVISAO ");
                query.Append(" WHERE ID_ESTUDO_REVISAO = :VALUE1 AND ID_ESTUDO <> :VALUE1");

                
                NpgsqlCommand commandRevisao = new NpgsqlCommand(query.ToString(), dbConnection);

                commandRevisao.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                commandRevisao.Parameters[0].Value = idEstudo;

                NpgsqlDataReader drRevisao = commandRevisao.ExecuteReader();

                if (drRevisao.Read())
                {
                    podeAlterar = false;
                }

                drRevisao.Close();

                return podeAlterar;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaPodeReplicar", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64? findRaiz(Int64? idEstudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findRaiz ");
            Int64? idRaiz = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" select id_revisao, id_estudo, comentario, id_estudo_raiz, ");
                query.Append(" id_estudo_revisao, tipo_revisao, versao_estudo, data_revisao from seg_revisao where id_estudo = :value1");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = idEstudo;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    idRaiz = dr.GetInt64(3);
                }

                return idRaiz;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findRaiz", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Revisao findRevisao(Int64? idEstudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findRevisao ");
            Revisao revisao = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" select id_revisao, id_estudo, comentario, id_estudo_raiz, ");
                query.Append(" id_estudo_revisao, tipo_revisao, versao_estudo, data_revisao, ind_correcao from seg_revisao where id_estudo = :value1");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = idEstudo;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    revisao = new Revisao(dr.GetInt64(0), new Estudo(dr.GetInt64(1)), dr.GetString(2), new Estudo(dr.GetInt64(3)), new Estudo(dr.GetInt64(4)), dr.GetString(5), dr.GetString(6), dr.GetDateTime(7), dr.GetBoolean(8));
                }

                return revisao;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findRevisao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean isCorrecao(Int64? idEstudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método isCorrecao ");
            Boolean isCorrecao = false;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" select ind_correcao from seg_revisao where id_estudo = :value1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = idEstudo;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    isCorrecao = dr.GetBoolean(0);
                }

                return isCorrecao;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - isCorrecao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
