﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using System.Data;
using Npgsql;
using NpgsqlTypes;
using SWS.Excecao;
using SWS.View.Resources;
using SWS.Helper;

namespace SWS.Dao 
{
    class GheSetorClienteFuncaoDao : IGheSetorClienteFuncaoDao
    {

        public HashSet<GheSetorClienteFuncao> findAllByGheSetorClienteFuncao(GheSetorClienteFuncao gheSetorClienteFuncao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByGheSetorClienteFuncao");

            HashSet<GheSetorClienteFuncao> gheSetorClienteFuncaoRetorno = new HashSet<GheSetorClienteFuncao>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR_CLIENTE_FUNCAO, GHE_SETOR_CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO, GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR,");
                query.Append(" CLIENTE_FUNCAO.ID_CLIENTE, CLIENTE_FUNCAO.ID_FUNCAO, CLIENTE_FUNCAO.SITUACAO, CLIENTE_FUNCAO.DATA_CADASTRO, CLIENTE_FUNCAO.DATA_DESLIGAMENTO,");
                query.Append(" FUNCAO.DESCRICAO, FUNCAO.COD_CBO, FUNCAO.SITUACAO,");
                query.Append(" CLIENTE.CNPJ, FUNCAO.COMENTARIO, GHE_SETOR_CLIENTE_FUNCAO.CONFINADO, GHE_SETOR_CLIENTE_FUNCAO.ALTURA, GHE_SETOR_CLIENTE_FUNCAO.SITUACAO, COMENTARIO_FUNCAO.ID_COMENTARIO_FUNCAO, COMENTARIO_FUNCAO.ID_FUNCAO, COMENTARIO_FUNCAO.ATIVIDADE, COMENTARIO_FUNCAO.SITUACAO, GHE_SETOR_CLIENTE_FUNCAO.ELETRICIDADE ");
                query.Append(" FROM SEG_GHE_SETOR_CLIENTE_FUNCAO GHE_SETOR_CLIENTE_FUNCAO");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = GHE_SETOR_CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO");
                query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTE_FUNCAO.ID_FUNCAO");
                query.Append(" JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = CLIENTE_FUNCAO.ID_CLIENTE");
                query.Append(" LEFT JOIN SEG_COMENTARIO_FUNCAO COMENTARIO_FUNCAO ON COMENTARIO_FUNCAO.ID_COMENTARIO_FUNCAO = GHE_SETOR_CLIENTE_FUNCAO.ID_COMENTARIO_FUNCAO AND COMENTARIO_FUNCAO.SITUACAO = 'A' ");
                query.Append(" WHERE CLIENTE_FUNCAO.SITUACAO = 'A' AND  ");
                query.Append(" GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR = :VALUE1  ");
                query.Append(" ORDER BY FUNCAO.DESCRICAO ASC");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheSetorClienteFuncao.GheSetor.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gheSetorClienteFuncaoRetorno.Add(new GheSetorClienteFuncao(dr.GetInt64(0), new ClienteFuncao(dr.GetInt64(1), new Cliente(dr.GetInt64(3)), new Funcao(dr.GetInt64(4), dr.GetString(8), dr.GetString(9), dr.GetString(10), dr.GetString(12)), dr.IsDBNull(5) ? string.Empty : dr.GetString(5), dr.IsDBNull(6) ? (DateTime?)null : dr.GetDateTime(6), dr.IsDBNull(7) ? (DateTime?)null : dr.GetDateTime(7)), gheSetorClienteFuncao.GheSetor, dr.GetBoolean(13), dr.GetBoolean(14), dr.GetString(15), dr.IsDBNull(16) ? null : new ComentarioFuncao(dr.GetInt64(16), dr.IsDBNull(17) ? null : new Funcao(dr.GetInt64(17), string.Empty, string.Empty, string.Empty, string.Empty), dr.IsDBNull(18) ? string.Empty : dr.GetString(18), dr.IsDBNull(19) ? string.Empty : dr.GetString(19)), dr.GetBoolean(20)));
                }
                
                return gheSetorClienteFuncaoRetorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByGheSetorClienteFuncao", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public DataSet findAllByGheSetorClienteFuncaoAndCliente(GheSetorClienteFuncao gheSetorClienteFuncao, Cliente cliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByGheSetorClienteFuncaoAndCliente");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO, CLIENTE_FUNCAO.ID_FUNCAO, FUNCAO.DESCRICAO, FALSE ");
                query.Append(" FROM SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO");
                query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTE_FUNCAO.ID_FUNCAO");
                query.Append(" WHERE CLIENTE_FUNCAO.SITUACAO = 'A' AND ");
                query.Append(" CLIENTE_FUNCAO.ID_CLIENTE = :VALUE1 AND CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO NOT IN ");
                query.Append(" (SELECT ID_SEG_CLIENTE_FUNCAO FROM SEG_GHE_SETOR_CLIENTE_FUNCAO WHERE ");
                query.Append(" ID_GHE_SETOR = :VALUE2) "); 
                
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = cliente.Id;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[1].Value = gheSetorClienteFuncao.GheSetor.Id;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Funções");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByGheSetorClienteFuncaoAndCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public GheSetorClienteFuncao insert(GheSetorClienteFuncao gheSetorClienteFuncao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            try
            {
                StringBuilder query = new StringBuilder();

                gheSetorClienteFuncao.Id = recuperaProximoId(dbConnection);

                query.Append(" INSERT INTO SEG_GHE_SETOR_CLIENTE_FUNCAO ");
                query.Append(" (ID_GHE_SETOR_CLIENTE_FUNCAO, ID_SEG_CLIENTE_FUNCAO, ID_GHE_SETOR, SITUACAO, CONFINADO, ALTURA, ID_COMENTARIO_FUNCAO ) ");
                query.Append(" VALUES ( :VALUE1, :VALUE2, :VALUE3, 'A', :VALUE4, :VALUE5, :VALUE6)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheSetorClienteFuncao.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = gheSetorClienteFuncao.ClienteFuncao.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = gheSetorClienteFuncao.GheSetor.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Boolean));
                command.Parameters[3].Value = gheSetorClienteFuncao.Confinado;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Boolean));
                command.Parameters[4].Value = gheSetorClienteFuncao.Altura;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Integer));
                command.Parameters[5].Value = gheSetorClienteFuncao.ComentarioFuncao != null ? gheSetorClienteFuncao.ComentarioFuncao.Id : null;

                command.ExecuteNonQuery();

                return gheSetorClienteFuncao;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void delete(GheSetorClienteFuncao gheSetorClienteFuncao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" DELETE FROM SEG_GHE_SETOR_CLIENTE_FUNCAO WHERE ID_GHE_SETOR_CLIENTE_FUNCAO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = gheSetorClienteFuncao.Id;
                
                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public HashSet<GheSetorClienteFuncao> findAtivosByGheSetor(GheSetor gheSetor, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtivosByGheSetor");

            HashSet<GheSetorClienteFuncao> gheSetorClienteFuncaoRetorno = new HashSet<GheSetorClienteFuncao>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR_CLIENTE_FUNCAO, GHE_SETOR_CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO, GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR, ");
                query.Append(" CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO, CLIENTE_FUNCAO.ID_CLIENTE, CLIENTE_FUNCAO.ID_FUNCAO, CLIENTE_FUNCAO.SITUACAO, CLIENTE_FUNCAO.DATA_CADASTRO, CLIENTE_FUNCAO.DATA_DESLIGAMENTO, ");
                query.Append(" FUNCAO.ID_FUNCAO, FUNCAO.DESCRICAO, FUNCAO.COD_CBO, FUNCAO.SITUACAO, FUNCAO.COMENTARIO, GHE_SETOR_CLIENTE_FUNCAO.CONFINADO, GHE_SETOR_CLIENTE_FUNCAO.ALTURA, GHE_SETOR_CLIENTE_FUNCAO.SITUACAO, ");
                query.Append(" COMENTARIO_FUNCAO.ID_COMENTARIO_FUNCAO, COMENTARIO_FUNCAO.ID_FUNCAO, COMENTARIO_FUNCAO.ATIVIDADE, COMENTARIO_FUNCAO.SITUACAO, GHE_SETOR_CLIENTE_FUNCAO.ELETRICIDADE ");
                query.Append(" FROM SEG_GHE_SETOR_CLIENTE_FUNCAO GHE_SETOR_CLIENTE_FUNCAO");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = GHE_SETOR_CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO");
                query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTE_FUNCAO.ID_FUNCAO");
                query.Append(" LEFT JOIN SEG_COMENTARIO_FUNCAO COMENTARIO_FUNCAO ON COMENTARIO_FUNCAO.ID_COMENTARIO_FUNCAO = GHE_SETOR_CLIENTE_FUNCAO.ID_COMENTARIO_FUNCAO AND COMENTARIO_FUNCAO.SITUACAO = 'A' ");
                query.Append(" WHERE GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR = :VALUE1 AND GHE_SETOR_CLIENTE_FUNCAO.SITUACAO = 'A' ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheSetor.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gheSetorClienteFuncaoRetorno.Add(new GheSetorClienteFuncao(dr.GetInt64(0), new ClienteFuncao(dr.GetInt64(1), new Cliente(dr.GetInt64(4)), new Funcao(dr.GetInt64(9), dr.GetString(10), dr.GetString(11), dr.GetString(12), dr.GetString(13)), dr.IsDBNull(6) ? string.Empty : dr.GetString(6), dr.IsDBNull(7) ? (DateTime?)null : dr.GetDateTime(7), dr.IsDBNull(8) ? (DateTime?)null : dr.GetDateTime(8)), gheSetor, dr.GetBoolean(14), dr.GetBoolean(15), dr.IsDBNull(16) ? null : dr.GetString(16), dr.IsDBNull(17) ? null : new ComentarioFuncao(dr.GetInt64(17), dr.IsDBNull(18) ? null : new Funcao(dr.GetInt64(18), string.Empty, string.Empty, string.Empty, string.Empty), dr.IsDBNull(19) ? string.Empty : dr.GetString(19), dr.IsDBNull(20) ? string.Empty : dr.GetString(20)), dr.GetBoolean(21)));
                }
                return gheSetorClienteFuncaoRetorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtivosByGheSetor", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public HashSet<GheSetorClienteFuncao> findAtivosByGheAndFuncao(Ghe ghe, Funcao funcao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtivosByGheAndFuncao");

            HashSet<GheSetorClienteFuncao> gheSetorClienteFuncaoRetorno = new HashSet<GheSetorClienteFuncao>();

            try
            {
                StringBuilder query = new StringBuilder();

                

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = ghe.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = funcao.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gheSetorClienteFuncaoRetorno.Add(new GheSetorClienteFuncao(dr.GetInt64(0), new ClienteFuncao(dr.GetInt64(1), dr.IsDBNull(4) ? null : new Cliente(dr.GetInt64(4)), new Funcao(dr.GetInt64(9), dr.GetString(10), dr.GetString(11), dr.GetString(12), dr.GetString(13)), dr.IsDBNull(6) ? string.Empty : dr.GetString(6), dr.IsDBNull(7) ? (DateTime?)null : dr.GetDateTime(7), dr.IsDBNull(8) ? (DateTime?)null : dr.GetDateTime(8)), new GheSetor(dr.GetInt64(2), null, null, string.Empty), dr.GetBoolean(14), dr.GetBoolean(15), dr.IsDBNull(16) ? null : dr.GetString(16), dr.IsDBNull(17) ? null : new ComentarioFuncao(dr.GetInt64(17), dr.IsDBNull(18) ? null : new Funcao(dr.GetInt64(18), string.Empty, string.Empty, string.Empty, string.Empty), dr.IsDBNull(19) ? string.Empty : dr.GetString(19), dr.IsDBNull(20) ? string.Empty : dr.GetString(20)), dr.GetBoolean(21)));
                }

                return gheSetorClienteFuncaoRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtivosByGheAndFuncao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void update(GheSetorClienteFuncao gheSetorClienteFuncao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_GHE_SETOR_CLIENTE_FUNCAO SET SITUACAO = :VALUE5, ID_COMENTARIO_FUNCAO = :VALUE2, CONFINADO = :VALUE3, ALTURA = :VALUE4, ELETRICIDADE = :VALUE6 WHERE ID_GHE_SETOR_CLIENTE_FUNCAO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheSetorClienteFuncao.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = gheSetorClienteFuncao.ComentarioFuncao != null ? gheSetorClienteFuncao.ComentarioFuncao.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Boolean));
                command.Parameters[2].Value = gheSetorClienteFuncao.Confinado;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Boolean));
                command.Parameters[3].Value = gheSetorClienteFuncao.Altura;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = gheSetorClienteFuncao.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Boolean));
                command.Parameters[5].Value = gheSetorClienteFuncao.Eletricidade;

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAllByGheSetor(GheSetor gheSetor, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByGheSetor");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR_CLIENTE_FUNCAO, GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR,");
                query.Append(" CLIENTE_FUNCAO.ID_FUNCAO, FUNCAO.DESCRICAO, COMENTARIO_FUNCAO.ATIVIDADE, GHE_SETOR_CLIENTE_FUNCAO.CONFINADO, GHE_SETOR_CLIENTE_FUNCAO.ALTURA, CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO, COMENTARIO_FUNCAO.ID_COMENTARIO_FUNCAO, GHE_SETOR_CLIENTE_FUNCAO.ELETRICIDADE");
                query.Append(" FROM SEG_GHE_SETOR_CLIENTE_FUNCAO GHE_SETOR_CLIENTE_FUNCAO ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = GHE_SETOR_CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO ");
                query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTE_FUNCAO.ID_FUNCAO ");
                query.Append(" LEFT JOIN SEG_COMENTARIO_FUNCAO COMENTARIO_FUNCAO ON COMENTARIO_FUNCAO.ID_COMENTARIO_FUNCAO = GHE_SETOR_CLIENTE_FUNCAO.ID_COMENTARIO_FUNCAO ");
                query.Append(" WHERE ");
                query.Append(" GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR = :VALUE1 AND GHE_SETOR_CLIENTE_FUNCAO.SITUACAO = 'A' ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                
                adapter.SelectCommand.Parameters[0].Value = gheSetor.Id;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Funções");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByGheSetor", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public Boolean IsNotUsedComentarioByGheSetorClienteFuncao(GheSetorClienteFuncao gheSetorClienteFuncao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método IsNotUsedComentarioByGheSetorClienteFuncao");

            Boolean retorno = true;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT * ");
                query.Append(" FROM SEG_GHE_SETOR_CLIENTE_FUNCAO GSCF");
                query.Append(" WHERE  ");
                query.Append(" ID_GHE_SETOR_CLIENTE_FUNCAO = :VALUE1 AND ");
                query.Append(" ID_COMENTARIO_FUNCAO IS NULL");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheSetorClienteFuncao.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    retorno = false;
                }

                return retorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - IsNotUsedComentarioByGheSetorClienteFuncao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_GHE_SETOR_CLIENTE_FUNCAO_ID_GHE_SETOR_CLIENTE_FUNCAO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public GheSetorClienteFuncao findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            GheSetorClienteFuncao gheSetorClienteFuncao = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR_CLIENTE_FUNCAO, GHE_SETOR_CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO, GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR, GHE_SETOR_CLIENTE_FUNCAO.SITUACAO, GHE_SETOR_CLIENTE_FUNCAO.ID_COMENTARIO_FUNCAO, GHE_SETOR_CLIENTE_FUNCAO.CONFINADO, GHE_SETOR_CLIENTE_FUNCAO.ALTURA, ");
                query.Append(" COMENTARIO_FUNCAO.ID_FUNCAO, COMENTARIO_FUNCAO.ATIVIDADE, COMENTARIO_FUNCAO.SITUACAO, GHE_SETOR_CLIENTE_FUNCAO.ELETRICIDADE ");
                query.Append(" FROM SEG_GHE_SETOR_CLIENTE_FUNCAO GHE_SETOR_CLIENTE_FUNCAO ");
                query.Append(" LEFT JOIN SEG_COMENTARIO_FUNCAO COMENTARIO_FUNCAO ON COMENTARIO_FUNCAO.ID_COMENTARIO_FUNCAO = GHE_SETOR_CLIENTE_FUNCAO.ID_COMENTARIO_FUNCAO AND COMENTARIO_FUNCAO.SITUACAO = 'A'");
                query.Append(" WHERE GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR_CLIENTE_FUNCAO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gheSetorClienteFuncao = new GheSetorClienteFuncao(dr.GetInt64(0), dr.IsDBNull(1) ? null : new ClienteFuncao(dr.GetInt64(1)), dr.IsDBNull(2) ? null : new GheSetor(dr.GetInt64(2), null, null, string.Empty), dr.GetBoolean(5), dr.GetBoolean(6), dr.IsDBNull(3) ? null : dr.GetString(3), dr.IsDBNull(4) ? null : new ComentarioFuncao(dr.GetInt64(4), new Funcao(dr.GetInt64(7), string.Empty, string.Empty, string.Empty, string.Empty), dr.GetString(8), dr.GetString(9)), dr.GetBoolean(10));
                        
                }

                return gheSetorClienteFuncao;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<GheSetorClienteFuncao> findAtivosByGheSetorAndClienteFuncao(GheSetor gheSetor, ClienteFuncao clienteFuncao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtivosByGheSetorAndClienteFuncao");

            List<GheSetorClienteFuncao> gheSetorClienteFuncaoList = new List<GheSetorClienteFuncao>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR_CLIENTE_FUNCAO, GHE_SETOR_CLIENTE_FUNCAO.SITUACAO, GHE_SETOR_CLIENTE_FUNCAO.ID_COMENTARIO_FUNCAO, ");
                query.Append(" GHE_SETOR_CLIENTE_FUNCAO.CONFINADO, GHE_SETOR_CLIENTE_FUNCAO.ALTURA, COMENTARIO_FUNCAO.ATIVIDADE, COMENTARIO_FUNCAO.SITUACAO, GHE_SETOR_CLIENTE_FUNCAO.ELETRICIDADE ");
                query.Append(" FROM SEG_GHE_SETOR_CLIENTE_FUNCAO GHE_SETOR_CLIENTE_FUNCAO");
                query.Append(" LEFT JOIN SEG_COMENTARIO_FUNCAO COMENTARIO_FUNCAO ON COMENTARIO_FUNCAO.ID_COMENTARIO_FUNCAO = GHE_SETOR_CLIENTE_FUNCAO.ID_COMENTARIO_FUNCAO AND COMENTARIO_FUNCAO.SITUACAO = 'A' ");
                query.Append(" WHERE GHE_SETOR_CLIENTE_FUNCAO.SITUACAO = 'A' AND ");
                query.Append(" GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR = :VALUE1 AND  ");
                query.Append(" GHE_SETOR_CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = :VALUE2 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheSetor.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = clienteFuncao.Id;


                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gheSetorClienteFuncaoList.Add(new GheSetorClienteFuncao(dr.GetInt64(0), clienteFuncao, gheSetor, dr.GetBoolean(3), dr.GetBoolean(4), dr.GetString(1), dr.IsDBNull(2) ? null : new ComentarioFuncao(dr.GetInt64(2), clienteFuncao.Funcao, dr.IsDBNull(5) ? string.Empty : dr.GetString(5), dr.IsDBNull(6) ? string.Empty : dr.GetString(6)), dr.GetBoolean(7)));
                    
                }

                return gheSetorClienteFuncaoList;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtivosByGheSetorAndClienteFuncao", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public bool isUsedInService(GheSetorClienteFuncao gheSetorClienteFuncao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método isUsedInService");

            bool result = false;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT DISTINCT GHE_SETOR_CLIENTE_FUNCAO.* FROM SEG_ASO ATENDIMENTO ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTE_FUNCAO_FUNCIONARIO ON CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO = ATENDIMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO  ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTE_FUNCAO_FUNCIONARIO.ID_SEG_CLIENTE_FUNCAO ");
                query.Append(" JOIN SEG_GHE_SETOR_CLIENTE_FUNCAO GHE_SETOR_CLIENTE_FUNCAO ON GHE_SETOR_CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO ");
                query.Append(" WHERE GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR_CLIENTE_FUNCAO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheSetorClienteFuncao.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    result = true;
                }

                return result;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - isUsedInService", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public List<GheSetorClienteFuncao> findAllByGheSetorList(GheSetor gheSetor, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByGheSetorList");

            List<GheSetorClienteFuncao> gheSetorClienteFuncaoRetorno = new List<GheSetorClienteFuncao>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR_CLIENTE_FUNCAO, GHE_SETOR_CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO, GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR, ");
                query.Append(" CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO, CLIENTE_FUNCAO.ID_CLIENTE, CLIENTE_FUNCAO.ID_FUNCAO, CLIENTE_FUNCAO.SITUACAO, CLIENTE_FUNCAO.DATA_CADASTRO, CLIENTE_FUNCAO.DATA_DESLIGAMENTO, ");
                query.Append(" FUNCAO.ID_FUNCAO, FUNCAO.DESCRICAO, FUNCAO.COD_CBO, FUNCAO.SITUACAO, FUNCAO.COMENTARIO, GHE_SETOR_CLIENTE_FUNCAO.CONFINADO, GHE_SETOR_CLIENTE_FUNCAO.ALTURA, GHE_SETOR_CLIENTE_FUNCAO.SITUACAO, ");
                query.Append(" COMENTARIO_FUNCAO.ID_COMENTARIO_FUNCAO, COMENTARIO_FUNCAO.ID_FUNCAO, COMENTARIO_FUNCAO.ATIVIDADE, COMENTARIO_FUNCAO.SITUACAO, GHE_SETOR_CLIENTE_FUNCAO.ELETRICIDADE ");
                query.Append(" FROM SEG_GHE_SETOR_CLIENTE_FUNCAO GHE_SETOR_CLIENTE_FUNCAO");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = GHE_SETOR_CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO");
                query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTE_FUNCAO.ID_FUNCAO");
                query.Append(" LEFT JOIN SEG_COMENTARIO_FUNCAO COMENTARIO_FUNCAO ON COMENTARIO_FUNCAO.ID_COMENTARIO_FUNCAO = GHE_SETOR_CLIENTE_FUNCAO.ID_COMENTARIO_FUNCAO AND COMENTARIO_FUNCAO.SITUACAO = 'A' ");
                query.Append(" WHERE GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheSetor.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gheSetorClienteFuncaoRetorno.Add(new GheSetorClienteFuncao(dr.GetInt64(0), new ClienteFuncao(dr.GetInt64(1), new Cliente(dr.GetInt64(4)), new Funcao(dr.GetInt64(9), dr.GetString(10), dr.GetString(11), dr.GetString(12), dr.GetString(13)), dr.IsDBNull(6) ? string.Empty : dr.GetString(6), dr.IsDBNull(7) ? (DateTime?)null : dr.GetDateTime(7), dr.IsDBNull(8) ? (DateTime?)null : dr.GetDateTime(8)), gheSetor, dr.GetBoolean(14), dr.GetBoolean(15), dr.IsDBNull(16) ? null : dr.GetString(16), dr.IsDBNull(17) ? null : new ComentarioFuncao(dr.GetInt64(17), dr.IsDBNull(18) ? null : new Funcao(dr.GetInt64(18), string.Empty, string.Empty, string.Empty, string.Empty), dr.IsDBNull(19) ? string.Empty : dr.GetString(19), dr.IsDBNull(20) ? string.Empty : dr.GetString(20)), dr.GetBoolean(21)));
                }
                return gheSetorClienteFuncaoRetorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByGheSetorList", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
