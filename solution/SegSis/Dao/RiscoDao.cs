﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using System.Data;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;

namespace SWS.Dao
{
    class RiscoDao : IRiscoDao
    {
        public DataSet findRisco(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findRisco");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" select id_risco, descricao from seg_risco order by id_risco");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);
                
                DataSet ds = new DataSet();
                adapter.Fill(ds, "Riscos");
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findRisco", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
