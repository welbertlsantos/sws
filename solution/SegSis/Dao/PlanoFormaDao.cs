﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;
using SWS.Entidade;
using NpgsqlTypes;
using SWS.View.Resources;

namespace SWS.Dao
{
    class PlanoFormaDao : IPlanoFormaDao
    {
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_PLANO_FORMA_ID_PLANO_FORMA_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public PlanoForma insert(PlanoForma planoForma, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertPlanoForma");

            try
            {
                planoForma.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_PLANO_FORMA( ID_PLANO_FORMA, ID_FORMA, ID_PLANO, SITUACAO) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = planoForma.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = planoForma.Forma.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = planoForma.Plano.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = planoForma.Situacao;

                command.ExecuteNonQuery();

                return planoForma;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertPlanoForma", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }


        public PlanoForma update(PlanoForma planoForma, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updatePlanoForma");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_PLANO_FORMA SET ");
                query.Append(" SITUACAO = :VALUE2 ");
                query.Append(" WHERE ID_PLANO_FORMA = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = planoForma.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = planoForma.Situacao;

                command.ExecuteNonQuery();

                return planoForma;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updatePlanoForma", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public PlanoForma findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findPlanoFormaById");

            PlanoForma planoForma = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT PLANO_FORMA.ID_PLANO_FORMA, PLANO_FORMA.ID_FORMA, FORMA.DESCRICAO, PLANO.ID_PLANO, PLANO.DESCRICAO,  ");
                query.Append(" PLANO.SITUACAO, PLANO.CARGA, PLANO_FORMA.SITUACAO ");
                query.Append(" FROM SEG_PLANO PLANO ");
                query.Append(" JOIN SEG_PLANO_FORMA PLANO_FORMA ON PLANO_FORMA.ID_PLANO = PLANO.ID_PLANO" );
                query.Append(" JOIN SEG_FORMA FORMA ON FORMA.ID_FORMA = PLANO_FORMA.ID_FORMA ");
                query.Append(" WHERE PLANO_FORMA.ID_PLANO_FORMA = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    planoForma = new PlanoForma(dr.GetInt64(0), new Forma(dr.GetInt64(1), dr.GetString(2)), new Plano(dr.GetInt64(3), dr.GetString(4), dr.GetString(5), dr.GetBoolean(6)), dr.GetString(7));
                }

                return planoForma;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findPlanoById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<PlanoForma> findAtivosByPlano(Plano plano, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtivosByPlano");

            List<PlanoForma> formas = new List<PlanoForma>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT PLANO_FORMA.ID_PLANO_FORMA, PLANO_FORMA.ID_FORMA, FORMA.DESCRICAO AS FORMA, PLANO_FORMA.SITUACAO ");
                query.Append(" FROM SEG_PLANO_FORMA PLANO_FORMA ");
                query.Append(" JOIN SEG_FORMA FORMA ON FORMA.ID_FORMA = PLANO_FORMA.ID_FORMA ");
                query.Append(" WHERE PLANO_FORMA.ID_PLANO = :VALUE1 AND PLANO_FORMA.SITUACAO = 'A' ");
                query.Append(" ORDER BY FORMA ASC ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = plano.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    formas.Add(new PlanoForma(dr.GetInt64(0), new Forma(dr.GetInt64(1), dr.GetString(2)), plano, dr.GetString(3)));
                }

                return formas;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtivosByPlano", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public PlanoForma findByFormaByPlano(Forma forma, Plano plano, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findPlanoFormaById");

            PlanoForma planoForma = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT PLANO_FORMA.ID_PLANO_FORMA, PLANO_FORMA.ID_FORMA, FORMA.DESCRICAO, PLANO.ID_PLANO, PLANO.DESCRICAO,  ");
                query.Append(" PLANO.SITUACAO, PLANO.CARGA, PLANO_FORMA.SITUACAO ");
                query.Append(" FROM SEG_PLANO PLANO ");
                query.Append(" JOIN SEG_PLANO_FORMA PLANO_FORMA ON PLANO_FORMA.ID_PLANO = PLANO.ID_PLANO");
                query.Append(" JOIN SEG_FORMA FORMA ON FORMA.ID_FORMA = PLANO_FORMA.ID_FORMA ");
                query.Append(" WHERE PLANO_FORMA.ID_FORMA = :VALUE1 AND PLANO_FORMA.ID_PLANO = :VALUE2 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = forma.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = plano.Id;


                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    planoForma = new PlanoForma(dr.GetInt64(0), new Forma(dr.GetInt64(1), dr.GetString(2)), new Plano(dr.GetInt64(3), dr.GetString(4), dr.GetString(5), dr.GetBoolean(6)), dr.GetString(7));
                }

                return planoForma;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findPlanoById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }





    }
}
