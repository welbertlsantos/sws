﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using SWS.IDao;
using SWS.Excecao;
using Npgsql;
using NpgsqlTypes;
using System.Data;
using SWS.Helper;
using SWS.View.Resources;

namespace SWS.Dao
{
    class HospitalEstudoDao :IHospitalEstudoDao
    {
        public void incluirHospitalEstudo(HospitalEstudo hospitalEstudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirHospitalEstudo");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" INSERT INTO SEG_ESTUDO_HOSPITAL ( ID_ESTUDO, ID_HOSPITAL ) VALUES ( :VALUE1, :VALUE2 ) ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = hospitalEstudo.Pcmso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = hospitalEstudo.Hospital.Id;
                

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirHospitalEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }


        public DataSet findAllHospitalEstudo(Estudo pcmso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllHospitalEstudo");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT H.ID_HOSPITAL, H.DESCRICAO, H.ENDERECO, H.NUMERO, H.COMPLEMENTO, H.BAIRRO, ");
                query.Append(" H.CIDADE, H.CEP, H.UF, H.TELEFONE1, H.TELEFONE2, H.OBSERVACAO, ");
                query.Append(" H.SITUACAO FROM SEG_HOSPITAL H, SEG_ESTUDO_HOSPITAL EH WHERE H.ID_HOSPITAL = EH.ID_HOSPITAL AND ");
                query.Append(" EH.ID_ESTUDO = :VALUE1 ORDER BY H.DESCRICAO ASC ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));


                adapter.SelectCommand.Parameters[0].Value = pcmso.Id;
                
                DataSet ds = new DataSet();

                adapter.Fill(ds, "Hospitais");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllHospitalEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void excluirHospitalEstudo(HospitalEstudo hospitalEstudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método excluirHospitalEstudo");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("DELETE FROM SEG_ESTUDO_HOSPITAL WHERE ID_HOSPITAL = :VALUE1 AND ID_ESTUDO = :VALUE2", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = hospitalEstudo.Hospital.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = hospitalEstudo.Pcmso.Id;
                
                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - excluirHospitalEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean findHospitalEstudoByHospital(Hospital hospital, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findHospitalEstudoByHospital");

            Boolean isHospitalEstudoInUsed = false;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT COUNT(*) FROM SEG_ESTUDO_HOSPITAL EH, SEG_ESTUDO E ");
                query.Append(" WHERE EH.ID_ESTUDO = E.ID_ESTUDO AND EH.ID_HOSPITAL = :VALUE1 AND E.SITUACAO = :VALUE2 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = hospital.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = ApplicationConstants.FECHADO;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    if (dr.GetInt64(0) > 0)
                    {
                        isHospitalEstudoInUsed = true;
                    }
                }

                return isHospitalEstudoInUsed;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findHospitalEstudoByHospital", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

    }
}
