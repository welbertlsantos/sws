﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using Npgsql;
using SWS.Helper;
using NpgsqlTypes;
using SWS.Excecao;

namespace SWS.Dao
{
    class LogDao : ILogDao
    {

        public void insertLog(Log log, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertLog");

            try
            {
                log.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_LOG ( ID_LOG, TABELA, ID_TABELA, ");
                query.Append(" DATA_ALTERACAO, ATRIBUTO, VALOR_ANTERIOR, VALOR_ALTERADO, USUARIO) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6, :VALUE7, :VALUE8) ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = log.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = log.Tabela;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = log.IdTabela;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Timestamp));
                command.Parameters[3].Value = log.DataAlteracao;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = log.Atributo;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = log.ValorAnterior;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = log.ValorAlterado;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Varchar));
                command.Parameters[7].Value = log.Usuario;

                command.ExecuteNonQuery();


            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertLog", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_LOG_ID_LOG_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
