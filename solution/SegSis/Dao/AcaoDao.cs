﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using System.Data;
using Npgsql;
using SWS.Excecao;
using NpgsqlTypes;
using SWS.Entidade;
using SWS.Helper;

namespace SWS.Dao
{
    class AcaoDao : IAcaoDao
    {
        public DataSet listaTodos(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método listaTodos");

            try
            {
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter("select id_acao, descricao from seg_acao", dbConnection);
                DataSet ds = new DataSet();

                adapter.Fill(ds, "Acoes");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - listaTodos", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet listaTodosByDominio(Int64 idDominio, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método listaTodosByDominio");

            try
            {
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter("select a.id_acao, a.descricao from seg_acao a, seg_dominio_acao da where da.id_acao = a.id_acao and da.id_dominio = :value1 order by a.descricao", dbConnection);
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                adapter.SelectCommand.Parameters[0].Value = idDominio;
                
                DataSet ds = new DataSet();

                adapter.Fill(ds, "Acoes");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - listaTodosByDominio", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean verificaAcaoSelecionadaParaPerfilDominio(Int64 idPerfil, Int64 idDominio, Int64 idAcao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaAcaoSelecionadaParaPerfilDominio");

            Boolean acaoSelecionada = false;

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("select id_perfil, id_dominio, id_acao from seg_perfil_dominio_acao where id_perfil = :value1 and id_dominio = :value2 and id_acao = :value3", dbConnection);

                // Now add the parameter to the parameter collection of the command specifying its type.
                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value2", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value3", NpgsqlDbType.Integer));

                // Now, add Nome value to it and later execute the command as usual.
                command.Parameters[0].Value = idPerfil;
                command.Parameters[1].Value = idDominio;
                command.Parameters[2].Value = idAcao;

                NpgsqlDataReader dr = command.ExecuteReader();

                if (dr.HasRows)
                {
                    acaoSelecionada = true;
                }
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaAcaoSelecionadaParaPerfilDominio", ex);
                throw new DataBaseConectionException(ex.Message);
            }

            return acaoSelecionada;
        }

        public void removeAcaoDominioFromPerfil(Int64 idPerfil, Int64 idDominio, Int64 idAcao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método removeAcaoDominioFromPerfil");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("delete from seg_perfil_dominio_acao where id_perfil = :value1 and id_dominio = :value2 and id_acao = :value3", dbConnection);
                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value2", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value3", NpgsqlDbType.Integer));

                command.Parameters[0].Value = idPerfil;
                command.Parameters[1].Value = idDominio;
                command.Parameters[2].Value = idAcao;

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - removeAcaoDominioFromPerfil", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void criaAcaoDominioFromPerfil(Int64 idPerfil, Int64 idDominio, Int64 idAcao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método criaAcaoDominioFromPerfil");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("insert into seg_perfil_dominio_acao (id_perfil, id_dominio, id_acao) values(:value1, :value2, :value3)", dbConnection);
                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value2", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value3", NpgsqlDbType.Integer));

                command.Parameters[0].Value = idPerfil;
                command.Parameters[1].Value = idDominio;
                command.Parameters[2].Value = idAcao;

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - criaAcaoDominioFromPerfil", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public HashSet<Acao> findByPerfilAndDominio(Int64? idPerfil, Int64 idDominio, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByPerfilAndDominio");

            HashSet<Acao> acoes = new HashSet<Acao>();
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("select a.id_acao, a.descricao, a.situacao from seg_acao a, seg_perfil_dominio_acao pda where a.id_acao = pda.id_acao and pda.id_perfil = :value1 and pda.id_dominio = :value2 and a.situacao = 'A'", dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value2", NpgsqlDbType.Integer));

                command.Parameters[0].Value = idPerfil;
                command.Parameters[1].Value = idDominio;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    acoes.Add(new Acao(dr.GetInt64(0), dr.GetString(1), dr.GetString(2)));
                }

                return acoes;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByPerfilAndDominio", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}