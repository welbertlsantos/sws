﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using Npgsql;
using NpgsqlTypes;
using SWS.Excecao;
using SWS.View.Resources;
using System.Data;
using SWS.Helper;

namespace SWS.Dao
{
    class ExameDao : IExameDao
    {
        public Exame insert(Exame exame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            StringBuilder query = new StringBuilder();

            try
            {
                exame.Id= recuperaProximoId(dbConnection);

                query.Append(" INSERT INTO SEG_EXAME (ID_EXAME, DESCRICAO, SITUACAO, LABORATORIO, PRECO, PRIORIDADE, CUSTO, EXTERNO, LIBERA_DOCUMENTO, CODIGO_TUSS, EXAME_COMPLEMENTAR, PERIODO_VENCIMENTO, PADRAO_CONTRATO )");
                query.Append(" VALUES (:VALUE1, :VALUE2, 'A', :VALUE3, :VALUE4, :VALUE5, :VALUE6, :VALUE7, :VALUE8, :VALUE9, :VALUE10, :VALUE11, :VALUE12) ");


                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = exame.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = exame.Descricao.ToUpper();

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Boolean));
                command.Parameters[2].Value = exame.Laboratorio;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Numeric));
                command.Parameters[3].Value = exame.Preco;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Integer));
                command.Parameters[4].Value = exame.Prioridade;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Numeric));
                command.Parameters[5].Value = exame.Custo;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Boolean));
                command.Parameters[6].Value = exame.Externo;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Boolean));
                command.Parameters[7].Value = exame.LiberaDocumento;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                command.Parameters[8].Value = exame.CodigoTuss;

                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Boolean));
                command.Parameters[9].Value = exame.ExameComplementar;

                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Integer));
                command.Parameters[10].Value = exame.PeriodoVencimento;

                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Boolean));
                command.Parameters[11].Value = exame.PadraoContrato;

                command.ExecuteNonQuery();

                return exame;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Exame update(Exame exame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            StringBuilder query = new StringBuilder();

            try
            {
                
                query.Append(" UPDATE SEG_EXAME SET DESCRICAO = :VALUE2, LABORATORIO = :VALUE3, SITUACAO = :VALUE4, ");
                query.Append(" PRECO = :VALUE5, PRIORIDADE = :VALUE6, CUSTO = :VALUE7, EXTERNO = :VALUE8, LIBERA_DOCUMENTO = :VALUE9, CODIGO_TUSS = :VALUE10, EXAME_COMPLEMENTAR = :VALUE11, PERIODO_VENCIMENTO = :VALUE12, PADRAO_CONTRATO = :VALUE13 WHERE ID_EXAME = :VALUE1 ");
                                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = exame.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = exame.Descricao.ToUpper();

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Boolean));
                command.Parameters[2].Value = exame.Laboratorio;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Char));
                command.Parameters[3].Value = exame.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Numeric));
                command.Parameters[4].Value = exame.Preco;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Integer));
                command.Parameters[5].Value = exame.Prioridade;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Numeric));
                command.Parameters[6].Value = exame.Custo;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Boolean));
                command.Parameters[7].Value = exame.Externo;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Boolean));
                command.Parameters[8].Value = exame.LiberaDocumento;

                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                command.Parameters[9].Value = exame.CodigoTuss;

                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Boolean));
                command.Parameters[10].Value = exame.ExameComplementar;

                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Integer));
                command.Parameters[11].Value = exame.PeriodoVencimento;

                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Boolean));
                command.Parameters[12].Value = exame.PadraoContrato;

                command.ExecuteNonQuery();

                return exame;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_EXAME_ID_EXAME_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Exame findByDescricao(String str, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByDescricao");

            Exame exameRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_EXAME, DESCRICAO, SITUACAO, LABORATORIO, PRECO, PRIORIDADE, CUSTO, EXTERNO, LIBERA_DOCUMENTO, CODIGO_TUSS, EXAME_COMPLEMENTAR, PERIODO_VENCIMENTO, PADRAO_CONTRATO ");
                query.Append(" FROM SEG_EXAME WHERE DESCRICAO = :VALUE1 AND SITUACAO = 'A' ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = str.ToUpper();

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    exameRetorno = new Exame(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetBoolean(3), dr.GetDecimal(4),dr.GetInt32(5), dr.GetDecimal(6), dr.GetBoolean(7), dr.GetBoolean(8), dr.IsDBNull(9) ? string.Empty : dr.GetString(9), dr.GetBoolean(10), dr.IsDBNull(11) ? (int?)null : dr.GetInt32(11), dr.GetBoolean(12));
                }

                return exameRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByDescricao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Exame findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            Exame exameRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {

                query.Append(" SELECT ID_EXAME, DESCRICAO, SITUACAO, LABORATORIO, PRECO, PRIORIDADE, CUSTO, EXTERNO, LIBERA_DOCUMENTO, CODIGO_TUSS, EXAME_COMPLEMENTAR, PERIODO_VENCIMENTO, PADRAO_CONTRATO ");
                query.Append(" FROM SEG_EXAME WHERE ID_EXAME = :VALUE1 ");
                                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    exameRetorno = new Exame(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetBoolean(3), dr.GetDecimal(4), dr.GetInt32(5), dr.GetDecimal(6), dr.GetBoolean(7), dr.GetBoolean(8), dr.IsDBNull(9) ? string.Empty : dr.GetString(9), dr.GetBoolean(10), dr.IsDBNull(11) ? (int?)null : dr.GetInt32(11), dr.GetBoolean(12));
                }
                
                return exameRetorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findByFilter(Exame exame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findExameByFilter");

            try
            {
                StringBuilder query = new StringBuilder();
                
                if (exame.isNullForFilter())
                {
                    query.Append(" SELECT ID_EXAME, DESCRICAO, SITUACAO, (CASE WHEN LABORATORIO = TRUE THEN 'SIM' ELSE '' END) AS LABORATORIO , PRECO, PRIORIDADE, CUSTO, (CASE WHEN EXTERNO = TRUE THEN 'SIM' ELSE '' END) AS EXTERNO, LIBERA_DOCUMENTO ");
                    query.Append(" FROM SEG_EXAME ORDER BY DESCRICAO ASC ");
                    
                }
                else
                {
                    query.Append(" SELECT ID_EXAME, DESCRICAO, SITUACAO, (CASE WHEN LABORATORIO = TRUE THEN 'SIM' ELSE '' END) AS LABORATORIO, PRECO, PRIORIDADE, CUSTO, (CASE WHEN EXTERNO = TRUE THEN 'SIM' ELSE '' END) AS EXTERNO, LIBERA_DOCUMENTO ");
                    query.Append(" FROM SEG_EXAME WHERE TRUE ");

                    if (!String.IsNullOrWhiteSpace(exame.Descricao))
                        query.Append(" AND DESCRICAO LIKE :VALUE1 ");

                    if (!String.IsNullOrWhiteSpace(exame.Situacao))
                        query.Append(" AND SITUACAO = :VALUE2 ");

                    if (exame.Laboratorio != null)
                        query.Append(" AND LABORATORIO = :VALUE3 ");

                    query.Append(" ORDER BY DESCRICAO ASC ");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[0].Value = exame.Descricao + "%";
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[1].Value = exame.Situacao;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Boolean));
                adapter.SelectCommand.Parameters[2].Value = exame.Laboratorio;

                DataSet ds = new DataSet();
                
                adapter.Fill(ds, "Exames");
                
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findExameByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean findInGheFonteAgenteExame(Exame exame, NpgsqlConnection dbConnection)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findExameInGheFonteAgenteExame");

            StringBuilder query = new StringBuilder();
            
            try
            {
                Boolean retorno = false;

                query.Append(" SELECT ID_GHE_FONTE_AGENTE_EXAME, ID_EXAME, ID_GHE_FONTE_AGENTE, IDADE_EXAME ");
                query.Append(" FROM SEG_GHE_FONTE_AGENTE_EXAME WHERE ID_EXAME = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = exame.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    retorno = true;
                }

                return retorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findExameByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public Boolean findInExameAso(Exame exame, NpgsqlConnection dbConnection)
        {

            // reaproveitando método para compatibilidade no novo modelo.
            // wellbert santos - 2013/01/31.

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findExameInExameAso");

            StringBuilder query = new StringBuilder();

            try
            {
                Boolean retorno = Convert.ToBoolean(false);

                query.Append(" SELECT ID_CLIENTE_FUNCAO_EXAME, ID_CLIENTE_FUNCAO, ID_EXAME, SITUACAO, IDADE_EXAME ");
                query.Append(" FROM SEG_CLIENTE_FUNCAO_EXAME WHERE ID_EXAME = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = exame.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    retorno = Convert.ToBoolean(true);
                }

                return retorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findExameInExameAso", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public void delete(Exame exame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" DELETE FROM SEG_EXAME WHERE ID_EXAME = :VALUE1", dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = exame.Id;
                
                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAtivoNotInClienteFuncao(Exame exame, ClienteFuncao clienteFuncao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllExameAtivoNotInClienteFuncao");

            try
            {
                StringBuilder query = new StringBuilder();

                if (String.IsNullOrEmpty(exame.Descricao))
                {
                    query.Append(" SELECT ID_EXAME, DESCRICAO, SITUACAO, LABORATORIO, PRECO, PRIORIDADE, CUSTO, EXTERNO, LIBERA_DOCUMENTO ");
                    query.Append(" FROM SEG_EXAME WHERE SITUACAO = 'A' AND ");
                    query.Append(" ID_EXAME NOT IN (SELECT ID_EXAME FROM SEG_CLIENTE_FUNCAO_EXAME WHERE ID_CLIENTE_FUNCAO = :VALUE1 AND SITUACAO = 'A') ");
                    query.Append(" ORDER BY DESCRICAO ASC ");
                }
                else
                {
                    query.Append(" SELECT ID_EXAME, DESCRICAO, SITUACAO, LABORATORIO, PRECO, PRIORIDADE, CUSTO, EXTERNO, LIBERA_DOCUMENTO ");
                    query.Append(" FROM SEG_EXAME WHERE SITUACAO = 'A' AND   ");
                    query.Append(" ID_EXAME NOT IN (SELECT ID_EXAME FROM SEG_CLIENTE_FUNCAO_EXAME WHERE ID_CLIENTE_FUNCAO = :VALUE1 AND SITUACAO = 'A') ");
                    query.Append(" AND DESCRICAO LIKE :VALUE2 ORDER BY DESCRICAO ASC ");
                }
                
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = clienteFuncao.Id;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = exame.Descricao.ToUpper() + "%";
                
                DataSet ds = new DataSet();

                adapter.Fill(ds, "Exames");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllExameAtivoNotInClienteFuncao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAtivoInClienteFuncao(ClienteFuncao clienteFuncao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllExameAtivoInClienteFuncao");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT EXAME.ID_EXAME, EXAME.DESCRICAO, EXAME.SITUACAO, EXAME.LABORATORIO, EXAME.PRECO, EXAME.PRIORIDADE, EXAME.CUSTO, EXAME.EXTERNO, "); 
                query.Append(" CLIENTE_FUNCAO_EXAME.IDADE_EXAME, EXAME.LIBERA_DOCUMENTO, EXAME.PERIODO_VENCIMENTO, EXAME.PADRAO_CONTRATO ");
                query.Append(" FROM SEG_EXAME EXAME ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME CLIENTE_FUNCAO_EXAME ON CLIENTE_FUNCAO_EXAME.ID_EXAME = EXAME.ID_EXAME ");
                query.Append(" WHERE CLIENTE_FUNCAO_EXAME.SITUACAO = 'A' AND ");
                query.Append(" CLIENTE_FUNCAO_EXAME.ID_CLIENTE_FUNCAO = :VALUE1 ");
                query.Append(" ORDER BY EXAME.DESCRICAO ASC ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = clienteFuncao.Id;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Exames");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllExameAtivoInClienteFuncao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int32 buscaQuantidadeExamesByGhe(Ghe ghe, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método buscaQuantidadeExamesByGhe");

            Int32 quantidadeRisco = 0;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT  DISTINCT E.DESCRICAO AS EXAME,");
                query.Append(" BUSCA_PERIODICIDADE(16,E.ID_EXAME)");
                query.Append(" FROM SEG_GHE G");
                query.Append(" JOIN SEG_GHE_FONTE GF ON G.ID_GHE = GF.ID_GHE AND GF.PRINCIPAL <> 't'");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE GFA ON GFA.ID_GHE_FONTE = GF.ID_GHE_FONTE");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GFAEX ON GFAEX.ID_GHE_FONTE_AGENTE = GFA.ID_GHE_FONTE_AGENTE");
                query.Append(" JOIN SEG_EXAME E ON E.ID_EXAME = GFAEX.ID_EXAME");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME_PERIODICIDADE GFAEP ON GFAEP.ID_GHE_FONTE_AGENTE_EXAME = GFAEX.ID_GHE_FONTE_AGENTE_EXAME");
                query.Append(" JOIN SEG_PERIODICIDADE P ON P.ID_PERIODICIDADE = GFAEP.ID_PERIODICIDADE");
                query.Append(" WHERE G.ID_GHE = :value1 ORDER BY E.DESCRICAO ASC");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = ghe.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    quantidadeRisco++;
                }

                return quantidadeRisco;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - buscaQuantidadeExamesByGhe", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAtivoNotInContrato(Contrato contrato, Exame exame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllExameAtivoNotInContrato");

            try
            {
                StringBuilder query = new StringBuilder();

                if (String.IsNullOrEmpty(exame.Descricao))
                {
                    query.Append(" SELECT ID_EXAME, DESCRICAO, SITUACAO, LABORATORIO, PRECO, PRIORIDADE, CUSTO, EXTERNO, FALSE, LIBERA_DOCUMENTO "); 
                    query.Append(" FROM SEG_EXAME WHERE SITUACAO = 'A' AND ");
                    query.Append(" ID_EXAME NOT IN (SELECT ID_EXAME FROM SEG_CONTRATO_EXAME WHERE ID_CONTRATO = :VALUE1 AND SITUACAO = 'A' ) ");
                    query.Append(" ORDER BY DESCRICAO ASC ");

                }
                else
                {
                    query.Append(" SELECT ID_EXAME, DESCRICAO, SITUACAO, LABORATORIO, PRECO, PRIORIDADE, CUSTO, EXTERNO, FALSE, LIBERA_DOCUMENTO "); 
                    query.Append(" FROM SEG_EXAME WHERE SITUACAO = 'A' AND ");
                    query.Append(" DESCRICAO LIKE :VALUE2 AND ");
                    query.Append(" ID_EXAME NOT IN (SELECT ID_EXAME FROM SEG_CONTRATO_EXAME WHERE ID_CONTRATO = :VALUE1 AND SITUACAO = 'A' ) ");
                    query.Append(" ORDER BY DESCRICAO ASC ");
                }
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = contrato.Id;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = exame.Descricao.ToUpper() + "%";

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Exames");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllExameAtivoNotInContrato", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAtivo(Exame exame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtivo");

            try
            {
                StringBuilder query = new StringBuilder();

                if (String.IsNullOrEmpty(exame.Descricao))
                {
                    query.Append(" SELECT ID_EXAME, DESCRICAO, SITUACAO, LABORATORIO, PRECO, PRIORIDADE, CUSTO, EXTERNO, FALSE, LIBERA_DOCUMENTO ");
                    query.Append(" FROM SEG_EXAME WHERE SITUACAO = 'A' ");
                    query.Append(" ORDER BY DESCRICAO ASC ");
                }
                else
                {
                    query.Append(" SELECT ID_EXAME, DESCRICAO, SITUACAO, LABORATORIO, PRECO, PRIORIDADE, CUSTO, EXTERNO, FALSE, LIBERA_DOCUMENTO ");
                    query.Append(" FROM SEG_EXAME WHERE SITUACAO = 'A' ");
                    query.Append(" AND DESCRICAO LIKE :VALUE1 ");
                    query.Append(" ORDER BY DESCRICAO ASC ");
                }
                
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = exame.Descricao.ToUpper() + "%";
                
                DataSet ds = new DataSet();

                adapter.Fill(ds, "Exames");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtivo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public LinkedList<Exame> findAtivoNotInAso(HashSet<Int64> idExames, Exame exame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtivoNotInAso");

            StringBuilder query = new StringBuilder();

            LinkedList<Exame> ExameRetornoSet = new LinkedList<Exame>();
            Int32 nrepet = 0;

            try
            {
                query.Append(" SELECT ID_EXAME, DESCRICAO, SITUACAO, LABORATORIO, PRECO, PRIORIDADE, CUSTO, EXTERNO, LIBERA_DOCUMENTO, CODIGO_TUSS, EXAME_COMPLEMENTAR, PERIODO_VENCIMENTO, PADRAO_CONTRATO ");
                query.Append(" FROM SEG_EXAME WHERE SITUACAO = 'A' AND ");
                query.Append(" ID_EXAME NOT IN (  ");

                foreach (Int64 id in idExames)
                {
                    query.Append(id);

                    if (nrepet < idExames.Count - 1)
                        query.Append(",");
                    
                    if (nrepet == idExames.Count - 1 )
                        query.Append(")");

                    nrepet++;
                }

                if (!String.IsNullOrEmpty(exame.Descricao))
                    query.Append(" AND DESCRICAO LIKE :VALUE1 ");

                query.Append(" ORDER BY DESCRICAO ASC ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = exame.Descricao.ToUpper() + "%";
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    ExameRetornoSet.AddLast(new Exame(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetBoolean(3), dr.GetDecimal(4), dr.GetInt32(5), dr.GetDecimal(6), dr.GetBoolean(7), dr.GetBoolean(8), dr.IsDBNull(9) ? string.Empty : dr.GetString(9), dr.GetBoolean(10), dr.IsDBNull(11) ? (int?)null : dr.GetInt32(11), dr.GetBoolean(12)));
                }

                return ExameRetornoSet;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtivoNotInAso", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAtivoNotInPropostaContrato(List<Exame> exames, Exame exame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtivoNotInPropostaContrato");

            try
            {
                StringBuilder query = new StringBuilder();
                Int32 count = 0;

                if (String.IsNullOrEmpty(exame.Descricao))
                {

                    query.Append(" SELECT ID_EXAME, DESCRICAO, SITUACAO, LABORATORIO, PRECO, PRIORIDADE, CUSTO, EXTERNO, FALSE, LIBERA_DOCUMENTO ");
                    query.Append(" FROM SEG_EXAME WHERE SITUACAO = 'A' ");

                    if (exames.Count > 0)
                    {
                        query.Append(" AND ID_EXAME NOT IN ( ");

                        foreach (Exame ex in exames)
                        {
                            query.Append(ex.Id);

                            if (count < exames.Count - 1)
                                query.Append(",");

                            if (count == exames.Count - 1)
                                query.Append(")");
                            count++;
                        }
                        
                    }

                }
                else
                {
                    query.Append(" SELECT ID_EXAME, DESCRICAO, SITUACAO, LABORATORIO, PRECO, PRIORIDADE, CUSTO, EXTERNO, FALSE, LIBERA_DOCUMENTO ");
                    query.Append(" FROM SEG_EXAME WHERE SITUACAO = 'A' AND ");
                    query.Append(" DESCRICAO LIKE :VALUE1 ");

                    if (exames.Count > 0)
                    {
                        query.Append(" AND ID_EXAME NOT IN ( ");

                        foreach (Exame ex in exames)
                        {

                            query.Append(ex.Id);

                            if (count < exames.Count - 1)
                                query.Append(",");

                            if (count == exames.Count - 1)
                                query.Append(")");
                        
                            count++;
                        }
                    }

                }
                
                query.Append(" ORDER BY DESCRICAO ASC ");
                
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = exame.Descricao + "%";

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Exames");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtivoNotInPropostaContrato", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<Exame> findAllAtivosPadraoContrato(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllExamePadraoContrato");

            try
            {
                List<Exame> exames = new List<Exame>();
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID_EXAME, DESCRICAO, SITUACAO, LABORATORIO, PRECO, PRIORIDADE, CUSTO, EXTERNO, LIBERA_DOCUMENTO, CODIGO_TUSS, EXAME_COMPLEMENTAR, PERIODO_VENCIMENTO, PADRAO_CONTRATO");
                query.Append(" FROM SEG_EXAME WHERE PADRAO_CONTRATO = TRUE AND SITUACAO = 'A'");
                query.Append(" ORDER BY DESCRICAO ASC ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    exames.Add(new Exame(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetBoolean(3), dr.GetDecimal(4), dr.GetInt32(5), dr.GetDecimal(6), dr.GetBoolean(7), dr.GetBoolean(8), dr.IsDBNull(9) ? string.Empty : dr.GetString(9), dr.GetBoolean(10), dr.IsDBNull(11) ? (int?)null : dr.GetInt32(11), dr.GetBoolean(12)));
                }

                return exames;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllExamePadraoContrato", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
