﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using System.Data;
using Npgsql;
using SWS.Excecao;
using SWS.Entidade;
using NpgsqlTypes;
using SWS.View.Entidade;
using SWS.View.Resources;
using SWS.Helper;


namespace SWS.Dao
{
    class FuncaoInternaDao : IFuncaoInternaDao
    {
        public FuncaoInterna findById(Int64 idFuncaoInterna,  NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByCronograma");
            
            FuncaoInterna funcaoInterna = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_FUNCAO_INTERNA, DESCRICAO, SITUACAO, PRIVADO ");
                query.Append(" FROM SEG_FUNCAO_INTERNA WHERE ID_FUNCAO_INTERNA = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = idFuncaoInterna;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    funcaoInterna = new FuncaoInterna(dr.GetInt64(0), dr.GetString(1), dr.GetString(2),dr.GetString(3));
                }

                return funcaoInterna;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByCronograma", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findByFilter(FuncaoInterna funcaoInterna, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFuncaoInternaByFilter");

            try
            {
                StringBuilder query = new StringBuilder();

                if (funcaoInterna.isNullForFilter())
                    query.Append(" SELECT ID_FUNCAO_INTERNA, DESCRICAO, SITUACAO, PRIVADO FROM SEG_FUNCAO_INTERNA ");
                else
                {
                    query.Append(" SELECT ID_FUNCAO_INTERNA, DESCRICAO, SITUACAO, PRIVADO FROM SEG_FUNCAO_INTERNA WHERE TRUE ");

                    if (!String.IsNullOrWhiteSpace(funcaoInterna.Descricao))
                        query.Append(" AND DESCRICAO LIKE :VALUE1 ");

                    if (!String.IsNullOrWhiteSpace(funcaoInterna.Situacao))
                        query.Append(" AND SITUACAO = :VALUE2 ");
                }
                
                query.Append("ORDER BY DESCRICAO ASC");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[0].Value = funcaoInterna.Descricao + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[1].Value = funcaoInterna.Situacao;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "FuncoesInternas");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFuncaoInternaByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public FuncaoInterna insert(FuncaoInterna funcaoInterna, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirFuncaoInterna");

            StringBuilder query = new StringBuilder();

            try
            {
                funcaoInterna.Id = recuperaProximoId(dbConnection);

                query.Append(" INSERT INTO SEG_FUNCAO_INTERNA (ID_FUNCAO_INTERNA, DESCRICAO, SITUACAO, PRIVADO ) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, 'A', '1')");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = funcaoInterna.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = funcaoInterna.Descricao;

                command.ExecuteNonQuery();

                return funcaoInterna;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirFuncaoInterna", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_FUNCAO_INTERNA_ID_FUNCAO_INTERNA_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();


                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoIdFuncaoInterna", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public FuncaoInterna findByDescricao(String descricao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFuncaoInternaByDescricao");

            FuncaoInterna funcaoInternaRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {

                query.Append(" SELECT ID_FUNCAO_INTERNA, DESCRICAO, SITUACAO, PRIVADO FROM SEG_FUNCAO_INTERNA ");
                query.Append(" WHERE DESCRICAO = :VALUE1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = descricao;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    funcaoInternaRetorno = new FuncaoInterna(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3));
                }

                return funcaoInternaRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFuncaoInternaByDescricao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public FuncaoInterna update(FuncaoInterna funcaoInterna, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateFuncaoInterna");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_FUNCAO_INTERNA SET DESCRICAO = :VALUE2, SITUACAO = :VALUE3 ");
                query.Append(" WHERE ID_FUNCAO_INTERNA = :VALUE1");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = funcaoInterna.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = funcaoInterna.Descricao;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = funcaoInterna.Situacao;

                command.ExecuteNonQuery();

                return funcaoInterna;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateFuncaoInterna", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public FuncaoInterna findFuncaoInternaAtivoByUsuario(Usuario usuario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFuncaoInternaAtivoByUsuario");

            try
            {
                StringBuilder query = new StringBuilder();

                FuncaoInterna funcaoInterna = null;
                
                query.Append(" SELECT ID_FUNCAO_INTERNA, DESCRICAO, SITUACAO, PRIVADO FROM SEG_FUNCAO_INTERNA ");
                query.Append(" WHERE ID_FUNCAO_INTERNA IN (SELECT ID_FUNCAO_INTERNA FROM SEG_USUARIO_FUNCAO_INTERNA ");
                query.Append(" WHERE ID_USUARIO = :VALUE1 AND SITUACAO = 'A' )");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = usuario.Id;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    funcaoInterna = new FuncaoInterna(dr.GetInt64(0), dr.GetString(1), dr.GetString(2),dr.GetString(3));
                }

                return funcaoInterna;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFuncaoInternaAtivoByUsuario", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

    }
}
