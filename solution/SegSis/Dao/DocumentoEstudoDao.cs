﻿using Npgsql;
using NpgsqlTypes;
using SWS.Entidade;
using SWS.Excecao;
using SWS.Helper;
using SWS.IDao;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS.Dao
{
    class DocumentoEstudoDao : IDocumentoEstudoDao
    {
        public long recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            long proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_DOCUMENTO_ESTUDO_ID_DOCUMENTO_ESTUDO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DocumentoEstudo insert(DocumentoEstudo documentoEstudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            try
            {
                documentoEstudo.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_DOCUMENTO_ESTUDO( ID_DOCUMENTO_ESTUDO, ID_DOCUMENTO, ID_ESTUDO )");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3 )");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = documentoEstudo.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = documentoEstudo.Documento.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = documentoEstudo.Estudo.Id;

                command.ExecuteNonQuery();

                return documentoEstudo;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DocumentoEstudo update(DocumentoEstudo documentoEstudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_DOCUMENTO_ESTUDO SET ID_DOCUMENTO = :VALUE2, ID_ESTUDO = :VALUE3 ");
                query.Append(" WHERE ID_DOCUMENTO_ESTUDO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = documentoEstudo.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = documentoEstudo.Documento.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = documentoEstudo.Estudo.Id;

                command.ExecuteNonQuery();

                return documentoEstudo;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void delete(DocumentoEstudo documentoEstudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" DELETE FROM SEG_DOCUMENTO_ESTUDO WHERE ID_DOCUMENTO_ESTUDO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = documentoEstudo.Id;

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DocumentoEstudo findById(long id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            DocumentoEstudo documentoEstudo = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID_DOCUMENTO_ESTUDO, ID_DOCUMENTO, ID_ESTUDO ");
                query.Append(" FROM SEG_DOCUMENTO_ESTUDO WHERE ID_DOCUMENTO_ESTUDO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    documentoEstudo  = new DocumentoEstudo();
                    documentoEstudo.Id = dr.GetInt64(0);
                    Documento documento = new Documento();
                    documento.Id = dr.GetInt64(1);
                    documentoEstudo.Documento = documento;
                    Estudo estudo = new Estudo();
                    estudo.Id = dr.GetInt64(2);
                    documentoEstudo.Estudo = estudo;

                }

                return documentoEstudo;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAllByEstudo(Estudo estudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByEstudo");
            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT DOCUMENTO_ESTUDO.ID_DOCUMENTO_ESTUDO, DOCUMENTO_ESTUDO.ID_DOCUMENTO, DOCUMENTO.DESCRICAO AS DOCUMENTO, DOCUMENTO_ESTUDO.ID_ESTUDO ");
                query.Append(" FROM SEG_DOCUMENTO_ESTUDO DOCUMENTO_ESTUDO ");
                query.Append(" INNER JOIN SEG_DOCUMENTO DOCUMENTO ON DOCUMENTO.ID_DOCUMENTO = DOCUMENTO_ESTUDO.ID_DOCUMENTO ");
                query.Append(" WHERE DOCUMENTO_ESTUDO.ID_ESTUDO = :VALUE1 ");
                query.Append(" ORDER BY DOCUMENTO ASC ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = estudo.Id;

                DataSet ds = new DataSet();
                adapter.Fill(ds, "Documentos");
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<DocumentoEstudo> findAllByEstudoList(Estudo pcmso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByEstudoList");
            try
            {
                StringBuilder query = new StringBuilder();

                List<DocumentoEstudo> documentos = new List<DocumentoEstudo>();

                query.Append(" SELECT DOCUMENTO_ESTUDO.ID_DOCUMENTO_ESTUDO, DOCUMENTO_ESTUDO.ID_DOCUMENTO, DOCUMENTO.DESCRICAO AS DOCUMENTO, DOCUMENTO_ESTUDO.ID_ESTUDO ");
                query.Append(" FROM SEG_DOCUMENTO_ESTUDO DOCUMENTO_ESTUDO ");
                query.Append(" INNER JOIN SEG_DOCUMENTO DOCUMENTO ON DOCUMENTO.ID_DOCUMENTO = DOCUMENTO_ESTUDO.ID_DOCUMENTO ");
                query.Append(" WHERE DOCUMENTO_ESTUDO.ID_ESTUDO = :VALUE1 ");
                query.Append(" ORDER BY DOCUMENTO ASC ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = pcmso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    DocumentoEstudo documentoEstudo = new DocumentoEstudo();
                    documentoEstudo.Id = dr.GetInt64(0);
                    Documento documento = new Documento();
                    documento.Id = dr.GetInt64(1);
                    documentoEstudo.Documento = documento;
                    documentoEstudo.Estudo = pcmso;
                    documentos.Add(documentoEstudo);
                }

                return documentos;


                
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
