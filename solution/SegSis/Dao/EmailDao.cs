﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;
using SWS.Entidade;
using NpgsqlTypes;
using System.Data;

namespace SWS.Dao
{
    class EmailDao : IEmailDao
    {
        /// <summary>
        /// Método que recupera o próximo ID da tabela SEG_EMAIL.
        /// </summary>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("select nextval(('seg_email_id_email_seq'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        /// <summary>
        /// Método que inclui um e-mail na tabela SEG_EMAIL.
        /// </summary>
        /// <param name="agente"></param>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public Email insert(Email email, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertEmail");

            try
            {
                email.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_EMAIL( ID_EMAIL, ID_PRONTUARIO, PARA, ASSUNTO, MENSAGEM, ANEXO, PRIORIDADE, DATA_ENVIO, ID_USUARIO)");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6, :VALUE7, :VALUE8, :VALUE9 )");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = email.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = email.Prontuario != null ? email.Prontuario.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = email.Para;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = email.Assunto;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = email.Mensagem;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = email.Anexo;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = email.Prioridade;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Timestamp));
                command.Parameters[7].Value = email.DataEnvio;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Integer));
                command.Parameters[8].Value = email.Usuario.Id;

                command.ExecuteNonQuery();

                return email;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertEmail", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        /// <summary>
        /// Método que busca todos os e-mail do prontuário.
        /// </summary>
        /// <param name="prontuario">Prontuário para pesquisar</param>
        /// <param name="dbConnection">Parametros de conexão do banco de dados.</param>
        /// <returns>Um DataSet com todos os e-mail do prontuário</returns>
        public List<Email> findEmailByProntuario(Prontuario prontuario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findEmailByProntuario");

            List<Email> emails = new List<Email>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT MAIL.ID_EMAIL, MAIL.PARA, MAIL.ASSUNTO, MAIL.MENSAGEM, MAIL.ANEXO, MAIL.PRIORIDADE, MAIL.DATA_ENVIO, MAIL.ID_USUARIO ");
                query.Append(" FROM SEG_EMAIL MAIL  ");
                query.Append(" WHERE MAIL.ID_PRONTUARIO = :VALUE1 ");
                query.Append(" ORDER BY DATA_ENVIO ASC");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = prontuario.Id;


                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    emails.Add(new Email(dr.GetInt64(0), prontuario, dr.GetString(1), dr.GetString(2), dr.GetString(3),
                        dr.IsDBNull(4) ? String.Empty : dr.GetString(4), dr.GetString(5), dr.GetDateTime(6), dr.IsDBNull(7) ? null : new Usuario(dr.GetInt64(7))));
                        
                }

                return emails;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllEmailByProntuario", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<Email> findEmailByUsuario(Usuario usuario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findEmailByUsuario");

            List<Email> emails = new List<Email>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT MAIL.ID_EMAIL, MAIL.PARA, MAIL.ASSUNTO, MAIL.MENSAGEM, MAIL.ANEXO, MAIL.PRIORIDADE, MAIL.DATA_ENVIO, MAIL.ID_USUARIO, MAIL.ID_PRONTUARIO ");
                query.Append(" FROM SEG_EMAIL MAIL  ");
                query.Append(" WHERE MAIL.ID_USUARIO = :VALUE1 ");
                query.Append(" ORDER BY DATA_ENVIO ASC");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = usuario.Id;


                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    emails.Add(new Email(dr.GetInt64(0), dr.IsDBNull(8) ? null : new Prontuario(dr.GetInt64(8)), dr.GetString(1), dr.GetString(2), dr.GetString(3),
                        dr.IsDBNull(4) ? String.Empty : dr.GetString(4), dr.GetString(5), dr.GetDateTime(6), dr.IsDBNull(7) ? null : new Usuario(dr.GetInt64(7))));

                }

                return emails;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findEmailByUsuario", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }


}
