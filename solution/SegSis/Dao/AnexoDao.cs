﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Facade;
using Npgsql;
using NpgsqlTypes;
using System.Data;
using SWS.IDao;
using SWS.Entidade;
using SWS.Excecao;
using SWS.Helper;

namespace SWS.Dao
{
    class AnexoDao :IAnexoDao
    {
        public void insertAnexo(Anexo anexo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertAnexo");

            try
            {                
                NpgsqlCommand command = new NpgsqlCommand("insert into seg_anexo values (nextval('seg_anexo_id_anexo_seq'), :value2, :value3)", dbConnection);
                                
                command.Parameters.Add(new NpgsqlParameter("value2", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value3", NpgsqlDbType.Varchar));
               
                command.Parameters[0].Value = anexo.getEstudo().Id;
                command.Parameters[1].Value = anexo.getConteudo();
                
                command.ExecuteNonQuery();
                                
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertAnexo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAllAnexoByEstudo(Estudo estudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAnexoByEstudo");

            try
            {
                StringBuilder query = new StringBuilder();
                 
                query.Append("select id_anexo, id_estudo, conteudo from seg_anexo where id_estudo = :value1 order by conteudo ");
                
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                adapter.SelectCommand.Parameters[0].Value = estudo.Id;
                
                DataSet ds = new DataSet();
                adapter.Fill(ds, "Anexos");
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAnexoByEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Anexo findAnexoByConteudo(Anexo anexo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAnexoByConteudo");

            Anexo anexoProcurado = null;
            
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("select id_anexo, id_estudo, conteudo from seg_anexo where conteudo = :value1 and id_estudo = :value2", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Varchar));
                command.Parameters.Add(new NpgsqlParameter("value2", NpgsqlDbType.Integer));

                command.Parameters[0].Value = anexo.getConteudo();
                command.Parameters[1].Value = anexo.getEstudo().Id;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    Estudo estudo = new Estudo(dr.GetInt64(1));
                    anexoProcurado = new Anexo(dr.GetInt64(0), estudo, dr.GetString(2));
                }

                return anexoProcurado;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAnexoByConteudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void deleteAnexo(Anexo anexo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteAnexo");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("delete from seg_anexo where id_anexo = :value1", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = anexo.getId();
                
                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteAnexo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void updateAnexo(Anexo anexo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateAnexo");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("update seg_anexo set conteudo = :value1 where id_anexo = :value2", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Varchar));
                command.Parameters.Add(new NpgsqlParameter("value2", NpgsqlDbType.Integer));

                command.Parameters[0].Value = anexo.getConteudo();
                command.Parameters[1].Value = anexo.getId();

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateAnexo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

    }
}
