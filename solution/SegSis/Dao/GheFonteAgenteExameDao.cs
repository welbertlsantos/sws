﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using NpgsqlTypes;
using SWS.Entidade;
using SWS.IDao;
using System.Data;
using SWS.Excecao;
using SWS.Helper;
using SWS.View.Resources;

namespace SWS.Dao
{
    class GheFonteAgenteExameDao : IGheFonteAgenteExameDao
    {

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_GHE_FONTE_AGENTE_EXAME_ID_GHE_FONTE_AGENTE_EXAME_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoIdAgente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAtivosByGheFonteAgente(GheFonteAgente gheFonteAgente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtivosByGheFonteAgente");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME, GHE_FONTE_AGENTE_EXAME.ID_EXAME, GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE, ");
                query.Append(" GHE_FONTE_AGENTE_EXAME.IDADE_EXAME, EXAME.DESCRICAO, EXAME.SITUACAO, EXAME.LABORATORIO, GHE_FONTE_AGENTE_EXAME.CONFINADO, GHE_FONTE_AGENTE_EXAME.ALTURA, GHE_FONTE_AGENTE_EXAME.ELETRICIDADE ");
                query.Append(" FROM SEG_EXAME EXAME ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GHE_FONTE_AGENTE_EXAME ON GHE_FONTE_AGENTE_EXAME.ID_EXAME = EXAME.ID_EXAME ");
                query.Append(" WHERE GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE = :VALUE1 AND ");
                query.Append(" GHE_FONTE_AGENTE_EXAME.SITUACAO = 'A' ");
                query.Append(" ORDER BY EXAME.DESCRICAO ASC ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = gheFonteAgente.Id;

                DataSet dsExame = new DataSet();

                adapter.Fill(dsExame, "Exames");

                return dsExame;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtivosByGheFonteAgente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public HashSet<GheFonteAgenteExame> findAllByGheFonteAgente(GheFonteAgente gheFonteAgente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByGheFonteAgente");

            HashSet<GheFonteAgenteExame> gheFonteAgenteExames = new HashSet<GheFonteAgenteExame>();
            
            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GFAE.ID_GHE_FONTE_AGENTE_EXAME, GFAE.ID_EXAME, GFAE.ID_GHE_FONTE_AGENTE, GFAE.IDADE_EXAME, GFAE.SITUACAO, GFAE.CONFINADO, GFAE.ALTURA, GFAE.ELETRICIDADE ");
                query.Append(" FROM SEG_GHE_FONTE_AGENTE_EXAME GFAE ");
                query.Append(" WHERE GFAE.ID_GHE_FONTE_AGENTE = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonteAgente.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gheFonteAgenteExames.Add(new GheFonteAgenteExame(dr.GetInt64(0), new Exame(dr.GetInt64(1),null,null,null,0,0,0,false, false, string.Empty, false, null, false), new GheFonteAgente(dr.GetInt64(2)),dr.GetInt32(3), dr.GetString(4), dr.GetBoolean(5), dr.GetBoolean(6), dr.GetBoolean(7)));
                }

                return gheFonteAgenteExames;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByGheFonteAgente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public GheFonteAgenteExame findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            GheFonteAgenteExame gheFonteAgenteExame = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME, GHE_FONTE_AGENTE_EXAME.ID_EXAME, GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE, GHE_FONTE_AGENTE_EXAME.IDADE_EXAME, GHE_FONTE_AGENTE_EXAME.SITUACAO, GHE_FONTE_AGENTE_EXAME.CONFINADO, GHE_FONTE_AGENTE_EXAME.ALTURA, GHE_FONTE_AGENTE_EXAME.ELETRICIDADE ");
                query.Append(" FROM SEG_GHE_FONTE_AGENTE_EXAME GHE_FONTE_AGENTE_EXAME ");
                query.Append(" WHERE GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gheFonteAgenteExame = new GheFonteAgenteExame(dr.GetInt64(0), new Exame(dr.GetInt64(1), null, null, null, 0, 0, 0, false, false, string.Empty, false, null, false), new GheFonteAgente(dr.GetInt64(2)), dr.GetInt32(3), dr.GetString(4), dr.GetBoolean(5), dr.GetBoolean(6), dr.GetBoolean(7));
                }

                return gheFonteAgenteExame;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public GheFonteAgenteExame insert(GheFonteAgenteExame gheFonteAgenteExame, NpgsqlConnection dbconnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirGheFonteAgenteExame");

            StringBuilder query = new StringBuilder();

            try
            {
                gheFonteAgenteExame.Id = recuperaProximoId(dbconnection);

                query.Append(" INSERT INTO SEG_GHE_FONTE_AGENTE_EXAME ");
                query.Append(" (ID_GHE_FONTE_AGENTE_EXAME, ID_EXAME, ID_GHE_FONTE_AGENTE, IDADE_EXAME, SITUACAO, CONFINADO, ALTURA, ELETRICIDADE ) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, 'A', :VALUE5, :VALUE6, :VALUE7)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbconnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonteAgenteExame.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = gheFonteAgenteExame.Exame.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = gheFonteAgenteExame.GheFonteAgente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = gheFonteAgenteExame.IdadeExame;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Boolean));
                command.Parameters[4].Value = gheFonteAgenteExame.Confinado;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Boolean));
                command.Parameters[5].Value = gheFonteAgenteExame.Altura;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Boolean));
                command.Parameters[6].Value = gheFonteAgenteExame.Eletricidade;

                command.ExecuteNonQuery();

                return gheFonteAgenteExame;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Exame findExameByGheFonteAgenteExame(GheFonteAgenteExame gheFonteAgenteExame, NpgsqlConnection dbconnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findExameByGheFonteAgenteExame");

            Exame exame = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT EXAME.ID_EXAME, EXAME.DESCRICAO, EXAME.SITUACAO, EXAME.LABORATORIO, EXAME.PRECO, EXAME.PRIORIDADE, EXAME.CUSTO, EXAME.EXTERNO, EXAME.LIBERA_DOCUMENTO, EXAME.CODIGO_TUSS, EXAME.EXAME_COMPLEMENTAR, EXAME.PERIODO_VENCIMENTO, EXAME.PADRAO_CONTRATO ");
                query.Append(" FROM SEG_GHE_FONTE_AGENTE_EXAME GHE_FONTE_AGENTE_EXAME");
                query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = GHE_FONTE_AGENTE_EXAME.ID_EXAME ");
                query.Append(" WHERE GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME = :VALUE1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbconnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = gheFonteAgenteExame.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    exame = new Exame(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetBoolean(3), dr.GetDecimal(4), dr.GetInt32(5), dr.GetDecimal(6),dr.GetBoolean(7), dr.GetBoolean(8), dr.IsDBNull(9) ? string.Empty : dr.GetString(9), dr.GetBoolean(10), dr.IsDBNull(11) ? (int?)null : dr.GetInt32(11), dr.GetBoolean(12));
                }

                return exame;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findExameByGheFonteAgenteExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void update(GheFonteAgenteExame gheFonteAgenteExame, NpgsqlConnection dbconnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateGheFonteAgenteExame");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_GHE_FONTE_AGENTE_EXAME SET IDADE_EXAME = :VALUE2, SITUACAO = :VALUE3, CONFINADO = :VALUE4, ALTURA = :VALUE5, ELETRICIDADE = :VALUE6 ");
                query.Append(" WHERE ID_GHE_FONTE_AGENTE_EXAME = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(),dbconnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonteAgenteExame.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = gheFonteAgenteExame.IdadeExame;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = gheFonteAgenteExame.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Boolean));
                command.Parameters[3].Value = gheFonteAgenteExame.Confinado;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Boolean));
                command.Parameters[4].Value = gheFonteAgenteExame.Altura;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Boolean));
                command.Parameters[5].Value = gheFonteAgenteExame.Eletricidade;
                
                
                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateGheFonteAgenteExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void delete(GheFonteAgenteExame gheFonteAgenteExame, NpgsqlConnection dbconnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteGheFonteAgenteExame");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" DELETE FROM SEG_GHE_FONTE_AGENTE_EXAME WHERE ID_GHE_FONTE_AGENTE_EXAME = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbconnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonteAgenteExame.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteGheFonteAgenteExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public bool isUsedInService(GheFonteAgenteExame gheFonteAgenteExame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método isGheFonteAgenteExameUsed");

            bool result = false;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT DISTINCT GHE_FONTE_AGENTE_EXAME.* FROM SEG_ASO ATENDIMENTO  ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GHE_FONTE_AGENTE_EXAME_ASO ON GHE_FONTE_AGENTE_EXAME_ASO.ID_ASO = ATENDIMENTO.ID_ASO  ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GHE_FONTE_AGENTE_EXAME ON GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME = GHE_FONTE_AGENTE_EXAME_ASO.ID_GHE_FONTE_AGENTE_EXAME  ");
                query.Append(" WHERE GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonteAgenteExame.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    if (dr.GetInt64(0) > 0)
                    {
                        result = true;
                    }
                }

                return result;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - isGheFonteAgenteExameUsed", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean isGheFonteAgenteExameUsedByExame(Exame exame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método isGheFonteAgenteExameUsedByExame");

            Boolean isGheFonteAgenteExameInUsed = false;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT COUNT(*) FROM SEG_GHE_FONTE_AGENTE_EXAME GHE_FONTE_AGENTE_EXAME ");
                query.Append(" WHERE GHE_FONTE_AGENTE_EXAME.ID_EXAME = :VALUE1 AND GHE_FONTE_AGENTE_EXAME.SITUACAO = 'A' ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = exame.Id;
                

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    if (dr.GetInt64(0) > 0)
                    {
                        isGheFonteAgenteExameInUsed = true;
                    }
                }

                return isGheFonteAgenteExameInUsed;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - isGheFonteAgenteExameUsedByExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

                
    }
}
