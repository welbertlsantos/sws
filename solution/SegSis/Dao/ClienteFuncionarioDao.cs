﻿using Npgsql;
using NpgsqlTypes;
using SWS.Entidade;
using SWS.Excecao;
using SWS.Helper;
using SWS.IDao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS.Dao
{
    class ClienteFuncionarioDao: IClienteFuncionarioDao
    {
        public ClienteFuncionario findById(long id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            ClienteFuncionario clienteFuncionario = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID, ID_CLIENTE, ID_FUNCIONARIO, BR_PDH, DATA_ADMISSAO, DATA_DEMISSAO, REGIME_REVEZAMENTO, MATRICULA, ESTAGIARIO, VINCULO ");
                query.Append(" FROM SEG_CLIENTE_FUNCIONARIO  ");
                query.Append(" WHERE ID = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    clienteFuncionario = new ClienteFuncionario(dr.GetInt64(0), new Funcionario(dr.GetInt64(2)), new Cliente(dr.GetInt64(1)), dr.IsDBNull(3) ? string.Empty : dr.GetString(3), dr.GetDateTime(4), dr.IsDBNull(5) ? (DateTime?)null : dr.GetDateTime(5), dr.IsDBNull(6) ? string.Empty : dr.GetString(6), dr.IsDBNull(7) ? string.Empty : dr.GetString(7), dr.GetBoolean(8), dr.GetBoolean(9)); 
                }

                return clienteFuncionario;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<ClienteFuncionario> findAtivosByCliente(Cliente cliente, DateTime periodo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtivosByCliente");

            List<ClienteFuncionario> clienteFuncionarioRetorno = new List<ClienteFuncionario>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID, ID_FUNCIONARIO, ID_CLIENTE, BR_PDH, DATA_ADMISSAO, DATA_DEMISSAO, REGIME_REVEZAMENTO, MATRICULA, ESTAGIARIO, VINCULO ");
                query.Append(" FROM SEG_CLIENTE_FUNCIONARIO  ");
                query.Append(" WHERE TRUE  ");
                query.Append(" AND VINCULO = TRUE  ");
                query.Append(" AND DATA_ADMISSAO <= :VALUE2  ");
                query.Append(" AND DATA_DEMISSAO IS NULL  ");
                query.Append(" AND ID_CLIENTE = :VALUE1  ");
                query.Append(" AND ID NOT IN (  ");
                query.Append(" SELECT CF.ID FROM SEG_CLIENTE_FUNCIONARIO CF ");
                query.Append(" JOIN SEG_MONITORAMENTO M ON M.ID_CLIENTE_FUNCIONARIO = CF.ID ");
                query.Append(" WHERE M.PERIODO = :VALUE2) ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Date));
                command.Parameters[1].Value = periodo;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {

                    clienteFuncionarioRetorno.Add(new ClienteFuncionario(dr.GetInt64(0), new Funcionario(dr.GetInt64(1)), new Cliente(dr.GetInt64(2)), dr.IsDBNull(3) ? string.Empty : dr.GetString(3), dr.GetDateTime(4), dr.IsDBNull(5) ? (DateTime?)null : dr.GetDateTime(5), dr.IsDBNull(6) ? string.Empty : dr.GetString(6), dr.IsDBNull(7) ? string.Empty : dr.GetString(7), dr.GetBoolean(8), dr.GetBoolean(9)));
                }

                return clienteFuncionarioRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtivosByCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<ClienteFuncionario> findAllByFuncionario(Funcionario funcionario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByFuncioanario");

            List<ClienteFuncionario> clienteFuncionarioList = new List<ClienteFuncionario>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID, ID_CLIENTE, ID_FUNCIONARIO, BR_PDH, DATA_ADMISSAO, DATA_DEMISSAO, REGIME_REVEZAMENTO, MATRICULA, ESTAGIARIO, VINCULO ");
                query.Append(" FROM SEG_CLIENTE_FUNCIONARIO  ");
                query.Append(" WHERE ID_FUNCIONARIO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = funcionario.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    clienteFuncionarioList.Add(new ClienteFuncionario(dr.GetInt64(0), new Funcionario(dr.GetInt64(2)), new Cliente(dr.GetInt64(1)), dr.IsDBNull(3) ? string.Empty : dr.GetString(3), dr.GetDateTime(4), dr.IsDBNull(5) ? (DateTime?)null : dr.GetDateTime(5), dr.IsDBNull(6) ? string.Empty : dr.GetString(6), dr.IsDBNull(7) ? string.Empty : dr.GetString(7), dr.GetBoolean(8), dr.GetBoolean(9)));
                }
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByFuncioanario", ex);
                throw new DataBaseConectionException(ex.Message);
            }
            return clienteFuncionarioList;
        }

        public ClienteFuncionario update(ClienteFuncionario clienteFuncionario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_CLIENTE_FUNCIONARIO SET ");
                query.Append(" BR_PDH = :VALUE2, ");
                query.Append(" DATA_ADMISSAO = :VALUE3 ");
                query.Append(" DATA_DEMISSAO = :VALUE4 ");
                query.Append(" REGIME_REVEZAMENTO = :VALUE5 ");
                query.Append(" MATRICULA = :VALUE6 ");
                query.Append(" ESTAGIARIO = :VALUE7 ");
                query.Append(" VINCULO = :VALUE8 ");
                query.Append(" WHERE ID = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncionario.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = clienteFuncionario.Brpdh;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Date));
                command.Parameters[2].Value = clienteFuncionario.DataAdmissao;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Date));
                command.Parameters[3].Value = clienteFuncionario.DataDemissao;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = clienteFuncionario.RegimeRevezamento;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = clienteFuncionario.Matricula;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Boolean));
                command.Parameters[6].Value = clienteFuncionario.Estagiario;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Boolean));
                command.Parameters[7].Value = clienteFuncionario.Vinculo;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }

            return clienteFuncionario;

        }

        public void delete(ClienteFuncionario clienteFuncionario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" DELETE FROM SEG_CLIENTE_FUNCIONARIO ");
                query.Append(" WHERE ID = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncionario.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }
    }
}
