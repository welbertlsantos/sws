﻿using SWS.Entidade;
using Npgsql;
using SWS.Excecao;
using System;
using NpgsqlTypes;
using System.Data;
using System.Text;
using SWS.IDao;
using SWS.View.Resources;
using System.Collections.Generic;
using SWS.Helper;

namespace SWS.Dao
{
    class GheNotaDao : IGheNotaDao
    {

        public void insert(GheNota gheNota, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirGheNorma");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" INSERT INTO SEG_NORMA_GHE ( ID_NORMA, ID_GHE ) VALUES ( :VALUE1, :VALUE2 ) ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheNota.Nota.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = gheNota.Ghe.Id;
                

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirGheNorma", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public void delete(GheNota gheNota, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteGheNorma");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" DELETE FROM SEG_NORMA_GHE WHERE ID_NORMA = :VALUE1 AND ID_GHE = :VALUE2 ", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheNota.Nota.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = gheNota.Ghe.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirGheNorma", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public HashSet<Nota> findAllByGhe(Ghe ghe, NpgsqlConnection dbconnection)
        {
            try
            {
                HashSet<Nota> notaGhe = new HashSet<Nota>();

                LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllNormaByGhe");
                
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT N.ID_NORMA, N.DESCRICAO, N.SITUACAO, N.CONTEUDO FROM SEG_NORMA_GHE NG ");
                query.Append(" LEFT JOIN SEG_NORMA N ON NG.ID_NORMA = N.ID_NORMA ");
                query.Append(" WHERE ID_GHE = :VALUE1 ORDER BY N.DESCRICAO ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbconnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = ghe.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    notaGhe.Add(new Nota(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3)));
                }

                return notaGhe;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllNormaByGhe", ex);
                throw new DataBaseConectionException(ex.Message);
            }   

        }

        public DataSet findAllByGheDataSet(Ghe ghe, NpgsqlConnection dbconnection)
        {
            try
            {
                LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllNotasByGhe");

                StringBuilder query = new StringBuilder();

                query.Append(" SELECT N.ID_NORMA, N.DESCRICAO, N.SITUACAO, N.CONTEUDO FROM SEG_NORMA_GHE NG ");
                query.Append(" LEFT JOIN SEG_NORMA N ON NG.ID_NORMA = N.ID_NORMA ");
                query.Append(" WHERE ID_GHE = :VALUE1 ORDER BY N.DESCRICAO ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbconnection);

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbconnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = ghe.Id;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Notas");

                return ds;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllNotasByGhe", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

    }
}
