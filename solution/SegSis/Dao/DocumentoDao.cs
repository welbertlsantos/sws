﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using System.Data;
using SWS.Entidade;
using Npgsql;
using SWS.Helper;
using NpgsqlTypes;
using SWS.Excecao;

namespace SWS.Dao
{
    class DocumentoDao : IDocumentoDao
    {
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_DOCUMENTO_ID_DOCUMENTO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public DataSet findByFilter(Documento documento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByFilter");

            try
            {
                StringBuilder query = new StringBuilder();
                
                if (documento.isNullForFilter())
                    query.Append(" SELECT ID_DOCUMENTO, DESCRICAO, SITUACAO FROM SEG_DOCUMENTO ");
                else
                {
                    query.Append(" SELECT ID_DOCUMENTO, DESCRICAO, SITUACAO FROM SEG_DOCUMENTO WHERE TRUE ");

                    if (!String.IsNullOrEmpty(documento.Descricao))
                        query.Append(" AND DESCRICAO LIKE :VALUE1 ");

                    if (!String.IsNullOrEmpty(documento.Situacao))
                        query.Append(" AND SITUACAO = :VALUE2 ");
                }

                query.Append(" ORDER BY DESCRICAO ASC");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = documento.Descricao + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = documento.Situacao;

                DataSet ds = new DataSet();
                adapter.Fill(ds, "Documentos");
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Documento insert(Documento documento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertDocumento");

            try
            {
                documento.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_DOCUMENTO(ID_DOCUMENTO, DESCRICAO, SITUACAO ) VALUES (:VALUE1, :VALUE2, 'A')");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = documento.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = documento.Descricao;

                command.ExecuteNonQuery();

                return documento;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertDocumento", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Documento findByDescricao(String descricao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByDescricao");

            Documento documentoReturn = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_DOCUMENTO, DESCRICAO, SITUACAO FROM SEG_DOCUMENTO WHERE DESCRICAO = :VALUE1 " );

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = descricao;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    documentoReturn = new Documento(dr.GetInt64(0), dr.GetString(1), dr.GetString(2));
                }

                return documentoReturn;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByDescricao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Documento findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            Documento documentoReturn = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID_DOCUMENTO, DESCRICAO, SITUACAO FROM SEG_DOCUMENTO WHERE ID_DOCUMENTO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    documentoReturn = new Documento(dr.GetInt64(0), dr.GetString(1), dr.GetString(2));

                }

                return documentoReturn;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Documento update(Documento documento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateDocumento");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_DOCUMENTO SET DESCRICAO = :VALUE2, SITUACAO = :VALUE3 WHERE ID_DOCUMENTO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = documento.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = documento.Descricao;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = documento.Situacao;

                command.ExecuteNonQuery();

                return documento;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateDocumento", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<Documento> findAll(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAll");

            List<Documento> documentos = new List<Documento>();
            
            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_DOCUMENTO, DESCRICAO, SITUACAO FROM SEG_DOCUMENTO WHERE SITUACAO = 'A' ORDER BY DESCRICAO ASC "); 

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    documentos.Add(new Documento(dr.GetInt64(0), dr.GetString(1), dr.GetString(2)));
                }

                return documentos;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAll", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }


        public Boolean checkCanChanges(Documento documento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método checkCanChanges");

            Boolean retorno = false;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT * FROM SEG_DOCUMENTO_PROTOCOLO WHERE ID_DOCUMENTO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = documento.Id;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    retorno = true;
                }

                return retorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - checkCanChanges", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
    }
}
