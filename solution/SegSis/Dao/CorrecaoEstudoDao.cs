﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;
using SWS.Entidade;
using NpgsqlTypes;
using System.Data;

namespace SWS.Dao
{
    class CorrecaoEstudoDao : ICorrecaoEstudoDao
    {
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_CORRECAO_ESTUDO_ID_CORRECAO_ESTUDO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoIdAgente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public CorrecaoEstudo insertCorrecaoEstudo(CorrecaoEstudo correcaoEstudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertCorrecaoEstudo");

            try
            {
                correcaoEstudo.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_CORRECAO_ESTUDO( ID_CORRECAO_ESTUDO, ID_USUARIO, ID_ESTUDO, HISTORICO, DATA_CORRECAO)");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = correcaoEstudo.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = correcaoEstudo.Usuario.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = correcaoEstudo.Estudo.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Text));
                command.Parameters[3].Value = correcaoEstudo.Historico;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Timestamp));
                command.Parameters[4].Value = correcaoEstudo.DataCorrecao;

                command.ExecuteNonQuery();

                return correcaoEstudo;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertCorrecaoEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAllCorrecaoByEstudo(Estudo estudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllCorrecaoByEstudo");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT CORRECAO_ESTUDO.ID_CORRECAO_ESTUDO, CORRECAO_ESTUDO.ID_USUARIO, USUARIO.LOGIN AS LOGIN, USUARIO.NOME AS NOME, CORRECAO_ESTUDO.ID_ESTUDO,  CORRECAO_ESTUDO.HISTORICO, CORRECAO_ESTUDO.DATA_CORRECAO ");
                query.Append(" FROM SEG_CORRECAO_ESTUDO CORRECAO_ESTUDO ");
                query.Append(" JOIN SEG_USUARIO USUARIO ON USUARIO.ID_USUARIO = CORRECAO_ESTUDO.ID_USUARIO ");
                query.Append(" WHERE CORRECAO_ESTUDO.ID_ESTUDO = :VALUE1 ORDER BY CORRECAO_ESTUDO.ID_CORRECAO_ESTUDO ASC ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = estudo.Id;

                DataSet ds = new DataSet();
                
                adapter.Fill(ds, "Correcoes");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllCorrecaoByEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

    }
}
