﻿using SWS.Entidade;
using Npgsql;
using SWS.Excecao;
using System;
using NpgsqlTypes;
using System.Data;
using System.Text;
using SWS.IDao;
using System.Collections .Generic;
using SWS.View.Resources;
using SWS.Helper;

namespace SWS.Dao
{
    class ClienteVendedorDao : IClienteVendedorDao
    {
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_REP_CLI_ID_SEG_REP_CLI_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoIdClienteVendedor", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public VendedorCliente insert(VendedorCliente vendedorCliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            StringBuilder query = new StringBuilder();

            try
            {
                vendedorCliente.Id = recuperaProximoId(dbConnection);

                query.Append(" INSERT INTO SEG_REP_CLI (ID_SEG_REP_CLI, ID_CLIENTE, ID_VEND, SITUACAO) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, 'A')");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = vendedorCliente.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = vendedorCliente.Cliente.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = vendedorCliente.Vendedor.Id;
                
                
                command.ExecuteNonQuery();

                return vendedorCliente;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<VendedorCliente> findByCliente(Cliente cliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByCliente");
   
            List<VendedorCliente> vendedorClienteList = new List<VendedorCliente>();
            
            StringBuilder query = new StringBuilder();
            
            
            try
            {
                // Montando o objeto vendedor do cliente

                query.Append(" SELECT VENDEDOR.ID_VEND, VENDEDOR.NOME, VENDEDOR.CPF, VENDEDOR.RG, VENDEDOR.ENDERECO, VENDEDOR.NUMERO, VENDEDOR.COMPLEMENTO, ");
                query.Append(" VENDEDOR.BAIRRO, VENDEDOR.CIDADE, VENDEDOR.UF, VENDEDOR.CEP, VENDEDOR.TELEFONE1, VENDEDOR.TELEFONE2, VENDEDOR.EMAIL, VENDEDOR.COMISSAO, VENDEDOR.DATA_CADASTRO, VENDEDOR.SITUACAO, ");
                query.Append(" REP_CLI.ID_SEG_REP_CLI, REP_CLI.SITUACAO, VENDEDOR.ID_CIDADE_IBGE, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE ");
                query.Append(" FROM SEG_VENDEDOR VENDEDOR  ");
                query.Append(" JOIN SEG_REP_CLI REP_CLI ON REP_CLI.ID_VEND = VENDEDOR.ID_VEND ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = VENDEDOR.ID_CIDADE_IBGE");
                query.Append(" WHERE REP_CLI.ID_CLIENTE = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    vendedorClienteList.Add(new VendedorCliente(dr.GetInt64(17), cliente, new Vendedor(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3),
                    dr.GetString(4), dr.GetString(5), dr.GetString(6), dr.GetString(7), dr.GetString(8),
                    dr.GetString(9), dr.GetString(10), dr.GetString(11), dr.IsDBNull(12) ? string.Empty : dr.GetString(12), dr.GetString(13),
                    dr.GetDecimal(14), dr.GetDateTime(15), dr.GetString(16), dr.IsDBNull(19) ? null : new CidadeIbge(dr.GetInt64(19), dr.GetString(20), dr.GetString(21), dr.GetString(22))), dr.GetString(18)));

                }
                
                return vendedorClienteList;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public VendedorCliente findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            VendedorCliente vendedorCliente = null;

            StringBuilder query = new StringBuilder();
            
            try
            {

                query.Append(" SELECT VENDEDOR.ID_VEND, VENDEDOR.NOME, VENDEDOR.CPF, VENDEDOR.RG, VENDEDOR.ENDERECO, VENDEDOR.NUMERO, VENDEDOR.COMPLEMENTO, VENDEDOR.BAIRRO, VENDEDOR.CIDADE, ");
                query.Append(" VENDEDOR.UF, VENDEDOR.CEP, VENDEDOR.TELEFONE1, VENDEDOR.TELEFONE2, VENDEDOR.EMAIL, VENDEDOR.COMISSAO, VENDEDOR.DATA_CADASTRO, VENDEDOR.SITUACAO, ");
                query.Append(" CLIENTE.ID_CLIENTE, CLIENTE.RAZAO_SOCIAL, CLIENTE.FANTASIA, CLIENTE.CNPJ, CLIENTE.INSCRICAO, CLIENTE.ENDERECO, CLIENTE.NUMERO, CLIENTE.COMPLEMENTO, ");
                query.Append(" CLIENTE.BAIRRO, CLIENTE.CEP, CLIENTE.UF, CLIENTE.TELEFONE1, CLIENTE.TELEFONE2, CLIENTE.EMAIL, CLIENTE.SITE, CLIENTE.DATA_CADASTRO, CLIENTE.RESPONSAVEL,  ");
                query.Append(" CLIENTE.CARGO, CLIENTE.DOCUMENTO, CLIENTE.ESCOPO, CLIENTE.JORNADA, CLIENTE.SITUACAO, CLIENTE.EST_MASC, CLIENTE.EST_FEM, CLIENTE.ENDERECOCOB, ");
                query.Append(" CLIENTE.NUMEROCOB, CLIENTE.COMPLEMENTOCOB, CLIENTE.BAIRROCOB, CLIENTE.CEPCOB, CLIENTE.UFCOB, CLIENTE.TELEFONECOB, CLIENTE.ID_MEDICO, CLIENTE.TELEFONE2COB, ");
                query.Append(" CLIENTE.VIP, CLIENTE.TELEFONE_CONTATO, CLIENTE.USA_CONTRATO, CLIENTE.ID_CLIENTE_MATRIZ, CLIENTE.ID_CLIENTE_CREDENCIADO, CLIENTE.PARTICULAR, ");
                query.Append(" CLIENTE.UNIDADE, CLIENTE.CREDENCIADA, CLIENTE.PRESTADOR, CLIENTE.ID_CIDADE_IBGE, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, ");
                query.Append(" CLIENTE.ID_CIDADE_IBGE_COBRANCA, CIDADE_COBRANCA.CODIGO, CIDADE_COBRANCA.NOME, CIDADE_COBRANCA.UF_CIDADE, ");
                query.Append(" CLIENTE.DESTACA_ISS, CLIENTE.GERA_COBRANCA_VALOR_LIQUIDO, CLIENTE.ALIQUOTA_ISS, ");
                query.Append(" REP_CLI.ID_SEG_REP_CLI, REP_CLI.SITUACAO, CLIENTE.CODIGO_CNES, CLIENTE.USA_CENTRO_CUSTO, CLIENTE.BLOQUEADO, CLIENTE.CREDENCIADORA, CLIENTE.FISICA, CLIENTE.USA_PO, VENDEDOR.ID_CIDADE_IBGE, CIDADE_VENDEDOR.CODIGO, CIDADE_VENDEDOR.NOME, CIDADE_VENDEDOR.UF_CIDADE, CLIENTE.SIMPLES_NACIONAL ");
                query.Append(" FROM SEG_REP_CLI  REP_CLI ");
                query.Append(" JOIN SEG_VENDEDOR VENDEDOR ON VENDEDOR.ID_VEND = REP_CLI.ID_VEND");
                query.Append(" JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = REP_CLI.ID_CLIENTE");
                query.Append(" JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = CLIENTE.ID_CIDADE_IBGE ");
                query.Append(" JOIN SEG_CIDADE_IBGE CIDADE_COBRANCA ON CIDADE_COBRANCA.ID_CIDADE_IBGE = CLIENTE.ID_CIDADE_IBGE_COBRANCA ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE_VENDEDOR ON CIDADE_VENDEDOR.ID_CIDADE_IBGE = VENDEDOR.ID_CIDADE_IBGE ");
                query.Append(" WHERE REP_CLI.ID_SEG_REP_CLI = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();
                
                while (dr.Read())
                {
                    vendedorCliente = new VendedorCliente(
                        dr.GetInt64(70), 
                        new Cliente(dr.GetInt64(17), dr.GetString(18), dr.GetString(19), dr.GetString(20), dr.GetString(21), dr.GetString(22), dr.GetString(23), dr.GetString(24), dr.GetString(25), dr.GetString(26), dr.GetString(27), dr.GetString(28), dr.GetString(29), dr.GetString(30), dr.GetString(31), dr.GetDateTime(32), dr.GetString(33), dr.GetString(34), dr.GetString(35), dr.GetString(36), dr.GetString(37), dr.GetString(38), dr.GetInt32(39), dr.GetInt32(40), dr.GetString(41), dr.GetString(42), dr.GetString(43), dr.GetString(44), dr.GetString(45), dr.GetString(46), dr.GetString(47), dr.IsDBNull(48) ? null : new Medico(dr.GetInt64(48)), dr.GetString(49), dr.GetBoolean(50), dr.GetString(51), dr.GetBoolean(52), dr.IsDBNull(53) ? null : new Cliente(dr.GetInt64(53)), dr.IsDBNull(54) ? null : new Cliente(dr.GetInt64(54)), dr.GetBoolean(55), dr.GetBoolean(56), dr.GetBoolean(57), dr.GetBoolean(58), dr.IsDBNull(59) ? null : new CidadeIbge(dr.GetInt64(59), dr.GetString(60), dr.GetString(61), dr.GetString(62)), dr.IsDBNull(63) ? null : new CidadeIbge(dr.GetInt64(63), dr.GetString(64), dr.GetString(65), dr.GetString(66)), dr.GetBoolean(67), dr.GetBoolean(68), dr.GetDecimal(69), dr.IsDBNull(72) ? string.Empty : dr.GetString(72), dr.GetBoolean(73), dr.GetBoolean(74), dr.GetBoolean(75), dr.GetBoolean(76), dr.GetBoolean(77), dr.GetBoolean(82)), 
                        new Vendedor(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.GetString(4), dr.GetString(5), dr.GetString(6), dr.GetString(7), dr.GetString(8), dr.GetString(9), dr.GetString(10), dr.GetString(11), dr.GetString(12), dr.GetString(13), dr.GetDecimal(14), dr.GetDateTime(15), dr.GetString(16), dr.IsDBNull(78) ? null : new CidadeIbge(dr.GetInt64(78), dr.GetString(79), dr.GetString(80), dr.GetString(81))), 
                        dr.GetString(71)); 
                }
                
                return vendedorCliente;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void update(VendedorCliente vendedorCliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("UPDATE SEG_REP_CLI SET SITUACAO = 'I' WHERE ID_SEG_REP_CLI = :VALUE1", dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = vendedorCliente.Id;

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

    }
}