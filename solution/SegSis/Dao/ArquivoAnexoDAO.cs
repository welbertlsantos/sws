﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using SWS.Entidade;
using SWS.Helper;
using SWS.Excecao;
using NpgsqlTypes;
using System.Data;
using SWS.IDao;

namespace SWS.Dao
{
    class ArquivoAnexoDao : IArquivoAnexoDao
    {
        public ArquivoAnexo insertArquivoAnexo(ArquivoAnexo arquivo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertArquivoAnexo");

            try
            {
                arquivo.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_ARQUIVO_ANEXO (ID_ARQUIVO, DESCRICAO, MIMETYPE, SIZE, CONTEUDO, ID_ASO) VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = arquivo.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = arquivo.Descricao.ToUpper();

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = arquivo.Mimetype.ToUpper();

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = arquivo.Size;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Bytea));
                command.Parameters[4].Value = arquivo.Conteudo;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Integer));
                command.Parameters[5].Value = arquivo.Aso.Id;

                command.ExecuteNonQuery();

                return arquivo;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertArquivoAnexo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public ArquivoAnexo findArquivoAnexoById(Int64? id, NpgsqlConnection dbConnection)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findArquivoAnexoById");

            ArquivoAnexo arquivoRetorno = null;

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT ID_ARQUIVO, DESCRICAO, MIMETYPE, SIZE, CONTEUDO, ID_ASO FROM SEG_ARQUIVO_ANEXO WHERE ID_ARQUIVO = :VALUE1", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                byte[] conteudo;

                while (dr.Read())
                {
                    conteudo = (byte[])dr.GetValue(4);
                    arquivoRetorno = new ArquivoAnexo(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetInt32(3), conteudo, new Aso(dr.GetInt64(5)));
                }

                return arquivoRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findArquivoAnexoById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAllArquivoAnexoByAso(Aso aso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllArquivoAnexoByAso");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append("SELECT ID_ARQUIVO, DESCRICAO FROM SEG_ARQUIVO_ANEXO WHERE ID_ASO = :VALUE1 ORDER BY DESCRICAO ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = aso.Id;

                DataSet ds = new DataSet();
                adapter.Fill(ds, "Anexos");
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllArquivoAnexoByAso", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        private Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_ARQUIVO_ANEXO_ID_ARQUIVO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void delete(ArquivoAnexo arquivoAnexo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("DELETE FROM SEG_ARQUIVO_ANEXO WHERE ID_ARQUIVO = :VALUE1", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = arquivoAnexo.Id;
              
                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
