﻿using Npgsql;
using NpgsqlTypes;
using SWS.Entidade;
using SWS.Excecao;
using SWS.Helper;
using SWS.IDao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Dao
{
    class ProcedimentoEsocialDao : IProcedimentoEsocialDao
    {
        public List<ProcedimentoEsocial> findAllByFilter(string stringText, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByFilter");

            List<ProcedimentoEsocial> procedimentos = new List<ProcedimentoEsocial>();

            StringBuilder query = new StringBuilder();

            try
            {
                if (string.IsNullOrEmpty(stringText))
                {
                    query.Append(" SELECT ID, CODIGO, PROCEDIMENTO ");
                    query.Append(" FROM SEG_PROCEDIMENTO_ESOCIAL ");
                    query.Append(" ORDER BY PROCEDIMENTO ASC");
                }
                else
                {
                    query.Append(" SELECT ID, CODIGO, PROCEDIMENTO ");
                    query.Append(" FROM SEG_PROCEDIMENTO_ESOCIAL ");
                    query.Append(" WHERE PROCEDIMENTO LIKE :VALUE1 ");
                    query.Append(" ORDER BY PROCEDIMENTO ASC");

                }

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = "%" + stringText + "%";
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    procedimentos.Add(new ProcedimentoEsocial(dr.GetInt64(0), dr.GetString(1), dr.GetString(2)));
                }

                return procedimentos;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }


    }
}
