﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;
using SWS.Entidade;
using NpgsqlTypes;

namespace SWS.Dao
{
    class DocumentoProtocoloDao : IDocumentoProtocoloDao
    {
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("select nextval(('seg_documento_protocolo_id_documento_protocolo_seq'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DocumentoProtocolo insertDocumentoProtocolo(DocumentoProtocolo documentoProtocolo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertDocumentoProtocolo");

            try
            {
                documentoProtocolo.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" insert into seg_documento_protocolo(id_documento_protocolo, id_aso, id_documento, id_protocolo, data_gravacao) values (:value1, :value2, :value3, :value4, :value5) ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value2", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value3", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value4", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value5", NpgsqlDbType.Date));

                command.Parameters[0].Value = documentoProtocolo.Id;
                command.Parameters[1].Value = documentoProtocolo.Aso != null ? documentoProtocolo.Aso.Id : null;
                command.Parameters[2].Value = documentoProtocolo.Documento != null ? documentoProtocolo.Documento.Id : null;
                command.Parameters[3].Value = documentoProtocolo.Protocolo != null ? documentoProtocolo.Protocolo.Id : null;
                command.Parameters[4].Value = documentoProtocolo.DataGravacao;

                command.ExecuteNonQuery();

                return documentoProtocolo;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertDocumentoProtocolo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void deleteDocumentoProtocolo(DocumentoProtocolo documentoProtocolo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteDocumentoProtocolo");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" delete from seg_documento_protocolo where id_documento_protocolo = :value1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = documentoProtocolo.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteDocumentoProtocolo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
    }
}
