﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;
using SWS.Entidade;
using NpgsqlTypes;
using System.Data;

namespace SWS.Dao
{
    class EstimativaEstudoDao :IEstimativaEstudoDao
    {

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_ESTIMATIVA_ESTUDO_ID_ESTIMATIVA_ESTUDO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public EstimativaEstudo insert(EstimativaEstudo estimativaEstudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            try
            {
                estimativaEstudo.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_ESTIMATIVA_ESTUDO( ID_ESTIMATIVA_ESTUDO, ID_ESTUDO, ID_ESTIMATIVA, QUANTIDADE )");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4 )");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = estimativaEstudo.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = estimativaEstudo.Estudo.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = estimativaEstudo.Estimativa.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = estimativaEstudo.Quantidade;

                command.ExecuteNonQuery();

                return estimativaEstudo;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void delete(EstimativaEstudo estimativaEstudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" DELETE FROM SEG_ESTIMATIVA_ESTUDO WHERE ID_ESTIMATIVA_ESTUDO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = estimativaEstudo.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public EstimativaEstudo update(EstimativaEstudo estimativaEstudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_ESTIMATIVA_ESTUDO SET ");
                query.Append(" QUANTIDADE = :VALUE2 ");
                query.Append(" WHERE ID_ESTIMATIVA_ESTUDO = :VALUE1 ");


                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = estimativaEstudo.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = estimativaEstudo.Quantidade;

                command.ExecuteNonQuery();

                return estimativaEstudo;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAllByEstudo(Estudo estudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByEstudo");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ESTIMATIVA_ESTUDO.ID_ESTIMATIVA_ESTUDO, ESTIMATIVA_ESTUDO.ID_ESTIMATIVA, ESTIMATIVA_ESTUDO.ID_ESTUDO, ESTIMATIVA.DESCRICAO, ESTIMATIVA_ESTUDO.QUANTIDADE  ");
                query.Append(" FROM SEG_ESTIMATIVA_ESTUDO ESTIMATIVA_ESTUDO  ");
                query.Append(" JOIN SEG_ESTIMATIVA ESTIMATIVA ON ESTIMATIVA.ID_ESTIMATIVA = ESTIMATIVA_ESTUDO.ID_ESTIMATIVA ");
                query.Append(" WHERE ESTIMATIVA_ESTUDO.ID_ESTUDO = :VALUE1  ");
                query.Append(" ORDER BY ESTIMATIVA.DESCRICAO ASC  ");
                
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                adapter.SelectCommand.Parameters[0].Value = estudo.Id;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Estimativa");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        

    }
}
