﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using Npgsql;
using SWS.Helper;
using NpgsqlTypes;
using SWS.Excecao;

namespace SWS.Dao
{
    public class AsoAgenteRiscoDao :IAsoAgenteRiscoDao
    {
        public AsoAgenteRisco insert(AsoAgenteRisco asoAgenteRisco, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" INSERT INTO SEG_ASO_AGENTE_RISCO(ID_ASO, ID_AGENTE, DESCRICAO_AGENTE, ID_RISCO, DESCRICAO_RISCO) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5 )");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = asoAgenteRisco.IdAso;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = asoAgenteRisco.IdAgente;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = asoAgenteRisco.DescricaoAgente;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = asoAgenteRisco.IdRisco;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = asoAgenteRisco.DescricaoRisco;
                
                command.ExecuteNonQuery();

                return asoAgenteRisco;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<AsoAgenteRisco> findAllByAtendimento(Aso atendimento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByAtendimento");

            try
            {
                StringBuilder query = new StringBuilder();

                List<AsoAgenteRisco> asoAgenteRiscoInAtendimento = new List<AsoAgenteRisco>();

                query.Append(" SELECT ID_ASO, ID_AGENTE, DESCRICAO_AGENTE, ID_RISCO, DESCRICAO_RISCO ");
                query.Append(" FROM SEG_ASO_AGENTE_RISCO ");
                query.Append(" WHERE ID_ASO = :VALUE1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = atendimento.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    asoAgenteRiscoInAtendimento.Add(new AsoAgenteRisco(atendimento.Id, dr.GetInt64(1), dr.GetString(2), dr.GetInt64(3), dr.GetString(4)));
                }

                return asoAgenteRiscoInAtendimento;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByAtendimento", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void delete(AsoAgenteRisco asoAgenteRisco, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" DELETE FROM SEG_ASO_AGENTE_RISCO WHERE ID_ASO =:VALUE1 AND ID_AGENTE = :VALUE2 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = asoAgenteRisco.IdAso;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = asoAgenteRisco.IdAgente;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
