﻿using SWS.Entidade;
using Npgsql;
using SWS.Excecao;
using System;
using NpgsqlTypes;
using System.Data;
using System.Text;
using SWS.IDao;
using SWS.Helper;
using SWS.View.Resources;

namespace SWS.Dao
{
    class AtividadeDao : IAtividadeDao
    {
        public Atividade insert(Atividade atividade, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            try
            {
                atividade.Id = recuperaProximoId(dbConnection);
                
                NpgsqlCommand command = new NpgsqlCommand("INSERT INTO SEG_ATIVIDADE (ID_ATIVIDADE, DESCRICAO, SITUACAO) VALUES (:VALUE1, :VALUE2, 'A')", dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = atividade.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = atividade.Descricao;

                command.ExecuteNonQuery();

                return atividade;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Atividade update(Atividade atividade, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("UPDATE SEG_ATIVIDADE SET DESCRICAO = :VALUE2, SITUACAO = :VALUE3 WHERE ID_ATIVIDADE = :VALUE1" , dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = atividade.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = atividade.Descricao;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = atividade.Situacao;
                
                command.ExecuteNonQuery();

                return atividade;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_ATIVIDADE_ID_ATIVIDADE_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Atividade findByDescricao(String descricao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByDescricao");

            Atividade atividadeRetorno = null;
           
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT ID_ATIVIDADE, DESCRICAO, SITUACAO FROM SEG_ATIVIDADE WHERE DESCRICAO = :VALUE1 ", dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = descricao.ToUpper();

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    atividadeRetorno = new Atividade(dr.GetInt64(0), dr.GetString(1), dr.GetString(2));
                }

                return atividadeRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByDescricao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findByAtividade(Atividade atividade, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByAtividade");

            StringBuilder query = new StringBuilder();
            
            try
            {
                if (atividade.isNullForFilter())
                   query.Append ("SELECT ID_ATIVIDADE, DESCRICAO FROM SEG_ATIVIDADE WHERE SITUACAO = :VALUE1 ");
                else
                    query.Append ("SELECT ID_ATIVIDADE, DESCRICAO FROM SEG_ATIVIDADE WHERE SITUACAO = :VALUE1 AND DESCRICAO LIKE :VALUE2 ");
                
                query.Append(" ORDER BY DESCRICAO");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[0].Value = atividade.Situacao;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[1].Value = atividade.Descricao + "%";
                
                DataSet ds = new DataSet();

                adapter.Fill(ds, "Atividades");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByAtividade", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findByFilter(Atividade atividade, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByFilter");

            try
            {
                StringBuilder query = new StringBuilder();

                if (atividade.isNullForFilter())
                    query.Append("SELECT ID_ATIVIDADE, DESCRICAO, SITUACAO FROM SEG_ATIVIDADE ");
                else
                {
                    query.Append("SELECT ID_ATIVIDADE, DESCRICAO, SITUACAO FROM SEG_ATIVIDADE WHERE TRUE");

                    if (!String.IsNullOrWhiteSpace(atividade.Descricao))
                        query.Append (" AND DESCRICAO LIKE :VALUE1 ");

                    if (!String.IsNullOrWhiteSpace(atividade.Situacao))
                        query.Append (" AND SITUACAO = :VALUE2 ");
                }

                query.Append(" ORDER BY DESCRICAO ASC");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[0].Value = atividade.Descricao + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[1].Value = atividade.Situacao;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Atividades");

                return ds;
            }
            catch (NpgsqlException ex)
             {
                 LogHelper.logger.Error(this.GetType().Name + " - findByFilter", ex);
                 throw new DataBaseConectionException(ex.Message);
             }
        }

        public Atividade findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            Atividade atividadeRetorno = null;
            
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT ID_ATIVIDADE, DESCRICAO, SITUACAO FROM SEG_ATIVIDADE WHERE ID_ATIVIDADE = :VALUE1", dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    atividadeRetorno = new Atividade (dr.GetInt64(0), dr.GetString(1), dr.GetString(2));
                }
                
                return atividadeRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findByAtividadeAndCronograma(Atividade atividade, Cronograma cronograma, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByAtividadeAndCronograma");

            StringBuilder query = new StringBuilder();
            
            try
            {
                if (String.IsNullOrWhiteSpace(atividade.Descricao))
                {
                    query.Append(" SELECT ATIVIDADE.ID_ATIVIDADE, ATIVIDADE.DESCRICAO, ATIVIDADE.SITUACAO FROM SEG_ATIVIDADE ATIVIDADE WHERE ATIVIDADE.SITUACAO = :VALUE1 AND ");
                    query.Append(" ATIVIDADE.ID_ATIVIDADE NOT IN (SELECT ID_ATIVIDADE FROM SEG_ATIVIDADE_CRONOGRAMA WHERE ID_CRONOGRAMA = :VALUE3) ");
                    query.Append(" ORDER BY ATIVIDADE.DESCRICAO ");
                   
                }
                else
                {
                    query.Append(" SELECT ATIVIDADE.ID_ATIVIDADE, ATIVIDADE.DESCRICAO, ATIVIDADE.SITUACAO FROM SEG_ATIVIDADE ATIVIDADE WHERE ATIVIDADE.SITUACAO = :VALUE1 AND ATIVIDADE.DESCRICAO LIKE :VALUE2 AND ");
                    query.Append(" ATIVIDADE.ID_ATIVIDADE NOT IN ( SELECT ID_ATIVIDADE FROM SEG_ATIVIDADE_CRONOGRAMA WHERE ID_CRONOGRAMA = :VALUE3) ");
                    query.Append(" ORDER BY ATIVIDADE.DESCRICAO");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = atividade.Situacao;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = atividade.Descricao + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[2].Value = cronograma.Id;
                
                DataSet ds = new DataSet();

                adapter.Fill(ds, "Atividades");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByAtividadeAndCronograma", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        
        }

    }
}