﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using Npgsql;
using NpgsqlTypes;
using SWS.View.Resources;
using SWS.Excecao;
using System.Data;
using SWS.Helper;


namespace SWS.Dao
{
    class ClienteFuncaoDao : IClienteFuncaoDao
    {

        public ClienteFuncao insert(ClienteFuncao clientefuncao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            StringBuilder query = new StringBuilder();

            try
            {
                clientefuncao.Id = recuperaProximoId(dbConnection);
                
                query.Append(" INSERT INTO SEG_CLIENTE_FUNCAO (ID_SEG_CLIENTE_FUNCAO, ID_CLIENTE, ");
                query.Append(" ID_FUNCAO, SITUACAO, DATA_CADASTRO) VALUES ");
                query.Append(" (:VALUE1, :VALUE2, :VALUE3, 'A', NOW())");

                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clientefuncao.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = clientefuncao.Cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = clientefuncao.Funcao != null ? clientefuncao.Funcao.Id : null;

                command.ExecuteNonQuery();

                return clientefuncao;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public DataSet findAtivosByCliente(Cliente cliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtivosByCliente");
            
            try
            {
                StringBuilder query = new StringBuilder();
                
                query.Append(" SELECT CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO, CLIENTE_FUNCAO.ID_CLIENTE, CLIENTE_FUNCAO.ID_FUNCAO, FUNCAO.DESCRICAO AS FUNCAO, FUNCAO.COD_CBO FROM SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ");
                query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTE_FUNCAO.ID_FUNCAO ");
                query.Append(" WHERE");
                query.Append(" CLIENTE_FUNCAO.ID_CLIENTE = :VALUE1 AND CLIENTE_FUNCAO.SITUACAO = 'A' ");
                query.Append(" ORDER BY FUNCAO.DESCRICAO ");
                
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = cliente.Id;
                
                DataSet ds = new DataSet();

                adapter.Fill(ds, "Funcoes");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtivosByCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void update(ClienteFuncao clientefuncao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_CLIENTE_FUNCAO SET SITUACAO = :VALUE2, DATA_DESLIGAMENTO = :VALUE3, DATA_CADASTRO = :VALUE4 ");
                query.Append(" WHERE ID_SEG_CLIENTE_FUNCAO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clientefuncao.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = clientefuncao.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Date));
                command.Parameters[2].Value = clientefuncao.DataExclusao;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Date));
                command.Parameters[3].Value = clientefuncao.DataCadastro;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAtivosByClienteFuncao(ClienteFuncao clienteFuncao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtivosByClienteFuncao");

            try
            {
                StringBuilder query = new StringBuilder();

                // testando para saber se algum campo da função foi selecionado.
                if (String.IsNullOrWhiteSpace(clienteFuncao.Funcao.Descricao) &&
                    String.IsNullOrWhiteSpace(clienteFuncao.Funcao.CodCbo))
                {

                    query.Append(" SELECT CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO, CLIENTE_FUNCAO.ID_FUNCAO, FUNCAO.DESCRICAO, ");
                    query.Append(" FUNCAO.COD_CBO FROM SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO" );
                    query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTE_FUNCAO.ID_FUNCAO ");
                    query.Append(" WHERE CLIENTE_FUNCAO.ID_CLIENTE = :VALUE1 AND CLIENTE_FUNCAO.SITUACAO = 'A' ");
                    query.Append(" ORDER BY FUNCAO.DESCRICAO ");

                }
                else
                {
                    query.Append(" SELECT CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO, CLIENTE_FUNCAO.ID_FUNCAO, FUNCAO.DESCRICAO, ");
                    query.Append(" FUNCAO.COD_CBO FROM SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ");
                    query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTE_FUNCAO.ID_FUNCAO ");
                    query.Append(" WHERE CLIENTE_FUNCAO.ID_CLIENTE = :VALUE1 AND CLIENTE_FUNCAO.SITUACAO = 'A' ");

                    if (!String.IsNullOrWhiteSpace(clienteFuncao.Funcao.Descricao))
                        query.Append(" AND FUNCAO.DESCRICAO LIKE :VALUE2 ");

                    if (!String.IsNullOrWhiteSpace(clienteFuncao.Funcao.CodCbo))
                        query.Append(" AND FUNCAO.COD_CBO = :VALUE3 ");

                    query.Append(" ORDER BY FUNCAO.DESCRICAO ASC ");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = clienteFuncao.Cliente.Id;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = clienteFuncao.Funcao.Descricao + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[2].Value = clienteFuncao.Funcao.CodCbo;
                
                DataSet ds = new DataSet();

                adapter.Fill(ds, "Funcoes");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtivosByClienteFuncao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAtivoByClienteFuncaoAndGheSetorNotInGheSetor(ClienteFuncao clienteFuncao, GheSetor gheSetor, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtivoByClienteFuncaoAndGheSetorNotInGheSetor");

            try
            {
                StringBuilder query = new StringBuilder();

                if (String.IsNullOrEmpty(clienteFuncao.Funcao.Descricao))
                {

                    query.Append(" SELECT CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO, CLIENTE_FUNCAO.ID_FUNCAO, FUNCAO.DESCRICAO, ");
                    query.Append(" FUNCAO.COD_CBO FROM SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ");
                    query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTE_FUNCAO.ID_FUNCAO ");
                    query.Append(" WHERE CLIENTE_FUNCAO.ID_CLIENTE = :VALUE1 AND CLIENTE_FUNCAO.SITUACAO = 'A' ");
                    query.Append(" AND CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO NOT IN (SELECT GSCF.ID_SEG_CLIENTE_FUNCAO FROM SEG_GHE_SETOR_CLIENTE_FUNCAO GSCF WHERE GSCF.ID_GHE_SETOR = :VALUE3 AND GSCF.SITUACAO = 'A') ");
                    query.Append(" ORDER BY FUNCAO.DESCRICAO ");

                }
                else
                {
                    query.Append(" SELECT CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO, CLIENTE_FUNCAO.ID_FUNCAO, FUNCAO.DESCRICAO, ");
                    query.Append(" FUNCAO.COD_CBO FROM SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO");
                    query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTE_FUNCAO.ID_FUNCAO ");
                    query.Append(" WHERE CLIENTE_FUNCAO.ID_CLIENTE = :VALUE1 AND CLIENTE_FUNCAO.SITUACAO = 'A' ");

                    if (!String.IsNullOrWhiteSpace(clienteFuncao.Funcao.Descricao))
                        query.Append(" AND FUNCAO.DESCRICAO LIKE :VALUE2 ");

                    query.Append(" AND CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO NOT IN (SELECT GSCF.ID_SEG_CLIENTE_FUNCAO FROM SEG_GHE_SETOR_CLIENTE_FUNCAO GSCF WHERE GSCF.ID_GHE_SETOR = :VALUE3 AND GSCF.SITUACAO = 'A') ");

                    query.Append(" ORDER BY FUNCAO.DESCRICAO ");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = clienteFuncao.Cliente.Id;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = "%" + clienteFuncao.Funcao.Descricao + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[2].Value = gheSetor.Id;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Funcoes");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtivoByClienteFuncaoAndGheSetorNotInGheSetor", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL('SEG_CLIENTE_FUNCAO_ID_SEG_CLIENTE_FUNCAO_SEQ')", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean funcaoIsUsedInEstudo(Funcao funcao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método funcaoIsUsedInEstudo");

            Boolean isClienteFuncaoInUsed = false;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT COUNT(*) FROM SEG_FUNCAO FUNCAO ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_FUNCAO = FUNCAO.ID_FUNCAO ");
                query.Append(" JOIN SEG_GHE_SETOR_CLIENTE_FUNCAO GHE_SETOR_CLIENTE_FUNCAO ON GHE_SETOR_CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO ");
                query.Append(" JOIN SEG_GHE_SETOR GHE_SETOR ON GHE_SETOR.ID_GHE_SETOR = GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR ");
                query.Append(" JOIN SEG_GHE GHE ON GHE.ID_GHE = GHE_SETOR.ID_GHE ");
                query.Append(" JOIN SEG_ESTUDO ESTUDO ON ESTUDO.ID_ESTUDO = GHE.ID_ESTUDO ");
                query.Append(" WHERE FUNCAO.ID_FUNCAO = :VALUE1 AND CLIENTE_FUNCAO.SITUACAO = 'A' AND ESTUDO.SITUACAO = 'F'");
                query.Append(" AND ESTUDO.AVULSO = FALSE ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = funcao.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    if (dr.GetInt64(0) > 0)
                    {
                        isClienteFuncaoInUsed = true;
                    }
                }

                return isClienteFuncaoInUsed;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - funcaoIsUsedInEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public ClienteFuncao findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            try
            {
                StringBuilder query = new StringBuilder();

                ClienteFuncao clienteFuncao = null;
                
                query.Append(" SELECT ID_SEG_CLIENTE_FUNCAO, ID_CLIENTE, ID_FUNCAO, SITUACAO, DATA_CADASTRO, DATA_DESLIGAMENTO ");
                query.Append(" FROM SEG_CLIENTE_FUNCAO WHERE ID_SEG_CLIENTE_FUNCAO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    clienteFuncao = new ClienteFuncao(dr.GetInt64(0), new Cliente(dr.GetInt64(1)),
                        new Funcao(dr.GetInt64(2), null, null, null, null), dr.GetString(3), dr.GetDateTime(4), !dr.IsDBNull(5) ? dr.GetDateTime(5) : (DateTime?)null);
                    
                }

                return clienteFuncao;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public Boolean IsPresentInPcmso(Estudo pcmso, ClienteFuncao clienteFuncao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método IsClienteFuncaoPresentPcmso");

            Boolean isClienteFuncaoInEstudo = false;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT COUNT(*) FROM SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ");
                query.Append(" JOIN SEG_GHE_SETOR_CLIENTE_FUNCAO GHE_SETOR_CLIENTE_FUNCAO ON GHE_SETOR_CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO AND GHE_SETOR_CLIENTE_FUNCAO.SITUACAO = 'A' AND CLIENTE_FUNCAO.SITUACAO = 'A' ");
                query.Append(" JOIN SEG_GHE_SETOR GHE_SETOR ON GHE_SETOR.ID_GHE_SETOR = GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR AND GHE_SETOR.SITUACAO = 'A' ");
                query.Append(" JOIN SEG_GHE GHE ON GHE.ID_GHE = GHE_SETOR.ID_GHE AND GHE.SITUACAO = 'A' ");
                query.Append(" WHERE CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = :VALUE1 AND GHE.ID_ESTUDO = :VALUE2 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncao.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = pcmso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    if (dr.GetInt64(0) > 0)
                    {
                        isClienteFuncaoInEstudo = true;
                    }
                }

                return isClienteFuncaoInEstudo;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - IsClienteFuncaoPresentPcmso", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public List<ClienteFuncao> findAtivosByClienteList(Cliente cliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtivosByClienteList");

            try
            {
                StringBuilder query = new StringBuilder();

                List<ClienteFuncao> funcaoCliente = new List<ClienteFuncao>();

                query.Append(" SELECT CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO, CLIENTE_FUNCAO.ID_CLIENTE, CLIENTE_FUNCAO.ID_FUNCAO, FUNCAO.DESCRICAO, FUNCAO.COD_CBO, ");
                query.Append(" FUNCAO.SITUACAO, FUNCAO.COMENTARIO, CLIENTE_FUNCAO.SITUACAO, CLIENTE_FUNCAO.DATA_CADASTRO, CLIENTE_FUNCAO.DATA_DESLIGAMENTO ");
                query.Append(" FROM SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ");
                query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTE_FUNCAO.ID_FUNCAO ");
                query.Append(" WHERE CLIENTE_FUNCAO.SITUACAO = 'A' AND CLIENTE_FUNCAO.ID_CLIENTE = :VALUE1 ");
                query.Append(" ORDER BY FUNCAO.DESCRICAO ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    funcaoCliente.Add(new ClienteFuncao(dr.GetInt64(0), cliente, new Funcao(dr.GetInt64(2),
                        dr.GetString(3), dr.IsDBNull(4) ? string.Empty : dr.GetString(4), dr.GetString(5), dr.GetString(6)), dr.GetString(7), dr.IsDBNull(8) ? (DateTime?)null : dr.GetDateTime(8),
                        dr.IsDBNull(9) ? (DateTime?)null : dr.GetDateTime(9)));
                }
                
                return funcaoCliente;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtivosByClienteList", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public LinkedList<ClienteFuncao> findAllByFuncaoAndCliente(Funcao funcao, Cliente cliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByFuncaoAndCliente");

            try
            {
                StringBuilder query = new StringBuilder();

                LinkedList<ClienteFuncao> funcaoCliente = new LinkedList<ClienteFuncao>();

                query.Append(" SELECT CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO, CLIENTE_FUNCAO.ID_CLIENTE, CLIENTE_FUNCAO.ID_FUNCAO, CLIENTE_FUNCAO.SITUACAO, CLIENTE_FUNCAO.DATA_CADASTRO, CLIENTE_FUNCAO.DATA_DESLIGAMENTO,  ");
                query.Append(" FUNCAO.DESCRICAO, FUNCAO.COD_CBO, FUNCAO.SITUACAO, FUNCAO.COMENTARIO ");
                query.Append(" FROM SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO  ");
                query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTE_FUNCAO.ID_FUNCAO ");
                query.Append(" JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = CLIENTE_FUNCAO.ID_CLIENTE ");
                query.Append(" WHERE  ");
                query.Append(" CLIENTE.ID_CLIENTE = :VALUE1  ");
                query.Append(" AND CLIENTE_FUNCAO.ID_FUNCAO = :VALUE2 ");
                query.Append(" ORDER BY CLIENTE_FUNCAO.DATA_CADASTRO ASC ");


                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = funcao.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {

                    funcaoCliente.AddFirst(new ClienteFuncao(dr.GetInt64(0), cliente, new Funcao(dr.GetInt64(0), dr.GetString(6), dr.GetString(7), 
                        dr.GetString(8), dr.GetString(9)), dr.GetString(3), dr.GetDateTime(4), dr.IsDBNull(5) ? null : (DateTime?)dr.GetDateTime(5)));

                }

                return funcaoCliente;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByFuncaoAndCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

    }
}