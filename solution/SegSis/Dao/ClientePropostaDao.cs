﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;
using NpgsqlTypes;


namespace SWS.Dao
{
    class ClientePropostaDao : IClientePropostaDao
    {
        public ClienteProposta insertClienteProposta(ClienteProposta clienteProposta, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertClienteProposta");

            try
            {
                clienteProposta.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_CLIENTE_PROPOSTA (ID_CLIENTE_PROPOSTA, ID_CIDADE_IBGE, RAZAO_SOCIAL, NOME_FANTASIA, CNPJ, INSCRICAO, ");
                query.Append(" ENDERECO, NUMERO, COMPLEMENTO, BAIRRO, CEP, UF, ");
                query.Append(" TELEFONE, CELULAR, DATA_CADASTRO, CONTATO, EMAIL ) VALUES ( :VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6, :VALUE7, "); 
                query.Append(" :VALUE8, :VALUE9, :VALUE10, :VALUE11, :VALUE12, :VALUE13, :VALUE14, :VALUE15, :VALUE16, :VALUE17 ) "); 

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteProposta.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = clienteProposta.Cidade.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = clienteProposta.RazaoSocial;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = clienteProposta.NomeFantasia;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = clienteProposta.Cnpj;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = clienteProposta.Inscricao;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = clienteProposta.Endereco;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Varchar));
                command.Parameters[7].Value = clienteProposta.Numero;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                command.Parameters[8].Value = clienteProposta.Complemento;

                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                command.Parameters[9].Value = clienteProposta.Bairro;

                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Varchar));
                command.Parameters[10].Value = clienteProposta.Cep;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Varchar));
                command.Parameters[11].Value = clienteProposta.Uf;

                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Varchar));
                command.Parameters[12].Value = clienteProposta.Telefone;

                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Varchar));
                command.Parameters[13].Value = clienteProposta.Celular;

                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Timestamp));
                command.Parameters[14].Value = clienteProposta.DataCadastro;

                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Varchar));
                command.Parameters[15].Value = clienteProposta.Contato;

                command.Parameters.Add(new NpgsqlParameter("VALUE17", NpgsqlDbType.Varchar));
                command.Parameters[16].Value = clienteProposta.Email;

                command.ExecuteNonQuery();

                return clienteProposta;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertClienteProposta", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_CLIENTE_PROPOSTA_ID_CLIENTE_PROPOSTA_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public ClienteProposta findClientePropostaByContrato(Contrato contrato, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClientePropostaByContrato");

            StringBuilder query = new StringBuilder();

            ClienteProposta clienteProposta = null;

            try
            {
                query.Append(" SELECT CLIENTE_PROPOSTA.ID_CLIENTE_PROPOSTA, CLIENTE_PROPOSTA.ID_CIDADE_IBGE, CLIENTE_PROPOSTA.RAZAO_SOCIAL, CLIENTE_PROPOSTA.NOME_FANTASIA, CLIENTE_PROPOSTA.CNPJ, CLIENTE_PROPOSTA.INSCRICAO, CLIENTE_PROPOSTA.ENDERECO, ");
                query.Append(" CLIENTE_PROPOSTA.NUMERO, CLIENTE_PROPOSTA.COMPLEMENTO, CLIENTE_PROPOSTA.BAIRRO, CLIENTE_PROPOSTA.CEP, CLIENTE_PROPOSTA.UF, CLIENTE_PROPOSTA.TELEFONE, CLIENTE_PROPOSTA.CELULAR, CLIENTE_PROPOSTA.DATA_CADASTRO, CLIENTE_PROPOSTA.CONTATO, CLIENTE_PROPOSTA.EMAIL, ");
                query.Append(" CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE ");
                query.Append(" FROM SEG_CLIENTE_PROPOSTA CLIENTE_PROPOSTA ");
                query.Append(" JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = CLIENTE_PROPOSTA.ID_CIDADE_IBGE ");
                query.Append(" JOIN SEG_CONTRATO CONTRATO ON CONTRATO.ID_CLIENTE_PROPOSTA = CLIENTE_PROPOSTA.ID_CLIENTE_PROPOSTA ");
                query.Append(" WHERE CONTRATO.ID_CONTRATO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = contrato.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {

                    clienteProposta = new ClienteProposta(dr.GetInt64(0),
                        dr.GetString(2),
                        dr.IsDBNull(3) ? String.Empty : dr.GetString(3),
                        dr.IsDBNull(4) ? String.Empty : dr.GetString(4),
                        dr.IsDBNull(5) ? String.Empty : dr.GetString(5),
                        dr.GetString(6),
                        dr.GetString(7),
                        dr.IsDBNull(8) ? String.Empty : dr.GetString(8),
                        dr.GetString(9),
                        dr.IsDBNull(10) ? String.Empty : dr.GetString(10),
                        dr.IsDBNull(1) ? null : new CidadeIbge(dr.GetInt64(1), dr.GetString(17), dr.GetString(18), dr.GetString(19)),
                        dr.IsDBNull(11) ? String.Empty : dr.GetString(11),
                        dr.IsDBNull(12) ? String.Empty : dr.GetString(12),
                        dr.IsDBNull(13) ? String.Empty : dr.GetString(13),
                        dr.IsDBNull(14) ? null : (DateTime?)dr.GetDateTime(14),
                        dr.IsDBNull(15) ? String.Empty : dr.GetString(15),
                        dr.IsDBNull(16) ? String.Empty : dr.GetString(16));
                    
                }

                return clienteProposta;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClientePropostaByContrato", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
