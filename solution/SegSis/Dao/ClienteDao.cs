﻿using SWS.Entidade;
using Npgsql;
using SWS.Excecao;
using System;
using NpgsqlTypes;
using System.Data;
using System.Text;
using SWS.IDao;
using SWS.Helper;
using System.Collections;
using System.Collections.Generic;
using SWS.View.Resources;

namespace SWS.Dao
{
    class ClienteDao : IClienteDao
    {
        public DataSet findClienteByFilter(Cliente cliente, Boolean? prestador, Boolean? credenciada, Boolean? unidade, DateTime? dtCadastroFinal, Boolean? aptoContrato, bool? particular, Usuario usuarioLogado, bool? credenciadora, bool? fisica, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteByFilter");

            try
            {
                StringBuilder query = new StringBuilder();
                
                if (cliente.isNullForFilter())
                {
                    query.Append(" SELECT CLIENTE.ID_CLIENTE, CLIENTE.RAZAO_SOCIAL, CLIENTE.FANTASIA, (CASE WHEN LENGTH(CLIENTE.CNPJ) = 11 THEN FORMATA_CPF(CLIENTE.CNPJ) ELSE FORMATA_CNPJ(CLIENTE.CNPJ) END) AS CNPJ, CLIENTE.INSCRICAO, CLIENTE.ENDERECO, CLIENTE.NUMERO, CLIENTE.COMPLEMENTO, CLIENTE.BAIRRO, CIDADE.NOME AS CIDADE, FORMATA_CEP(CLIENTE.CEP) AS CEP, CLIENTE.UF, ");
                    query.Append(" CLIENTE.TELEFONE1, CLIENTE.TELEFONE2, CLIENTE.EMAIL, CLIENTE.SITE, CLIENTE.DATA_CADASTRO, CLIENTE.RESPONSAVEL, CLIENTE.CARGO, CLIENTE.DOCUMENTO, CLIENTE.ESCOPO, CLIENTE.JORNADA, CLIENTE.SITUACAO, CLIENTE.EST_MASC, ");
                    query.Append(" CLIENTE.EST_FEM, CLIENTE.ENDERECOCOB, CLIENTE.NUMEROCOB, CLIENTE.COMPLEMENTOCOB, CLIENTE.BAIRROCOB, FORMATA_CEP(CLIENTE.CEPCOB) AS CEPCOBRANCA, CIDADE_COBRANCA.NOME AS CIDADECOBRANCA, CLIENTE.UFCOB, CLIENTE.TELEFONECOB, CLIENTE.TELEFONE2COB, MEDICO.NOME AS MEDICOCOORDENADOR, CLIENTE.VIP, CLIENTE.TELEFONE_CONTATO, ");
                    query.Append(" CLIENTE.USA_CONTRATO, CLIENTE.PARTICULAR, CLIENTE.UNIDADE, CLIENTE.CREDENCIADA, CLIENTE.PRESTADOR, CLIENTE.DESTACA_ISS, CLIENTE.GERA_COBRANCA_VALOR_LIQUIDO, CLIENTE.ALIQUOTA_ISS ");
                    query.Append(" FROM SEG_CLIENTE CLIENTE ");
                    query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = CLIENTE.ID_CIDADE_IBGE ");
                    query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE_COBRANCA ON CIDADE_COBRANCA.ID_CIDADE_IBGE = CLIENTE.ID_CIDADE_IBGE_COBRANCA ");
                    query.Append(" LEFT JOIN SEG_MEDICO MEDICO ON MEDICO.ID_MEDICO = CLIENTE.ID_MEDICO ");
                    
                    // verificando se algum parametro foi passado para pesquisar tipo de cliente

                    if (prestador != null || credenciada != null || unidade != null || particular != null || credenciadora != null || fisica != null)
                    {
                        query.Append(" WHERE TRUE ");

                        if (prestador != null)
                            query.Append(" AND CLIENTE.PRESTADOR = :VALUE9 ");

                        if (credenciada != null)
                            query.Append(" AND CLIENTE.CREDENCIADA = :VALUE10 ");

                        if (unidade != null)
                            query.Append(" AND CLIENTE.UNIDADE = :VALUE11 ");

                        if (aptoContrato != null)
                            query.Append(" AND CLIENTE.USA_CONTRATO = :VALUE12");

                        if (particular != null)
                            query.Append(" AND CLIENTE.PARTICULAR = :VALUE13");

                        if (credenciadora != null)
                            query.Append(" AND CLIENTE.CREDENCIADORA = :VALUE14");

                        if (fisica != null)
                            query.Append(" AND CLIENTE.FISICA = :VALUE15");

                        /* verificando se o usuário tem restrinção para pesquisa */
                        if (usuarioLogado.UsuarioCliente.Count > 1)
                        {
                            query.Append(" AND CLIENTE.ID_CLIENTE IN ( ");

                            int index = 0;
                            foreach (UsuarioCliente usuarioCliente in usuarioLogado.UsuarioCliente)
                            {
                                query.Append(usuarioCliente.Cliente.Id);
                                index++;
                                if (usuarioLogado.UsuarioCliente.Count == index)
                                    query.Append(")");
                                else
                                    query.Append(",");
                            }
                        }
                    }
                    
                }
                else
                {
                    query.Append(" SELECT CLIENTE.ID_CLIENTE, CLIENTE.RAZAO_SOCIAL, CLIENTE.FANTASIA, (CASE WHEN LENGTH(CLIENTE.CNPJ) = 11 THEN FORMATA_CPF(CLIENTE.CNPJ) ELSE FORMATA_CNPJ(CLIENTE.CNPJ) END) AS CNPJ, CLIENTE.INSCRICAO, CLIENTE.ENDERECO, CLIENTE.NUMERO, CLIENTE.COMPLEMENTO, CLIENTE.BAIRRO, CIDADE.NOME AS CIDADE, FORMATA_CEP(CLIENTE.CEP) AS CEP, CLIENTE.UF, ");
                    query.Append(" CLIENTE.TELEFONE1, CLIENTE.TELEFONE2, CLIENTE.EMAIL, CLIENTE.SITE, CLIENTE.DATA_CADASTRO, CLIENTE.RESPONSAVEL, CLIENTE.CARGO, CLIENTE.DOCUMENTO, CLIENTE.ESCOPO, CLIENTE.JORNADA, CLIENTE.SITUACAO, CLIENTE.EST_MASC, ");
                    query.Append(" CLIENTE.EST_FEM, CLIENTE.ENDERECOCOB, CLIENTE.NUMEROCOB, CLIENTE.COMPLEMENTOCOB, CLIENTE.BAIRROCOB, FORMATA_CEP(CLIENTE.CEPCOB) AS CEPCOBRANCA, CIDADE_COBRANCA.NOME AS CIDADECOBRANCA, CLIENTE.UFCOB, CLIENTE.TELEFONECOB, CLIENTE.TELEFONE2COB, MEDICO.NOME AS MEDICOCOORDENADOR, CLIENTE.VIP, CLIENTE.TELEFONE_CONTATO, ");
                    query.Append(" CLIENTE.USA_CONTRATO, CLIENTE.PARTICULAR, CLIENTE.UNIDADE, CLIENTE.CREDENCIADA, CLIENTE.PRESTADOR, CLIENTE.DESTACA_ISS, CLIENTE.GERA_COBRANCA_VALOR_LIQUIDO, CLIENTE.ALIQUOTA_ISS ");
                    query.Append(" FROM SEG_CLIENTE CLIENTE ");
                    query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = CLIENTE.ID_CIDADE_IBGE ");
                    query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE_COBRANCA ON CIDADE_COBRANCA.ID_CIDADE_IBGE = CLIENTE.ID_CIDADE_IBGE_COBRANCA ");
                    query.Append(" LEFT JOIN SEG_MEDICO MEDICO ON MEDICO.ID_MEDICO = CLIENTE.ID_MEDICO ");
                    query.Append(" WHERE TRUE ");

                    if (!String.IsNullOrWhiteSpace(cliente.RazaoSocial))
                        query.Append(" AND CLIENTE.RAZAO_SOCIAL LIKE :VALUE1 ");

                    if (!String.IsNullOrWhiteSpace(cliente.Fantasia))
                        query.Append(" AND CLIENTE.FANTASIA LIKE :VALUE2 ");

                    if (!String.IsNullOrWhiteSpace(cliente.Cnpj))
                        query.Append(" AND CLIENTE.CNPJ = :VALUE3 ");

                    if (cliente.CidadeIbge != null)
                        query.Append(" AND CLIENTE.ID_CIDADE_IBGE = :VALUE4 ");

                    if (!String.IsNullOrWhiteSpace(cliente.Uf))
                        query.Append(" AND CLIENTE.UF  = :VALUE5 ");

                    if (!String.IsNullOrWhiteSpace(Convert.ToString(cliente.DataCadastro)))
                        query.Append(" AND CLIENTE.DATA_CADASTRO BETWEEN :VALUE6 AND :VALUE7 ");

                    if (!String.IsNullOrWhiteSpace(cliente.Situacao))
                        query.Append(" AND CLIENTE.SITUACAO = :VALUE8 ");

                    // alteracao para inclusao dos flags para selecionar os tipos de clientes
                    // 2014-08-23 - Welbert Santos

                    if (prestador != null)
                        query.Append(" AND CLIENTE.PRESTADOR = :VALUE9 ");

                    if (credenciada != null)
                        query.Append(" AND CLIENTE.CREDENCIADA = :VALUE10 ");

                    if (unidade != null)
                        query.Append(" AND CLIENTE.UNIDADE = :VALUE11 ");

                    if (aptoContrato != null)
                        query.Append(" AND CLIENTE.USA_CONTRATO = :VALUE12");

                    if (particular != null)
                        query.Append(" AND CLIENTE.PARTICULAR = :VALUE13");

                    if (credenciadora != null)
                        query.Append(" AND CLIENTE.CREDENCIADORA = :VALUE14");

                    if (fisica != null)
                        query.Append(" AND CLIENTE.FISICA = :VALUE15 ");

                    /* verificando se o usuário tem restrinção para pesquisa */
                    if (usuarioLogado.UsuarioCliente.Count > 1)
                    {
                        query.Append(" AND CLIENTE.ID_CLIENTE IN ( ");

                        int index = 0;
                        foreach (UsuarioCliente usuarioCliente in usuarioLogado.UsuarioCliente)
                        {
                            query.Append(usuarioCliente.Cliente.Id);
                            index++;
                            if (usuarioLogado.UsuarioCliente.Count == index)
                                query.Append(")");
                            else
                                query.Append(",");
                        }
                    }
                
                }

                query.Append(" ORDER BY CLIENTE.RAZAO_SOCIAL ASC");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[0].Value = String.IsNullOrEmpty(cliente.RazaoSocial) ? String.Empty : "%" + cliente.RazaoSocial + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[1].Value = String.IsNullOrEmpty(cliente.Fantasia) ? String.Empty : cliente.Fantasia + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[2].Value = String.IsNullOrEmpty(cliente.Cnpj) ? String.Empty : cliente.Cnpj;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[3].Value = cliente.CidadeIbge != null ? cliente.CidadeIbge.Id : null;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[4].Value = cliente.Uf != String.Empty ? cliente.Uf : null;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Date));
                adapter.SelectCommand.Parameters[5].Value = cliente.DataCadastro;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Date));
                adapter.SelectCommand.Parameters[6].Value = dtCadastroFinal;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[7].Value = cliente.Situacao;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Boolean));
                adapter.SelectCommand.Parameters[8].Value = prestador != null ? prestador : null;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Boolean));
                adapter.SelectCommand.Parameters[9].Value = credenciada != null ? credenciada : null;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Boolean));
                adapter.SelectCommand.Parameters[10].Value = unidade != null ? unidade : null;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Boolean));
                adapter.SelectCommand.Parameters[11].Value = aptoContrato != null ? aptoContrato : null;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Boolean));
                adapter.SelectCommand.Parameters[12].Value = particular != null ? particular : null;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Boolean));
                adapter.SelectCommand.Parameters[13].Value = credenciadora != null ? credenciadora : null;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Boolean));
                adapter.SelectCommand.Parameters[14].Value = fisica != null ? fisica : null;

                               
                DataSet ds = new DataSet();
                adapter.Fill(ds, "Clientes");
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Cliente findClienteByCnpj(String cnpj, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteByCnpj");

            Cliente clienteRetorno = null;

            StringBuilder query = new StringBuilder();
      
            try
            {
                query.Append(" SELECT C.ID_CLIENTE, C.RAZAO_SOCIAL, C.FANTASIA, C.CNPJ, C.INSCRICAO, C.ENDERECO,  ");
                query.Append(" C.NUMERO, C.COMPLEMENTO, C.BAIRRO, C.CEP, C.UF, C.TELEFONE1, C.TELEFONE2, C.EMAIL,  ");
                query.Append(" C.SITE, C.DATA_CADASTRO, C.RESPONSAVEL, C.CARGO, C.DOCUMENTO, C.ESCOPO, C.JORNADA, C.SITUACAO,  ");
                query.Append(" C.EST_MASC, C.EST_FEM, C.ENDERECOCOB, C.NUMEROCOB, C.COMPLEMENTOCOB, C.BAIRROCOB, C.CEPCOB, ");
                query.Append(" C.UFCOB, C.TELEFONECOB, C.ID_MEDICO, C.TELEFONE2COB, C.VIP, C.TELEFONE_CONTATO, C.USA_CONTRATO, C.ID_CLIENTE_MATRIZ,  ");
                query.Append(" C.ID_CLIENTE_CREDENCIADO, C.PARTICULAR, C.UNIDADE, C.CREDENCIADA, C.PRESTADOR, C.ID_CIDADE_IBGE, CIDADE.CODIGO, CIDADE.NOME, ");
                query.Append(" CIDADE.UF_CIDADE, C.ID_CIDADE_IBGE_COBRANCA, CIDADE_COBRANCA.CODIGO, CIDADE_COBRANCA.NOME, CIDADE_COBRANCA.UF_CIDADE, C.DESTACA_ISS, C.GERA_COBRANCA_VALOR_LIQUIDO, C.ALIQUOTA_ISS, C.CODIGO_CNES, C.USA_CENTRO_CUSTO, C.BLOQUEADO, C.CREDENCIADORA, C.FISICA, C.USA_PO, C.SIMPLES_NACIONAL ");
                query.Append(" FROM SEG_CLIENTE C ");
                query.Append(" JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = C.ID_CIDADE_IBGE ");
                query.Append(" JOIN SEG_CIDADE_IBGE CIDADE_COBRANCA ON CIDADE_COBRANCA.ID_CIDADE_IBGE = C.ID_CIDADE_IBGE_COBRANCA ");
                query.Append(" LEFT JOIN SEG_MEDICO M ON M.ID_MEDICO = C.ID_MEDICO");
                query.Append(" WHERE C.CNPJ = :VALUE1");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = cnpj;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    clienteRetorno = new Cliente( dr.GetInt64(0), dr.IsDBNull(1) ? string.Empty : dr.GetString(1), dr.IsDBNull(2) ? string.Empty : dr.GetString(2), dr.IsDBNull(3) ? string.Empty : dr.GetString(3), dr.IsDBNull(4) ? string.Empty : dr.GetString(4), dr.IsDBNull(5) ? string.Empty : dr.GetString(5), dr.IsDBNull(6) ? string.Empty : dr.GetString(6), dr.IsDBNull(7) ? string.Empty : dr.GetString(7), dr.IsDBNull(8) ? string.Empty : dr.GetString(8), dr.IsDBNull(9) ? string.Empty : dr.GetString(9), dr.IsDBNull(10) ? string.Empty : dr.GetString(10), dr.IsDBNull(11) ? string.Empty : dr.GetString(11), dr.IsDBNull(12) ? string.Empty : dr.GetString(12), dr.IsDBNull(13) ? string.Empty : dr.GetString(13), dr.IsDBNull(14) ? string.Empty : dr.GetString(14), dr.GetDateTime(15), dr.IsDBNull(16) ? string.Empty : dr.GetString(16), dr.IsDBNull(17) ? string.Empty : dr.GetString(17), dr.IsDBNull(18) ? string.Empty : dr.GetString(18), dr.IsDBNull(19) ? string.Empty : dr.GetString(19), dr.IsDBNull(20) ? string.Empty : dr.GetString(20), dr.GetString(21), dr.GetInt32(22), dr.GetInt32(23), dr.GetString(24), dr.GetString(25), dr.IsDBNull(26) ? string.Empty : dr.GetString(26), dr.GetString(27), dr.GetString(28), dr.GetString(29), dr.IsDBNull(30) ? string.Empty : dr.GetString(30), dr.IsDBNull(31) ? null : new Medico(dr.GetInt64(31)), dr.IsDBNull(32) ? string.Empty : dr.GetString(32), dr.GetBoolean(33), dr.IsDBNull(34) ? string.Empty : dr.GetString(34), dr.GetBoolean(35), dr.IsDBNull(36) ? null : new Cliente(dr.GetInt64(36)), dr.IsDBNull(37) ? null : new Cliente(dr.GetInt64(37)), dr.GetBoolean(38), dr.GetBoolean(39), dr.GetBoolean(40), dr.GetBoolean(41), new CidadeIbge(dr.GetInt64(42), dr.GetString(43), dr.GetString(44), dr.GetString(45)), new CidadeIbge(dr.GetInt64(46), dr.GetString(47), dr.GetString(48), dr.GetString(49)), dr.GetBoolean(50), dr.GetBoolean(51), dr.GetDecimal(52), dr.IsDBNull(53) ? string.Empty : dr.GetString(53), dr.GetBoolean(54), dr.GetBoolean(55), dr.GetBoolean(56), dr.GetBoolean(57), dr.GetBoolean(58), dr.GetBoolean(59));
                }

                return clienteRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteByCpf", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Cliente insert(Cliente cliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirCliente");

            try
            {
                cliente.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_CLIENTE ( ");
                query.Append(" ID_CLIENTE, RAZAO_SOCIAL, FANTASIA, CNPJ, INSCRICAO, ENDERECO, NUMERO, COMPLEMENTO, ");
                query.Append(" BAIRRO, CEP, UF, TELEFONE1, TELEFONE2, EMAIL, SITE, DATA_CADASTRO, RESPONSAVEL, ");
                query.Append(" CARGO, DOCUMENTO, ESCOPO, JORNADA, SITUACAO, EST_MASC, EST_FEM, ENDERECOCOB, ");
                query.Append(" NUMEROCOB, COMPLEMENTOCOB, BAIRROCOB, CEPCOB, UFCOB, TELEFONECOB, ");
                query.Append(" ID_MEDICO, TELEFONE2COB, VIP, TELEFONE_CONTATO, USA_CONTRATO, ID_CLIENTE_MATRIZ, ID_CLIENTE_CREDENCIADO, ");
                query.Append(" PARTICULAR, UNIDADE, CREDENCIADA, PRESTADOR, ID_CIDADE_IBGE, ID_CIDADE_IBGE_COBRANCA, DESTACA_ISS, GERA_COBRANCA_VALOR_LIQUIDO, ALIQUOTA_ISS, ID_RAMO, CODIGO_CNES, USA_CENTRO_CUSTO, BLOQUEADO, CREDENCIADORA, FISICA, USA_PO, SIMPLES_NACIONAL) VALUES ");
                query.Append(" (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6, :VALUE7 , :VALUE8, :VALUE9, ");
                query.Append(" :VALUE10, :VALUE11, :VALUE12, :VALUE13, :VALUE14, :VALUE15, :VALUE16, :VALUE17, ");
                query.Append(" :VALUE18, :VALUE19, :VALUE20, ");
                query.Append(" :VALUE21, :VALUE22, :VALUE23, :VALUE24, :VALUE25, :VALUE26, :VALUE27, :VALUE28, ");
                query.Append(" :VALUE29, :VALUE30, :VALUE31, :VALUE32, :VALUE33, :VALUE34, :VALUE35, :VALUE36, :VALUE37, :VALUE38, :VALUE39, :VALUE40, ");
                query.Append(" :VALUE41, :VALUE42, :VALUE43, :VALUE44, :VALUE45, :VALUE46, :VALUE47, :VALUE48, :VALUE49, :VALUE50, :VALUE51, :VALUE52, :VALUE53, :VALUE54, :VALUE55 ) ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = cliente.RazaoSocial;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = cliente.Fantasia;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = cliente.Cnpj;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = cliente.Inscricao;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = cliente.Endereco;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = cliente.Numero;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Varchar));
                command.Parameters[7].Value = cliente.Complemento;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                command.Parameters[8].Value = cliente.Bairro;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                command.Parameters[9].Value = cliente.Cep;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Varchar));
                command.Parameters[10].Value = cliente.Uf;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Varchar));
                command.Parameters[11].Value = cliente.Telefone1;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Varchar));
                command.Parameters[12].Value = cliente.Telefone2;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Varchar));
                command.Parameters[13].Value = cliente.Email;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Varchar));
                command.Parameters[14].Value = cliente.Site;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Date));
                command.Parameters[15].Value = DateTime.Now;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE17", NpgsqlDbType.Varchar));
                command.Parameters[16].Value = cliente.Responsavel;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE18", NpgsqlDbType.Varchar));
                command.Parameters[17].Value = cliente.Cargo;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE19", NpgsqlDbType.Varchar));
                command.Parameters[18].Value = cliente.Documento;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE20", NpgsqlDbType.Varchar));
                command.Parameters[19].Value = cliente.Escopo;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE21", NpgsqlDbType.Varchar));
                command.Parameters[20].Value = cliente.Jornada;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE22", NpgsqlDbType.Varchar));
                command.Parameters[21].Value = cliente.Situacao;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE23", NpgsqlDbType.Integer));
                command.Parameters[22].Value = cliente.EstMasc;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE24", NpgsqlDbType.Integer));
                command.Parameters[23].Value = cliente.EstFem;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE25", NpgsqlDbType.Varchar));
                command.Parameters[24].Value = cliente.EnderecoCob;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE26", NpgsqlDbType.Varchar));
                command.Parameters[25].Value = cliente.NumeroCob;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE27", NpgsqlDbType.Varchar));
                command.Parameters[26].Value = cliente.ComplementoCob;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE28", NpgsqlDbType.Varchar));
                command.Parameters[27].Value = cliente.BairroCob;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE29", NpgsqlDbType.Varchar));
                command.Parameters[28].Value = cliente.CepCob;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE30", NpgsqlDbType.Varchar));
                command.Parameters[29].Value = cliente.UfCob;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE31", NpgsqlDbType.Varchar));
                command.Parameters[30].Value = cliente.TelefoneCob.ToUpper();
                
                command.Parameters.Add(new NpgsqlParameter("VALUE32", NpgsqlDbType.Integer));
                command.Parameters[31].Value = cliente.Coordenador != null ? cliente.Coordenador.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE33", NpgsqlDbType.Varchar));
                command.Parameters[32].Value = cliente.Telefone2Cob;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE34", NpgsqlDbType.Boolean));
                command.Parameters[33].Value = cliente.Vip;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE35", NpgsqlDbType.Varchar));
                command.Parameters[34].Value = cliente.TelefoneContato.ToUpper();
                
                command.Parameters.Add(new NpgsqlParameter("VALUE36", NpgsqlDbType.Boolean));
                command.Parameters[35].Value = cliente.UsaContrato;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE37", NpgsqlDbType.Integer));
                command.Parameters[36].Value = cliente.Matriz != null ? cliente.Matriz.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE38", NpgsqlDbType.Integer));
                command.Parameters[37].Value = cliente.CredenciadaCliente != null ? cliente.CredenciadaCliente.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE39", NpgsqlDbType.Boolean));
                command.Parameters[38].Value = cliente.Particular;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE40", NpgsqlDbType.Boolean));
                command.Parameters[39].Value = cliente.Unidade;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE41", NpgsqlDbType.Boolean));
                command.Parameters[40].Value = cliente.Credenciada;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE42", NpgsqlDbType.Boolean));
                command.Parameters[41].Value = cliente.Prestador;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE43", NpgsqlDbType.Integer));
                command.Parameters[42].Value = cliente.CidadeIbge.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE44", NpgsqlDbType.Integer));
                command.Parameters[43].Value = cliente.CidadeIbgeCobranca != null ? cliente.CidadeIbgeCobranca.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE45", NpgsqlDbType.Boolean));
                command.Parameters[44].Value = cliente.DestacaIss;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE46", NpgsqlDbType.Boolean));
                command.Parameters[45].Value = cliente.GeraCobrancaValorLiquido;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE47", NpgsqlDbType.Numeric));
                command.Parameters[46].Value = cliente.AliquotaIss;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE48", NpgsqlDbType.Integer));
                command.Parameters[47].Value = cliente.Ramo != null ? cliente.Ramo.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE49", NpgsqlDbType.Varchar));
                command.Parameters[48].Value = cliente.CodigoCnes;

                command.Parameters.Add(new NpgsqlParameter("VALUE50", NpgsqlDbType.Boolean));
                command.Parameters[49].Value = cliente.UsaCentroCusto;

                command.Parameters.Add(new NpgsqlParameter("VALUE51", NpgsqlDbType.Boolean));
                command.Parameters[50].Value = cliente.Bloqueado;

                command.Parameters.Add(new NpgsqlParameter("VALUE52", NpgsqlDbType.Boolean));
                command.Parameters[51].Value = cliente.Credenciadora;

                command.Parameters.Add(new NpgsqlParameter("VALUE53", NpgsqlDbType.Boolean));
                command.Parameters[52].Value = cliente.Fisica;

                command.Parameters.Add(new NpgsqlParameter("VALUE54", NpgsqlDbType.Boolean));
                command.Parameters[53].Value = cliente.UsaPo;

                command.Parameters.Add(new NpgsqlParameter("VALUE55", NpgsqlDbType.Boolean));
                command.Parameters[54].Value = cliente.SimplesNacional;

                command.ExecuteNonQuery();

                return cliente;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_CLIENTE_ID_CLIENTE_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoIdCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Cliente findById(Int64 idCliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteById");

            Cliente clienteRetorno = null;
            
            StringBuilder query = new StringBuilder();
            
            try
            {

                query.Append(" SELECT C.ID_CLIENTE, C.RAZAO_SOCIAL, C.FANTASIA, C.CNPJ, C.INSCRICAO, C.ENDERECO, C.NUMERO, ");
                query.Append(" C.COMPLEMENTO, C.BAIRRO, C.CEP, C.UF, C.TELEFONE1, C.TELEFONE2, C.EMAIL, C.SITE, ");
                query.Append(" C.DATA_CADASTRO, C.RESPONSAVEL, C.CARGO, C.DOCUMENTO, C.ESCOPO, C.JORNADA, C.SITUACAO, ");
                query.Append(" C.EST_MASC, C.EST_FEM, C.ENDERECOCOB, C.NUMEROCOB, C.COMPLEMENTOCOB, C.BAIRROCOB, C.CEPCOB, ");
                query.Append(" C.UFCOB, C.TELEFONECOB, C.ID_MEDICO, C.TELEFONE2COB, C.VIP, ");
                query.Append(" C.TELEFONE_CONTATO, C.USA_CONTRATO, ");
                query.Append(" ID_CLIENTE_MATRIZ, C.ID_CLIENTE_CREDENCIADO, C.PARTICULAR, ");
                query.Append(" C.UNIDADE, C.CREDENCIADA, C.PRESTADOR, C.ID_CIDADE_IBGE, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, ");
                query.Append(" C.ID_CIDADE_IBGE_COBRANCA, CIDADE_COBRANCA.CODIGO, CIDADE_COBRANCA.NOME, CIDADE_COBRANCA.UF_CIDADE, ");
                query.Append(" C.DESTACA_ISS, C.GERA_COBRANCA_VALOR_LIQUIDO, C.ALIQUOTA_ISS, C.ID_RAMO, RAMO.DESCRICAO, RAMO.SITUACAO, C.CODIGO_CNES, C.USA_CENTRO_CUSTO, C.BLOQUEADO, C.CREDENCIADORA, C.FISICA, C.USA_PO, C.SIMPLES_NACIONAL ");
                query.Append(" FROM SEG_CLIENTE C  ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = C.ID_CIDADE_IBGE ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE_COBRANCA ON CIDADE_COBRANCA.ID_CIDADE_IBGE = C.ID_CIDADE_IBGE_COBRANCA ");
                query.Append(" LEFT JOIN SEG_RAMO RAMO ON RAMO.ID = C.ID_RAMO");
                query.Append(" WHERE C.ID_CLIENTE = :VALUE1");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = idCliente;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    clienteRetorno = new Cliente(dr.GetInt64(0), dr.GetString(1), dr.IsDBNull(2) ? string.Empty : dr.GetString(2), dr.GetString(3), dr.GetString(4), dr.GetString(5), dr.GetString(6), dr.GetString(7), dr.GetString(8), dr.GetString(9), dr.GetString(10), dr.GetString(11), dr.IsDBNull(12) ? string.Empty : dr.GetString(12), dr.GetString(13), dr.IsDBNull(14) ? string.Empty : dr.GetString(14), dr.GetDateTime(15), dr.IsDBNull(16) ? string.Empty : dr.GetString(16), dr.IsDBNull(17) ? string.Empty :dr.GetString(17), dr.IsDBNull(18) ? string.Empty : dr.GetString(18), dr.IsDBNull(19) ? string.Empty : dr.GetString(19), dr.IsDBNull(20) ? string.Empty : dr.GetString(20), dr.GetString(21), dr.GetInt32(22), dr.GetInt32(23), dr.GetString(24),  dr.GetString(25), dr.IsDBNull(26) ? string.Empty : dr.GetString(26), dr.GetString(27), dr.GetString(28), dr.GetString(29), dr.GetString(30), dr.IsDBNull(31) ? null : new Medico(dr.GetInt64(31)), dr.IsDBNull(32) ? string.Empty : dr.GetString(32), dr.GetBoolean(33), dr.IsDBNull(34) ? string.Empty : dr.GetString(34), dr.GetBoolean(35), dr.IsDBNull(36) ? null : new Cliente(dr.GetInt64(36)), dr.IsDBNull(37) ? null : new Cliente(dr.GetInt64(37)), dr.GetBoolean(38), dr.GetBoolean(39), dr.GetBoolean(40), dr.GetBoolean(41), new CidadeIbge(dr.GetInt64(42), dr.GetString(43), dr.GetString(44), dr.GetString(45)), dr.IsDBNull(46) ? null : new CidadeIbge(dr.GetInt64(46), dr.GetString(47), dr.GetString(48), dr.GetString(49)), dr.GetBoolean(50), dr.GetBoolean(51), dr.GetDecimal(52), dr.IsDBNull(56) ? string.Empty : dr.GetString(56), dr.GetBoolean(57), dr.GetBoolean(58), dr.GetBoolean(59), dr.GetBoolean(60), dr.GetBoolean(61), dr.GetBoolean(62)); 
                    clienteRetorno.Ramo = dr.IsDBNull(53) ? null : new Ramo(dr.GetInt64(53), dr.GetString(54), dr.GetString(55));

                }

                return clienteRetorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
                
        public Cliente updateCliente(Cliente cliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateCliente");

            try
            {

                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_CLIENTE SET RAZAO_SOCIAL = :VALUE2, FANTASIA = :VALUE3, CNPJ = :VALUE4, INSCRICAO = :VALUE5,");
                query.Append(" ENDERECO = :VALUE6, NUMERO = :VALUE7, COMPLEMENTO = :VALUE8, BAIRRO = :VALUE9, CEP = :VALUE10,");
                query.Append(" UF = :VALUE11, TELEFONE1 = :VALUE12, TELEFONE2 = :VALUE13, EMAIL = :VALUE14, SITE = :VALUE15, DATA_CADASTRO = :VALUE16,");
                query.Append(" RESPONSAVEL = :VALUE17, CARGO = :VALUE18, DOCUMENTO = :VALUE19, ESCOPO = :VALUE20, JORNADA = :VALUE21, SITUACAO = :VALUE22,");
                query.Append(" EST_MASC = :VALUE23, EST_FEM = :VALUE24, ENDERECOCOB = :VALUE25, NUMEROCOB = :VALUE26, COMPLEMENTOCOB = :VALUE27,");
                query.Append(" BAIRROCOB = :VALUE28, CEPCOB = :VALUE29, UFCOB = :VALUE30, TELEFONECOB = :VALUE31, ID_MEDICO = :VALUE32, TELEFONE2COB = :VALUE33, ");
                query.Append(" VIP = :VALUE34, TELEFONE_CONTATO = :VALUE35, USA_CONTRATO = :VALUE36, ID_CLIENTE_MATRIZ = :VALUE37, ID_CLIENTE_CREDENCIADO = :VALUE38, ");
                query.Append(" PARTICULAR = :VALUE39, UNIDADE = :VALUE40, CREDENCIADA = :VALUE41, PRESTADOR = :VALUE42, ID_CIDADE_IBGE = :VALUE43,  ");
                query.Append(" ID_CIDADE_IBGE_COBRANCA = :VALUE44, DESTACA_ISS = :VALUE45, GERA_COBRANCA_VALOR_LIQUIDO = :VALUE46, ALIQUOTA_ISS = :VALUE47, ID_RAMO = :VALUE48, CODIGO_CNES = :VALUE49, USA_CENTRO_CUSTO = :VALUE50, BLOQUEADO = :VALUE51, CREDENCIADORA = :VALUE52, FISICA = :VALUE53, USA_PO = :VALUE54, SIMPLES_NACIONAL = :VALUE55 ");
                query.Append(" WHERE ID_CLIENTE = :VALUE1 ");
                                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString() , dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = cliente.RazaoSocial;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = cliente.Fantasia;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = cliente.Cnpj;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = cliente.Inscricao;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = cliente.Endereco;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = cliente.Numero;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Varchar));
                command.Parameters[7].Value = cliente.Complemento;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                command.Parameters[8].Value = cliente.Bairro;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                command.Parameters[9].Value = cliente.Cep;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Varchar));
                command.Parameters[10].Value = cliente.Uf;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Varchar));
                command.Parameters[11].Value = cliente.Telefone1;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Varchar));
                command.Parameters[12].Value = cliente.Telefone2;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Varchar));
                command.Parameters[13].Value = cliente.Email;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Varchar));
                command.Parameters[14].Value = cliente.Site;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Date));
                command.Parameters[15].Value = cliente.DataCadastro;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE17", NpgsqlDbType.Varchar));
                command.Parameters[16].Value = cliente.Responsavel;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE18", NpgsqlDbType.Varchar));
                command.Parameters[17].Value = cliente.Cargo;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE19", NpgsqlDbType.Varchar));
                command.Parameters[18].Value = cliente.Documento;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE20", NpgsqlDbType.Varchar));
                command.Parameters[19].Value = cliente.Escopo;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE21", NpgsqlDbType.Varchar));
                command.Parameters[20].Value = cliente.Jornada;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE22", NpgsqlDbType.Varchar));
                command.Parameters[21].Value = cliente.Situacao;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE23", NpgsqlDbType.Integer));
                command.Parameters[22].Value = cliente.EstMasc;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE24", NpgsqlDbType.Integer));
                command.Parameters[23].Value = cliente.EstFem;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE25", NpgsqlDbType.Varchar));
                command.Parameters[24].Value = cliente.EnderecoCob;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE26", NpgsqlDbType.Varchar));
                command.Parameters[25].Value = cliente.NumeroCob;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE27", NpgsqlDbType.Varchar));
                command.Parameters[26].Value = cliente.ComplementoCob;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE28", NpgsqlDbType.Varchar));
                command.Parameters[27].Value = cliente.BairroCob;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE29", NpgsqlDbType.Varchar));
                command.Parameters[28].Value = cliente.CepCob;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE30", NpgsqlDbType.Varchar));
                command.Parameters[29].Value = cliente.UfCob;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE31", NpgsqlDbType.Varchar));
                command.Parameters[30].Value = cliente.TelefoneCob;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE32", NpgsqlDbType.Integer));
                command.Parameters[31].Value = cliente.Coordenador != null ? cliente.Coordenador.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE33", NpgsqlDbType.Varchar));
                command.Parameters[32].Value = cliente.Telefone2Cob;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE34", NpgsqlDbType.Boolean));
                command.Parameters[33].Value = cliente.Vip;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE35", NpgsqlDbType.Varchar));
                command.Parameters[34].Value = cliente.TelefoneContato;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE36", NpgsqlDbType.Boolean));
                command.Parameters[35].Value = cliente.UsaContrato;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE37", NpgsqlDbType.Integer));
                command.Parameters[36].Value = cliente.Matriz != null ? cliente.Matriz.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE38", NpgsqlDbType.Integer));
                command.Parameters[37].Value = cliente.CredenciadaCliente != null ? cliente.CredenciadaCliente.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE39", NpgsqlDbType.Boolean));
                command.Parameters[38].Value = cliente.Particular;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE40", NpgsqlDbType.Boolean));
                command.Parameters[39].Value = cliente.Unidade;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE41", NpgsqlDbType.Boolean));
                command.Parameters[40].Value = cliente.Credenciada;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE42", NpgsqlDbType.Boolean));
                command.Parameters[41].Value = cliente.Prestador;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE43", NpgsqlDbType.Integer));
                command.Parameters[42].Value = cliente.CidadeIbge.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE44", NpgsqlDbType.Integer));
                command.Parameters[43].Value = cliente.CidadeIbgeCobranca != null ? cliente.CidadeIbgeCobranca.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE45", NpgsqlDbType.Boolean));
                command.Parameters[44].Value = cliente.DestacaIss;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE46", NpgsqlDbType.Boolean));
                command.Parameters[45].Value = cliente.GeraCobrancaValorLiquido;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE47", NpgsqlDbType.Numeric));
                command.Parameters[46].Value = cliente.AliquotaIss;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE48", NpgsqlDbType.Integer));
                command.Parameters[47].Value = cliente.Ramo != null ? cliente.Ramo.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE49", NpgsqlDbType.Varchar));
                command.Parameters[48].Value = cliente.CodigoCnes;

                command.Parameters.Add(new NpgsqlParameter("VALUE50", NpgsqlDbType.Boolean));
                command.Parameters[49].Value = cliente.UsaCentroCusto;

                command.Parameters.Add(new NpgsqlParameter("VALUE51", NpgsqlDbType.Boolean));
                command.Parameters[50].Value = cliente.Bloqueado;

                command.Parameters.Add(new NpgsqlParameter("VALUE52", NpgsqlDbType.Boolean));
                command.Parameters[51].Value = cliente.Credenciadora;

                command.Parameters.Add(new NpgsqlParameter("VALUE53", NpgsqlDbType.Boolean));
                command.Parameters[52].Value = cliente.Fisica;

                command.Parameters.Add(new NpgsqlParameter("VALUE54", NpgsqlDbType.Boolean));
                command.Parameters[53].Value = cliente.UsaPo;

                command.Parameters.Add(new NpgsqlParameter("VALUE55", NpgsqlDbType.Boolean));
                command.Parameters[54].Value = cliente.SimplesNacional;

                command.ExecuteNonQuery();
                return cliente;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void changeStatusCliente(Int64 idCliente, String novaSituacao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método changeStatusCliente");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" UPDATE SEG_CLIENTE SET SITUACAO = :VALUE2 WHERE ID_CLIENTE = :VALUE1", dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = idCliente;
            
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = novaSituacao.ToUpper();

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - changeStatusCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findClienteAtivoByFilter(Cliente cliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteAtivoByFilter");
            
            StringBuilder query = new StringBuilder();

            try
            {
                if (cliente.isNullForFilterSelect())
                {
                    query.Append("SELECT ID_CLIENTE, RAZAO_SOCIAL, FANTASIA FROM SEG_CLIENTE WHERE SITUACAO = :VALUE3 ORDER BY RAZAO_SOCIAL");
                }
                else
                {
                    query.Append("SELECT ID_CLIENTE,RAZAO_SOCIAL,FANTASIA FROM SEG_CLIENTE WHERE SITUACAO = :VALUE3 ");

                    if (!String.IsNullOrWhiteSpace(cliente.RazaoSocial))
                        query.Append(" AND RAZAO_SOCIAL LIKE :VALUE1 ");

                    if (!String.IsNullOrWhiteSpace(cliente.Fantasia))
                        query.Append(" AND FANTASIA LIKE :VALUE2 ");
               
                    query.Append(" ORDER BY RAZAO_SOCIAL ASC");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[0].Value = cliente.RazaoSocial.ToUpper() + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[1].Value = cliente.Fantasia.ToUpper() + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[2].Value = cliente.Situacao;

                DataSet ds = new DataSet();
                adapter.Fill(ds, "Clientes");
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findClienteAtivoAptoContrato(Cliente cliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteAtivoByAptoContrato");

            StringBuilder query = new StringBuilder();

            try
            {
                if (cliente.isNullForFilterSelect())
                {
                    query.Append("SELECT ID_CLIENTE, RAZAO_SOCIAL, FANTASIA FROM SEG_CLIENTE WHERE SITUACAO = :VALUE3 AND USA_CONTRATO = TRUE ORDER BY RAZAO_SOCIAL");
                }
                else
                {
                    query.Append("SELECT ID_CLIENTE,RAZAO_SOCIAL,FANTASIA FROM SEG_CLIENTE WHERE SITUACAO = :VALUE3 AND USA_CONTRATO = TRUE ");

                    if (!String.IsNullOrWhiteSpace(cliente.RazaoSocial))
                        query.Append(" AND RAZAO_SOCIAL LIKE :VALUE1 ");

                    if (!String.IsNullOrWhiteSpace(cliente.Fantasia))
                        query.Append(" AND FANTASIA LIKE :VALUE2 ");

                    query.Append(" ORDER BY RAZAO_SOCIAL ASC");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[0].Value = cliente.RazaoSocial.ToUpper() + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[1].Value = cliente.Fantasia.ToUpper() + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[2].Value = cliente.Situacao;

                DataSet ds = new DataSet();
                adapter.Fill(ds, "Clientes");
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteAtivoByAptoContrato", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findCnaeByFilter(Cnae cnae, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findCnaeByFilter");

            try
            {
                StringBuilder query = new StringBuilder();

                if (cnae.isNullForFilter())
                {
                    query.Append("SELECT ID_CNAE, FORMATA_CNAE(COD_CNAE) AS COD_CNAE, ATIVIDADE, GRAURISCO, GRUPO, SITUACAO FROM SEG_CNAE ORDER BY COD_CNAE");
                }
                else
                {
                    query.Append("SELECT ID_CNAE, FORMATA_CNAE(COD_CNAE) AS COD_CNAE, ATIVIDADE, GRAURISCO, GRUPO, SITUACAO FROM SEG_CNAE WHERE TRUE");

                    if (!String.IsNullOrWhiteSpace(cnae.CodCnae))
                        query.Append(" AND COD_CNAE = :VALUE1 ");

                    if (!String.IsNullOrWhiteSpace(cnae.Atividade))
                        query.Append(" AND ATIVIDADE LIKE :VALUE2 ");

                    if (!String.IsNullOrEmpty(cnae.Situacao))
                        query.Append(" AND SITUACAO = :VALUE3 ");

                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[0].Value = cnae.CodCnae;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[1].Value = "%" + cnae.Atividade.ToUpper() + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[2].Value = String.IsNullOrEmpty(cnae.Situacao) ? null : cnae.Situacao;
                
                DataSet ds = new DataSet();
                adapter.Fill(ds, "Cnaes");
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findCnaeByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Cnae findCnaeByCodigo(Cnae cnae, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findCnaeByCodigo");

            Cnae cnaRetorno =  null;
           
            try
            {
                
                NpgsqlCommand command = new NpgsqlCommand("SELECT ID_CNAE, FORMATA_CNAE(COD_CNAE), ATIVIDADE, GRAURISCO, GRUPO, SITUACAO FROM SEG_CNAE WHERE COD_CNAE = :VALUE1", dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = cnae.CodCnae;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    cnaRetorno = new Cnae(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3),dr.GetString(4), dr.GetString(5));
                }

                return cnaRetorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findCnaeByCodigo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
              
        }

        public Cnae incluirCnae(Cnae cnae, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirCnae");

            StringBuilder query = new StringBuilder();

            try
            {
                cnae.Id = recuperaProximoIdCnae(dbConnection);

                query.Append( "INSERT INTO SEG_CNAE (ID_CNAE, COD_CNAE, ATIVIDADE, GRAURISCO, GRUPO, SITUACAO) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cnae.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = cnae.CodCnae;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = cnae.Atividade.ToUpper();
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = cnae.GrauRisco;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = cnae.Grupo.ToUpper();
                
                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = cnae.Situacao;
                
                command.ExecuteNonQuery();

                return cnae;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirCnae", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoIdCnae(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoIdCnae");

            Int64 proximoId = Convert.ToInt64(0);

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_CNAE_ID_CNAE_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoIdCnae", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Cnae findCnaeById(Int64 idCnae, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findCnaeById");

            Cnae cnaeRetorno = null;

            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" SELECT ID_CNAE, FORMATA_CNAE(COD_CNAE), ATIVIDADE, GRAURISCO, GRUPO, SITUACAO FROM SEG_CNAE WHERE ID_CNAE = :VALUE1", dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = idCnae;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    cnaeRetorno = new Cnae(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.GetString(4), dr.GetString(5));
                }

                return cnaeRetorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findCnaeById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public Cnae updateCnae(Cnae cnae, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateCnae");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("UPDATE SEG_CNAE SET COD_CNAE = :VALUE2, ATIVIDADE = :VALUE3, GRAURISCO = :VALUE4, GRUPO = :VALUE5 WHERE ID_CNAE = :VALUE1", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cnae.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = cnae.CodCnae;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = cnae.Atividade.ToUpper();
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = cnae.GrauRisco;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = cnae.Grupo.ToUpper();
                

                command.ExecuteNonQuery();
                return cnae;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateCnae", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void deleteCnae(Int64 idCnae, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteCnae");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("DELETE FROM SEG_CNAE WHERE ID_CNAE = :VALUE1", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = idCnae;
                
                command.ExecuteNonQuery();
                
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteCnae", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void changeSituacao(Cnae cnae, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método changeSituacao");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("UPDATE SEG_CNAE SET SITUACAO = :VALUE2 WHERE ID_CNAE = :VALUE1", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cnae.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = cnae.Situacao;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteCnae", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findClienteAtivoByFilterNotInClienteFuncaoFuncionarioSet(Cliente cliente, List<ClienteFuncaoFuncionario> clienteFuncaoFuncionarioSet, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteAtivoByFilterNotInClienteFuncaoFuncionarioSet");

            StringBuilder query = new StringBuilder();
            
            int i = 0;

            try
            {
                if (cliente.isNullForFilterSelect())
                {
                    query.Append(" SELECT ID_CLIENTE, RAZAO_SOCIAL, FANTASIA FROM SEG_CLIENTE WHERE SITUACAO = :VALUE3 AND ID_CLIENTE_MATRIZ IS NULL ");

                    if (clienteFuncaoFuncionarioSet.Count > 0)
                    {
                        query.Append(" AND UNIDADE = FALSE AND ID_CLIENTE NOT IN ( " );

                        foreach (ClienteFuncaoFuncionario clienteFuncaoFuncionario in clienteFuncaoFuncionarioSet)
                        {
                            if (i > 0 && i < clienteFuncaoFuncionarioSet.Count)
                            {
                                query.Append(",");
                            }

                            query.Append(clienteFuncaoFuncionario.ClienteFuncao.Cliente.Id);
                            i++;
                        }

                        query.Append(")");
                    }
                                        
                    query.Append(" ORDER BY RAZAO_SOCIAL");
                }
                else
                {
                    query.Append("SELECT ID_CLIENTE,RAZAO_SOCIAL,FANTASIA FROM SEG_CLIENTE WHERE SITUACAO = :VALUE3 AND UNIDADE = FALSE AND ID_CLIENTE_MATRIZ IS NULL ");

                    if (!String.IsNullOrWhiteSpace(cliente.RazaoSocial))
                        query.Append(" AND RAZAO_SOCIAL LIKE :VALUE1 ");

                    if (!String.IsNullOrWhiteSpace(cliente.Fantasia))
                        query.Append(" AND FANTASIA LIKE :VALUE2 ");

                    if (clienteFuncaoFuncionarioSet.Count > 0)
                    {
                        query.Append(" AND ID_CLIENTE NOT IN ( ");

                        foreach (ClienteFuncaoFuncionario clienteFuncaoFuncionario in clienteFuncaoFuncionarioSet)
                        {
                            if (i > 0 && i < clienteFuncaoFuncionarioSet.Count)
                                query.Append(",");
                        
                            query.Append(clienteFuncaoFuncionario.ClienteFuncao.Cliente.Id);
                            i++;
                        }

                        query.Append(")");
                    }


                    query.Append(" ORDER BY RAZAO_SOCIAL ASC");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[0].Value = cliente.RazaoSocial.ToUpper() + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[1].Value = cliente.Fantasia.ToUpper() + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[2].Value = cliente.Situacao;

                DataSet ds = new DataSet();
                adapter.Fill(ds, "Clientes");
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean verificaPodeAlterarCliente(Cliente cliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaPodeAlterarCliente");

            try
            {
                StringBuilder query = new StringBuilder();
                Boolean podeAlterar = true;

                query.Append("SELECT * ");
                query.Append("FROM SEG_CLIENTE WHERE ");
                query.Append("CNPJ = :VALUE1 ");
                query.Append("AND ID_CLIENTE <> :VALUE2 ");
                query.Append("AND ID_CLIENTE_MATRIZ IS NULL AND PRESTADOR = FALSE ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = cliente.Cnpj;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = cliente.Id;
           
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    podeAlterar = false;
                }

                return podeAlterar;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaPodeAlterarCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAllClienteParticularAtivosByFilter(Cliente cliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllClienteParticularBYFilter");

            try
            {
                StringBuilder query = new StringBuilder();

                if (String.IsNullOrEmpty(cliente.RazaoSocial) && String.IsNullOrEmpty(cliente.Fantasia))
                {
                    query.Append(" SELECT ID_CLIENTE, RAZAO_SOCIAL, FANTASIA ");
                    query.Append(" FROM SEG_CLIENTE WHERE PARTICULAR = TRUE AND SITUACAO = 'A' ORDER BY RAZAO_SOCIAL ASC");
                }
                else
                {
                    query.Append(" SELECT ID_CLIENTE, RAZAO_SOCIAL, FANTASIA ");
                    query.Append(" FROM SEG_CLIENTE WHERE PARTICULAR = TRUE AND SITUACAO = 'A' ");

                    if (!String.IsNullOrWhiteSpace(cliente.RazaoSocial))
                        query.Append(" AND RAZAO_SOCIAL LIKE :VALUE1 ");

                    if (!String.IsNullOrWhiteSpace(cliente.Fantasia))
                        query.Append(" AND FANTASIA LIKE :VALUE2 ");

                    query.Append(" ORDER BY RAZAO_SOCIAL ASC");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[0].Value = cliente.RazaoSocial.ToUpper() + "%";
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[1].Value = cliente.Fantasia.ToUpper() + "%";

                DataSet ds = new DataSet();
                adapter.Fill(ds, "Clientes");
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllClienteParticularBYFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<Cliente> findAllPrestadorByCliente(Cliente cliente, Cliente prestador, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllPrestadorByCliente");

            try
            {
                StringBuilder query = new StringBuilder();
                List<Cliente> prestadores = new List<Cliente>();

                if (String.IsNullOrEmpty(prestador.RazaoSocial) && String.IsNullOrEmpty(prestador.Fantasia))
                {
                    query.Append(" SELECT C.ID_CLIENTE, C.RAZAO_SOCIAL, C.FANTASIA, C.CNPJ, C.INSCRICAO, ");
                    query.Append(" C.ENDERECO, C.NUMERO, C.COMPLEMENTO, C.BAIRRO, C.CEP, C.UF, ");
                    query.Append(" C.TELEFONE1, C.TELEFONE2, C.EMAIL, C.SITE, C.DATA_CADASTRO, C.RESPONSAVEL, C.CARGO, C.DOCUMENTO, ");
                    query.Append(" C.ESCOPO, C.JORNADA, C.SITUACAO, C.EST_MASC, ");
                    query.Append(" C.EST_FEM, C.ENDERECOCOB, C.NUMEROCOB, C.COMPLEMENTOCOB, C.BAIRROCOB, C.CEPCOB, C.UFCOB, C.TELEFONECOB, ");
                    query.Append(" C.ID_MEDICO, C.TELEFONE2COB, C.VIP, C.TELEFONE_CONTATO, ");
                    query.Append(" C.USA_CONTRATO, C.ID_CLIENTE_MATRIZ, C.ID_CLIENTE_CREDENCIADO, C.PARTICULAR, C.UNIDADE, C.CREDENCIADA, C.PRESTADOR, C.ID_CIDADE_IBGE, ");
                    query.Append(" CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, C.ID_CIDADE_IBGE_COBRANCA, C.USA_CENTRO_CUSTO, C.BLOQUEADO, C.CREDENCIADORA, C.FISICA, C.USA_PO, C.SIMPLES_NACIONAL ");
                    query.Append(" FROM SEG_CLIENTE C ");
                    query.Append(" JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = C.ID_CIDADE_IBGE ");
                    query.Append(" WHERE C.PRESTADOR = TRUE AND C.SITUACAO = 'A' AND ");
                    query.Append(" C.ID_CLIENTE NOT IN (SELECT ID_CLIENTE_PRESTADOR FROM SEG_CONTRATO WHERE ID_CLIENTE = :VALUE1 AND ");
                    query.Append(" ID_CLIENTE_PRESTADOR IS NOT NULL ) ");
                }
                else
                {
                    query.Append(" SELECT C.ID_CLIENTE, C.RAZAO_SOCIAL, C.FANTASIA, C.CNPJ, C.INSCRICAO, ");
                    query.Append(" C.ENDERECO, C.NUMERO, C.COMPLEMENTO, C.BAIRRO, C.CEP, C.UF, ");
                    query.Append(" C.TELEFONE1, C.TELEFONE2, C.EMAIL, C.SITE, C.DATA_CADASTRO, C.RESPONSAVEL, C.CARGO, C.DOCUMENTO, ");
                    query.Append(" C.ESCOPO, C.JORNADA, C.SITUACAO, C.EST_MASC, ");
                    query.Append(" C.EST_FEM, C.ENDERECOCOB, C.NUMEROCOB, C.COMPLEMENTOCOB, C.BAIRROCOB, C.CEPCOB, C.UFCOB, C.TELEFONECOB, ");
                    query.Append(" C.ID_MEDICO, C.TELEFONE2COB, C.VIP, C.TELEFONE_CONTATO, ");
                    query.Append(" C.USA_CONTRATO, C.ID_CLIENTE_MATRIZ, C.ID_CLIENTE_CREDENCIADO, C.PARTICULAR, C.UNIDADE, C.CREDENCIADA, C.PRESTADOR, C.ID_CIDADE_IBGE, ");
                    query.Append(" CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, C.ID_CIDADE_IBGE_COBRANCA, C.USA_CENTRO_CUSTO, C.BLOQUEADO, C.CREDENCIADORA, C.FISICA, C.USA_PO, C.SIMPLES_NACIONAL ");
                    query.Append(" FROM SEG_CLIENTE C ");
                    query.Append(" JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = C.ID_CIDADE_IBGE ");
                    query.Append(" WHERE C.PRESTADOR = TRUE AND C.SITUACAO = 'A' AND ");
                    query.Append(" C.ID_CLIENTE NOT IN (SELECT ID_CLIENTE_PRESTADOR FROM SEG_CONTRATO WHERE ID_CLIENTE = :VALUE1 AND ");
                    query.Append(" ID_CLIENTE_PRESTADOR IS NOT NULL ) ");


                    if (!String.IsNullOrEmpty(prestador.RazaoSocial)) 
                        query.Append(" AND C.RAZAO_SOCIAL LIKE :VALUE2 ");

                    if (!String.IsNullOrEmpty(prestador.Fantasia))
                        query.Append(" AND C.FANTASIA LIKE :VALUE3 ");
                }

                query.Append(" ORDER BY C.RAZAO_SOCIAL ASC ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = prestador.RazaoSocial.ToUpper() + "%";

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = prestador.Fantasia.ToUpper() + "%"; 

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    prestadores.Add(new Cliente( dr.GetInt64(0), dr.GetString(1), dr.IsDBNull(2) ? string.Empty : dr.GetString(2), dr.GetString(3), dr.GetString(4), dr.GetString(5), dr.GetString(6), dr.IsDBNull(7) ? string.Empty : dr.GetString(7), dr.GetString(8), dr.GetString(9),  dr.GetString(10), dr.GetString(11), dr.IsDBNull(12) ? string.Empty : dr.GetString(12), dr.GetString(13), String.Empty,  dr.GetDateTime(15), String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, dr.GetString(21), 0, 0, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, null, String.Empty, false, String.Empty, false, null, null, false, false, false, dr.GetBoolean(41), new CidadeIbge(dr.GetInt64(42), dr.GetString(43), dr.GetString(44), dr.GetString(45)),null, false, false, null, string.Empty, dr.GetBoolean(47), dr.GetBoolean(48), dr.GetBoolean(49), dr.GetBoolean(50), dr.GetBoolean(51), dr.GetBoolean(52)));
                }

                return prestadores;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllPrestadorByCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Cliente findClienteByRazaoSocial(String razaoSocial, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteByRazaoSocial");

            Cliente clienteRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT C.ID_CLIENTE, C.RAZAO_SOCIAL, C.FANTASIA, C.CNPJ, C.INSCRICAO, C.ENDERECO,  ");
                query.Append(" C.NUMERO, C.COMPLEMENTO, C.BAIRRO, C.CEP, C.UF, C.TELEFONE1, C.TELEFONE2, C.EMAIL,  ");
                query.Append(" C.SITE, C.DATA_CADASTRO, C.RESPONSAVEL, C.CARGO, C.DOCUMENTO, C.ESCOPO, C.JORNADA, C.SITUACAO,  ");
                query.Append(" C.EST_MASC, C.EST_FEM, C.ENDERECOCOB, C.NUMEROCOB, C.COMPLEMENTOCOB, C.BAIRROCOB, C.CEPCOB, ");
                query.Append(" C.UFCOB, C.TELEFONECOB, C.ID_MEDICO, C.TELEFONE2COB, C.VIP, C.TELEFONE_CONTATO, C.USA_CONTRATO, C.ID_CLIENTE_MATRIZ,  ");
                query.Append(" C.ID_CLIENTE_CREDENCIADO, C.PARTICULAR, C.UNIDADE, C.CREDENCIADA, C.PRESTADOR, C.ID_CIDADE_IBGE, CIDADE.CODIGO, CIDADE.NOME, ");
                query.Append(" CIDADE.UF_CIDADE, C.ID_CIDADE_IBGE_COBRANCA, CIDADE_COBRANCA.CODIGO, CIDADE_COBRANCA.NOME, CIDADE_COBRANCA.UF_CIDADE, ");
                query.Append(" C.DESTACA_ISS, C.GERA_COBRANCA_VALOR_LIQUIDO, C.ALIQUOTA_ISS, C.ID_RAMO, C.CODIGO_CNES, RAMO.DESCRICAO, RAMO.SITUACAO, C.USA_CENTRO_CUSTO, C.BLOQUEADO, C.CREDENCIADORA, C.FISICA, C.USA_PO, C.SIMPLES_NACIONAL ");
                query.Append(" FROM SEG_CLIENTE C ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = C.ID_CIDADE_IBGE ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE_COBRANCA ON CIDADE_COBRANCA.ID_CIDADE_IBGE = C.ID_CIDADE_IBGE_COBRANCA ");
                query.Append(" LEFT JOIN SEG_MEDICO M ON M.ID_MEDICO = C.ID_MEDICO");
                query.Append(" LEFT JOIN SEG_RAMO RAMO ON RAMO.ID = C.ID_RAMO");
                query.Append(" WHERE C.RAZAO_SOCIAL = :VALUE1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = razaoSocial;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    clienteRetorno = new Cliente(dr.GetInt64(0), dr.GetString(1), dr.IsDBNull(2) ? string.Empty : dr.GetString(2), dr.GetString(3), dr.GetString(4), dr.GetString(5), dr.GetString(6), dr.IsDBNull(7) ? string.Empty : dr.GetString(7), dr.GetString(8), dr.GetString(9), dr.GetString(10), dr.GetString(11), dr.IsDBNull(12) ? string.Empty : dr.GetString(12), dr.GetString(13), dr.IsDBNull(14) ? string.Empty : dr.GetString(14), dr.GetDateTime(15), dr.IsDBNull(16) ? string.Empty : dr.GetString(16), dr.IsDBNull(17) ? string.Empty : dr.GetString(17), dr.IsDBNull(18) ? string.Empty : dr.GetString(18), dr.IsDBNull(19) ? string.Empty : dr.GetString(19), dr.IsDBNull(20) ? string.Empty : dr.GetString(20), dr.GetString(21), dr.GetInt32(22), dr.GetInt32(23), dr.GetString(24), dr.GetString(25), dr.IsDBNull(26) ? string.Empty : dr.GetString(26), dr.GetString(27), dr.GetString(28), dr.GetString(29), dr.GetString(30), dr.IsDBNull(31) ? null : new Medico(dr.GetInt64(31)), dr.IsDBNull(32) ? string.Empty : dr.GetString(32), dr.GetBoolean(33), dr.IsDBNull(34) ? string.Empty : dr.GetString(34), dr.GetBoolean(35), dr.IsDBNull(36)? null : new Cliente(dr.GetInt64(36)), dr.IsDBNull(37) ? null : new Cliente(dr.GetInt64(37)), dr.GetBoolean(38), dr.GetBoolean(39), dr.GetBoolean(40), dr.GetBoolean(41), new CidadeIbge(dr.GetInt64(42), dr.GetString(43), dr.GetString(44), dr.GetString(45)), dr.IsDBNull(46) ? null : new CidadeIbge(dr.GetInt64(46), dr.GetString(47), dr.GetString(48), dr.GetString(49)), dr.GetBoolean(50), dr.GetBoolean(51), dr.GetDecimal(52), dr.IsDBNull(54) ? string.Empty : dr.GetString(54), dr.GetBoolean(57), dr.GetBoolean(58), dr.GetBoolean(59), dr.GetBoolean(60), dr.GetBoolean(61), dr.GetBoolean(63)); 
                    clienteRetorno.Ramo = dr.IsDBNull(53) ? null : new Ramo(dr.GetInt64(53), dr.GetString(55), dr.GetString(56));
                }

                return clienteRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteByRazaoSocial", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAllUnidadeByCliente(Cliente cliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllUnidadeByCliente");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID_CLIENTE, RAZAO_SOCIAL, FANTASIA ");
                query.Append(" FROM SEG_CLIENTE ");
                query.Append(" WHERE ID_CLIENTE_MATRIZ = :VALUE1 AND ");
                query.Append(" SITUACAO = 'A' ");
                query.Append(" ORDER BY RAZAO_SOCIAL ASC ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = cliente.Id;

                DataSet ds = new DataSet();
                adapter.Fill(ds, "Unidades");
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllUnidadeByCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean findClienteHaveUnidade(Cliente cliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteHaveUnidade");

            try
            {
                StringBuilder query = new StringBuilder();

                Boolean retorno = false;

                query.Append(" SELECT * ");
                query.Append(" FROM SEG_CLIENTE ");
                query.Append(" WHERE ID_CLIENTE_MATRIZ = :VALUE1 AND ");
                query.Append(" SITUACAO = 'A' ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    retorno = true;
                }

                return retorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteHaveUnidade", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findCnaeNotInCliente(Cnae cnae, Cliente cliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findCnaeNotInCliente");

            try
            {
                StringBuilder query = new StringBuilder();

                if (cnae.isNullForFilter())
                {
                    query.Append(" SELECT ID_CNAE, FORMATA_CNAE(COD_CNAE) AS COD_CNAE, ATIVIDADE, GRAURISCO, GRUPO, SITUACAO FROM SEG_CNAE WHERE ID_CNAE NOT IN (SELECT ID_CNAE FROM SEG_CLI_CNAE WHERE ID_CLIENTE = :VALUE4 ) ORDER BY COD_CNAE");
                }
                else
                {
                    query.Append("SELECT ID_CNAE, FORMATA_CNAE(COD_CNAE) AS COD_CNAE, ATIVIDADE, GRAURISCO, GRUPO, SITUACAO FROM SEG_CNAE WHERE ID_CNAE NOT IN (SELECT ID_CNAE FROM SEG_CLI_CNAE WHERE ID_CLIENTE = :VALUE4 ) ");

                    if (!String.IsNullOrWhiteSpace(cnae.CodCnae))
                        query.Append(" AND COD_CNAE = :VALUE1 ");

                    if (!String.IsNullOrWhiteSpace(cnae.Atividade))
                        query.Append(" AND ATIVIDADE LIKE :VALUE2 ");

                    if (!String.IsNullOrEmpty(cnae.Situacao))
                        query.Append(" AND SITUACAO = :VALUE3 ");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[0].Value = cnae.CodCnae;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[1].Value = "%" + cnae.Atividade.ToUpper() + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[2].Value = String.IsNullOrEmpty(cnae.Situacao) ? null : cnae.Situacao;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[3].Value = cliente.Id;

                DataSet ds = new DataSet();
                adapter.Fill(ds, "Cnaes");
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findCnaeNotInCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAllClienteByCnpjAptoContrato(String cnpj, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllClienteByCnpjAptoContrato");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT CLIENTE.ID_CLIENTE, CLIENTE.RAZAO_SOCIAL, CLIENTE.FANTASIA, (CASE WHEN CLIENTE.FISICA THEN FORMATA_CPF(CLIENTE.CNPJ) ELSE FORMATA_CNPJ(CLIENTE.CNPJ) END) AS CNPJ,  ");
                query.Append(" CLIENTE.UNIDADE, CLIENTE.ID_CLIENTE_MATRIZ ");
                query.Append(" FROM SEG_CLIENTE CLIENTE ");
                query.Append(" WHERE CLIENTE.CNPJ = :VALUE1 AND CLIENTE.USA_CONTRATO = TRUE ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = cnpj;

                DataSet ds = new DataSet();
                
                adapter.Fill(ds, "Clientes");
                
                return ds;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllClienteByCnpjAptoContrato", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAllCredenciadasByCliente(Cliente cliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllCredenciadasByCliente");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID_CLIENTE, RAZAO_SOCIAL, FANTASIA, CNPJ ");
                query.Append(" FROM SEG_CLIENTE ");
                query.Append(" WHERE ID_CLIENTE_CREDENCIADO = :VALUE1 ");
                query.Append(" ORDER BY RAZAO_SOCIAL ASC ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = cliente.Id;

                DataSet ds = new DataSet();
                adapter.Fill(ds, "Credenciadas");
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllCredenciadasByCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }


}
