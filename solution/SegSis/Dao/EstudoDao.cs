﻿using SWS.Entidade;
using Npgsql;
using SWS.Excecao;
using System;
using NpgsqlTypes;
using System.Data;
using System.Text;
using SWS.IDao;
using SWS.View.Resources;
using System.Collections.Generic;
using SWS.Helper;

namespace SWS.Dao
{
    class EstudoDao : IEstudoDao
    {
        public Estudo insert(Estudo estudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirEstudo");

            StringBuilder query = new StringBuilder();

            try
            {

                estudo.Id = recuperaProximoId(dbConnection);

                estudo.CodigoEstudo = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + Convert.ToString(estudo.Id).PadLeft(6, '0') + "00";

                query.Append(" INSERT INTO SEG_ESTUDO (ID_ESTUDO, COD_ESTUDO, ID_CRONOGRAMA, ID_SEG_REP_CLI, ");
                query.Append(" ID_ENGENHEIRO, ID_TECNO, ID_CLIENTE_CONTRATADO, DATA_CRIACAO, DATA_VENCIMENTO, ");
                query.Append(" DATA_FECHAMENTO, SITUACAO, ESTUDO, DATA_FIM_CONTRATO, OBRA, LOCAL_OBRA, ENDERECO, ");
                query.Append(" NUMERO, COMPLEMENTO, BAIRRO, CIDADE, UF, NUM_CONTRATO, GRAU_RISCO, CLI_RAZAO_SOCIAL, ");
                query.Append(" CLI_CNPJ, CLI_INSCRICAO, CLI_EST_FEM, CLI_EST_MASC, CLI_JORNADA, CLI_ENDERECO, CLI_NUMERO, ");
                query.Append(" CLI_COMPLEMENTO, CLI_BAIRRO, CLI_CIDADE, CLI_EMAIL, CLI_CEP, CLI_UF, CLI_TELEFONE1, ");
                query.Append(" CLI_TELEFONE2, CONTR_RAZAO_SOCIAL, CONTR_CNPJ, CONTR_ENDERECO, CONTR_NUMERO, CONTR_COMPLEMENTO, ");
                query.Append(" CONTR_BAIRRO, CONTR_CIDADE, CONTR_CEP, CONTR_UF, CONTR_INSCRICAO, DATA_INICIO_CONTRATO, ");
                query.Append(" CEP, CLI_FANTASIA, COMENTARIO, ID_RELATORIO, AVULSO ) VALUES ");
                query.Append(" (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6, :VALUE7, :VALUE8,");
                query.Append(" :VALUE9, :VALUE10, :VALUE11, :VALUE12, :VALUE13, :VALUE14, :VALUE15, :VALUE16, :VALUE17, ");
                query.Append(" :VALUE18, :VALUE19, :VALUE20,");
                query.Append(" :VALUE21, :VALUE22, :VALUE23, :VALUE24, :VALUE25, :VALUE26, :VALUE27, :VALUE28, :VALUE29, ");
                query.Append(" :VALUE30, :VALUE31, :VALUE32,");
                query.Append(" :VALUE33, :VALUE34, :VALUE35, :VALUE36, :VALUE37, :VALUE38, :VALUE39, :VALUE40, :VALUE41, ");
                query.Append(" :VALUE42, :VALUE43, :VALUE44,");
                query.Append(" :VALUE45, :VALUE46, :VALUE47, :VALUE48, :VALUE49, :VALUE50, :VALUE51, :VALUE52, :VALUE53, :VALUE54, :VALUE55 )");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = estudo.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = estudo.CodigoEstudo;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = estudo.Cronograma != null ? estudo.Cronograma.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Integer));
                command.Parameters[4].Value = estudo.Engenheiro != null ? estudo.Engenheiro.Id : (Int64?)null;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Integer));
                command.Parameters[5].Value = estudo.Tecno != null ? estudo.Tecno.Id : (Int64?)null;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Integer));
                command.Parameters[6].Value = estudo.ClienteContratante != null ? estudo.ClienteContratante.Id : (Int64?)null;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Date));
                command.Parameters[7].Value = estudo.DataCriacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Date));
                command.Parameters[8].Value = estudo.DataVencimento;

                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Date));
                command.Parameters[9].Value = estudo.DataFechamento;

                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Varchar));
                command.Parameters[10].Value = !String.IsNullOrEmpty(estudo.Situacao) ? estudo.Situacao : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Boolean));
                command.Parameters[11].Value = estudo.TipoEstudo;

                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Varchar));
                command.Parameters[12].Value = !String.IsNullOrEmpty(estudo.DataFimContrato) ? estudo.DataFimContrato : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Varchar));
                command.Parameters[13].Value = !String.IsNullOrEmpty(estudo.Obra) ? estudo.Obra.ToUpper() : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Varchar));
                command.Parameters[14].Value = !String.IsNullOrEmpty(estudo.LocalObra) ? estudo.LocalObra.ToUpper() : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Varchar));
                command.Parameters[15].Value = !String.IsNullOrEmpty(estudo.Endereco) ? estudo.Endereco.ToUpper() : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE17", NpgsqlDbType.Varchar));
                command.Parameters[16].Value = !String.IsNullOrEmpty(estudo.Numero) ? estudo.Numero.ToUpper() : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE18", NpgsqlDbType.Varchar));
                command.Parameters[17].Value = !String.IsNullOrEmpty(estudo.Complemento) ? estudo.Complemento : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE19", NpgsqlDbType.Varchar));
                command.Parameters[18].Value = !String.IsNullOrEmpty(estudo.Bairro) ? estudo.Bairro.ToUpper() : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE20", NpgsqlDbType.Varchar));
                command.Parameters[19].Value = !String.IsNullOrEmpty(estudo.Cidade) ? estudo.Cidade.ToUpper() : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE21", NpgsqlDbType.Varchar));
                command.Parameters[20].Value = !String.IsNullOrEmpty(estudo.Uf) ? estudo.Uf : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE22", NpgsqlDbType.Varchar));
                command.Parameters[21].Value = !String.IsNullOrEmpty(estudo.NumContrato) ? estudo.NumContrato : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE23", NpgsqlDbType.Varchar));
                command.Parameters[22].Value = !String.IsNullOrEmpty(estudo.GrauRisco) ? estudo.GrauRisco : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE24", NpgsqlDbType.Varchar));
                command.Parameters[23].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.RazaoSocial : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE25", NpgsqlDbType.Varchar));
                command.Parameters[24].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.Cnpj : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE26", NpgsqlDbType.Varchar));
                command.Parameters[25].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.Inscricao : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE27", NpgsqlDbType.Integer));
                command.Parameters[26].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.EstFem : (Int32?)null;

                command.Parameters.Add(new NpgsqlParameter("VALUE28", NpgsqlDbType.Integer));
                command.Parameters[27].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.EstMasc : (Int32?)null;

                command.Parameters.Add(new NpgsqlParameter("VALUE29", NpgsqlDbType.Varchar));
                command.Parameters[28].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.Jornada : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE30", NpgsqlDbType.Varchar));
                command.Parameters[29].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.Endereco : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE31", NpgsqlDbType.Varchar));
                command.Parameters[30].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.Numero : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE32", NpgsqlDbType.Varchar));
                command.Parameters[31].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.Complemento : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE33", NpgsqlDbType.Varchar));
                command.Parameters[32].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.Bairro : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE34", NpgsqlDbType.Varchar));
                command.Parameters[33].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.CidadeIbge.Nome : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE35", NpgsqlDbType.Varchar));
                command.Parameters[34].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.Email : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE36", NpgsqlDbType.Varchar));
                command.Parameters[35].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.Cep : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE37", NpgsqlDbType.Varchar));
                command.Parameters[36].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.Uf : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE38", NpgsqlDbType.Varchar));
                command.Parameters[37].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.Telefone1 : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE39", NpgsqlDbType.Varchar));
                command.Parameters[38].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.Telefone2 : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE40", NpgsqlDbType.Varchar));
                command.Parameters[39].Value = estudo.ClienteContratante != null ? estudo.ClienteContratante.RazaoSocial : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE41", NpgsqlDbType.Varchar));
                command.Parameters[40].Value = estudo.ClienteContratante != null ? estudo.ClienteContratante.Cnpj : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE42", NpgsqlDbType.Varchar));
                command.Parameters[41].Value = estudo.ClienteContratante != null ? estudo.ClienteContratante.Endereco : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE43", NpgsqlDbType.Varchar));
                command.Parameters[42].Value = estudo.ClienteContratante != null ? estudo.ClienteContratante.Numero : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE44", NpgsqlDbType.Varchar));
                command.Parameters[43].Value = estudo.ClienteContratante != null ? estudo.ClienteContratante.Complemento : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE45", NpgsqlDbType.Varchar));
                command.Parameters[44].Value = estudo.ClienteContratante != null ? estudo.ClienteContratante.Bairro : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE46", NpgsqlDbType.Varchar));
                command.Parameters[45].Value = estudo.ClienteContratante != null ? estudo.ClienteContratante.CidadeIbge.Nome.ToUpper() : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE47", NpgsqlDbType.Varchar));
                command.Parameters[46].Value = estudo.ClienteContratante != null ? estudo.ClienteContratante.Cep : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE48", NpgsqlDbType.Varchar));
                command.Parameters[47].Value = estudo.ClienteContratante != null ? estudo.ClienteContratante.Uf : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE49", NpgsqlDbType.Varchar));
                command.Parameters[48].Value = estudo.ClienteContratante != null ? estudo.ClienteContratante.Inscricao : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE50", NpgsqlDbType.Varchar));
                command.Parameters[49].Value = !String.IsNullOrEmpty(estudo.DataInicioContrato) ? estudo.DataInicioContrato : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE51", NpgsqlDbType.Varchar));
                command.Parameters[50].Value = !String.IsNullOrEmpty(estudo.Cep) ? estudo.Cep : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE52", NpgsqlDbType.Varchar));
                command.Parameters[51].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.Fantasia : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE53", NpgsqlDbType.Text));
                command.Parameters[52].Value = !String.IsNullOrEmpty(estudo.Comentario) ? estudo.Comentario : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE54", NpgsqlDbType.Integer));
                command.Parameters[53].Value = estudo.IdRelatorio != null ? estudo.IdRelatorio : (Int64?)null;

                command.Parameters.Add(new NpgsqlParameter("VALUE55", NpgsqlDbType.Boolean));
                command.Parameters[54].Value = estudo.Avulso;

                command.ExecuteNonQuery();

                return estudo;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_ESTUDO_ID_ESTUDO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoIdCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Estudo update(Estudo estudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_ESTUDO SET ");
                query.Append(" ID_SEG_REP_CLI = :VALUE2, ");
                query.Append(" ID_TECNO = :VALUE3, ");
                query.Append(" ID_CLIENTE_CONTRATADO = :VALUE4,");
                query.Append(" DATA_CRIACAO = :VALUE5, ");
                query.Append(" DATA_VENCIMENTO = :VALUE6, ");
                query.Append(" SITUACAO = :VALUE7, ");
                query.Append(" DATA_FIM_CONTRATO = :VALUE8, ");
                query.Append(" OBRA = :VALUE9, ");
                query.Append(" LOCAL_OBRA = :VALUE10, ");
                query.Append(" ENDERECO = :VALUE11, ");
                query.Append(" NUMERO = :VALUE12, ");
                query.Append(" COMPLEMENTO = :VALUE13, ");
                query.Append(" BAIRRO = :VALUE14, ");
                query.Append(" CIDADE = :VALUE15, ");
                query.Append(" UF = :VALUE16, ");
                query.Append(" NUM_CONTRATO = :VALUE17, ");
                query.Append(" GRAU_RISCO = :VALUE18, ");
                query.Append(" CLI_RAZAO_SOCIAL = :VALUE19, ");
                query.Append(" CLI_CNPJ = :VALUE20, ");
                query.Append(" CLI_INSCRICAO = :VALUE21, ");
                query.Append(" CLI_EST_FEM = :VALUE22, ");
                query.Append(" CLI_EST_MASC = :VALUE23, ");
                query.Append(" CLI_JORNADA = :VALUE24, ");
                query.Append(" CLI_ENDERECO = :VALUE25, ");
                query.Append(" CLI_NUMERO = :VALUE26, ");
                query.Append(" CLI_COMPLEMENTO = :VALUE27, ");
                query.Append(" CLI_BAIRRO = :VALUE28, ");
                query.Append(" CLI_CIDADE = :VALUE29, ");
                query.Append(" CLI_EMAIL = :VALUE30, ");
                query.Append(" CLI_CEP = :VALUE31, ");
                query.Append(" CLI_UF = :VALUE32, ");
                query.Append(" CLI_TELEFONE1 = :VALUE33, ");
                query.Append(" CLI_TELEFONE2 = :VALUE34, ");
                query.Append(" CONTR_RAZAO_SOCIAL = :VALUE35, ");
                query.Append(" CONTR_CNPJ = :VALUE36, ");
                query.Append(" CONTR_ENDERECO = :VALUE37, ");
                query.Append(" CONTR_NUMERO = :VALUE38,");
                query.Append(" CONTR_COMPLEMENTO = :VALUE39, ");
                query.Append(" CONTR_BAIRRO = :VALUE40, ");
                query.Append(" CONTR_CIDADE = :VALUE41, ");
                query.Append(" CONTR_CEP = :VALUE42, ");
                query.Append(" CONTR_UF = :VALUE43, ");
                query.Append(" CONTR_INSCRICAO = :VALUE44, ");
                query.Append(" DATA_INICIO_CONTRATO = :VALUE45, ");
                query.Append(" CEP = :VALUE46, ");
                query.Append(" CLI_FANTASIA = :VALUE47, ");
                query.Append(" COMENTARIO = :VALUE48, ");
                query.Append(" AVULSO = :VALUE49, ");
                query.Append(" ID_RELATORIO = :VALUE50 ");
                query.Append(" WHERE ID_ESTUDO = :VALUE1");


                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = estudo.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = estudo.VendedorCliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = estudo.Tecno != null ? estudo.Tecno.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = estudo.ClienteContratante != null ? estudo.ClienteContratante.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Date));
                command.Parameters[4].Value = estudo.DataCriacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Date));
                command.Parameters[5].Value = estudo.DataVencimento;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = estudo.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Varchar));
                command.Parameters[7].Value = !String.IsNullOrEmpty(estudo.DataFimContrato) ? estudo.DataFimContrato : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                command.Parameters[8].Value = estudo.Obra;

                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                command.Parameters[9].Value = !String.IsNullOrEmpty(estudo.LocalObra) ? estudo.LocalObra.ToUpper() : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Varchar));
                command.Parameters[10].Value = !String.IsNullOrEmpty(estudo.Endereco) ? estudo.Endereco.ToUpper() : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Varchar));
                command.Parameters[11].Value = !String.IsNullOrEmpty(estudo.Numero) ? estudo.Numero.ToUpper() : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Varchar));
                command.Parameters[12].Value = !String.IsNullOrEmpty(estudo.Complemento) ? estudo.Complemento : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Varchar));
                command.Parameters[13].Value = !String.IsNullOrEmpty(estudo.Bairro) ? estudo.Bairro.ToUpper() : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Varchar));
                command.Parameters[14].Value = !String.IsNullOrEmpty(estudo.Cidade) ? estudo.Cidade.ToUpper() : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Varchar));
                command.Parameters[15].Value = !String.IsNullOrEmpty(estudo.Uf) ? estudo.Uf : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE17", NpgsqlDbType.Varchar));
                command.Parameters[16].Value = !String.IsNullOrEmpty(estudo.NumContrato) ? estudo.NumContrato.ToUpper() : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE18", NpgsqlDbType.Varchar));
                command.Parameters[17].Value = !String.IsNullOrEmpty(estudo.GrauRisco) ? estudo.GrauRisco : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE19", NpgsqlDbType.Varchar));
                command.Parameters[18].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.RazaoSocial : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE20", NpgsqlDbType.Varchar));
                command.Parameters[19].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.Cnpj : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE21", NpgsqlDbType.Varchar));
                command.Parameters[20].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.Inscricao : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE22", NpgsqlDbType.Integer));
                command.Parameters[21].Value = estudo.VendedorCliente.Cliente.EstFem != (Int32?)null ? (int?)estudo.VendedorCliente.Cliente.EstFem : (Int32?)null;

                command.Parameters.Add(new NpgsqlParameter("VALUE23", NpgsqlDbType.Integer));
                command.Parameters[22].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.EstMasc : (Int32?)null;

                command.Parameters.Add(new NpgsqlParameter("VALUE24", NpgsqlDbType.Varchar));
                command.Parameters[23].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.Jornada : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE25", NpgsqlDbType.Varchar));
                command.Parameters[24].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.Endereco : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE26", NpgsqlDbType.Varchar));
                command.Parameters[25].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.Numero : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE27", NpgsqlDbType.Varchar));
                command.Parameters[26].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.Complemento : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE28", NpgsqlDbType.Varchar));
                command.Parameters[27].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.Bairro : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE29", NpgsqlDbType.Varchar));
                command.Parameters[28].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.CidadeIbge.Nome : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE30", NpgsqlDbType.Varchar));
                command.Parameters[29].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.Email : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE31", NpgsqlDbType.Varchar));
                command.Parameters[30].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.Cep : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE32", NpgsqlDbType.Varchar));
                command.Parameters[31].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.Uf : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE33", NpgsqlDbType.Varchar));
                command.Parameters[32].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.Telefone1 : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE34", NpgsqlDbType.Varchar));
                command.Parameters[33].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.Telefone2 : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE35", NpgsqlDbType.Varchar));
                command.Parameters[34].Value = estudo.ClienteContratante != null ? estudo.ClienteContratante.RazaoSocial : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE36", NpgsqlDbType.Varchar));
                command.Parameters[35].Value = estudo.ClienteContratante != null ? estudo.ClienteContratante.Cnpj : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE37", NpgsqlDbType.Varchar));
                command.Parameters[36].Value = estudo.ClienteContratante != null ? estudo.ClienteContratante.Endereco : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE38", NpgsqlDbType.Varchar));
                command.Parameters[37].Value = estudo.ClienteContratante != null ? estudo.ClienteContratante.Numero : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE39", NpgsqlDbType.Varchar));
                command.Parameters[38].Value = estudo.ClienteContratante != null ? estudo.ClienteContratante.Complemento : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE40", NpgsqlDbType.Varchar));
                command.Parameters[39].Value = estudo.ClienteContratante != null ? estudo.ClienteContratante.Bairro : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE41", NpgsqlDbType.Varchar));
                command.Parameters[40].Value = estudo.ClienteContratante != null ? estudo.ClienteContratante.CidadeIbge.Nome : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE42", NpgsqlDbType.Varchar));
                command.Parameters[41].Value = estudo.ClienteContratante != null ? estudo.ClienteContratante.Cep : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE43", NpgsqlDbType.Varchar));
                command.Parameters[42].Value = estudo.ClienteContratante != null ? estudo.ClienteContratante.Uf : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE44", NpgsqlDbType.Varchar));
                command.Parameters[43].Value = estudo.ClienteContratante != null ? estudo.ClienteContratante.Inscricao : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE45", NpgsqlDbType.Varchar));
                command.Parameters[44].Value = !String.IsNullOrEmpty(estudo.DataInicioContrato) ? estudo.DataInicioContrato : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE46", NpgsqlDbType.Varchar));
                command.Parameters[45].Value = !String.IsNullOrEmpty(estudo.Cep) ? estudo.Cep : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE47", NpgsqlDbType.Varchar));
                command.Parameters[46].Value = estudo.VendedorCliente != null ? estudo.VendedorCliente.Cliente.Fantasia : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE48", NpgsqlDbType.Text));
                command.Parameters[47].Value = !String.IsNullOrEmpty(estudo.Comentario) ? estudo.Comentario : String.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE49", NpgsqlDbType.Boolean));
                command.Parameters[48].Value = estudo.Avulso;

                command.Parameters.Add(new NpgsqlParameter("VALUE50", NpgsqlDbType.Integer));
                command.Parameters[49].Value = estudo.IdRelatorio;

                command.ExecuteNonQuery();

                return estudo;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public Cnae findCnaeById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findCnaeById");

            Cnae cnaeRetorno = null;

            try
            {

                NpgsqlCommand command = new NpgsqlCommand("SELECT ID_CNAE FROM SEG_CNAE_ESTUDO WHERE ID_CNAE = :VALUE1", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    cnaeRetorno = new Cnae(dr.GetInt64(0));
                }

                return cnaeRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findCnaeById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 replicaEstudo(Int64 idEstudo, Int64? idCronograma, Int64 idRelatorio, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método replicaEstudo");

            StringBuilder query = new StringBuilder();

            Int64 idNovoEstudo = this.recuperaProximoId(dbConnection);

            try
            {
                query.Append(" INSERT INTO SEG_ESTUDO (ID_ESTUDO, ID_RELATORIO, COD_ESTUDO, ID_CRONOGRAMA, ");
                query.Append(" ID_SEG_REP_CLI, ID_TECNO, ID_CLIENTE_CONTRATADO, DATA_CRIACAO, DATA_VENCIMENTO, SITUACAO, ");
                query.Append(" ESTUDO, DATA_FIM_CONTRATO, OBRA, LOCAL_OBRA, ENDERECO, NUMERO, COMPLEMENTO, BAIRRO, CIDADE, UF, ");
                query.Append(" NUM_CONTRATO, GRAU_RISCO, CLI_RAZAO_SOCIAL, CLI_CNPJ, CLI_INSCRICAO, CLI_EST_FEM, CLI_EST_MASC, ");
                query.Append(" CLI_JORNADA, CLI_ENDERECO, CLI_NUMERO, CLI_COMPLEMENTO, CLI_BAIRRO, CLI_CIDADE, CLI_EMAIL, CLI_CEP, ");
                query.Append(" CLI_UF, CLI_TELEFONE1, CLI_TELEFONE2, CONTR_RAZAO_SOCIAL, CONTR_CNPJ, CONTR_ENDERECO, CONTR_NUMERO, ");
                query.Append(" CONTR_COMPLEMENTO, CONTR_BAIRRO, CONTR_CIDADE, CONTR_CEP, CONTR_UF, CONTR_INSCRICAO, DATA_INICIO_CONTRATO, ");
                query.Append(" CEP, CLI_FANTASIA, COMENTARIO, AVULSO ) (SELECT :VALUE1, :VALUE4, CAST((CAST(COD_ESTUDO AS BIGINT) + CAST(1 AS BIGINT)) AS CHARACTER VARYING), ");
                query.Append(" :VALUE3, ID_SEG_REP_CLI, ID_TECNO, ID_CLIENTE_CONTRATADO, DATA_CRIACAO, ");
                query.Append(" DATA_VENCIMENTO, 'C', ESTUDO, DATA_FIM_CONTRATO, OBRA, LOCAL_OBRA, ENDERECO, ");
                query.Append(" NUMERO, COMPLEMENTO, BAIRRO, CIDADE, UF, NUM_CONTRATO, GRAU_RISCO, CLI_RAZAO_SOCIAL, ");
                query.Append(" CLI_CNPJ, CLI_INSCRICAO, CLI_EST_FEM, CLI_EST_MASC, CLI_JORNADA, CLI_ENDERECO, CLI_NUMERO, ");
                query.Append(" CLI_COMPLEMENTO, CLI_BAIRRO, CLI_CIDADE, CLI_EMAIL, CLI_CEP, CLI_UF,");
                query.Append(" CLI_TELEFONE1, CLI_TELEFONE2, CONTR_RAZAO_SOCIAL, CONTR_CNPJ, CONTR_ENDERECO, ");
                query.Append(" CONTR_NUMERO, CONTR_COMPLEMENTO, CONTR_BAIRRO, CONTR_CIDADE, CONTR_CEP, CONTR_UF, ");
                query.Append(" CONTR_INSCRICAO, DATA_INICIO_CONTRATO, CEP, CLI_FANTASIA, COMENTARIO, AVULSO FROM SEG_ESTUDO ");
                query.Append(" WHERE ID_ESTUDO = :VALUE2)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = idNovoEstudo;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = idEstudo;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = idCronograma;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = idRelatorio;


                command.ExecuteNonQuery();

                return idNovoEstudo;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - replicaEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public Estudo findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            Estudo estudo = null;

            StringBuilder query = new StringBuilder();

            try
            {

                query.Append(" SELECT ESTUDO.ID_ESTUDO, ESTUDO.ID_RELATORIO, ESTUDO.COD_ESTUDO, ESTUDO.ID_CRONOGRAMA, ESTUDO.ID_SEG_REP_CLI, ESTUDO.ID_ENGENHEIRO, ESTUDO.ID_TECNO, ESTUDO.ID_CLIENTE_CONTRATADO, ");
                query.Append(" ESTUDO.DATA_CRIACAO, ESTUDO.DATA_VENCIMENTO, ESTUDO.DATA_FECHAMENTO, ESTUDO.SITUACAO, ESTUDO.ESTUDO, ESTUDO.DATA_FIM_CONTRATO, ESTUDO.OBRA, ");
                query.Append(" ESTUDO.LOCAL_OBRA, ESTUDO.ENDERECO, ESTUDO.NUMERO, ESTUDO.COMPLEMENTO, ESTUDO.BAIRRO, ESTUDO.CIDADE, ESTUDO.UF, ESTUDO.NUM_CONTRATO, ESTUDO.GRAU_RISCO, ESTUDO.CLI_RAZAO_SOCIAL,");
                query.Append(" ESTUDO.CLI_CNPJ, ESTUDO.CLI_INSCRICAO, ESTUDO.CLI_EST_FEM, ESTUDO.CLI_EST_MASC, ESTUDO.CLI_JORNADA, ESTUDO.CLI_ENDERECO, ESTUDO.CLI_NUMERO, ESTUDO.CLI_COMPLEMENTO,");
                query.Append(" ESTUDO.CLI_BAIRRO, ESTUDO.CLI_CIDADE, ESTUDO.CLI_EMAIL, ESTUDO.CLI_CEP, ESTUDO.CLI_UF, ESTUDO.CLI_TELEFONE1, ESTUDO.CLI_TELEFONE2, ESTUDO.CONTR_RAZAO_SOCIAL, ");
                query.Append(" ESTUDO.CONTR_CNPJ, ESTUDO.CONTR_ENDERECO, ESTUDO.CONTR_NUMERO, ESTUDO.CONTR_COMPLEMENTO, ESTUDO.CONTR_BAIRRO, ESTUDO.CONTR_CIDADE, ESTUDO.CONTR_CEP,");
                query.Append(" ESTUDO.CONTR_UF, ESTUDO.CONTR_INSCRICAO, ESTUDO.DATA_INICIO_CONTRATO, ESTUDO.CEP, ESTUDO.CLI_FANTASIA, ESTUDO.COMENTARIO, ESTUDO.AVULSO, ESTUDO.ID_PROTOCOLO, REP_CLI.ID_CLIENTE, REP_CLI.ID_VEND, REP_CLI.SITUACAO");
                query.Append(" FROM SEG_ESTUDO ESTUDO ");
                query.Append(" JOIN SEG_REP_CLI REP_CLI ON REP_CLI.ID_SEG_REP_CLI = ESTUDO.ID_SEG_REP_CLI");
                query.Append(" WHERE ESTUDO.ID_ESTUDO = :VALUE1 ");


                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;


                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    estudo = new Estudo(dr.GetInt64(0), dr.IsDBNull(1) ? (Int64?)null : dr.GetInt64(1), dr.GetString(2), dr.IsDBNull(3) ? null : new Cronograma(dr.GetInt64(3), String.Empty), dr.IsDBNull(4) ? null : new VendedorCliente(dr.GetInt64(4), dr.IsDBNull(56) ? null : new Cliente(dr.GetInt64(56)), dr.IsDBNull(57) ? null : new Vendedor(dr.GetInt64(57)), dr.IsDBNull(58) ? string.Empty : dr.GetString(58)), dr.IsDBNull(5) ? null : new Usuario(dr.GetInt64(5)), dr.IsDBNull(6) ? null : new Usuario(dr.GetInt64(6)), dr.IsDBNull(7) ? null : new Cliente(dr.GetInt64(7)), dr.GetDateTime(8), dr.IsDBNull(9) ? null : (DateTime?)dr.GetDateTime(9), dr.IsDBNull(10) ? (DateTime?)null : dr.GetDateTime(10), dr.GetString(11), dr.GetBoolean(12), dr.IsDBNull(13) ? String.Empty : dr.GetString(13), dr.IsDBNull(14) ? String.Empty : dr.GetString(14), dr.IsDBNull(15) ? String.Empty : dr.GetString(15), dr.IsDBNull(16) ? String.Empty : dr.GetString(16), dr.IsDBNull(17) ? String.Empty : dr.GetString(17), dr.IsDBNull(18) ? String.Empty : dr.GetString(18), dr.IsDBNull(19) ? String.Empty : dr.GetString(19), dr.IsDBNull(20) ? String.Empty : dr.GetString(20), dr.IsDBNull(21) ? String.Empty : dr.GetString(21), dr.IsDBNull(22) ? String.Empty : dr.GetString(22), dr.GetString(23), dr.GetString(24), dr.GetString(25), dr.GetString(26), dr.GetInt32(27), dr.GetInt32(28), dr.IsDBNull(29) ? String.Empty : dr.GetString(29), dr.IsDBNull(30) ? String.Empty : dr.GetString(30), dr.IsDBNull(31) ? String.Empty : dr.GetString(31), dr.IsDBNull(32) ? String.Empty : dr.GetString(32), dr.IsDBNull(33) ? String.Empty : dr.GetString(33), dr.IsDBNull(34) ? String.Empty : dr.GetString(34), dr.IsDBNull(35) ? String.Empty : dr.GetString(35), dr.IsDBNull(36) ? String.Empty : dr.GetString(36), dr.IsDBNull(37) ? String.Empty : dr.GetString(37), dr.IsDBNull(38) ? String.Empty : dr.GetString(38), dr.IsDBNull(39) ? String.Empty : dr.GetString(39), dr.IsDBNull(40) ? String.Empty : dr.GetString(40), dr.IsDBNull(41) ? String.Empty : dr.GetString(41), dr.IsDBNull(42) ? String.Empty : dr.GetString(42), dr.IsDBNull(43) ? String.Empty : dr.GetString(43), dr.IsDBNull(44) ? String.Empty : dr.GetString(44), dr.IsDBNull(45) ? String.Empty : dr.GetString(45), dr.IsDBNull(46) ? String.Empty : dr.GetString(46), dr.IsDBNull(47) ? String.Empty : dr.GetString(47), dr.IsDBNull(48) ? String.Empty : dr.GetString(48), dr.IsDBNull(49) ? String.Empty : dr.GetString(49), dr.IsDBNull(50) ? String.Empty : dr.GetString(50), dr.IsDBNull(51) ? String.Empty : dr.GetString(51), dr.IsDBNull(52) ? String.Empty : dr.GetString(52), dr.IsDBNull(53) ? String.Empty : dr.GetString(53), dr.GetBoolean(54), dr.IsDBNull(55) ? null : new Protocolo(dr.GetInt64(55)));
                }

                return estudo;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public string findCodigoEstudoById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findCodigoEstudoById");

            string codEstudo = string.Empty;

            try
            {

                NpgsqlCommand command = new NpgsqlCommand(" SELECT COD_ESTUDO FROM SEG_ESTUDO WHERE ID_ESTUDO = :VALUE1 ", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    codEstudo = dr.GetString(0);
                }

                return codEstudo;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findCodigoEstudoById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void delete(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            try
            {

                NpgsqlCommand command = new NpgsqlCommand(" DELETE FROM SEG_ESTUDO WHERE ID_ESTUDO = :VALUE1 ", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void changeEstado(Estudo estudo, String estado, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método changeEstado");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" UPDATE SEG_ESTUDO SET SITUACAO = :VALUE1, ID_ENGENHEIRO = :VALUE3 WHERE ID_ESTUDO = :VALUE2 ", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = estado;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = estudo.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = estudo.Engenheiro != null ? estudo.Engenheiro.Id : (Int64?)null;

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - changeEstado", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void finalizaEstudo(Estudo estudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método finalizaEstudo");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" UPDATE SEG_ESTUDO SET DATA_FECHAMENTO = NOW(), SITUACAO = 'F' WHERE ID_ESTUDO = :VALUE1 ", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = estudo.Id;

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - finalizaEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void limpaVersao(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método limpaVersao");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" DELETE FROM SEG_VERSAO WHERE ID_ESTUDO = :VALUE1 ", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - limpaVersao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void gravaVersao(Versao versao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método gravaVersao");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" INSERT INTO SEG_VERSAO ( ID_VERSAO, ID_ESTUDO, NUMERO_REVISAO, COMENTARIO, DATA_CRICAO ) ");
                query.Append(" VALUES ( NEXTVAL('SEG_VERSAO_ID_VERSAO_SEQ'), :VALUE1, :VALUE2, :VALUE3, :VALUE4) ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = versao.Estudo.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = versao.NumeroRevisao;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = versao.Comentario;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Date));
                command.Parameters[3].Value = versao.DataCriacao;

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - gravaVersao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean verificaExistePcmsoAvusoCadastrado(Cliente cliente, Cliente clienteContratado, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaExistePcmsoAvusoCadastrado");

            Boolean existePcmsoAvulsoCadastrado = false;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ESTUDO.ID_ESTUDO ");
                query.Append(" FROM SEG_ESTUDO ESTUDO JOIN SEG_REP_CLI REP_CLI ON REP_CLI.ID_SEG_REP_CLI = ESTUDO.ID_SEG_REP_CLI ");
                query.Append(" WHERE REP_CLI.ID_CLIENTE = :VALUE1");

                if (clienteContratado != null)
                    query.Append(" AND ESTUDO.ID_CLIENTE_CONTRATADO = :VALUE2");
                else
                    query.Append(" AND ESTUDO.ID_CLIENTE_CONTRATADO is null ");

                query.Append(" AND ESTUDO.AVULSO = TRUE");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = clienteContratado != null ? clienteContratado.Id : (Int64?)null;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    existePcmsoAvulsoCadastrado = true;
                }

                return existePcmsoAvulsoCadastrado;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaExistePcmsoAvusoCadastrado", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean pcmsoIsUnidade(Estudo pcmso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método pcmsoIsUnidade");

            Boolean retorno = false;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ESTUDO.* FROM SEG_ESTUDO ESTUDO ");
                query.Append(" JOIN SEG_CLIENTE CLIENTE ON ESTUDO.ID_CLIENTE_CONTRATADO = CLIENTE.ID_CLIENTE AND CLIENTE.UNIDADE = TRUE ");
                query.Append(" WHERE ESTUDO.ID_ESTUDO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = pcmso == null ? null : pcmso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    retorno = true;
                }

                return retorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - pcmsoIsUnidade", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void insertInProtocolo(Protocolo protocolo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertInProtocolo");

            try
            {

                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_ESTUDO SET ID_PROTOCOLO = :VALUE1 WHERE ID_ESTUDO = :VALUE2 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = protocolo.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = protocolo.Pcmso != null ? protocolo.Pcmso.Id : protocolo.Ppra != null ? protocolo.Ppra.Id : (Int64?)null;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertInProtocolo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findByFilter(Estudo pcmso, DateTime? dataCriacaoFinal, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByFilter");

            try
            {
                StringBuilder query = new StringBuilder();

                if (pcmso.isNullForFilter())
                {
                    query.Append(" SELECT ESTUDO.ID_ESTUDO, FORMATA_COD_ESTUDO(ESTUDO.COD_ESTUDO) AS CODIGO_ESTUDO, ESTUDO.DATA_CRIACAO, ESTUDO.DATA_VENCIMENTO, ESTUDO.DATA_FECHAMENTO, ESTUDO.SITUACAO, CLIENTE.RAZAO_SOCIAL AS CLIENTE, CONTRATADO.RAZAO_SOCIAL AS CONTRATANTE, ESTUDO.OBRA, FORMATA_CNPJ(CLIENTE.CNPJ) AS CNPJ_CLIENTE, FORMATA_CNPJ(CONTRATADO.CNPJ) AS CNPJ_CONTRATANTE, AVULSO FROM SEG_ESTUDO ESTUDO ");
                    query.Append(" JOIN SEG_REP_CLI REP_CLI ON REP_CLI.ID_SEG_REP_CLI = ESTUDO.ID_SEG_REP_CLI ");
                    query.Append(" JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = REP_CLI.ID_CLIENTE ");
                    query.Append(" LEFT JOIN SEG_CLIENTE CONTRATADO ON CONTRATADO.ID_CLIENTE = ESTUDO.ID_CLIENTE_CONTRATADO ");
                    query.Append(" WHERE ESTUDO.ESTUDO = TRUE ");
                    query.Append(" ORDER BY ESTUDO.COD_ESTUDO ASC");
                }
                else
                {
                    query.Append(" SELECT ESTUDO.ID_ESTUDO, FORMATA_COD_ESTUDO(ESTUDO.COD_ESTUDO) AS CODIGO_ESTUDO, ESTUDO.DATA_CRIACAO, ESTUDO.DATA_VENCIMENTO, ESTUDO.DATA_FECHAMENTO, ESTUDO.SITUACAO, CLIENTE.RAZAO_SOCIAL AS CLIENTE, CONTRATADO.RAZAO_SOCIAL AS CONTRATANTE, ESTUDO.OBRA, FORMATA_CNPJ(CLIENTE.CNPJ) AS CNPJ_CLIENTE, FORMATA_CNPJ(CONTRATADO.CNPJ) AS CNPJ_CONTRATANTE,  AVULSO FROM SEG_ESTUDO ESTUDO ");
                    query.Append(" JOIN SEG_REP_CLI REP_CLI ON REP_CLI.ID_SEG_REP_CLI = ESTUDO.ID_SEG_REP_CLI ");
                    query.Append(" JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = REP_CLI.ID_CLIENTE ");
                    query.Append(" LEFT JOIN SEG_CLIENTE CONTRATADO ON CONTRATADO.ID_CLIENTE = ESTUDO.ID_CLIENTE_CONTRATADO ");
                    query.Append(" WHERE ESTUDO.ESTUDO = TRUE ");

                    if (!String.IsNullOrWhiteSpace(pcmso.CodigoEstudo))
                        query.Append(" AND ESTUDO.COD_ESTUDO = :VALUE1 ");

                    if (pcmso.DataCriacao != null)
                        query.Append(" AND ESTUDO.DATA_CRIACAO BETWEEN :VALUE2 AND :VALUE3 ");

                    if (pcmso.Engenheiro != null)
                        query.Append(" AND ESTUDO.ID_ENGENHEIRO = :VALUE4 ");

                    if (pcmso.Tecno != null)
                        query.Append(" AND ESTUDO.ID_TECNO = :VALUE5 ");

                    if (!String.IsNullOrWhiteSpace(pcmso.Situacao))
                    {
                        if (string.Equals(pcmso.Situacao, "T"))
                            query.Append(" AND ESTUDO.AVULSO = TRUE ");
                        else
                            query.Append(" AND ESTUDO.SITUACAO = :VALUE6 AND ESTUDO.AVULSO = FALSE ");
                    }
                    else
                        query.Append(" AND ESTUDO.SITUACAO <> 'I' AND ESTUDO.AVULSO = FALSE ");

                    if (pcmso.VendedorCliente != null)
                        query.Append(" AND ESTUDO.ID_SEG_REP_CLI IN (SELECT ID_SEG_REP_CLI FROM SEG_REP_CLI WHERE ID_CLIENTE = :VALUE7) ");

                    if (pcmso.ClienteContratante != null)
                        query.Append(" AND ESTUDO.ID_CLIENTE_CONTRATADO = :VALUE8");

                    query.Append(" ORDER BY ESTUDO.COD_ESTUDO ASC");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[0].Value = pcmso.CodigoEstudo;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Date));
                adapter.SelectCommand.Parameters[1].Value = pcmso.DataCriacao;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Date));
                adapter.SelectCommand.Parameters[2].Value = dataCriacaoFinal;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[3].Value = pcmso.Engenheiro != null ? pcmso.Engenheiro.Id : null;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[4].Value = pcmso.Tecno != null ? pcmso.Tecno.Id : null;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[5].Value = pcmso.Situacao;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[6].Value = pcmso.VendedorCliente != null ? pcmso.VendedorCliente.Cliente.Id : null;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[7].Value = pcmso.ClienteContratante != null ? pcmso.ClienteContratante.Id : null;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Pcmso");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void gravaCronograma(Cronograma cronograma, Estudo pcmso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método gravaCronograma");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_ESTUDO SET ID_CRONOGRAMA = :VALUE1 WHERE ID_ESTUDO = :VALUE2 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cronograma.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = pcmso.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - gravaCronograma", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public LinkedList<Estudo> findLastPcmsoByCliente(Cliente cliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findLastPcmsoByCliente");

            LinkedList<Estudo> pcmsosFinalizados = new LinkedList<Estudo>();

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ESTUDO.ID_ESTUDO, ESTUDO.ID_RELATORIO, ESTUDO.COD_ESTUDO, ESTUDO.ID_CRONOGRAMA, ESTUDO.ID_SEG_REP_CLI, ESTUDO.ID_ENGENHEIRO, ESTUDO.ID_TECNO, ");
                query.Append(" ESTUDO.ID_CLIENTE_CONTRATADO, ESTUDO.DATA_CRIACAO, ESTUDO.DATA_VENCIMENTO, ESTUDO.DATA_FECHAMENTO, ESTUDO.SITUACAO, ");
                query.Append(" ESTUDO.ESTUDO, ESTUDO.DATA_FIM_CONTRATO, ESTUDO.OBRA, ESTUDO.LOCAL_OBRA, ESTUDO.ENDERECO, ESTUDO.NUMERO, ESTUDO.COMPLEMENTO, ");
                query.Append(" ESTUDO.BAIRRO, ESTUDO.CIDADE, ESTUDO.UF, ESTUDO.NUM_CONTRATO, ESTUDO.GRAU_RISCO, ESTUDO.CLI_RAZAO_SOCIAL,");
                query.Append(" ESTUDO.CLI_CNPJ, ESTUDO.CLI_INSCRICAO, ESTUDO.CLI_EST_FEM, ESTUDO.CLI_EST_MASC, ESTUDO.CLI_JORNADA, ESTUDO.CLI_ENDERECO, ");
                query.Append(" ESTUDO.CLI_NUMERO, ESTUDO.CLI_COMPLEMENTO, ESTUDO.CLI_BAIRRO, ESTUDO.CLI_CIDADE, ESTUDO.CLI_EMAIL, ESTUDO.CLI_CEP, ");
                query.Append(" ESTUDO.CLI_UF, ESTUDO.CLI_TELEFONE1, ESTUDO.CLI_TELEFONE2, ESTUDO.CONTR_RAZAO_SOCIAL, ");
                query.Append(" ESTUDO.CONTR_CNPJ, ESTUDO.CONTR_ENDERECO, ESTUDO.CONTR_NUMERO, ESTUDO.CONTR_COMPLEMENTO, ESTUDO.CONTR_BAIRRO, ");
                query.Append(" ESTUDO.CONTR_CIDADE, ESTUDO.CONTR_CEP, ESTUDO.CONTR_UF, ESTUDO.CONTR_INSCRICAO, ESTUDO.DATA_INICIO_CONTRATO, ");
                query.Append(" ESTUDO.CEP, ESTUDO.CLI_FANTASIA, ESTUDO.COMENTARIO, ESTUDO.AVULSO, ESTUDO.ID_PROTOCOLO, REP_CLI.ID_CLIENTE, REP_CLI.ID_VEND, REP_CLI.SITUACAO ");
                query.Append(" FROM SEG_ESTUDO ESTUDO ");
                query.Append(" JOIN SEG_REP_CLI REP_CLI ON REP_CLI.ID_SEG_REP_CLI = ESTUDO.ID_SEG_REP_CLI ");
                query.Append(" WHERE ESTUDO.SITUACAO = 'F' AND ESTUDO.ESTUDO = TRUE AND REP_CLI.ID_CLIENTE = :VALUE1 ");
                query.Append(" ORDER BY ESTUDO.DATA_FECHAMENTO DESC ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;


                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    pcmsosFinalizados.AddLast(new Estudo(dr.GetInt64(0), dr.IsDBNull(1) ? (Int64?)null : dr.GetInt64(1), dr.GetString(2), dr.IsDBNull(3) ? null : new Cronograma(dr.GetInt64(3), String.Empty), dr.IsDBNull(4) ? null : new VendedorCliente(dr.GetInt64(4), dr.IsDBNull(56) ? null : new Cliente(dr.GetInt64(56)), dr.IsDBNull(57) ? null : new Vendedor(dr.GetInt64(57)), dr.IsDBNull(58) ? string.Empty : dr.GetString(58)), dr.IsDBNull(5) ? null : new Usuario(dr.GetInt64(5)), dr.IsDBNull(6) ? null : new Usuario(dr.GetInt64(6)), dr.IsDBNull(7) ? null : new Cliente(dr.GetInt64(7)), dr.GetDateTime(8), dr.IsDBNull(9) ? null : (DateTime?)dr.GetDateTime(9), dr.IsDBNull(10) ? (DateTime?)null : dr.GetDateTime(10), dr.GetString(11), dr.GetBoolean(12), dr.IsDBNull(13) ? String.Empty : dr.GetString(13), dr.IsDBNull(14) ? String.Empty : dr.GetString(14), dr.IsDBNull(15) ? String.Empty : dr.GetString(15), dr.IsDBNull(16) ? String.Empty : dr.GetString(16), dr.IsDBNull(17) ? String.Empty : dr.GetString(17), dr.IsDBNull(18) ? String.Empty : dr.GetString(18), dr.IsDBNull(19) ? String.Empty : dr.GetString(19), dr.IsDBNull(20) ? String.Empty : dr.GetString(20), dr.IsDBNull(21) ? String.Empty : dr.GetString(21), dr.IsDBNull(22) ? String.Empty : dr.GetString(22), dr.GetString(23), dr.GetString(24), dr.GetString(25), dr.GetString(26), dr.GetInt32(27), dr.GetInt32(28), dr.IsDBNull(29) ? String.Empty : dr.GetString(29), dr.IsDBNull(30) ? String.Empty : dr.GetString(30), dr.IsDBNull(31) ? String.Empty : dr.GetString(31), dr.IsDBNull(32) ? String.Empty : dr.GetString(32), dr.IsDBNull(33) ? String.Empty : dr.GetString(33), dr.IsDBNull(34) ? String.Empty : dr.GetString(34), dr.IsDBNull(35) ? String.Empty : dr.GetString(35), dr.IsDBNull(36) ? String.Empty : dr.GetString(36), dr.IsDBNull(37) ? String.Empty : dr.GetString(37), dr.IsDBNull(38) ? String.Empty : dr.GetString(38), dr.IsDBNull(39) ? String.Empty : dr.GetString(39), dr.IsDBNull(40) ? String.Empty : dr.GetString(40), dr.IsDBNull(41) ? String.Empty : dr.GetString(41), dr.IsDBNull(42) ? String.Empty : dr.GetString(42), dr.IsDBNull(43) ? String.Empty : dr.GetString(43), dr.IsDBNull(44) ? String.Empty : dr.GetString(44), dr.IsDBNull(45) ? String.Empty : dr.GetString(45), dr.IsDBNull(46) ? String.Empty : dr.GetString(46), dr.IsDBNull(47) ? String.Empty : dr.GetString(47), dr.IsDBNull(48) ? String.Empty : dr.GetString(48), dr.IsDBNull(49) ? String.Empty : dr.GetString(49), dr.IsDBNull(50) ? String.Empty : dr.GetString(50), dr.IsDBNull(51) ? String.Empty : dr.GetString(51), dr.IsDBNull(52) ? String.Empty : dr.GetString(52), dr.IsDBNull(53) ? String.Empty : dr.GetString(53), dr.GetBoolean(54), dr.IsDBNull(55) ? null : new Protocolo(dr.GetInt64(55))));
                }

                return pcmsosFinalizados;


            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findLastPcmsoByCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public bool isPcmsoUsedByAso(long id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método isPcmsoUsedByAso");

            bool result = false;

            StringBuilder query = new StringBuilder();


            try
            {
                query.Append(" SELECT ATENDIMENTO.* FROM SEG_ASO ATENDIMENTO ");
                query.Append(" WHERE ATENDIMENTO.ID_ESTUDO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;


                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    result = true;
                }

                return result;


            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - isPcmsoUsedByAso", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public void disablePcmso(long id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método disablePcmso");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_ESTUDO SET SITUACAO = 'I' WHERE ID_ESTUDO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - disablePcmso", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }



    }
}
