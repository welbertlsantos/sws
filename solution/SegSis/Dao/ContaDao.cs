﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;
using SWS.Entidade;
using NpgsqlTypes;
using System.Data;
using SWS.View.Resources;

namespace SWS.Dao
{
    class ContaDao : IContaDao
    {

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_CONTA_ID_CONTA_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Conta insert(Conta conta, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertConta");

            try
            {
                conta.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_CONTA (ID_CONTA, ID_BANCO, NOME, AGENCIA_NUMERO, AGENCIA_DIGITO, CONTA_NUMERO, ");
                query.Append(" CONTA_DIGITO, PROXIMO_NUMERO, NUMERO_REMESSA, CARTEIRA, CONVENIO, VARIACAO_CARTEIRA, ");
                query.Append(" CODIGO_CEDENTE_BANCO, REGISTRO, SITUACAO, PRIVADO ) ");
                query.Append(" VALUES ( :VALUE1, :VALUE2, :VALUE15, :VALUE3, :VALUE4, :VALUE5, :VALUE6, :VALUE7, :VALUE8, :VALUE9, ");
                query.Append(" :VALUE10, :VALUE11, :VALUE12, :VALUE13, :VALUE14, :VALUE16) ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = conta.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = conta.Banco.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = conta.AgenciaNumero;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = conta.AgenciaDigito;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = conta.ContaNumero;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = conta.ContaDigito;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Integer));
                command.Parameters[6].Value = conta.ProximoNumero;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Varchar));
                command.Parameters[7].Value = conta.NumeroRemessa;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                command.Parameters[8].Value = conta.Carteira;

                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                command.Parameters[9].Value = conta.Convenio;

                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Varchar));
                command.Parameters[10].Value = conta.VariacaoCarteira;

                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Varchar));
                command.Parameters[11].Value = conta.CodigoCedenteBanco;

                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Boolean));
                command.Parameters[12].Value = conta.Registro;


                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Varchar));
                command.Parameters[13].Value = conta.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Varchar));
                command.Parameters[14].Value = conta.Nome;

                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Boolean));
                command.Parameters[15].Value = conta.Privado;

                command.ExecuteNonQuery();

                return conta;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertConta", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Conta update(Conta conta, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateConta");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_CONTA SET ");
                query.Append(" ID_BANCO = :VALUE2, ");
                query.Append(" NOME = :VALUE15, ");
                query.Append(" AGENCIA_NUMERO = :VALUE3, ");
                query.Append(" AGENCIA_DIGITO = :VALUE4, ");
                query.Append(" CONTA_NUMERO = :VALUE5, ");
                query.Append(" CONTA_DIGITO = :VALUE6, ");
                query.Append(" PROXIMO_NUMERO = :VALUE7, ");
                query.Append(" NUMERO_REMESSA = :VALUE8, ");
                query.Append(" CARTEIRA = :VALUE9,  ");
                query.Append(" CONVENIO = :VALUE10,  ");
                query.Append(" VARIACAO_CARTEIRA = :VALUE11,  ");
                query.Append(" CODIGO_CEDENTE_BANCO = :VALUE12, ");
                query.Append(" REGISTRO = :VALUE13, ");
                query.Append(" SITUACAO = :VALUE14  ");
                query.Append(" WHERE ID_CONTA = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = conta.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = conta.Banco.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = conta.AgenciaNumero;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = conta.AgenciaDigito;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = conta.ContaNumero;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = conta.ContaDigito;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Integer));
                command.Parameters[6].Value = conta.ProximoNumero;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Varchar));
                command.Parameters[7].Value = conta.NumeroRemessa;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                command.Parameters[8].Value = conta.Carteira;

                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                command.Parameters[9].Value = conta.Convenio;

                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Varchar));
                command.Parameters[10].Value = conta.VariacaoCarteira;

                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Varchar));
                command.Parameters[11].Value = conta.CodigoCedenteBanco;

                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Boolean));
                command.Parameters[12].Value = conta.Registro;

                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Varchar));
                command.Parameters[13].Value = conta.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Varchar));
                command.Parameters[14].Value = conta.Nome;

                command.ExecuteNonQuery();

                return conta;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateConta", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Conta findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findContaById");

            Conta conta = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT CONTA.ID_CONTA, BANCO.ID_BANCO, BANCO.NOME, BANCO.CODIGO_BANCO, BANCO.CAIXA, BANCO.SITUACAO,  ");
                query.Append(" CONTA.NOME, CONTA.AGENCIA_NUMERO, CONTA.AGENCIA_DIGITO, CONTA.CONTA_NUMERO, CONTA.CONTA_DIGITO, CONTA.PROXIMO_NUMERO, ");
                query.Append(" CONTA.NUMERO_REMESSA, CONTA.CARTEIRA, CONTA.CONVENIO, CONTA.VARIACAO_CARTEIRA, CONTA.CODIGO_CEDENTE_BANCO, CONTA.REGISTRO, ");
                query.Append(" CONTA.SITUACAO, CONTA.PRIVADO, BANCO.PRIVADO, BANCO.BOLETO ");
                query.Append(" FROM SEG_BANCO BANCO, SEG_CONTA CONTA WHERE ");
                query.Append(" CONTA.ID_BANCO = BANCO.ID_BANCO AND CONTA.ID_CONTA = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    conta = new Conta(dr.GetInt64(0), dr.IsDBNull(1) ? null : new Banco(dr.GetInt64(1), dr.GetString(2), dr.IsDBNull(3) ? string.Empty : dr.GetString(3), dr.GetBoolean(4), dr.GetString(5), dr.GetBoolean(20), dr.GetBoolean(21)), dr.GetString(6), dr.GetString(7), dr.GetString(8), dr.GetString(9), dr.GetString(10), dr.IsDBNull(11) ? (Int64?)null : dr.GetInt64(11), dr.GetString(12), dr.GetString(13), dr.GetString(14), dr.GetString(15), dr.GetString(16), dr.GetBoolean(17), dr.GetString(18), dr.GetBoolean(19)); 
                }

                return conta;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findContaById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void mudaSituacaoConta(Conta conta, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método mudaSituacaoConta");
            
            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_CONTA SET ");
                query.Append(" SITUACAO = :VALUE2 ");
                query.Append(" WHERE ID_CONTA = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = conta.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = conta.Situacao;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - mudaSituacaoConta", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findByFilter(Conta conta, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findContaByFilter");

            try
            {
                StringBuilder query = new StringBuilder();

                if (conta.Nome != null)
                {

                    if (String.IsNullOrEmpty(conta.Nome))
                    {
                        query.Append(" SELECT C.ID_CONTA, C.NOME, C.AGENCIA_NUMERO, C.AGENCIA_DIGITO, C.CONTA_NUMERO, C.CONTA_DIGITO, ");
                        query.Append(" C.PROXIMO_NUMERO, C.NUMERO_REMESSA, C.CARTEIRA, C.CONVENIO, C.VARIACAO_CARTEIRA, C.CODIGO_CEDENTE_BANCO, ");
                        query.Append(" C.REGISTRO, C.SITUACAO, B.ID_BANCO, B.NOME, B.CODIGO_BANCO, B.CAIXA, B.SITUACAO, C.PRIVADO, ");
                        query.Append(" B.PRIVADO, B.BOLETO FROM SEG_CONTA C ");
                        query.Append(" JOIN SEG_BANCO B ON B.ID_BANCO = C.ID_BANCO ");

                        if (conta.Banco != null)
                            query.Append(" AND B.ID_BANCO = :VALUE2 ");

                        if (!String.IsNullOrEmpty(conta.Situacao))
                            query.Append(" AND C.SITUACAO = :VALUE3 ");
                    }
                    else
                    {
                        query.Append(" SELECT C.ID_CONTA, C.NOME, C.AGENCIA_NUMERO, C.AGENCIA_DIGITO, C.CONTA_NUMERO, C.CONTA_DIGITO, ");
                        query.Append(" C.PROXIMO_NUMERO, C.NUMERO_REMESSA, C.CARTEIRA, C.CONVENIO, C.VARIACAO_CARTEIRA, C.CODIGO_CEDENTE_BANCO, ");
                        query.Append(" C.REGISTRO, C.SITUACAO, B.ID_BANCO, B.NOME, B.CODIGO_BANCO, B.CAIXA, B.SITUACAO, C.PRIVADO, B.PRIVADO, B.BOLETO FROM SEG_CONTA C ");
                        query.Append(" JOIN SEG_BANCO B ON B.ID_BANCO = C.ID_BANCO ");

                        if (conta.Banco != null)
                            query.Append(" AND B.ID_BANCO = :VALUE2 ");

                        query.Append(" WHERE C.NOME LIKE :VALUE1 ");

                        if (!String.IsNullOrEmpty(conta.Situacao))
                            query.Append(" AND C.SITUACAO = :VALUE3 ");
                    }
                }
                else
                {
                    query.Append(" SELECT C.ID_CONTA, C.NOME, C.AGENCIA_NUMERO, C.AGENCIA_DIGITO, C.CONTA_NUMERO, C.CONTA_DIGITO, ");
                    query.Append(" C.PROXIMO_NUMERO, C.NUMERO_REMESSA, C.CARTEIRA, C.CONVENIO, C.VARIACAO_CARTEIRA, C.CODIGO_CEDENTE_BANCO, ");
                    query.Append(" C.REGISTRO, C.SITUACAO, B.ID_BANCO, B.NOME, B.CODIGO_BANCO, B.CAIXA, B.SITUACAO, C.PRIVADO, B.PRIVADO, B.BOLETO FROM SEG_CONTA C ");
                    query.Append(" JOIN SEG_BANCO B ON B.ID_BANCO = C.ID_BANCO AND B.ID_BANCO = :VALUE2 ");

                    if (!String.IsNullOrEmpty(conta.Situacao))
                        query.Append(" WHERE C.SITUACAO = :VALUE3 ");
                }


                query.Append(" ORDER BY C.NOME ASC ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = string.IsNullOrEmpty(conta.Nome) ? string.Empty : "%" + conta.Nome + "%";
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[1].Value = conta.Banco != null ? conta.Banco.Id : null;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[2].Value = String.IsNullOrEmpty(conta.Situacao) ? null : conta.Situacao;
                
                DataSet ds = new DataSet();
                adapter.Fill(ds, "Contas");
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findContaByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Conta findByNome(String nome, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findContaByNome");

            Conta conta = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT C.ID_CONTA, C.NOME, C.AGENCIA_NUMERO, C.AGENCIA_DIGITO, C.CONTA_NUMERO, C.CONTA_DIGITO,  ");
                query.Append(" C.PROXIMO_NUMERO, C.NUMERO_REMESSA, C.CARTEIRA, C.CONVENIO, C.VARIACAO_CARTEIRA, C.CODIGO_CEDENTE_BANCO, ");
                query.Append(" C.REGISTRO, C.SITUACAO, B.ID_BANCO, B.NOME, B.CODIGO_BANCO, B.CAIXA, B.SITUACAO, C.PRIVADO, B.PRIVADO, B.BOLETO FROM SEG_CONTA C ");
                query.Append(" JOIN SEG_BANCO B ON B.ID_BANCO = C.ID_BANCO ");
                query.Append(" WHERE C.NOME = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = nome;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    conta = new Conta(dr.GetInt64(0), dr.IsDBNull(14) ? null : new Banco(dr.GetInt64(14), dr.GetString(15), dr.IsDBNull(16) ? string.Empty : dr.GetString(16), dr.GetBoolean(17), dr.GetString(18), dr.GetBoolean(20), dr.GetBoolean(21)), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.GetString(4), dr.GetString(5), dr.IsDBNull(6) ? (Int64?) null : dr.GetInt64(6), dr.GetString(7), dr.GetString(8), dr.GetString(9), dr.GetString(10), dr.GetString(11), dr.GetBoolean(12), dr.GetString(13), dr.GetBoolean(19)); 
                }

                return conta;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findContaByNome", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean verificaPodeAlterarConta(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaPodeAlterarConta");

            Boolean podeAlterar = true;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT COUNT(*) AS QUANTIDADE FROM SEG_CRC WHERE ID_CONTA = :value1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    if (dr.GetInt64(0) > 0)
                    {
                        podeAlterar = false;
                    }
                }

                return podeAlterar;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaPodeAlterarConta", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void incrementaProximoNumero(Conta conta, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incrementaProximoNumero");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_CONTA SET ");
                query.Append(" PROXIMO_NUMERO =  ");
                query.Append(" (SELECT PROXIMO_NUMERO FROM SEG_CONTA WHERE ID_CONTA = :VALUE1 ) + 1 ");
                query.Append(" WHERE ID_CONTA = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = conta.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incrementaProximoNumero", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
