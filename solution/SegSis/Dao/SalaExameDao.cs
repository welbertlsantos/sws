﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;
using NpgsqlTypes;
using System.Data;
using SWS.View.Resources;

namespace SWS.Dao
{
    class SalaExameDao : ISalaExameDao
    {
        public SalaExame insert(SalaExame salaExame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirSalaExame");

            StringBuilder query = new StringBuilder();

            try
            {
                salaExame.Id = recuperaProximoId(dbConnection);

                query.Append(" INSERT INTO SEG_SALA_EXAME (ID_SALA_EXAME, ID_EXAME, ID_SALA, TEMPO_MEDIO_ATENDIMENTO, SITUACAO)");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = salaExame.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = salaExame.Exame.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = salaExame.Sala.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = salaExame.TempoMedioAtendimento;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = salaExame.Situacao;

                command.ExecuteNonQuery();

                return salaExame;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirSalaExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_SALA_EXAME_ID_SALA_EXAME_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findExameNotInSala(Exame exame, Sala sala, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findExameNotInSalaExame");

            try
            {
                StringBuilder query = new StringBuilder();

                if (String.IsNullOrWhiteSpace(exame.Descricao.Trim()) && exame.Laboratorio == false)
                {
                    query.Append(" select e.id_exame, e.descricao, e.situacao, e.laboratorio, e.preco, e.prioridade, e.custo, e.externo, e.libera_documento from seg_exame e ");
                    query.Append(" where e.id_exame not in (select id_exame from seg_sala_exame where id_sala = :value1 and situacao = 'A' ) ");
                    query.Append(" and e.situacao = :value3 order by e.descricao ");
                }
                else
                {
                    query.Append(" select e.id_exame, e.descricao, e.situacao, e.laboratorio, e.preco, e.prioridade, e.custo, e.externo, e.libera_documento from seg_exame e ");
                    query.Append(" where e.id_exame not in (select id_exame from seg_sala_exame where id_sala = :value1 and situacao = 'A' )");
                    query.Append(" and e.situacao = :value3 ");

                    if (!String.IsNullOrWhiteSpace(exame.Descricao.Trim()))
                    {
                        query.Append(" and e.descricao like :value2 ");
                    }

                    if (exame.Laboratorio == true)
                    {
                        query.Append(" and e.laboratorio = :value4 ");
                    }

                    query.Append(" order by e.descricao asc ");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("value2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("value3", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("value4", NpgsqlDbType.Boolean));

                adapter.SelectCommand.Parameters[0].Value = sala.Id;
                adapter.SelectCommand.Parameters[1].Value = exame.Descricao + '%';
                adapter.SelectCommand.Parameters[2].Value = ApplicationConstants.ATIVO;
                adapter.SelectCommand.Parameters[3].Value = exame.Laboratorio;

                DataSet ds = new DataSet();
                adapter.Fill(ds, "Exames");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findExameNotInSalaExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void delete(SalaExame salaExame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteSalaExame");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" DELETE FROM SEG_SALA_EXAME WHERE ID_SALA_EXAME = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = salaExame.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteSalaExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public SalaExame update(SalaExame salaExame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateSalaExame");

            StringBuilder query = new StringBuilder();

            try
            {

                query.Append(" UPDATE SEG_SALA_EXAME SET TEMPO_MEDIO_ATENDIMENTO = :VALUE2, SITUACAO = :VALUE3 ");
                query.Append(" WHERE ID_SALA_EXAME = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = salaExame.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = salaExame.TempoMedioAtendimento;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = salaExame.Situacao;

                command.ExecuteNonQuery();

                return salaExame;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateSalaExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean verificaPodeExcluirSalaExame(SalaExame salaExame, NpgsqlConnection dbConnection)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaPodeExcluirSalaExame");

            Boolean retorno = Convert.ToBoolean(false);

            try
            {
                NpgsqlCommand commandPcmso = new NpgsqlCommand("SELECT ID_SALA_EXAME FROM SEG_GHE_FONTE_AGENTE_EXAME_ASO WHERE ID_SALA_EXAME = :VALUE1", dbConnection);

                commandPcmso.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                commandPcmso.Parameters[0].Value = salaExame.Id;

                NpgsqlDataReader drPcmso = commandPcmso.ExecuteReader();

                while (drPcmso.Read())
                {
                    retorno = true;
                }

                NpgsqlCommand commandExameExtra = new NpgsqlCommand("SELECT ID_SALA_EXAME FROM SEG_CLIENTE_FUNCAO_EXAME_ASO WHERE ID_SALA_EXAME = :VALUE1", dbConnection);

                commandExameExtra.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                commandExameExtra.Parameters[0].Value = salaExame.Id;

                NpgsqlDataReader drExameExtra = commandExameExtra.ExecuteReader();

                while (drExameExtra.Read())
                {
                    retorno = Convert.ToBoolean(true);
                }

                return retorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaPodeExcluirSalaExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public HashSet<SalaExame> buscaSalasAtendemExame(Exame exame, Boolean salaVip, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método buscaSalasAtendemExame");

            HashSet<SalaExame> salaExame = new HashSet<SalaExame>();

            StringBuilder query = new StringBuilder();

            try
            {

                query.Append(" SELECT SALA_EXAME.ID_SALA_EXAME, SALA_EXAME.ID_EXAME, SALA_EXAME.ID_SALA, SALA_EXAME.TEMPO_MEDIO_ATENDIMENTO, ");
                query.Append(" SALA_EXAME.TEMPO_MEDIO_ATENDIMENTO_CALCULADO, SALA_EXAME.SITUACAO, ");
                query.Append(" EXAME.DESCRICAO, EXAME.SITUACAO, EXAME.LABORATORIO, EXAME.PRECO, EXAME.PRIORIDADE, ");
                query.Append(" EXAME.CUSTO, EXAME.EXTERNO, SALA.DESCRICAO, SALA.ANDAR, SALA.SITUACAO, SALA.VIP, SALA.ID_EMPRESA, SALA.SLUG_SALA, SALA.QTDE_CHAMADA " );
                query.Append(" FROM SEG_SALA_EXAME SALA_EXAME ");
                query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = SALA_EXAME.ID_EXAME");
                query.Append(" JOIN SEG_SALA SALA ON SALA.ID_SALA = SALA_EXAME.ID_SALA ");
                query.Append(" WHERE SALA_EXAME.ID_EXAME = :VALUE1 ");
                query.Append(" AND SALA.VIP = :VALUE2 ");
                query.Append(" AND SALA_EXAME.SITUACAO = 'A' AND SALA.SITUACAO = 'A' ");

                NpgsqlCommand commandPcmso = new NpgsqlCommand(query.ToString(), dbConnection);

                commandPcmso.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                commandPcmso.Parameters[0].Value = exame.Id;

                commandPcmso.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Boolean));
                commandPcmso.Parameters[1].Value = salaVip;

                NpgsqlDataReader dr = commandPcmso.ExecuteReader();

                while (dr.Read())
                {
                    salaExame.Add(new SalaExame(dr.GetInt64(0), new Exame(dr.GetInt64(1)), new Sala(dr.GetInt64(2), dr.GetString(13), dr.GetString(15), dr.GetInt32(14),  dr.GetBoolean(16), new Empresa(dr.GetInt64(17)), dr.GetString(18), dr.GetInt32(19)), dr.GetInt32(3), dr.IsDBNull(4) ? (int?)null : dr.GetInt32(4), dr.GetString(5)));
                }

                return salaExame;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - buscaSalasAtendemExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public HashSet<Exame> buscaExamesBySala(Sala sala, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método buscaExamesBySala");

            HashSet<Exame> exames = new HashSet<Exame>();

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT EXAME.ID_EXAME, EXAME.DESCRICAO, EXAME.SITUACAO, EXAME.LABORATORIO, EXAME.PRECO, EXAME.PRIORIDADE, EXAME.CUSTO, EXAME.EXTERNO, EXAME.LIBERA_DOCUMENTO, EXAME.CODIGO_TUSS, EXAME.EXAME_COMPLEMENTAR, EXAME.PERIODO_VENCIMENTO, EXAME.PADRAO_CONTRATO ");
                query.Append(" FROM SEG_EXAME EXAME ");
                query.Append(" JOIN SEG_SALA_EXAME SALA_EXAME ON SALA_EXAME.ID_EXAME = EXAME.ID_EXAME ");
                query.Append(" WHERE SALA_EXAME.ID_SALA = :VALUE1 AND SALA_EXAME.SITUACAO = 'A' ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = sala.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    exames.Add(new Exame(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetBoolean(3), dr.GetDecimal(4), dr.GetInt32(5), dr.GetDecimal(6), dr.GetBoolean(7), dr.GetBoolean(8), dr.IsDBNull(9) ? string.Empty : dr.GetString(9), dr.GetBoolean(10), dr.IsDBNull(11) ? (int?)null : dr.GetInt32(11), dr.GetBoolean(12)));
                }

                return exames;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - buscaExamesBySala", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public SalaExame buscaSalaExameBySala(Sala sala, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método buscaSalaExamesBySala");

            SalaExame salaExame = new SalaExame();

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT SALA_EXAME.ID_SALA_EXAME, SALA_EXAME.ID_EXAME, SALA_EXAME.ID_SALA, SALA_EXAME.TEMPO_MEDIO_ATENDIMENTO, ");
                query.Append(" SALA_EXAME.TEMPO_MEDIO_ATENDIMENTO_CALCULADO, SALA_EXAME.SITUACAO, ");
                query.Append(" EXAME.ID_EXAME, EXAME.DESCRICAO, EXAME.SITUACAO, EXAME.LABORATORIO, EXAME.PRECO, EXAME.PRIORIDADE, EXAME.CUSTO, EXAME.EXTERNO, EXAME.LIBERA_DOCUMENTO, EXAME.CODIGO_TUSS, EXAME.EXAME_COMPLEMENTAR, EXAME.PERIODO_VENCIMENTO, EXAME.PADRAO_CONTRATO ");
                query.Append(" FROM SEG_SALA_EXAME SALA_EXAME ");
                query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = SALA_EXAME.ID_EXAME ");
                query.Append(" WHERE SALA_EXAME.ID_SALA = :VALUE1 ");

                NpgsqlCommand commandPcmso = new NpgsqlCommand(query.ToString(), dbConnection);

                commandPcmso.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                commandPcmso.Parameters[0].Value = sala.Id;

                NpgsqlDataReader dr = commandPcmso.ExecuteReader();

                while (dr.Read())
                {
                    salaExame = new SalaExame(dr.GetInt64(0), new Exame(dr.GetInt64(1), dr.GetString(7), dr.GetString(8), dr.GetBoolean(9), dr.GetDecimal(10), dr.GetInt32(11), dr.GetDecimal(12), dr.GetBoolean(13), dr.GetBoolean(14), dr.IsDBNull(15) ? string.Empty : dr.GetString(15), dr.GetBoolean(16), dr.IsDBNull(17) ? (int?)null : dr.GetInt32(17), dr.GetBoolean(18)), sala, dr.GetInt32(3), dr.IsDBNull(4) ? (int?)null : dr.GetInt32(4) , dr.GetString(5));
                }

                return salaExame;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - buscaSalaExamesBySala", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean verificaSalaAtendeExame(Sala sala, Exame exame, NpgsqlConnection dbConnection)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaSalaAtendeExame");

            Boolean retorno = Convert.ToBoolean(false);

            try
            {
                NpgsqlCommand commandPcmso = new NpgsqlCommand("SELECT CAST(COUNT(*) AS INTEGER) FROM SEG_SALA_EXAME WHERE ID_SALA = :VALUE1 AND ID_EXAME = :VALUE2 AND SITUACAO = 'A'", dbConnection);

                commandPcmso.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                commandPcmso.Parameters[0].Value = sala.Id;
                
                commandPcmso.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                commandPcmso.Parameters[1].Value = exame.Id;

                NpgsqlDataReader drPcmso = commandPcmso.ExecuteReader();

                while (drPcmso.Read())
                {
                    if (drPcmso.GetInt32(0) > 0)
                    {
                        retorno = Convert.ToBoolean(true);
                    }
                }

                return retorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaSalaAtendeExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public SalaExame findById(Int64 id, NpgsqlConnection dbConnection)
        {
            
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            SalaExame salaExame = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT SALA_EXAME.ID_SALA_EXAME, SALA_EXAME.ID_EXAME, SALA_EXAME.ID_SALA, SALA_EXAME.TEMPO_MEDIO_ATENDIMENTO, SALA_EXAME.TEMPO_MEDIO_ATENDIMENTO_CALCULADO, ");
                query.Append(" SALA_EXAME.SITUACAO, SALA_EXAME.LIBERADA, ");
                query.Append(" EXAME.ID_EXAME, EXAME.DESCRICAO, EXAME.SITUACAO, EXAME.LABORATORIO, EXAME.PRECO, EXAME.PRIORIDADE, EXAME.CUSTO, EXAME.EXTERNO, ");
                query.Append(" SALA.ID_SALA, SALA.DESCRICAO, SALA.SITUACAO, SALA.ANDAR, SALA.VIP, EXAME.LIBERA_DOCUMENTO, EXAME.CODIGO_TUSS, EXAME.EXAME_COMPLEMENTAR, EXAME.PERIODO_VENCIMENTO, SALA.ID_EMPRESA, SALA.SLUG_SALA, EXAME.PADRAO_CONTRATO, SALA.QTDE_CHAMADA ");
                query.Append(" FROM SEG_SALA_EXAME SALA_EXAME ");
                query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = SALA_EXAME.ID_EXAME ");
                query.Append(" JOIN SEG_SALA SALA ON SALA.ID_SALA = SALA_EXAME.ID_SALA ");
                query.Append(" WHERE SALA_EXAME.ID_SALA_EXAME = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    salaExame = new SalaExame( dr.GetInt64(0), dr.IsDBNull(1) ? null : new Exame(dr.GetInt64(7), dr.GetString(8), dr.GetString(9), dr.GetBoolean(10), dr.GetDecimal(11), dr.GetInt32(12), dr.GetDecimal(13), dr.GetBoolean(14), dr.GetBoolean(20), dr.IsDBNull(21) ? string.Empty : dr.GetString(21), dr.GetBoolean(22), dr.IsDBNull(23) ? (int?)null : dr.GetInt32(23), dr.GetBoolean(26)), dr.IsDBNull(3) ? null : new Sala(dr.GetInt64(15), dr.GetString(16), dr.GetString(17), dr.GetInt32(18), dr.GetBoolean(19), new Empresa(dr.GetInt64(24)), dr.GetString(25), dr.GetInt32(27)), dr.IsDBNull(3) ? null : (Int32?)dr.GetInt32(3), dr.IsDBNull(4) ? null : (Int32?)dr.GetInt32(4), dr.GetString(5));
                }

                return salaExame;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<SalaExame> findAtivosBySala(Sala sala, NpgsqlConnection dbConnection)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findBySala");
            List<SalaExame> exames = new List<SalaExame>();
            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT SALA_EXAME.ID_SALA_EXAME, SALA_EXAME.ID_EXAME, SALA_EXAME.ID_SALA, SALA_EXAME.TEMPO_MEDIO_ATENDIMENTO, SALA_EXAME.TEMPO_MEDIO_ATENDIMENTO_CALCULADO, ");
                query.Append(" SALA_EXAME.SITUACAO, SALA_EXAME.LIBERADA, ");
                query.Append(" EXAME.ID_EXAME, EXAME.DESCRICAO, EXAME.SITUACAO, EXAME.LABORATORIO, EXAME.PRECO, EXAME.PRIORIDADE, EXAME.CUSTO, EXAME.EXTERNO, ");
                query.Append(" SALA.ID_SALA, SALA.DESCRICAO, SALA.SITUACAO, SALA.ANDAR, SALA.VIP, EXAME.LIBERA_DOCUMENTO, EXAME.CODIGO_TUSS, EXAME.EXAME_COMPLEMENTAR, EXAME.PERIODO_VENCIMENTO, SALA.ID_EMPRESA, SALA.SLUG_SALA, EXAME.PADRAO_CONTRATO, SALA.QTDE_CHAMADA ");
                query.Append(" FROM SEG_SALA_EXAME SALA_EXAME ");
                query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = SALA_EXAME.ID_EXAME ");
                query.Append(" JOIN SEG_SALA SALA ON SALA.ID_SALA = SALA_EXAME.ID_SALA ");
                query.Append(" WHERE SALA_EXAME.ID_SALA = :VALUE1 AND SALA_EXAME.SITUACAO = 'A' ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = sala.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    exames.Add(new SalaExame(dr.GetInt64(0), dr.IsDBNull(1) ? null : new Exame(dr.GetInt64(7), dr.GetString(8), dr.GetString(9), dr.GetBoolean(10), dr.GetDecimal(11), dr.GetInt32(12), dr.GetDecimal(13), dr.GetBoolean(14), dr.GetBoolean(20), dr.IsDBNull(21) ? string.Empty : dr.GetString(21), dr.GetBoolean(22), dr.IsDBNull(23) ? (int?)null : dr.GetInt32(23), dr.GetBoolean(26)), dr.IsDBNull(3) ? null : new Sala(dr.GetInt64(15), dr.GetString(16), dr.GetString(17), dr.GetInt32(18), dr.GetBoolean(19), new Empresa(dr.GetInt64(24)), dr.GetString(25), dr.GetInt32(27)), dr.IsDBNull(3) ? null : (Int32?)dr.GetInt32(3), dr.IsDBNull(4) ? null : (Int32?)dr.GetInt32(4), dr.GetString(5)));
                }

                return exames;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<Exame> listAllNotInSala(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método listAllNotInSala");

            List<Exame> exames = new List<Exame>();

            StringBuilder query = new StringBuilder();
            
            try
            {
                query.Append(" SELECT ID_EXAME, DESCRICAO AS EXAME, SITUACAO, LABORATORIO, PRECO, PRIORIDADE, CUSTO, EXTERNO, LIBERA_DOCUMENTO, CODIGO_TUSS, EXAME_COMPLEMENTAR, PERIODO_VENCIMENTO, PADRAO_CONTRATO ");
                query.Append(" FROM SEG_EXAME EXAME ");
                query.Append(" WHERE EXAME.ID_EXAME NOT IN (SELECT ID_EXAME FROM SEG_SALA_EXAME) AND EXAME.SITUACAO = 'A' ");
                query.Append(" ORDER BY EXAME ASC ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    exames.Add(new Exame(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetBoolean(3), dr.GetDecimal(4), dr.GetInt32(5), dr.GetDecimal(6), dr.GetBoolean(7), dr.GetBoolean(8), dr.IsDBNull(9) ? string.Empty : dr.GetString(9), dr.GetBoolean(10), dr.IsDBNull(11) ? (int?)null : dr.GetInt32(11), dr.GetBoolean(12)));
                }

                return exames;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - listAllNotInSala", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<SalaExame> findAllSalaExameAtivoByExame(Exame exame, Empresa empresa, NpgsqlConnection dbConnection)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllSalaExameAtivoByExame");
            List<SalaExame> exames = new List<SalaExame>();
            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT SALA_EXAME.ID_SALA_EXAME, SALA_EXAME.ID_EXAME, SALA_EXAME.ID_SALA, SALA_EXAME.TEMPO_MEDIO_ATENDIMENTO, SALA_EXAME.TEMPO_MEDIO_ATENDIMENTO_CALCULADO, ");
                query.Append(" SALA_EXAME.SITUACAO, SALA_EXAME.LIBERADA, ");
                query.Append(" SALA.ID_SALA, SALA.DESCRICAO, SALA.SITUACAO, SALA.ANDAR, SALA.VIP, SALA.ID_EMPRESA, SALA.SLUG_SALA, SALA.QTDE_CHAMADA ");
                query.Append(" FROM SEG_SALA_EXAME SALA_EXAME ");
                query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = SALA_EXAME.ID_EXAME ");
                query.Append(" JOIN SEG_SALA SALA ON SALA.ID_SALA = SALA_EXAME.ID_SALA ");
                query.Append(" WHERE SALA_EXAME.ID_EXAME = :VALUE1 AND SALA_EXAME.SITUACAO = 'A' AND SALA.ID_EMPRESA = :VALUE2");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = exame.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = empresa.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    exames.Add(new SalaExame(dr.GetInt64(0), exame, dr.IsDBNull(2) ? null : new Sala(dr.GetInt64(7), dr.GetString(8), dr.GetString(9), dr.GetInt32(10), dr.GetBoolean(11), new Empresa(dr.GetInt64(12)), dr.GetString(13), dr.GetInt32(14)), dr.IsDBNull(3) ? null : (Int32?)dr.GetInt32(3), dr.IsDBNull(4) ? null : (Int32?)dr.GetInt32(4), dr.GetString(5)));
                }

                return exames;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllSalaExameAtivoByExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}