﻿using SWS.Entidade;
using Npgsql;
using SWS.Excecao;
using System;
using NpgsqlTypes;
using System.Data;
using System.Text;
using SWS.IDao;
using SWS.ViewHelper;
using SWS.View.Resources;
using System.Collections.Generic;
using SWS.Helper;

namespace SWS.Dao
{
    class FonteDao : IFonteDao
    {

        public Fonte insert(Fonte fonte, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirFonte");

            StringBuilder query = new StringBuilder();

            try
            {
                fonte.Id = recuperaProximoId(dbConnection);

                query.Append(" INSERT INTO SEG_FONTE (ID_FONTE, DESCRICAO, SITUACAO, PRIVADO) VALUES (:VALUE1, :VALUE2, 'A', :VALUE3)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = fonte.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = fonte.Descricao;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Boolean));
                command.Parameters[2].Value = fonte.Privado;

                command.ExecuteNonQuery();

                return fonte;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirFonte", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Fonte update(Fonte fonte, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateFonte");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" UPDATE SEG_FONTE SET DESCRICAO = :VALUE2, SITUACAO = :VALUE3 WHERE ID_FONTE = :VALUE1", dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = fonte.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = fonte.Descricao;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = fonte.Situacao;

                command.ExecuteNonQuery();

                return fonte;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateAtividade", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" SELECT NEXTVAL(('SEG_FONTE_ID_FONTE_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Fonte findByDescricao(String descricao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFonteByDescricao");

            Fonte fonteRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_FONTE, DESCRICAO, SITUACAO, PRIVADO FROM SEG_FONTE WHERE DESCRICAO = :VALUE1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = descricao;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    fonteRetorno = new Fonte(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetBoolean(3));
                }

                return fonteRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFonteByDescricao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findByFilter(Fonte fonte, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFonteByFilter");

            try
            {
                StringBuilder query = new StringBuilder();

                if (fonte.isNullForFilter())
                    query.Append(" SELECT ID_FONTE, DESCRICAO, SITUACAO, PRIVADO FROM SEG_FONTE ");
                else
                {
                    query.Append(" SELECT ID_FONTE,DESCRICAO,SITUACAO, PRIVADO FROM SEG_FONTE WHERE TRUE");

                    if (!string.IsNullOrWhiteSpace(fonte.Descricao))
                        query.Append(" AND DESCRICAO LIKE :VALUE1 ");

                    if (!String.IsNullOrWhiteSpace(fonte.Situacao))
                        query.Append(" AND SITUACAO = :VALUE2 ");
                }

                query.Append(" order by descricao asc");
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[0].Value = fonte.Descricao + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[1].Value = fonte.Situacao;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Fontes");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFonteByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAllAtivaByGhe(Fonte fonte, Ghe ghe, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllFonteAtivaByGhe");

            try
            {
                StringBuilder query = new StringBuilder();

                if (String.IsNullOrWhiteSpace(fonte.Descricao))
                {
                    query.Append(" SELECT ID_FONTE, DESCRICAO, SITUACAO, FALSE, FALSE FROM SEG_FONTE WHERE SITUACAO = 'A' ");
                    query.Append(" AND ID_FONTE NOT IN ( SELECT ID_FONTE FROM SEG_GHE_FONTE WHERE ID_GHE = :VALUE2) ");
                }
                else
                {
                    query.Append(" SELECT ID_FONTE, DESCRICAO, SITUACAO, FALSE, FALSE FROM SEG_FONTE WHERE TRUE");
                    query.Append(" AND SITUACAO = 'A' AND DESCRICAO LIKE :VALUE2");
                    query.Append(" AND ID_FONTE NOT IN (SELECT ID_FONTE FROM SEG_GHE_FONTE WHERE ID_GHE = :VALUE2 )");
                    query.Append(" ORDER BY DESCRICAO ASC");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = fonte.Descricao + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[1].Value = ghe.Id;
                
                DataSet ds = new DataSet();

                adapter.Fill(ds, "Fontes");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllFonteAtivaByGhe", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }


        public Fonte findById(Int64 idFonte, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFonteById");

            Fonte fonteRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_FONTE, DESCRICAO, SITUACAO, PRIVADO FROM SEG_FONTE WHERE ID_FONTE = :VALUE1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = idFonte;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    fonteRetorno = new Fonte(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetBoolean(3));

                }
                return fonteRetorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFonteById", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

    }
}
