﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using System.Data;
using SWS.Entidade;
using Npgsql;
using SWS.Helper;
using NpgsqlTypes;
using SWS.Excecao;
using SWS.View.Resources;

namespace SWS.Dao
{
    class EmpresaDao : IEmpresaDao
    {
        
        public DataSet findByFilter(Empresa empresa, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findEmpresaByFilter");

            try
            {
                StringBuilder query = new StringBuilder();

                if (empresa.isNullForFilter())
                {
                    query.Append(" SELECT EMPRESA.ID_EMPRESA, EMPRESA.RAZAO_SOCIAL, EMPRESA.FANTASIA, FORMATA_CNPJ(EMPRESA.CNPJ) AS CNPJ, EMPRESA.INSCRICAO, EMPRESA.ENDERECO, ");
                    query.Append(" EMPRESA.NUMERO, EMPRESA.COMPLEMENTO, EMPRESA.BAIRRO, CIDADE.NOME, FORMATA_CEP(EMPRESA.CEP) AS CEP, EMPRESA.UF, ");
                    query.Append(" EMPRESA.TELEFONE1, EMPRESA.TELEFONE2, EMPRESA.EMAIL, EMPRESA.SITE, EMPRESA.DATA_CADASTRO, EMPRESA.SITUACAO, EMPRESA.MIMETYPE, EMPRESA.LOGO, EMPRESA.SIMPLES_NACIONAL ");
                    query.Append(" FROM SEG_EMPRESA EMPRESA ");
                    query.Append(" JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = EMPRESA.ID_CIDADE_IBGE ");
                    
                }
                else
                {
                    query.Append(" SELECT EMPRESA.ID_EMPRESA, EMPRESA.RAZAO_SOCIAL, EMPRESA.FANTASIA, FORMATA_CNPJ(EMPRESA.CNPJ) AS CNPJ, EMPRESA.INSCRICAO, EMPRESA.ENDERECO, ");
                    query.Append(" EMPRESA.NUMERO, EMPRESA.COMPLEMENTO, EMPRESA.BAIRRO, CIDADE.NOME, FORMATA_CEP(EMPRESA.CEP) AS CEP, EMPRESA.UF, ");
                    query.Append(" EMPRESA.TELEFONE1, EMPRESA.TELEFONE2, EMPRESA.EMAIL, EMPRESA.SITE, EMPRESA.DATA_CADASTRO, EMPRESA.SITUACAO, EMPRESA.MIMETYPE, EMPRESA.LOGO, EMPRESA.SIMPLES_NACIONAL ");
                    query.Append(" FROM SEG_EMPRESA EMPRESA ");
                    query.Append(" JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = EMPRESA.ID_CIDADE_IBGE ");
                    query.Append(" WHERE TRUE ");

                    if (!String.IsNullOrWhiteSpace(empresa.RazaoSocial))
                        query.Append(" AND EMPRESA.RAZAO_SOCIAL LIKE :VALUE1 ");

                    if (!String.IsNullOrWhiteSpace(empresa.Cnpj))
                        query.Append(" AND EMPRESA.CNPJ = :VALUE2 ");

                    if (!String.IsNullOrEmpty(empresa.Situacao))
                        query.Append(" AND EMPRESA.SITUACAO = :VALUE3 ");
                }

                query.Append(" ORDER BY EMPRESA.RAZAO_SOCIAL ASC");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = String.IsNullOrEmpty(empresa.RazaoSocial) ? String.Empty : empresa.RazaoSocial + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = String.IsNullOrEmpty(empresa.Cnpj) ? String.Empty : empresa.Cnpj;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[2].Value = String.IsNullOrEmpty(empresa.Situacao) ? String.Empty : empresa.Situacao;
                
                
                DataSet ds = new DataSet();
                adapter.Fill(ds, "Empresas");
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findEmpresaByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Empresa findByCnpj(String cnpj, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findEmpresaByCnpj");

            Empresa empresaRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT EMPRESA.ID_EMPRESA, EMPRESA.RAZAO_SOCIAL, EMPRESA.FANTASIA, EMPRESA.CNPJ, EMPRESA.INSCRICAO, EMPRESA.ENDERECO, ");
                query.Append(" EMPRESA.NUMERO, EMPRESA.COMPLEMENTO, EMPRESA.BAIRRO, EMPRESA.CEP, EMPRESA.UF, EMPRESA.TELEFONE1, EMPRESA.TELEFONE2, ");
                query.Append(" EMPRESA.EMAIL, EMPRESA.SITE, EMPRESA.DATA_CADASTRO, EMPRESA.SITUACAO, EMPRESA.MIMETYPE, EMPRESA.LOGO, ");
                query.Append(" EMPRESA.ID_CIDADE_IBGE, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, EMPRESA.SIMPLES_NACIONAL, EMPRESA.ID_EMPRESA_MATRIZ, EMPRESA.CODIGO_CNES, ");
                query.Append(" EMPRESA.VALOR_MINIMO_IMPOSTO_FEDERAL, EMPRESA.VALOR_MINIMO_IR, EMPRESA.ALIQUOTA_PIS, EMPRESA.ALIQUOTA_COFINS, EMPRESA.ALIQUOTA_IR, EMPRESA.ALIQUOTA_CSLL, EMPRESA.ALIQUOTA_ISS ");
                query.Append(" FROM SEG_EMPRESA EMPRESA");
                query.Append(" JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = EMPRESA.ID_CIDADE_IBGE ");
                query.Append(" WHERE EMPRESA.CNPJ = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = cnpj;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    empresaRetorno = new Empresa(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.GetString(4), dr.GetString(5), dr.GetString(6), dr.GetString(7), dr.GetString(8), dr.GetString(9), dr.GetString(10), dr.GetString(11), dr.GetString(12), dr.GetString(13), dr.GetString(14), dr.GetDateTime(15), dr.GetString(16), dr.IsDBNull(17) ? null : dr.GetString(17), dr.IsDBNull(18) ? null : (byte[])dr.GetValue(18), new CidadeIbge(dr.GetInt64(19), dr.GetString(20), dr.GetString(21), dr.GetString(22)), dr.GetBoolean(23), dr.IsDBNull(24) ? null : findById((Int64)dr.GetInt64(24), dbConnection), dr.IsDBNull(25) ? string.Empty : dr.GetString(25), dr.GetDecimal(26), dr.GetDecimal(27), dr.GetDecimal(28),dr.GetDecimal(29), dr.GetDecimal(30), dr.GetDecimal(31), dr.GetDecimal(32));
                }

                return empresaRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findEmpresaByCnpj", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Empresa insert(Empresa empresa, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertEmpresa");

            try
            {
                empresa.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_EMPRESA (ID_EMPRESA, RAZAO_SOCIAL, FANTASIA, CNPJ, INSCRICAO, ENDERECO, NUMERO, COMPLEMENTO, BAIRRO, CEP, UF, TELEFONE1, TELEFONE2, EMAIL, SITE, DATA_CADASTRO, SITUACAO, LOGO, MIMETYPE, ID_CIDADE_IBGE, SIMPLES_NACIONAL, CODIGO_CNES, ID_EMPRESA_MATRIZ, VALOR_MINIMO_IMPOSTO_FEDERAL, VALOR_MINIMO_IR, ALIQUOTA_PIS, ALIQUOTA_COFINS, ALIQUOTA_IR, ALIQUOTA_CSLL, ALIQUOTA_ISS) VALUES ");
                query.Append(" (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6, :VALUE7 , :VALUE8, :VALUE9, :VALUE10, :VALUE11, :VALUE12, :VALUE13, :VALUE14, :VALUE15, :VALUE16, :VALUE17, :VALUE18, :VALUE19, :VALUE20, :VALUE21, :VALUE22, :VALUE23, :VALUE24, :VALUE25, :VALUE26, :VALUE27, :VALUE28, :VALUE29, :VALUE30 ) ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = empresa.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = empresa.RazaoSocial;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = empresa.Fantasia;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = empresa.Cnpj;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = empresa.Inscricao;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = empresa.Endereco;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = empresa.Numero;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Varchar));
                command.Parameters[7].Value = empresa.Complemento;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                command.Parameters[8].Value = empresa.Bairro;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                command.Parameters[9].Value = empresa.Cep;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Varchar));
                command.Parameters[10].Value = empresa.Uf;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Varchar));
                command.Parameters[11].Value = empresa.Telefone1;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Varchar));
                command.Parameters[12].Value = empresa.Telefone2;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Varchar));
                command.Parameters[13].Value = empresa.Email;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Varchar));
                command.Parameters[14].Value = empresa.Site;

                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Date));
                command.Parameters[15].Value = DateTime.Now;

                command.Parameters.Add(new NpgsqlParameter("VALUE17", NpgsqlDbType.Varchar));
                command.Parameters[16].Value = ApplicationConstants.ATIVO;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE18", NpgsqlDbType.Bytea));
                command.Parameters[17].Value = empresa.Logo;

                command.Parameters.Add(new NpgsqlParameter("VALUE19", NpgsqlDbType.Varchar));
                command.Parameters[18].Value = empresa.Mimetype;

                command.Parameters.Add(new NpgsqlParameter("VALUE20", NpgsqlDbType.Integer));
                command.Parameters[19].Value = empresa.Cidade != null ? empresa.Cidade.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE21", NpgsqlDbType.Boolean));
                command.Parameters[20].Value = empresa.SimplesNacional;

                command.Parameters.Add(new NpgsqlParameter("VALUE22", NpgsqlDbType.Varchar));
                command.Parameters[21].Value = empresa.CodigoCnes;

                command.Parameters.Add(new NpgsqlParameter("VALUE23", NpgsqlDbType.Integer));
                command.Parameters[22].Value = empresa.Matriz == null ? null : empresa.Matriz.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE24", NpgsqlDbType.Numeric));
                command.Parameters[23].Value = empresa.ValorMinimoImpostoFederal;

                command.Parameters.Add(new NpgsqlParameter("VALUE25", NpgsqlDbType.Numeric));
                command.Parameters[24].Value = empresa.ValorMinimoIR;

                command.Parameters.Add(new NpgsqlParameter("VALUE26", NpgsqlDbType.Numeric));
                command.Parameters[25].Value = empresa.AliquotaPIS;

                command.Parameters.Add(new NpgsqlParameter("VALUE27", NpgsqlDbType.Numeric));
                command.Parameters[26].Value = empresa.AliquotaCOFINS;

                command.Parameters.Add(new NpgsqlParameter("VALUE28", NpgsqlDbType.Numeric));
                command.Parameters[27].Value = empresa.AliquotaIR;

                command.Parameters.Add(new NpgsqlParameter("VALUE29", NpgsqlDbType.Numeric));
                command.Parameters[28].Value = empresa.AliquotaCSLL;

                command.Parameters.Add(new NpgsqlParameter("VALUE30", NpgsqlDbType.Numeric));
                command.Parameters[29].Value = empresa.AliquotaISS;
                
                command.ExecuteNonQuery();

                return empresa;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertEmpresa", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_EMPRESA_ID_EMPRESA_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Empresa findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findEmpresaById");

            Empresa empresaRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {

                query.Append(" SELECT EMPRESA.ID_EMPRESA, EMPRESA.RAZAO_SOCIAL, EMPRESA.FANTASIA, EMPRESA.CNPJ, EMPRESA.INSCRICAO, EMPRESA.ENDERECO, ");
                query.Append(" EMPRESA.NUMERO, EMPRESA.COMPLEMENTO, EMPRESA.BAIRRO, EMPRESA.CEP, EMPRESA.UF, EMPRESA.TELEFONE1, EMPRESA.TELEFONE2, ");
                query.Append(" EMPRESA.EMAIL, EMPRESA.SITE, EMPRESA.DATA_CADASTRO, EMPRESA.SITUACAO, EMPRESA.MIMETYPE, EMPRESA.LOGO, ");
                query.Append(" EMPRESA.ID_CIDADE_IBGE, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, EMPRESA.SIMPLES_NACIONAL, EMPRESA.ID_EMPRESA_MATRIZ, EMPRESA.CODIGO_CNES, ");
                query.Append(" EMPRESA.VALOR_MINIMO_IMPOSTO_FEDERAL, EMPRESA.VALOR_MINIMO_IR, EMPRESA.ALIQUOTA_PIS, EMPRESA.ALIQUOTA_COFINS, EMPRESA.ALIQUOTA_IR, EMPRESA.ALIQUOTA_CSLL, EMPRESA.ALIQUOTA_ISS ");
                query.Append(" FROM SEG_EMPRESA EMPRESA ");
                query.Append(" JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = EMPRESA.ID_CIDADE_IBGE ");
                query.Append(" WHERE EMPRESA.ID_EMPRESA = :VALUE1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                String mimetype = String.Empty;

                while (dr.Read())
                {
                    empresaRetorno = new Empresa(dr.GetInt64(0),dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.GetString(4),dr.GetString(5),dr.GetString(6),dr.GetString(7),dr.GetString(8), dr.GetString(9),dr.GetString(10),dr.GetString(11),dr.GetString(12),dr.GetString(13), dr.GetString(14), dr.GetDateTime(15), dr.GetString(16), dr.IsDBNull(17) ? String.Empty : dr.GetString(17),dr.IsDBNull(18) ? null : (byte[])dr.GetValue(18),new CidadeIbge(dr.GetInt64(19), dr.GetString(20), dr.GetString(21), dr.GetString(22)),dr.GetBoolean(23), dr.IsDBNull(24) ? null : findById(dr.GetInt64(24), dbConnection), dr.IsDBNull(25) ? string.Empty : dr.GetString(25), dr.GetDecimal(26), dr.GetDecimal(27), dr.GetDecimal(28), dr.GetDecimal(29), dr.GetDecimal(30), dr.GetDecimal(31), dr.GetDecimal(32));
                }

                return empresaRetorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findEmpresaById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Empresa update(Empresa empresa, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateEmpresa");

            try
            {

                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_EMPRESA SET ");
                query.Append(" RAZAO_SOCIAL = :VALUE2, ");
                query.Append(" FANTASIA = :VALUE3, ");
                query.Append(" CNPJ = :VALUE4, ");
                query.Append(" INSCRICAO = :VALUE5,");
                query.Append(" ENDERECO = :VALUE6, ");
                query.Append(" NUMERO = :VALUE7, ");
                query.Append(" COMPLEMENTO = :VALUE8, ");
                query.Append(" BAIRRO = :VALUE9, ");
                query.Append(" CEP = :VALUE10,");
                query.Append(" UF = :VALUE11, "); 
                query.Append(" TELEFONE1 = :VALUE12, ");
                query.Append(" TELEFONE2 = :VALUE13, ");
                query.Append(" EMAIL = :VALUE14, ");
                query.Append(" SITE = :VALUE15, ");
                query.Append(" DATA_CADASTRO = :VALUE16,");
                query.Append(" SITUACAO = :VALUE17, ");
                query.Append(" LOGO = :VALUE18, ");
                query.Append(" MIMETYPE = :VALUE19, ");
                query.Append(" ID_CIDADE_IBGE = :VALUE20, ");
                query.Append(" SIMPLES_NACIONAL = :VALUE21, ");
                query.Append(" CODIGO_CNES = :VALUE22, ");
                query.Append(" ID_EMPRESA_MATRIZ = :VALUE23,");
                query.Append(" VALOR_MINIMO_IMPOSTO_FEDERAL = :VALUE24, ");
                query.Append(" VALOR_MINIMO_IR = :VALUE25, ");
                query.Append(" ALIQUOTA_PIS = :VALUE26, ");
                query.Append(" ALIQUOTA_COFINS = :VALUE27, ");
                query.Append(" ALIQUOTA_IR = :VALUE28, ");
                query.Append(" ALIQUOTA_CSLL = :VALUE29, ");
                query.Append(" ALIQUOTA_ISS = :VALUE30 ");
                query.Append(" WHERE ID_EMPRESA = :VALUE1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = empresa.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = empresa.RazaoSocial;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = empresa.Fantasia;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = empresa.Cnpj;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = empresa.Inscricao;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = empresa.Endereco;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = empresa.Numero;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Varchar));
                command.Parameters[7].Value = empresa.Complemento;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                command.Parameters[8].Value = empresa.Bairro;

                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                command.Parameters[9].Value = empresa.Cep;

                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Varchar));
                command.Parameters[10].Value = empresa.Uf;

                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Varchar));
                command.Parameters[11].Value = empresa.Telefone1;

                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Varchar));
                command.Parameters[12].Value = empresa.Telefone2;

                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Varchar));
                command.Parameters[13].Value = empresa.Email;

                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Varchar));
                command.Parameters[14].Value = empresa.Site;

                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Date));
                command.Parameters[15].Value = empresa.DataCadastro;

                command.Parameters.Add(new NpgsqlParameter("VALUE17", NpgsqlDbType.Varchar));
                command.Parameters[16].Value = ApplicationConstants.ATIVO;

                command.Parameters.Add(new NpgsqlParameter("VALUE18", NpgsqlDbType.Bytea));
                command.Parameters[17].Value = empresa.Logo;

                command.Parameters.Add(new NpgsqlParameter("VALUE19", NpgsqlDbType.Varchar));
                command.Parameters[18].Value = empresa.Mimetype;

                command.Parameters.Add(new NpgsqlParameter("VALUE20", NpgsqlDbType.Integer));
                command.Parameters[19].Value = empresa.Cidade != null ? empresa.Cidade.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE21", NpgsqlDbType.Boolean));
                command.Parameters[20].Value = empresa.SimplesNacional;

                command.Parameters.Add(new NpgsqlParameter("VALUE22", NpgsqlDbType.Varchar));
                command.Parameters[21].Value = empresa.CodigoCnes;

                command.Parameters.Add(new NpgsqlParameter("VALUE23", NpgsqlDbType.Integer));
                command.Parameters[22].Value = empresa.Matriz == null ? null : empresa.Matriz.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE24", NpgsqlDbType.Numeric));
                command.Parameters[23].Value = empresa.ValorMinimoImpostoFederal;

                command.Parameters.Add(new NpgsqlParameter("VALUE25", NpgsqlDbType.Numeric));
                command.Parameters[24].Value = empresa.ValorMinimoIR;

                command.Parameters.Add(new NpgsqlParameter("VALUE26", NpgsqlDbType.Numeric));
                command.Parameters[25].Value = empresa.AliquotaPIS;

                command.Parameters.Add(new NpgsqlParameter("VALUE27", NpgsqlDbType.Numeric));
                command.Parameters[26].Value = empresa.AliquotaCOFINS;

                command.Parameters.Add(new NpgsqlParameter("VALUE28", NpgsqlDbType.Numeric));
                command.Parameters[27].Value = empresa.AliquotaIR;

                command.Parameters.Add(new NpgsqlParameter("VALUE29", NpgsqlDbType.Numeric));
                command.Parameters[28].Value = empresa.AliquotaCSLL;

                command.Parameters.Add(new NpgsqlParameter("VALUE30", NpgsqlDbType.Numeric));
                command.Parameters[29].Value = empresa.AliquotaISS;
                
                command.ExecuteNonQuery();
                
                return empresa;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateEmpresa", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<Empresa> findAll(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAll");

            List<Empresa> empresas = new List<Empresa>();

            StringBuilder query = new StringBuilder();

            try
            {

                query.Append(" SELECT EMPRESA.ID_EMPRESA, EMPRESA.RAZAO_SOCIAL, EMPRESA.FANTASIA, EMPRESA.CNPJ, EMPRESA.INSCRICAO, EMPRESA.ENDERECO, ");
                query.Append(" EMPRESA.NUMERO, EMPRESA.COMPLEMENTO, EMPRESA.BAIRRO, EMPRESA.CEP, EMPRESA.UF, EMPRESA.TELEFONE1, EMPRESA.TELEFONE2, ");
                query.Append(" EMPRESA.EMAIL, EMPRESA.SITE, EMPRESA.DATA_CADASTRO, EMPRESA.SITUACAO, EMPRESA.MIMETYPE, EMPRESA.LOGO, ");
                query.Append(" EMPRESA.ID_CIDADE_IBGE, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, EMPRESA.SIMPLES_NACIONAL, EMPRESA.ID_EMPRESA_MATRIZ, EMPRESA.CODIGO_CNES, ");
                query.Append(" EMPRESA.VALOR_MINIMO_IMPOSTO_FEDERAL, EMPRESA.VALOR_MINIMO_IR, EMPRESA.ALIQUOTA_PIS, EMPRESA.ALIQUOTA_COFINS, EMPRESA.ALIQUOTA_IR, EMPRESA.ALIQUOTA_CSLL, EMPRESA.ALIQUOTA_ISS ");
                query.Append(" FROM SEG_EMPRESA EMPRESA ");
                query.Append(" JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = EMPRESA.ID_CIDADE_IBGE ");
                query.Append(" WHERE EMPRESA.SITUACAO = 'A' ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                String mimetype = String.Empty;

                while (dr.Read())
                {
                    empresas.Add(new Empresa(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.GetString(4), dr.GetString(5), dr.GetString(6), dr.GetString(7), dr.GetString(8), dr.GetString(9), dr.GetString(10), dr.GetString(11), dr.GetString(12), dr.GetString(13), dr.GetString(14), dr.GetDateTime(15), dr.GetString(16), dr.IsDBNull(17) ? String.Empty : dr.GetString(17), dr.IsDBNull(18) ? null : (byte[])dr.GetValue(18), new CidadeIbge(dr.GetInt64(19), dr.GetString(20), dr.GetString(21), dr.GetString(22)), dr.GetBoolean(23), dr.IsDBNull(24) ? null : findById(dr.GetInt64(24), dbConnection), dr.IsDBNull(25) ? string.Empty : dr.GetString(25), dr.GetDecimal(26), dr.GetDecimal(27), dr.GetDecimal(28), dr.GetDecimal(29), dr.GetDecimal(30), dr.GetDecimal(31), dr.GetDecimal(32)));
                }

                return empresas;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAll", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
    }
}
