﻿using Npgsql;
using NpgsqlTypes;
using SWS.Entidade;
using SWS.Excecao;
using SWS.Helper;
using SWS.IDao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Dao
{
    class UsuarioClienteDao : IUsuarioClienteDao
    {
        public long recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " recuperaProximoId");

            long id = 0;

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_USUARIO_CLIENTE_ID_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    id = dr.GetInt64(0);
                }

                return id;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public UsuarioCliente insert(UsuarioCliente usuarioCliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            StringBuilder query = new StringBuilder();

            try
            {
                usuarioCliente.Id = recuperaProximoId(dbConnection);

                query.Append(" INSERT INTO SEG_USUARIO_CLIENTE (ID, ID_CLIENTE, ID_USUARIO, DATA_INCLUSAO ) VALUES ");
                query.Append(" (:VALUE1, :VALUE2, :VALUE3, NOW() )");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = usuarioCliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = usuarioCliente.Cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = usuarioCliente.Usuario.Id;

                command.ExecuteNonQuery();

                return usuarioCliente;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public UsuarioCliente update(UsuarioCliente usuarioCliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_USUARIO_CLIENTE SET ID_CLIENTE = :VALUE2, ID_USUARIO = :VALUE3, DATA_INCLUSAO = :VALUE4 WHERE ID = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = usuarioCliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = usuarioCliente.Cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = usuarioCliente.Usuario.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Date));
                command.Parameters[3].Value = usuarioCliente.DataInclusao;

                command.ExecuteNonQuery();

                return usuarioCliente;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<UsuarioCliente> findAllByUsuario(Usuario usuario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByUsuario");

            List<UsuarioCliente> clientes = new List<UsuarioCliente>();

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID, ID_CLIENTE, ID_USUARIO, DATA_INCLUSAO ");
                query.Append(" FROM SEG_USUARIO_CLIENTE ");
                query.Append(" WHERE ID_USUARIO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = usuario.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    clientes.Add(new UsuarioCliente(dr.GetInt64(0), new Cliente(dr.GetInt64(1)), new Usuario(dr.GetInt64(2)), dr.GetDateTime(3)));
                }

                return clientes;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByUsuario", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void delete(UsuarioCliente usuarioCliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" DELETE FROM SEG_USUARIO_CLIENTE WHERE ID = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = usuarioCliente.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public UsuarioCliente findById(long id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            UsuarioCliente usuarioCliente = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID, ID_CLIENTE, ID_USUARIO, DATA_INCLUSAO ");
                query.Append(" FROM SEG_USUARIO_CLIENTE ");
                query.Append(" WHERE ID = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = usuarioCliente.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    usuarioCliente = new UsuarioCliente(dr.GetInt64(0), new Cliente(dr.GetInt64(1)), new Usuario(dr.GetInt64(2)), dr.GetDateTime(3));
                }

                return usuarioCliente;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByUsuario", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
