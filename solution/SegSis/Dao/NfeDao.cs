﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;
using NpgsqlTypes;
using System.Data;
using SWS.View.Resources;

namespace SWS.Dao
{
    class NfeDao : INfeDao
    {
        public Nfe insert(Nfe nfe, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            StringBuilder query = new StringBuilder();

            try
            {
                nfe.Id = recuperaProximoId(dbConnection);

                query.Append(" INSERT INTO SEG_NFE ( ID_NFE, ID_USUARIO_CRIADOR, ID_EMPRESA, NUMERO_NFE, ");
                query.Append(" VALOR_TOTAL_NF, DATA_EMISSAO, VALOR_PIS, VALOR_COFINS, BASE_CALCULO, ALIQUOTA_ISS, VALOR_IR, VALOR_ISS, ");
                query.Append(" VALOR_CSLL, RAZAO_SOCIAL, CNPJ, INSCRICAO, ENDERECO, NUMERO, COMPLEMENTO, BAIRRO, "); 
                query.Append(" CIDADE, CEP, UF, TELEFONE_1, EMAIL, DATA_GRAVACAO, SITUACAO, ID_PLANO_FORMA, ID_CLIENTE, VALOR_LIQUIDO, ID_CENTROCUSTO) VALUES ");
                query.Append(" (:VALUE1, :VALUE2, :VALUE3, NEXTVAL('SEG_NUMERONFE_SEQ'), :VALUE4, :VALUE5, :VALUE6, ");
                query.Append(" :VALUE7, :VALUE8, :VALUE9, :VALUE10, :VALUE11, :VALUE12, :VALUE13, :VALUE14, "); 
                query.Append(" :VALUE15, :VALUE16, :VALUE17, :VALUE18, :VALUE19, :VALUE20, :VALUE21, :VALUE22, :VALUE23, :VALUE24, :VALUE25, :VALUE26, :VALUE27, :VALUE28, :VALUE29, :VALUE30)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = nfe.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = nfe.UsuarioCriador.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = nfe.Empresa.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Numeric));
                command.Parameters[3].Value = nfe.ValorTotalNf;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Timestamp));
                command.Parameters[4].Value = nfe.DataEmissao;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Numeric));
                command.Parameters[5].Value = nfe.ValorPis;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Numeric));
                command.Parameters[6].Value = nfe.ValorCofins;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Numeric));
                command.Parameters[7].Value = nfe.BaseCalculo;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Numeric));
                command.Parameters[8].Value = nfe.AliquotaIss;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Numeric));
                command.Parameters[9].Value = nfe.ValorIr;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Numeric));
                command.Parameters[10].Value = nfe.ValorIss;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Numeric));
                command.Parameters[11].Value = nfe.ValorCsll;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Varchar));
                command.Parameters[12].Value = nfe.Cliente.RazaoSocial.ToUpper();
                
                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Varchar));
                command.Parameters[13].Value = nfe.Cliente.Cnpj;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Varchar));
                command.Parameters[14].Value = nfe.Cliente.Inscricao;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Varchar));
                command.Parameters[15].Value = nfe.Cliente.Endereco.ToUpper();
                
                command.Parameters.Add(new NpgsqlParameter("VALUE17", NpgsqlDbType.Varchar));
                command.Parameters[16].Value = nfe.Cliente.Numero.ToUpper();
                
                command.Parameters.Add(new NpgsqlParameter("VALUE18", NpgsqlDbType.Varchar));
                command.Parameters[17].Value = nfe.Cliente.Complemento.ToUpper();
                
                command.Parameters.Add(new NpgsqlParameter("VALUE19", NpgsqlDbType.Varchar));
                command.Parameters[18].Value = nfe.Cliente.Bairro.ToUpper();
                
                command.Parameters.Add(new NpgsqlParameter("VALUE20", NpgsqlDbType.Varchar));
                command.Parameters[19].Value = nfe.Cliente.CidadeIbge.Nome.ToUpper();
                
                command.Parameters.Add(new NpgsqlParameter("VALUE21", NpgsqlDbType.Varchar));
                command.Parameters[20].Value = nfe.Cliente.Cep;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE22", NpgsqlDbType.Varchar));
                command.Parameters[21].Value = nfe.Cliente.Uf;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE23", NpgsqlDbType.Varchar));
                command.Parameters[22].Value = nfe.Cliente.Telefone1;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE24", NpgsqlDbType.Varchar));
                command.Parameters[23].Value = nfe.Cliente.Email.ToLower();
                
                command.Parameters.Add(new NpgsqlParameter("VALUE25", NpgsqlDbType.Timestamp));
                command.Parameters[24].Value = nfe.DataGravacao;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE26", NpgsqlDbType.Varchar));
                command.Parameters[25].Value = nfe.Situacao;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE27", NpgsqlDbType.Integer));
                command.Parameters[26].Value = nfe.PlanoForma.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE28", NpgsqlDbType.Integer));
                command.Parameters[27].Value = nfe.Cliente.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE29", NpgsqlDbType.Numeric));
                command.Parameters[28].Value = nfe.ValorLiquido;

                command.Parameters.Add(new NpgsqlParameter("VALUE30", NpgsqlDbType.Integer));
                command.Parameters[29].Value = nfe.CentroCusto != null ? nfe.CentroCusto.Id : null;
                
                command.ExecuteNonQuery();

                return nfe;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_NFE_ID_NFE_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public long? findLastNumeroNf(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findLastNumeroNf");

            long? ultimaNf = 0;
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT MAX(NUMERO_NFE) FROM SEG_NFE", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    ultimaNf = dr.IsDBNull(0) ? (long?)null : dr.GetInt64(0);
                }

                return ultimaNf;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findLastNumeroNf", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findByFilter(Nfe notaFiscal, DateTime? dataEmissaoInicial, DateTime? dataEmissaoFinal, DateTime? dataCancelamentoInicial,  DateTime? dataCancelamentoFinal, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findNotaFiscalByFilter");

            try
            {
                StringBuilder query = new StringBuilder();
                
                bool isEmpresa = false;
                bool isSituacao = false;
                bool isNumeroNota = false;
                bool isDataInicialEmissao = false;
                bool isDataCancelamentoInicial = false;
                bool isCliente = false;
                bool isNullFilter = false;

                if (notaFiscal.isNullForFilter(dataEmissaoInicial, dataCancelamentoInicial))
                {
                    isNullFilter = true;
                    query.Append(" SELECT NOTA.ID_NFE, NOTA.NUMERO_NFE, NOTA.DATA_EMISSAO, NOTA.DATA_CANCELADA, NOTA.VALOR_TOTAL_NF,  ");
                    query.Append(" CLIENTE.RAZAO_SOCIAL, (CASE WHEN CLIENTE.FISICA = TRUE THEN FORMATA_CPF(CLIENTE.CNPJ) ELSE FORMATA_CNPJ(CLIENTE.CNPJ) END ) AS CNPJ, PLANO.DESCRICAO, USUARIO.LOGIN, EMPRESA.FANTASIA, NOTA.SITUACAO FROM SEG_NFE NOTA ");
                    query.Append(" JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = NOTA.ID_CLIENTE ");
                    query.Append(" JOIN SEG_PLANO_FORMA PLANO_FORMA ON PLANO_FORMA.ID_PLANO_FORMA = NOTA.ID_PLANO_FORMA  ");
                    query.Append(" JOIN SEG_PLANO PLANO ON PLANO.ID_PLANO = PLANO_FORMA.ID_PLANO ");
                    query.Append(" JOIN SEG_USUARIO USUARIO ON USUARIO.ID_USUARIO = NOTA.ID_USUARIO_CRIADOR ");
                    query.Append(" JOIN SEG_EMPRESA EMPRESA ON EMPRESA.ID_EMPRESA = NOTA.ID_EMPRESA ");
                }
                else
                {
                    query.Append(" SELECT NOTA.ID_NFE, NOTA.NUMERO_NFE, NOTA.DATA_EMISSAO, NOTA.DATA_CANCELADA, NOTA.VALOR_TOTAL_NF,  ");
                    query.Append(" CLIENTE.RAZAO_SOCIAL, (CASE WHEN CLIENTE.FISICA = TRUE THEN FORMATA_CPF(CLIENTE.CNPJ) ELSE FORMATA_CNPJ(CLIENTE.CNPJ) END ) AS CNPJ, PLANO.DESCRICAO, USUARIO.LOGIN, EMPRESA.FANTASIA, NOTA.SITUACAO FROM SEG_NFE NOTA ");
                    query.Append(" JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = NOTA.ID_CLIENTE ");
                    query.Append(" JOIN SEG_PLANO_FORMA PLANO_FORMA ON PLANO_FORMA.ID_PLANO_FORMA = NOTA.ID_PLANO_FORMA  ");
                    query.Append(" JOIN SEG_PLANO PLANO ON PLANO.ID_PLANO = PLANO_FORMA.ID_PLANO ");
                    query.Append(" JOIN SEG_USUARIO USUARIO ON USUARIO.ID_USUARIO = NOTA.ID_USUARIO_CRIADOR ");
                    query.Append(" JOIN SEG_EMPRESA EMPRESA ON EMPRESA.ID_EMPRESA = NOTA.ID_EMPRESA "); 
                    query.Append(" WHERE TRUE ");

                    if (notaFiscal.Empresa != null)
                    {
                        isEmpresa = true;
                        query.Append(" AND EMPRESA.ID_EMPRESA = :VALUE1 ");
                    }

                    if (!string.IsNullOrWhiteSpace(notaFiscal.Situacao))
                    {
                        isSituacao = true;
                        query.Append(" AND NOTA.SITUACAO = :VALUE2 ");
                    }

                    if (notaFiscal.NumeroNfe != null)
                    {
                        isNumeroNota = true;
                        query.Append(" AND NOTA.NUMERO_NFE = :VALUE3 ");
                    }

                    if (dataEmissaoInicial != null)
                    {
                        isDataInicialEmissao = true;
                        query.Append(" AND NOTA.DATA_EMISSAO BETWEEN :VALUE4 AND :VALUE5 ");
                    }

                    if (dataCancelamentoInicial != null)
                    {
                        isDataCancelamentoInicial = true;
                        query.Append(" AND NOTA.DATA_CANCELADA BETWEEN :VALUE6 AND :VALUE7 ");
                    }

                    if (notaFiscal.Cliente != null)
                    {
                        isCliente = true;
                        query.Append(" AND NOTA.ID_CLIENTE = :VALUE8 ");
                    }
                }

                query.Append(" ORDER BY NOTA.NUMERO_NFE ASC ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = notaFiscal.Empresa != null ? notaFiscal.Empresa.Id : null;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = !String.IsNullOrEmpty(notaFiscal.Situacao) ? notaFiscal.Situacao : string.Empty;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[2].Value = notaFiscal.NumeroNfe != null ? notaFiscal.NumeroNfe : (long?)null;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[3].Value = dataEmissaoInicial != null ? dataEmissaoInicial : (DateTime?)null;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[4].Value = dataEmissaoFinal != null ? dataEmissaoFinal : (DateTime?)null;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[5].Value = dataCancelamentoInicial != null ? dataCancelamentoInicial : (DateTime?)null;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[6].Value = dataCancelamentoFinal != null ? dataCancelamentoFinal : (DateTime?)null;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[7].Value = notaFiscal.Cliente != null ? notaFiscal.Cliente.Id : null;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Notas");

                /* montando totais */

                query.Clear();

                if (isNullFilter)
                {
                    query.Append("SELECT ( SELECT COUNT(*) FROM SEG_NFE WHERE ID_EMPRESA = :VALUE1) AS QUANTIDADE, (SELECT SUM(VALOR_TOTAL_NF) FROM SEG_NFE WHERE ID_EMPRESA = :VALUE1 ) AS VALOR_TOTAL, (SELECT SUM(VALOR_LIQUIDO) FROM SEG_NFE WHERE ID_EMPRESA = :VALUE1) AS VALOR_LIQUIDO ");
                }
                else
                {
                    query.Append(" SELECT ( SELECT COUNT(*) FROM SEG_NFE ");
                    query.Append(" WHERE TRUE ");

                    if (isEmpresa)
                        query.Append(" AND ID_EMPRESA = :VALUE1 ");
                    
                    if (isSituacao)
                        query.Append(" AND SITUACAO = :VALUE2 ");
                    
                    if (isNumeroNota)
                        query.Append(" AND NUMERO_NFE = :VALUE3 ");

                    if (isDataInicialEmissao)
                        query.Append(" AND DATA_EMISSAO BETWEEN :VALUE4 AND :VALUE5 ");

                    if (isDataCancelamentoInicial)
                        query.Append(" AND DATA_CANCELADA BETWEEN :VALUE6 AND :VALUE7 ");

                    if (isCliente)
                        query.Append(" AND ID_CLIENTE = :VALUE8 ");
                    
                    query.Append(" ) AS QUANTIDADE, ( SELECT SUM(VALOR_TOTAL_NF) FROM SEG_NFE ");
                    query.Append(" WHERE TRUE ");

                    if (isEmpresa)
                        query.Append(" AND ID_EMPRESA = :VALUE1 ");
                    
                    if (isSituacao)
                        query.Append(" AND SITUACAO = :VALUE2 ");
                    
                    if (isNumeroNota)
                        query.Append(" AND NUMERO_NFE = :VALUE3 ");

                    if (isDataInicialEmissao)
                        query.Append(" AND DATA_EMISSAO BETWEEN :VALUE4 AND :VALUE5 ");

                    if (isDataCancelamentoInicial)
                        query.Append(" AND DATA_CANCELADA BETWEEN :VALUE6 AND :VALUE7 ");

                    if (isCliente)
                        query.Append(" AND ID_CLIENTE = :VALUE8 ");
                    
                    query.Append(" ) AS VALOR_TOTAL, (SELECT SUM(VALOR_LIQUIDO) FROM SEG_NFE ");
                    query.Append(" WHERE TRUE ");
                    
                    if (isEmpresa)
                        query.Append(" AND ID_EMPRESA = :VALUE1 ");
                    
                    if (isSituacao)
                        query.Append(" AND SITUACAO = :VALUE2 ");
                    
                    if (isNumeroNota)
                        query.Append(" AND NUMERO_NFE = :VALUE3 ");

                    if (isDataInicialEmissao)
                        query.Append(" AND DATA_EMISSAO BETWEEN :VALUE4 AND :VALUE5 ");

                    if (isDataCancelamentoInicial)
                        query.Append(" AND DATA_CANCELADA BETWEEN :VALUE6 AND :VALUE7 ");

                    if (isCliente)
                        query.Append(" AND ID_CLIENTE = :VALUE8 ");
                    
                    query.Append(" ) AS VALOR_LIQUIDO ");
                }

                NpgsqlDataAdapter adapterTotal = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapterTotal.SelectCommand.Parameters[0].Value = notaFiscal.Empresa != null ? notaFiscal.Empresa.Id : null;

                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapterTotal.SelectCommand.Parameters[1].Value = !String.IsNullOrEmpty(notaFiscal.Situacao) ? notaFiscal.Situacao : string.Empty;

                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                adapterTotal.SelectCommand.Parameters[2].Value = notaFiscal.NumeroNfe != null ? notaFiscal.NumeroNfe : (long?)null;

                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Timestamp));
                adapterTotal.SelectCommand.Parameters[3].Value = dataEmissaoInicial != null ? dataEmissaoInicial : (DateTime?)null;

                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Timestamp));
                adapterTotal.SelectCommand.Parameters[4].Value = dataEmissaoFinal != null ? dataEmissaoFinal : (DateTime?)null;

                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Timestamp));
                adapterTotal.SelectCommand.Parameters[5].Value = dataCancelamentoInicial != null ? dataCancelamentoInicial : (DateTime?)null;

                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Timestamp));
                adapterTotal.SelectCommand.Parameters[6].Value = dataCancelamentoFinal != null ? dataCancelamentoFinal : (DateTime?)null;

                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Integer));
                adapterTotal.SelectCommand.Parameters[7].Value = notaFiscal.Cliente != null ? notaFiscal.Cliente.Id : null;

                adapterTotal.Fill(ds, "Totais");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findNotaFiscalByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void cancela(Nfe nfe, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método cancela");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_NFE SET ");
                query.Append(" DATA_CANCELADA = NOW(), ");
                query.Append(" MOTIVO_CANCELAMENTO = :VALUE2,  "); 
                query.Append(" ID_USUARIO_CANCELA = :VALUE3,  ");
                query.Append(" SITUACAO = :VALUE4  ");
                query.Append(" WHERE ID_NFE = :VALUE1 "); 

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = nfe.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Text));
                command.Parameters[1].Value = nfe.MotivoCancelamento;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = nfe.UsuarioCancelamento.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar)); 
                command.Parameters[3].Value = ApplicationConstants.CANCELADA;
                
                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - cancela", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean findByNumero(long numeroNf, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findNotaFiscalByNumero");
            
            try
            {
                StringBuilder query = new StringBuilder();
                Boolean retorno = false;

                query.Append(" SELECT * FROM SEG_NFE WHERE NUMERO_NFE = :VALUE1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = numeroNf;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    retorno = true;
                }

                return retorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findNotaFiscalByNumero", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Nfe findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            try
            {
                StringBuilder query = new StringBuilder();

                Nfe nfe = null;

                query.Append(" SELECT NFE.ID_NFE, NFE.ID_USUARIO_CRIADOR, NFE.ID_EMPRESA, NFE.NUMERO_NFE, NFE.VALOR_TOTAL_NF, NFE.DATA_EMISSAO,   ");
                query.Append(" NFE.VALOR_PIS, NFE.VALOR_COFINS, NFE.BASE_CALCULO, NFE.ALIQUOTA_ISS, NFE.VALOR_IR, NFE.VALOR_ISS, NFE.VALOR_CSLL, ");
                query.Append(" NFE.RAZAO_SOCIAL, NFE.CNPJ, NFE.INSCRICAO, NFE.ENDERECO, NFE.NUMERO, NFE.COMPLEMENTO, NFE.BAIRRO, NFE.CIDADE,");
                query.Append(" NFE.CEP, NFE.UF, NFE.TELEFONE_1, NFE.EMAIL, NFE.DATA_GRAVACAO, NFE.DATA_CANCELADA, NFE.MOTIVO_CANCELAMENTO, ");
                query.Append(" NFE.ID_USUARIO_CANCELA, NFE.SITUACAO, NFE.ID_PLANO_FORMA, NFE.ID_CLIENTE, NFE.VALOR_LIQUIDO, NFE.ID_CENTROCUSTO");
                query.Append(" FROM SEG_NFE NFE");
                query.Append(" LEFT JOIN SEG_CENTROCUSTO CENTRO_CUSTO ON CENTRO_CUSTO.ID = NFE.ID_CENTROCUSTO ");
                query.Append(" WHERE NFE.ID_NFE = :VALUE1 ");

                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    nfe = new Nfe( dr.GetInt64(0),dr.IsDBNull(1) ? null : new Usuario(dr.GetInt64(1)), dr.IsDBNull(2) ? null : new Empresa(dr.GetInt64(2)), dr.IsDBNull(3) ? (long?)null : dr.GetInt64(3), dr.GetDecimal(4), dr.GetDateTime(5), dr.IsDBNull(6) ? null : (Decimal?)dr.GetDecimal(6), dr.IsDBNull(7) ? null : (Decimal?)dr.GetDecimal(7), dr.GetDecimal(8), dr.IsDBNull(9) ? 0 : dr.GetDecimal(9), dr.IsDBNull(10) ? null : (Decimal?)dr.GetDecimal(10), dr.IsDBNull(11) ? null : (Decimal?)dr.GetDecimal(11), dr.IsDBNull(12) ? null : (Decimal?)dr.GetDecimal(12), dr.IsDBNull(31) ? null : new Cliente(dr.GetInt64(31)), dr.GetDateTime(25), dr.IsDBNull(26) ? null : (DateTime?)dr.GetDateTime(26), dr.IsDBNull(27) ? String.Empty : dr.GetString(27), dr.IsDBNull(28) ? null : new Usuario(dr.GetInt64(28)), dr.GetString(29), dr.IsDBNull(30) ? null : new PlanoForma(dr.GetInt64(30), null, null, string.Empty), dr.IsDBNull(32) ? (Decimal?)null : dr.GetDecimal(32), dr.IsDBNull(33) ? null : new CentroCusto(dr.GetInt64(33), null, string.Empty, string.Empty));
                }

                return nfe;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void restartSequence(long numeroNfe, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método restartSequence");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append("SELECT SETVAL('SEG_NUMERONFE_SEQ', COALESCE((SELECT :VALUE1), 1), FALSE); ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = numeroNfe;
                
                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - restartSequence", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    
    
    }
}
