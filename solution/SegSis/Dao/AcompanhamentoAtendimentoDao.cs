﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;
using NpgsqlTypes;

namespace SWS.Dao
{
    class AcompanhamentoAtendimentoDao : IAcompanhamentoAtendimentoDao
    {
        public AcompanhamentoAtendimento insert(AcompanhamentoAtendimento acompanhamentoAtendimento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            StringBuilder query = new StringBuilder();

            try
            {
                acompanhamentoAtendimento.Id = recuperaProximoId(dbConnection);

                query.Append(" INSERT INTO SEG_ACOMPANHAMENTO_ATENDIMENTO ( ID_ACOMPANHAMENTO_ATENDIMENTO, NOME_FUNCIONARIO, SALA, ANDAR, CLIENTE_RAZAO_SOCIAL, RG, DATA_INCLUSAO, SENHA_ATENDIMENTO, FLAG_ATENDIDO, CPF_FUNCIONARIO, ID_ASO, CODIGO_ASO, EXAME, ID_EMPRESA, NOME_EMPRESA ) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6, NOW(), :VALUE7, FALSE, :VALUE8, :VALUE9, :VALUE10, :VALUE11, :VALUE12, :VALUE13) ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = acompanhamentoAtendimento.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = acompanhamentoAtendimento.NomeFuncionario;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = acompanhamentoAtendimento.Sala;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = acompanhamentoAtendimento.Andar;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = acompanhamentoAtendimento.RazaoSocial;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = acompanhamentoAtendimento.Rg;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = acompanhamentoAtendimento.SenhaAtendimento;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Varchar));
                command.Parameters[7].Value = acompanhamentoAtendimento.Cpf;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Integer));
                command.Parameters[8].Value = acompanhamentoAtendimento.IdAtendimento;

                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                command.Parameters[9].Value = acompanhamentoAtendimento.CodigoAtendimento;

                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Varchar));
                command.Parameters[10].Value = acompanhamentoAtendimento.Exame;

                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Integer));
                command.Parameters[11].Value = acompanhamentoAtendimento.IdEmpresa;

                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Varchar));
                command.Parameters[12].Value = acompanhamentoAtendimento.NomeEmpresa;
                
                command.ExecuteNonQuery();

                return acompanhamentoAtendimento;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_ACOMPANHAMENTO_ATENDIMENT_ID_ACOMPANHAMENTO_ATENDIMENTO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
