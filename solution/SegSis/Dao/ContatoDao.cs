﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;
using SWS.Entidade;
using NpgsqlTypes;
using System.Data;

namespace SWS.Dao
{
    class ContatoDao :IContatoDao
    {
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = 0;

            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" SELECT NEXTVAL(('SEG_CONTATO_ID_SEQ')) ", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoIdFuncao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Contato insert(Contato contato, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            StringBuilder query = new StringBuilder();

            try
            {
                contato.Id = recuperaProximoId(dbConnection);

                query.Append(" INSERT INTO SEG_CONTATO (ID, ID_CIDADE, ID_CLIENTE, ID_TIPO_CONTATO,  NOME, CPF, RG, DATA_NASCIMENTO, ENDERECO, NUMERO, COMPLEMENTO, BAIRRO, UF, CEP, TELEFONE, CELULAR, PIS_PASEP, EMAIL, RESPONSAVEL_CONTRATO, RESPONSAVEL_ESTUDO )");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6, :VALUE7, :VALUE8, :VALUE9, :VALUE10, :VALUE11, :VALUE12, :VALUE13, :VALUE14, :VALUE15, :VALUE16, :VALUE17, :VALUE18, :VALUE19, :VALUE20)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = contato.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = contato.Cidade != null ? contato.Cidade.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = contato.Cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = contato.TipoContato.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = contato.Nome;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = contato.Cpf;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = contato.Rg;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Date));
                command.Parameters[7].Value = contato.DataNascimento;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                command.Parameters[8].Value = contato.Endereco;

                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                command.Parameters[9].Value = contato.Numero;

                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Varchar));
                command.Parameters[10].Value = contato.Complemento;

                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Varchar));
                command.Parameters[11].Value = contato.Bairro;

                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Varchar));
                command.Parameters[12].Value = contato.Uf;

                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Varchar));
                command.Parameters[13].Value = contato.Cep;

                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Varchar));
                command.Parameters[14].Value = contato.Telefone;

                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Varchar));
                command.Parameters[15].Value = contato.Celular;

                command.Parameters.Add(new NpgsqlParameter("VALUE17", NpgsqlDbType.Varchar));
                command.Parameters[16].Value = contato.PisPasep;

                command.Parameters.Add(new NpgsqlParameter("VALUE18", NpgsqlDbType.Varchar));
                command.Parameters[17].Value = contato.Email;

                command.Parameters.Add(new NpgsqlParameter("VALUE19", NpgsqlDbType.Boolean));
                command.Parameters[18].Value = contato.ResponsavelContrato;

                command.Parameters.Add(new NpgsqlParameter("VALUE20", NpgsqlDbType.Boolean));
                command.Parameters[19].Value = contato.ResponsavelEstudo;

                command.ExecuteNonQuery();

                return contato;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Contato update(Contato contato, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_CONTATO SET ID_CIDADE = :VALUE2, ID_CLIENTE = :VALUE3, ID_TIPO_CONTATO = :VALUE4,  NOME = :VALUE5, CPF = :VALUE6, RG = :VALUE7, DATA_NASCIMENTO = :VALUE8, ENDERECO = :VALUE9, NUMERO = :VALUE10, COMPLEMENTO = :VALUE11, BAIRRO = :VALUE12, UF = :VALUE13, CEP = :VALUE14, TELEFONE = :VALUE15, CELULAR = :VALUE16, PIS_PASEP = :VALUE17, EMAIL = :VALUE18, RESPONSAVEL_CONTRATO = :VALUE19, RESPONSAVEL_ESTUDO = :VALUE20 ");
                query.Append(" WHERE ID = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = contato.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = contato.Cidade.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = contato.Cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = contato.TipoContato.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = contato.Nome;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = contato.Cpf;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = contato.Rg;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Date));
                command.Parameters[7].Value = contato.DataNascimento;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                command.Parameters[8].Value = contato.Endereco;

                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                command.Parameters[9].Value = contato.Numero;

                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Varchar));
                command.Parameters[10].Value = contato.Complemento;

                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Varchar));
                command.Parameters[11].Value = contato.Bairro;

                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Varchar));
                command.Parameters[12].Value = contato.Uf;

                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Varchar));
                command.Parameters[13].Value = contato.Cep;

                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Varchar));
                command.Parameters[14].Value = contato.Telefone;

                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Varchar));
                command.Parameters[15].Value = contato.Celular;

                command.Parameters.Add(new NpgsqlParameter("VALUE17", NpgsqlDbType.Varchar));
                command.Parameters[16].Value = contato.PisPasep;

                command.Parameters.Add(new NpgsqlParameter("VALUE18", NpgsqlDbType.Varchar));
                command.Parameters[17].Value = contato.Email;

                command.Parameters.Add(new NpgsqlParameter("VALUE19", NpgsqlDbType.Boolean));
                command.Parameters[18].Value = contato.ResponsavelContrato;

                command.Parameters.Add(new NpgsqlParameter("VALUE20", NpgsqlDbType.Boolean));
                command.Parameters[19].Value = contato.ResponsavelEstudo;

                command.ExecuteNonQuery();

                return contato;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Contato findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            Contato contatoRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT CONTATO.ID, CONTATO.ID_CIDADE, CONTATO.ID_CLIENTE, CONTATO.ID_TIPO_CONTATO,  CONTATO.NOME, CONTATO.CPF, CONTATO.RG, CONTATO.DATA_NASCIMENTO, CONTATO.ENDERECO, CONTATO.NUMERO, CONTATO.COMPLEMENTO, CONTATO.BAIRRO, CONTATO.UF, CONTATO.CEP, CONTATO.TELEFONE, CONTATO.CELULAR, CONTATO.PIS_PASEP, TIPO_CONTATO.DESCRICAO, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, CONTATO.EMAIL, CONTATO.RESPONSAVEL_CONTRATO, CONTATO.RESPONSAVEL_ESTUDO  ");
                query.Append(" FROM SEG_CONTATO CONTATO WHERE CONTATO.ID = :VALUE1 ");
                query.Append(" JOIN SEG_TIPO_CONTATO TIPO_CONTATO ON TIPO_CONTATO.ID_TIPO_CONTATO = TIPO_CONTATO.ID ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = CONTATO.ID_CIDADE ");
                query.Append(" WHERE CONTATO.ID = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    contatoRetorno = new Contato( dr.GetInt64(0), dr.IsDBNull(1) ? null : new CidadeIbge(dr.GetInt64(1), dr.GetString(19), dr.GetString(20), dr.GetString(21)), dr.IsDBNull(3) ? null : new TipoContato(dr.GetInt64(3), dr.GetString(17)), dr.IsDBNull(2) ? null : new Cliente(dr.GetInt64(2)), dr.IsDBNull(4) ? String.Empty : dr.GetString(4), dr.IsDBNull(5) ? String.Empty : dr.GetString(5), dr.IsDBNull(6) ? String.Empty : dr.GetString(6), dr.IsDBNull(7) ? (DateTime?)null : dr.GetDateTime(7), dr.IsDBNull(8) ? String.Empty : dr.GetString(8), dr.IsDBNull(9) ? String.Empty : dr.GetString(9), dr.IsDBNull(10) ? String.Empty : dr.GetString(10), dr.IsDBNull(11) ? String.Empty : dr.GetString(11), dr.IsDBNull(12) ? String.Empty : dr.GetString(12), dr.IsDBNull(13) ? String.Empty : dr.GetString(13), dr.IsDBNull(14) ? String.Empty : dr.GetString(14), dr.IsDBNull(15) ? String.Empty : dr.GetString(15), dr.IsDBNull(16) ? String.Empty : dr.GetString(16), dr.IsDBNull(22) ? String.Empty : dr.GetString(22), dr.GetBoolean(23), dr.GetBoolean(24));
                }

                return contatoRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void delete(Contato contato, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" DELETE FROM SEG_CONTATO ");
                query.Append(" WHERE ID = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = contato.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<Contato> findByCliente(Cliente cliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByCliente");

            List<Contato> contatos = new List<Contato>();

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT CONTATO.ID, CONTATO.ID_CIDADE, CONTATO.ID_CLIENTE, CONTATO.ID_TIPO_CONTATO,  CONTATO.NOME, CONTATO.CPF, CONTATO.RG, CONTATO.DATA_NASCIMENTO, CONTATO.ENDERECO, CONTATO.NUMERO, CONTATO.COMPLEMENTO, CONTATO.BAIRRO, CONTATO.UF, CONTATO.CEP, CONTATO.TELEFONE, CONTATO.CELULAR, CONTATO.PIS_PASEP, TIPO_CONTATO.DESCRICAO, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, CONTATO.EMAIL, CONTATO.RESPONSAVEL_CONTRATO, CONTATO.RESPONSAVEL_ESTUDO  ");
                query.Append(" FROM SEG_CONTATO CONTATO ");
                query.Append(" JOIN SEG_TIPO_CONTATO TIPO_CONTATO ON TIPO_CONTATO.ID = CONTATO.ID_TIPO_CONTATO ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = CONTATO.ID_CIDADE   ");
                query.Append(" WHERE CONTATO.ID_CLIENTE = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = cliente.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    contatos.Add(new Contato( dr.GetInt64(0), dr.IsDBNull(1) ? null : new CidadeIbge(dr.GetInt64(1), dr.GetString(18), dr.GetString(19), dr.GetString(20)), dr.IsDBNull(3) ? null : new TipoContato(dr.GetInt64(3), dr.GetString(17)), cliente, dr.IsDBNull(4) ? String.Empty : dr.GetString(4), dr.IsDBNull(5) ? String.Empty : dr.GetString(5), dr.IsDBNull(6) ? String.Empty : dr.GetString(6), dr.IsDBNull(7) ? (DateTime?)null : dr.GetDateTime(7), dr.IsDBNull(8) ? String.Empty : dr.GetString(8), dr.IsDBNull(9) ? String.Empty : dr.GetString(9), dr.IsDBNull(10) ? String.Empty : dr.GetString(10), dr.IsDBNull(11) ? String.Empty : dr.GetString(11), dr.IsDBNull(12) ? String.Empty : dr.GetString(12), dr.IsDBNull(13) ? String.Empty : dr.GetString(13), dr.IsDBNull(14) ? String.Empty : dr.GetString(14), dr.IsDBNull(15) ? String.Empty : dr.GetString(15), dr.IsDBNull(16) ? String.Empty : dr.GetString(16), dr.IsDBNull(21) ? String.Empty : dr.GetString(21), dr.GetBoolean(22), dr.GetBoolean(23)));
                }

                return contatos;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findTipoContato(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findTipoContato");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID, DESCRICAO FROM SEG_TIPO_CONTATO ORDER BY DESCRICAO ASC ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                DataSet ds = new DataSet();

                adapter.Fill(ds);

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findTipoContato", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
