﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;
using SWS.Entidade;
using NpgsqlTypes;
using System.Data;
using SWS.View.Resources;

namespace SWS.Dao
{
    class ClienteFuncaoSetorDao : IClienteFuncaoSetorDao
    {
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_CLIENTE_FUNCAO_SETOR_ID_CLIENTE_FUNCAO_SETOR_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void insertClienteFuncaoSetor(ClienteFuncaoSetor clienteFuncaoSetor, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertClienteFuncaoSetor");

            try
            {
                clienteFuncaoSetor.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_CLIENTE_FUNCAO_SETOR ");
                query.Append(" (ID_CLIENTE_FUNCAO_SETOR, ID_SETOR, ID_CLIENTE_FUNCAO, SITUACAO )");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncaoSetor.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = clienteFuncaoSetor.Setor.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = clienteFuncaoSetor.ClienteFuncao.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = clienteFuncaoSetor.Situacao;
                
                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertClienteFuncaoSetor", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public void deleteClienteFuncaoSetor(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteClienteFuncaoSetor");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" DELETE FROM SEG_CLIENTE_FUNCAO_SETOR WHERE ID_CLIENTE_FUNCAO_SETOR = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertClienteFuncaoSetor", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public DataSet findAllSetorByClienteFuncao(ClienteFuncao clienteFuncao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllSetorByClienteFuncao");

            StringBuilder query = new StringBuilder();

            try
            {

                query.Append(" SELECT CFS.ID_CLIENTE_FUNCAO_SETOR, CFS.ID_SETOR, S.DESCRICAO, S.SITUACAO FROM SEG_CLIENTE_FUNCAO_SETOR CFS ");
                query.Append(" JOIN SEG_SETOR S ON S.ID_SETOR = CFS.ID_SETOR ");
                query.Append(" WHERE CFS.ID_CLIENTE_FUNCAO = :VALUE1 AND CFS.SITUACAO = :VALUE2 ");
                query.Append(" ORDER BY S.DESCRICAO ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = clienteFuncao.Id;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = ApplicationConstants.ATIVO;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Setores");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllSetorByClienteFuncao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean findClienteFuncaoSetorByClienteFuncaoExameAso(ClienteFuncaoSetor clienteFuncaoSetor, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteFuncaoSetorByClienteFuncaoExameAso");
            
            StringBuilder query = new StringBuilder();

            Boolean retorno = false;

            try
            {
                query.Append(" SELECT CFEA.ID_CLIENTE_FUNCAO_EXAME_ASO, CFEA.ID_ASO, CFEA.ID_CLIENTE_FUNCAO_EXAME FROM SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME CFE ON CFE.ID_CLIENTE_FUNCAO_EXAME = CFEA.ID_CLIENTE_FUNCAO_EXAME  ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO CF ON CF.ID_SEG_CLIENTE_FUNCAO = CFE.ID_CLIENTE_FUNCAO ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_SETOR CFS ON CFS.ID_CLIENTE_FUNCAO = CF.ID_SEG_CLIENTE_FUNCAO ");
                query.Append(" WHERE CFS.ID_CLIENTE_FUNCAO_SETOR = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncaoSetor.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    retorno = true;
                    break;
                }
                
                return retorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteFuncaoSetorByClienteFuncaoExameAso", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public void desativaClienteFuncaoSetor(ClienteFuncaoSetor clienteFuncaoSetor, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método desativaClienteFuncaoSetor");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_CLIENTE_FUNCAO_SETOR SET SITUACAO = :VALUE2, DATA_EXCLUSAO = NOW(), USUARIO = :VALUE3 " );
                query.Append(" WHERE ID_CLIENTE_FUNCAO_SETOR = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncaoSetor.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = clienteFuncaoSetor.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = clienteFuncaoSetor.Usuario.Nome;
                
                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - desativaClienteFuncaoSetor", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

    }
}
