﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using Npgsql;
using SWS.Helper;
using NpgsqlTypes;
using SWS.Excecao;

using SWS.View.Resources;

namespace SWS.Dao
{
    class ClienteFuncaoExameDao : IClienteFuncaoExameDao
    {
        public ClienteFuncaoExame insert(ClienteFuncaoExame clienteFuncaoExame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            StringBuilder query = new StringBuilder();

            try
            {
                clienteFuncaoExame.Id = this.recuperaProximoId(dbConnection);

                query.Append(" INSERT INTO SEG_CLIENTE_FUNCAO_EXAME ");
                query.Append(" (ID_CLIENTE_FUNCAO_EXAME, ID_CLIENTE_FUNCAO, ID_EXAME, SITUACAO, IDADE_EXAME) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, 'A', :VALUE4)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncaoExame.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = clienteFuncaoExame.ClienteFuncao.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = clienteFuncaoExame.Exame.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = clienteFuncaoExame.IdadeExame;

                command.ExecuteNonQuery();

                return clienteFuncaoExame;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_CLIENTE_FUNCAO_EXAME_ID_CLIENTE_FUNCAO_EXAME_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoIdClienteFuncaoExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public ClienteFuncaoExame findByClienteFuncaoAndExame(ClienteFuncao clienteFuncao, Exame exame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteFuncaoExameByClienteFuncaoAndExame");

            ClienteFuncaoExame clienteFuncaoExame = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_CLIENTE_FUNCAO_EXAME, ID_CLIENTE_FUNCAO, ID_EXAME, SITUACAO, IDADE_EXAME ");
                query.Append(" FROM SEG_CLIENTE_FUNCAO_EXAME WHERE ID_CLIENTE_FUNCAO = :VALUE1 AND ID_EXAME = :VALUE2 AND SITUACAO = 'A'");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncao.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = exame.Id;
                

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    clienteFuncaoExame = new ClienteFuncaoExame(dr.GetInt64(0), new ClienteFuncao(dr.GetInt64(1)), exame, dr.GetString(3), dr.GetInt32(4));
                }

                return clienteFuncaoExame;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteFuncaoExameByClienteFuncaoAndExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean isUsedByClienteFuncaoExame(ClienteFuncaoExame clienteFuncaoExame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaFoiUsado");

            Boolean podeExcluir = false;

            StringBuilder query = new StringBuilder();

            try
            {

                query.Append(" SELECT ID_CLIENTE_FUNCAO_EXAME_ASO, ID_ASO, ID_CLIENTE_FUNCAO_EXAME, ID_SALA_EXAME, ");
                query.Append(" ATENDIDO, FINALIZADO, LOGIN, DEVOLVIDO, DATA_ATENDIDO, DATA_DEVOLVIDO, ");
                query.Append(" DATA_FINALIZADO, ID_USUARIO, CANCELADO, DATA_CANCELADO ");
                query.Append(" FROM SEG_CLIENTE_FUNCAO_EXAME_ASO WHERE ID_CLIENTE_FUNCAO_EXAME = :VALUE1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncaoExame.Id;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    podeExcluir = true;
                }

                return podeExcluir;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaFoiUsado", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void delete(ClienteFuncaoExame clienteFuncaoExame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("DELETE FROM SEG_CLIENTE_FUNCAO_EXAME WHERE ID_CLIENTE_FUNCAO_EXAME = :VALUE1", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncaoExame.Id;

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void disable(ClienteFuncaoExame clienteFuncaoExame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método disable");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("UPDATE SEG_CLIENTE_FUNCAO_EXAME SET SITUACAO = 'I' WHERE ID_CLIENTE_FUNCAO_EXAME = :VALUE1", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncaoExame.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - disable", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Exame findExameByClienteFuncaoExame(ClienteFuncaoExame clienteFuncaoExame, NpgsqlConnection dbconnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findExameByClienteFuncaoExameExame");

            Exame exame = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT EXAME.ID_EXAME, EXAME.DESCRICAO, EXAME.SITUACAO, EXAME.LABORATORIO, EXAME.PRECO, ");
                query.Append(" EXAME.PRIORIDADE, EXAME.CUSTO, EXAME.EXTERNO, EXAME.LIBERA_DOCUMENTO, EXAME.CODIGO_TUSS, EXAME.EXAME_COMPLEMENTAR, EXAME.PERIODO_VENCIMENTO, EXAME.PADRAO_CONTRATO ");
                query.Append(" FROM SEG_CLIENTE_FUNCAO_EXAME CLIENTE_FUNCAO_EXAME");
                query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = CLIENTE_FUNCAO_EXAME.ID_EXAME");
                query.Append(" WHERE CLIENTE_FUNCAO_EXAME.ID_CLIENTE_FUNCAO_EXAME = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbconnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncaoExame.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    exame = new Exame(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetBoolean(3), dr.GetDecimal(4), dr.GetInt32(5), dr.GetDecimal(6), dr.GetBoolean(7), dr.GetBoolean(8), dr.IsDBNull(9) ? string.Empty : dr.GetString(9), dr.GetBoolean(10), dr.IsDBNull(11) ? (int?)null : dr.GetInt32(11), dr.GetBoolean(12));
                }

                return exame;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findExameByClienteFuncaoExameExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void update(ClienteFuncaoExame clienteFuncaoExame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_CLIENTE_FUNCAO_EXAME SET IDADE_EXAME = :VALUE2 ");
                query.Append(" WHERE ID_CLIENTE_FUNCAO_EXAME = :VALUE1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(),dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncaoExame.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = clienteFuncaoExame.IdadeExame;
                
                command.ExecuteNonQuery();
                
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean IsUsedByExame(Exame exame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteFuncaoExameByExame");

            Boolean isClienteFuncaoExameInUsed = false;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT COUNT(*) FROM SEG_CLIENTE_FUNCAO_EXAME CLIENTE_FUNCAO_EXAME ");
                query.Append(" WHERE CLIENTE_FUNCAO_EXAME.ID_EXAME = :VALUE1 AND CLIENTE_FUNCAO_EXAME.SITUACAO = 'A' ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = exame.Id;
                

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    if (dr.GetInt64(0) > 0)
                    {
                        isClienteFuncaoExameInUsed = true;
                    }
                }

                return isClienteFuncaoExameInUsed;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteFuncaoExameByExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public Boolean isPrimeiroAtendimento(Aso aso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método isPrimeiroAtendimento");

            StringBuilder query = new StringBuilder();

            Boolean primeiroAtendimento = true;

            try
            {
                query.Append(" SELECT COUNT(*) FROM SEG_CLIENTE_FUNCAO_EXAME_ASO CLIENTE_FUNCAO_EXAME_ASO ");
                query.Append(" WHERE CLIENTE_FUNCAO_EXAME_ASO.ID_ASO = :VALUE1 ");
                query.Append(" AND CLIENTE_FUNCAO_EXAME_ASO.ATENDIDO = FALSE AND CLIENTE_FUNCAO_EXAME_ASO.FINALIZADO = FALSE AND CLIENTE_FUNCAO_EXAME_ASO.CANCELADO = FALSE");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    if (dr.GetInt64(0) > 0)
                    {
                        primeiroAtendimento = false;
                    }
                }

                return primeiroAtendimento;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - isPrimeiroAtendimento", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public ClienteFuncaoExame findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            ClienteFuncaoExame clienteFuncaoExame = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT CLIENTE_FUNCAO_EXAME.ID_CLIENTE_FUNCAO_EXAME, CLIENTE_FUNCAO_EXAME.ID_CLIENTE_FUNCAO, CLIENTE_FUNCAO_EXAME.ID_EXAME, CLIENTE_FUNCAO_EXAME.SITUACAO, CLIENTE_FUNCAO_EXAME.IDADE_EXAME, ");
                query.Append(" CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO, CLIENTE_FUNCAO.ID_CLIENTE, CLIENTE_FUNCAO.ID_FUNCAO, CLIENTE_FUNCAO.SITUACAO, CLIENTE_FUNCAO.DATA_CADASTRO, CLIENTE_FUNCAO.DATA_DESLIGAMENTO, ");
                query.Append(" FUNCAO.ID_FUNCAO, FUNCAO.DESCRICAO, FUNCAO.COD_CBO, FUNCAO.SITUACAO, FUNCAO.COMENTARIO, ");
                query.Append(" EXAME.ID_EXAME, EXAME.DESCRICAO, EXAME.SITUACAO, EXAME.LABORATORIO, EXAME.PRECO, EXAME.PRIORIDADE, EXAME.CUSTO, EXAME.EXTERNO, EXAME.LIBERA_DOCUMENTO, EXAME.CODIGO_TUSS, EXAME.EXAME_COMPLEMENTAR, EXAME.PERIODO_VENCIMENTO, EXAME.PADRAO_CONTRATO ");
                query.Append(" FROM SEG_CLIENTE_FUNCAO_EXAME CLIENTE_FUNCAO_EXAME ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTE_FUNCAO_EXAME.ID_CLIENTE_FUNCAO ");
                query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTE_FUNCAO.ID_FUNCAO ");
                query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = CLIENTE_FUNCAO_EXAME.ID_EXAME ");
                query.Append(" WHERE ID_CLIENTE_FUNCAO_EXAME = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    clienteFuncaoExame = new ClienteFuncaoExame(dr.GetInt64(0), dr.IsDBNull(1) ? null : new ClienteFuncao(dr.GetInt64(5), new Cliente(dr.GetInt64(6)), new Funcao(dr.GetInt64(11), dr.GetString(12), dr.GetString(13), dr.GetString(14), dr.GetString(15)), dr.GetString(8), dr.IsDBNull(9) ? null : (DateTime?)dr.GetDateTime(9), dr.IsDBNull(10) ? null : (DateTime?)dr.GetDateTime(10)), dr.IsDBNull(16) ? null : new Exame(dr.GetInt64(16), dr.GetString(17), dr.GetString(18), dr.GetBoolean(19), dr.GetDecimal(20), dr.GetInt32(21), dr.GetDecimal(22), dr.GetBoolean(23), dr.GetBoolean(24), dr.IsDBNull(25) ? string.Empty : dr.GetString(25), dr.GetBoolean(26), dr.IsDBNull(27) ? (int?)null : dr.GetInt32(27), dr.GetBoolean(28)), dr.GetString(3), dr.GetInt32(4)); 
                }

                return clienteFuncaoExame;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<Aso> findAllAtendimentoInSituacaoByClienteByPeriodo(Cliente cliente, DateTime dataInicial, DateTime dataFinal, string situacao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllAtendimentoInSituacaoByClienteByPeriodo");

            List<Aso> atendimento = new List<Aso>();

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" ");
                query.Append(" ");
                query.Append(" ");
                query.Append(" ");
                query.Append(" ");
                query.Append(" ");
                query.Append(" ");
                query.Append(" ");
                query.Append(" ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                command.Parameters[1].Value = dataInicial;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Timestamp));
                command.Parameters[2].Value = dataFinal;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = situacao;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    
                }

                return atendimento;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllAtendimentoInSituacaoByClienteByPeriodo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }


    }
}
