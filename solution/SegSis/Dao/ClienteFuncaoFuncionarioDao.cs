﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using SWS.Excecao;
using Npgsql;
using NpgsqlTypes;
using SWS.IDao;
using SWS.View.Resources;
using SWS.Helper;

namespace SWS.Dao
{
    class ClienteFuncaoFuncionarioDao : IClienteFuncaoFuncionarioDao
    {
        
        public ClienteFuncaoFuncionario insert(ClienteFuncaoFuncionario clienteFuncaoFuncionario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertClienteFuncaoFuncionario");

            try
            {
                clienteFuncaoFuncionario.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_CLIENTE_FUNCAO_FUNCIONARIO ");
                query.Append(" (ID_CLIENTE_FUNCAO_FUNCIONARIO, ID_SEG_CLIENTE_FUNCAO, ");
                query.Append(" ID_FUNCIONARIO, SITUACAO, DATA_CADASTRO, MATRICULA, VIP, BR_PDH, REGIME_REVEZAMENTO, ESTAGIARIO, VINCULO) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, 'A', :VALUE10, :VALUE4, :VALUE5, :VALUE6, :VALUE7, :VALUE8, :VALUE9)");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncaoFuncionario.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = clienteFuncaoFuncionario.ClienteFuncao.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = clienteFuncaoFuncionario.Funcionario.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = clienteFuncaoFuncionario.Matricula;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Boolean)); 
                command.Parameters[4].Value = clienteFuncaoFuncionario.Vip;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = clienteFuncaoFuncionario.BrPdh;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = clienteFuncaoFuncionario.RegimeRevezamento;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Boolean));
                command.Parameters[7].Value = clienteFuncaoFuncionario.Estagiario;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Boolean));
                command.Parameters[8].Value = clienteFuncaoFuncionario.Vinculo;

                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Date));
                command.Parameters[9].Value = clienteFuncaoFuncionario.DataCadastro;
                
                command.ExecuteNonQuery();

                return clienteFuncaoFuncionario;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_CLIENTE_FUNCAO_FUNCIONARI_ID_CLIENTE_FUNCAO_FUNCIONARIO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public ClienteFuncaoFuncionario findAtivoByFuncionario(Funcionario funcionario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteFuncaoFuncionario");

            ClienteFuncaoFuncionario clienteFuncaoFuncionario = null;
            
            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT CLIENTE.ID_CLIENTE, CLIENTE.RAZAO_SOCIAL, CLIENTE.FANTASIA, CLIENTE.CNPJ, CLIENTE.INSCRICAO, ");
                query.Append(" CLIENTE.ENDERECO, CLIENTE.NUMERO, CLIENTE.COMPLEMENTO, CLIENTE.BAIRRO, CLIENTE.CEP, CLIENTE.UF, ");
                query.Append(" CLIENTE.TELEFONE1, CLIENTE.TELEFONE2, CLIENTE.EMAIL, CLIENTE.SITE, CLIENTE.DATA_CADASTRO, ");
                query.Append(" CLIENTE.RESPONSAVEL, CLIENTE.CARGO, CLIENTE.DOCUMENTO, CLIENTE.ESCOPO, CLIENTE.JORNADA, CLIENTE.SITUACAO, ");
                query.Append(" CLIENTE.EST_MASC, CLIENTE.EST_FEM, CLIENTE.ENDERECOCOB, CLIENTE.NUMEROCOB, CLIENTE.COMPLEMENTOCOB, ");
                query.Append(" CLIENTE.BAIRROCOB, CLIENTE.CEPCOB, CLIENTE.UFCOB, CLIENTE.TELEFONECOB, CLIENTE.ID_MEDICO, ");
                query.Append(" CLIENTE.TELEFONE2COB, CLIENTE.VIP, CLIENTE.TELEFONE_CONTATO, CLIENTE.USA_CONTRATO, CLIENTE.ID_CLIENTE_MATRIZ, CLIENTE.ID_CLIENTE_CREDENCIADO, CLIENTE.PARTICULAR, CLIENTE.UNIDADE, CLIENTE.CREDENCIADA, ");
                query.Append(" CLIENTE.PRESTADOR, CLIENTE.ID_CIDADE_IBGE,  ");
                query.Append(" CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, CLIENTE.ID_CIDADE_IBGE_COBRANCA, CIDADE_COBRANCA.CODIGO, CIDADE_COBRANCA.NOME, CIDADE_COBRANCA.UF_CIDADE, ");
                query.Append(" FUNCAO.ID_FUNCAO, FUNCAO.DESCRICAO, FUNCAO.COD_CBO, FUNCAO.SITUACAO, FUNCAO.COMENTARIO, ");
                query.Append(" CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO, CLIENTE_FUNCAO.SITUACAO, ");
                query.Append(" CLIENTE_FUNCAO.DATA_CADASTRO, CLIENTE_FUNCAO.DATA_DESLIGAMENTO, CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO, ");
                query.Append(" CLIENTE_FUNCAO_FUNCIONARIO.SITUACAO, CLIENTE_FUNCAO_FUNCIONARIO.DATA_CADASTRO, ");
                query.Append(" CLIENTE_FUNCAO_FUNCIONARIO.DATA_DESLIGAMENTO, ");
                query.Append(" CLIENTE.DESTACA_ISS, CLIENTE.GERA_COBRANCA_VALOR_LIQUIDO, CLIENTE.ALIQUOTA_ISS, CLIENTE_FUNCAO_FUNCIONARIO.MATRICULA, CLIENTE_FUNCAO_FUNCIONARIO.VIP, CLIENTE.CODIGO_CNES, CLIENTE.USA_CENTRO_CUSTO, CLIENTE.BLOQUEADO, CLIENTE.CREDENCIADORA, CLIENTE.FISICA, CLIENTE.USA_PO, CLIENTE_FUNCAO_FUNCIONARIO.BR_PDH, CLIENTE_FUNCAO_FUNCIONARIO.REGIME_REVEZAMENTO, CLIENTE_FUNCAO_FUNCIONARIO.ESTAGIARIO, CLIENTE_FUNCAO_FUNCIONARIO.VINCULO, CLIENTE.SIMPLES_NACIONAL ");
                query.Append(" FROM SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTE_FUNCAO_FUNCIONARIO ");
                query.Append(" INNER JOIN SEG_FUNCIONARIO FUNCIONARIO ON FUNCIONARIO.ID_FUNCIONARIO = CLIENTE_FUNCAO_FUNCIONARIO.ID_FUNCIONARIO ");
                query.Append(" INNER JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTE_FUNCAO_FUNCIONARIO.ID_SEG_CLIENTE_FUNCAO ");
                query.Append(" INNER JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTE_FUNCAO.ID_FUNCAO ");
                query.Append(" INNER JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = CLIENTE_FUNCAO.ID_CLIENTE ");
                query.Append(" JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = CLIENTE.ID_CIDADE_IBGE ");
                query.Append(" JOIN SEG_CIDADE_IBGE CIDADE_COBRANCA ON CIDADE_COBRANCA.ID_CIDADE_IBGE = CLIENTE.ID_CIDADE_IBGE_COBRANCA ");
                query.Append(" WHERE FUNCIONARIO.ID_FUNCIONARIO = :VALUE1 AND ");
                query.Append(" CLIENTE_FUNCAO.SITUACAO = 'A' ");
                                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = funcionario.Id;
                
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    clienteFuncaoFuncionario = new ClienteFuncaoFuncionario(
                        dr.GetInt64(59), 
                        new ClienteFuncao(
                            dr.GetInt64(55), 
                            new Cliente(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.GetString(4), dr.GetString(5), dr.GetString(6), dr.GetString(7), dr.GetString(8), dr.GetString(9), dr.GetString(10), dr.GetString(11), dr.GetString(12), dr.GetString(13), dr.GetString(14), dr.GetDateTime(15), dr.GetString(16), dr.GetString(17), dr.GetString(18), dr.GetString(19), dr.GetString(20), dr.GetString(21), dr.GetInt32(22), dr.GetInt32(23), dr.GetString(24), dr.GetString(25), dr.GetString(26), dr.GetString(27), dr.GetString(28), dr.GetString(29), dr.GetString(30), dr.IsDBNull(31) ? null : new Medico(dr.GetInt64(31)), dr.GetString(32), dr.GetBoolean(33), dr.GetString(34), dr.GetBoolean(35), dr.IsDBNull(36) ? null : new Cliente(dr.GetInt64(36)), dr.IsDBNull(37) ? null : new Cliente(dr.GetInt64(37)), dr.GetBoolean(38), dr.GetBoolean(39), dr.GetBoolean(40), dr.GetBoolean(41), dr.IsDBNull(42) ? null : new CidadeIbge(dr.GetInt64(42), dr.GetString(43), dr.GetString(44), dr.GetString(45)), dr.IsDBNull(46) ? null : new CidadeIbge(dr.GetInt64(46), dr.GetString(47), dr.GetString(48), dr.GetString(49)), dr.GetBoolean(63), dr.GetBoolean(64), dr.GetDecimal(65), dr.IsDBNull(68) ? string.Empty : dr.GetString(68), dr.GetBoolean(69), dr.GetBoolean(70), dr.GetBoolean(71), dr.GetBoolean(72), dr.GetBoolean(73), dr.GetBoolean(78)), 
                            new Funcao(dr.GetInt64(50), dr.GetString(51), dr.GetString(52), dr.GetString(53), dr.GetString(54)), 
                            dr.GetString(56), 
                            dr.GetDateTime(57), 
                            dr.IsDBNull(58) ? (DateTime?)null : dr.GetDateTime(58)),  
                        funcionario, 
                        dr.GetString(60), 
                        dr.GetDateTime(61), 
                        dr.IsDBNull(62) ? (DateTime?)null : dr.GetDateTime(62), 
                        dr.IsDBNull(66) ? string.Empty : dr.GetString(66), 
                        dr.GetBoolean(67), 
                        dr.IsDBNull(74) ? string.Empty : dr.GetString(74), 
                        dr.IsDBNull(75) ? string.Empty : dr.GetString(75), 
                        dr.GetBoolean(76), 
                        dr.GetBoolean(77));
                }

                return clienteFuncaoFuncionario;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtivoByFuncionario", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void disable(ClienteFuncaoFuncionario clienteFuncaoFuncionario, DateTime dataDemissao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método disable");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_CLIENTE_FUNCAO_FUNCIONARIO SET ");
                query.Append(" SITUACAO = 'I', DATA_DESLIGAMENTO = :VALUE2 ");
                query.Append(" WHERE ID_CLIENTE_FUNCAO_FUNCIONARIO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer)); 
                command.Parameters[0].Value = clienteFuncaoFuncionario.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Date));
                command.Parameters[1].Value = dataDemissao;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - disable", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public ClienteFuncaoFuncionario findById(Int64? id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteFuncaoFuncionarioById");

            ClienteFuncaoFuncionario clienteFuncaoFuncionario = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT CLIENTE.ID_CLIENTE, CLIENTE.RAZAO_SOCIAL, CLIENTE.FANTASIA, CLIENTE.CNPJ, CLIENTE.INSCRICAO, CLIENTE.ENDERECO, ");
                query.Append(" CLIENTE.NUMERO, CLIENTE.COMPLEMENTO, CLIENTE.BAIRRO, CLIENTE.CEP, CLIENTE.UF, CLIENTE.TELEFONE1, CLIENTE.TELEFONE2, ");
                query.Append(" CLIENTE.EMAIL, CLIENTE.SITE, CLIENTE.DATA_CADASTRO, CLIENTE.RESPONSAVEL, CLIENTE.CARGO, CLIENTE.DOCUMENTO, CLIENTE.ESCOPO, ");
                query.Append(" CLIENTE.JORNADA, CLIENTE.SITUACAO, CLIENTE.EST_MASC, CLIENTE.EST_FEM, CLIENTE.ENDERECOCOB, CLIENTE.NUMEROCOB, ");
                query.Append(" CLIENTE.COMPLEMENTOCOB, CLIENTE.BAIRROCOB, CLIENTE.CEPCOB, CLIENTE.UFCOB, CLIENTE.TELEFONECOB, ");
                query.Append(" CLIENTE.ID_MEDICO, CLIENTE.TELEFONE2COB, CLIENTE.VIP, CLIENTE.TELEFONE_CONTATO, CLIENTE.USA_CONTRATO, CLIENTE.ID_CLIENTE_MATRIZ, CLIENTE.ID_CLIENTE_CREDENCIADO, ");
                query.Append(" CLIENTE.PARTICULAR, CLIENTE.UNIDADE, CLIENTE.CREDENCIADA, CLIENTE.PRESTADOR, CLIENTE.ID_CIDADE_IBGE, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, ");
                query.Append(" CLIENTE.ID_CIDADE_IBGE_COBRANCA, CIDADE_COBRANCA.CODIGO, CIDADE_COBRANCA.NOME, CIDADE_COBRANCA.UF_CIDADE, ");
                query.Append(" FUNCAO.ID_FUNCAO, FUNCAO.DESCRICAO, FUNCAO.COD_CBO, FUNCAO.SITUACAO, FUNCAO.COMENTARIO, ");
                query.Append(" CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO, CLIENTE_FUNCAO.SITUACAO, CLIENTE_FUNCAO.DATA_CADASTRO, CLIENTE_FUNCAO.DATA_DESLIGAMENTO, ");
                query.Append(" FUNCIONARIO.ID_FUNCIONARIO, FUNCIONARIO.NOME, FUNCIONARIO.CPF, FUNCIONARIO.RG, FUNCIONARIO.ENDERECO, FUNCIONARIO.NUMERO, FUNCIONARIO.COMPLEMENTO, ");
                query.Append(" FUNCIONARIO.BAIRRO, FUNCIONARIO.ID_CIDADE, FUNCIONARIO.UF, FUNCIONARIO.TELEFONE1, FUNCIONARIO.TELEFONE2, FUNCIONARIO.EMAIL, FUNCIONARIO.TIPOSAN, ");
                query.Append(" FUNCIONARIO.FTRH, FUNCIONARIO.DATA_NASCIMENTO, FUNCIONARIO.PESO, FUNCIONARIO.ALTURA, FUNCIONARIO.SITUACAO, FUNCIONARIO.CEP, FUNCIONARIO.SEXO, ");
                query.Append(" CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO, CLIENTE_FUNCAO_FUNCIONARIO.SITUACAO, CLIENTE_FUNCAO_FUNCIONARIO.DATA_CADASTRO, CLIENTE_FUNCAO_FUNCIONARIO.DATA_DESLIGAMENTO, CLIENTE_FUNCAO_FUNCIONARIO.MATRICULA, CLIENTE_FUNCAO_FUNCIONARIO.VIP,  ");
                query.Append(" CLIENTE.DESTACA_ISS, CLIENTE.GERA_COBRANCA_VALOR_LIQUIDO, CLIENTE.ALIQUOTA_ISS, FUNCIONARIO.CTPS, FUNCIONARIO.PIS_PASEP, CIDADE_FUNCIONARIO.CODIGO, CIDADE_FUNCIONARIO.NOME, CIDADE_FUNCIONARIO.UF_CIDADE, CLIENTE.CODIGO_CNES, CLIENTE.USA_CENTRO_CUSTO, CLIENTE.BLOQUEADO, CLIENTE.CREDENCIADORA, CLIENTE.FISICA, FUNCIONARIO.PCD, FUNCIONARIO.ORGAO_EMISSOR, FUNCIONARIO.UF_EMISSOR, CLIENTE.USA_PO, FUNCIONARIO.SERIE, FUNCIONARIO.UF_EMISSOR_CTPS, CLIENTE_FUNCAO_FUNCIONARIO.BR_PDH, CLIENTE_FUNCAO_FUNCIONARIO.REGIME_REVEZAMENTO, CLIENTE_FUNCAO_FUNCIONARIO.ESTAGIARIO, CLIENTE_FUNCAO_FUNCIONARIO.VINCULO, CLIENTE.SIMPLES_NACIONAL ");
                query.Append(" FROM SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTE_FUNCAO_FUNCIONARIO ");
                query.Append(" INNER JOIN SEG_FUNCIONARIO FUNCIONARIO ON FUNCIONARIO.ID_FUNCIONARIO = CLIENTE_FUNCAO_FUNCIONARIO.ID_FUNCIONARIO ");
                query.Append(" INNER JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTE_FUNCAO_FUNCIONARIO.ID_SEG_CLIENTE_FUNCAO ");
                query.Append(" INNER JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTE_FUNCAO.ID_FUNCAO ");
                query.Append(" INNER JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = CLIENTE_FUNCAO.ID_CLIENTE ");
                query.Append(" JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = CLIENTE.ID_CIDADE_IBGE ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE_COBRANCA ON CIDADE_COBRANCA.ID_CIDADE_IBGE = CLIENTE.ID_CIDADE_IBGE_COBRANCA ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE_FUNCIONARIO ON CIDADE_FUNCIONARIO.ID_CIDADE_IBGE = FUNCIONARIO.ID_CIDADE ");
                query.Append(" WHERE CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer)); 
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    clienteFuncaoFuncionario = new ClienteFuncaoFuncionario(
                        dr.GetInt64(80), 
                        new ClienteFuncao(
                            dr.GetInt64(55), 
                            new Cliente(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.GetString(4), dr.GetString(5), dr.GetString(6), dr.GetString(7), dr.GetString(8), dr.GetString(9), dr.GetString(10), dr.GetString(11), dr.GetString(12), dr.GetString(13), dr.GetString(14), dr.GetDateTime(15), dr.GetString(16), dr.GetString(17), dr.GetString(18), dr.GetString(19), dr.GetString(20), dr.GetString(21), dr.GetInt32(22), dr.GetInt32(23), dr.GetString(24), dr.GetString(25), dr.GetString(26), dr.GetString(27), dr.GetString(28), dr.GetString(29), dr.GetString(30), dr.IsDBNull(31) ? null : new Medico(dr.GetInt64(31)),  dr.GetString(32), dr.GetBoolean(33), dr.GetString(34), dr.GetBoolean(35), dr.IsDBNull(36) ? null : new Cliente(dr.GetInt64(36)), dr.IsDBNull(37) ? null : new Cliente(dr.GetInt64(37)),  dr.GetBoolean(38), dr.GetBoolean(39), dr.GetBoolean(40), dr.GetBoolean(41),  dr.IsDBNull(42) ? null : new CidadeIbge(dr.GetInt64(42), dr.GetString(43), dr.GetString(44), dr.GetString(45)), dr.IsDBNull(46) ? null : new CidadeIbge(dr.GetInt64(46), dr.GetString(47), dr.GetString(48), dr.GetString(49)), dr.GetBoolean(86), dr.GetBoolean(87), dr.GetDecimal(88), dr.IsDBNull(94) ? string.Empty : dr.GetString(94), dr.GetBoolean(95), dr.GetBoolean(96), dr.GetBoolean(97), dr.GetBoolean(98), dr.GetBoolean(102), dr.GetBoolean(109)), 
                            new Funcao(
                                dr.GetInt64(50), 
                                dr.GetString(51), 
                                dr.GetString(52), 
                                dr.GetString(53), 
                                dr.GetString(54)), 
                            dr.GetString(56), 
                            dr.GetDateTime(57), 
                            dr.IsDBNull(58) ? (DateTime?)null : dr.GetDateTime(58)), 
                        new Funcionario(
                            dr.GetInt64(59), 
                            dr.GetString(60), 
                            dr.GetString(61), 
                            dr.GetString(62), 
                            dr.GetString(63), 
                            dr.GetString(64), 
                            dr.GetString(65), 
                            dr.GetString(66), 
                            dr.IsDBNull(67)? null : new CidadeIbge(dr.GetInt64(67), dr.GetString(91), dr.GetString(92), dr.GetString(93)), 
                            dr.GetString(68), 
                            dr.GetString(69), 
                            dr.GetString(70), 
                            dr.GetString(71), 
                            dr.GetString(72), 
                            dr.GetString(73), 
                            dr.GetDateTime(74), 
                            dr.GetInt32(75), 
                            dr.GetDecimal(76), 
                            dr.GetString(77), 
                            dr.GetString(78), 
                            dr.GetString(79), 
                            dr.IsDBNull(89) ? String.Empty : dr.GetString(89), 
                            dr.IsDBNull(90) ? String.Empty : dr.GetString(90), 
                            dr.GetBoolean(99), 
                            dr.IsDBNull(100) ? string.Empty : dr.GetString(100), 
                            dr.IsDBNull(101) ? string.Empty : dr.GetString(101), 
                            dr.IsDBNull(103) ? string.Empty : dr.GetString(103), 
                            dr.IsDBNull(104) ? string.Empty : dr.GetString(104)), 
                        dr.GetString(81), 
                        dr.GetDateTime(82), 
                        dr.IsDBNull(83) ? (DateTime?)null : dr.GetDateTime(83), 
                        dr.IsDBNull(84) ? String.Empty : dr.GetString(84), 
                        dr.GetBoolean(85), 
                        dr.IsDBNull(105) ? string.Empty : dr.GetString(105), 
                        dr.IsDBNull(106) ? string.Empty : dr.GetString(106), 
                        dr.GetBoolean(107), 
                        dr.GetBoolean(108));              
                }

                return clienteFuncaoFuncionario;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteFuncaoFuncionario", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public ClienteFuncaoFuncionario findByFuncionarioAndSituacao(Funcionario funcionario, String str, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByFuncionarioAndSituacao");

            ClienteFuncaoFuncionario clienteFuncaoFuncionario = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT CLIENTE.ID_CLIENTE, CLIENTE.RAZAO_SOCIAL, CLIENTE.FANTASIA, CLIENTE.CNPJ, CLIENTE.INSCRICAO, ");
                query.Append(" CLIENTE.ENDERECO, CLIENTE.NUMERO, CLIENTE.COMPLEMENTO, CLIENTE.BAIRRO, CLIENTE.CEP, CLIENTE.UF, ");
                query.Append(" CLIENTE.TELEFONE1, CLIENTE.TELEFONE2, CLIENTE.EMAIL, CLIENTE.SITE, CLIENTE.DATA_CADASTRO, ");
                query.Append(" CLIENTE.RESPONSAVEL, CLIENTE.CARGO, CLIENTE.DOCUMENTO, CLIENTE.ESCOPO, CLIENTE.JORNADA, CLIENTE.SITUACAO, ");
                query.Append(" CLIENTE.EST_MASC, CLIENTE.EST_FEM, CLIENTE.ENDERECOCOB, CLIENTE.NUMEROCOB, CLIENTE.COMPLEMENTOCOB, ");
                query.Append(" CLIENTE.BAIRROCOB, CLIENTE.CEPCOB, CLIENTE.UFCOB, CLIENTE.TELEFONECOB, CLIENTE.ID_MEDICO, ");
                query.Append(" CLIENTE.TELEFONE2COB, CLIENTE.VIP, CLIENTE.TELEFONE_CONTATO, CLIENTE.USA_CONTRATO, CLIENTE.ID_CLIENTE_MATRIZ, CLIENTE.ID_CLIENTE_CREDENCIADO, ");
                query.Append(" CLIENTE.PARTICULAR, CLIENTE.UNIDADE,CLIENTE.CREDENCIADA, CLIENTE.PRESTADOR, CLIENTE.ID_CIDADE_IBGE, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, ");
                query.Append(" CLIENTE.ID_CIDADE_IBGE_COBRANCA, CIDADE_COBRANCA.CODIGO, CIDADE_COBRANCA.NOME, CIDADE_COBRANCA.UF_CIDADE, ");
                query.Append(" FUNCAO.ID_FUNCAO, FUNCAO.DESCRICAO, FUNCAO.COD_CBO, FUNCAO.SITUACAO, FUNCAO.COMENTARIO, ");
                query.Append(" CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO, CLIENTE_FUNCAO.SITUACAO, CLIENTE_FUNCAO.DATA_CADASTRO, CLIENTE_FUNCAO.DATA_DESLIGAMENTO, ");
                query.Append(" CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO, CLIENTE_FUNCAO_FUNCIONARIO.SITUACAO, CLIENTE_FUNCAO_FUNCIONARIO.DATA_CADASTRO, CLIENTE_FUNCAO_FUNCIONARIO.DATA_DESLIGAMENTO, CLIENTE_FUNCAO_FUNCIONARIO.MATRICULA, CLIENTE_FUNCAO_FUNCIONARIO.VIP, ");
                query.Append(" CLIENTE.DESTACA_ISS, CLIENTE.GERA_COBRANCA_VALOR_LIQUIDO, CLIENTE.ALIQUOTA_ISS, CLIENTE.CODIGO_CNES, CLIENTE.USA_CENTRO_CUSTO, CLIENTE.BLOQUEADO, CLIENTE.CREDENCIADORA, CLIENTE.FISICA, CLIENTE.USA_PO, CLIENTE_FUNCAO_FUNCIONARIO.BR_PDH, CLIENTE_FUNCAO_FUNCIONARIO.REGIME_REVEZAMENTO, CLIENTE_FUNCAO_FUNCIONARIO.ESTAGIARIO, CLIENTE_FUNCAO_FUNCIONARIO.VINCULO, CLIENTE.SIMPLES_NACIONAL ");
                query.Append(" FROM SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTE_FUNCAO_FUNCIONARIO ");
                query.Append(" INNER JOIN SEG_FUNCIONARIO FUNCIONARIO ON FUNCIONARIO.ID_FUNCIONARIO = CLIENTE_FUNCAO_FUNCIONARIO.ID_FUNCIONARIO ");
                query.Append(" INNER JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTE_FUNCAO_FUNCIONARIO.ID_SEG_CLIENTE_FUNCAO ");
                query.Append(" INNER JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTE_FUNCAO.ID_FUNCAO ");
                query.Append(" INNER JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = CLIENTE_FUNCAO.ID_CLIENTE ");
                query.Append(" JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = CLIENTE.ID_CIDADE_IBGE ");
                query.Append(" JOIN SEG_CIDADE_IBGE CIDADE_COBRANCA ON CIDADE_COBRANCA.ID_CIDADE_IBGE = CLIENTE.ID_CIDADE_IBGE_COBRANCA ");
                query.Append(" WHERE FUNCIONARIO.ID_FUNCIONARIO = :VALUE1 AND ");
                query.Append(" CLIENTE_FUNCAO.SITUACAO = 'A' ");
                query.Append(" AND CLIENTE_FUNCAO_FUNCIONARIO.SITUACAO = :VALUE2 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer)); // id_funcionario
                command.Parameters[0].Value = funcionario.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar)); // Situacao ATivo
                command.Parameters[1].Value = str;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    clienteFuncaoFuncionario = new ClienteFuncaoFuncionario(
                        dr.GetInt64(59), 
                        new ClienteFuncao(
                            dr.GetInt64(55), 
                            new Cliente(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.GetString(4), dr.GetString(5), dr.GetString(6), dr.GetString(7), dr.GetString(8), dr.GetString(9), dr.GetString(10), dr.GetString(11), dr.GetString(12), dr.GetString(13), dr.GetString(14), dr.GetDateTime(15), dr.GetString(16), dr.GetString(17), dr.GetString(18), dr.GetString(19), dr.GetString(20), dr.GetString(21), dr.GetInt32(22), dr.GetInt32(23), dr.GetString(24), dr.GetString(25), dr.GetString(26), dr.GetString(27), dr.GetString(28), dr.GetString(29), dr.GetString(30), dr.IsDBNull(31) ? null : new Medico(dr.GetInt64(31)),  dr.GetString(32), dr.GetBoolean(33), dr.GetString(34), dr.GetBoolean(35), dr.IsDBNull(36) ? null : new Cliente(dr.GetInt64(36)), dr.IsDBNull(37) ? null : new Cliente(dr.GetInt64(37)), dr.GetBoolean(38), dr.GetBoolean(39), dr.GetBoolean(40), dr.GetBoolean(41), dr.IsDBNull(42) ? null : new CidadeIbge(dr.GetInt64(42), dr.GetString(43), dr.GetString(44), dr.GetString(45)), dr.IsDBNull(46) ? null : new CidadeIbge(dr.GetInt64(46), dr.GetString(47), dr.GetString(48), dr.GetString(49)), dr.GetBoolean(65), dr.GetBoolean(66), dr.GetDecimal(67), dr.IsDBNull(68) ? string.Empty : dr.GetString(68), dr.GetBoolean(69), dr.GetBoolean(70), dr.GetBoolean(71), dr.GetBoolean(72), dr.GetBoolean(73), dr.GetBoolean(78)), 
                            new Funcao(dr.GetInt64(50), dr.GetString(51), dr.GetString(52), dr.GetString(53), dr.GetString(54)), 
                            dr.GetString(56), 
                            dr.GetDateTime(57), 
                            dr.IsDBNull(58) ? (DateTime?)null : dr.GetDateTime(58)), 
                        funcionario, 
                        dr.GetString(60), 
                        dr.GetDateTime(61), 
                        dr.IsDBNull(62) ? (DateTime?)null : dr.GetDateTime(62), 
                        dr.IsDBNull(63) ? String.Empty : dr.GetString(63), 
                        dr.GetBoolean(64), 
                        dr.IsDBNull(74) ? string.Empty : dr.GetString(74), 
                        dr.IsDBNull(75) ? string.Empty : dr.GetString(75), 
                        dr.GetBoolean(76), 
                        dr.GetBoolean(77)); 
                }

                return clienteFuncaoFuncionario;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByFuncionarioAndSituacao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<ClienteFuncaoFuncionario> findByFuncionarioAndSituacaoList(Funcionario funcionario, bool ativos, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByFuncionarioAndSituacaoList");

            ClienteFuncaoFuncionario clienteFuncaoFuncionario = null;
            List<ClienteFuncaoFuncionario> clienteFuncaoFuncionarioRetorno = new List<ClienteFuncaoFuncionario>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT CLIENTE.ID_CLIENTE, CLIENTE.RAZAO_SOCIAL, CLIENTE.FANTASIA, CLIENTE.CNPJ, CLIENTE.INSCRICAO, CLIENTE.ENDERECO, CLIENTE.NUMERO, CLIENTE.COMPLEMENTO, ");
                query.Append(" CLIENTE.BAIRRO, CLIENTE.CEP, CLIENTE.UF, CLIENTE.TELEFONE1, CLIENTE.TELEFONE2, CLIENTE.EMAIL, CLIENTE.SITE, CLIENTE.DATA_CADASTRO, CLIENTE.RESPONSAVEL,  ");
                query.Append(" CLIENTE.CARGO, CLIENTE.DOCUMENTO, CLIENTE.ESCOPO, CLIENTE.JORNADA, CLIENTE.SITUACAO, CLIENTE.EST_MASC, CLIENTE.EST_FEM, CLIENTE.ENDERECOCOB, ");
                query.Append(" CLIENTE.NUMEROCOB, CLIENTE.COMPLEMENTOCOB, CLIENTE.BAIRROCOB, CLIENTE.CEPCOB, CLIENTE.UFCOB, CLIENTE.TELEFONECOB, CLIENTE.ID_MEDICO, CLIENTE.TELEFONE2COB, ");
                query.Append(" CLIENTE.VIP, CLIENTE.TELEFONE_CONTATO, CLIENTE.USA_CONTRATO, CLIENTE.ID_CLIENTE_MATRIZ, CLIENTE.ID_CLIENTE_CREDENCIADO, CLIENTE.PARTICULAR, ");
                query.Append(" CLIENTE.UNIDADE, CLIENTE.CREDENCIADA, CLIENTE.PRESTADOR, CLIENTE.ID_CIDADE_IBGE, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, ");
                query.Append(" CLIENTE.ID_CIDADE_IBGE_COBRANCA, CIDADE_COBRANCA.CODIGO, CIDADE_COBRANCA.NOME, CIDADE_COBRANCA.UF_CIDADE, ");
                query.Append(" FUNCAO.ID_FUNCAO, FUNCAO.DESCRICAO, FUNCAO.COD_CBO, FUNCAO.SITUACAO, FUNCAO.COMENTARIO, ");
                query.Append(" CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO, CLIENTE_FUNCAO.SITUACAO, CLIENTE_FUNCAO.DATA_CADASTRO, CLIENTE_FUNCAO.DATA_DESLIGAMENTO, ");
                query.Append(" CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO,  CLIENTE_FUNCAO_FUNCIONARIO.SITUACAO, CLIENTE_FUNCAO_FUNCIONARIO.DATA_CADASTRO, CLIENTE_FUNCAO_FUNCIONARIO.DATA_DESLIGAMENTO, CLIENTE_FUNCAO_FUNCIONARIO.MATRICULA, CLIENTE_FUNCAO_FUNCIONARIO.VIP, ");
                query.Append(" CLIENTE.DESTACA_ISS, CLIENTE.GERA_COBRANCA_VALOR_LIQUIDO, CLIENTE.ALIQUOTA_ISS, CLIENTE.CODIGO_CNES, CLIENTE.USA_CENTRO_CUSTO, CLIENTE.BLOQUEADO, CLIENTE.CREDENCIADORA, CLIENTE.FISICA, CLIENTE.USA_PO, CLIENTE_FUNCAO_FUNCIONARIO.BR_PDH, CLIENTE_FUNCAO_FUNCIONARIO.REGIME_REVEZAMENTO, CLIENTE_FUNCAO_FUNCIONARIO.ESTAGIARIO, CLIENTE_FUNCAO_FUNCIONARIO.VINCULO, CLIENTE.SIMPLES_NACIONAL ");
                query.Append(" FROM SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTE_FUNCAO_FUNCIONARIO  ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTE_FUNCAO_FUNCIONARIO.ID_SEG_CLIENTE_FUNCAO ");
                query.Append(" JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = CLIENTE_FUNCAO.ID_CLIENTE  ");
                query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTE_FUNCAO.ID_FUNCAO  ");
                query.Append(" JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = CLIENTE.ID_CIDADE_IBGE ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE_COBRANCA ON CIDADE_COBRANCA.ID_CIDADE_IBGE = CLIENTE.ID_CIDADE_IBGE_COBRANCA ");
                query.Append(" WHERE TRUE "); 
                query.Append(" AND CLIENTE_FUNCAO_FUNCIONARIO.ID_FUNCIONARIO = :VALUE1 ");
                if (ativos)
                    query.Append(" AND CLIENTE_FUNCAO_FUNCIONARIO.SITUACAO = 'A' ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = funcionario.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {

                    clienteFuncaoFuncionario = new ClienteFuncaoFuncionario(
                        dr.GetInt64(59), 
                        new ClienteFuncao(dr.GetInt64(55), 
                            new Cliente(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.GetString(4), dr.GetString(5), dr.GetString(6), dr.GetString(7), dr.GetString(8), dr.GetString(9), dr.GetString(10), dr.GetString(11), dr.GetString(12), dr.GetString(13), dr.GetString(14), dr.GetDateTime(15), dr.GetString(16), dr.GetString(17), dr.GetString(18), dr.GetString(19), dr.GetString(20), dr.GetString(21), dr.GetInt32(22), dr.GetInt32(23), dr.GetString(24), dr.GetString(25), dr.GetString(26), dr.GetString(27), dr.GetString(28), dr.GetString(29), dr.GetString(30), dr.IsDBNull(31) ? null : new Medico(dr.GetInt64(31)),  dr.GetString(32), dr.GetBoolean(33), dr.GetString(34), dr.GetBoolean(35), dr.IsDBNull(36) ? null : new Cliente(dr.GetInt64(36)),  dr.IsDBNull(37) ? null : new Cliente(dr.GetInt64(37)), dr.GetBoolean(38), dr.GetBoolean(39), dr.GetBoolean(40), dr.GetBoolean(41), dr.IsDBNull(42) ? null : new CidadeIbge(dr.GetInt64(42), dr.GetString(43), dr.GetString(44), dr.GetString(45)), dr.IsDBNull(46) ? null : new CidadeIbge(dr.GetInt64(46), dr.GetString(47), dr.GetString(48), dr.GetString(49)), dr.GetBoolean(65), dr.GetBoolean(66), dr.GetDecimal(67), dr.IsDBNull(68) ? string.Empty : dr.GetString(68), dr.GetBoolean(69), dr.GetBoolean(70), dr.GetBoolean(71), dr.GetBoolean(72), dr.GetBoolean(73), dr.GetBoolean(78)), 
                            new Funcao(dr.GetInt64(50), dr.GetString(51), dr.GetString(52), dr.GetString(53), dr.GetString(54)), 
                            dr.GetString(56), 
                            dr.GetDateTime(57), 
                            dr.IsDBNull(58) ? (DateTime?)null : 
                            dr.GetDateTime(58)), 
                        funcionario, 
                        dr.GetString(60), 
                        dr.GetDateTime(61), 
                        dr.IsDBNull(62) ? (DateTime?)null : dr.GetDateTime(62), 
                        dr.IsDBNull(63) ? String.Empty : dr.GetString(63), 
                        dr.GetBoolean(64), 
                        dr.IsDBNull(74) ? string.Empty : dr.GetString(74), 
                        dr.IsDBNull(75) ? string.Empty : dr.GetString(75), 
                        dr.GetBoolean(76), 
                        dr.GetBoolean(77)); 

                    clienteFuncaoFuncionarioRetorno.Add(clienteFuncaoFuncionario);
                }

                return clienteFuncaoFuncionarioRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByFuncionarioAndSituacaoList", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void delete(ClienteFuncaoFuncionario clienteFuncaoFuncionario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" DELETE FROM SEG_CLIENTE_FUNCAO_FUNCIONARIO WHERE ID_CLIENTE_FUNCAO_FUNCIONARIO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncaoFuncionario.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public void update(ClienteFuncaoFuncionario clienteFuncaoFuncionario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_CLIENTE_FUNCAO_FUNCIONARIO SET ");
                query.Append(" ID_SEG_CLIENTE_FUNCAO = :VALUE2, ID_FUNCIONARIO = :VALUE3, SITUACAO = :VALUE4, DATA_CADASTRO = :VALUE5, DATA_DESLIGAMENTO = :VALUE6, MATRICULA = :VALUE7, VIP = :VALUE8, BR_PDH = :VALUE9, REGIME_REVEZAMENTO = :VALUE10, ESTAGIARIO = :VALUE11, VINCULO = :VALUE12 ");
                query.Append(" WHERE ID_CLIENTE_FUNCAO_FUNCIONARIO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncaoFuncionario.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = clienteFuncaoFuncionario.ClienteFuncao.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = clienteFuncaoFuncionario.Funcionario.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = clienteFuncaoFuncionario.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Date));
                command.Parameters[4].Value = clienteFuncaoFuncionario.DataCadastro;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Date));
                command.Parameters[5].Value = clienteFuncaoFuncionario.DataDesligamento;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = clienteFuncaoFuncionario.Matricula;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Boolean));
                command.Parameters[7].Value = clienteFuncaoFuncionario.Vip;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                command.Parameters[8].Value = clienteFuncaoFuncionario.BrPdh;

                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                command.Parameters[9].Value = clienteFuncaoFuncionario.RegimeRevezamento;

                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Boolean));
                command.Parameters[10].Value = clienteFuncaoFuncionario.Estagiario;

                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Boolean));
                command.Parameters[11].Value = clienteFuncaoFuncionario.Vinculo;
                

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public List<ClienteFuncaoFuncionario> findAllFuncionarioByClienteAndFuncionario(Cliente cliente, Funcionario funcionario, bool ativos, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllFuncionarioByClienteAndFuncionario");

            ClienteFuncaoFuncionario clienteFuncaoFuncionario = null;
            List<ClienteFuncaoFuncionario> clienteFuncaoFuncionarioRetorno = new List<ClienteFuncaoFuncionario>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT CLIENTE.ID_CLIENTE, CLIENTE.RAZAO_SOCIAL, CLIENTE.FANTASIA, CLIENTE.CNPJ, CLIENTE.INSCRICAO, CLIENTE.ENDERECO, CLIENTE.NUMERO, CLIENTE.COMPLEMENTO, ");
                query.Append(" CLIENTE.BAIRRO, CLIENTE.CEP, CLIENTE.UF, CLIENTE.TELEFONE1, CLIENTE.TELEFONE2, CLIENTE.EMAIL, CLIENTE.SITE, CLIENTE.DATA_CADASTRO, CLIENTE.RESPONSAVEL,  ");
                query.Append(" CLIENTE.CARGO, CLIENTE.DOCUMENTO, CLIENTE.ESCOPO, CLIENTE.JORNADA, CLIENTE.SITUACAO, CLIENTE.EST_MASC, CLIENTE.EST_FEM, CLIENTE.ENDERECOCOB, ");
                query.Append(" CLIENTE.NUMEROCOB, CLIENTE.COMPLEMENTOCOB, CLIENTE.BAIRROCOB, CLIENTE.CEPCOB, CLIENTE.UFCOB, CLIENTE.TELEFONECOB, CLIENTE.ID_MEDICO, CLIENTE.TELEFONE2COB, ");
                query.Append(" CLIENTE.VIP, CLIENTE.TELEFONE_CONTATO, CLIENTE.USA_CONTRATO, CLIENTE.ID_CLIENTE_MATRIZ, CLIENTE.ID_CLIENTE_CREDENCIADO, CLIENTE.PARTICULAR, ");
                query.Append(" CLIENTE.UNIDADE, CLIENTE.CREDENCIADA, CLIENTE.PRESTADOR, CLIENTE.ID_CIDADE_IBGE, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, ");
                query.Append(" CLIENTE.ID_CIDADE_IBGE_COBRANCA, CIDADE_COBRANCA.CODIGO, CIDADE_COBRANCA.NOME, CIDADE_COBRANCA.UF_CIDADE, ");
                query.Append(" FUNCAO.ID_FUNCAO, FUNCAO.DESCRICAO, FUNCAO.COD_CBO, FUNCAO.SITUACAO, FUNCAO.COMENTARIO, ");
                query.Append(" CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO, CLIENTE_FUNCAO.SITUACAO, CLIENTE_FUNCAO.DATA_CADASTRO, CLIENTE_FUNCAO.DATA_DESLIGAMENTO, ");
                query.Append(" CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO,  CLIENTE_FUNCAO_FUNCIONARIO.SITUACAO, CLIENTE_FUNCAO_FUNCIONARIO.DATA_CADASTRO, CLIENTE_FUNCAO_FUNCIONARIO.DATA_DESLIGAMENTO, CLIENTE_FUNCAO_FUNCIONARIO.MATRICULA, CLIENTE_FUNCAO_FUNCIONARIO.VIP, ");
                query.Append(" CLIENTE.DESTACA_ISS, CLIENTE.GERA_COBRANCA_VALOR_LIQUIDO, CLIENTE.ALIQUOTA_ISS, CLIENTE.CODIGO_CNES, CLIENTE.USA_CENTRO_CUSTO, CLIENTE.BLOQUEADO, CLIENTE.CREDENCIADORA, CLIENTE.FISICA, CLIENTE.USA_PO, CLIENTE_FUNCAO_FUNCIONARIO.BR_PDH, CLIENTE_FUNCAO_FUNCIONARIO.REGIME_REVEZAMENTO, CLIENTE_FUNCAO_FUNCIONARIO.ESTAGIARIO, CLIENTE_FUNCAO_FUNCIONARIO.VINCULO, FUNCIONARIO.ID_FUNCIONARIO, CLIENTE.SIMPLES_NACIONAL ");
                query.Append(" FROM SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTE_FUNCAO_FUNCIONARIO  ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTE_FUNCAO_FUNCIONARIO.ID_SEG_CLIENTE_FUNCAO ");
                query.Append(" JOIN SEG_FUNCIONARIO FUNCIONARIO ON FUNCIONARIO.ID_FUNCIONARIO = CLIENTE_FUNCAO_FUNCIONARIO.ID_FUNCIONARIO ");
                query.Append(" JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = CLIENTE_FUNCAO.ID_CLIENTE  ");
                query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTE_FUNCAO.ID_FUNCAO  ");
                query.Append(" JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = CLIENTE.ID_CIDADE_IBGE ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE_COBRANCA ON CIDADE_COBRANCA.ID_CIDADE_IBGE = CLIENTE.ID_CIDADE_IBGE_COBRANCA ");
                query.Append(" WHERE TRUE ");
                query.Append(" AND CLIENTE.ID_CLIENTE = :VALUE1 ");

                if (ativos == true)
                    query.Append(" AND CLIENTE_FUNCAO_FUNCIONARIO.SITUACAO = 'A' ");

                if (!string.IsNullOrEmpty(funcionario.Nome))
                    query.Append(" AND FUNCIONARIO.NOME LIKE :VALUE2 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = funcionario.Nome + "%";

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {

                    clienteFuncaoFuncionario = new ClienteFuncaoFuncionario(
                        dr.GetInt64(59), 
                        new ClienteFuncao(
                            dr.GetInt64(55), 
                            new Cliente(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.GetString(4), dr.GetString(5), dr.GetString(6), dr.GetString(7), dr.GetString(8), dr.GetString(9), dr.GetString(10), dr.GetString(11), dr.GetString(12), dr.GetString(13), dr.GetString(14), dr.GetDateTime(15), dr.GetString(16), dr.GetString(17), dr.GetString(18), dr.GetString(19), dr.GetString(20), dr.GetString(21), dr.GetInt32(22), dr.GetInt32(23), dr.GetString(24), dr.GetString(25), dr.GetString(26), dr.GetString(27), dr.GetString(28), dr.GetString(29), dr.GetString(30), dr.IsDBNull(31) ? null : new Medico(dr.GetInt64(31)), dr.GetString(32), dr.GetBoolean(33), dr.GetString(34), dr.GetBoolean(35), dr.IsDBNull(36) ? null : new Cliente(dr.GetInt64(36)), dr.IsDBNull(37) ? null : new Cliente(dr.GetInt64(37)), dr.GetBoolean(38), dr.GetBoolean(39), dr.GetBoolean(40), dr.GetBoolean(41), dr.IsDBNull(42) ? null : new CidadeIbge(dr.GetInt64(42), dr.GetString(43), dr.GetString(44), dr.GetString(45)), dr.IsDBNull(46) ? null : new CidadeIbge(dr.GetInt64(46), dr.GetString(47), dr.GetString(48), dr.GetString(49)), dr.GetBoolean(65), dr.GetBoolean(66), dr.GetDecimal(67), dr.IsDBNull(68) ? string.Empty : dr.GetString(68), dr.GetBoolean(69), dr.GetBoolean(70), dr.GetBoolean(71), dr.GetBoolean(72), dr.GetBoolean(73), dr.GetBoolean(79)), 
                            new Funcao(dr.GetInt64(50), dr.GetString(51), dr.GetString(52), dr.GetString(53), dr.GetString(54)), 
                            dr.GetString(56), 
                            dr.GetDateTime(57), 
                            dr.IsDBNull(58) ? (DateTime?)null : dr.GetDateTime(58)), 
                        new Funcionario(dr.GetInt64(78)), 
                        dr.GetString(60), 
                        dr.GetDateTime(61), 
                        dr.IsDBNull(62) ? (DateTime?)null : dr.GetDateTime(62), 
                        dr.IsDBNull(63) ? String.Empty : dr.GetString(63), 
                        dr.GetBoolean(64), 
                        dr.IsDBNull(74) ? string.Empty : dr.GetString(74), 
                        dr.IsDBNull(75) ? string.Empty : dr.GetString(75), 
                        dr.GetBoolean(76), 
                        dr.GetBoolean(77));

                    clienteFuncaoFuncionarioRetorno.Add(clienteFuncaoFuncionario);
                }

                return clienteFuncaoFuncionarioRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllFuncionarioByClienteAndFuncionario", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<ClienteFuncaoFuncionario> findAtivosByCliente(Cliente cliente, DateTime periodo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtivosByCliente");

            List<ClienteFuncaoFuncionario> clienteFuncaoFuncionarioRetorno = new List<ClienteFuncaoFuncionario>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT DISTINCT CLIENTE.ID_CLIENTE, CLIENTE.RAZAO_SOCIAL, CLIENTE.FANTASIA, CLIENTE.CNPJ, CLIENTE.INSCRICAO, CLIENTE.ENDERECO, CLIENTE.NUMERO, CLIENTE.COMPLEMENTO, CLIENTE.BAIRRO, CLIENTE.CEP, CLIENTE.UF, CLIENTE.TELEFONE1, CLIENTE.TELEFONE2, CLIENTE.EMAIL, CLIENTE.SITE, CLIENTE.DATA_CADASTRO, CLIENTE.RESPONSAVEL,  CLIENTE.CARGO, CLIENTE.DOCUMENTO, CLIENTE.ESCOPO, CLIENTE.JORNADA, CLIENTE.SITUACAO, CLIENTE.EST_MASC, CLIENTE.EST_FEM, CLIENTE.ENDERECOCOB, CLIENTE.NUMEROCOB, CLIENTE.COMPLEMENTOCOB, CLIENTE.BAIRROCOB, CLIENTE.CEPCOB, CLIENTE.UFCOB, CLIENTE.TELEFONECOB, CLIENTE.ID_MEDICO, CLIENTE.TELEFONE2COB, CLIENTE.VIP, CLIENTE.TELEFONE_CONTATO, CLIENTE.USA_CONTRATO, CLIENTE.ID_CLIENTE_MATRIZ, CLIENTE.ID_CLIENTE_CREDENCIADO, CLIENTE.PARTICULAR, CLIENTE.UNIDADE, CLIENTE.CREDENCIADA, CLIENTE.PRESTADOR, CLIENTE.ID_CIDADE_IBGE, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, CLIENTE.ID_CIDADE_IBGE_COBRANCA, CIDADE_COBRANCA.CODIGO, CIDADE_COBRANCA.NOME, CIDADE_COBRANCA.UF_CIDADE, FUNCAO.ID_FUNCAO, FUNCAO.DESCRICAO, FUNCAO.COD_CBO, FUNCAO.SITUACAO, FUNCAO.COMENTARIO, CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO, CLIENTE_FUNCAO.SITUACAO, CLIENTE_FUNCAO.DATA_CADASTRO, CLIENTE_FUNCAO.DATA_DESLIGAMENTO, CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO,  CLIENTE_FUNCAO_FUNCIONARIO.SITUACAO, CLIENTE_FUNCAO_FUNCIONARIO.DATA_CADASTRO, CLIENTE_FUNCAO_FUNCIONARIO.DATA_DESLIGAMENTO, CLIENTE_FUNCAO_FUNCIONARIO.MATRICULA, CLIENTE_FUNCAO_FUNCIONARIO.VIP, CLIENTE.DESTACA_ISS, CLIENTE.GERA_COBRANCA_VALOR_LIQUIDO, CLIENTE.ALIQUOTA_ISS, CLIENTE.CODIGO_CNES, CLIENTE.USA_CENTRO_CUSTO, CLIENTE.BLOQUEADO, CLIENTE.CREDENCIADORA, CLIENTE.FISICA, CLIENTE.USA_PO, CLIENTE_FUNCAO_FUNCIONARIO.BR_PDH, CLIENTE_FUNCAO_FUNCIONARIO.REGIME_REVEZAMENTO, CLIENTE_FUNCAO_FUNCIONARIO.ESTAGIARIO, CLIENTE_FUNCAO_FUNCIONARIO.VINCULO, CLIENTE_FUNCAO_FUNCIONARIO.ID_FUNCIONARIO, CLIENTE.SIMPLES_NACIONAL ");
                query.Append(" FROM SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTE_FUNCAO_FUNCIONARIO ");
                query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTE_FUNCAO_FUNCIONARIO.ID_SEG_CLIENTE_FUNCAO ");
                query.Append(" LEFT JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = CLIENTE_FUNCAO.ID_CLIENTE  ");
                query.Append(" LEFT JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTE_FUNCAO.ID_FUNCAO  ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = CLIENTE.ID_CIDADE_IBGE  ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE_COBRANCA ON CIDADE_COBRANCA.ID_CIDADE_IBGE = CLIENTE.ID_CIDADE_IBGE_COBRANCA  ");
                query.Append(" LEFT JOIN SEG_MONITORAMENTO MONITORAMENTO ON MONITORAMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO = CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO  ");
                query.Append(" LEFT JOIN SEG_ESOCIAL_MONITORAMENTO ESOCIAL_MONITORAMENTO ON ESOCIAL_MONITORAMENTO.ID_MONITORAMENTO = MONITORAMENTO.ID_MONITORAMENTO  ");
                query.Append(" LEFT JOIN SEG_ESOCIAL_LOTE ESOCIAL ON ESOCIAL.ID_ESOCIAL_LOTE = ESOCIAL_MONITORAMENTO.ID_ESOCIAL_LOTE  ");
                query.Append(" WHERE TRUE  ");
                query.Append(" AND CLIENTE_FUNCAO_FUNCIONARIO.VINCULO = TRUE  ");
                query.Append(" AND CLIENTE_FUNCAO_FUNCIONARIO.DATA_CADASTRO <= :VALUE2  ");
                query.Append(" AND CLIENTE_FUNCAO_FUNCIONARIO.DATA_DESLIGAMENTO IS NULL  ");
                query.Append(" AND CLIENTE.ID_CLIENTE = :VALUE1  ");
                query.Append(" AND CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO NOT IN (  ");
                query.Append(" SELECT SCFF.ID_CLIENTE_FUNCAO_FUNCIONARIO FROM SEG_CLIENTE_FUNCAO_FUNCIONARIO SCFF  ");
                query.Append(" JOIN SEG_MONITORAMENTO SM ON SM.ID_CLIENTE_FUNCAO_FUNCIONARIO = SCFF.ID_CLIENTE_FUNCAO_FUNCIONARIO  ");
                query.Append(" WHERE SM.PERIODO = :VALUE2) ");
           
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Date));
                command.Parameters[1].Value = periodo;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {

                    clienteFuncaoFuncionarioRetorno.Add(new ClienteFuncaoFuncionario(
                        dr.GetInt64(59), 
                        new ClienteFuncao(
                            dr.GetInt64(55), 
                            new Cliente(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.GetString(4), dr.GetString(5), dr.GetString(6), dr.GetString(7), dr.GetString(8), dr.GetString(9), dr.GetString(10), dr.GetString(11), dr.GetString(12), dr.GetString(13), dr.GetString(14), dr.GetDateTime(15), dr.GetString(16), dr.GetString(17), dr.GetString(18), dr.GetString(19), dr.GetString(20), dr.GetString(21), dr.GetInt32(22), dr.GetInt32(23), dr.GetString(24), dr.GetString(25), dr.GetString(26), dr.GetString(27), dr.GetString(28), dr.GetString(29), dr.GetString(30), dr.IsDBNull(31) ? null : new Medico(dr.GetInt64(31)), dr.GetString(32), dr.GetBoolean(33), dr.GetString(34), dr.GetBoolean(35), dr.IsDBNull(36) ? null : new Cliente(dr.GetInt64(36)), dr.IsDBNull(37) ? null : new Cliente(dr.GetInt64(37)), dr.GetBoolean(38), dr.GetBoolean(39), dr.GetBoolean(40), dr.GetBoolean(41), dr.IsDBNull(42) ? null : new CidadeIbge(dr.GetInt64(42), dr.GetString(43), dr.GetString(44), dr.GetString(45)), dr.IsDBNull(46) ? null : new CidadeIbge(dr.GetInt64(46), dr.GetString(47), dr.GetString(48), dr.GetString(49)), dr.GetBoolean(65), dr.GetBoolean(66), dr.GetDecimal(67), dr.IsDBNull(68) ? string.Empty : dr.GetString(68), dr.GetBoolean(69), dr.GetBoolean(70), dr.GetBoolean(71), dr.GetBoolean(72), dr.GetBoolean(73), dr.GetBoolean(79)), 
                            new Funcao(dr.GetInt64(50), dr.GetString(51), dr.GetString(52), dr.GetString(53), dr.GetString(54)), 
                            dr.GetString(56), 
                            dr.GetDateTime(57), 
                            dr.IsDBNull(58) ? (DateTime?)null : dr.GetDateTime(58)), 
                        new Funcionario(dr.GetInt64(78)), 
                        dr.GetString(60), 
                        dr.GetDateTime(61), 
                        dr.IsDBNull(62) ? (DateTime?)null : dr.GetDateTime(62), 
                        dr.IsDBNull(63) ? String.Empty : dr.GetString(63), 
                        dr.GetBoolean(64), 
                        dr.IsDBNull(74) ? string.Empty : dr.GetString(74), 
                        dr.IsDBNull(75) ? string.Empty : dr.GetString(75), 
                        dr.GetBoolean(76), 
                        dr.GetBoolean(77)));
                }

                return clienteFuncaoFuncionarioRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllFuncionarioByClienteAndFuncionario", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }


        public List<ClienteFuncaoFuncionario> findAllByClienteFuncao(ClienteFuncao clienteFuncao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtivosByCliente");

            List<ClienteFuncaoFuncionario> listClienteFuncaoFuncionario = new List<ClienteFuncaoFuncionario>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO, CLIENTE_FUNCAO_FUNCIONARIO.ID_SEG_CLIENTE_FUNCAO, CLIENTE_FUNCAO_FUNCIONARIO.ID_FUNCIONARIO, CLIENTE_FUNCAO_FUNCIONARIO.SITUACAO, CLIENTE_FUNCAO_FUNCIONARIO.DATA_CADASTRO, CLIENTE_FUNCAO_FUNCIONARIO.DATA_DESLIGAMENTO, CLIENTE_FUNCAO_FUNCIONARIO.MATRICULA, CLIENTE_FUNCAO_FUNCIONARIO.VIP, CLIENTE_FUNCAO_FUNCIONARIO.BR_PDH, CLIENTE_FUNCAO_FUNCIONARIO.REGIME_REVEZAMENTO, CLIENTE_FUNCAO_FUNCIONARIO.ESTAGIARIO, CLIENTE_FUNCAO_FUNCIONARIO.VINCULO ");
                query.Append(" FROM SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTE_FUNCAO_FUNCIONARIO ");
                query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTE_FUNCAO_FUNCIONARIO.ID_SEG_CLIENTE_FUNCAO ");
                query.Append(" WHERE CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = :VALUE1 ");
                query.Append(" AND CLIENTE_FUNCAO_FUNCIONARIO.SITUACAO = 'A' ");
                query.Append(" ORDER BY ID_CLIENTE_FUNCAO_FUNCIONARIO ASC ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncao.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    ClienteFuncaoFuncionario clienteFuncaoFuncionario = new ClienteFuncaoFuncionario();
                    clienteFuncaoFuncionario.Id = dr.GetInt64(0);
                    clienteFuncaoFuncionario.ClienteFuncao = clienteFuncao;
                    clienteFuncaoFuncionario.Funcionario = new Funcionario(dr.GetInt64(2));
                    clienteFuncaoFuncionario.Situacao = dr.GetString(3);
                    clienteFuncaoFuncionario.DataCadastro = dr.GetDateTime(4);
                    clienteFuncaoFuncionario.DataDesligamento = dr.IsDBNull(5) ? (DateTime?)null : dr.GetDateTime(5);
                    clienteFuncaoFuncionario.Matricula = dr.IsDBNull(6) ? string.Empty : dr.GetString(6);
                    clienteFuncaoFuncionario.Vip = dr.GetBoolean(7);
                    clienteFuncaoFuncionario.BrPdh = dr.IsDBNull(8) ? string.Empty : dr.GetString(8);
                    clienteFuncaoFuncionario.RegimeRevezamento = dr.IsDBNull(9) ? string.Empty : dr.GetString(9);
                    clienteFuncaoFuncionario.Estagiario = dr.GetBoolean(10);
                    clienteFuncaoFuncionario.Vinculo = dr.GetBoolean(11);

                    listClienteFuncaoFuncionario.Add(clienteFuncaoFuncionario);
                    
                }

                return listClienteFuncaoFuncionario;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByClienteFuncao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }


    }
}
