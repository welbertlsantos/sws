﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using System.Data;
using Npgsql;
using SWS.Excecao;
using NpgsqlTypes;
using SWS.Entidade;
using SWS.Helper;

namespace SWS.Dao
{
    class PerfilDao : IPerfilDao
    {
        public DataSet findAll(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método listaTodos");

            try
            {
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter("SELECT ID_PERFIL, DESCRICAO, SITUACAO FROM SEG_PERFIL", dbConnection);

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Perfis");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - listaTodos", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public HashSet<Perfil> findByUsuario(long? idUsuario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByUsuario");

            HashSet<Perfil> perfis = new HashSet<Perfil>();

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT PERFIL.ID_PERFIL, PERFIL.DESCRICAO, PERFIL.SITUACAO ");
                query.Append(" FROM SEG_USUARIO_PERFIL USUARIO_PERFIL ");
                query.Append(" JOIN SEG_PERFIL PERFIL ON PERFIL.ID_PERFIL = USUARIO_PERFIL.ID_PERFIL ");
                query.Append(" WHERE USUARIO_PERFIL.ID_USUARIO =:VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = idUsuario;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    perfis.Add(new Perfil(dr.GetInt64(0), dr.GetString(1), dr.GetString(2)));
                }

                return perfis;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByUsuario", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findByFilter(Perfil perfil, NpgsqlConnection dbConnection) 
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findPerfilByFilter");

            try
            {
                StringBuilder query = new StringBuilder();
                

                if (perfil.isNullForFilter())
                    query.Append(" SELECT ID_PERFIL, DESCRICAO, SITUACAO FROM SEG_PERFIL ");
                else
                {
                    query.Append(" SELECT ID_PERFIL, DESCRICAO, SITUACAO FROM SEG_PERFIL WHERE TRUE ");

                    if (!String.IsNullOrWhiteSpace(perfil.Descricao))
                        query.Append(" AND DESCRICAO LIKE :VALUE1 ");

                    if (!String.IsNullOrWhiteSpace(perfil.Situacao))
                        query.Append(" AND SITUACAO = :VALUE2 ");
                    
                    
                }
                
                query.Append("ORDER BY DESCRICAO ASC");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[0].Value = perfil.Descricao + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("value2", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[1].Value = perfil.Situacao;
                
                DataSet ds = new DataSet();

                adapter.Fill(ds, "Perfis");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Perfil findById(Int64 idPerfil, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findPerfilById");

            Perfil perfilRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {

                query.Append(" SELECT ID_PERFIL, DESCRICAO, SITUACAO FROM SEG_PERFIL WHERE ID_PERFIL = :VALUE1");

                NpgsqlCommand command = new NpgsqlCommand( query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = idPerfil;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    perfilRetorno = new Perfil(dr.GetInt64(0), dr.GetString(1), dr.GetString(2));
                }

                return perfilRetorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public void changeStatusPerfil(Int64 idPerfil, string novaSituacao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método changeStatusPerfil");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("update seg_perfil set situacao = :value2 where id_perfil = :value1", dbConnection);
                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value2", NpgsqlDbType.Varchar));

                command.Parameters[0].Value = idPerfil;
                command.Parameters[1].Value = novaSituacao.ToUpper();

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - changeStatusPerfil", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public Perfil insert(Perfil perfil, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirPerfil");

            StringBuilder query = new StringBuilder();

            try
            {
                perfil.Id = recuperaProximoId(dbConnection);

                query.Append(" INSERT INTO SEG_PERFIL (ID_PERFIL, DESCRICAO, SITUACAO) VALUES ( :VALUE1, :VALUE2, 'A' ) ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = perfil.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = perfil.Descricao;

                command.ExecuteNonQuery();

                return perfil;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirPerfil", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_PERFIL_ID_PERFIL_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Perfil findByDescricao(String descricao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findPerfilByDescricao");

            Perfil perfilRetorno = null;

            StringBuilder query = new StringBuilder();
            
            try
            {
                query.Append(" SELECT ID_PERFIL, DESCRICAO, SITUACAO FROM SEG_PERFIL WHERE DESCRICAO = :VALUE1");
                
                NpgsqlCommand command = new NpgsqlCommand( query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = descricao.ToUpper();

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    perfilRetorno = new Perfil(dr.GetInt64(0), dr.GetString(1), dr.GetString(2));
                }

                return perfilRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findPerfilByDescricao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Perfil update(Perfil perfil, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updatePerfil");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("UPDATE SEG_PERFIL SET DESCRICAO = :VALUE2, SITUACAO = :VALUE3 WHERE ID_PERFIL = :VALUE1", dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = perfil.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = perfil.Descricao;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = perfil.Situacao;

                command.ExecuteNonQuery();

                return perfil;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    
    }
}
