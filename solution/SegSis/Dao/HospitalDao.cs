﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.Entidade;
using Npgsql;
using SWS.Excecao;
using SWS.View.Resources;
using NpgsqlTypes;
using System.Data;
using SWS.IDao;
using SWS.Helper;

namespace SWS.Dao
{
    class HospitalDao : IHospitalDao
    {
        public Hospital insert(Hospital hospital, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirHospital");

            StringBuilder query = new StringBuilder();

            try
            {
                hospital.Id = recuperaProximoId(dbConnection);

                query.Append(" INSERT INTO SEG_HOSPITAL ( ID_HOSPITAL, DESCRICAO, ENDERECO, NUMERO, COMPLEMENTO, ");
                query.Append(" BAIRRO, CEP, UF, TELEFONE1, TELEFONE2, OBSERVACAO, SITUACAO, ID_CIDADE ) VALUES ");
                query.Append(" (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6, :VALUE7, :VALUE8, :VALUE9, ");
                query.Append(" :VALUE10, :VALUE11, 'A', :VALUE12)");
            
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = hospital.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = hospital.Nome;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = hospital.Endereco;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = hospital.Numero;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = hospital.Complemento;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = hospital.Bairro;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = hospital.Cep;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Varchar));
                command.Parameters[7].Value = hospital.Uf;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                command.Parameters[8].Value = hospital.Telefone1;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                command.Parameters[9].Value = hospital.Telefone2;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Varchar));
                command.Parameters[10].Value = hospital.Observacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Integer));
                command.Parameters[11].Value = hospital.Cidade != null ? hospital.Cidade.Id : (long?)null;

                command.ExecuteNonQuery();

                return hospital;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirHospital", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Hospital update(Hospital hospital, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateHospital");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_HOSPITAL SET DESCRICAO = :VALUE2, ENDERECO = :VALUE3, NUMERO = :VALUE4, ");
                query.Append(" COMPLEMENTO = :VALUE5, BAIRRO = :VALUE6, CEP = :VALUE7, UF = :VALUE8, ");
                query.Append(" TELEFONE1 = :VALUE9, TELEFONE2 = :VALUE10, OBSERVACAO = :VALUE11, SITUACAO = :VALUE12, ID_CIDADE = :VALUE13 WHERE ID_HOSPITAL = :VALUE1");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = hospital.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = hospital.Nome;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = hospital.Endereco;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = hospital.Numero;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = hospital.Complemento;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = hospital.Bairro;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = hospital.Cep;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Varchar));
                command.Parameters[7].Value = hospital.Uf;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                command.Parameters[8].Value = hospital.Telefone1;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                command.Parameters[9].Value = hospital.Telefone2;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Varchar));
                command.Parameters[10].Value = hospital.Observacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Varchar));
                command.Parameters[11].Value = hospital.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Integer));
                command.Parameters[12].Value = hospital.Cidade != null ? hospital.Cidade.Id : (long?)null;
                
                command.ExecuteNonQuery();

                return hospital;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateHospital", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_HOSPITAL_ID_HOSPITAL_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Hospital findByNome(String descricao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findHospitalByDescricao");

            Hospital hospital = null;

            StringBuilder query = new StringBuilder();
            try
            {
                query.Append(" SELECT HOSPITAL.ID_HOSPITAL, HOSPITAL.DESCRICAO, HOSPITAL.ENDERECO, HOSPITAL.NUMERO, HOSPITAL.COMPLEMENTO, HOSPITAL.BAIRRO, ");
                query.Append(" HOSPITAL.CEP, HOSPITAL.UF, HOSPITAL.TELEFONE1, HOSPITAL.TELEFONE2, HOSPITAL.OBSERVACAO, HOSPITAL.SITUACAO, HOSPITAL.ID_CIDADE, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE ");
                query.Append(" FROM SEG_HOSPITAL HOSPITAL ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = HOSPITAL.ID_CIDADE ");
                query.Append(" WHERE HOSPITAL.DESCRICAO = :VALUE1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = descricao;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    hospital = new Hospital(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.GetString(4), dr.GetString(5), dr.GetString(6), dr.GetString(7), dr.GetString(8), dr.GetString(9), dr.GetString(10), dr.GetString(11), dr.IsDBNull(12) ? null : new CidadeIbge(dr.GetInt64(12), dr.GetString(13), dr.GetString(14), dr.GetString(15)));
                }

                return hospital;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findHospitalByDescricao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<Hospital> findAllByEstudo(Estudo pcmso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByEstudo");

            List<Hospital> hospitais = new List<Hospital>();

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT HOSPITAL.ID_HOSPITAL, HOSPITAL.DESCRICAO, HOSPITAL.ENDERECO, HOSPITAL.NUMERO, HOSPITAL.COMPLEMENTO, ");
                query.Append(" HOSPITAL.BAIRRO, HOSPITAL.CEP, HOSPITAL.UF, HOSPITAL.TELEFONE1, HOSPITAL.TELEFONE2, HOSPITAL.OBSERVACAO, ");
                query.Append(" HOSPITAL.SITUACAO, HOSPITAL.ID_CIDADE, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE ");
                query.Append(" FROM SEG_HOSPITAL HOSPITAL ");
                query.Append(" JOIN SEG_ESTUDO_HOSPITAL ESTUDO_HOSPITAL ON ESTUDO_HOSPITAL.ID_HOSPITAL = HOSPITAL.ID_HOSPITAL ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = HOSPITAL.ID_CIDADE ");
                query.Append(" WHERE ESTUDO_HOSPITAL.ID_ESTUDO = :VALUE1");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = pcmso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    hospitais.Add(new Hospital(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.GetString(4), dr.GetString(5), dr.GetString(6), dr.GetString(7), dr.GetString(8), dr.GetString(9), dr.GetString(10), dr.GetString(11), dr.IsDBNull(12) ? null : new CidadeIbge(dr.GetInt64(12), dr.GetString(13), dr.GetString(14), dr.GetString(15))));
                }

                return hospitais;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Hospital findHospitalById(Int64 idHospital, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findHospitalById");

            Hospital hospital = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT HOSPITAL.ID_HOSPITAL, HOSPITAL.DESCRICAO, HOSPITAL.ENDERECO, HOSPITAL.NUMERO, HOSPITAL.COMPLEMENTO, HOSPITAL.BAIRRO, HOSPITAL.CEP, HOSPITAL.UF, ");
                query.Append(" HOSPITAL.TELEFONE1, HOSPITAL.TELEFONE2, HOSPITAL.OBSERVACAO, HOSPITAL.SITUACAO, HOSPITAL.ID_CIDADE, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE ");
                query.Append(" FROM SEG_HOSPITAL HOSPITAL ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = HOSPITAL.ID_CIDADE ");
                query.Append(" WHERE ID_HOSPITAL = :VALUE1");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = idHospital;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    hospital = new Hospital(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.GetString(4), dr.GetString(5), dr.GetString(6), dr.GetString(7), dr.GetString(8), dr.GetString(9), dr.GetString(10), dr.GetString(11), dr.IsDBNull(12) ? null : new CidadeIbge(dr.GetInt64(12), dr.GetString(13), dr.GetString(14), dr.GetString(15)));
                }
                
                return hospital;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findHospitalById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findHospitalByFilter(Hospital hospital, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findHospitalByFilter");

            try
            {
                StringBuilder query = new StringBuilder();

                if (hospital.isNullForFilter())
                {
                    query.Append(" SELECT HOSPITAL.ID_HOSPITAL, HOSPITAL.DESCRICAO AS NOME, HOSPITAL.ENDERECO, HOSPITAL.NUMERO, HOSPITAL.COMPLEMENTO, HOSPITAL.BAIRRO, CIDADE.NOME AS CIDADE, ");
                    query.Append(" HOSPITAL.CEP, HOSPITAL.UF, HOSPITAL.TELEFONE1, HOSPITAL.TELEFONE2, HOSPITAL.OBSERVACAO, HOSPITAL.SITUACAO ");
                    query.Append(" FROM SEG_HOSPITAL HOSPITAL ");
                    query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = HOSPITAL.ID_CIDADE ");
                }
                else
                {
                    query.Append(" SELECT HOSPITAL.ID_HOSPITAL, HOSPITAL.DESCRICAO AS NOME, HOSPITAL.ENDERECO, HOSPITAL.NUMERO, HOSPITAL.COMPLEMENTO, HOSPITAL.BAIRRO, CIDADE.NOME AS CIDADE, ");
                    query.Append(" HOSPITAL.CEP, HOSPITAL.UF, HOSPITAL.TELEFONE1, HOSPITAL.TELEFONE2, HOSPITAL.OBSERVACAO, HOSPITAL.SITUACAO ");
                    query.Append(" FROM SEG_HOSPITAL HOSPITAL ");
                    query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = HOSPITAL.ID_CIDADE ");
                    query.Append(" WHERE TRUE ");

                    if (!String.IsNullOrWhiteSpace(hospital.Nome))
                        query.Append(" AND HOSPITAL.DESCRICAO LIKE :VALUE1 ");

                    if (!String.IsNullOrWhiteSpace(hospital.Situacao))
                        query.Append(" AND HOSPITAL.SITUACAO = :VALUE2 ");

                    query.Append(" ORDER BY NOME ASC ");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = hospital.Nome + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = hospital.Situacao;
              
                DataSet ds = new DataSet();

                adapter.Fill(ds, "Hospitais");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findHospitalByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }


        public Boolean findHospitalInEstudo(Hospital hospital, NpgsqlConnection dbConnection)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findHospitalInEstudo");

            StringBuilder query = new StringBuilder();

            try
            {
                Boolean retorno = Convert.ToBoolean(false);

                query.Append(" SELECT ID_ESTUDO, ID_HOSPITAL ");
                query.Append(" FROM SEG_ESTUDO_HOSPITAL WHERE ID_HOSPITAL = :VALUE1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = hospital.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    retorno = true;
                }

                return retorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findHospitalInEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public List<Hospital> findAll(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllHospitaisAtivo");

            List<Hospital> hospitais = new List<Hospital>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT HOSPITAL.ID_HOSPITAL, HOSPITAL.DESCRICAO AS NOME, HOSPITAL.ENDERECO, HOSPITAL.NUMERO, HOSPITAL.COMPLEMENTO, HOSPITAL.BAIRRO, CIDADE.NOME AS CIDADE, ");
                query.Append(" HOSPITAL.CEP, HOSPITAL.UF, HOSPITAL.TELEFONE1, HOSPITAL.TELEFONE2, HOSPITAL.OBSERVACAO, HOSPITAL.SITUACAO ");
                query.Append(" FROM SEG_HOSPITAL HOSPITAL ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = HOSPITAL.ID_CIDADE ");
                query.Append(" ORDER BY NOME ASC ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    hospitais.Add(new Hospital(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.GetString(4), dr.GetString(5), dr.GetString(6), dr.GetString(7), dr.GetString(8), dr.GetString(9), dr.GetString(10), dr.GetString(11), dr.IsDBNull(12) ? null : new CidadeIbge(dr.GetInt64(12), dr.GetString(13), dr.GetString(14), dr.GetString(15))));
                }

                return hospitais;


            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAll", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAllHospitalEstudoByPcmso(HospitalEstudo hospitalEstudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllHospitalEstudoByPcmso");

            try
            {
                StringBuilder query = new StringBuilder();

                if (String.IsNullOrWhiteSpace(hospitalEstudo.Hospital.Nome))
                {
                    query.Append(" SELECT HOSPITAL.ID_HOSPITAL, HOSPITAL.DESCRICAO, HOSPITAL.ENDERECO, HOSPITAL.NUMERO, HOSPITAL.COMPLEMENTO, HOSPITAL.BAIRRO, ");
                    query.Append(" CIDADE.NOME AS CIDADE, HOSPITAL.CEP, HOSPITAL.UF, HOSPITAL.TELEFONE1, HOSPITAL.TELEFONE2, HOSPITAL.OBSERVACAO, HOSPITAL.SITUACAO, HOSPITAL.ID_CIDADE FROM SEG_HOSPITAL HOSPITAL");
                    query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = HOSPITAL.ID_CIDADE ");
                    query.Append(" WHERE HOSPITAL.SITUACAO = 'A' AND ");
                    query.Append(" HOSPITAL.ID_HOSPITAL NOT IN (SELECT ID_HOSPITAL FROM SEG_ESTUDO_HOSPITAL WHERE ID_ESTUDO = :VALUE2 ) ");
                    

                }
                else
                {
                    query.Append(" SELECT HOSPITAL.ID_HOSPITAL, HOSPITAL.DESCRICAO, HOSPITAL.ENDERECO, HOSPITAL.NUMERO, HOSPITAL.COMPLEMENTO, HOSPITAL.BAIRRO, ");
                    query.Append(" CIDADE.NOME AS CIDADE, HOSPITAL.CEP, HOSPITAL.UF, HOSPITAL.TELEFONE1, HOSPITAL.TELEFONE2, HOSPITAL.OBSERVACAO, HOSPITAL.SITUACAO, HOSPITAL.ID_CIDADE FROM SEG_HOSPITAL HOSPITAL ");
                    query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = HOSPITAL.ID_CIDADE ");
                    query.Append(" WHERE HOSPITAL.SITUACAO = 'A' AND HOSPITAL.DESCRICAO LIKE :VALUE1 AND ");
                    query.Append(" HOSPITAL.ID_HOSPITAL NOT IN (SELECT ID_HOSPITAL FROM SEG_ESTUDO_HOSPITAL WHERE ID_ESTUDO = :VALUE2 ) ");
                    
                }
                
                query.Append(" ORDER BY DESCRICAO ASC");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = hospitalEstudo.Hospital.Nome + "%";
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[1].Value = hospitalEstudo.Pcmso.Id;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Hospitais");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllHospitaisAtivo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
