﻿
using SWS.IDao;
namespace SWS.Dao
{
    class FabricaDao
    {
        public static IUsuarioDao getUsuarioDao()
        {
            return new UsuarioDao();
        }

        public static IAtividadeDao getAtividadeDao()
        {
            return new AtividadeDao();
        }

        public static IFonteDao getFonteDao()
        {
            return new FonteDao();
        }

        public static ISetorDao getSetorDao()
        {
            return new SetorDao();
        }

        public static IEpiDao getEpiDao()
        {
            return new EpiDao();
        }

        public static IPerfilDao getPerfilDao()
        {
            return new PerfilDao();
        }

        public static IFuncaoInternaDao getFuncaoInternaDao()
        {
            return new FuncaoInternaDao();
        }

        public static IDominioDao getDominioDao()
        {
            return new DominioDao();
        }

        public static IAcaoDao getAcaoDao()
        {
            return new AcaoDao();
        }

        public static IUsuarioPerfilDao getUsuarioPerfilDao()
        {
            return new UsuarioPerfilDao();
        }

        public static IAgenteDao getAgenteDao()
        {
            return new AgenteDao();
        }

        public static IFuncaoDao getFuncaoDao()
        {
            return new FuncaoDao();
        }

        public static IVendedorDao getVendedorDao()
        {
            return new VendedorDao();
        }

        public static IClienteDao getClienteDao()
        {
            return new ClienteDao();
        }

        public static IClienteVendedorDao getClienteVendedorDao()
        {
            return new ClienteVendedorDao();
        }

        public static IClienteCnaeDao getClienteCnaeDao()
        {
            return new ClienteCnaeDao();
        }

        public static IEstudoDao getEstudoDao()
        {
            return new EstudoDao();
        }

        public static ICronogramaDao getCronogramaDao()
        {
            return new CronogramaDao();
        }

        public static ICronogramaAtividadeDao getCronogramaAtividadeDao()
        {
            return new CronogramaAtividadeDao();
        }

        public static IGheDao getGheDao()
        {
            return new GheDao();
        }

        public static IGheFonteDao getGheFonteDao()
        {
            return new GheFonteDao();
        }

        public static IGradExposicaoDao getGradExposicaoDao()
        {
            return new GradExposicaoDao();
        }

        public static IGradEfeitoDao getGradEfeitoDao()
        {
            return new GradEfeitoDao();
        }

        public static IGradSomaDao getGradSomaDao()
        {
            return new GradSomaDao();
        }

        public static IGheFonteAgenteDao getGheFonteAgenteDao()
        {
            return new GheFonteAgenteDao();
        }

        public static INotaDao getNormaDao()
        {
            return new NotaDao();
        }

        public static IGheNotaDao getGheNormaDao()
        {
            return new GheNotaDao();
        }

        public static IFuncaoInternaUsuarioDao getFuncaoInternaUsuarioDao()
        {
            return new FuncaoInternaUsuarioDao();
        }

        public static IGheFonteAgenteEpiDao getGheFonteAgenteEpiDao()
        {
            return new GheFonteAgenteEpiDao();
        }

        public static IGheSetorDao getGheSetorDao()
        {
            return new GheSetorDao();
        }

        public static IClienteFuncaoDao getClienteFuncaoDao()
        {
            return new ClienteFuncaoDao();
        }

        public static IGheSetorClienteFuncaoDao getGheSetorClienteFuncaoDao()
        {
            return new GheSetorClienteFuncaoDao();
        }

        public static ICnaeEstudoDao getCnaeEstudoDao()
        {
            return new CnaeEstudoDao();
        }

        public static IRevisaoDao getRevisaoDao()
        {
            return new RevisaoDao();
        }

        public static IExameDao getExameDao()
        {
            return new ExameDao();
        }

        public static IGheFonteAgenteExameDao getGheFonteAgenteExameDao()
        {
            return new GheFonteAgenteExameDao();
        }

        public static IMaterialHospitalarDao getMaterialHospitalarDao()
        {
            return new MaterialHospitalarDao();
        }

        public static IPeriodicidadeDao getPeriodicidadeDao()
        {
            return new PeriodicidadeDao();
        }

        public static IGheFonteAgenteExamePeriodicidadeDao getGheFonteAgenteExamePeriodicidadeDao()
        {
            return new GheFonteAgenteExamePeriodicidadeDao();
        }

        public static IHospitalDao getHospitalDao()
        {
            return new HospitalDao();
        }

        public static IMedicoDao getMedicoDao()
        {
            return new MedicoDao();
        }

        public static IMaterialHospitalarEstudoDao getMaterialHospitalarEstudoDao()
        {
            return new MaterialHospitalarEstudoDao();
        }

        public static IHospitalEstudoDao getHospitalEstudoDao()
        {
            return new HospitalEstudoDao();
        }

        public static IMedicoEstudoDao getMedicoEstudoDao()
        {
            return new MedicoEstudoDao();
        }

        public static IFuncionarioDao getFuncionarioDao()
        {
            return new FuncionarioDao();
        }

        public static IClienteFuncaoFuncionarioDao getClienteFuncaoFuncionarioDao()
        {
            return new ClienteFuncaoFuncionarioDao();
        }

        public static IAsoDao getAsoDao()
        {
            return new AsoDao();
        }

        public static IClienteFuncaoExameDao getClienteFuncaoExameDao()
        {
            return new ClienteFuncaoExameDao();
        }

        public static IClienteFuncaoExamePeriodicidadeDao getClienteFuncaoExamePeriodicidadeDao()
        {
            return new ClienteFuncaoExamePeriodicidadeDao();
        }

        public static IGheFonteAgenteExameAsoDao getGheFonteAgenteExameAsoDao()
        {
            return new GheFonteAgenteExameAsoDao();
        }

        public static IClienteFuncaoExameASoDao getClienteFuncaoExameASoDao()
        {
            return new ClienteFuncaoExameAsoDao();
        }

        public static IRelatorioDao getRelatorioDao()
        {
            return new RelatorioDao();
        }

        public static ISalaDao getSalaDao()
        {
            return new SalaDao();
        }

        public static IContratoDao getContratoDao()
        {
            return new ContratoDao();
        }

        public static IContratoExameDao getContratoExameDao()
        {
            return new ContratoExameDao();
        }

        public static IContratoProdutoDao getContratoProdutoDao()
        {
            return new ContratoProdutoDao();
        }

        public static IProdutoDao getProdutoDao()
        {
            return new ProdutoDao();
        }

        public static IMovimentoDao getMovimentoDao()
        {
            return new MovimentoDao();
        }

        public static IEmpresaDao getEmpresaDao()
        {
            return new EmpresaDao();
        }

        public static IArquivoDao getArquivoDao()
        {
            return new ArquivoDao();
        }

        public static IClienteArquivoDao getClienteArquivoDao()
        {
            return new ClienteArquivoDao();
        }

        public static IRiscoDao getRiscoDao()
        {
            return new RiscoDao();
        }

        public static IAcompanhamentoAtendimentoDao getAcompanhamentoAtendimentoDao()
        {
            return new AcompanhamentoAtendimentoDao();
        }

        public static ISalaExameDao getSalaExameDao()
        {
            return new SalaExameDao();
        }

        public static IProntuarioDao getProntuarioDao()
        {
            return new ProntuarioDao();
        }

        public static IAsoGheSetorDao getAsoGheSetorDao()
        {
            return new AsoGheSetorDao();
        }

        public static IAsoAgenteRiscoDao getAsoAgenteRisco()
        {
            return new AsoAgenteRiscoDao();
        }

        public static IPlanoDao getPlanoDao()
        {
            return new PlanoDao();
        }

        public static IBancoDao getBancoDao()
        {
            return new BancoDao();
        }

        public static IContaDao getContaDao()
        {
            return new ContaDao();
        }

        public static INfeDao getNfeDao()
        {
            return new NfeDao();
        }

        public static IConfiguracaoDao getConfiguracaoDao()
        {
            return new ConfiguracaoDao();
        }

        public static IFormaDao getFormaDao()
        {
            return new FormaDao();
        }

        public static IPlanoFormaDao getPlanoFormaDao()
        {
            return new PlanoFormaDao();
        }

        public static ICobrancaDao getCobrancaDao()
        {
            return new CobrancaDao();
        }

        public static IParcelaDao getParcelaDao()
        {
            return new ParcelaDao();
        }

        public static IItensCanceladosNfDao getItensCanceladosNfDao()
        {
            return new ItensCanceladosNfDao();
        }

        public static ICidadeIbgeDao getCidadeIbgeDao()
        {
            return new CidadeIbgeDao();
        }

        public static IGheSetorClienteFuncaoExameDao getGheSetorClienteFuncaoExameDao()
        {
            return new GheSetorClienteFuncaoExameDao();
        }

        public static ILogDao getLogDao()
        {
            return new LogDao();
        }

        public static IProtocoloDao getProtocoloDao()
        {
            return new ProtocoloDao();
        }

        public static IDocumentoDao getDocumentoDao()
        {
            return new DocumentoDao();
        }

        public static IDocumentoProtocoloDao getDocumentoProtocoloDao()
        {
            return new DocumentoProtocoloDao();
        }

        public static ICorrecaoEstudoDao getCorrecaoEstudoDao()
        {
            return new CorrecaoEstudoDao();
        }

        public static IComentarioFuncaoDao getComentarioFuncaoDao()
        {
            return new ComentarioFuncaoDao();
        }

        public static IRelatorioAnualDao getRelatorioAnualDao()
        {
            return new RelatorioAnualDao();
        }

        public static IEstimativaEstudoDao getEstimativaEstudoDao()
        {
            return new EstimativaEstudoDao();
        }

        public static IEstimativaDao getEstimativaDao()
        {
            return new EstimativaDao();
        }

        public static IArquivoAnexoDao getArquivoAnexoDao()
        {
            return new ArquivoAnexoDao();
        }

        public static IClientePropostaDao getClientePropostaDao()
        {
            return new ClientePropostaDao();
        }

        public static IEmailDao getEmailDao()
        {
            return new EmailDao();
        }

        public static IRamoDao getRamoDao()
        {
            return new RamoDao();
        }

        public static IContatoDao getContatoDao()
        {
            return new ContatoDao();
        }

        public static IUsuarioClienteDao getUsuarioClienteDao()
        {
            return new UsuarioClienteDao();
        }

        public static IMedicoExameDao getMedicoExameDao()
        {
            return new MedicoExameDao();
        }

        public static ICentroCustoDao getCentroCustoDao()
        {
            return new CentroCustoDao();
        }

        public static ILoteEsocialDao getLoteEsocialDao()
        {
            return new LoteEsocialDao();
        }

        public static IRelatorioExameDao getRelatorioExameDao()
        {
            return new RelatorioExameDao();
        }

        public static IProcedimentoEsocialDao getProcedimentoEsocialDao()
        {
            return new ProcedimentoEsocialDao();
        }

        public static IComplementoAgenteEsocialDao getComplementoEsocialDao()
        {
            return new ComplementoAgenteEsocialDao();
        }

        public static IMonitoramentoDao getMonitoramentoDao()
        {
            return new MonitoramentoDao();
        }

        public static IMonitoramentoGheFonteAgenteDao getMonitoramentoGheFonteAgenteDao()
        {
            return new MonitoramentoGheFonteAgenteDao();
        }

        public static IEpiMonitoramentoDao getEpiMonitoramentoDao()
        {
            return new EpiMonitoramentoDao();
        }

        public static IClienteFuncionarioDao getclienteFuncionarioDao()
        {
            return new ClienteFuncionarioDao();
        }

        public static ILoginAuditoriaDao getLoginAuditoriaDao()
        {
            return new LoginAuditoriaDao();
        }

        public static IDocumentoEstudoDao getDocumentoEstudoDao()
        {
            return new DocumentoEstudoDao();
        }

    }
}
