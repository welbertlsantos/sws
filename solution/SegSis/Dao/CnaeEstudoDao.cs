﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using Npgsql;
using NpgsqlTypes;
using SWS.Excecao;
using SWS.View.Resources;
using SWS.Helper;

namespace SWS.Dao
{
    class CnaeEstudoDao : ICnaeEstudoDao
    {
        public CnaeEstudo insert(CnaeEstudo cnaeEstudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertCnaeEstudo");

            StringBuilder query = new StringBuilder();

            try
            {
                cnaeEstudo.Id = recuperaProximoId(dbConnection);

                query.Append(" INSERT INTO SEG_CNAE_ESTUDO (ID_CNAE_ESTUDO, ID_ESTUDO, ID_CNAE, FLAG_CLIENTE ) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4 ) ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cnaeEstudo.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = cnaeEstudo.Estudo.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = cnaeEstudo.Cnae.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Boolean));
                command.Parameters[3].Value = cnaeEstudo.FlagCliente;

                command.ExecuteNonQuery();

                return cnaeEstudo;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertCnaeEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void delete(CnaeEstudo cnaeEstudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("DELETE FROM SEG_CNAE_ESTUDO WHERE ID_CNAE_ESTUDO = :VALUE1 ", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cnaeEstudo.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void replicaCnaeEstudo(Int64 idEstudo, Int64 idEstudoNovo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método replicaCanaeEstudo");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" INSERT INTO SEG_CNAE_ESTUDO (ID_CNAE_ESTUDO, ID_ESTUDO, ID_CNAE, FLAG_CLIENTE) ");
                query.Append(" (SELECT NEXTVAL('SEG_CNAE_ESTUDO_ID_CNAE_ESTUDO_SEQ'), :VALUE1 AS ID_ESTUDO, ID_CNAE, FLAG_CLIENTE ");
                query.Append(" FROM SEG_CNAE_ESTUDO WHERE ID_ESTUDO = :VALUE2)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = idEstudoNovo;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = idEstudo;

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - replicaCanaeEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<CnaeEstudo> findAllByPcmso(Estudo pcmso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByPcmso");

            List<CnaeEstudo> cnaes = new List<CnaeEstudo>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID_CNAE_ESTUDO, ID_ESTUDO, ID_CNAE, FLAG_CLIENTE ");
                query.Append(" FROM SEG_CNAE_ESTUDO ");
                query.Append(" WHERE ID_ESTUDO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = pcmso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    CnaeEstudo cnaeEstudo = new CnaeEstudo();
                    cnaeEstudo.Id = (long)dr.GetInt64(0);
                    cnaeEstudo.FlagCliente = dr.GetBoolean(3);
                    cnaeEstudo.Estudo = pcmso;
                    Cnae cnae = new Cnae();
                    cnae.Id = (long)dr.GetInt64(2);
                    cnaeEstudo.Cnae = cnae;
                    cnaes.Add(cnaeEstudo);
                    
                }

                return cnaes;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByPcmso", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_CNAE_ESTUDO_ID_CNAE_ESTUDO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
