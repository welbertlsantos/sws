﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using Npgsql;
using NpgsqlTypes;
using SWS.Excecao;
using SWS.Helper;

namespace SWS.Dao
{
    class GradSomaDao : IGradSomaDao
    {
        public GradSoma findGradSomaByGradSoma(GradSoma gradSoma, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGradSomaByGradSoma");

            GradSoma gradSomaRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_GRAD_SOMA, FAIXA, MED_CONTROLE, DESCRICAO FROM SEG_GRAD_SOMA WHERE FAIXA = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = gradSoma.Faixa;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gradSomaRetorno = new GradSoma(dr.GetInt64(0), dr.GetInt64(1), dr.GetString(2), dr.GetString(3));

                }

                return gradSomaRetorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGradSomaByGradSoma", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public GradSoma findGradSomaById(GradSoma gradSoma, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGradSomaById");

            GradSoma gradSomaRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" select id_grad_Soma, faixa, med_controle, descricao from seg_grad_soma where id_grad_soma = :value1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                if (gradSoma != null)
                {
                    command.Parameters[0].Value = gradSoma.Id;
                }
                else
                {
                    command.Parameters[0].Value = null;
                }

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gradSomaRetorno = new GradSoma(dr.GetInt64(0), dr.GetInt64(1), dr.GetString(2), dr.GetString(3));

                }

                return gradSomaRetorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGradSomaById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
