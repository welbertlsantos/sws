﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;
using SWS.Entidade;
using NpgsqlTypes;

namespace SWS.Dao
{
    class ProntuarioDao : IProntuarioDao
    {
        
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("select nextval(('seg_prontuario_id_prontuario_seq'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Prontuario insertProntuario(Prontuario prontuario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertProntuario");

            StringBuilder query = new StringBuilder();

            try
            {
                prontuario.Id = recuperaProximoId(dbConnection);
                
                query.Append(" INSERT INTO SEG_PRONTUARIO (  ");
                query.Append(" ID_PRONTUARIO, ID_USUARIO, ID_ASO, PRESSAO, FREQUENCIA, PESO, ALTURA, LASSEGUE, BEBIDA, FUMO, DIARREIA,  ");
                query.Append(" TOSSE, AFASTADO, CONVULSAO, HEPATITE, ACIDENTE, TUBERCULOSE, ANTITETANICA, ATIVIDADE_FISICA,  ");
                query.Append(" MEDO_ALTURA, DORES_JUNTAS, PALPITACAO, TONTURAS, AZIA, DESMAIO, DORES_CABECA, EPILEPSIA,   ");
                query.Append(" INSONIA, PROBLEMA_AUDICAO, TENDINITE, FRATURA, ALERGIA, ASMA, DIABETE, HERNIA, MANCHAS_PELE, PRESSAO_ALTA, ");
                query.Append(" PROBLEMA_VISAO, PROBLEMA_COLUNA, RINITE, VARIZES, OCULOS, APARELHO_MUCOSA, APARELHO_MUCOSA_OBS, APARELHO_VASCULAR,  ");
                query.Append(" APARELHO_VASCULAR_OBS, APARELHO_RESPIRATORIO, APARELHO_RESPIRATORIO_OBS, APARELHO_OLHO_DIR, APARELHO_OLHO_DIR_OBS, ");
                query.Append(" APARELHO_OLHO_ESQ, APARELHO_OLHO_ESQ_OBS, APARELHO_PROTESE, APARELHO_PROTESE_OBS, APARELHO_GANGLIO, APARELHO_GANGLIO_OBS, ");
                query.Append(" APARELHO_ABDOMEN, ");
                query.Append(" APARELHO_ABDOMEN_OBS, APARELHO_URINA, APARELHO_URINA_OBS, APARELHO_MEMBRO, APARELHO_MEMBRO_OBS, APARELHO_COLUNA, ");
                query.Append(" APARELHO_COLUNA_OBS, APARELHO_EQUILIBRIO, APARELHO_EQUILIBRIO_OBS, APARELHO_FORCA, APARELHO_FORCA_OBS, APARELHO_MOTOR, ");
                query.Append(" APARELHO_MOTOR_OBS, PARTO, METODO, DATA_ULT_MENSTRUACAO, PREVI_AFASTADO, PREVI_RECURSO, DATA_AFASTAMENTO,  ");
                query.Append(" CAUSA, LAUDO, LAUDO_COMENTARIO, METODO_OBS, OBSERVACAO_APARELHO )  ");
                query.Append(" VALUES ( ");
                query.Append(" :VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6, :VALUE7, :VALUE8, :VALUE9, :VALUE10,   ");
                query.Append(" :VALUE11, :VALUE12, :VALUE13, :VALUE14, :VALUE15, :VALUE16, :VALUE17, :VALUE18, ");
                query.Append(" :VALUE19, :VALUE20, :VALUE21, :VALUE22, :VALUE23, :VALUE24, :VALUE25, :VALUE26, :VALUE27,  ");
                query.Append(" :VALUE28, :VALUE29, :VALUE30, :VALUE31, :VALUE32, :VALUE33, :VALUE34, :VALUE35,  ");
                query.Append(" :VALUE36, :VALUE37, :VALUE38, :VALUE39, :VALUE40, :VALUE41, :VALUE42, :VALUE43, :VALUE44, ");
                query.Append(" :VALUE45, :VALUE46, :VALUE47, :VALUE48, :VALUE49, :VALUE50, :VALUE51, :VALUE52, :VALUE53,  ");
                query.Append(" :VALUE54, :VALUE55, :VALUE56, :VALUE57, :VALUE58, :VALUE59, :VALUE60, :VALUE61, :VALUE62, ");
                query.Append(" :VALUE63, :VALUE64, :VALUE65, :VALUE66, :VALUE67, :VALUE68, :VALUE69, :VALUE70, :VALUE71, ");
                query.Append(" :VALUE72, :VALUE73, :VALUE74, :VALUE75, :VALUE76, :VALUE77, :VALUE78, :VALUE79, :VALUE80, :VALUE81  ) ");
                

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = prontuario.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = prontuario.Usuario.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = prontuario.Aso.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = prontuario.Pressao;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = prontuario.Frequencia;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Numeric));
                command.Parameters[5].Value = prontuario.Peso;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Numeric));
                command.Parameters[6].Value = prontuario.Altura;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Varchar));
                command.Parameters[7].Value = prontuario.Lassegue;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Boolean));
                command.Parameters[8].Value = prontuario.Bebida;

                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Boolean));
                command.Parameters[9].Value = prontuario.Fumo;

                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Boolean));
                command.Parameters[10].Value = prontuario.Diarreia;

                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Boolean));
                command.Parameters[11].Value = prontuario.Tosse;

                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Boolean));
                command.Parameters[12].Value = prontuario.Afastado;

                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Boolean));
                command.Parameters[13].Value = prontuario.Convulsao;

                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Boolean));
                command.Parameters[14].Value = prontuario.Hepatite;

                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Boolean));
                command.Parameters[15].Value = prontuario.Acidente;

                command.Parameters.Add(new NpgsqlParameter("VALUE17", NpgsqlDbType.Boolean));
                command.Parameters[16].Value = prontuario.Tuberculose;

                command.Parameters.Add(new NpgsqlParameter("VALUE18", NpgsqlDbType.Boolean));
                command.Parameters[17].Value = prontuario.Antitetanica;

                command.Parameters.Add(new NpgsqlParameter("VALUE19", NpgsqlDbType.Boolean));
                command.Parameters[18].Value = prontuario.AtividadeFisica;

                command.Parameters.Add(new NpgsqlParameter("VALUE20", NpgsqlDbType.Boolean));
                command.Parameters[19].Value = prontuario.MedoAltura;

                command.Parameters.Add(new NpgsqlParameter("VALUE21", NpgsqlDbType.Boolean));
                command.Parameters[20].Value = prontuario.DoresJuntas;

                command.Parameters.Add(new NpgsqlParameter("VALUE22", NpgsqlDbType.Boolean));
                command.Parameters[21].Value = prontuario.Palpitacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE23", NpgsqlDbType.Boolean));
                command.Parameters[22].Value = prontuario.Tontura;

                command.Parameters.Add(new NpgsqlParameter("VALUE24", NpgsqlDbType.Boolean));
                command.Parameters[23].Value = prontuario.Azia;

                command.Parameters.Add(new NpgsqlParameter("VALUE25", NpgsqlDbType.Boolean));
                command.Parameters[24].Value = prontuario.Desmaio;

                command.Parameters.Add(new NpgsqlParameter("VALUE26", NpgsqlDbType.Boolean));
                command.Parameters[25].Value = prontuario.DoresCabeca;

                command.Parameters.Add(new NpgsqlParameter("VALUE27", NpgsqlDbType.Boolean));
                command.Parameters[26].Value = prontuario.Epilepsia;

                command.Parameters.Add(new NpgsqlParameter("VALUE28", NpgsqlDbType.Boolean));
                command.Parameters[27].Value = prontuario.Insonia;

                command.Parameters.Add(new NpgsqlParameter("VALUE29", NpgsqlDbType.Boolean));
                command.Parameters[28].Value = prontuario.ProblemaAudicao;

                command.Parameters.Add(new NpgsqlParameter("VALUE30", NpgsqlDbType.Boolean));
                command.Parameters[29].Value = prontuario.Tendinite;

                command.Parameters.Add(new NpgsqlParameter("VALUE31", NpgsqlDbType.Boolean));
                command.Parameters[30].Value = prontuario.Fratura;

                command.Parameters.Add(new NpgsqlParameter("VALUE32", NpgsqlDbType.Boolean));
                command.Parameters[31].Value = prontuario.Alergia;

                command.Parameters.Add(new NpgsqlParameter("VALUE33", NpgsqlDbType.Boolean));
                command.Parameters[32].Value = prontuario.Asma;

                command.Parameters.Add(new NpgsqlParameter("VALUE34", NpgsqlDbType.Boolean));
                command.Parameters[33].Value = prontuario.Diabete;

                command.Parameters.Add(new NpgsqlParameter("VALUE35", NpgsqlDbType.Boolean));
                command.Parameters[34].Value = prontuario.Hernia;

                command.Parameters.Add(new NpgsqlParameter("VALUE36", NpgsqlDbType.Boolean));
                command.Parameters[35].Value = prontuario.ManchasPele;

                command.Parameters.Add(new NpgsqlParameter("VALUE37", NpgsqlDbType.Boolean));
                command.Parameters[36].Value = prontuario.PressaoAlta;

                command.Parameters.Add(new NpgsqlParameter("VALUE38", NpgsqlDbType.Boolean));
                command.Parameters[37].Value = prontuario.ProblemaVisao;

                command.Parameters.Add(new NpgsqlParameter("VALUE39", NpgsqlDbType.Boolean));
                command.Parameters[38].Value = prontuario.ProblemaColuna;

                command.Parameters.Add(new NpgsqlParameter("VALUE40", NpgsqlDbType.Boolean));
                command.Parameters[39].Value = prontuario.Rinite;

                command.Parameters.Add(new NpgsqlParameter("VALUE41", NpgsqlDbType.Boolean));
                command.Parameters[40].Value = prontuario.Varize;

                command.Parameters.Add(new NpgsqlParameter("VALUE42", NpgsqlDbType.Boolean));
                command.Parameters[41].Value = prontuario.Oculos;

                command.Parameters.Add(new NpgsqlParameter("VALUE43", NpgsqlDbType.Varchar));
                command.Parameters[42].Value = prontuario.AparelhoMucosa;

                command.Parameters.Add(new NpgsqlParameter("VALUE44", NpgsqlDbType.Varchar));
                command.Parameters[43].Value = prontuario.AparelhoMucosaObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE45", NpgsqlDbType.Varchar));
                command.Parameters[44].Value = prontuario.AparelhoVascular;

                command.Parameters.Add(new NpgsqlParameter("VALUE46", NpgsqlDbType.Varchar));
                command.Parameters[45].Value = prontuario.AparelhoVascularObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE47", NpgsqlDbType.Varchar));
                command.Parameters[46].Value = prontuario.AparelhoRespiratorio;

                command.Parameters.Add(new NpgsqlParameter("VALUE48", NpgsqlDbType.Varchar));
                command.Parameters[47].Value = prontuario.AparelhoRespiratorioObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE49", NpgsqlDbType.Varchar));
                command.Parameters[48].Value = prontuario.AparelhoOlhoDir;

                command.Parameters.Add(new NpgsqlParameter("VALUE50", NpgsqlDbType.Varchar));
                command.Parameters[49].Value = prontuario.AparelhoOlhoDirObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE51", NpgsqlDbType.Varchar));
                command.Parameters[50].Value = prontuario.AparelhoOlhoEsq;

                command.Parameters.Add(new NpgsqlParameter("VALUE52", NpgsqlDbType.Varchar));
                command.Parameters[51].Value = prontuario.AparelhoOlhoEsqObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE53", NpgsqlDbType.Varchar));
                command.Parameters[52].Value = prontuario.AparelhoProtese;

                command.Parameters.Add(new NpgsqlParameter("VALUE54", NpgsqlDbType.Varchar));
                command.Parameters[53].Value = prontuario.AparelhoProteseObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE55", NpgsqlDbType.Varchar));
                command.Parameters[54].Value = prontuario.AparelhoGanglio;

                command.Parameters.Add(new NpgsqlParameter("VALUE56", NpgsqlDbType.Varchar));
                command.Parameters[55].Value = prontuario.AparelhoGanglioObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE57", NpgsqlDbType.Varchar));
                command.Parameters[56].Value = prontuario.AparelhoAbdomen;

                command.Parameters.Add(new NpgsqlParameter("VALUE58", NpgsqlDbType.Varchar));
                command.Parameters[57].Value = prontuario.AparelhoAbdomenObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE59", NpgsqlDbType.Varchar));
                command.Parameters[58].Value = prontuario.AparelhoUrina;

                command.Parameters.Add(new NpgsqlParameter("VALUE60", NpgsqlDbType.Varchar));
                command.Parameters[59].Value = prontuario.AparelhoUrinaObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE61", NpgsqlDbType.Varchar));
                command.Parameters[60].Value = prontuario.AparelhoMembro;

                command.Parameters.Add(new NpgsqlParameter("VALUE62", NpgsqlDbType.Varchar));
                command.Parameters[61].Value = prontuario.AparelhoMembroObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE63", NpgsqlDbType.Varchar));
                command.Parameters[62].Value = prontuario.AparelhoColuna;

                command.Parameters.Add(new NpgsqlParameter("VALUE64", NpgsqlDbType.Varchar));
                command.Parameters[63].Value = prontuario.AparelhoColunaObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE65", NpgsqlDbType.Varchar));
                command.Parameters[64].Value = prontuario.AparelhoEquilibrio;

                command.Parameters.Add(new NpgsqlParameter("VALUE66", NpgsqlDbType.Varchar));
                command.Parameters[65].Value = prontuario.AparelhoEquilibrioObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE67", NpgsqlDbType.Varchar));
                command.Parameters[66].Value = prontuario.AparelhoForca;

                command.Parameters.Add(new NpgsqlParameter("VALUE68", NpgsqlDbType.Varchar));
                command.Parameters[67].Value = prontuario.AparelhoForcaObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE69", NpgsqlDbType.Varchar));
                command.Parameters[68].Value = prontuario.AparelhoMotor;

                command.Parameters.Add(new NpgsqlParameter("VALUE70", NpgsqlDbType.Varchar));
                command.Parameters[69].Value = prontuario.AparelhoMotorObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE71", NpgsqlDbType.Integer));
                command.Parameters[70].Value = prontuario.Parto;

                command.Parameters.Add(new NpgsqlParameter("VALUE72", NpgsqlDbType.Varchar));
                command.Parameters[71].Value = prontuario.Metodo;

                command.Parameters.Add(new NpgsqlParameter("VALUE73", NpgsqlDbType.Date));
                command.Parameters[72].Value = prontuario.DataUltMenstruacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE74", NpgsqlDbType.Boolean));
                command.Parameters[73].Value = prontuario.PreviAfastado;

                command.Parameters.Add(new NpgsqlParameter("VALUE75", NpgsqlDbType.Boolean));
                command.Parameters[74].Value = prontuario.PreviRecurso;

                command.Parameters.Add(new NpgsqlParameter("VALUE76", NpgsqlDbType.Date));
                command.Parameters[75].Value = prontuario.DataRecurso;

                command.Parameters.Add(new NpgsqlParameter("VALUE77", NpgsqlDbType.Text));
                command.Parameters[76].Value = prontuario.Causa;

                command.Parameters.Add(new NpgsqlParameter("VALUE78", NpgsqlDbType.Varchar));
                command.Parameters[77].Value = prontuario.Laudo;

                command.Parameters.Add(new NpgsqlParameter("VALUE79", NpgsqlDbType.Text));
                command.Parameters[78].Value = prontuario.LaudoComentario;

                command.Parameters.Add(new NpgsqlParameter("VALUE80", NpgsqlDbType.Text));
                command.Parameters[79].Value = prontuario.MetodoObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE81", NpgsqlDbType.Text));
                command.Parameters[80].Value = prontuario.TextComentarioAparelho;
                
                command.ExecuteNonQuery();

                return prontuario;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertProntuario", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Prontuario updateProntuario(Prontuario prontuario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateProntuario");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_PRONTUARIO SET  ");
                query.Append(" ID_USUARIO = :VALUE2, ID_ASO = :VALUE3, PRESSAO = :VALUE4, FREQUENCIA = :VALUE5, PESO = :VALUE6, ALTURA = :VALUE7,  ");
                query.Append(" LASSEGUE = :VALUE8, BEBIDA = :VALUE9, FUMO = :VALUE10, DIARREIA = :VALUE11, TOSSE = :VALUE12, ");
                query.Append(" AFASTADO = :VALUE13, CONVULSAO = :VALUE14, HEPATITE = :VALUE15, ACIDENTE = :VALUE16, ");
                query.Append(" TUBERCULOSE = :VALUE17, ANTITETANICA = :VALUE18, ATIVIDADE_FISICA = :VALUE19, MEDO_ALTURA = :VALUE20, ");
                query.Append(" DORES_JUNTAS = :VALUE21, PALPITACAO = :VALUE22, TONTURAS = :VALUE23, AZIA = :VALUE24, DESMAIO = :VALUE25, ");
                query.Append(" DORES_CABECA = :VALUE26, EPILEPSIA = :VALUE27, INSONIA = :VALUE28, PROBLEMA_AUDICAO = :VALUE29, ");
                query.Append(" TENDINITE = :VALUE30, FRATURA = :VALUE31, ALERGIA = :VALUE32, ASMA = :VALUE33, DIABETE = :VALUE34, HERNIA = :VALUE35, ");
                query.Append(" MANCHAS_PELE = :VALUE36, PRESSAO_ALTA = :VALUE37, PROBLEMA_VISAO = :VALUE38, PROBLEMA_COLUNA = :VALUE39, RINITE = :VALUE40, ");
                query.Append(" VARIZES = :VALUE41, OCULOS = :VALUE42, APARELHO_MUCOSA = :VALUE43, APARELHO_MUCOSA_OBS = :VALUE44, ");
                query.Append(" APARELHO_VASCULAR = :VALUE45, APARELHO_VASCULAR_OBS = :VALUE46, APARELHO_RESPIRATORIO = :VALUE47, ");
                query.Append(" APARELHO_RESPIRATORIO_OBS = :VALUE48, APARELHO_OLHO_DIR = :VALUE49, APARELHO_OLHO_DIR_OBS = :VALUE50, ");
                query.Append(" APARELHO_OLHO_ESQ = :VALUE51, APARELHO_OLHO_ESQ_OBS = :VALUE52, APARELHO_PROTESE = :VALUE53, APARELHO_PROTESE_OBS = :VALUE54, ");
                query.Append(" APARELHO_GANGLIO = :VALUE55, APARELHO_GANGLIO_OBS = :VALUE56, ");
                query.Append(" APARELHO_ABDOMEN = :VALUE57, ");
                query.Append(" APARELHO_ABDOMEN_OBS = :VALUE58, APARELHO_URINA = :VALUE59, APARELHO_URINA_OBS = :VALUE60, APARELHO_MEMBRO = :VALUE61, ");
                query.Append(" APARELHO_MEMBRO_OBS = :VALUE62, APARELHO_COLUNA = :VALUE63, APARELHO_COLUNA_OBS = :VALUE64, APARELHO_EQUILIBRIO = :VALUE65, ");
                query.Append(" APARELHO_EQUILIBRIO_OBS = :VALUE66, APARELHO_FORCA = :VALUE67, APARELHO_FORCA_OBS = :VALUE68, APARELHO_MOTOR = :VALUE69, ");
                query.Append(" APARELHO_MOTOR_OBS = :VALUE70, PARTO = :VALUE71, METODO = :VALUE72, DATA_ULT_MENSTRUACAO = :VALUE73, ");
                query.Append(" PREVI_AFASTADO = :VALUE74, PREVI_RECURSO = :VALUE75, DATA_AFASTAMENTO = :VALUE76, CAUSA = :VALUE77, ");
                query.Append(" LAUDO = :VALUE78, LAUDO_COMENTARIO = :VALUE79, METODO_OBS = :VALUE80, OBSERVACAO_APARELHO = :VALUE81 ");
                query.Append(" WHERE ID_PRONTUARIO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = prontuario.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = prontuario.Usuario.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = prontuario.Aso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = prontuario.Pressao;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = prontuario.Frequencia;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Numeric));
                command.Parameters[5].Value = prontuario.Peso;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Numeric));
                command.Parameters[6].Value = prontuario.Altura;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Varchar));
                command.Parameters[7].Value = prontuario.Lassegue;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Boolean));
                command.Parameters[8].Value = prontuario.Bebida;

                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Boolean));
                command.Parameters[9].Value = prontuario.Fumo;

                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Boolean));
                command.Parameters[10].Value = prontuario.Diarreia;

                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Boolean));
                command.Parameters[11].Value = prontuario.Tosse;

                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Boolean));
                command.Parameters[12].Value = prontuario.Afastado;

                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Boolean));
                command.Parameters[13].Value = prontuario.Convulsao;

                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Boolean));
                command.Parameters[14].Value = prontuario.Hepatite;

                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Boolean));
                command.Parameters[15].Value = prontuario.Acidente;

                command.Parameters.Add(new NpgsqlParameter("VALUE17", NpgsqlDbType.Boolean));
                command.Parameters[16].Value = prontuario.Tuberculose;

                command.Parameters.Add(new NpgsqlParameter("VALUE18", NpgsqlDbType.Boolean));
                command.Parameters[17].Value = prontuario.Antitetanica;

                command.Parameters.Add(new NpgsqlParameter("VALUE19", NpgsqlDbType.Boolean));
                command.Parameters[18].Value = prontuario.AtividadeFisica;

                command.Parameters.Add(new NpgsqlParameter("VALUE20", NpgsqlDbType.Boolean));
                command.Parameters[19].Value = prontuario.MedoAltura;

                command.Parameters.Add(new NpgsqlParameter("VALUE21", NpgsqlDbType.Boolean));
                command.Parameters[20].Value = prontuario.DoresJuntas;

                command.Parameters.Add(new NpgsqlParameter("VALUE22", NpgsqlDbType.Boolean));
                command.Parameters[21].Value = prontuario.Palpitacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE23", NpgsqlDbType.Boolean));
                command.Parameters[22].Value = prontuario.Tontura;

                command.Parameters.Add(new NpgsqlParameter("VALUE24", NpgsqlDbType.Boolean));
                command.Parameters[23].Value = prontuario.Azia;

                command.Parameters.Add(new NpgsqlParameter("VALUE25", NpgsqlDbType.Boolean));
                command.Parameters[24].Value = prontuario.Desmaio;

                command.Parameters.Add(new NpgsqlParameter("VALUE26", NpgsqlDbType.Boolean));
                command.Parameters[25].Value = prontuario.DoresCabeca;

                command.Parameters.Add(new NpgsqlParameter("VALUE27", NpgsqlDbType.Boolean));
                command.Parameters[26].Value = prontuario.Epilepsia;

                command.Parameters.Add(new NpgsqlParameter("VALUE28", NpgsqlDbType.Boolean));
                command.Parameters[27].Value = prontuario.Insonia;

                command.Parameters.Add(new NpgsqlParameter("VALUE29", NpgsqlDbType.Boolean));
                command.Parameters[28].Value = prontuario.ProblemaAudicao;

                command.Parameters.Add(new NpgsqlParameter("VALUE30", NpgsqlDbType.Boolean));
                command.Parameters[29].Value = prontuario.Tendinite;

                command.Parameters.Add(new NpgsqlParameter("VALUE31", NpgsqlDbType.Boolean));
                command.Parameters[30].Value = prontuario.Fratura;

                command.Parameters.Add(new NpgsqlParameter("VALUE32", NpgsqlDbType.Boolean));
                command.Parameters[31].Value = prontuario.Alergia;

                command.Parameters.Add(new NpgsqlParameter("VALUE33", NpgsqlDbType.Boolean));
                command.Parameters[32].Value = prontuario.Asma;

                command.Parameters.Add(new NpgsqlParameter("VALUE34", NpgsqlDbType.Boolean));
                command.Parameters[33].Value = prontuario.Diabete;

                command.Parameters.Add(new NpgsqlParameter("VALUE35", NpgsqlDbType.Boolean));
                command.Parameters[34].Value = prontuario.Hernia;

                command.Parameters.Add(new NpgsqlParameter("VALUE36", NpgsqlDbType.Boolean));
                command.Parameters[35].Value = prontuario.ManchasPele;

                command.Parameters.Add(new NpgsqlParameter("VALUE37", NpgsqlDbType.Boolean));
                command.Parameters[36].Value = prontuario.PressaoAlta;

                command.Parameters.Add(new NpgsqlParameter("VALUE38", NpgsqlDbType.Boolean));
                command.Parameters[37].Value = prontuario.ProblemaVisao;

                command.Parameters.Add(new NpgsqlParameter("VALUE39", NpgsqlDbType.Boolean));
                command.Parameters[38].Value = prontuario.ProblemaColuna;

                command.Parameters.Add(new NpgsqlParameter("VALUE40", NpgsqlDbType.Boolean));
                command.Parameters[39].Value = prontuario.Rinite;

                command.Parameters.Add(new NpgsqlParameter("VALUE41", NpgsqlDbType.Boolean));
                command.Parameters[40].Value = prontuario.Varize;

                command.Parameters.Add(new NpgsqlParameter("VALUE42", NpgsqlDbType.Boolean));
                command.Parameters[41].Value = prontuario.Oculos;

                command.Parameters.Add(new NpgsqlParameter("VALUE43", NpgsqlDbType.Varchar));
                command.Parameters[42].Value = prontuario.AparelhoMucosa;

                command.Parameters.Add(new NpgsqlParameter("VALUE44", NpgsqlDbType.Varchar));
                command.Parameters[43].Value = prontuario.AparelhoMucosaObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE45", NpgsqlDbType.Varchar));
                command.Parameters[44].Value = prontuario.AparelhoVascular;

                command.Parameters.Add(new NpgsqlParameter("VALUE46", NpgsqlDbType.Varchar));
                command.Parameters[45].Value = prontuario.AparelhoVascularObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE47", NpgsqlDbType.Varchar));
                command.Parameters[46].Value = prontuario.AparelhoRespiratorio;

                command.Parameters.Add(new NpgsqlParameter("VALUE48", NpgsqlDbType.Varchar));
                command.Parameters[47].Value = prontuario.AparelhoRespiratorioObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE49", NpgsqlDbType.Varchar));
                command.Parameters[48].Value = prontuario.AparelhoOlhoDir;

                command.Parameters.Add(new NpgsqlParameter("VALUE50", NpgsqlDbType.Varchar));
                command.Parameters[49].Value = prontuario.AparelhoOlhoDirObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE51", NpgsqlDbType.Varchar));
                command.Parameters[50].Value = prontuario.AparelhoOlhoEsq;

                command.Parameters.Add(new NpgsqlParameter("VALUE52", NpgsqlDbType.Varchar));
                command.Parameters[51].Value = prontuario.AparelhoOlhoEsqObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE53", NpgsqlDbType.Varchar));
                command.Parameters[52].Value = prontuario.AparelhoProtese;

                command.Parameters.Add(new NpgsqlParameter("VALUE54", NpgsqlDbType.Varchar));
                command.Parameters[53].Value = prontuario.AparelhoProteseObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE55", NpgsqlDbType.Varchar));
                command.Parameters[54].Value = prontuario.AparelhoGanglio;

                command.Parameters.Add(new NpgsqlParameter("VALUE56", NpgsqlDbType.Varchar));
                command.Parameters[55].Value = prontuario.AparelhoGanglioObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE57", NpgsqlDbType.Varchar));
                command.Parameters[56].Value = prontuario.AparelhoAbdomen;

                command.Parameters.Add(new NpgsqlParameter("VALUE58", NpgsqlDbType.Varchar));
                command.Parameters[57].Value = prontuario.AparelhoAbdomenObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE59", NpgsqlDbType.Varchar));
                command.Parameters[58].Value = prontuario.AparelhoUrina;

                command.Parameters.Add(new NpgsqlParameter("VALUE60", NpgsqlDbType.Varchar));
                command.Parameters[59].Value = prontuario.AparelhoUrinaObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE61", NpgsqlDbType.Varchar));
                command.Parameters[60].Value = prontuario.AparelhoMembro;

                command.Parameters.Add(new NpgsqlParameter("VALUE62", NpgsqlDbType.Varchar));
                command.Parameters[61].Value = prontuario.AparelhoMembroObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE63", NpgsqlDbType.Varchar));
                command.Parameters[62].Value = prontuario.AparelhoColuna;

                command.Parameters.Add(new NpgsqlParameter("VALUE64", NpgsqlDbType.Varchar));
                command.Parameters[63].Value = prontuario.AparelhoColunaObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE65", NpgsqlDbType.Varchar));
                command.Parameters[64].Value = prontuario.AparelhoEquilibrio;

                command.Parameters.Add(new NpgsqlParameter("VALUE66", NpgsqlDbType.Varchar));
                command.Parameters[65].Value = prontuario.AparelhoEquilibrioObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE67", NpgsqlDbType.Varchar));
                command.Parameters[66].Value = prontuario.AparelhoForca;

                command.Parameters.Add(new NpgsqlParameter("VALUE68", NpgsqlDbType.Varchar));
                command.Parameters[67].Value = prontuario.AparelhoForcaObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE69", NpgsqlDbType.Varchar));
                command.Parameters[68].Value = prontuario.AparelhoMotor;

                command.Parameters.Add(new NpgsqlParameter("VALUE70", NpgsqlDbType.Varchar));
                command.Parameters[69].Value = prontuario.AparelhoMotorObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE71", NpgsqlDbType.Integer));
                command.Parameters[70].Value = prontuario.Parto;

                command.Parameters.Add(new NpgsqlParameter("VALUE72", NpgsqlDbType.Varchar));
                command.Parameters[71].Value = prontuario.Metodo;

                command.Parameters.Add(new NpgsqlParameter("VALUE73", NpgsqlDbType.Date));
                command.Parameters[72].Value = prontuario.DataUltMenstruacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE74", NpgsqlDbType.Boolean));
                command.Parameters[73].Value = prontuario.PreviAfastado;

                command.Parameters.Add(new NpgsqlParameter("VALUE75", NpgsqlDbType.Boolean));
                command.Parameters[74].Value = prontuario.PreviRecurso;

                command.Parameters.Add(new NpgsqlParameter("VALUE76", NpgsqlDbType.Date));
                command.Parameters[75].Value = prontuario.DataRecurso;

                command.Parameters.Add(new NpgsqlParameter("VALUE77", NpgsqlDbType.Text));
                command.Parameters[76].Value = prontuario.Causa;

                command.Parameters.Add(new NpgsqlParameter("VALUE78", NpgsqlDbType.Varchar));
                command.Parameters[77].Value = prontuario.Laudo;

                command.Parameters.Add(new NpgsqlParameter("VALUE79", NpgsqlDbType.Text));
                command.Parameters[78].Value = prontuario.LaudoComentario;

                command.Parameters.Add(new NpgsqlParameter("VALUE80", NpgsqlDbType.Text));
                command.Parameters[79].Value = prontuario.MetodoObs;

                command.Parameters.Add(new NpgsqlParameter("VALUE81", NpgsqlDbType.Text));
                command.Parameters[80].Value = prontuario.TextComentarioAparelho;

                command.ExecuteNonQuery();

                return prontuario;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateProntuario", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Prontuario findProntuarioById(Int64 id, NpgsqlConnection dbConnection)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findProntuarioById");

            Prontuario prontuarioRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_PRONTUARIO, ID_USUARIO, ID_ASO, PRESSAO, FREQUENCIA, PESO, ALTURA, LASSEGUE, BEBIDA, ");
                query.Append(" FUMO, DIARREIA, TOSSE, AFASTADO, CONVULSAO, HEPATITE, ACIDENTE, TUBERCULOSE, ANTITETANICA, ");
                query.Append(" ATIVIDADE_FISICA, MEDO_ALTURA, DORES_JUNTAS, PALPITACAO, TONTURAS, AZIA, DESMAIO, DORES_CABECA, ");
                query.Append(" EPILEPSIA, INSONIA, PROBLEMA_AUDICAO, TENDINITE, FRATURA, ALERGIA, ASMA, DIABETE, HERNIA, ");
                query.Append(" MANCHAS_PELE, PRESSAO_ALTA, PROBLEMA_VISAO, PROBLEMA_COLUNA, RINITE, VARIZES, OCULOS, ");
                query.Append(" APARELHO_MUCOSA, APARELHO_MUCOSA_OBS, APARELHO_VASCULAR, APARELHO_VASCULAR_OBS, APARELHO_RESPIRATORIO, ");
                query.Append(" APARELHO_RESPIRATORIO_OBS, APARELHO_OLHO_DIR, APARELHO_OLHO_DIR_OBS, APARELHO_OLHO_ESQ, APARELHO_OLHO_ESQ_OBS, ");
                query.Append(" APARELHO_PROTESE, APARELHO_PROTESE_OBS, APARELHO_GANGLIO, APARELHO_GANGLIO_OBS,  ");
                query.Append(" APARELHO_ABDOMEN, ");
                query.Append(" APARELHO_ABDOMEN_OBS, APARELHO_URINA, APARELHO_URINA_OBS, APARELHO_MEMBRO, APARELHO_MEMBRO_OBS, ");
                query.Append(" APARELHO_COLUNA, APARELHO_COLUNA_OBS, APARELHO_EQUILIBRIO, APARELHO_EQUILIBRIO_OBS, APARELHO_FORCA, ");
                query.Append(" APARELHO_FORCA_OBS, APARELHO_MOTOR, APARELHO_MOTOR_OBS, PARTO, METODO, DATA_ULT_MENSTRUACAO, ");
                query.Append(" PREVI_AFASTADO, PREVI_RECURSO, DATA_AFASTAMENTO, CAUSA, LAUDO, LAUDO_COMENTARIO, METODO_OBS, OBSERVACAO_APARELHO ");
                query.Append(" FROM SEG_PRONTUARIO");
                query.Append(" WHERE ID_PRONTUARIO = :VALUE1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = id;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    prontuarioRetorno = new Prontuario(
                        dr.GetInt64(0), dr.IsDBNull(1) ? null : new Usuario(dr.GetInt64(1)), new Aso(dr.GetInt64(2)), dr.IsDBNull(3) ? String.Empty : dr.GetString(3),
                        dr.IsDBNull(4) ? String.Empty : dr.GetString(4), dr.GetDecimal(5), dr.GetDecimal(6), dr.IsDBNull(7) ? String.Empty : dr.GetString(7), dr.GetBoolean(8), dr.GetBoolean(9),
                        dr.GetBoolean(10), dr.GetBoolean(11), dr.GetBoolean(12), dr.GetBoolean(13), dr.GetBoolean(14),
                        dr.GetBoolean(15), dr.GetBoolean(16), dr.GetBoolean(17), dr.GetBoolean(18), dr.GetBoolean(19), dr.GetBoolean(20),
                        dr.GetBoolean(21), dr.GetBoolean(22), dr.GetBoolean(23), dr.GetBoolean(24), dr.GetBoolean(25), dr.GetBoolean(26),
                        dr.GetBoolean(27), dr.GetBoolean(28), dr.GetBoolean(29), dr.GetBoolean(30), dr.GetBoolean(31),
                        dr.GetBoolean(32), dr.GetBoolean(33), dr.GetBoolean(34), dr.GetBoolean(35), dr.GetBoolean(36), dr.GetBoolean(37),
                        dr.GetBoolean(38), dr.GetBoolean(39), dr.GetBoolean(40), dr.GetBoolean(41), dr.IsDBNull(42) ? String.Empty : dr.GetString(42), dr.IsDBNull(43) ? String.Empty : dr.GetString(43),
                        dr.IsDBNull(44) ? String.Empty : dr.GetString(44), dr.IsDBNull(45) ? String.Empty : dr.GetString(45), dr.IsDBNull(46) ? String.Empty : dr.GetString(46), dr.IsDBNull(47) ? String.Empty : dr.GetString(47), dr.IsDBNull(48) ? String.Empty : dr.GetString(48), dr.IsDBNull(49) ? String.Empty : dr.GetString(49),
                        dr.IsDBNull(50) ? String.Empty : dr.GetString(50), dr.IsDBNull(51) ? String.Empty : dr.GetString(51), dr.IsDBNull(52) ? String.Empty : dr.GetString(52), dr.IsDBNull(53) ? String.Empty : dr.GetString(53), dr.IsDBNull(54) ? String.Empty : dr.GetString(54),
                        dr.IsDBNull(55) ? String.Empty : dr.GetString(55), dr.IsDBNull(56) ? String.Empty : dr.GetString(56), dr.IsDBNull(57) ? String.Empty : dr.GetString(57),
                        dr.IsDBNull(58) ? String.Empty : dr.GetString(58), dr.IsDBNull(59) ? String.Empty : dr.GetString(59), dr.IsDBNull(60) ? String.Empty : dr.GetString(60), dr.IsDBNull(61) ? String.Empty : dr.GetString(61), dr.IsDBNull(62) ? String.Empty : dr.GetString(62), dr.IsDBNull(63) ? String.Empty : dr.GetString(63),
                        dr.IsDBNull(64) ? String.Empty : dr.GetString(64), dr.IsDBNull(65) ? String.Empty : dr.GetString(65), dr.IsDBNull(66) ? String.Empty : dr.GetString(66), dr.IsDBNull(67) ? String.Empty : dr.GetString(67), dr.IsDBNull(68) ? String.Empty : dr.GetString(68), dr.IsDBNull(69) ? String.Empty : dr.GetString(69),
                        dr.GetInt32(70), dr.IsDBNull(71) ? String.Empty : dr.GetString(71), dr.IsDBNull(72) ? (DateTime?)null : dr.GetDateTime(72), dr.GetBoolean(73), dr.GetBoolean(74), dr.IsDBNull(75) ? (DateTime?)null : dr.GetDateTime(75),
                        dr.IsDBNull(76) ? String.Empty : dr.GetString(76), dr.IsDBNull(77) ? String.Empty : dr.GetString(77), dr.IsDBNull(78) ? String.Empty : dr.GetString(78), dr.IsDBNull(79) ? String.Empty : dr.GetString(79), dr.IsDBNull(80) ? String.Empty : dr.GetString(80));

                }

                return prontuarioRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findProntuarioById", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public Prontuario findProntuarioByAso(Aso aso, NpgsqlConnection dbConnection)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findProntuarioByAso");

            Prontuario prontuarioRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_PRONTUARIO, ID_USUARIO, ID_ASO, PRESSAO, FREQUENCIA, PESO, ALTURA, LASSEGUE, BEBIDA, ");
                query.Append(" FUMO, DIARREIA, TOSSE, AFASTADO, CONVULSAO, HEPATITE, ACIDENTE, TUBERCULOSE, ANTITETANICA, ");
                query.Append(" ATIVIDADE_FISICA, MEDO_ALTURA, DORES_JUNTAS, PALPITACAO, TONTURAS, AZIA, DESMAIO, DORES_CABECA, ");
                query.Append(" EPILEPSIA, INSONIA, PROBLEMA_AUDICAO, TENDINITE, FRATURA, ALERGIA, ASMA, DIABETE, HERNIA, ");
                query.Append(" MANCHAS_PELE, PRESSAO_ALTA, PROBLEMA_VISAO, PROBLEMA_COLUNA, RINITE, VARIZES, OCULOS, ");
                query.Append(" APARELHO_MUCOSA, APARELHO_MUCOSA_OBS, APARELHO_VASCULAR, APARELHO_VASCULAR_OBS, APARELHO_RESPIRATORIO, ");
                query.Append(" APARELHO_RESPIRATORIO_OBS, APARELHO_OLHO_DIR, APARELHO_OLHO_DIR_OBS, APARELHO_OLHO_ESQ, APARELHO_OLHO_ESQ_OBS, ");
                query.Append(" APARELHO_PROTESE, APARELHO_PROTESE_OBS, APARELHO_GANGLIO, APARELHO_GANGLIO_OBS,  ");
                query.Append(" APARELHO_ABDOMEN, ");
                query.Append(" APARELHO_ABDOMEN_OBS, APARELHO_URINA, APARELHO_URINA_OBS, APARELHO_MEMBRO, APARELHO_MEMBRO_OBS, ");
                query.Append(" APARELHO_COLUNA, APARELHO_COLUNA_OBS, APARELHO_EQUILIBRIO, APARELHO_EQUILIBRIO_OBS, APARELHO_FORCA, ");
                query.Append(" APARELHO_FORCA_OBS, APARELHO_MOTOR, APARELHO_MOTOR_OBS, PARTO, METODO, DATA_ULT_MENSTRUACAO, ");
                query.Append(" PREVI_AFASTADO, PREVI_RECURSO, DATA_AFASTAMENTO, CAUSA, LAUDO, LAUDO_COMENTARIO, METODO_OBS, OBSERVACAO_APARELHO ");
                query.Append(" FROM SEG_PRONTUARIO");
                query.Append(" WHERE ID_ASO = :VALUE1");
                

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = aso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    prontuarioRetorno = new Prontuario(
                        dr.GetInt64(0), dr.IsDBNull(1) ? null : new Usuario(dr.GetInt64(1)), new Aso(dr.GetInt64(2)), dr.IsDBNull(3) ? String.Empty : dr.GetString(3),
                        dr.IsDBNull(4) ? String.Empty : dr.GetString(4), dr.IsDBNull(5) ? (Decimal?)null : dr.GetDecimal(5), dr.IsDBNull(6) ? (Decimal?)null : dr.GetDecimal(6), dr.IsDBNull(7) ? String.Empty : dr.GetString(7), dr.GetBoolean(8), dr.GetBoolean(9),
                        dr.GetBoolean(10), dr.GetBoolean(11), dr.GetBoolean(12), dr.GetBoolean(13), dr.GetBoolean(14),
                        dr.GetBoolean(15), dr.GetBoolean(16), dr.GetBoolean(17), dr.GetBoolean(18), dr.GetBoolean(19), dr.GetBoolean(20),
                        dr.GetBoolean(21), dr.GetBoolean(22), dr.GetBoolean(23), dr.GetBoolean(24), dr.GetBoolean(25), dr.GetBoolean(26),
                        dr.GetBoolean(27), dr.GetBoolean(28), dr.GetBoolean(29), dr.GetBoolean(30), dr.GetBoolean(31),
                        dr.GetBoolean(32), dr.GetBoolean(33), dr.GetBoolean(34), dr.GetBoolean(35), dr.GetBoolean(36), dr.GetBoolean(37),
                        dr.GetBoolean(38), dr.GetBoolean(39), dr.GetBoolean(40), dr.GetBoolean(41), dr.IsDBNull(42) ? String.Empty : dr.GetString(42), dr.IsDBNull(43) ? String.Empty : dr.GetString(43),
                        dr.IsDBNull(44) ? String.Empty : dr.GetString(44), dr.IsDBNull(45) ? String.Empty : dr.GetString(45), dr.IsDBNull(46) ? String.Empty : dr.GetString(46), dr.IsDBNull(47) ? String.Empty : dr.GetString(47), dr.IsDBNull(48) ? String.Empty : dr.GetString(48), dr.IsDBNull(49) ? String.Empty : dr.GetString(49),
                        dr.IsDBNull(50) ? String.Empty : dr.GetString(50), dr.IsDBNull(51) ? String.Empty : dr.GetString(51), dr.IsDBNull(52) ? String.Empty : dr.GetString(52), dr.IsDBNull(53) ? String.Empty : dr.GetString(53), dr.IsDBNull(54) ? String.Empty : dr.GetString(54), 
                        dr.IsDBNull(55) ? String.Empty : dr.GetString(55), dr.IsDBNull(56) ? String.Empty : dr.GetString(56), dr.IsDBNull(57) ? String.Empty : dr.GetString(57),
                        dr.IsDBNull(58) ? String.Empty : dr.GetString(58), dr.IsDBNull(59) ? String.Empty : dr.GetString(59), dr.IsDBNull(60) ? String.Empty : dr.GetString(60), dr.IsDBNull(61) ? String.Empty : dr.GetString(61), dr.IsDBNull(62) ? String.Empty : dr.GetString(62), dr.IsDBNull(63) ? String.Empty : dr.GetString(63),
                        dr.IsDBNull(64) ? String.Empty : dr.GetString(64), dr.IsDBNull(65) ? String.Empty : dr.GetString(65), dr.IsDBNull(66) ? String.Empty : dr.GetString(66), dr.IsDBNull(67) ? String.Empty : dr.GetString(67), dr.IsDBNull(68) ? String.Empty : dr.GetString(68), dr.IsDBNull(69) ? String.Empty : dr.GetString(69),
                        dr.GetInt32(70), dr.IsDBNull(71) ? String.Empty : dr.GetString(71), dr.IsDBNull(72) ? (DateTime?)null : dr.GetDateTime(72), dr.GetBoolean(73), dr.GetBoolean(74), dr.IsDBNull(75) ? (DateTime?)null : dr.GetDateTime(75),
                        dr.IsDBNull(76) ? String.Empty : dr.GetString(76), dr.IsDBNull(77) ? String.Empty : dr.GetString(77), dr.IsDBNull(78) ? String.Empty : dr.GetString(78), dr.IsDBNull(79) ? String.Empty : dr.GetString(79), dr.IsDBNull(80) ? String.Empty : dr.GetString(80));
                }

                return prontuarioRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findProntuarioByAso", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }


        public Prontuario findLastProntuarioByFuncionario( Funcionario funcionario, NpgsqlConnection dbConnection)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findLastProntuarioByFuncionario");

            Prontuario lastProntuario = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT PRONTUARIO.ID_PRONTUARIO, PRONTUARIO.ID_USUARIO, PRONTUARIO.ID_ASO, PRONTUARIO.PRESSAO, PRONTUARIO.FREQUENCIA, PRONTUARIO.PESO, PRONTUARIO.ALTURA, PRONTUARIO.LASSEGUE, PRONTUARIO.BEBIDA, ");
                query.Append(" PRONTUARIO.FUMO, PRONTUARIO.DIARREIA, PRONTUARIO.TOSSE, PRONTUARIO.AFASTADO, PRONTUARIO.CONVULSAO, PRONTUARIO.HEPATITE, PRONTUARIO.ACIDENTE, PRONTUARIO.TUBERCULOSE, PRONTUARIO.ANTITETANICA, ");
                query.Append(" PRONTUARIO.ATIVIDADE_FISICA, PRONTUARIO.MEDO_ALTURA, PRONTUARIO.DORES_JUNTAS, PRONTUARIO.PALPITACAO, PRONTUARIO.TONTURAS, PRONTUARIO.AZIA, PRONTUARIO.DESMAIO, PRONTUARIO.DORES_CABECA, ");
                query.Append(" PRONTUARIO.EPILEPSIA, PRONTUARIO.INSONIA, PRONTUARIO.PROBLEMA_AUDICAO, PRONTUARIO.TENDINITE, PRONTUARIO.FRATURA, PRONTUARIO.ALERGIA, PRONTUARIO.ASMA, PRONTUARIO.DIABETE, PRONTUARIO.HERNIA, ");
                query.Append(" PRONTUARIO.MANCHAS_PELE, PRONTUARIO.PRESSAO_ALTA, PRONTUARIO.PROBLEMA_VISAO, PRONTUARIO.PROBLEMA_COLUNA, PRONTUARIO.RINITE, PRONTUARIO.VARIZES, PRONTUARIO.OCULOS, ");
                query.Append(" PRONTUARIO.APARELHO_MUCOSA, PRONTUARIO.APARELHO_MUCOSA_OBS, PRONTUARIO.APARELHO_VASCULAR, PRONTUARIO.APARELHO_VASCULAR_OBS, PRONTUARIO.APARELHO_RESPIRATORIO, ");
                query.Append(" PRONTUARIO.APARELHO_RESPIRATORIO_OBS, PRONTUARIO.APARELHO_OLHO_DIR, PRONTUARIO.APARELHO_OLHO_DIR_OBS, PRONTUARIO.APARELHO_OLHO_ESQ, PRONTUARIO.APARELHO_OLHO_ESQ_OBS, ");
                query.Append(" PRONTUARIO.APARELHO_PROTESE, PRONTUARIO.APARELHO_PROTESE_OBS, PRONTUARIO.APARELHO_GANGLIO, PRONTUARIO.APARELHO_GANGLIO_OBS,  ");
                query.Append(" PRONTUARIO.APARELHO_ABDOMEN, ");
                query.Append(" PRONTUARIO.APARELHO_ABDOMEN_OBS, PRONTUARIO.APARELHO_URINA, PRONTUARIO.APARELHO_URINA_OBS, PRONTUARIO.APARELHO_MEMBRO, PRONTUARIO.APARELHO_MEMBRO_OBS, ");
                query.Append(" PRONTUARIO.APARELHO_COLUNA, PRONTUARIO.APARELHO_COLUNA_OBS, PRONTUARIO.APARELHO_EQUILIBRIO, PRONTUARIO.APARELHO_EQUILIBRIO_OBS, PRONTUARIO.APARELHO_FORCA, ");
                query.Append(" PRONTUARIO.APARELHO_FORCA_OBS, PRONTUARIO.APARELHO_MOTOR, PRONTUARIO.APARELHO_MOTOR_OBS, PRONTUARIO.PARTO, PRONTUARIO.METODO, PRONTUARIO.DATA_ULT_MENSTRUACAO, ");
                query.Append(" PRONTUARIO.PREVI_AFASTADO, PRONTUARIO.PREVI_RECURSO, PRONTUARIO.DATA_AFASTAMENTO, PRONTUARIO.CAUSA, PRONTUARIO.LAUDO, PRONTUARIO.LAUDO_COMENTARIO, PRONTUARIO.METODO_OBS, PRONTUARIO.OBSERVACAO_APARELHO ");
                query.Append(" FROM SEG_PRONTUARIO PRONTUARIO");
                query.Append(" JOIN SEG_ASO ATENDIMENTO ON ATENDIMENTO.ID_ASO = PRONTUARIO.ID_ASO");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CFF ON CFF.ID_CLIENTE_FUNCAO_FUNCIONARIO = ATENDIMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                query.Append(" JOIN SEG_FUNCIONARIO FUNCIONARIO ON FUNCIONARIO.ID_FUNCIONARIO = CFF.ID_FUNCIONARIO ");
                query.Append(" WHERE FUNCIONARIO.ID_FUNCIONARIO = :VALUE1 ");
                query.Append(" ORDER BY ATENDIMENTO.DATA_ASO DESC LIMIT 1 ");


                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = funcionario.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    lastProntuario = new Prontuario(
                        dr.GetInt64(0), dr.IsDBNull(1) ? null : new Usuario(dr.GetInt64(1)), new Aso(dr.GetInt64(2)), dr.IsDBNull(3) ? String.Empty : dr.GetString(3),
                        dr.IsDBNull(4) ? String.Empty : dr.GetString(4), dr.IsDBNull(5) ? null : (decimal?)dr.GetDecimal(5), dr.IsDBNull(6) ? null : (decimal?)dr.GetDecimal(6), dr.IsDBNull(7) ? String.Empty : dr.GetString(7), dr.GetBoolean(8), dr.GetBoolean(9),
                        dr.GetBoolean(10), dr.GetBoolean(11), dr.GetBoolean(12), dr.GetBoolean(13), dr.GetBoolean(14),
                        dr.GetBoolean(15), dr.GetBoolean(16), dr.GetBoolean(17), dr.GetBoolean(18), dr.GetBoolean(19), dr.GetBoolean(20),
                        dr.GetBoolean(21), dr.GetBoolean(22), dr.GetBoolean(23), dr.GetBoolean(24), dr.GetBoolean(25), dr.GetBoolean(26),
                        dr.GetBoolean(27), dr.GetBoolean(28), dr.GetBoolean(29), dr.GetBoolean(30), dr.GetBoolean(31),
                        dr.GetBoolean(32), dr.GetBoolean(33), dr.GetBoolean(34), dr.GetBoolean(35), dr.GetBoolean(36), dr.GetBoolean(37),
                        dr.GetBoolean(38), dr.GetBoolean(39), dr.GetBoolean(40), dr.GetBoolean(41), dr.IsDBNull(42) ? String.Empty : dr.GetString(42), dr.IsDBNull(43) ? String.Empty : dr.GetString(43),
                        dr.IsDBNull(44) ? String.Empty : dr.GetString(44), dr.IsDBNull(45) ? String.Empty : dr.GetString(45), dr.IsDBNull(46) ? String.Empty : dr.GetString(46), dr.IsDBNull(47) ? String.Empty : dr.GetString(47), dr.IsDBNull(48) ? String.Empty : dr.GetString(48), dr.IsDBNull(49) ? String.Empty : dr.GetString(49),
                        dr.IsDBNull(50) ? String.Empty : dr.GetString(50), dr.IsDBNull(51) ? String.Empty : dr.GetString(51), dr.IsDBNull(52) ? String.Empty : dr.GetString(52), dr.IsDBNull(53) ? String.Empty : dr.GetString(53), dr.IsDBNull(54) ? String.Empty : dr.GetString(54),
                        dr.IsDBNull(55) ? String.Empty : dr.GetString(55), dr.IsDBNull(56) ? String.Empty : dr.GetString(56), dr.IsDBNull(57) ? String.Empty : dr.GetString(57),
                        dr.IsDBNull(58) ? String.Empty : dr.GetString(58), dr.IsDBNull(59) ? String.Empty : dr.GetString(59), dr.IsDBNull(60) ? String.Empty : dr.GetString(60), dr.IsDBNull(61) ? String.Empty : dr.GetString(61), dr.IsDBNull(62) ? String.Empty : dr.GetString(62), dr.IsDBNull(63) ? String.Empty : dr.GetString(63),
                        dr.IsDBNull(64) ? String.Empty : dr.GetString(64), dr.IsDBNull(65) ? String.Empty : dr.GetString(65), dr.IsDBNull(66) ? String.Empty : dr.GetString(66), dr.IsDBNull(67) ? String.Empty : dr.GetString(67), dr.IsDBNull(68) ? String.Empty : dr.GetString(68), dr.IsDBNull(69) ? String.Empty : dr.GetString(69),
                        dr.GetInt32(70), dr.IsDBNull(71) ? String.Empty : dr.GetString(71), dr.IsDBNull(72) ? (DateTime?)null : dr.GetDateTime(72), dr.GetBoolean(73), dr.GetBoolean(74), dr.IsDBNull(75)? (DateTime?)null : dr.GetDateTime(75),
                        dr.IsDBNull(76) ? String.Empty : dr.GetString(76), dr.IsDBNull(77) ? String.Empty : dr.GetString(77), dr.IsDBNull(78) ? String.Empty : dr.GetString(78), dr.IsDBNull(79) ? String.Empty : dr.GetString(79), dr.IsDBNull(80) ? String.Empty : dr.GetString(80));
                }

                return lastProntuario;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findLastProntuarioByFuncionario", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }


    }
}
