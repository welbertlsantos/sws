﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using System.Data;
using SWS.Helper;
using Npgsql;
using NpgsqlTypes;
using SWS.Excecao;
using SWS.Entidade;

namespace SWS.Dao
{
    class GheSetorClienteFuncaoExameDao : IGheSetorClienteFuncaoExameDao
    {
        public DataSet findAllFuncaoNotInGheSetorClienteFuncaoExame(Exame exame, Ghe ghe, Funcao funcao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllFuncaoNotInGheSetorClienteFuncaoExame");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT DISTINCT FUNCAO.ID_FUNCAO, FUNCAO.DESCRICAO, FALSE ");
                query.Append(" FROM SEG_GHE_SETOR_CLIENTE_FUNCAO GHE_SETOR_CLIENTE_FUNCAO ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = GHE_SETOR_CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO");
                query.Append(" JOIN SEG_GHE_SETOR GHE_SETOR ON GHE_SETOR.ID_GHE_SETOR = GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR ");
                query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTE_FUNCAO.ID_FUNCAO ");
                query.Append(" WHERE GHE_SETOR.ID_GHE = :VALUE2 ");
                query.Append(" AND GHE_SETOR_CLIENTE_FUNCAO.SITUACAO = 'A' ");
                
                if (!String.IsNullOrEmpty(funcao.Descricao))
                    query.Append(" AND FUNCAO.DESCRICAO LIKE :VALUE3 ");

                if (!String.IsNullOrEmpty(funcao.CodCbo))
                    query.Append(" AND FUNCAO.COD_CBO = :VALUE4 ");

                query.Append(" AND GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR_CLIENTE_FUNCAO NOT IN ");
                query.Append(" (SELECT GHE_SETOR_CLIENTE_FUNCAO_EXAME.ID_GHE_SETOR_CLIENTE_FUNCAO  ");
                query.Append(" FROM SEG_GHE_SETOR_CLIENTE_FUNCAO_EXAME GHE_SETOR_CLIENTE_FUNCAO_EXAME ");
                query.Append(" JOIN SEG_GHE_SETOR_CLIENTE_FUNCAO GHE_SETOR_CLIENTE_FUNCAO ON GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR_CLIENTE_FUNCAO = GHE_SETOR_CLIENTE_FUNCAO_EXAME.ID_GHE_SETOR_CLIENTE_FUNCAO");
                query.Append(" JOIN SEG_GHE_SETOR GHE_SETOR ON GHE_SETOR.ID_GHE_SETOR = GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR ");
                query.Append(" WHERE  GHE_SETOR_CLIENTE_FUNCAO_EXAME.ID_EXAME = :VALUE1 ");
                query.Append(" AND GHE_SETOR.ID_GHE = :VALUE2) ORDER BY FUNCAO.DESCRICAO ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = exame.Id;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[1].Value = ghe.Id;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[2].Value = funcao.Descricao + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[3].Value = funcao.CodCbo;
                
                DataSet ds = new DataSet();

                adapter.Fill(ds, "Funcoes");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllFuncaoNotInGheSetorClienteFuncaoExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<GheSetorClienteFuncaoExame> findByGheAndFuncao(Ghe ghe, Funcao funcao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGheSetorClienteFuncaoExameByGheAndFuncao");

            try
            {
                List<GheSetorClienteFuncaoExame> gheSetorClienteFuncaoExame = new List<GheSetorClienteFuncaoExame>();

                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE_SETOR_CLIENTE_FUNCAO_EXAME.ID_GHE_SETOR_CLIENTE_FUNCAO, GHE_SETOR_CLIENTE_FUNCAO_EXAME.ID_EXAME, GHE_SETOR_CLIENTE_FUNCAO_EXAME.IDADE, GHE_SETOR_CLIENTE_FUNCAO_EXAME.ID_GHE_FONTE_AGENTE_EXAME, COMENTARIO_FUNCAO.ID_COMENTARIO_FUNCAO, COMENTARIO_FUNCAO.ID_FUNCAO, COMENTARIO_FUNCAO.ATIVIDADE, COMENTARIO_FUNCAO.SITUACAO, GHE_SETOR_CLIENTE_FUNCAO.ELETRICIDADE ");
                query.Append(" FROM SEG_GHE_SETOR_CLIENTE_FUNCAO_EXAME GHE_SETOR_CLIENTE_FUNCAO_EXAME");
                query.Append(" JOIN SEG_GHE_SETOR_CLIENTE_FUNCAO GHE_SETOR_CLIENTE_FUNCAO ON GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR_CLIENTE_FUNCAO = GHE_SETOR_CLIENTE_FUNCAO_EXAME.ID_GHE_SETOR_CLIENTE_FUNCAO ");
                query.Append(" JOIN SEG_GHE_SETOR GHE_SETOR ON GHE_SETOR.ID_GHE_SETOR = GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = GHE_SETOR_CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO ");
                query.Append(" LEFT JOIN SEG_COMENTARIO_FUNCAO COMENTARIO_FUNCAO ON COMENTARIO_FUNCAO.ID_COMENTARIO_FUNCAO = GHE_SETOR_CLIENTE_FUNCAO.ID_COMENTARIO_FUNCAO AND COMENTARIO_FUNCAO.SITUACAO = 'A' ");
                query.Append(" WHERE ");
                query.Append(" CLIENTE_FUNCAO.ID_FUNCAO = :VALUE1 ");
                query.Append(" AND GHE_SETOR.ID_GHE = :VALUE2 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = funcao.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = ghe.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gheSetorClienteFuncaoExame.Add(new GheSetorClienteFuncaoExame(new GheSetorClienteFuncao(dr.GetInt64(0), null, null, false, false, String.Empty, dr.IsDBNull(4) ? null : new ComentarioFuncao(dr.GetInt64(4), dr.IsDBNull(5) ? null : new Funcao(dr.GetInt64(5), string.Empty, string.Empty, string.Empty, string.Empty), dr.IsDBNull(6) ? string.Empty : dr.GetString(6), dr.IsDBNull(7) ? string.Empty : dr.GetString(7)), dr.GetBoolean(8)), new Exame(dr.GetInt64(1)), dr.GetInt32(2), new GheFonteAgenteExame(dr.GetInt64(3))));
                }

                return gheSetorClienteFuncaoExame;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGheSetorClienteFuncaoExameByGheAndFuncao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public GheSetorClienteFuncaoExame insert(GheSetorClienteFuncaoExame gheSetorClienteFuncaoExame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_GHE_SETOR_CLIENTE_FUNCAO_EXAME ");
                query.Append(" (ID_GHE_SETOR_CLIENTE_FUNCAO, ID_EXAME, IDADE, ID_GHE_FONTE_AGENTE_EXAME ) ");
                query.Append(" VALUES ( :VALUE1, :VALUE2, :VALUE3, :VALUE4 ) ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheSetorClienteFuncaoExame.GheSetorClienteFuncao.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = gheSetorClienteFuncaoExame.Exame.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = gheSetorClienteFuncaoExame.Idade;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = gheSetorClienteFuncaoExame.GheFonteAgenteExame.Id;

                command.ExecuteNonQuery();

                return gheSetorClienteFuncaoExame;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Dictionary<Int64,Dictionary<Funcao, Int32>> findAllByGheAndExame(Ghe ghe, Exame exame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByGheAndExame");

            Dictionary<Int64, Dictionary<Funcao, Int32>> funcoesExcecao = new Dictionary<Int64, Dictionary<Funcao, Int32>>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT DISTINCT FUNCAO.ID_FUNCAO, FUNCAO.DESCRICAO, GHE_SETOR_CLIENTE_FUNCAO_EXAME.IDADE  ");
                query.Append(" FROM SEG_GHE_SETOR_CLIENTE_FUNCAO_EXAME GHE_SETOR_CLIENTE_FUNCAO_EXAME ");
                query.Append(" JOIN SEG_GHE_SETOR_CLIENTE_FUNCAO GHE_SETOR_CLIENTE_FUNCAO ON GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR_CLIENTE_FUNCAO = GHE_SETOR_CLIENTE_FUNCAO_EXAME.ID_GHE_SETOR_CLIENTE_FUNCAO ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = GHE_SETOR_CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO ");
                query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTE_FUNCAO.ID_FUNCAO ");
                query.Append(" JOIN SEG_GHE_SETOR GHE_SETOR ON GHE_SETOR.ID_GHE_SETOR = GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR ");
                query.Append(" WHERE  GHE_SETOR_CLIENTE_FUNCAO_EXAME.ID_EXAME = :VALUE1 ");
                query.Append(" AND GHE_SETOR_CLIENTE_FUNCAO.SITUACAO = 'A' ");
                query.Append(" AND GHE_SETOR.ID_GHE = :VALUE2 ORDER BY FUNCAO.DESCRICAO ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = exame.Id;

                command.Parameters.Add(new NpgsqlParameter("value2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = ghe.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    funcoesExcecao.Add(dr.GetInt64(0), new Dictionary<Funcao, Int32>() { { new Funcao(dr.GetInt64(0), dr.GetString(1), String.Empty, String.Empty, String.Empty), dr.GetInt32(2) } });
                }

                return funcoesExcecao;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByGheAndExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void deleteByGheAndExame(Ghe ghe, Exame exame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteByGheAndExame");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" DELETE FROM SEG_GHE_SETOR_CLIENTE_FUNCAO_EXAME GHE_SETOR_CLIENTE_FUNCAO_EXAME WHERE GHE_SETOR_CLIENTE_FUNCAO_EXAME.ID_GHE_SETOR_CLIENTE_FUNCAO IN ( ");
                query.Append(" SELECT GHE_SETOR_CLIENTE_FUNCAO_EXAME.ID_GHE_SETOR_CLIENTE_FUNCAO FROM SEG_GHE_SETOR_CLIENTE_FUNCAO_EXAME GHE_SETOR_CLIENTE_FUNCAO_EXAME ");
                query.Append(" JOIN SEG_GHE_SETOR_CLIENTE_FUNCAO GHE_SETOR_CLIENTE_FUNCAO ON GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR_CLIENTE_FUNCAO = GHE_SETOR_CLIENTE_FUNCAO_EXAME.ID_GHE_SETOR_CLIENTE_FUNCAO ");
                query.Append(" JOIN SEG_GHE_SETOR GHE_SETOR ON GHE_SETOR.ID_GHE_SETOR = GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR ");
                query.Append(" WHERE GHE_SETOR.ID_GHE = :VALUE1 ");
                query.Append(" AND GHE_SETOR_CLIENTE_FUNCAO_EXAME.ID_EXAME = :VALUE2 ) AND GHE_SETOR_CLIENTE_FUNCAO_EXAME.ID_EXAME = :VALUE2 ");
                 
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = ghe.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = exame.Id;

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteByGheAndExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void deleteByGheAndExameAndFuncao(Ghe ghe, Exame exame, Funcao funcao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteByGheAndExameAndFuncao");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" DELETE FROM SEG_GHE_SETOR_CLIENTE_FUNCAO_EXAME GHE_SETOR_CLIENTE_FUNCAO_EXAME WHERE GHE_SETOR_CLIENTE_FUNCAO_EXAME.ID_GHE_SETOR_CLIENTE_FUNCAO IN ( ");
                query.Append(" SELECT GHE_SETOR_CLIENTE_FUNCAO_EXAME.ID_GHE_SETOR_CLIENTE_FUNCAO FROM SEG_GHE_SETOR_CLIENTE_FUNCAO_EXAME GHE_SETOR_CLIENTE_FUNCAO_EXAME ");
                query.Append(" JOIN SEG_GHE_SETOR_CLIENTE_FUNCAO GHE_SETOR_CLIENTE_FUNCAO ON GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR_CLIENTE_FUNCAO = GHE_SETOR_CLIENTE_FUNCAO_EXAME.ID_GHE_SETOR_CLIENTE_FUNCAO ");
                query.Append(" JOIN SEG_GHE_SETOR GHE_SETOR ON GHE_SETOR.ID_GHE_SETOR = GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = GHE_SETOR_CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO ");
                query.Append(" WHERE GHE_SETOR.ID_GHE = :VALUE1 ");
                query.Append(" AND GHE_SETOR_CLIENTE_FUNCAO_EXAME.ID_EXAME = :VALUE2 AND CLIENTE_FUNCAO.ID_FUNCAO = :VALUE3 ) AND GHE_SETOR_CLIENTE_FUNCAO_EXAME.ID_EXAME = :VALUE2 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = ghe.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = exame.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = funcao.Id;

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteByGheAndExameAndFuncao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public GheSetorClienteFuncaoExame update(GheSetorClienteFuncaoExame gheSetorClienteFuncaoExame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_GHE_SETOR_CLIENTE_FUNCAO_EXAME SET IDADE = :VALUE1 WHERE ID_GHE_SETOR_CLIENTE_FUNCAO = :VALUE2 AND ID_EXAME = :VALUE3 ");
                

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheSetorClienteFuncaoExame.Idade;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = gheSetorClienteFuncaoExame.GheSetorClienteFuncao.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = gheSetorClienteFuncaoExame.Exame.Id;

                command.ExecuteNonQuery();

                return gheSetorClienteFuncaoExame;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<GheSetorClienteFuncaoExame> findAllByGheSetorClienteFuncao(GheSetorClienteFuncao gheSetorClienteFuncao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByGheSetorClienteFuncao");

            try
            {
                List<GheSetorClienteFuncaoExame> gheSetorClienteFuncaoExames = new List<GheSetorClienteFuncaoExame>();

                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE_SETOR_CLIENTE_FUNCAO_EXAME.ID_GHE_SETOR_CLIENTE_FUNCAO, GHE_SETOR_CLIENTE_FUNCAO_EXAME.ID_EXAME, GHE_SETOR_CLIENTE_FUNCAO_EXAME.ID_GHE_FONTE_AGENTE_EXAME, GHE_SETOR_CLIENTE_FUNCAO_EXAME.IDADE ");
                query.Append(" FROM SEG_GHE_SETOR_CLIENTE_FUNCAO_EXAME GHE_SETOR_CLIENTE_FUNCAO_EXAME");
                query.Append(" WHERE GHE_SETOR_CLIENTE_FUNCAO_EXAME.ID_GHE_SETOR_CLIENTE_FUNCAO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheSetorClienteFuncao.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    GheSetorClienteFuncaoExame gheSetorClienteFuncaoExame = new GheSetorClienteFuncaoExame();
                    gheSetorClienteFuncaoExame.GheSetorClienteFuncao = gheSetorClienteFuncao;
                    Exame exame = new Exame();
                    exame.Id = dr.GetInt64(1);
                    gheSetorClienteFuncaoExame.Exame = exame;
                    GheFonteAgenteExame gheFonteAgenteExame = new GheFonteAgenteExame();
                    gheFonteAgenteExame.Id = dr.GetInt64(2);
                    gheSetorClienteFuncaoExame.GheFonteAgenteExame = gheFonteAgenteExame;

                    gheSetorClienteFuncaoExames.Add(gheSetorClienteFuncaoExame);
                }

                return gheSetorClienteFuncaoExames;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByGheSetorClienteFuncao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
