﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SWS.Entidade;
using Npgsql;
using SWS.Helper;
using NpgsqlTypes;
using SWS.Excecao;
using SWS.IDao;

namespace SWS.Dao
{
    class RamoDao : IRamoDao
    {
        public DataSet findRamoByFilter(Ramo ramo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findRamoByFilter");

            try
            {
                StringBuilder query = new StringBuilder();

                if (ramo.isNullForFilter())
                {
                    query.Append(" SELECT ID, DESCRICAO, SITUACAO FROM SEG_RAMO ORDER BY DESCRICAO ASC");
                }
                else
                {
                    query.Append(" SELECT ID, DESCRICAO, SITUACAO FROM SEG_RAMO WHERE TRUE ");

                    if (!String.IsNullOrEmpty(ramo.Descricao))
                        query.Append (" AND DESCRICAO LIKE :VALUE1 ");

                    if (!String.IsNullOrEmpty(ramo.Situacao))
                        query.Append(" AND SITUACAO = :VALUE2 ");

                    query.Append ("ORDER BY DESCRICAO ASC ");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));

                adapter.SelectCommand.Parameters[0].Value = String.IsNullOrEmpty(ramo.Descricao) ? String.Empty : ramo.Descricao + "%";
                adapter.SelectCommand.Parameters[1].Value = String.IsNullOrEmpty(ramo.Situacao) ? String.Empty : ramo.Situacao;

                DataSet ds = new DataSet();
                adapter.Fill(ds, "Ramos");
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findRamoByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_RAMO_ID_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Ramo insert(Ramo ramo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            try
            {
                ramo.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_RAMO( ID, DESCRICAO, SITUACAO )");
                query.Append(" VALUES ( :VALUE1, :VALUE2, :VALUE3 )");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));


                command.Parameters[0].Value = ramo.Id;
                command.Parameters[1].Value = ramo.Descricao;
                command.Parameters[2].Value = ramo.Situacao;
                
                command.ExecuteNonQuery();

                return ramo;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Ramo findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            Ramo ramo = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID, DESCRICAO, SITUACAO FROM SEG_RAMO WHERE ID = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    ramo = new Ramo(dr.GetInt64(0), dr.GetString(1), dr.GetString(2));
                }

                return ramo;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Ramo update(Ramo ramo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_RAMO SET DESCRICAO = :VALUE2, SITUACAO = :VALUE3 WHERE ID = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value2", NpgsqlDbType.Varchar));
                command.Parameters.Add(new NpgsqlParameter("value3", NpgsqlDbType.Varchar));

                command.Parameters[0].Value = ramo.Id;
                command.Parameters[1].Value = ramo.Descricao;
                command.Parameters[2].Value = ramo.Situacao;

                command.ExecuteNonQuery();

                return ramo;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateAgente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Ramo findByDescricao(String value, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByDescricao");

            try
            {
                StringBuilder query = new StringBuilder();

                Ramo ramo = null;
                
                query.Append(" SELECT ID, DESCRICAO, SITUACAO FROM SEG_RAMO WHERE DESCRICAO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Varchar));

                command.Parameters[0].Value = value;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    ramo = new Ramo(dr.GetInt64(0), dr.GetString(1), dr.GetString(2));
                 
                }

                return ramo;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByDescricao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }



    }
}
