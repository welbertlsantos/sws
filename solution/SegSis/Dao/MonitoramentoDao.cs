﻿using Npgsql;
using NpgsqlTypes;
using SWS.Entidade;
using SWS.Excecao;
using SWS.Helper;
using SWS.IDao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Dao
{
    class MonitoramentoDao : IMonitoramentoDao
    {
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_MONITORAMENTO_ID_MONITORAMENTO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Monitoramento insert(Monitoramento monitoramento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            try
            {
                monitoramento.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_MONITORAMENTO (ID_MONITORAMENTO, ID_MONITORAMENTO_ANTERIOR, ID_CLIENTE_FUNCAO_FUNCIONARIO, ID_USUARIO, DATA_INICIO_CONDICAO, OBSERVACAO, ID_CLIENTE, PERIODO, ID_ESTUDO, DATA_FIM_CONDICAO) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6, :VALUE7, :VALUE8, :VALUE9, :VALUE10) ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = monitoramento.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = monitoramento.MonitoramentoAnterior != null ? monitoramento.MonitoramentoAnterior.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = monitoramento.ClienteFuncaoFuncionario.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = monitoramento.UsuarioResponsavel.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Date));
                command.Parameters[4].Value = monitoramento.DataInicioCondicao;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Text));
                command.Parameters[5].Value = monitoramento.Observacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Integer));
                command.Parameters[6].Value = monitoramento.Cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Date));
                command.Parameters[7].Value = monitoramento.Periodo;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Integer));
                command.Parameters[8].Value = monitoramento.Estudo.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Date));
                command.Parameters[9].Value = monitoramento.DataFimCondicao;

                command.ExecuteNonQuery();

                return monitoramento;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Monitoramento update(Monitoramento monitoramento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_MONITORAMENTO SET  ");
                query.Append(" ID_MONITORAMENTO_ANTERIOR = :VALUE2, ID_CLIENTE_FUNCAO_FUNCIONARIO = :VALUE3, ID_USUARIO = :VALUE4, DATA_INICIO_CONDICAO = :VALUE5, OBSERVACAO = :VALUE6, ID_CLIENTE = :VALUE7, PERIODO = :VALUE8, ID_ESTUDO = :VALUE9, ID_CLIENTE_FUNCIONARIO = NULL, DATA_FIM_CONDICAO = :VALUE10 ");
                query.Append(" WHERE ID_MONITORAMENTO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = monitoramento.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = monitoramento.MonitoramentoAnterior != null ? monitoramento.MonitoramentoAnterior.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = monitoramento.ClienteFuncaoFuncionario.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = monitoramento.UsuarioResponsavel.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Date));
                command.Parameters[4].Value = monitoramento.DataInicioCondicao;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Text));
                command.Parameters[5].Value = monitoramento.Observacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Integer));
                command.Parameters[6].Value = monitoramento.Cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Date));
                command.Parameters[7].Value = monitoramento.Periodo;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Integer));
                command.Parameters[8].Value = monitoramento.Estudo.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Date));
                command.Parameters[9].Value = monitoramento.DataFimCondicao;

                command.ExecuteNonQuery();

                return monitoramento;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Monitoramento findById(long id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            Monitoramento monitoramento = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID_MONITORAMENTO, ID_MONITORAMENTO_ANTERIOR, ID_CLIENTE_FUNCAO_FUNCIONARIO, ID_USUARIO, DATA_INICIO_CONDICAO, OBSERVACAO, ID_CLIENTE, PERIODO, ID_ESTUDO, ID_CLIENTE_FUNCIONARIO, DATA_FIM_CONDICAO ");
                query.Append(" FROM SEG_MONITORAMENTO ");
                query.Append(" WHERE ID_MONITORAMENTO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    monitoramento = new Monitoramento(dr.GetInt64(0), dr.IsDBNull(1) ? null : new Monitoramento(dr.GetInt64(1)), dr.IsDBNull(2) ? null : new ClienteFuncaoFuncionario(dr.GetInt64(2)), dr.IsDBNull(3) ? null : new Usuario(dr.GetInt64(3)), dr.GetDateTime(4), dr.IsDBNull(5) ? string.Empty : dr.GetString(5), dr.IsDBNull(6) ? null : new Cliente(dr.GetInt64(6)), dr.GetDateTime(7), dr.IsDBNull(8) ? null : new Estudo(dr.GetInt64(8)));

                    if (!dr.IsDBNull(9))
                    {
                        monitoramento.ClienteFuncionario = new ClienteFuncionario(dr.GetInt64(9), null, null, string.Empty, DateTime.Now, null, string.Empty, string.Empty, false, false);
                    }

                    if (!dr.IsDBNull(10))
                        monitoramento.DataFimCondicao = dr.GetDateTime(10);
                }

                return monitoramento;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void delete(Monitoramento monitoramento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" DELETE FROM SEG_MONITORAMENTO ");
                query.Append(" WHERE ID_MONITORAMENTO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = monitoramento.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<Monitoramento> findAllByClienteAndPeriodo(Cliente cliente, DateTime periodo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByClienteAndPeriodo");

            List<Monitoramento> monitoramentos = new List<Monitoramento>();
            Monitoramento monitoramento = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID_MONITORAMENTO, ID_MONITORAMENTO_ANTERIOR, ID_CLIENTE_FUNCAO_FUNCIONARIO, ID_USUARIO, DATA_INICIO_CONDICAO, OBSERVACAO, ID_CLIENTE, PERIODO, ID_ESTUDO, ID_CLIENTE_FUNCIONARIO, DATA_FIM_CONDICAO ");
                query.Append(" FROM SEG_MONITORAMENTO ");
                query.Append(" WHERE ID_CLIENTE = :VALUE1 ");
                query.Append(" AND PERIODO = :VALUE2 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Date));
                command.Parameters[1].Value = periodo;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    monitoramento = new Monitoramento(dr.GetInt64(0), dr.IsDBNull(1) ? null : new Monitoramento(dr.GetInt64(1)), dr.IsDBNull(2) ? null :  new ClienteFuncaoFuncionario(dr.GetInt64(2)), dr.IsDBNull(3) ? null : new Usuario(dr.GetInt64(3)), dr.GetDateTime(4), dr.IsDBNull(5) ? string.Empty : dr.GetString(5), dr.IsDBNull(6) ? null : cliente, dr.GetDateTime(7), dr.IsDBNull(8) ? null : new Estudo(dr.GetInt64(8)));

                    if (!dr.IsDBNull(9))
                    {
                        monitoramento.ClienteFuncionario = new ClienteFuncionario(dr.GetInt64(9), null, null, string.Empty, DateTime.Now, null, string.Empty, string.Empty, false, false);
                    }

                    if (!dr.IsDBNull(10))
                        monitoramento.DataFimCondicao = dr.GetDateTime(10);

                    monitoramentos.Add(monitoramento);
                }

                return monitoramentos;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByClienteAndPeriodo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public bool existMonitoramentoPassadoByClienteAndPeriodo(Cliente cliente, DateTime periodo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByClienteAndPeriodo");

            bool existeMonitoramento = false;


            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT * ");
                query.Append(" FROM SEG_MONITORAMENTO ");
                query.Append(" WHERE ID_CLIENTE = :VALUE1 ");
                query.Append(" AND PERIODO <= :VALUE2 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Date));
                command.Parameters[1].Value = periodo;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    existeMonitoramento = true;
                }

                return existeMonitoramento;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByClienteAndPeriodo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<Monitoramento> findAllByLote(LoteEsocial lote, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByLote");

            List<Monitoramento> monitoramentos = new List<Monitoramento>();
            Monitoramento monitoramento = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT MONITORAMENTO.ID_MONITORAMENTO, MONITORAMENTO.ID_MONITORAMENTO_ANTERIOR, MONITORAMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO, MONITORAMENTO.ID_USUARIO, MONITORAMENTO.DATA_INICIO_CONDICAO, MONITORAMENTO.OBSERVACAO, MONITORAMENTO.ID_CLIENTE, MONITORAMENTO.PERIODO, ID_ESTUDO, MONITORAMENTO.ID_CLIENTE_FUNCIONARIO, DATA_FIM_CONDICAO ");
                query.Append(" FROM SEG_MONITORAMENTO MONITORAMENTO ");
                query.Append(" JOIN SEG_ESOCIAL_MONITORAMENTO ESOCIAL_MONITORAMENTO ON ESOCIAL_MONITORAMENTO.ID_MONITORAMENTO = MONITORAMENTO.ID_MONITORAMENTO ");
                query.Append(" WHERE ID_ESOCIAL_LOTE = :VALUE1 ");
                query.Append(" AND ESOCIAL_MONITORAMENTO.SITUACAO = 'E'");
                    
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = lote.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    monitoramento = new Monitoramento(dr.GetInt64(0), dr.IsDBNull(1) ? null : new Monitoramento(dr.GetInt64(1)), dr.IsDBNull(2) ? null : new ClienteFuncaoFuncionario(dr.GetInt64(2)), dr.IsDBNull(3) ? null : new Usuario(dr.GetInt64(3)), dr.GetDateTime(4), dr.IsDBNull(5) ? string.Empty : dr.GetString(5), dr.IsDBNull(6) ? null : new Cliente(dr.GetInt64(6)), dr.GetDateTime(7), dr.IsDBNull(8) ? null : new Estudo(dr.GetInt64(8)));

                    if (!dr.IsDBNull(9))
                        monitoramento.ClienteFuncionario = new ClienteFuncionario(dr.GetInt64(9), null, null, string.Empty, DateTime.Now, null, string.Empty, string.Empty, false, false);

                    if (!dr.IsDBNull(10))
                        monitoramento.DataFimCondicao = dr.GetDateTime(10);

                    monitoramentos.Add(monitoramento);
                }

                return monitoramentos;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByLote", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public string findAtividadeDoFuncionarioByMonitoramento(Monitoramento monitoramento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtividadeDoFuncionarioByMonitoramento");

            string atividade = string.Empty;

            try
            {
                StringBuilder query = new StringBuilder();

                if (monitoramento.ClienteFuncaoFuncionario != null)
                {
                    query.Append(" SELECT COMENTARIO_FUNCAO.ATIVIDADE FROM SEG_MONITORAMENTO MONITORAMENTO ");
                    query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTE_FUNCAO_FUNCIONARIO ON MONITORAMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO = CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                    query.Append(" JOIN SEG_FUNCIONARIO FUNCIONARIO ON FUNCIONARIO.ID_FUNCIONARIO = CLIENTE_FUNCAO_FUNCIONARIO.ID_FUNCIONARIO ");
                    query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO  = CLIENTE_FUNCAO_FUNCIONARIO .ID_SEG_CLIENTE_FUNCAO ");
                    query.Append(" JOIN SEG_GHE_SETOR_CLIENTE_FUNCAO GHE_SETOR_CLIENTE_FUNCAO ON GHE_SETOR_CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO ");
                    query.Append(" JOIN SEG_COMENTARIO_FUNCAO COMENTARIO_FUNCAO ON COMENTARIO_FUNCAO.ID_COMENTARIO_FUNCAO = GHE_SETOR_CLIENTE_FUNCAO.ID_COMENTARIO_FUNCAO ");
                    query.Append(" WHERE CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO = :VALUE1");
                }
                else
                {
                    query.Append(" SELECT COMENTARIO_FUNCAO.ATIVIDADE FROM SEG_MONITORAMENTO MONITORAMENTO ");
                    query.Append(" JOIN SEG_CLIENTE_FUNCIONARIO CLIENTE_FUNCIONARIO ON MONITORAMENTO.ID_CLIENTE_FUNCIONARIO = CLIENTE_FUNCIONARIO.ID ");
                    query.Append(" JOIN SEG_FUNCIONARIO FUNCIONARIO ON FUNCIONARIO.ID_FUNCIONARIO = CLIENTE_FUNCIONARIO.ID_FUNCIONARIO ");
                    query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTE_FUNCAO_FUNCIONARIO ON CLIENTE_FUNCAO_FUNCIONARIO.ID_FUNCIONARIO = FUNCIONARIO.ID_FUNCIONARIO ");
                    query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO  = CLIENTE_FUNCAO_FUNCIONARIO .ID_SEG_CLIENTE_FUNCAO ");
                    query.Append(" JOIN SEG_GHE_SETOR_CLIENTE_FUNCAO GHE_SETOR_CLIENTE_FUNCAO ON GHE_SETOR_CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO ");
                    query.Append(" JOIN SEG_COMENTARIO_FUNCAO COMENTARIO_FUNCAO ON COMENTARIO_FUNCAO.ID_COMENTARIO_FUNCAO = GHE_SETOR_CLIENTE_FUNCAO.ID_COMENTARIO_FUNCAO ");
                    query.Append(" WHERE CLIENTE_FUNCIONARIO.ID = :VALUE1");
                }

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = monitoramento.ClienteFuncaoFuncionario != null ? monitoramento.ClienteFuncaoFuncionario.Id : monitoramento.ClienteFuncionario.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    atividade = dr.IsDBNull(0) ? string.Empty : dr.GetString(0);
                }

                return atividade;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtividadeDoFuncionarioByMonitoramento", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public string findSetorInLotacaoFuncionarioByMonitoramento(Monitoramento monitoramento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findSetorInLotacaoFuncionarioByMonitoramento");

            string descricaoSetor = string.Empty;

            try
            {
                StringBuilder query = new StringBuilder();

                if (monitoramento.ClienteFuncaoFuncionario != null)
                {

                    query.Append(" SELECT SETOR.DESCRICAO FROM SEG_MONITORAMENTO MONITORAMENTO ");
                    query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTE_FUNCAO_FUNCIONARIO ON MONITORAMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO  = CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                    query.Append(" JOIN SEG_FUNCIONARIO FUNCIONARIO ON FUNCIONARIO.ID_FUNCIONARIO = CLIENTE_FUNCAO_FUNCIONARIO.ID_FUNCIONARIO ");
                    query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO  = CLIENTE_FUNCAO_FUNCIONARIO.ID_SEG_CLIENTE_FUNCAO ");
                    query.Append(" JOIN SEG_GHE_SETOR_CLIENTE_FUNCAO GHE_SETOR_CLIENTE_FUNCAO ON GHE_SETOR_CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO ");
                    query.Append(" JOIN SEG_GHE_SETOR GHE_SETOR ON GHE_SETOR.ID_GHE_SETOR = GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR ");
                    query.Append(" JOIN SEG_SETOR SETOR ON SETOR.ID_SETOR = GHE_SETOR.ID_SETOR ");
                    query.Append(" WHERE CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO = :VALUE1 ");
                }
                else
                {
                    query.Append(" SELECT SETOR.DESCRICAO FROM SEG_MONITORAMENTO MONITORAMENTO ");
                    query.Append(" JOIN SEG_CLIENTE_FUNCIONARIO CLIENTE_FUNCIONARIO ON MONITORAMENTO.ID_CLIENTE_FUNCIONARIO  = CLIENTE_FUNCIONARIO.ID ");
                    query.Append(" JOIN SEG_FUNCIONARIO FUNCIONARIO ON FUNCIONARIO.ID_FUNCIONARIO = CLIENTE_FUNCIONARIO.ID_FUNCIONARIO  ");
                    query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTE_FUNCAO_FUNCIONARIO ON CLIENTE_FUNCAO_FUNCIONARIO.ID_FUNCIONARIO = FUNCIONARIO.ID_FUNCIONARIO  ");
                    query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO  = CLIENTE_FUNCAO_FUNCIONARIO .ID_SEG_CLIENTE_FUNCAO ");
                    query.Append(" JOIN SEG_GHE_SETOR_CLIENTE_FUNCAO GHE_SETOR_CLIENTE_FUNCAO ON GHE_SETOR_CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO  ");
                    query.Append(" JOIN SEG_GHE_SETOR GHE_SETOR ON GHE_SETOR.ID_GHE_SETOR = GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR  ");
                    query.Append(" JOIN SEG_SETOR SETOR ON SETOR.ID_SETOR = GHE_SETOR.ID_SETOR  ");
                    query.Append(" WHERE CLIENTE_FUNCIONARIO.ID = :VALUE1  ");
                }

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = monitoramento.ClienteFuncaoFuncionario != null ? monitoramento.ClienteFuncaoFuncionario.Id : monitoramento.ClienteFuncionario.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    descricaoSetor = dr.IsDBNull(0) ? string.Empty : dr.GetString(0);
                }

                return descricaoSetor;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findSetorInLotacaoFuncionarioByMonitoramento", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public string findFuncaoLotacaoFuncionarioByMonitoramento(Monitoramento monitoramento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findSetorInLotacaoFuncionarioByMonitoramento");

            string descricaoFuncao = string.Empty;

            try
            {
                StringBuilder query = new StringBuilder();


                if (monitoramento.ClienteFuncaoFuncionario != null)
                {
                    query.Append(" SELECT FUNCAO.DESCRICAO FROM SEG_MONITORAMENTO MONITORAMENTO  ");
                    query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTE_FUNCAO_FUNCIONARIO ON MONITORAMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO  = CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                    query.Append(" JOIN SEG_FUNCIONARIO FUNCIONARIO ON FUNCIONARIO.ID_FUNCIONARIO = CLIENTE_FUNCAO_FUNCIONARIO.ID_FUNCIONARIO  ");
                    query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO  = CLIENTE_FUNCAO_FUNCIONARIO .ID_SEG_CLIENTE_FUNCAO ");
                    query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO  = CLIENTE_FUNCAO.ID_FUNCAO ");
                    query.Append(" WHERE CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO = :VALUE1 ");
                }
                else
                {
                    query.Append(" SELECT FUNCAO.DESCRICAO FROM SEG_MONITORAMENTO MONITORAMENTO  ");
                    query.Append(" JOIN SEG_CLIENTE_FUNCIONARIO CLIENTE_FUNCIONARIO ON MONITORAMENTO.ID_CLIENTE_FUNCIONARIO  = CLIENTE_FUNCIONARIO.ID ");
                    query.Append(" JOIN SEG_FUNCIONARIO FUNCIONARIO ON FUNCIONARIO.ID_FUNCIONARIO = CLIENTE_FUNCIONARIO.ID_FUNCIONARIO  ");
                    query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTE_FUNCAO_FUNCIONARIO ON CLIENTE_FUNCAO_FUNCIONARIO.ID_FUNCIONARIO = FUNCIONARIO.ID_FUNCIONARIO ");
                    query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO  = CLIENTE_FUNCAO_FUNCIONARIO .ID_SEG_CLIENTE_FUNCAO ");
                    query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO  = CLIENTE_FUNCAO.ID_FUNCAO ");
                    query.Append(" WHERE CLIENTE_FUNCIONARIO.ID = :VALUE1 ");
                }

                

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = monitoramento.ClienteFuncaoFuncionario != null ? monitoramento.ClienteFuncaoFuncionario.Id : monitoramento.ClienteFuncionario.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    descricaoFuncao = dr.IsDBNull(0) ? string.Empty : dr.GetString(0);
                }

                return descricaoFuncao;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findSetorInLotacaoFuncionarioByMonitoramento", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<Monitoramento> findAllByFilter(Monitoramento monitoramento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByFilter");

            List<Monitoramento> monitoramentos = new List<Monitoramento>();
            Monitoramento monitoramentoReturn = null;

            try
            {
                StringBuilder query = new StringBuilder();

                if (monitoramento.isNullForFilter())
                {
                    query.Append(" SELECT MONITORAMENTO.ID_MONITORAMENTO, MONITORAMENTO.ID_MONITORAMENTO_ANTERIOR, MONITORAMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO, MONITORAMENTO.ID_USUARIO, MONITORAMENTO.DATA_INICIO_CONDICAO, MONITORAMENTO.OBSERVACAO, MONITORAMENTO.ID_CLIENTE, MONITORAMENTO.PERIODO, MONITORAMENTO.ID_ESTUDO, MONITORAMENTO.ID_CLIENTE_FUNCIONARIO, MONITORAMENTO.DATA_FIM_CONDICAO ");
                    query.Append(" FROM SEG_MONITORAMENTO MONITORAMENTO ");
                    query.Append(" ORDER BY ID_MONITORAMENTO ASC ");
                }
                else
                {
                    query.Append(" SELECT MONITORAMENTO.ID_MONITORAMENTO, MONITORAMENTO.ID_MONITORAMENTO_ANTERIOR, MONITORAMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO, MONITORAMENTO.ID_USUARIO, MONITORAMENTO.DATA_INICIO_CONDICAO, MONITORAMENTO.OBSERVACAO, MONITORAMENTO.ID_CLIENTE, MONITORAMENTO.PERIODO, MONITORAMENTO.ID_ESTUDO, MONITORAMENTO.ID_CLIENTE_FUNCIONARIO, MONITORAMENTO.DATA_FIM_CONDICAO ");
                    query.Append(" FROM SEG_MONITORAMENTO MONITORAMENTO ");
                    query.Append(" WHERE TRUE ");

                    if (monitoramento.Cliente != null)
                        query.Append(" AND MONITORAMENTO.ID_CLIENTE = :VALUE1 ");

                    if (monitoramento.ClienteFuncaoFuncionario != null)
                        query.Append(" AND MONITORAMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO = :VALUE2 ");

                    if (monitoramento.Periodo != null)
                        query.Append("AND MONITORAMENTO.PERIODO = :VALUE3");

                    query.Append(" ORDER BY ID_MONITORAMENTO ASC ");
                }

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = monitoramento.Cliente != null ? monitoramento.Cliente.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = monitoramento.ClienteFuncaoFuncionario != null ? monitoramento.ClienteFuncaoFuncionario.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Date));
                command.Parameters[2].Value = monitoramento.Periodo != null ? monitoramento.Periodo : null;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    monitoramentoReturn = new Monitoramento(dr.GetInt64(0), dr.IsDBNull(1) ? null : new Monitoramento(dr.GetInt64(1)), dr.IsDBNull(2) ? null : new ClienteFuncaoFuncionario(dr.GetInt64(2)), dr.IsDBNull(3) ? null : new Usuario(dr.GetInt64(3)), dr.GetDateTime(4), dr.IsDBNull(5) ? string.Empty : dr.GetString(5), dr.IsDBNull(6) ? null : new Cliente(dr.GetInt64(6)), dr.GetDateTime(7), dr.IsDBNull(8) ? null : new Estudo(dr.GetInt64(8)));
                    if (!dr.IsDBNull(9))
                    {
                        monitoramentoReturn.ClienteFuncionario = new ClienteFuncionario(dr.GetInt64(9), null, null, string.Empty, DateTime.Now, null, string.Empty, string.Empty, false, false);
                        
                    }

                    if (!dr.IsDBNull(10))
                        monitoramento.DataFimCondicao = dr.GetDateTime(10);

                    
                    monitoramentos.Add(monitoramentoReturn);
                    
                }

                return monitoramentos;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Monitoramento findLastMonitoramentoByClienteFuncaoFuncionario(ClienteFuncaoFuncionario clienteFuncaoFuncionario, DateTime periodo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findLastMonitoramentoByClienteFuncionario");

            Monitoramento lastMonitoramento = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID_MONITORAMENTO, ID_MONITORAMENTO_ANTERIOR, ID_CLIENTE_FUNCAO_FUNCIONARIO, ID_USUARIO, DATA_INICIO_CONDICAO, OBSERVACAO, ID_CLIENTE, PERIODO, ID_ESTUDO, DATA_FIM_CONDICAO ");
                query.Append(" FROM SEG_MONITORAMENTO ");
                query.Append(" WHERE TRUE");
                query.Append(" AND ID_CLIENTE_FUNCAO_FUNCIONARIO = :VALUE1 ");
                query.Append(" AND PERIODO < :VALUE2 ");
                query.Append(" ORDER BY PERIODO DESC ");
                query.Append(" LIMIT 1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncaoFuncionario.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Date));
                command.Parameters[1].Value = periodo;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    lastMonitoramento = new Monitoramento(dr.GetInt64(0), dr.IsDBNull(1) ? null : new Monitoramento(dr.GetInt64(1)), clienteFuncaoFuncionario, new Usuario(dr.GetInt64(3)), dr.GetDateTime(4), dr.IsDBNull(5) ? string.Empty : dr.GetString(5), clienteFuncaoFuncionario.ClienteFuncao.Cliente, dr.GetDateTime(7), dr.IsDBNull(8) ? null : new Estudo(dr.GetInt64(8)));

                    if (!dr.IsDBNull(9))
                        lastMonitoramento.DataFimCondicao = dr.GetDateTime(9);
                }

                return lastMonitoramento;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findLastMonitoramentoByClienteFuncionario", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<Monitoramento> findAllMonitoramentoByClienteFuncaoFuncionario(ClienteFuncaoFuncionario clienteFuncaoFuncionario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllMonitoramentoByClienteFuncaoFuncionario");

            List<Monitoramento> monitoramentos = new List<Monitoramento>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID_MONITORAMENTO, ID_MONITORAMENTO_ANTERIOR, ID_CLIENTE_FUNCAO_FUNCIONARIO, ID_USUARIO, DATA_INICIO_CONDICAO, OBSERVACAO, ID_CLIENTE, PERIODO, ID_ESTUDO, ID_CLIENTE_FUNCIONARIO, DATA_FIM_CONDICAO ");
                query.Append(" FROM SEG_MONITORAMENTO ");
                query.Append(" WHERE ID_CLIENTE_FUNCAO_FUNCIONARIO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncaoFuncionario.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    Monitoramento monitoramento = new Monitoramento(dr.GetInt64(0), dr.IsDBNull(1) ? null : new Monitoramento(dr.GetInt64(1)), dr.IsDBNull(2) ? null : new ClienteFuncaoFuncionario(dr.GetInt64(2)), dr.IsDBNull(3) ? null : new Usuario(dr.GetInt64(3)), dr.GetDateTime(4), dr.IsDBNull(5) ? string.Empty : dr.GetString(5), dr.IsDBNull(6) ? null : new Cliente(dr.GetInt64(6)), dr.GetDateTime(7), dr.IsDBNull(8) ? null : new Estudo(dr.GetInt64(8)));

                    if (!dr.IsDBNull(9))
                    {
                        monitoramento.ClienteFuncionario = new ClienteFuncionario(dr.GetInt64(9), null, null, string.Empty, DateTime.Now, null, string.Empty, string.Empty, false, false);
                    }

                    if (!dr.IsDBNull(10))
                        monitoramento.DataFimCondicao = dr.GetDateTime(10);

                    monitoramentos.Add(monitoramento);
                }
                
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllMonitoramentoByClienteFuncaoFuncionario", ex);
                throw new DataBaseConectionException(ex.Message);
            }
            return monitoramentos;
        }
        
    }
}

