﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using SWS.Entidade;
using NpgsqlTypes;
using SWS.View.Resources;
using SWS.Excecao;
using System.Data;
using SWS.IDao;
using SWS.Helper;

namespace SWS.Dao
{
    class MedicoDao : IMedicoDao
    {
        public Medico insert(Medico medico, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            StringBuilder query = new StringBuilder();

            try
            {
                medico.Id = recuperaProximoId(dbConnection);

                query.Append(" INSERT INTO SEG_MEDICO ( ID_MEDICO, NOME, CRM, SITUACAO, TELEFONE1, TELEFONE2, EMAIL, ");
                query.Append(" ENDERECO, NUMERO, COMPLEMENTO, BAIRRO, CEP, UF, ID_CIDADE, PIS_PASEP, UF_CRM, DOCUMENTO_EXTRA, ASSINATURA, MIMETYPE, CPF, RQE) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, 'A', :VALUE4, :VALUE5, :VALUE6, :VALUE7, ");
                query.Append(" :VALUE8, :VALUE9, :VALUE10, :VALUE11, :VALUE12, :VALUE13, :VALUE14, :VALUE15, :VALUE16, :VALUE17, :VALUE18, :VALUE19, :VALUE20)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = medico.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = medico.Nome;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = medico.Crm;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = medico.Telefone1;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = medico.Telefone2;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = medico.Email;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = medico.Endereco;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Varchar));
                command.Parameters[7].Value = medico.Numero;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                command.Parameters[8].Value = medico.Complemento;

                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                command.Parameters[9].Value = medico.Bairro;

                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Varchar));
                command.Parameters[10].Value = medico.Cep;

                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Varchar));
                command.Parameters[11].Value = medico.Uf;

                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Integer));
                command.Parameters[12].Value = medico.Cidade != null ? medico.Cidade.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Varchar));
                command.Parameters[13].Value = medico.Pispasep;

                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Varchar));
                command.Parameters[14].Value = medico.UfCrm;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Varchar));
                command.Parameters[15].Value = medico.DocumentoExtra;

                command.Parameters.Add(new NpgsqlParameter("VALUE17", NpgsqlDbType.Bytea));
                command.Parameters[16].Value = medico.Assinatura;

                command.Parameters.Add(new NpgsqlParameter("VALUE18", NpgsqlDbType.Varchar));
                command.Parameters[17].Value = medico.Mimetype;

                command.Parameters.Add(new NpgsqlParameter("VALUE19", NpgsqlDbType.Varchar));
                command.Parameters[18].Value = medico.Cpf;

                command.Parameters.Add(new NpgsqlParameter("VALUE20", NpgsqlDbType.Varchar));
                command.Parameters[19].Value = medico.Rqe;


                command.ExecuteNonQuery();

                return medico;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Medico update(Medico medico, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateMedico");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_MEDICO SET NOME = :VALUE2, CRM = :VALUE3, TELEFONE1 = :VALUE4, ");
                query.Append(" TELEFONE2 = :VALUE5, EMAIL = :VALUE6, ENDERECO = :VALUE7, NUMERO = :VALUE8, ");
                query.Append(" COMPLEMENTO = :VALUE9, BAIRRO = :VALUE10, CEP = :VALUE11, ID_CIDADE = :VALUE12, ");
                query.Append(" UF = :VALUE13, SITUACAO = :VALUE14, PIS_PASEP = :VALUE15, UF_CRM = :VALUE16, DOCUMENTO_EXTRA = :VALUE17, ASSINATURA = :VALUE18, MIMETYPE = :VALUE19, CPF = :VALUE20, RQE = :VALUE21 WHERE ID_MEDICO = :VALUE1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = medico.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = medico.Nome;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = medico.Crm;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = medico.Telefone1;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = medico.Telefone2;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = medico.Email;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = medico.Endereco;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Varchar));
                command.Parameters[7].Value = medico.Numero;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                command.Parameters[8].Value = medico.Complemento;

                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                command.Parameters[9].Value = medico.Bairro;

                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Varchar));
                command.Parameters[10].Value = medico.Cep;

                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Integer));
                command.Parameters[11].Value = medico.Cidade != null ? medico.Cidade.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Varchar));
                command.Parameters[12].Value = medico.Uf;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Varchar));
                command.Parameters[13].Value = medico.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Varchar));
                command.Parameters[14].Value = medico.Pispasep;

                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Varchar));
                command.Parameters[15].Value = medico.UfCrm;

                command.Parameters.Add(new NpgsqlParameter("VALUE17", NpgsqlDbType.Varchar));
                command.Parameters[16].Value = medico.DocumentoExtra;

                command.Parameters.Add(new NpgsqlParameter("VALUE18", NpgsqlDbType.Bytea));
                command.Parameters[17].Value = medico.Assinatura;

                command.Parameters.Add(new NpgsqlParameter("VALUE19", NpgsqlDbType.Varchar));
                command.Parameters[18].Value = medico.Mimetype;

                command.Parameters.Add(new NpgsqlParameter("VALUE20", NpgsqlDbType.Varchar));
                command.Parameters[19].Value = medico.Cpf;

                command.Parameters.Add(new NpgsqlParameter("VALUE21", NpgsqlDbType.Varchar));
                command.Parameters[20].Value = medico.Rqe;

                command.ExecuteNonQuery();

                return medico;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_MEDICO_ID_MEDICO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Medico findById(Int64 idMedico, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMedicoById");

            StringBuilder query = new StringBuilder();

            Medico medico = null;

            try
            {
                
                query.Append(" SELECT MEDICO.ID_MEDICO, MEDICO.NOME, MEDICO.CRM, MEDICO.SITUACAO, MEDICO.TELEFONE1, MEDICO.TELEFONE2, MEDICO.EMAIL, MEDICO.ENDERECO, MEDICO.NUMERO, ");
                query.Append(" MEDICO.COMPLEMENTO, MEDICO.BAIRRO, MEDICO.CEP, MEDICO.UF, MEDICO.ID_CIDADE, MEDICO.PIS_PASEP, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, MEDICO.UF_CRM, MEDICO.DOCUMENTO_EXTRA, MEDICO.ASSINATURA, MEDICO.MIMETYPE, MEDICO.CPF, MEDICO.RQE ");
                query.Append(" FROM SEG_MEDICO MEDICO ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = MEDICO.ID_CIDADE ");
                query.Append(" WHERE MEDICO.ID_MEDICO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = idMedico;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    medico = new Medico(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.IsDBNull(4) ? string.Empty : dr.GetString(4), dr.IsDBNull(5) ? string.Empty : dr.GetString(5), dr.IsDBNull(6) ? string.Empty : dr.GetString(6), dr.IsDBNull(7) ? string.Empty : dr.GetString(7), dr.IsDBNull(8) ? string.Empty : dr.GetString(8), dr.IsDBNull(9) ? string.Empty : dr.GetString(9), dr.IsDBNull(10) ? string.Empty : dr.GetString(10), dr.IsDBNull(11) ? string.Empty : dr.GetString(11), dr.IsDBNull(12) ? string.Empty : dr.GetString(12), dr.IsDBNull(13) ? null : new CidadeIbge(dr.GetInt64(13), dr.GetString(15), dr.GetString(16), dr.GetString(17)), dr.IsDBNull(14) ? String.Empty : dr.GetString(14), dr.IsDBNull(18) ? string.Empty : dr.GetString(18), dr.IsDBNull(19) ? string.Empty : dr.GetString(19), dr.IsDBNull(20) ? null : (byte[])dr.GetValue(20), dr.IsDBNull(21) ? string.Empty : dr.GetString(21), dr.IsDBNull(22) ? string.Empty : dr.GetString(22), dr.IsDBNull(23) ? string.Empty : dr.GetString(23));
                }
                
                return medico;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findByFilter(Medico medico, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByFilter");

            try
            {
                StringBuilder query = new StringBuilder();

                if (medico.isNullForFilter())
                {
                    query.Append(" SELECT MEDICO.ID_MEDICO, MEDICO.NOME, MEDICO.CRM, MEDICO.SITUACAO, MEDICO.TELEFONE1, MEDICO.TELEFONE2, MEDICO.EMAIL, MEDICO.ENDERECO, MEDICO.NUMERO, ");
                    query.Append(" MEDICO.COMPLEMENTO, MEDICO.BAIRRO, MEDICO.CEP, MEDICO.UF, CIDADE.NOME AS CIDADE, MEDICO.PIS_PASEP ");
                    query.Append(" FROM SEG_MEDICO MEDICO ");
                    query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = MEDICO.ID_CIDADE ");
                    query.Append(" ORDER BY MEDICO.NOME ASC ");
                }
                else
                {
                    query.Append(" SELECT MEDICO.ID_MEDICO, MEDICO.NOME, MEDICO.CRM, MEDICO.SITUACAO, MEDICO.TELEFONE1, MEDICO.TELEFONE2, MEDICO.EMAIL, MEDICO.ENDERECO, MEDICO.NUMERO, ");
                    query.Append(" MEDICO.COMPLEMENTO, MEDICO.BAIRRO, MEDICO.CEP, MEDICO.UF, CIDADE.NOME AS CIDADE, MEDICO.PIS_PASEP ");
                    query.Append(" FROM SEG_MEDICO MEDICO ");
                    query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = MEDICO.ID_CIDADE ");
                    query.Append(" WHERE TRUE ");

                    if (!String.IsNullOrWhiteSpace(medico.Nome))
                        query.Append(" AND MEDICO.NOME LIKE :VALUE1 ");

                    if (!String.IsNullOrWhiteSpace(medico.Situacao))
                        query.Append(" AND MEDICO.SITUACAO = :VALUE2 ");

                    if (!String.IsNullOrWhiteSpace(medico.Crm))
                        query.Append(" AND  MEDICO.CRM = :VALUE3 ");

                    query.Append(" ORDER BY MEDICO.NOME ASC ");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = "%" + medico.Nome + "%";
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = medico.Situacao;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[2].Value = medico.Crm;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Medicos");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean isMedicoUsed(Medico medico, NpgsqlConnection dbConnection)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMedicoInEstudo");

            StringBuilder query = new StringBuilder();

            try
            {
                Boolean retorno = Convert.ToBoolean(false);

                query.Append(" SELECT ID_ESTUDO, ID_MEDICO FROM SEG_ESTUDO_MEDICO WHERE ID_MEDICO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = medico.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    retorno = Convert.ToBoolean(true);
                }

                return retorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMedicoInEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public DataSet findAtivosByMedicoEstudo(MedicoEstudo medicoEstudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllMedicosAtivoByPcmso");

            try
            {
                StringBuilder query = new StringBuilder();

                if (String.IsNullOrWhiteSpace(medicoEstudo.Medico.Nome))
                {
                    query.Append(" SELECT MEDICO.ID_MEDICO, MEDICO.NOME, MEDICO.CRM, MEDICO.SITUACAO, MEDICO.TELEFONE1, MEDICO.TELEFONE2, MEDICO.EMAIL, MEDICO.ENDERECO, ");
                    query.Append(" MEDICO.NUMERO, MEDICO.COMPLEMENTO, MEDICO.BAIRRO, MEDICO.CEP, CIDADE.NOME AS CIDADE, MEDICO.UF ");
                    query.Append(" FROM SEG_MEDICO MEDICO ");
                    query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = MEDICO.ID_CIDADE ");
                    query.Append(" WHERE MEDICO.SITUACAO = 'A' AND  ");
                    query.Append(" MEDICO.ID_MEDICO NOT IN (SELECT ID_MEDICO FROM SEG_ESTUDO_MEDICO WHERE ID_ESTUDO = :VALUE2) ");
                    query.Append(" ORDER BY MEDICO.NOME ");
                }
                else
                {
                    query.Append(" SELECT MEDICO.ID_MEDICO, MEDICO.NOME, MEDICO.CRM, MEDICO.SITUACAO, MEDICO.TELEFONE1, MEDICO.TELEFONE2, MEDICO.EMAIL, MEDICO.ENDERECO, ");
                    query.Append(" MEDICO.NUMERO, MEDICO.COMPLEMENTO, MEDICO.BAIRRO, MEDICO.CEP, CIDADE.NOME AS CIDADE, MEDICO.UF ");
                    query.Append(" FROM SEG_MEDICO MEDICO ");
                    query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = MEDICO.ID_CIDADE ");
                    query.Append(" WHERE MEDICO.SITUACAO = 'A' AND MEDICO.NOME LIKE :VALUE1 AND ");
                    query.Append(" MEDICO.ID_MEDICO NOT IN (SELECT ID_MEDICO FROM SEG_ESTUDO_MEDICO WHERE ID_ESTUDO = :VALUE2) ");
                    query.Append(" ORDER BY MEDICO.NOME ASC ");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = medicoEstudo.Medico.Nome.ToUpper() + "%";
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[1].Value = medicoEstudo.Pcmso.Id;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Medicos");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllMedicosAtivoByPcmso", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findMedicosAtivoByMedico(Medico medico, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllMedicosAtivo");

            try
            {
                StringBuilder query = new StringBuilder();

                if (String.IsNullOrWhiteSpace(medico.Nome))
                {
                    query.Append(" SELECT MEDICO.ID_MEDICO, MEDICO.NOME, MEDICO.CRM, MEDICO.SITUACAO, MEDICO.TELEFONE1, MEDICO.TELEFONE2, MEDICO.EMAIL, MEDICO.ENDERECO, ");
                    query.Append(" MEDICO.NUMERO, MEDICO.COMPLEMENTO, MEDICO.BAIRRO, MEDICO.CEP, CIDADE.NOME AS CIDADE, MEDICO.UF ");
                    query.Append(" FROM SEG_MEDICO MEDICO ");
                    query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = MEDICO.ID_CIDADE ");
                    query.Append(" WHERE MEDICO.SITUACAO = 'A' ORDER BY MEDICO.NOME ");
                }
                else
                {
                    query.Append(" SELECT MEDICO.ID_MEDICO, MEDICO.NOME, MEDICO.CRM, MEDICO.SITUACAO, MEDICO.TELEFONE1, MEDICO.TELEFONE2, MEDICO.EMAIL, MEDICO.ENDERECO, ");
                    query.Append(" MEDICO.NUMERO, MEDICO.COMPLEMENTO, MEDICO.BAIRRO, MEDICO.CEP, CIDADE.NOME AS CIDADE, MEDICO.UF ");
                    query.Append(" FROM SEG_MEDICO MEDICO ");
                    query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = MEDICO.ID_CIDADE ");
                    query.Append(" WHERE MEDICO.SITUACAO = 'A' AND MEDICO.NOME LIKE :VALUE1 ");
                    query.Append(" ORDER BY MEDICO.NOME ASC ");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[0].Value = medico.Nome.ToUpper() + "%";

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Medicos");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllMedicosAtivo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public HashSet<Medico> findAllByPcmso(Estudo pcmso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByPcmso");

            HashSet<Medico> medicos = new HashSet<Medico>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT MEDICO.ID_MEDICO, MEDICO.NOME, MEDICO.CRM, MEDICO.SITUACAO, MEDICO.TELEFONE1, MEDICO.TELEFONE2, MEDICO.EMAIL, MEDICO.ENDERECO, ");
                query.Append(" MEDICO.NUMERO, MEDICO.COMPLEMENTO, MEDICO.BAIRRO, MEDICO.CEP, MEDICO.UF, MEDICO.ID_CIDADE, MEDICO.PIS_PASEP, CIDADE.ID_CIDADE_IBGE, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, MEDICO.UF_CRM, MEDICO.DOCUMENTO_EXTRA, MEDICO.ASSINATURA, MEDICO.MIMETYPE, MEDICO.CPF, MEDICO.RQE ");
                query.Append(" FROM SEG_ESTUDO_MEDICO ESTUDO_MEDICO ");
                query.Append(" JOIN SEG_MEDICO MEDICO ON MEDICO.ID_MEDICO = ESTUDO_MEDICO.ID_MEDICO");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = MEDICO.ID_CIDADE");
                query.Append(" WHERE ESTUDO_MEDICO.ID_ESTUDO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = pcmso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    medicos.Add(new Medico(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.IsDBNull(4) ? string.Empty : dr.GetString(4), dr.IsDBNull(5) ? string.Empty : dr.GetString(5), dr.IsDBNull(6) ? string.Empty : dr.GetString(6), dr.IsDBNull(7) ? string.Empty : dr.GetString(7), dr.IsDBNull(8) ? string.Empty : dr.GetString(8), dr.IsDBNull(9) ? string.Empty : dr.GetString(9), dr.IsDBNull(10) ? string.Empty : dr.GetString(10), dr.IsDBNull(11) ? string.Empty : dr.GetString(11), dr.IsDBNull(12) ? string.Empty : dr.GetString(12), dr.IsDBNull(13) ? null : new CidadeIbge(dr.GetInt64(13), dr.GetString(16), dr.GetString(17), dr.GetString(18)), dr.IsDBNull(14) ? string.Empty : dr.GetString(14), dr.IsDBNull(19) ? string.Empty : dr.GetString(19), dr.IsDBNull(20) ? string.Empty : dr.GetString(20), dr.IsDBNull(21) ? null : (byte[])dr.GetValue(21), dr.IsDBNull(22) ? string.Empty : dr.GetString(22), dr.IsDBNull(23) ? string.Empty : dr.GetString(23), dr.IsDBNull(24) ? string.Empty : dr.GetString(24)));
                }

                return medicos;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByPcmso", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Medico findCoordenadorByEstudo(Estudo pcmso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMedicoByEstudo");

            Medico medico = null;

            StringBuilder query = new StringBuilder();
            
            try
            {

                query.Append(" SELECT MEDICO.ID_MEDICO, MEDICO.NOME, MEDICO.CRM, MEDICO.SITUACAO, MEDICO.TELEFONE1, MEDICO.TELEFONE2, MEDICO.EMAIL, MEDICO.ENDERECO, ");
                query.Append(" MEDICO.NUMERO, MEDICO.COMPLEMENTO, MEDICO.BAIRRO, MEDICO.CEP, MEDICO.UF, MEDICO.ID_CIDADE, MEDICO.PIS_PASEP, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, MEDICO.UF_CRM, MEDICO.DOCUMENTO_EXTRA, MEDICO.ASSINATURA, MEDICO.MIMETYPE, MEDICO.CPF, MEDICO.RQE ");
                query.Append(" FROM SEG_MEDICO MEDICO ");
                query.Append(" JOIN SEG_ESTUDO_MEDICO ESTUDO_MEDICO ON ESTUDO_MEDICO.ID_MEDICO = MEDICO.ID_MEDICO ");
                query.Append(" JOIN SEG_ESTUDO ESTUDO ON ESTUDO.ID_ESTUDO = ESTUDO_MEDICO.ID_ESTUDO ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = MEDICO.ID_CIDADE ");
                query.Append(" WHERE ESTUDO_MEDICO.COORDENADOR = TRUE AND ESTUDO_MEDICO.SITUACAO = 'A' AND ESTUDO.ID_ESTUDO = :VALUE1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = pcmso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();


                while (dr.Read())
                {
                    medico = new Medico(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.IsDBNull(4) ? string.Empty :dr.GetString(4), dr.IsDBNull(5) ? string.Empty : dr.GetString(5), dr.IsDBNull(6) ? string.Empty : dr.GetString(6), dr.IsDBNull(7) ? string.Empty : dr.GetString(7), dr.IsDBNull(8) ? string.Empty : dr.GetString(8), dr.IsDBNull(9) ? string.Empty : dr.GetString(9), dr.IsDBNull(10) ? string.Empty : dr.GetString(10), dr.IsDBNull(11) ? string.Empty : dr.GetString(11), dr.IsDBNull(12) ? string.Empty : dr.GetString(12), dr.IsDBNull(13) ? null : new CidadeIbge(dr.GetInt64(13), dr.GetString(15), dr.GetString(16), dr.GetString(17)), dr.IsDBNull(14) ? String.Empty : dr.GetString(14), dr.IsDBNull(18) ? string.Empty : dr.GetString(18), dr.IsDBNull(19) ? string.Empty : dr.GetString(19), dr.IsDBNull(20) ? null : (byte[])dr.GetValue(20), dr.IsDBNull(21) ? string.Empty : dr.GetString(21), dr.IsDBNull(22) ? string.Empty : dr.GetString(22), dr.IsDBNull(23) ? string.Empty : dr.GetString(23));
                }
                
                return medico;


            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMedicoByEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Medico findMedicoCoordenadorByCliente(Cliente cliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMedicoCoordenadorByCliente");

            Medico medico = null;

            StringBuilder query = new StringBuilder();

            try
            {

                query.Append(" SELECT MEDICO.ID_MEDICO, MEDICO.NOME, MEDICO.CRM, MEDICO.SITUACAO, MEDICO.TELEFONE1, MEDICO.TELEFONE2, MEDICO.EMAIL, MEDICO.ENDERECO, ");
                query.Append(" MEDICO.NUMERO, MEDICO.COMPLEMENTO, MEDICO.BAIRRO, MEDICO.CEP, MEDICO.UF, MEDICO.ID_CIDADE, MEDICO.PIS_PASEP, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, MEDICO.UF_CRM, MEDICO.DOCUMENTO_EXTRA, MEDICO.ASSINATURA, MEDICO.MIMETYPE, MEDICO.CPF, MEDICO.RQE ");
                query.Append(" FROM SEG_MEDICO MEDICO  ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = MEDICO.ID_CIDADE ");
                query.Append(" JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_MEDICO = MEDICO.ID_MEDICO ");
                query.Append(" WHERE CLIENTE.ID_CLIENTE = :VALUE1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;

                NpgsqlDataReader dr = command.ExecuteReader();


                while (dr.Read())
                {
                    medico = new Medico(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.IsDBNull(4) ? string.Empty : dr.GetString(4), dr.IsDBNull(5) ? string.Empty : dr.GetString(5), dr.IsDBNull(6) ? string.Empty : dr.GetString(6), dr.IsDBNull(7) ? string.Empty : dr.GetString(7), dr.IsDBNull(8) ? string.Empty : dr.GetString(8), dr.IsDBNull(9) ? string.Empty : dr.GetString(9), dr.IsDBNull(10) ? string.Empty : dr.GetString(10), dr.IsDBNull(11) ? string.Empty : dr.GetString(11), dr.IsDBNull(12) ? string.Empty : dr.GetString(12), dr.IsDBNull(13) ? null : new CidadeIbge(dr.GetInt64(13), dr.GetString(15), dr.GetString(16), dr.GetString(17)), dr.IsDBNull(14) ? String.Empty : dr.GetString(14), dr.IsDBNull(18) ? string.Empty : dr.GetString(18), dr.IsDBNull(19) ? string.Empty : dr.GetString(19), dr.IsDBNull(20) ? null : (byte[])dr.GetValue(20), dr.IsDBNull(21) ? string.Empty : dr.GetString(21), dr.IsDBNull(22) ? string.Empty : dr.GetString(22), dr.IsDBNull(23) ? string.Empty : dr.GetString(23));

                }

                return medico;


            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMedicoCoordenadorByCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean findMedicoByCRM(String crm, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMedicoByCRM");

            StringBuilder query = new StringBuilder();

            Boolean medicoCrm = false;

            try
            {
                query.Append(" SELECT COUNT(*) FROM SEG_MEDICO WHERE CRM = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));

                command.Parameters[0].Value = crm;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    if (dr.GetInt64(0) > 0)
                    {
                        medicoCrm = true;
                    }

                }

                return medicoCrm;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMedicoByCRM", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Medico findMedicoByCrm(String crm, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMedicoByCrm");

            StringBuilder query = new StringBuilder();

            Medico medico = null;

            try
            {

                query.Append(" SELECT MEDICO.ID_MEDICO, MEDICO.NOME, MEDICO.CRM, MEDICO.SITUACAO, MEDICO.TELEFONE1, MEDICO.TELEFONE2, MEDICO.EMAIL, MEDICO.ENDERECO, MEDICO.NUMERO, ");
                query.Append(" MEDICO.COMPLEMENTO, MEDICO.BAIRRO, MEDICO.CEP, MEDICO.UF, MEDICO.ID_CIDADE, MEDICO.PIS_PASEP, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, MEDICO.UF_CRM, MEDICO.DOCUMENTO_EXTRA, MEDICO.ASSINATURA, MEDICO.MIMETYPE, MEDICO.CPF, MEDICO.RQE ");
                query.Append(" FROM SEG_MEDICO MEDICO ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = MEDICO.ID_CIDADE ");
                query.Append(" WHERE MEDICO.CRM = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = crm;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    medico = new Medico(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.IsDBNull(4) ? string.Empty : dr.GetString(4), dr.IsDBNull(5) ? string.Empty : dr.GetString(5), dr.IsDBNull(6) ? string.Empty : dr.GetString(6), dr.IsDBNull(7) ? string.Empty : dr.GetString(7), dr.IsDBNull(8) ? string.Empty : dr.GetString(8), dr.IsDBNull(9) ? string.Empty : dr.GetString(9), dr.IsDBNull(10) ? string.Empty : dr.GetString(10), dr.IsDBNull(11) ? string.Empty : dr.GetString(11), dr.IsDBNull(12) ? string.Empty : dr.GetString(12), dr.IsDBNull(13) ? null : new CidadeIbge(dr.GetInt64(13), dr.GetString(15), dr.GetString(16), dr.GetString(17)), dr.IsDBNull(14) ? String.Empty : dr.GetString(14), dr.IsDBNull(18) ? string.Empty : dr.GetString(18), dr.IsDBNull(19) ? string.Empty : dr.GetString(19), dr.IsDBNull(20) ? null : (byte[])dr.GetValue(20), dr.IsDBNull(21) ? string.Empty : dr.GetString(21), dr.IsDBNull(22) ? string.Empty : dr.GetString(22), dr.IsDBNull(23) ? string.Empty : dr.GetString(23));

                }
                return medico;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMedicoByCrm", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findMedicoCoordenadorNotInCliente(Cliente cliente, Medico medico, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMedicoCoordenadorNotInCliente");

            StringBuilder query = new StringBuilder();

            try
            {

                if (String.IsNullOrEmpty(medico.Nome))
                {

                    query.Append(" SELECT MEDICO.ID_MEDICO, MEDICO.NOME, MEDICO.CRM, MEDICO.SITUACAO, MEDICO.TELEFONE1, MEDICO.TELEFONE2, MEDICO.EMAIL, MEDICO.ENDERECO, ");
                    query.Append(" MEDICO.NUMERO, MEDICO.COMPLEMENTO, MEDICO.BAIRRO, MEDICO.CEP, CIDADE.NOME AS CIDADE, MEDICO.UF FROM SEG_MEDICO MEDICO  ");
                    query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = MEDICO.ID_CIDADE ");
                    query.Append(" WHERE MEDICO.ID_MEDICO NOT IN (SELECT ID_MEDICO FROM SEG_CLIENTE WHERE ID_CLIENTE = :VALUE1 ) ");
                    query.Append(" ORDER BY MEDICO.NOME ASC ");
                }
                else
                {
                    query.Append(" SELECT MEDICO.ID_MEDICO, MEDICO.NOME, MEDICO.CRM, MEDICO.SITUACAO, MEDICO.TELEFONE1, MEDICO.TELEFONE2, MEDICO.EMAIL, MEDICO.ENDERECO, ");
                    query.Append(" MEDICO.NUMERO, MEDICO.COMPLEMENTO, MEDICO.BAIRRO, MEDICO.CEP, CIDADE.NOME AS CIDADE, MEDICO.UF FROM SEG_MEDICO MEDICO  ");
                    query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = MEDICO.ID_CIDADE ");
                    query.Append(" WHERE MEDICO.ID_MEDICO NOT IN (SELECT ID_MEDICO FROM SEG_CLIENTE WHERE ID_CLIENTE = :VALUE1 ) AND  ");
                    query.Append(" MEDICO.NOME LIKE :VALUE2 ORDER BY MEDICO.NOME ASC ");
                }
                
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = cliente.Id;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = medico.Nome + "%";

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Medicos");

                return ds;
            
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMedicoCoordenadorNotInCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

    }
}