﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;
using NpgsqlTypes;
using System.Data;
using SWS.View.Resources;

namespace SWS.Dao
{
    class PlanoDao : IPlanoDao
    {
        
        public Plano insert(Plano plano, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            try
            {
                plano.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_PLANO( ID_PLANO, DESCRICAO, SITUACAO, CARGA) " );
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, FALSE )");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = plano.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = plano.Descricao;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = plano.Situacao;

                command.ExecuteNonQuery();

                return plano;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_PLANO_ID_PLANO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Plano update(Plano plano, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updatePlano");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_PLANO SET ");
                query.Append(" DESCRICAO = :VALUE2, SITUACAO = :VALUE3 ");
                query.Append(" WHERE ID_PLANO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = plano.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = plano.Descricao;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = plano.Situacao;


                command.ExecuteNonQuery();

                return plano;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updatePlano", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Plano findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            Plano plano = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT PLANO.ID_PLANO, PLANO.DESCRICAO, PLANO.SITUACAO, PLANO.CARGA ");
                query.Append(" FROM SEG_PLANO PLANO "); 
                query.Append(" WHERE PLANO.ID_PLANO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    plano = new Plano(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetBoolean(3));
                }

                return plano;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findByFilter(Plano plano, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByFilter");

            try
            {
                StringBuilder query = new StringBuilder();

                if (plano.isNullForFilter())
                {
                    query.Append(" SELECT ID_PLANO, DESCRICAO, SITUACAO, CARGA FROM SEG_PLANO ");
                }
                else
                {
                    query.Append(" SELECT ID_PLANO, DESCRICAO, SITUACAO, CARGA FROM SEG_PLANO WHERE TRUE ");
                    
                    if (!String.IsNullOrEmpty(plano.Descricao.Trim()))
                        query.Append(" AND DESCRICAO LIKE :VALUE1 ");

                    if (!String.IsNullOrEmpty(plano.Situacao))
                        query.Append(" AND SITUACAO = :VALUE2 ");
                }
                
                query.Append(" ORDER BY DESCRICAO ASC");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = "%" + plano.Descricao + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = plano.Situacao;
                
                DataSet ds = new DataSet();
                adapter.Fill(ds, "Planos");
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Plano findByDescricao(string descricao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByDescricao");

            Plano plano = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT PLANO.ID_PLANO, PLANO.DESCRICAO, PLANO.SITUACAO, PLANO.CARGA ");
                query.Append(" FROM SEG_PLANO PLANO ");
                query.Append(" WHERE PLANO.DESCRICAO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = descricao;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    plano = new Plano(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetBoolean(3));
                }

                return plano;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByDescricao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }


    }
}
