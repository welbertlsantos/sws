﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;
using SWS.Entidade;
using NpgsqlTypes;
using System.Data;

namespace SWS.Dao
{
    class CobrancaDao : ICobrancaDao
    {
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_CRC_ID_CRC_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Cobranca insert(Cobranca cobranca, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            try
            {
                cobranca.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_CRC( ID_CRC, ID_FORMA, ID_USUARIO_CRIADOR, ID_CONTA, ");
                query.Append(" ID_NFE, VALOR, DATA_EMISSAO, DATA_VENCIMENTO, SITUACAO, NUMERO_PARCELA, NUMERO_DOCUMENTO, ID_CRC_DESDOBRAMENTO, NOSSO_NUMERO, VALOR_AJUSTADO) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6, :VALUE7, :VALUE8, :VALUE9, :VALUE10, :VALUE11, :VALUE12, :VALUE13, :VALUE14)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cobranca.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = cobranca.Forma.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = cobranca.UsuarioCriador.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = cobranca.Conta != null ? cobranca.Conta.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Integer));
                command.Parameters[4].Value = cobranca.Nota.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Numeric));
                command.Parameters[5].Value = cobranca.Valor;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Timestamp));
                command.Parameters[6].Value = cobranca.DataEmissao;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Date));
                command.Parameters[7].Value = cobranca.DataVencimento;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                command.Parameters[8].Value = cobranca.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Integer));
                command.Parameters[9].Value = cobranca.NumeroParcela;

                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Varchar));
                command.Parameters[10].Value = cobranca.NumeroDocumento;

                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Integer));
                command.Parameters[11].Value = cobranca.CobrancaDesdobrada != null ? cobranca.CobrancaDesdobrada.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Varchar));
                command.Parameters[12].Value = cobranca.NossoNumero != null ? cobranca.NossoNumero : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Numeric));
                command.Parameters[13].Value = cobranca.Valor;

                command.ExecuteNonQuery();

                return cobranca;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findByFilter(Cobranca cobranca, Cliente cliente, Nfe nfe, DateTime? dataIEmissaoInicial, DateTime? dataEmissaoFinal, DateTime? dataVencimentoInicial, DateTime? dataVencimentoFinal, DateTime? dataBaixaInicial, DateTime? dataBaixaFinal, DateTime? dataCancelamentoInicial, DateTime? dataCancelamentoFinal, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByFilter");

            try
            {
                StringBuilder query = new StringBuilder();
                bool situacao = false;
                bool isNfe = false;
                bool isCliente = false;
                bool isEmissao = false;
                bool isVencimento = false;
                bool isBaixa = false;
                bool isCancelamento = false;

                query.Append("SELECT COBRANCA.ID_CRC, NFE.NUMERO_NFE, FORMA.DESCRICAO, COBRANCA.NUMERO_PARCELA, COBRANCA.SITUACAO, COBRANCA.VALOR_AJUSTADO, COBRANCA.DATA_EMISSAO, ");
                query.Append("COBRANCA.DATA_VENCIMENTO, COBRANCA.DATA_PAGAMENTO, COBRANCA.NUMERO_DOCUMENTO, CONTA.NOME, CONTA.AGENCIA_NUMERO, ");
                query.Append("CONTA.AGENCIA_DIGITO, ");
                query.Append("CONTA.CONTA_NUMERO, CONTA.CONTA_DIGITO, COBRANCA.OBSERVACAO_CANCELAMENTO, CLIENTE.RAZAO_SOCIAL, COBRANCA.NOSSO_NUMERO, ");
                query.Append("COBRANCA.VALOR, COBRANCA.VALOR_BAIXADO ");
                query.Append("FROM SEG_CRC COBRANCA ");
                query.Append("JOIN SEG_NFE NFE ON NFE.ID_NFE = COBRANCA.ID_NFE ");
                query.Append("JOIN SEG_FORMA FORMA ON FORMA.ID_FORMA = COBRANCA.ID_FORMA ");
                query.Append("LEFT JOIN SEG_CONTA CONTA ON CONTA.ID_CONTA = COBRANCA.ID_CONTA ");
                query.Append("JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = NFE.ID_CLIENTE ");
                query.Append("WHERE TRUE ");

                if (!string.IsNullOrEmpty(cobranca.Situacao))
                {
                    situacao = true;
                    query.Append("AND COBRANCA.SITUACAO = :VALUE1 ");
                }

                if (cliente != null)
                {
                    isCliente = true;
                    query.Append("AND NFE.ID_CLIENTE = :VALUE2 ");
                }

                if (nfe != null)
                {
                    isNfe = true;
                    query.Append("AND NFE.NUMERO_NFE = :VALUE3 ");
                }

                if (dataIEmissaoInicial != null && dataEmissaoFinal != null)
                {
                    isEmissao = true;
                    query.Append("AND COBRANCA.DATA_EMISSAO BETWEEN :VALUE4 AND :VALUE5 ");
                }

                if (dataVencimentoInicial != null && dataVencimentoFinal != null)
                {
                    isVencimento = true;
                    query.Append("AND COBRANCA.DATA_VENCIMENTO BETWEEN :VALUE6 AND :VALUE7 ");
                }

                if (dataBaixaInicial != null && dataBaixaFinal != null)
                {
                    isBaixa = true;
                    query.Append("AND COBRANCA.DATA_BAIXA BETWEEN :VALUE8 AND :VALUE9 ");
                }

                if (dataCancelamentoInicial != null && dataCancelamentoFinal != null)
                {
                    isCancelamento = true;
                    query.Append("AND COBRANCA.DATA_CANCELAMENTO BETWEEN :VALUE10 AND :VALUE11 ");
                }

                query.Append(" ORDER BY NUMERO_NFE ASC ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = cobranca.Situacao;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[1].Value = cliente != null ? cliente.Id : (null);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[2].Value = nfe != null ? nfe.NumeroNfe : null;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Date));
                adapter.SelectCommand.Parameters[3].Value = dataIEmissaoInicial;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Date));
                adapter.SelectCommand.Parameters[4].Value = dataEmissaoFinal;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Date));
                adapter.SelectCommand.Parameters[5].Value = dataVencimentoInicial;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Date));
                adapter.SelectCommand.Parameters[6].Value = dataVencimentoFinal;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Date));
                adapter.SelectCommand.Parameters[7].Value = dataBaixaInicial;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Date));
                adapter.SelectCommand.Parameters[8].Value = dataBaixaFinal;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Date));
                adapter.SelectCommand.Parameters[9].Value = dataCancelamentoInicial;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Date));
                adapter.SelectCommand.Parameters[10].Value = dataCancelamentoFinal;

                DataSet ds = new DataSet();
                adapter.Fill(ds, "Cobrancas");

                /* preenchendo totais */
                query.Clear();

                query.Append("SELECT (SELECT COUNT(*) FROM SEG_CRC COBRANCA ");
                query.Append("JOIN SEG_NFE NFE ON NFE.ID_NFE = COBRANCA.ID_NFE ");
                query.Append("JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = NFE.ID_CLIENTE ");
                query.Append("WHERE TRUE ");

                if (situacao)
                    query.Append("AND COBRANCA.SITUACAO = :VALUE1 ");

                if (isCliente)
                    query.Append("AND CLIENTE.ID_CLIENTE = :VALUE2 ");

                if (isNfe)
                    query.Append("AND NFE.NUMERO_NFE = :VALUE3 ");

                if (isEmissao)
                    query.Append("AND COBRANCA.DATA_EMISSAO BETWEEN :VALUE4 AND :VALUE5 ");

                if (isVencimento)
                    query.Append("AND COBRANCA.DATA_VENCIMENTO BETWEEN :VALUE6 AND :VALUE7 ");

                if (isBaixa)
                    query.Append("AND COBRANCA.DATA_BAIXA BETWEEN :VALUE8 AND :VALUE9 ");

                if (isCancelamento)
                    query.Append("AND COBRANCA.DATA_CANCELAMENTO BETWEEN :VALUE10 AND :VALUE11 ");


                query.Append(" ) AS QUANTIDADE , ");

                query.Append(" (SELECT SUM(VALOR) FROM SEG_CRC COBRANCA ");
                query.Append("JOIN SEG_NFE NFE ON NFE.ID_NFE = COBRANCA.ID_NFE ");
                query.Append("JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = NFE.ID_CLIENTE ");
                query.Append("WHERE TRUE ");

                if (situacao)
                    query.Append("AND COBRANCA.SITUACAO = :VALUE1 ");

                if (isCliente)
                    query.Append("AND CLIENTE.ID_CLIENTE = :VALUE2 ");

                if (isNfe)
                    query.Append("AND NFE.NUMERO_NFE = :VALUE3 ");

                if (isEmissao)
                    query.Append("AND COBRANCA.DATA_EMISSAO BETWEEN :VALUE4 AND :VALUE5 ");

                if (isVencimento)
                    query.Append("AND COBRANCA.DATA_VENCIMENTO BETWEEN :VALUE6 AND :VALUE7 ");

                if (isBaixa)
                    query.Append("AND COBRANCA.DATA_BAIXA BETWEEN :VALUE8 AND :VALUE9 ");

                if (isCancelamento)
                    query.Append("AND COBRANCA.DATA_CANCELAMENTO BETWEEN :VALUE10 AND :VALUE11 ");

                query.Append(" ) AS VALOR ");

                NpgsqlDataAdapter adapterTotal = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapterTotal.SelectCommand.Parameters[0].Value = cobranca.Situacao;

                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                adapterTotal.SelectCommand.Parameters[1].Value = cliente != null ? cliente.Id : (null);

                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                adapterTotal.SelectCommand.Parameters[2].Value = nfe != null ? nfe.NumeroNfe : null;

                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Date));
                adapterTotal.SelectCommand.Parameters[3].Value = dataIEmissaoInicial;

                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Date));
                adapterTotal.SelectCommand.Parameters[4].Value = dataEmissaoFinal;

                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Date));
                adapterTotal.SelectCommand.Parameters[5].Value = dataVencimentoInicial;

                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Date));
                adapterTotal.SelectCommand.Parameters[6].Value = dataVencimentoFinal;

                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Date));
                adapterTotal.SelectCommand.Parameters[7].Value = dataBaixaInicial;

                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Date));
                adapterTotal.SelectCommand.Parameters[8].Value = dataBaixaFinal;

                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Date));
                adapterTotal.SelectCommand.Parameters[9].Value = dataCancelamentoInicial;

                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Date));
                adapterTotal.SelectCommand.Parameters[10].Value = dataCancelamentoFinal;

                adapterTotal.Fill(ds, "Totais");

                return ds;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Cobranca findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            Cobranca cobranca = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT COBRANCA.ID_CRC, COBRANCA.ID_FORMA, COBRANCA.ID_USUARIO_CANCELA, COBRANCA.ID_USUARIO_ESTORNA, COBRANCA.ID_USUARIO_DESDOBRA, ");
                query.Append(" COBRANCA.ID_USUARIO_CRIADOR, COBRANCA.ID_CONTA, COBRANCA.ID_NFE, COBRANCA.VALOR, COBRANCA.DATA_EMISSAO, COBRANCA.DATA_VENCIMENTO, ");
                query.Append(" COBRANCA.DATA_PAGAMENTO, COBRANCA.SITUACAO, COBRANCA.NUMERO_PARCELA, COBRANCA.NUMERO_DOCUMENTO, COBRANCA.DATA_ALTERACAO, ");
                query.Append(" COBRANCA.OBSERVACAO_CANCELAMENTO, COBRANCA.ID_CRC_DESDOBRAMENTO, COBRANCA.OBSERVACAO_COBRANCA, COBRANCA.OBSERVACAO_BAIXA, ");
                query.Append(" COBRANCA.DATA_BAIXA, COBRANCA.ID_USUARIO_BAIXA, COBRANCA.OBSERVACAO_ESTORNO_BAIXA, COBRANCA.DATA_ESTORNO, COBRANCA.DATA_CANCELAMENTO, ");
                query.Append(" COBRANCA.ID_USUARIO_PRORROGACAO, COBRANCA.OBSERVACAO_PRORROGACAO, COBRANCA.VALOR_BAIXADO, COBRANCA.VALOR_AJUSTADO, BANCO.ID_BANCO, BANCO.NOME, FORMA.DESCRICAO, COBRANCA.NOSSO_NUMERO ");
                query.Append(" FROM SEG_CRC COBRANCA ");
                query.Append(" LEFT JOIN SEG_FORMA FORMA ON FORMA.ID_FORMA = COBRANCA.ID_FORMA  ");
                query.Append(" LEFT JOIN SEG_CONTA CONTA ON CONTA.ID_CONTA = COBRANCA.ID_CONTA ");
                query.Append(" LEFT JOIN SEG_BANCO BANCO ON BANCO.ID_BANCO = CONTA.ID_BANCO  ");
                query.Append(" WHERE COBRANCA.ID_CRC = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    cobranca = new Cobranca(dr.GetInt64(0), new Forma(dr.GetInt64(1), dr.GetString(31)), dr.IsDBNull(2) ? null : new Usuario(dr.GetInt64(2)), dr.IsDBNull(3) ? null : new Usuario(dr.GetInt64(3)), dr.IsDBNull(4) ? null : new Usuario(dr.GetInt64(4)), dr.IsDBNull(5) ? null : new Usuario(dr.GetInt64(5)), dr.IsDBNull(21) ? null : new Usuario(dr.GetInt64(21)), dr.IsDBNull(6) ? null : new Conta(dr.GetInt64(6), dr.IsDBNull(29) ? null : new Banco(dr.GetInt64(29), dr.GetString(30), null, false, null, false, false), null, null, null, null, null, null, null, null, null, null, null, false, null, false), dr.IsDBNull(7) ? null : new Nfe(dr.GetInt64(7), null, null, (long?)null, 0, DateTime.Now, null, null, 0, 0, null, null, null, null, DateTime.Now, null, string.Empty, null, string.Empty, null, null, null), dr.GetDecimal(8), dr.GetDateTime(9), dr.GetDateTime(10), dr.IsDBNull(11) ? null : (DateTime?)dr.GetDateTime(11), dr.GetString(12), dr.GetInt32(13), dr.GetString(14), dr.IsDBNull(15) ? null : (DateTime?)dr.GetDateTime(15), dr.IsDBNull(16) ? String.Empty : dr.GetString(16), dr.IsDBNull(17) ? null : new Cobranca(dr.GetInt64(17)), dr.IsDBNull(18) ? String.Empty : dr.GetString(18), dr.IsDBNull(19) ? String.Empty : dr.GetString(19), dr.IsDBNull(20) ? null : (DateTime?)dr.GetDateTime(20), dr.IsDBNull(22) ? String.Empty : dr.GetString(22), dr.IsDBNull(23) ? null : (DateTime?)dr.GetDateTime(23), dr.IsDBNull(25) ? null : new Usuario(dr.GetInt64(25)), dr.IsDBNull(26) ? String.Empty : dr.GetString(26), dr.IsDBNull(27) ? null : (Decimal?)dr.GetDecimal(27), dr.IsDBNull(28) ? null : (Decimal?)dr.GetDecimal(28), dr.IsDBNull(32) ? String.Empty : dr.GetString(32));
                }

                return cobranca;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void baixarCobranca(Cobranca cobranca, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método baixarCobranca");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_CRC SET ");
                query.Append(" DATA_PAGAMENTO = :VALUE2, ");
                query.Append(" SITUACAO = :VALUE3, ");
                query.Append(" OBSERVACAO_BAIXA = :VALUE4, ");
                query.Append(" ID_USUARIO_BAIXA = :VALUE5, ");
                query.Append(" DATA_BAIXA = NOW(), ");
                query.Append(" VALOR_BAIXADO = :VALUE6, ");
                query.Append(" ID_CONTA = :VALUE7 ");
                query.Append(" WHERE ID_CRC = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cobranca.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Date));
                command.Parameters[1].Value = cobranca.DataPagamento;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = cobranca.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Text));
                command.Parameters[3].Value = cobranca.ObservacaoBaixa;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Integer));
                command.Parameters[4].Value = cobranca.UsuarioBaixa.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Double));
                command.Parameters[5].Value = cobranca.ValorBaixado;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Integer));
                command.Parameters[6].Value = cobranca.Conta.Id;

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - baixarCobranca", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void estornarBaixaCobranca(Cobranca cobranca, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método estornarBaixaCobranca");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_CRC SET ");
                query.Append(" DATA_PAGAMENTO = NULL, ");
                query.Append(" VALOR_BAIXADO = NULL, ");
                query.Append(" SITUACAO = :VALUE3, ");
                //OS DADOS DA BAIXA NÃO SERÃO LIMPOS, A NÃO SER A DATA DO PAGAMENTO
                //PARA IDENTIFICAR OS DADOS FEITOS ANTERIORMENTE
                //query.Append(" OBSERVACAO_BAIXA = NULL, ");
                //query.Append(" ID_USUARIO_BAIXA = NULL, ");
                //query.Append(" DATA_BAIXA = NULL, ");
                query.Append(" DATA_ESTORNO = NOW(), ");
                query.Append(" ID_USUARIO_ESTORNA = :VALUE2, ");
                query.Append(" OBSERVACAO_ESTORNO_BAIXA = :VALUE4 ");
                query.Append(" WHERE ID_CRC = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cobranca.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = cobranca.UsuarioEstorna.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = cobranca.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Text));
                command.Parameters[3].Value = cobranca.ObservacaoEstorno;

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - estornarBaixaCobranca", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void cancelarCobranca(Cobranca cobranca, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método cancelarCobranca");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_CRC SET ");
                query.Append(" SITUACAO = :VALUE3, ");
                query.Append(" DATA_CANCELAMENTO = NOW(), ");
                query.Append(" ID_USUARIO_CANCELA = :VALUE2, ");
                query.Append(" OBSERVACAO_CANCELAMENTO = :VALUE4 ");
                query.Append(" WHERE ID_CRC = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cobranca.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = cobranca.UsuarioCancela.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = cobranca.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Text));
                command.Parameters[3].Value = cobranca.ObservacaoCancelamento;

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - cancelarCobranca", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void reativarCobranca(Cobranca cobranca, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método reativarCobranca");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_CRC SET ");
                query.Append(" SITUACAO = :VALUE2 ");
                query.Append(" WHERE ID_CRC = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cobranca.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = cobranca.Situacao;

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - reativarCobranca", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void alteraFormaPagamento(Cobranca cobranca, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alteraFormaPagamento");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_CRC SET ");
                query.Append(" ID_FORMA = :VALUE2, ");
                query.Append(" ID_CONTA = :VALUE3, ");
                query.Append(" DATA_ALTERACAO = NOW(), ");
                query.Append(" NOSSO_NUMERO = :VALUE4 ");
                query.Append(" WHERE ID_CRC = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cobranca.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = cobranca.Forma != null ? cobranca.Forma.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = cobranca.Conta != null ? cobranca.Conta.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = cobranca.NossoNumero;

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alteraFormaPagamento", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public LinkedList<Cobranca> findCobrancasByNotafiscal(Nfe notaFiscal, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findCobrancasByNotafiscal");

            try
            {
                StringBuilder query = new StringBuilder();

                LinkedList<Cobranca> cobrancas = new LinkedList<Cobranca>();

                query.Append(" SELECT COBRANCA.ID_CRC, COBRANCA.ID_FORMA, COBRANCA.ID_USUARIO_CANCELA, COBRANCA.ID_USUARIO_ESTORNA, COBRANCA.ID_USUARIO_DESDOBRA, "); //4
                query.Append(" COBRANCA.ID_USUARIO_CRIADOR, COBRANCA.ID_CONTA, COBRANCA.ID_NFE, COBRANCA.VALOR, COBRANCA.DATA_EMISSAO, COBRANCA.DATA_PAGAMENTO, COBRANCA.SITUACAO,   "); //11
                query.Append(" COBRANCA.NUMERO_PARCELA, COBRANCA.NUMERO_DOCUMENTO, COBRANCA.DATA_ALTERACAO, COBRANCA.OBSERVACAO_CANCELAMENTO, COBRANCA.ID_CRC_DESDOBRAMENTO, "); //16
                query.Append(" COBRANCA.OBSERVACAO_COBRANCA, COBRANCA.OBSERVACAO_BAIXA, COBRANCA.DATA_BAIXA, COBRANCA.ID_USUARIO_BAIXA, COBRANCA.OBSERVACAO_ESTORNO_BAIXA,   "); //21
                query.Append(" COBRANCA.DATA_ESTORNO, FORMA.DESCRICAO, BANCO.ID_BANCO, COBRANCA.DATA_VENCIMENTO, BANCO.NOME, COBRANCA.ID_USUARIO_PRORROGACAO, COBRANCA.OBSERVACAO_PRORROGACAO, COBRANCA.VALOR_BAIXADO, COBRANCA.VALOR_AJUSTADO, COBRANCA.NOSSO_NUMERO "); //31
                query.Append(" FROM SEG_CRC COBRANCA ");
                query.Append(" LEFT JOIN SEG_FORMA FORMA ON FORMA.ID_FORMA = COBRANCA.ID_FORMA  ");
                query.Append(" LEFT JOIN SEG_CONTA CONTA ON CONTA.ID_CONTA = COBRANCA.ID_CONTA ");
                query.Append(" LEFT JOIN SEG_BANCO BANCO ON BANCO.ID_BANCO = CONTA.ID_BANCO  ");
                query.Append(" WHERE COBRANCA.ID_NFE = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = notaFiscal.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    cobrancas.AddLast(new Cobranca(dr.GetInt64(0), new Forma(dr.GetInt64(1), dr.GetString(23)), dr.IsDBNull(2) ? null : new Usuario(dr.GetInt64(2)), dr.IsDBNull(3) ? null : new Usuario(dr.GetInt64(3)), dr.IsDBNull(4) ? null : new Usuario(dr.GetInt64(4)), dr.IsDBNull(5) ? null : new Usuario(dr.GetInt64(5)), dr.IsDBNull(20) ? null : new Usuario(dr.GetInt64(20)), dr.IsDBNull(6) ? null : new Conta(dr.GetInt64(6), dr.IsDBNull(24) ? null : new Banco(dr.GetInt64(24), dr.GetString(26), null, false, null, false, false), null, null, null, null, null, null, null, null, null, null, null, false, null, false), notaFiscal, dr.GetDecimal(8), dr.GetDateTime(9), dr.GetDateTime(25), dr.IsDBNull(10) ? null : (DateTime?)dr.GetDateTime(10), dr.GetString(11), dr.GetInt32(12), dr.GetString(13), dr.IsDBNull(14) ? null : (DateTime?)dr.GetDateTime(14), dr.IsDBNull(15) ? String.Empty : dr.GetString(15), dr.IsDBNull(16) ? null : new Cobranca(dr.GetInt64(16)), dr.IsDBNull(17) ? String.Empty : dr.GetString(17), dr.IsDBNull(18) ? String.Empty : dr.GetString(18), dr.IsDBNull(19) ? null : (DateTime?)dr.GetDateTime(19), dr.IsDBNull(21) ? String.Empty : dr.GetString(21), dr.IsDBNull(22) ? null : (DateTime?)dr.GetDateTime(22), dr.IsDBNull(27) ? null : new Usuario(dr.GetInt64(27)), dr.IsDBNull(28) ? String.Empty : dr.GetString(28), dr.IsDBNull(29) ? null : (Decimal?)dr.GetDecimal(29), dr.IsDBNull(30) ? null : (Decimal?)dr.GetDecimal(30), dr.IsDBNull(31) ? String.Empty : dr.GetString(31))); 
                }

                return cobrancas;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMovimentosByNotaFiscal", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }


        public void prorrogar(Cobranca cobranca, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método prorrogar");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_CRC SET ");
                query.Append(" DATA_VENCIMENTO = :VALUE2, ");
                query.Append(" OBSERVACAO_PRORROGACAO = :VALUE3, ");
                query.Append(" ID_USUARIO_PRORROGACAO = :VALUE4, ");
                query.Append(" VALOR_AJUSTADO = :VALUE5, ");
                query.Append(" DATA_ALTERACAO = NOW() ");
                query.Append(" WHERE ID_CRC = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cobranca.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Date));
                command.Parameters[1].Value = cobranca.DataVencimento;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Text));
                command.Parameters[2].Value = cobranca.ObservacaoProrrogacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = cobranca.UsuarioProrroga.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Double));
                command.Parameters[4].Value = cobranca.ValorAjustado;

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - prorrogar", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void alteraSituacaoCobranca(Cobranca cobranca, String situacao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alteraSituacaoCobranca");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_CRC SET ");
                query.Append(" SITUACAO = :VALUE2 ");
                query.Append(" WHERE ID_CRC = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cobranca.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = situacao;

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alteraSituacaoCobranca", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
