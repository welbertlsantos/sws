﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using NpgsqlTypes;
using Npgsql;
using System.Data;
using SWS.Helper;
using SWS.View.Resources;
using SWS.Excecao;

namespace SWS.Dao
{
    class RelatorioDao :IRelatorioDao
    {

        public DataSet findAllRelatoriosBySituacao(String tipo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllRelatoriosBySituacao");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID_RELATORIO,  NOME_RELATORIO, TIPO, DATA_CRIACAO, SITUACAO, NMRELATORIO ");
                query.Append(" FROM SEG_RELATORIO WHERE TIPO = :VALUE1 AND SITUACAO = 'A' ORDER BY NOME_RELATORIO ASC ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = tipo;

                DataSet ds = new DataSet();
                adapter.Fill(ds, "Relatorios");
                return ds;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAsoByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 findUltimaVersaoByTipo(String tipoRelatorio, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findUltimaVersaoByTipo");

            Int64 idRelatorio = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT ID_RELATORIO FROM SEG_RELATORIO WHERE TIPO = :VALUE1 AND DATA_CRIACAO = (SELECT MAX(DATA_CRIACAO) FROM SEG_RELATORIO WHERE TIPO = :VALUE1 AND SITUACAO = 'A')", dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = tipoRelatorio;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    idRelatorio = dr.GetInt64(0);

                }

                return idRelatorio;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findUltimaVersaoByTipo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public String findNomeRelatorioById(Int64? idRelatorio, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findNomeRelatorioById");

            String nomeRelatorio = null;
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NOME_RELATORIO FROM SEG_RELATORIO WHERE ID_RELATORIO = :VALUE1", dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = idRelatorio;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    nomeRelatorio = dr.GetString(0);

                }

                return nomeRelatorio;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findNomeRelatorioById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Relatorio findById(long id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            Relatorio relatorio = null;
            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" SELECT ID_RELATORIO, NOME_RELATORIO, TIPO, DATA_CRIACAO, SITUACAO, NMRELATORIO FROM SEG_RELATORIO WHERE ID_RELATORIO = :VALUE1", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    relatorio = new Relatorio(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetDateTime(3), dr.GetString(4), dr.GetString(5));
                }

                return relatorio;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
