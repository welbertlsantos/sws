﻿using Npgsql;
using NpgsqlTypes;
using SWS.Entidade;
using SWS.Excecao;
using SWS.Helper;
using SWS.IDao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Dao
{
    class EpiMonitoramentoDao : IEpiMonitoramentoDao
    {
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_EPI_MONITORAMENTO_ID_EPI_MONITORAMENTO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public EpiMonitoramento insert(EpiMonitoramento epiMonitoramento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            try
            {
                epiMonitoramento.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_EPI_MONITORAMENTO (ID_EPI_MONITORAMENTO, ID_EPI,  ID_MONITORAMENTO_GHEFONTEAGENTE) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3) ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = epiMonitoramento.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = epiMonitoramento.Epi != null ? epiMonitoramento.Epi.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = epiMonitoramento.MonitoramentoGheFonteAgente.Id;

                command.ExecuteNonQuery();

                return epiMonitoramento;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public EpiMonitoramento update(EpiMonitoramento epiMonitoramento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_EPI_MONITORAMENTO SET ");
                query.Append(" ID_EPI = :VALUE2, ID_MONITORAMENTO_GHEFONTEAGENTE = :VALUE3 ");
                query.Append(" WHERE ID_EPI_MONITORAMENTO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = epiMonitoramento.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = epiMonitoramento.Epi != null ? epiMonitoramento.Epi.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = epiMonitoramento.MonitoramentoGheFonteAgente.Id;

                command.ExecuteNonQuery();

                return epiMonitoramento;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void delete(EpiMonitoramento epiMonitoramento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" DELETE FROM SEG_EPI_MONITORAMENTO ");
                query.Append(" WHERE ID_EPI_MONITORAMENTO = :VALUE1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = epiMonitoramento.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public EpiMonitoramento findById(long id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            EpiMonitoramento epiMonitoramento = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID_EPI_MONITORAMENTO, ID_EPI, ID_MONITORAMENTO_GHEFONTEAGENTE ");
                query.Append(" FROM SEG_EPI_MONITORAMENTO  ");
                query.Append(" WHERE ID_EPI_MONITORAMENTO = :VALUE1  ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    epiMonitoramento = new EpiMonitoramento(dr.GetInt64(0), new Epi(dr.GetInt64(1)), new MonitoramentoGheFonteAgente(dr.GetInt64(2)));
                }

                return epiMonitoramento;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }


        public List<EpiMonitoramento> findAllByMonitoramentoGheFonteAgente(MonitoramentoGheFonteAgente monitoramentoAgente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByMonitoramentoGheFonteAgente");

            List<EpiMonitoramento> epiMonitoramentos = new List<EpiMonitoramento>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID_EPI_MONITORAMENTO, ID_EPI, ID_MONITORAMENTO_GHEFONTEAGENTE ");
                query.Append(" FROM SEG_EPI_MONITORAMENTO ");
                query.Append(" WHERE ID_MONITORAMENTO_GHEFONTEAGENTE  = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = monitoramentoAgente.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    epiMonitoramentos.Add(new EpiMonitoramento(dr.GetInt64(0), new Epi(dr.GetInt64(1)), new MonitoramentoGheFonteAgente(dr.GetInt64(2))));
                }

                return epiMonitoramentos;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByMonitoramentoGheFonteAgente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }



    }
}
