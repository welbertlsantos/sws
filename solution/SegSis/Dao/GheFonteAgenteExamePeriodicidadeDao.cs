﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SWS.IDao;
using SWS.Entidade;
using Npgsql;
using NpgsqlTypes;
using SWS.Excecao;
using SWS.Helper;
using SWS.View.Resources;

namespace SWS.Dao
{
    class GheFonteAgenteExamePeriodicidadeDao : IGheFonteAgenteExamePeriodicidadeDao
    {
        public void insert(GheFonteAgenteExamePeriodicidade gheFonteAgenteExamePeriodicidade, NpgsqlConnection dbconnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertGheFonteAgenteExamePeriodicidade");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" INSERT INTO SEG_GHE_FONTE_AGENTE_EXAME_PERIODICIDADE ");
                query.Append(" (ID_PERIODICIDADE, ID_GHE_FONTE_AGENTE_EXAME, SITUACAO, PERIODO_VENCIMENTO ) VALUES (:VALUE1, :VALUE2, 'A', :VALUE3)");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbconnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonteAgenteExamePeriodicidade.Periodicidade.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = gheFonteAgenteExamePeriodicidade.GheFonteAgenteExame.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = gheFonteAgenteExamePeriodicidade.PeriodoVencimento; 

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertGheFonteAgenteExamePeriodicidade", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public HashSet<GheFonteAgenteExamePeriodicidade> findAll(GheFonteAgenteExame gheFonteAgenteExame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAll");

            HashSet<GheFonteAgenteExamePeriodicidade> gheFonteAgenteExamePeriodicidades = new HashSet<GheFonteAgenteExamePeriodicidade>();
            
            try
            {

                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.ID_PERIODICIDADE, GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.ID_GHE_FONTE_AGENTE_EXAME, GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.SITUACAO, GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.PERIODO_VENCIMENTO ");
                query.Append(" FROM SEG_GHE_FONTE_AGENTE_EXAME_PERIODICIDADE GHE_FONTE_AGENTE_EXAME_PERIODICIDADE WHERE GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.ID_GHE_FONTE_AGENTE_EXAME = :VALUE1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonteAgenteExame.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gheFonteAgenteExamePeriodicidades.Add(new GheFonteAgenteExamePeriodicidade(new Periodicidade(dr.GetInt64(0)), new GheFonteAgenteExame(dr.GetInt64(1)), dr.GetString(2), dr.IsDBNull(3) ? (int?)null : dr.GetInt32(3)));
                }

                return gheFonteAgenteExamePeriodicidades;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAll", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void update(GheFonteAgenteExamePeriodicidade gheFonteAgenteExamePeriodicidade, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_GHE_FONTE_AGENTE_EXAME_PERIODICIDADE SET SITUACAO = :VALUE1, PERIODO_VENCIMENTO = :VALUE4 WHERE ID_GHE_FONTE_AGENTE_EXAME = :VALUE2 AND ID_PERIODICIDADE = :VALUE3 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = gheFonteAgenteExamePeriodicidade.Situacao;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = gheFonteAgenteExamePeriodicidade.GheFonteAgenteExame.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = gheFonteAgenteExamePeriodicidade.Periodicidade.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = gheFonteAgenteExamePeriodicidade.PeriodoVencimento;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void delete(GheFonteAgenteExamePeriodicidade gheFonteAgenteExamePeriodicidade, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" DELETE FROM SEG_GHE_FONTE_AGENTE_EXAME_PERIODICIDADE WHERE ID_GHE_FONTE_AGENTE_EXAME = :VALUE1 AND ID_PERIODICIDADE = :VALUE2");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonteAgenteExamePeriodicidade.GheFonteAgenteExame.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = gheFonteAgenteExamePeriodicidade.Periodicidade.Id;
                
                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public bool isUsedInService(GheFonteAgenteExamePeriodicidade gheFonteAgenteExamePeriodicidade, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método isUsedInService");

            bool result = false;

            try
            {

                StringBuilder query = new StringBuilder();

                query.Append(" SELECT DISTINCT GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.* FROM SEG_ASO ATENDIMENTO ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GHE_FONTE_AGENTE_EXAME_ASO ON GHE_FONTE_AGENTE_EXAME_ASO.ID_ASO = ATENDIMENTO.ID_ASO ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GHE_FONTE_AGENTE_EXAME ON GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME = GHE_FONTE_AGENTE_EXAME_ASO.ID_GHE_FONTE_AGENTE_EXAME");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME_PERIODICIDADE GHE_FONTE_AGENTE_EXAME_PERIODICIDADE ON GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.ID_GHE_FONTE_AGENTE_EXAME = GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME");
                query.Append(" WHERE GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.ID_GHE_FONTE_AGENTE_EXAME = :VALUE1 AND GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.ID_PERIODICIDADE = :VALUE2 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonteAgenteExamePeriodicidade.GheFonteAgenteExame.Id;

                command.Parameters.Add(new NpgsqlParameter("value2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = gheFonteAgenteExamePeriodicidade.Periodicidade.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    result = true;
                }

                return result;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - isUsedInService", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<GheFonteAgenteExamePeriodicidade> listAllByGheByPeriodicidade(List<Ghe> ghes, Periodicidade periodicidade, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método listAllByGheByPeriodicidade");

            List<GheFonteAgenteExamePeriodicidade> gheFonteAgenteExamePeriodicidade = new List<GheFonteAgenteExamePeriodicidade>();
            int count = 0;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.ID_GHE_FONTE_AGENTE_EXAME, GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.SITUACAO, GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.PERIODO_VENCIMENTO, GHE_FONTE_AGENTE_EXAME.ID_EXAME, GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE, GHE_FONTE_AGENTE_EXAME.IDADE_EXAME, GHE_FONTE_AGENTE_EXAME.SITUACAO, GHE_FONTE_AGENTE_EXAME.CONFINADO, GHE_FONTE_AGENTE_EXAME.ALTURA, GHE_FONTE_AGENTE.ID_GRAD_SOMA, GHE_FONTE_AGENTE.ID_GRAD_EFEITO, GHE_FONTE_AGENTE.ID_GRAD_EXPOSICAO, GHE_FONTE_AGENTE.ID_GHE_FONTE, GHE_FONTE_AGENTE.ID_AGENTE, GHE_FONTE_AGENTE.TEMPO_EXPOSICAO, GHE_FONTE_AGENTE.SITUACAO, GHE_FONTE.ID_FONTE, GHE_FONTE.ID_GHE, GHE_FONTE.PRINCIPAL, GHE_FONTE.SITUACAO, GHE_FONTE_AGENTE_EXAME.ELETRICIDADE "); 
                query.Append(" FROM SEG_GHE_FONTE_AGENTE_EXAME_PERIODICIDADE GHE_FONTE_AGENTE_EXAME_PERIODICIDADE  ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GHE_FONTE_AGENTE_EXAME ON GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME = GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.ID_GHE_FONTE_AGENTE_EXAME ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE GHE_FONTE_AGENTE ON GHE_FONTE_AGENTE.ID_GHE_FONTE_AGENTE = GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE");
                query.Append(" JOIN SEG_GHE_FONTE GHE_FONTE ON GHE_FONTE.ID_GHE_FONTE = GHE_FONTE_AGENTE.ID_GHE_FONTE");
                query.Append(" WHERE TRUE  ");

                if (ghes.Count > 0)
                {
                    query.Append(" AND GHE_FONTE.ID_GHE IN (");
                    ghes.ForEach(delegate(Ghe ghe)
                    {
                        query.Append(ghe.Id);
                        count++;
                        if (ghes.Count == count)
                            query.Append(")");
                        else
                            query.Append(",");
                    });
                }               
                query.Append(" AND GHE_FONTE.SITUACAO = 'A' AND GHE_FONTE_AGENTE.SITUACAO = 'A' AND GHE_FONTE_AGENTE_EXAME.SITUACAO = 'A' ");

                if (periodicidade.Id == 2)
                    query.Append(" AND GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.ID_PERIODICIDADE IN (1,2) ");
                else
                    query.Append(" AND GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.ID_PERIODICIDADE = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = periodicidade.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gheFonteAgenteExamePeriodicidade.Add(new GheFonteAgenteExamePeriodicidade(periodicidade, new GheFonteAgenteExame(dr.GetInt64(0), new Exame(dr.GetInt64(3)), new GheFonteAgente(dr.GetInt64(4), dr.IsDBNull(9) ? null : new GradSoma(dr.GetInt64(9)), dr.IsDBNull(10) ? null : new GradEfeito(dr.GetInt64(10), string.Empty, string.Empty, null), dr.IsDBNull(11) ? null : new GradExposicao(dr.GetInt64(11), string.Empty, string.Empty, 0), new GheFonte(dr.GetInt64(12), new Fonte(dr.GetInt64(16), string.Empty, string.Empty, false), new Ghe(dr.GetInt64(17), null, string.Empty, 0, string.Empty, string.Empty), dr.GetBoolean(18), dr.GetString(19)), new Agente(dr.GetInt64(13), null, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, false, string.Empty, string.Empty, string.Empty), dr.IsDBNull(14) ? string.Empty : dr.GetString(14), dr.GetString(15)), dr.GetInt32(5), dr.GetString(6), dr.GetBoolean(7), dr.GetBoolean(8), dr.GetBoolean(20)), dr.GetString(1), dr.IsDBNull(2) ? (int?)null : dr.GetInt32(2)));
                }

                return gheFonteAgenteExamePeriodicidade;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - listAllByGheByPeriodicidade", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<GheFonteAgenteExamePeriodicidade> findAllAtivosByGheFonteAgenteExame(GheFonteAgenteExame gheFonteAgenteExame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllAtivosByGheFonteAgenteExame");

            List<GheFonteAgenteExamePeriodicidade> gheFonteAgenteExamePeriodicidades = new List<GheFonteAgenteExamePeriodicidade>();

            try
            {

                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.ID_PERIODICIDADE, GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.ID_GHE_FONTE_AGENTE_EXAME, GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.SITUACAO, GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.PERIODO_VENCIMENTO ");
                query.Append(" FROM SEG_GHE_FONTE_AGENTE_EXAME_PERIODICIDADE GHE_FONTE_AGENTE_EXAME_PERIODICIDADE ");
                query.Append(" WHERE GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.ID_GHE_FONTE_AGENTE_EXAME = :VALUE1 ");
                query.Append(" AND GHE_FONTE_AGENTE_EXAME_PERIODICIDADE.SITUACAO = 'A' ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonteAgenteExame.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gheFonteAgenteExamePeriodicidades.Add(new GheFonteAgenteExamePeriodicidade(new Periodicidade(dr.GetInt64(0)), new GheFonteAgenteExame(dr.GetInt64(1)), dr.GetString(2), dr.IsDBNull(3) ? (int?)null : dr.GetInt32(3)));
                }

                return gheFonteAgenteExamePeriodicidades;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllAtivosByGheFonteAgenteExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
