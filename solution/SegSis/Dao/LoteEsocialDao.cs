﻿using Npgsql;
using NpgsqlTypes;
using SWS.Entidade;
using SWS.Excecao;
using SWS.Helper;
using SWS.IDao;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace SWS.Dao
{
    public class LoteEsocialDao : ILoteEsocialDao
    {
        public DataSet findByFilter(LoteEsocial loteEsocial, DateTime? dataCriacaoInicial, DateTime? dataCriacaoFinal, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByFilter");

            try
            {
                StringBuilder query = new StringBuilder();

                if (loteEsocial.isNullForFilter())
                {
                    query.Append(" SELECT DISTINCT ESOCIAL.ID_ESOCIAL_LOTE, ESOCIAL.CODIGO, CLIENTE.RAZAO_SOCIAL, CLIENTE.CNPJ, ESOCIAL.TIPO, ESOCIAL.DATA_CRIACAO, ESOCIAL.PERIODO ");
                    query.Append(" FROM SEG_ESOCIAL_LOTE ESOCIAL ");
                    query.Append(" JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = ESOCIAL.ID_CLIENTE ");
                    if (string.Equals(loteEsocial.Tipo, "2220"))
                    {
                        query.Append(" LEFT JOIN SEG_ESOCIAL_LOTE_ASO ESOCIAL_LOTE_ASO ON ESOCIAL_LOTE_ASO.ID_ESOCIAL_LOTE = ESOCIAL.ID_ESOCIAL_LOTE ");
                    }
                    else
                    {
                        query.Append(" LEFT JOIN SEG_ESOCIAL_MONITORAMENTO ESOCIAL_MONITORAMENTO ON ESOCIAL_MONITORAMENTO.ID_ESOCIAL_LOTE = ESOCIAL.ID_ESOCIAL_LOTE ");
                    }
                    query.Append(" WHERE TRUE ");
                    if (string.Equals(loteEsocial.Tipo, "2220"))
                    {
                        query.Append(" AND ESOCIAL.DATA_CRIACAO BETWEEN :VALUE2 AND :VALUE3 ");
                        query.Append(" AND ESOCIAL_LOTE_ASO.SITUACAO = 'E' ");
                    }
                    else
                    {
                        query.Append(" AND ESOCIAL.PERIODO = :VALUE3 ");
                        query.Append(" AND ESOCIAL_MONITORAMENTO.SITUACAO = 'E' ");
                    }
                        
                    query.Append(" AND ESOCIAL.TIPO = :VALUE4 ");
                    query.Append(" ORDER BY RAZAO_SOCIAL ASC; ");
                }
                else
                {
                    query.Append(" SELECT DISTINCT ESOCIAL.ID_ESOCIAL_LOTE, ESOCIAL.CODIGO, CLIENTE.RAZAO_SOCIAL, CLIENTE.CNPJ, ESOCIAL.TIPO, ESOCIAL.DATA_CRIACAO,ESOCIAL.PERIODO ");
                    query.Append(" FROM SEG_ESOCIAL_LOTE ESOCIAL ");
                    query.Append(" JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = ESOCIAL.ID_CLIENTE ");
                    if (string.Equals(loteEsocial.Tipo, "2220"))
                    {
                        query.Append(" LEFT JOIN SEG_ESOCIAL_LOTE_ASO ESOCIAL_LOTE_ASO ON ESOCIAL_LOTE_ASO.ID_ESOCIAL_LOTE = ESOCIAL.ID_ESOCIAL_LOTE ");
                    }
                    else
                    {
                        query.Append(" LEFT JOIN SEG_ESOCIAL_MONITORAMENTO ESOCIAL_MONITORAMENTO ON ESOCIAL_MONITORAMENTO.ID_ESOCIAL_LOTE = ESOCIAL.ID_ESOCIAL_LOTE ");
                    }

                    query.Append(" WHERE TRUE ");
                    if (string.Equals(loteEsocial.Tipo, "2220"))
                    {
                        query.Append(" AND ESOCIAL.DATA_CRIACAO BETWEEN :VALUE2 AND :VALUE3 ");
                        query.Append(" AND ESOCIAL_LOTE_ASO.SITUACAO = 'E' ");
                    }
                    else
                    {
                        query.Append(" AND ESOCIAL.PERIODO = :VALUE3 ");
                        query.Append(" AND ESOCIAL_MONITORAMENTO.SITUACAO = 'E' ");
                    }
                        
                    query.Append(" AND ESOCIAL.TIPO = :VALUE4 ");
                    query.Append(" AND CLIENTE.ID_CLIENTE = :VALUE1");
                    
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = loteEsocial.cliente != null ? loteEsocial.cliente.Id : null;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[1].Value = dataCriacaoInicial;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[2].Value = dataCriacaoFinal;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[3].Value = loteEsocial.Tipo;

                DataSet ds = new DataSet();
                adapter.Fill(ds, "Lotes");
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_ESOCIAL_LOTE_ID_ESOCIAL_LOTE_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoIdLoteesocialAso(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoIdLoteesocialAso");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_ESOCIAL_LOTE_ASO_ID_SEG_ESOCIAL_LOTE_ASO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoIdLoteesocialAso", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoIdLoteEsocialMonitoramento(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoIdLoteEsocialMonitoramento");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_ESOCIAL_MONITORAMENTO_ID_ESOCIAL_MONITORAMENTO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoIdLoteEsocialMonitoramento", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public LoteEsocial incluirLoteEsocial(LoteEsocial loteEsocial, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            StringBuilder query = new StringBuilder();

            try
            {
                loteEsocial.Id = recuperaProximoId(dbConnection);
                loteEsocial.codigo = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0') + Convert.ToString(loteEsocial.Id).PadLeft(6, '0');

                query.Append(" INSERT INTO SEG_ESOCIAL_LOTE (ID_ESOCIAL_LOTE, ID_CLIENTE, TIPO, DATA_CRIACAO, CODIGO, PERIODO) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = loteEsocial.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = loteEsocial.cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = loteEsocial.Tipo;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Timestamp));
                command.Parameters[3].Value = loteEsocial.DataCriacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = loteEsocial.codigo;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Date));
                command.Parameters[5].Value = loteEsocial.periodo;

                command.ExecuteNonQuery();
                
                return loteEsocial;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public LoteEsocialAso incluirLoteEsocialAso(LoteEsocialAso loteEsocialAso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirLoteEsocialAso");

            StringBuilder query = new StringBuilder();

            try
            {
                loteEsocialAso.Id = recuperaProximoIdLoteesocialAso(dbConnection);

                query.Append(" INSERT INTO SEG_ESOCIAL_LOTE_ASO (ID_SEG_ESOCIAL_LOTE_ASO, ID_ESOCIAL_LOTE, ID_ASO, SITUACAO) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = loteEsocialAso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = loteEsocialAso.idLoteEsocialAso;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = loteEsocialAso.idAso;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = loteEsocialAso.situacao;


                command.ExecuteNonQuery();

                return loteEsocialAso;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirLoteEsocialAso", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<string> cpfDuplicadosNoLote(LoteEsocial loteEsocial, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método cpfDuplicadosNoLote");
            StringBuilder query = new StringBuilder();
            List<string> cpfDuplicado = new List<string>();
            try
            {

                query.Append(" SELECT COUNT(*), ATENDIMENTO.CPF_FUNCIONARIO FROM SEG_ASO ATENDIMENTO ");
                query.Append(" JOIN SEG_ESOCIAL_LOTE_ASO LOTE_ASO ON LOTE_ASO.ID_ASO = ATENDIMENTO.ID_ASO");
                query.Append(" WHERE LOTE_ASO.ID_ESOCIAL_LOTE = :VALUE1 ");
                query.Append(" GROUP BY ATENDIMENTO.CPF_FUNCIONARIO ");
                query.Append(" HAVING  COUNT(*) > 1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = loteEsocial.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    cpfDuplicado.Add(dr.GetString(1));
                }

                return cpfDuplicado;
            }
            catch (Exception ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - cpfDuplicadosNoLote", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public LoteEsocial findById(long id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");
            StringBuilder query = new StringBuilder();
            LoteEsocial loteEsocial = null;
            try
            {

                query.Append(" SELECT ID_ESOCIAL_LOTE, ID_CLIENTE, TIPO, DATA_CRIACAO, CODIGO, PERIODO ");
                query.Append(" FROM SEG_ESOCIAL_LOTE " );
                query.Append(" WHERE ID_ESOCIAL_LOTE = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    loteEsocial = new LoteEsocial(dr.GetInt64(0), new Cliente(dr.GetInt64(1)), dr.GetString(2), dr.GetDateTime(3), dr.GetString(4), dr.IsDBNull(5) ? (DateTime?)null : dr.GetDateTime(5));
                }
            }
            catch (Exception ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
            return loteEsocial;
        }

        public void delete(LoteEsocial loteEsocial, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("DELETE FROM SEG_ESOCIAL_LOTE WHERE ID_ESOCIAL_LOTE = :VALUE1 ", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = loteEsocial.Id;

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public LoteEsocialMonitoramento insertLoteEsocialMonitoramento(LoteEsocialMonitoramento loteMonitoramento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertLoteEsocialMonitoramento");

            StringBuilder query = new StringBuilder();

            try
            {
                loteMonitoramento.Id = recuperaProximoIdLoteEsocialMonitoramento(dbConnection);

                query.Append(" INSERT INTO SEG_ESOCIAL_MONITORAMENTO (ID_ESOCIAL_MONITORAMENTO, ID_ESOCIAL_LOTE, ID_MONITORAMENTO, SITUACAO) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = loteMonitoramento.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = loteMonitoramento.LoteEsocial.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = loteMonitoramento.Monitoramento.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = loteMonitoramento.Situacao;

                command.ExecuteNonQuery();

                return loteMonitoramento;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertLoteEsocialMonitoramento", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public bool verificaExisteLoteJaGeradoByClienteAndPeriodo(Cliente cliente, DateTime periodo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaExisteLoteJaGeradoByClienteAndPeriodo");
            StringBuilder query = new StringBuilder();
            bool verifica = false;
            
            try
            {
                query.Append(" SELECT * FROM SEG_ESOCIAL_LOTE WHERE ID_CLIENTE = :VALUE1 AND PERIODO = :VALUE2 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Date));
                command.Parameters[1].Value = periodo;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    verifica = true;
                }
            }
            catch (Exception ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaExisteLoteJaGeradoByClienteAndPeriodo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
            return verifica;
        }

        public List<LoteEsocialAso> findAllCancelados2220ByFilter(
            Cliente cliente, 
            DateTime periodoInicial, 
            DateTime periodoFinal, 
            NpgsqlConnection dbConnection
            )
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllCancelados2220ByFilter");
            StringBuilder query = new StringBuilder();

            List<LoteEsocialAso> lista2220Cancelados = new List<LoteEsocialAso>();


            try
            {
                query.Append(" SELECT LOTE_ASO.ID_SEG_ESOCIAL_LOTE_ASO, LOTE_ASO.ID_ASO, LOTE_ASO.ID_ESOCIAL_LOTE, LOTE_ASO.SITUACAO, LOTE_ASO.NRECIBO, LOTE_ASO.DATA_CANCELAMENTO, LOTE_ASO.USUARIO_CANCELOU ");
                query.Append(" FROM SEG_ESOCIAL_LOTE_ASO LOTE_ASO ");
                query.Append(" INNER JOIN SEG_ESOCIAL_LOTE LOTE ON LOTE.ID_ESOCIAL_LOTE = LOTE_ASO.ID_ESOCIAL_LOTE ");
                query.Append(" WHERE LOTE.ID_CLIENTE = :VALUE1 ");
                query.Append(" AND LOTE_ASO.DATA_CANCELAMENTO BETWEEN :VALUE2 AND :VALUE3 ");
                query.Append(" AND LOTE_ASO.SITUACAO = 'C' ");
                query.Append(" ORDER BY DATA_CANCELAMENTO ASC ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                command.Parameters[1].Value = periodoInicial;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Timestamp));
                command.Parameters[2].Value = periodoFinal;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    LoteEsocialAso lote = new LoteEsocialAso();
                    lote.Id = dr.GetInt64(0);
                    lote.idAso = dr.GetInt64(1);
                    lote.idLoteEsocialAso = dr.GetInt64(2);
                    lote.situacao = dr.GetString(3);
                    lote.nrebido = dr.IsDBNull(4) ? string.Empty : dr.GetString(4);
                    lote.dataCancelamento = dr.IsDBNull(5) ? (DateTime?)null : dr.GetDateTime(5);
                    lote.usuarioCancelou = dr.IsDBNull(6) ? string.Empty : dr.GetString(6);

                    lista2220Cancelados.Add(lote);
                }
            }
            catch (Exception ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllCancelados2220ByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
            return lista2220Cancelados;
        }

        public List<LoteEsocialMonitoramento> findAllCancelados2240ByFilter(Cliente cliente, DateTime periodoInicial, DateTime periodoFinal, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllCancelados2240ByFilter");
            StringBuilder query = new StringBuilder();

            List<LoteEsocialMonitoramento> lista2240Cancelados = new List<LoteEsocialMonitoramento>();


            try
            {
                query.Append(" SELECT LOTE_MONITORAMENTO.ID_ESOCIAL_MONITORAMENTO, LOTE_MONITORAMENTO.ID_ESOCIAL_LOTE, LOTE_MONITORAMENTO.ID_MONITORAMENTO, LOTE_MONITORAMENTO.SITUACAO, LOTE_MONITORAMENTO.NRECIBO, LOTE_MONITORAMENTO.DATA_CANCELAMENTO, LOTE_MONITORAMENTO.USUARIO_CANCELOU  ");
                query.Append(" FROM SEG_ESOCIAL_MONITORAMENTO LOTE_MONITORAMENTO ");
                query.Append(" INNER JOIN SEG_ESOCIAL_LOTE LOTE ON LOTE.ID_ESOCIAL_LOTE = LOTE_MONITORAMENTO.ID_ESOCIAL_LOTE " );
                query.Append(" WHERE LOTE.ID_CLIENTE = :VALUE1 " );
                query.Append(" AND LOTE_MONITORAMENTO.SITUACAO = 'C'");
                query.Append(" AND LOTE_MONITORAMENTO.DATA_CANCELAMENTO BETWEEN :VALUE2 AND :VALUE3 ");
                query.Append(" ORDER BY DATA_CANCELAMENTO ASC ");


                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                command.Parameters[1].Value = periodoInicial;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Timestamp));
                command.Parameters[2].Value = periodoFinal;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    LoteEsocialMonitoramento loteMonitoramento = new LoteEsocialMonitoramento();
                    loteMonitoramento.Id = dr.GetInt64(0);
                    loteMonitoramento.LoteEsocial = new LoteEsocial(dr.GetInt64(1));
                    loteMonitoramento.Monitoramento = new Monitoramento(dr.GetInt64(2));
                    loteMonitoramento.Situacao = dr.GetString(3);
                    loteMonitoramento.NRecibo = dr.IsDBNull(4) ? string.Empty : dr.GetString(4);
                    loteMonitoramento.DataCancelamento = dr.IsDBNull(5) ? (DateTime?)null : dr.GetDateTime(5);
                    loteMonitoramento.UsuarioCancelou = dr.IsDBNull(6) ? string.Empty : dr.GetString(6);

                    lista2240Cancelados.Add(loteMonitoramento);

                }
            }
            catch (Exception ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllCancelados2240ByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
            return lista2240Cancelados;
        }

        public List<LoteEsocialAso> findAll2220ForCancelByFilter(
            Cliente cliente,
            ClienteFuncaoFuncionario clienteFuncaoFuncionario,
            String codigoLote,
            DateTime periodoInicial,
            DateTime periodoFinal,
            NpgsqlConnection dbConnection
            )
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAll2220ForCancelByFilter");
            StringBuilder query = new StringBuilder();

            List<LoteEsocialAso> lista2220ForCancel = new List<LoteEsocialAso>();


            try
            {
                query.Append(" SELECT LOTE_ASO.ID_SEG_ESOCIAL_LOTE_ASO, LOTE_ASO.ID_ASO, LOTE_ASO.ID_ESOCIAL_LOTE, LOTE_ASO.SITUACAO, LOTE_ASO.NRECIBO, LOTE_ASO.DATA_CANCELAMENTO, LOTE_ASO.USUARIO_CANCELOU ");
                query.Append(" FROM SEG_ESOCIAL_LOTE_ASO LOTE_ASO ");
                query.Append(" INNER JOIN SEG_ESOCIAL_LOTE LOTE ON LOTE.ID_ESOCIAL_LOTE = LOTE_ASO.ID_ESOCIAL_LOTE ");
                query.Append(" INNER JOIN SEG_ASO ASO ON ASO.ID_ASO = LOTE_ASO.ID_ASO ");
                query.Append(" INNER JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTE_FUNCAO_FUNCIONARIO ON CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO = ASO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                query.Append(" WHERE LOTE.ID_CLIENTE = :VALUE1 ");
                query.Append(" AND LOTE.DATA_CRIACAO BETWEEN :VALUE2 AND :VALUE3 ");
                query.Append(" AND LOTE_ASO.SITUACAO = 'E' ");

                if (clienteFuncaoFuncionario != null)
                    query.Append(" AND CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO = :VALUE4 ");

                if (!string.IsNullOrEmpty(codigoLote))
                    query.Append(" AND LOTE.CODIGO = :VALUE5 ");

                query.Append(" ORDER BY DATA_GRAVACAO ASC ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                command.Parameters[1].Value = periodoInicial;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Timestamp));
                command.Parameters[2].Value = periodoFinal;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = clienteFuncaoFuncionario != null ? clienteFuncaoFuncionario.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = codigoLote;


                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    LoteEsocialAso lote = new LoteEsocialAso();
                    lote.Id = dr.GetInt64(0);
                    lote.idAso = dr.GetInt64(1);
                    lote.idLoteEsocialAso = dr.GetInt64(2);
                    lote.situacao = dr.GetString(3);
                    lote.nrebido = dr.IsDBNull(4) ? string.Empty : dr.GetString(4);
                    lote.dataCancelamento = dr.IsDBNull(5) ? (DateTime?)null : dr.GetDateTime(5);
                    lote.usuarioCancelou = dr.IsDBNull(6) ? string.Empty : dr.GetString(6);

                    lista2220ForCancel.Add(lote);
                }
            }
            catch (Exception ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAll2220ForCancelByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
            return lista2220ForCancel;
        }

        public List<LoteEsocialMonitoramento> findAll2240ForCancelByFilter(
            Cliente cliente,
            ClienteFuncaoFuncionario clienteFuncaoFuncionario,
            String codigoLote,
            DateTime periodoInicial,
            DateTime periodoFinal,
            NpgsqlConnection dbConnection
            )
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAll2240ForCancelByFilter");
            StringBuilder query = new StringBuilder();

            List<LoteEsocialMonitoramento> lista2240ForCancel = new List<LoteEsocialMonitoramento>();


            try
            {
                query.Append(" SELECT LOTE_MONITORAMENTO.ID_ESOCIAL_MONITORAMENTO, LOTE_MONITORAMENTO.ID_ESOCIAL_LOTE, LOTE_MONITORAMENTO.ID_MONITORAMENTO, LOTE_MONITORAMENTO.SITUACAO, LOTE_MONITORAMENTO.NRECIBO, LOTE_MONITORAMENTO.DATA_CANCELAMENTO, LOTE_MONITORAMENTO.USUARIO_CANCELOU  ");
                query.Append(" FROM SEG_ESOCIAL_MONITORAMENTO LOTE_MONITORAMENTO ");
                query.Append(" INNER JOIN SEG_ESOCIAL_LOTE LOTE_ESOCIAL ON LOTE_ESOCIAL.ID_ESOCIAL_LOTE = LOTE_MONITORAMENTO.ID_ESOCIAL_LOTE ");
                query.Append(" INNER JOIN SEG_MONITORAMENTO MONITORAMENTO ON MONITORAMENTO.ID_MONITORAMENTO = LOTE_MONITORAMENTO.ID_MONITORAMENTO ");
                query.Append(" WHERE LOTE_ESOCIAL.ID_CLIENTE = :VALUE1  ");
                query.Append(" AND LOTE_MONITORAMENTO.SITUACAO = 'E' ");
                query.Append(" AND LOTE_ESOCIAL.PERIODO BETWEEN :VALUE2 AND :VALUE3 ");

                if (clienteFuncaoFuncionario != null)
                    query.Append(" AND MONITORAMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO = :VALUE4 ");

                if (!string.IsNullOrEmpty(codigoLote))
                    query.Append(" AND LOTE_ESOCIAL.CODIGO = :VALUE5  ");

                query.Append(" ORDER BY LOTE_ESOCIAL.PERIODO ASC ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                command.Parameters[1].Value = periodoInicial;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Timestamp));
                command.Parameters[2].Value = periodoFinal;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = clienteFuncaoFuncionario != null ? clienteFuncaoFuncionario.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = codigoLote;


                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    LoteEsocialMonitoramento loteMonitoramento = new LoteEsocialMonitoramento();
                    loteMonitoramento.Id = dr.GetInt64(0);
                    loteMonitoramento.LoteEsocial = new LoteEsocial(dr.GetInt64(1));
                    loteMonitoramento.Monitoramento = new Monitoramento(dr.GetInt64(2));
                    loteMonitoramento.Situacao = dr.GetString(3);
                    loteMonitoramento.NRecibo = dr.IsDBNull(4) ? string.Empty : dr.GetString(4);
                    loteMonitoramento.DataCancelamento = dr.IsDBNull(5) ? (DateTime?)null : dr.GetDateTime(5);
                    loteMonitoramento.UsuarioCancelou = dr.IsDBNull(6) ? string.Empty : dr.GetString(6);

                    lista2240ForCancel.Add(loteMonitoramento);
                }
            }
            catch (Exception ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAll2240ForCancelByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
            return lista2240ForCancel;
        }

        public LoteEsocialAso findEsocialLoteAsoById(long id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findEsocialLoteAsoById");
            StringBuilder query = new StringBuilder();
            LoteEsocialAso loteEsocialAso = null;
            try
            {

                query.Append(" SELECT ID_SEG_ESOCIAL_LOTE_ASO, ID_ASO, ID_ESOCIAL_lOTE, SITUACAO, NRECIBO, DATA_CANCELAMENTO, USUARIO_CANCELOU ");
                query.Append(" FROM SEG_ESOCIAL_LOTE_ASO  ");
                query.Append(" WHERE ID_SEG_ESOCIAL_LOTE_ASO = :VALUE1  ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    loteEsocialAso = new LoteEsocialAso();
                    loteEsocialAso.Id = dr.GetInt64(0);
                    loteEsocialAso.idAso = dr.GetInt64(1);
                    loteEsocialAso.idLoteEsocialAso = dr.GetInt64(2);
                    loteEsocialAso.situacao = dr.GetString(3);
                    loteEsocialAso.nrebido = dr.IsDBNull(4) ? string.Empty : dr.GetString(4);
                    loteEsocialAso.dataCancelamento = dr.IsDBNull(5) ? (DateTime?)null : dr.GetDateTime(5);
                    loteEsocialAso.usuarioCancelou = dr.IsDBNull(6) ? string.Empty : dr.GetString(6);
                }
            }
            catch (Exception ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findEsocialLoteAsoById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
            return loteEsocialAso;
        }

        public LoteEsocialAso updateLoteEsocialAso(LoteEsocialAso loteEsocialAso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateLoteEsocialAso");
            StringBuilder query = new StringBuilder();
            try
            {

                query.Append(" UPDATE SEG_ESOCIAL_LOTE_ASO SET ");
                query.Append(" SITUACAO = :VALUE2, ");
                query.Append(" NRECIBO = :VALUE3, ");
                query.Append(" DATA_CANCELAMENTO = :VALUE4, ");
                query.Append(" USUARIO_CANCELOU = :VALUE5 ");
                query.Append(" WHERE ID_SEG_ESOCIAL_LOTE_ASO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = loteEsocialAso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = loteEsocialAso.situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = loteEsocialAso.nrebido;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Timestamp));
                command.Parameters[3].Value = loteEsocialAso.dataCancelamento;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = loteEsocialAso.usuarioCancelou;

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateLoteEsocialAso", ex);
                throw new DataBaseConectionException(ex.Message);
            }
            return loteEsocialAso;
        }

        public LoteEsocialMonitoramento findLoteEsocialMonitoramentoById(long id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findEsocialLoteAsoById");
            StringBuilder query = new StringBuilder();
            LoteEsocialMonitoramento loteEsocialMonitoramento = null;
            try
            {
                query.Append(" SELECT ID_ESOCIAL_MONITORAMENTO, ID_ESOCIAL_LOTE, ID_MONITORAMENTO, SITUACAO, NRECIBO, DATA_CANCELAMENTO, USUARIO_CANCELOU ");
                query.Append(" FROM SEG_ESOCIAL_MONITORAMENTO   ");
                query.Append(" WHERE ID_ESOCIAL_MONITORAMENTO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    loteEsocialMonitoramento = new LoteEsocialMonitoramento();
                    loteEsocialMonitoramento.Id = dr.GetInt64(0);
                    loteEsocialMonitoramento.LoteEsocial = new LoteEsocial(dr.GetInt64(1));
                    loteEsocialMonitoramento.Monitoramento = new Monitoramento(dr.GetInt64(2));
                    loteEsocialMonitoramento.Situacao = dr.GetString(3);
                    loteEsocialMonitoramento.NRecibo = dr.IsDBNull(4) ? string.Empty : dr.GetString(4);
                    loteEsocialMonitoramento.DataCancelamento = dr.IsDBNull(5) ? (DateTime?)null : dr.GetDateTime(5);
                    loteEsocialMonitoramento.UsuarioCancelou = dr.IsDBNull(6) ? string.Empty : dr.GetString(6);
                }
            }
            catch (Exception ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findLoteEsocialMonitoramentoById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
            return loteEsocialMonitoramento;
        }

        public LoteEsocialMonitoramento updateLoteEsocialMonitoramento(LoteEsocialMonitoramento loteEsocialMonitoramento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateLoteEsocialAso");
            StringBuilder query = new StringBuilder();
            try
            {

                query.Append(" UPDATE SEG_ESOCIAL_MONITORAMENTO SET ");
                query.Append(" SITUACAO = :VALUE2, ");
                query.Append(" NRECIBO = :VALUE3, ");
                query.Append(" DATA_CANCELAMENTO = :VALUE4, ");
                query.Append(" USUARIO_CANCELOU = :VALUE5 ");
                query.Append(" WHERE ID_ESOCIAL_MONITORAMENTO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = loteEsocialMonitoramento.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = loteEsocialMonitoramento.Situacao ;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = loteEsocialMonitoramento.NRecibo;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Timestamp));
                command.Parameters[3].Value = loteEsocialMonitoramento.DataCancelamento;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = loteEsocialMonitoramento.UsuarioCancelou;

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateLoteEsocialMonitoramento", ex);
                throw new DataBaseConectionException(ex.Message);
            }
            return loteEsocialMonitoramento;
        }
    }

}
