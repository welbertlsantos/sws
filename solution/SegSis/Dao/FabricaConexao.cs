﻿using System;
using Npgsql;
using System.Windows.Forms;
using System.Xml.Linq;
using System.IO;
using SWS.Facade;

namespace SWS.Dao
{
    class FabricaConexao
    {
        private String stringconnection;

        private static NpgsqlConnection dbConnection = null;
        
        private FabricaConexao(){

            string server = PermissionamentoFacade.filialAutenticada.Host;
            string port = PermissionamentoFacade.filialAutenticada.Port;
            string user = PermissionamentoFacade.filialAutenticada.User;
            string password = PermissionamentoFacade.filialAutenticada.Password;
            string database = PermissionamentoFacade.filialAutenticada.Database;
            string schema = PermissionamentoFacade.filialAutenticada.Schema;

            stringconnection = "Server=" + server + ";Port=" + port + ";User Id=" + user + ";Password=" + password + ";Database=" + database + ";SearchPath=" + schema + ";CommandTimeout=120;preload reader=true";
            
            dbConnection = new NpgsqlConnection(stringconnection);
        }

        public static NpgsqlConnection getConexao()
        {
            try
            {
                if (dbConnection == null)
                {
                    new FabricaConexao();
                }

                dbConnection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return dbConnection;
        }
            
        public static void fecharConexao(){
            dbConnection.Close();
        }
    }
}