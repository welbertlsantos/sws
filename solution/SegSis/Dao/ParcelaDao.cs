﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;
using SWS.Entidade;
using NpgsqlTypes;

namespace SWS.Dao
{
    class ParcelaDao : IParcelaDao
    {
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_PARCELA_ID_PARCELA_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Parcela insert(Parcela parcela, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertParcela");

            try
            {
                parcela.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_PARCELA( ID_PARCELA, ID_PLANO, DIAS) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = parcela.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = parcela.Plano.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = parcela.Dias;

                command.ExecuteNonQuery();

                return parcela;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertParcela", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<Parcela> findParcelasByPLano(Plano plano, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findParcelasByPLano");

            List<Parcela> parcelas = new List<Parcela>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT PARCELA.ID_PARCELA, PARCELA.DIAS ");
                query.Append(" FROM SEG_PARCELA PARCELA ");
                query.Append(" JOIN SEG_PLANO PLANO ON PLANO.ID_PLANO = PARCELA.ID_PLANO AND PLANO.ID_PLANO = :VALUE1 ");
                query.Append(" ORDER BY DIAS ASC ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = plano.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    parcelas.Add(new Parcela(dr.GetInt64(0), plano, dr.GetInt32(1)));
                }

                return parcelas;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findParcelasByPLano", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void deleteByPlano(Plano plano, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteParcelasByPlano");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" DELETE FROM SEG_PARCELA WHERE ID_PLANO = :VALUE1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = plano.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteParcelasByPlano", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void delete(Parcela parcela, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" DELETE FROM SEG_PARCELA WHERE ID_PARCELA = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = parcela.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Parcela update(Parcela parcela, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_PARCELA SET DIAS = :VALUE2 WHERE ID_PARCELA = :VALUE1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = parcela.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = parcela.Dias;

                command.ExecuteNonQuery();

                return parcela;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

    }
}
