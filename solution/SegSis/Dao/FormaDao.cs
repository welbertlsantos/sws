﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using System.Data;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;
using SWS.Entidade;
using NpgsqlTypes;
using SWS.View.Resources;

namespace SWS.Dao
{
    class FormaDao :IFormaDao
    {

        public DataSet findFormaByFilter(Forma forma, Plano plano, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFormaByFilter");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" select id_forma, descricao, false from seg_forma ");
                
                if (forma.Descricao.Trim() != null)
                {
                    query.Append(" where descricao like :value1 ");
                }

                if (plano.Formas.Count > 0)
                {
                    int i = 0;

                    query.Append(" and id_forma not in (" );
                    foreach (PlanoForma formas in plano.Formas)
                    {
                        query.Append(formas.Forma.Id);
                        i++;

                        if (i < plano.Formas.Count)
                        {
                            query.Append(",");
                        }
                        else
                        {
                            query.Append(")");
                        }

                    }
                }
                
                query.Append(" order by descricao asc");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Varchar));

                adapter.SelectCommand.Parameters[0].Value = "%" + forma.Descricao.ToUpper() + "%";
                
                DataSet ds = new DataSet();
                
                adapter.Fill(ds, "Formas");
                
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFormaByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }


        public DataSet findFormaByPlano(Plano plano, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFormaByPlano");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" select pf.id_plano_forma, f.descricao from seg_forma f ");
                query.Append(" join seg_plano_forma pf on pf.id_forma = f.id_forma and pf.situacao = :value1 ");
                query.Append(" join seg_plano p on p.id_plano = pf.id_plano and p.id_plano = :value2 ");
                query.Append(" order by descricao asc");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("value2", NpgsqlDbType.Integer));

                adapter.SelectCommand.Parameters[0].Value = ApplicationConstants.ATIVO;
                adapter.SelectCommand.Parameters[1].Value = plano.Id;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Formas");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFormaByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }


        public List<Forma> findAllFormas(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllFormas");

            List<Forma> formas = new List<Forma>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID_FORMA, DESCRICAO FROM SEG_FORMA ");
                query.Append(" ORDER BY DESCRICAO ASC");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    formas.Add(new Forma(dr.GetInt64(0), dr.GetString(1)));
                }

                return formas;
                
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllFormas", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
