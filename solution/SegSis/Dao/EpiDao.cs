﻿using SWS.Entidade;
using Npgsql;
using SWS.Excecao;
using System;
using NpgsqlTypes;
using System.Data;
using System.Text;
using SWS.IDao;
using SWS.View.Resources;
using SWS.Helper;

namespace SWS.Dao
{
    class EpiDao : IEpiDao
    {

        public DataSet findByFilter(Epi epi, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findEpiByFilter");

            try
            {
                StringBuilder query = new StringBuilder();
                
                if (epi.isNullForFilter())
                {
                    query.Append(" SELECT ID_EPI,DESCRICAO,FINALIDADE,CA,SITUACAO FROM SEG_EPI ORDER BY DESCRICAO ASC");
                }
                else
                {
                    query.Append(" SELECT ID_EPI,DESCRICAO,FINALIDADE,CA,SITUACAO FROM SEG_EPI WHERE TRUE");

                    if (!String.IsNullOrWhiteSpace(epi.Descricao))
                        query.Append(" AND DESCRICAO LIKE :VALUE1 ");

                    if (!String.IsNullOrWhiteSpace(epi.Ca))
                        query.Append(" AND CA LIKE :VALUE2 ");

                    if (!String.IsNullOrWhiteSpace(epi.Situacao))
                        query.Append(" AND SITUACAO = :VALUE3 ");

                    query.Append(" ORDER BY DESCRICAO ASC");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[0].Value = epi.Descricao + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[1].Value = epi.Ca + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[2].Value = epi.Situacao;
                
                DataSet ds = new DataSet();
                adapter.Fill(ds, "Epis");
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findEpiByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Epi findByDescricao(String descricao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findEpiByDescricao");

            Epi epiRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append("SELECT ID_EPI, DESCRICAO, FINALIDADE, CA, SITUACAO FROM SEG_EPI WHERE DESCRICAO = :VALUE1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
        
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = descricao.ToUpper();
                
                NpgsqlDataReader dr = command.ExecuteReader();
                
                while (dr.Read())
                {
                    epiRetorno = new Epi(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.GetString(4));
                }
                
                return epiRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findEpiByDescricao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Epi insert(Epi epi, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirEpi");

            StringBuilder query = new StringBuilder();

            try
            {
                epi.Id= recuperaProximoId(dbConnection);

                query.Append(" INSERT INTO SEG_EPI (ID_EPI, DESCRICAO, FINALIDADE, CA, SITUACAO ) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, 'A')");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = epi.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = epi.Descricao;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = epi.Finalidade;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = epi.Ca;
                
                command.ExecuteNonQuery();
                return epi;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirEpi", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");
            
            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_EPI_ID_EPI_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoIdEpi", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Epi update(Epi epi, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateEpi");

            StringBuilder query = new StringBuilder();

            try
            {

                query.Append(" UPDATE SEG_EPI SET DESCRICAO = :VALUE2, FINALIDADE = :VALUE3, ");
                query.Append(" CA = :VALUE4, SITUACAO = :VALUE5 WHERE ID_EPI = :VALUE1");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = epi.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = epi.Descricao;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = epi.Finalidade;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = epi.Ca;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = epi.Situacao;

                command.ExecuteNonQuery();

                return epi;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateEpi", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Epi findById(Int64 idEpi, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findEpiById");
            
            Epi epiRetorno = null;

            StringBuilder query = new StringBuilder();
            
            try
            {
                query.Append(" SELECT ID_EPI, DESCRICAO, FINALIDADE, CA, SITUACAO FROM SEG_EPI WHERE ID_EPI = :VALUE1");
            
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = idEpi;
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    epiRetorno = new Epi(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.GetString(4));

                }
                return epiRetorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findEpiById", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public DataSet findAtivosByEpiAndGhefonteAgente(Epi epi, GheFonteAgente gheFonteAgente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findEpiAtivoByGhefonteAgenteByFilter");

            try
            {
                StringBuilder query = new StringBuilder();

                if (String.IsNullOrWhiteSpace(epi.Descricao))
                {
                    query.Append(" SELECT ID_EPI,DESCRICAO,FINALIDADE,CA,SITUACAO FROM SEG_EPI WHERE SITUACAO = 'A' AND ");
                    query.Append(" ID_EPI NOT IN (SELECT ID_EPI FROM SEG_GHE_FONTE_AGENTE_EPI WHERE ");
                    query.Append(" ID_GHE_FONTE_AGENTE = :VALUE3)");
                    query.Append(" ORDER BY DESCRICAO ASC");
                }
                else
                {
                    query.Append(" SELECT ID_EPI,DESCRICAO,FINALIDADE,CA,SITUACAO FROM SEG_EPI WHERE SITUACAO = 'A' AND");
                    query.Append(" ID_EPI NOT IN (SELECT ID_EPI FROM SEG_GHE_FONTE_AGENTE_EPI WHERE ID_GHE_FONTE_AGENTE = :VALUE2) AND");
                    query.Append(" DESCRICAO LIKE :VALUE1");
                    query.Append(" ORDER BY DESCRICAO ASC");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[0].Value = "%" + epi.Descricao + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[1].Value = gheFonteAgente.Id;

                DataSet ds = new DataSet();
                adapter.Fill(ds, "Epis");
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findEpiAtivoByGhefonteAgenteByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }

}
