﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using System.Data;
using Npgsql;
using SWS.ViewHelper;
using SWS.Helper;
using NpgsqlTypes;
using SWS.Excecao;
using SWS.Entidade;

namespace SWS.Dao
{
    class CidadeIbgeDao : ICidadeIbgeDao
    {

        public IList<SelectItem> findAllCidadeByUf(String uf, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllCidadeByUf");

            try
            {
                StringBuilder query = new StringBuilder();

                IList<SelectItem> cidades = new List<SelectItem>() { };
                                
                query.Append(" select id_cidade_ibge, nome from seg_cidade_ibge ");
                query.Append(" where uf_cidade = :value1 ");
                query.Append(" order by nome asc");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Varchar));

                command.Parameters[0].Value = uf;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    SelectItem item = new SelectItem(Convert.ToString(dr.GetInt64(0)), dr.GetString(1));

                    cidades.Add(item);
                }

                return cidades;
                
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllCidadeByUf", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public CidadeIbge findCidadeById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findCidadeById");

            try
            {
                StringBuilder query = new StringBuilder();

                CidadeIbge cidade = null;

                query.Append(" select id_cidade_ibge, codigo, nome, uf_cidade from seg_cidade_ibge ");
                query.Append(" where id_cidade_ibge = :value1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    cidade = new CidadeIbge(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3));
                }

                return cidade;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findCidadeById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }


        public IList<SelectItem> findAllCidade(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllCidade");

            try
            {
                StringBuilder query = new StringBuilder();

                IList<SelectItem> cidades = new List<SelectItem>() { };

                query.Append(" select id_cidade_ibge, nome from seg_cidade_ibge ");
                query.Append(" order by nome asc");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    SelectItem item = new SelectItem(Convert.ToString(dr.GetInt64(0)), dr.GetString(1));

                    cidades.Add(item);
                }

                return cidades;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllCidade", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public SelectItem findItemCidadeById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findItemCidadeById");

            try
            {
                StringBuilder query = new StringBuilder();

                SelectItem item = null;

                query.Append(" select id_cidade_ibge, nome from seg_cidade_ibge where id_cidade_ibge = :value1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = id;


                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    item = new SelectItem(Convert.ToString(dr.GetInt64(0)), Convert.ToString(dr.GetString(1)));

                }

                return item;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findItemCidadeById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAllCidadeByUfByCidade(String uf, CidadeIbge cidade, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findCidadesByUf");

            try
            {
                StringBuilder query = new StringBuilder();

                if (cidade == null)
                {
                    query.Append(" SELECT ID_CIDADE_IBGE, CODIGO, NOME, UF_CIDADE FROM SEG_CIDADE_IBGE ");
                    query.Append(" WHERE UF_CIDADE = :VALUE1 ");
                    query.Append(" ORDER BY NOME ASC");
                }
                else
                {
                    query.Append(" SELECT ID_CIDADE_IBGE, CODIGO, NOME, UF_CIDADE FROM SEG_CIDADE_IBGE ");
                    query.Append(" WHERE UF_CIDADE = :VALUE1 AND NOME LIKE :VALUE2 ");
                    query.Append(" ORDER BY NOME ASC");
                }
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));

                adapter.SelectCommand.Parameters[0].Value = uf;
                adapter.SelectCommand.Parameters[1].Value = cidade != null ? "%" + cidade.Nome + "%" : null;

                DataSet ds = new DataSet();
                adapter.Fill(ds, "Cidades");
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findCidadesByUf", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }


    }
}
