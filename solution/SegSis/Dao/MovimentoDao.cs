﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using SWS.Excecao;
using Npgsql;
using NpgsqlTypes;
using SWS.Helper;
using System.Data;
using SWS.View.Resources;
using SWS.Facade;

namespace SWS.Dao
{
    class MovimentoDao :IMovimentoDao
    {
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");
         
            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_MOVIMENTO_ID_MOVIMENTO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void insert(Movimento movimento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertMovimento");

            try
            {
                movimento.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_MOVIMENTO ( ID_MOVIMENTO, ID_GHE_FONTE_AGENTE_EXAME_ASO, ID_CLIENTE_FUNCAO_EXAME_ASO, ");
                query.Append(" ID_CLIENTE, ID_PRODUTO_CONTRATO, DT_INCLUSAO, SITUACAO, PRC_UNIT, COM_VALOR, ID_ESTUDO, ID_ASO, QUANTIDADE,  ");
                query.Append(" USUARIO, ID_USUARIO, PRECO_CUSTO, ID_CLIENTE_UNIDADE, DATA_GRAVACAO, ID_EMPRESA, ID_CENTROCUSTO, ID_CONTRATO_EXAME, ID_CLIENTE_CREDENCIADA ) "); 
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6, :VALUE7, ");
                query.Append(":VALUE8, :VALUE9, :VALUE10, :VALUE11, :VALUE12, :VALUE13, :VALUE14, :VALUE15, :VALUE16, :VALUE17, :VALUE18, :VALUE19, :VALUE20, :VALUE21) ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = movimento.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = movimento.GheFonteAgenteExameAso != null ? movimento.GheFonteAgenteExameAso.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = movimento.ClienteFuncaoExameAso != null ? movimento.ClienteFuncaoExameAso.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = movimento.Cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Integer));
                command.Parameters[4].Value = movimento.ContratoProduto != null ? movimento.ContratoProduto.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Timestamp));
                command.Parameters[5].Value = movimento.DataInclusao;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = movimento.Situacao;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Numeric));
                command.Parameters[7].Value = movimento.PrecoUnit;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Numeric));
                command.Parameters[8].Value = movimento.ValorComissao != null ? movimento.ValorComissao : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Integer));
                command.Parameters[9].Value = movimento.Estudo != null ? movimento.Estudo.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Integer));
                command.Parameters[10].Value = movimento.Aso != null ? movimento.Aso.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Integer));
                command.Parameters[11].Value = movimento.Quantidade;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Varchar));
                command.Parameters[12].Value = movimento.Usuario.Login;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Integer));
                command.Parameters[13].Value = movimento.Usuario.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Numeric));
                command.Parameters[14].Value = movimento.CustoUnitario;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Integer));
                command.Parameters[15].Value = movimento.Unidade != null ? movimento.Unidade.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE17", NpgsqlDbType.Timestamp)); 
                command.Parameters[16].Value = movimento.DataGravacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE18", NpgsqlDbType.Integer));
                command.Parameters[17].Value = movimento.Empresa != null ? movimento.Empresa.Id : (long?)null;

                command.Parameters.Add(new NpgsqlParameter("VALUE19", NpgsqlDbType.Integer));
                command.Parameters[18].Value = movimento.CentroCusto != null ? movimento.CentroCusto.Id : (long?)null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE20", NpgsqlDbType.Integer));
                command.Parameters[19].Value = movimento.ContratoExame != null ? movimento.ContratoExame.Id : (long?)null;

                command.Parameters.Add(new NpgsqlParameter("VALUE21", NpgsqlDbType.Integer));
                command.Parameters[20].Value = movimento.ClienteCredenciada != null ? movimento.ClienteCredenciada.Id : (long?)null;
                                                
                                                
                command.ExecuteNonQuery();

                
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertMovimento", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }
        
        public DataSet findMovimentoByFilter(Movimento movimento, DateTime? dataFinalLancamento, DateTime? dataFinalFaturamento, Boolean check, Boolean particular,  Produto produto, Exame exame, DateTime? DataInicialAtendimento, DateTime? DataFinalAtendimento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMovimentoByFilter");
            
            try
            {

                StringBuilder query = new StringBuilder();
                
                Boolean isNullFilter = false;
                Boolean isClient = false;
                Boolean isDataInclusao = false;
                Boolean isDataFaturamento = false; 
                Boolean isNfe = false;
                Boolean isSituacao = false;
                Boolean isParticular = false;
                Boolean isProduto = false; 
                Boolean isExame = false;
                Boolean isCentroCusto = false;
                bool isCredenciada = false;
                bool isDataAtendimento = false;

                if (movimento.isNullForFilter() && (produto == null) && (exame == null))
                {
                    // sem parâmetro para pesquisa

                    query.Append(" SELECT MOVIMENTO.ID_MOVIMENTO, MOVIMENTO.DT_INCLUSAO, MOVIMENTO.DT_FATURAMENTO, MOVIMENTO.SITUACAO, NFE.NUMERO_NFE, ");
                    query.Append(" MOVIMENTO.ID_CLIENTE, CLIENTE.CNPJ, CLIENTE.RAZAO_SOCIAL, MOVIMENTO.PRC_UNIT, MOVIMENTO.QUANTIDADE, MOVIMENTO.COM_VALOR, ");
                    query.Append(" MOVIMENTO.USUARIO, MOVIMENTO.ID_GHE_FONTE_AGENTE_EXAME_ASO, MOVIMENTO.ID_CLIENTE_FUNCAO_EXAME_ASO, EXAME.ID_EXAME, ");
                    query.Append(" EXAME.DESCRICAO, MOVIMENTO.ID_PRODUTO_CONTRATO, PRODUTO.ID_PRODUTO, PRODUTO.DESCRICAO, MOVIMENTO.ID_ASO, FORMATA_CODIGO_ATENDIMENTO(ASO.CODIGO_ASO),  ");
                    query.Append(" MOVIMENTO.ID_ESTUDO, ESTUDO.COD_ESTUDO, MOVIMENTO.ID_USUARIO, FUNCIONARIO.ID_FUNCIONARIO, FUNCIONARIO.NOME, CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO, :VALUE8, MOVIMENTO.ID_EMPRESA, EMPRESA.RAZAO_SOCIAL, MOVIMENTO.PRECO_CUSTO, PRESTADOR.RAZAO_SOCIAL, MOVIMENTO.ID_CLIENTE_UNIDADE, UNIDADE.RAZAO_SOCIAL, MOVIMENTO.DATA_GRAVACAO, MOVIMENTO.ID_CENTROCUSTO, CENTROCUSTO.DESCRICAO, MOVIMENTO.ID_CONTRATO_EXAME ");
                    query.Append(" FROM SEG_MOVIMENTO MOVIMENTO ");
                    query.Append(" LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GHE_FONTE_AGENTE_EXAME_ASO ON GHE_FONTE_AGENTE_EXAME_ASO.ID_GHE_FONTE_AGENTE_EXAME_ASO = MOVIMENTO.ID_GHE_FONTE_AGENTE_EXAME_ASO ");
                    query.Append(" LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME GHE_FONTE_AGENTE_EXAME ON GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME = GHE_FONTE_AGENTE_EXAME_ASO.ID_GHE_FONTE_AGENTE_EXAME ");
                    query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO_EXAME_ASO CLIENTE_FUNCAO_EXAME_ASO ON CLIENTE_FUNCAO_EXAME_ASO.ID_CLIENTE_FUNCAO_EXAME_ASO = MOVIMENTO.ID_CLIENTE_FUNCAO_EXAME_ASO ");
                    query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO_EXAME CLIENTE_FUNCAO_EXAME ON CLIENTE_FUNCAO_EXAME.ID_CLIENTE_FUNCAO_EXAME = CLIENTE_FUNCAO_EXAME_ASO.ID_CLIENTE_FUNCAO_EXAME ");
                    query.Append(" LEFT JOIN SEG_EXAME EXAME ON (EXAME.ID_EXAME = GHE_FONTE_AGENTE_EXAME.ID_EXAME OR EXAME.ID_EXAME = CLIENTE_FUNCAO_EXAME.ID_EXAME) ");
                    query.Append(" LEFT JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = MOVIMENTO.ID_CLIENTE ");
                    query.Append(" LEFT JOIN SEG_PRODUTO_CONTRATO PRODUTO_CONTRATO ON PRODUTO_CONTRATO.ID_PRODUTO_CONTRATO = MOVIMENTO.ID_PRODUTO_CONTRATO ");
                    query.Append(" LEFT JOIN SEG_PRODUTO PRODUTO ON PRODUTO.ID_PRODUTO = PRODUTO_CONTRATO.ID_PRODUTO ");
                    query.Append(" LEFT JOIN SEG_ASO ASO ON ASO.ID_ASO = MOVIMENTO.ID_ASO ");
                    query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTE_FUNCAO_FUNCIONARIO ON CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO = ASO.ID_CLIENTE_FUNCAO_FUNCIONARIO");
                    query.Append(" LEFT JOIN SEG_FUNCIONARIO FUNCIONARIO ON FUNCIONARIO.ID_FUNCIONARIO = CLIENTE_FUNCAO_FUNCIONARIO.ID_FUNCIONARIO");
                    query.Append(" LEFT JOIN SEG_ESTUDO ESTUDO ON ESTUDO.ID_ESTUDO = MOVIMENTO.ID_ESTUDO ");
                    query.Append(" LEFT JOIN SEG_NFE NFE ON NFE.ID_NFE = MOVIMENTO.ID_NFE ");
                    query.Append(" LEFT JOIN SEG_EMPRESA EMPRESA ON EMPRESA.ID_EMPRESA = MOVIMENTO.ID_EMPRESA ");
                    query.Append(" LEFT JOIN SEG_CLIENTE PRESTADOR ON PRESTADOR.ID_CLIENTE = GHE_FONTE_AGENTE_EXAME_ASO.ID_PRESTADOR OR PRESTADOR.ID_CLIENTE = CLIENTE_FUNCAO_EXAME_ASO.ID_PRESTADOR ");
                    query.Append(" LEFT JOIN SEG_CLIENTE UNIDADE ON UNIDADE.ID_CLIENTE = MOVIMENTO.ID_CLIENTE_UNIDADE ");
                    query.Append(" LEFT JOIN SEG_CENTROCUSTO CENTRO_CUSTO ON CENTRO_CUSTO.ID = MOVIMENTO.ID_CENTROCUSTO ");
                    query.Append(" WHERE MOVIMENTO.ID_EMPRESA = :VALUE11 ");
                    
                    isNullFilter = true;
                    
                }
                else
                {
                    query.Append(" SELECT MOVIMENTO.ID_MOVIMENTO, MOVIMENTO.DT_INCLUSAO, MOVIMENTO.DT_FATURAMENTO, MOVIMENTO.SITUACAO, NFE.NUMERO_NFE, ");
                    query.Append(" MOVIMENTO.ID_CLIENTE, CLIENTE.CNPJ, CLIENTE.RAZAO_SOCIAL, MOVIMENTO.PRC_UNIT, MOVIMENTO.QUANTIDADE, MOVIMENTO.COM_VALOR, ");
                    query.Append(" MOVIMENTO.USUARIO, MOVIMENTO.ID_GHE_FONTE_AGENTE_EXAME_ASO, MOVIMENTO.ID_CLIENTE_FUNCAO_EXAME_ASO, EXAME.ID_EXAME, ");
                    query.Append(" EXAME.DESCRICAO, MOVIMENTO.ID_PRODUTO_CONTRATO, PRODUTO.ID_PRODUTO, PRODUTO.DESCRICAO, MOVIMENTO.ID_ASO, FORMATA_CODIGO_ATENDIMENTO(ASO.CODIGO_ASO),  ");
                    query.Append(" MOVIMENTO.ID_ESTUDO, ESTUDO.COD_ESTUDO, MOVIMENTO.ID_USUARIO, FUNCIONARIO.ID_FUNCIONARIO, FUNCIONARIO.NOME, CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO, :VALUE8, MOVIMENTO.ID_EMPRESA, EMPRESA.RAZAO_SOCIAL, MOVIMENTO.PRECO_CUSTO, PRESTADOR.RAZAO_SOCIAL, MOVIMENTO.ID_CLIENTE_UNIDADE, UNIDADE.RAZAO_SOCIAL, MOVIMENTO.DATA_GRAVACAO, MOVIMENTO.ID_CENTROCUSTO, CENTRO_CUSTO.DESCRICAO, MOVIMENTO.ID_CONTRATO_EXAME ");
                    query.Append(" FROM SEG_MOVIMENTO MOVIMENTO ");
                    query.Append(" LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GHE_FONTE_AGENTE_EXAME_ASO ON GHE_FONTE_AGENTE_EXAME_ASO.ID_GHE_FONTE_AGENTE_EXAME_ASO = MOVIMENTO.ID_GHE_FONTE_AGENTE_EXAME_ASO ");
                    query.Append(" LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME GHE_FONTE_AGENTE_EXAME ON GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME = GHE_FONTE_AGENTE_EXAME_ASO.ID_GHE_FONTE_AGENTE_EXAME ");
                    query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO_EXAME_ASO CLIENTE_FUNCAO_EXAME_ASO ON CLIENTE_FUNCAO_EXAME_ASO.ID_CLIENTE_FUNCAO_EXAME_ASO = MOVIMENTO.ID_CLIENTE_FUNCAO_EXAME_ASO ");
                    query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO_EXAME CLIENTE_FUNCAO_EXAME ON CLIENTE_FUNCAO_EXAME.ID_CLIENTE_FUNCAO_EXAME = CLIENTE_FUNCAO_EXAME_ASO.ID_CLIENTE_FUNCAO_EXAME ");
                    query.Append(" LEFT JOIN SEG_EXAME EXAME ON (EXAME.ID_EXAME = GHE_FONTE_AGENTE_EXAME.ID_EXAME OR EXAME.ID_EXAME = CLIENTE_FUNCAO_EXAME.ID_EXAME) ");
                    query.Append(" LEFT JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = MOVIMENTO.ID_CLIENTE ");
                    query.Append(" LEFT JOIN SEG_PRODUTO_CONTRATO PRODUTO_CONTRATO ON PRODUTO_CONTRATO.ID_PRODUTO_CONTRATO = MOVIMENTO.ID_PRODUTO_CONTRATO ");
                    query.Append(" LEFT JOIN SEG_PRODUTO PRODUTO ON PRODUTO.ID_PRODUTO = PRODUTO_CONTRATO.ID_PRODUTO ");
                    query.Append(" LEFT JOIN SEG_ASO ASO ON ASO.ID_ASO = MOVIMENTO.ID_ASO ");
                    query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTE_FUNCAO_FUNCIONARIO ON CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO = ASO.ID_CLIENTE_FUNCAO_FUNCIONARIO");
                    query.Append(" LEFT JOIN SEG_FUNCIONARIO FUNCIONARIO ON FUNCIONARIO.ID_FUNCIONARIO = CLIENTE_FUNCAO_FUNCIONARIO.ID_FUNCIONARIO");
                    query.Append(" LEFT JOIN SEG_ESTUDO ESTUDO ON ESTUDO.ID_ESTUDO = MOVIMENTO.ID_ESTUDO ");
                    query.Append(" LEFT JOIN SEG_NFE NFE ON NFE.ID_NFE = MOVIMENTO.ID_NFE ");
                    query.Append(" LEFT JOIN SEG_EMPRESA EMPRESA ON EMPRESA.ID_EMPRESA = MOVIMENTO.ID_EMPRESA ");
                    query.Append(" LEFT JOIN SEG_CLIENTE PRESTADOR ON PRESTADOR.ID_CLIENTE = GHE_FONTE_AGENTE_EXAME_ASO.ID_PRESTADOR OR PRESTADOR.ID_CLIENTE = CLIENTE_FUNCAO_EXAME_ASO.ID_PRESTADOR ");
                    query.Append(" LEFT JOIN SEG_CLIENTE UNIDADE ON UNIDADE.ID_CLIENTE = MOVIMENTO.ID_CLIENTE_UNIDADE ");
                    query.Append(" LEFT JOIN SEG_CENTROCUSTO CENTRO_CUSTO ON CENTRO_CUSTO.ID = MOVIMENTO.ID_CENTROCUSTO ");
                    query.Append(" WHERE MOVIMENTO.ID_EMPRESA = :VALUE11 ");

                    // testando se o usuário selecionou o cliente

                    if (movimento.Cliente != null)
                    {
                        query.Append(" AND MOVIMENTO.ID_CLIENTE = :VALUE1 ");
                        isClient = true;
                    }

                    if (movimento.CentroCusto != null)
                    {
                        query.Append(" AND MOVIMENTO.ID_CENTROCUSTO = :VALUE12");
                        isCentroCusto = true;
                    }
                    // testando para saber se o usuário selecionou o período de lançamento.

                    if (movimento.DataInclusao != null)
                    {
                        query.Append(" AND MOVIMENTO.DT_INCLUSAO BETWEEN :VALUE2 AND :VALUE3 ");
                        isDataInclusao = true;
                    }

                    // testando para saber se o usuário selecionou o período de faturamento

                    if (movimento.DataFaturamento != null)
                    {
                        query.Append(" AND MOVIMENTO.DT_FATURAMENTO BETWEEN :VALUE4 AND :VALUE5 ");
                        isDataFaturamento = true;
                    }

                    // verificando se o usuário selecionou o filtro da nota fiscal.
                    if (movimento.Nfe != null)
                    {
                        query.Append(" AND NFE.NUMERO_NFE = :VALUE6 ");
                        isNfe = true;

                    }

                    // verificando se o usuário selecionou o filtro situação do movimento

                    if (!String.IsNullOrEmpty(movimento.Situacao))
                    {
                        query.Append(" AND MOVIMENTO.SITUACAO = :VALUE7 ");
                        isSituacao = true;

                    }


                    // verificando se o usuário selecionou para marcar somente clientes ativos.
                    if (particular)
                    {
                        query.Append(" AND CLIENTE.PARTICULAR = TRUE ");
                        isParticular = true;

                    }

                    // verificando se foi passado algum produto para pesquisar

                    if (exame != null)
                    {
                        query.Append(" AND EXAME.ID_EXAME = :VALUE9 ");
                        isExame = true;
                    }

                    if (produto != null)
                    {
                        query.Append(" AND PRODUTO.ID_PRODUTO = :VALUE10 ");
                        isProduto = true;
                    }

                    if (DataInicialAtendimento != null)
                    {
                        query.Append(" AND ASO.DATA_ASO BETWEEN :VALUE13 AND :VALUE14 ");
                        isDataAtendimento = true;
                    }

                    if (movimento.ClienteCredenciada != null)
                    {
                        query.Append(" AND MOVIMENTO.ID_CLIENTE_CREDENCIADA = :VALUE15 ");
                        isCredenciada = true;
                    }

                    /* ordernando por atendimento */
                    query.Append(" ORDER BY MOVIMENTO.ID_ASO ASC, EXAME.DESCRICAO ASC ");


                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = movimento.Cliente != null ? movimento.Cliente.Id : null;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[1].Value = movimento.DataInclusao != null ? movimento.DataInclusao : null;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[2].Value = dataFinalLancamento;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[3].Value = movimento.DataFaturamento != null ? movimento.DataFaturamento : null;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[4].Value = dataFinalFaturamento;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[5].Value = movimento.Nfe != null ? movimento.Nfe.NumeroNfe : (long?)null;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[6].Value = !String.IsNullOrEmpty(movimento.Situacao) ? movimento.Situacao : null;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Boolean));
                adapter.SelectCommand.Parameters[7].Value = check;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[8].Value = exame != null ? exame.Id : null;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[9].Value = produto != null ? produto.Id : null;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[10].Value = movimento.Empresa.Id;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[11].Value = movimento.CentroCusto != null ? movimento.CentroCusto.Id : (long?)null;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[12].Value = DataInicialAtendimento == null ? (DateTime?)null : DataInicialAtendimento;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[13].Value = DataFinalAtendimento == null ? (DateTime?)null : DataFinalAtendimento;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[14].Value = movimento.ClienteCredenciada != null ? movimento.ClienteCredenciada.Id : (long?)null;

                DataSet ds = new DataSet();
                adapter.Fill(ds, "Movimentos");


                /* coluna total */

                query.Length = 0;

                if (isNullFilter)
                {
                    query.Append(" SELECT (SELECT COUNT(*) FROM SEG_MOVIMENTO WHERE ID_EMPRESA = :VALUE10) AS QUANTIDADE, ");
                    query.Append(" (SELECT SUM(PRC_UNIT * QUANTIDADE) FROM SEG_MOVIMENTO WHERE ID_EMPRESA = :VALUE10) AS VALOR, ");
                    query.Append(" (SELECT COUNT(*) FROM SEG_MOVIMENTO WHERE ID_EMPRESA = :VALUE10 AND ID_PRODUTO_CONTRATO IS NOT NULL) AS QUANTIDADEPRODUTO, ");
                    query.Append(" (SELECT COUNT(*) FROM SEG_MOVIMENTO WHERE ID_EMPRESA = :VALUE10 AND (ID_GHE_FONTE_AGENTE_EXAME_ASO IS NOT NULL OR ID_CLIENTE_FUNCAO_EXAME_ASO IS NOT NULL) AND ID_PRODUTO_CONTRATO IS NULL ) AS QUANTIDADEEXAME ");
                }
                else
                {
                    /* total geral de itens no movimento */

                    #region total itens
                                        
                    query.Append(" SELECT (SELECT COUNT(*) FROM SEG_MOVIMENTO MOVIMENTO ");
                    query.Append(" LEFT JOIN SEG_NFE NFE ON NFE.ID_NFE = MOVIMENTO.ID_NFE ");
                    query.Append(" LEFT JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = MOVIMENTO.ID_CLIENTE ");
                    query.Append(" LEFT JOIN SEG_PRODUTO_CONTRATO PRODUTO_CONTRATO ON PRODUTO_CONTRATO.ID_PRODUTO_CONTRATO = MOVIMENTO.ID_PRODUTO_CONTRATO ");
                    query.Append(" LEFT JOIN SEG_PRODUTO PRODUTO ON PRODUTO.ID_PRODUTO = PRODUTO_CONTRATO.ID_PRODUTO ");
                    query.Append(" LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GHE_FONTE_AGENTE_EXAME_ASO ON GHE_FONTE_AGENTE_EXAME_ASO.ID_GHE_FONTE_AGENTE_EXAME_ASO = MOVIMENTO.ID_GHE_FONTE_AGENTE_EXAME_ASO ");
                    query.Append(" LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME GHE_FONTE_AGENTE_EXAME ON GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME = GHE_FONTE_AGENTE_EXAME_ASO.ID_GHE_FONTE_AGENTE_EXAME ");
                    query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO_EXAME_ASO CLIENTE_FUNCAO_EXAME_ASO ON CLIENTE_FUNCAO_EXAME_ASO.ID_CLIENTE_FUNCAO_EXAME_ASO = MOVIMENTO.ID_CLIENTE_FUNCAO_EXAME_ASO ");
                    query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO_EXAME CLIENTE_FUNCAO_EXAME ON CLIENTE_FUNCAO_EXAME.ID_CLIENTE_FUNCAO_EXAME = CLIENTE_FUNCAO_EXAME_ASO.ID_CLIENTE_FUNCAO_EXAME ");
                    query.Append(" LEFT JOIN SEG_EXAME EXAME ON (EXAME.ID_EXAME = GHE_FONTE_AGENTE_EXAME.ID_EXAME OR EXAME.ID_EXAME = CLIENTE_FUNCAO_EXAME.ID_EXAME) ");
                    query.Append(" LEFT JOIN SEG_CENTROCUSTO CENTRO_CUSTO ON CENTRO_CUSTO.ID = MOVIMENTO.ID_CENTROCUSTO ");
                    query.Append(" LEFT JOIN SEG_ASO ASO ON ASO.ID_ASO = MOVIMENTO.ID_ASO ");
                    query.Append(" WHERE MOVIMENTO.ID_EMPRESA = :VALUE10 ");

                    if (isClient)
                        query.Append(" AND MOVIMENTO.ID_CLIENTE = :VALUE1");

                    if (isCentroCusto)
                        query.Append(" AND MOVIMENTO.ID_CENTROCUSTO = :VALUE11 ");
                    
                    if (isDataInclusao)
                        query.Append(" AND MOVIMENTO.DT_INCLUSAO BETWEEN :VALUE2 AND :VALUE3");
                    
                    if (isDataFaturamento)
                        query.Append(" AND MOVIMENTO.DT_FATURAMENTO BETWEEN :VALUE4 AND :VALUE5 ");
                    
                    if (isNfe)
                        query.Append(" AND NFE.NUMERO_NFE = :VALUE6 ");
                    
                    if (isSituacao)
                        query.Append(" AND MOVIMENTO.SITUACAO = :VALUE7 ");

                    if (isParticular)
                        query.Append(" AND CLIENTE.PARTICULAR = TRUE");

                    if (isProduto)
                        query.Append(" AND PRODUTO.ID_PRODUTO = :VALUE9 ");
                    
                    if (isExame)
                        query.Append(" AND EXAME.ID_EXAME = :VALUE8 ");

                    if (isDataAtendimento)
                        query.Append(" AND ASO.DATA_ASO BETWEEN :VALUE12 AND :VALUE13 ");

                    if (isCredenciada)
                        query.Append(" AND MOVIMENTO.ID_CLIENTE_CREDENCIADA = :VALUE14 ");

                    query.Append(" ) AS QUANTIDADE, ");

                    #endregion

                    #region total valor

                    query.Append(" (SELECT SUM(PRC_UNIT * QUANTIDADE) FROM SEG_MOVIMENTO MOVIMENTO ");
                    query.Append(" LEFT JOIN SEG_NFE NFE ON NFE.ID_NFE = MOVIMENTO.ID_NFE ");
                    query.Append(" LEFT JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = MOVIMENTO.ID_CLIENTE ");
                    query.Append(" LEFT JOIN SEG_PRODUTO_CONTRATO PRODUTO_CONTRATO ON PRODUTO_CONTRATO.ID_PRODUTO_CONTRATO = MOVIMENTO.ID_PRODUTO_CONTRATO ");
                    query.Append(" LEFT JOIN SEG_PRODUTO PRODUTO ON PRODUTO.ID_PRODUTO = PRODUTO_CONTRATO.ID_PRODUTO ");
                    query.Append(" LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GHE_FONTE_AGENTE_EXAME_ASO ON GHE_FONTE_AGENTE_EXAME_ASO.ID_GHE_FONTE_AGENTE_EXAME_ASO = MOVIMENTO.ID_GHE_FONTE_AGENTE_EXAME_ASO ");
                    query.Append(" LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME GHE_FONTE_AGENTE_EXAME ON GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME = GHE_FONTE_AGENTE_EXAME_ASO.ID_GHE_FONTE_AGENTE_EXAME ");
                    query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO_EXAME_ASO CLIENTE_FUNCAO_EXAME_ASO ON CLIENTE_FUNCAO_EXAME_ASO.ID_CLIENTE_FUNCAO_EXAME_ASO = MOVIMENTO.ID_CLIENTE_FUNCAO_EXAME_ASO ");
                    query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO_EXAME CLIENTE_FUNCAO_EXAME ON CLIENTE_FUNCAO_EXAME.ID_CLIENTE_FUNCAO_EXAME = CLIENTE_FUNCAO_EXAME_ASO.ID_CLIENTE_FUNCAO_EXAME ");
                    query.Append(" LEFT JOIN SEG_EXAME EXAME ON (EXAME.ID_EXAME = GHE_FONTE_AGENTE_EXAME.ID_EXAME OR EXAME.ID_EXAME = CLIENTE_FUNCAO_EXAME.ID_EXAME) ");
                    query.Append(" LEFT JOIN SEG_CENTROCUSTO CENTRO_CUSTO ON CENTRO_CUSTO.ID = MOVIMENTO.ID_CENTROCUSTO ");
                    query.Append(" LEFT JOIN SEG_ASO ASO ON ASO.ID_ASO = MOVIMENTO.ID_ASO ");
                    query.Append(" WHERE MOVIMENTO.ID_EMPRESA = :VALUE10 ");

                    if (isClient)
                        query.Append(" AND MOVIMENTO.ID_CLIENTE = :VALUE1");

                    if (isCentroCusto)
                        query.Append(" AND MOVIMENTO.ID_CENTROCUSTO = :VALUE11 ");
                    
                    if (isDataInclusao)
                        query.Append(" AND MOVIMENTO.DT_INCLUSAO BETWEEN :VALUE2 AND :VALUE3 ");

                    if (isDataFaturamento)
                        query.Append(" AND MOVIMENTO.DT_FATURAMENTO BETWEEN :VALUE4 AND :VALUE5 ");
                    
                    if (isNfe)
                        query.Append(" AND NFE.NUMERO_NFE = :VALUE6 ");
                    
                    if (isSituacao)
                        query.Append(" AND MOVIMENTO.SITUACAO = :VALUE7 ");

                    if (isParticular)
                        query.Append(" AND CLIENTE.PARTICULAR = TRUE");
                    
                    if (isProduto)
                        query.Append(" AND PRODUTO.ID_PRODUTO = :VALUE9 ");
                    
                    if (isExame)
                        query.Append(" AND EXAME.ID_EXAME = :VALUE8 ");

                    if (isDataAtendimento)
                        query.Append(" AND ASO.DATA_ASO BETWEEN :VALUE12 AND :VALUE13 ");

                    if (isCredenciada)
                        query.Append(" AND MOVIMENTO.ID_CLIENTE_CREDENCIADA = :VALUE14 ");

                    query.Append(" ) AS VALOR, ");


                    #endregion

                    #region quantidade produtos

                    query.Append(" (SELECT COUNT(*) FROM SEG_MOVIMENTO MOVIMENTO  ");
                    query.Append(" LEFT JOIN SEG_NFE NFE ON NFE.ID_NFE = MOVIMENTO.ID_NFE ");
                    query.Append(" LEFT JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = MOVIMENTO.ID_CLIENTE ");
                    query.Append(" LEFT JOIN SEG_PRODUTO_CONTRATO PRODUTO_CONTRATO ON PRODUTO_CONTRATO.ID_PRODUTO_CONTRATO = MOVIMENTO.ID_PRODUTO_CONTRATO ");
                    query.Append(" LEFT JOIN SEG_PRODUTO PRODUTO ON PRODUTO.ID_PRODUTO = PRODUTO_CONTRATO.ID_PRODUTO ");
                    query.Append(" LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GHE_FONTE_AGENTE_EXAME_ASO ON GHE_FONTE_AGENTE_EXAME_ASO.ID_GHE_FONTE_AGENTE_EXAME_ASO = MOVIMENTO.ID_GHE_FONTE_AGENTE_EXAME_ASO ");
                    query.Append(" LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME GHE_FONTE_AGENTE_EXAME ON GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME = GHE_FONTE_AGENTE_EXAME_ASO.ID_GHE_FONTE_AGENTE_EXAME ");
                    query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO_EXAME_ASO CLIENTE_FUNCAO_EXAME_ASO ON CLIENTE_FUNCAO_EXAME_ASO.ID_CLIENTE_FUNCAO_EXAME_ASO = MOVIMENTO.ID_CLIENTE_FUNCAO_EXAME_ASO ");
                    query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO_EXAME CLIENTE_FUNCAO_EXAME ON CLIENTE_FUNCAO_EXAME.ID_CLIENTE_FUNCAO_EXAME = CLIENTE_FUNCAO_EXAME_ASO.ID_CLIENTE_FUNCAO_EXAME ");
                    query.Append(" LEFT JOIN SEG_EXAME EXAME ON (EXAME.ID_EXAME = GHE_FONTE_AGENTE_EXAME.ID_EXAME OR EXAME.ID_EXAME = CLIENTE_FUNCAO_EXAME.ID_EXAME) ");
                    query.Append(" LEFT JOIN SEG_CENTROCUSTO CENTRO_CUSTO ON CENTRO_CUSTO.ID = MOVIMENTO.ID_CENTROCUSTO ");
                    query.Append(" LEFT JOIN SEG_ASO ASO ON ASO.ID_ASO = MOVIMENTO.ID_ASO ");
                    query.Append(" WHERE MOVIMENTO.ID_EMPRESA = :VALUE10 AND MOVIMENTO.ID_PRODUTO_CONTRATO IS NOT NULL ");

                    if (isClient)
                        query.Append(" AND MOVIMENTO.ID_CLIENTE = :VALUE1");

                    if (isCentroCusto)
                        query.Append(" AND MOVIMENTO.ID_CENTROCUSTO = :VALUE11 ");

                    if (isDataInclusao)
                        query.Append(" AND MOVIMENTO.DT_INCLUSAO BETWEEN :VALUE2 AND :VALUE3 ");

                    if (isDataFaturamento)
                        query.Append(" AND MOVIMENTO.DT_FATURAMENTO BETWEEN :VALUE4 AND :VALUE5 ");
                    
                    if (isNfe)
                        query.Append(" AND NFE.NUMERO_NFE = :VALUE6 ");
                    
                    if (isSituacao)
                        query.Append(" AND MOVIMENTO.SITUACAO = :VALUE7 ");
                    
                    if (isParticular)
                        query.Append(" AND CLIENTE.PARTICULAR = TRUE");
                    
                    if (isProduto)
                        query.Append(" AND PRODUTO.ID_PRODUTO = :VALUE9 ");
                    
                    if (isExame)
                        query.Append(" AND EXAME.ID_EXAME = :VALUE8 ");

                    if (isDataAtendimento)
                        query.Append(" AND ASO.DATA_ASO BETWEEN :VALUE12 AND :VALUE13 ");

                    if (isCredenciada)
                        query.Append(" AND MOVIMENTO.ID_CLIENTE_CREDENCIADA = :VALUE14 ");

                    query.Append(" ) AS QUANTIDADEPRODUTO, ");
                    
                    #endregion

                    #region exames

                    query.Append(" (SELECT COUNT(*) FROM SEG_MOVIMENTO MOVIMENTO  ");
                    query.Append(" LEFT JOIN SEG_NFE NFE ON NFE.ID_NFE = MOVIMENTO.ID_NFE ");
                    query.Append(" LEFT JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = MOVIMENTO.ID_CLIENTE ");
                    query.Append(" LEFT JOIN SEG_PRODUTO_CONTRATO PRODUTO_CONTRATO ON PRODUTO_CONTRATO.ID_PRODUTO_CONTRATO = MOVIMENTO.ID_PRODUTO_CONTRATO ");
                    query.Append(" LEFT JOIN SEG_PRODUTO PRODUTO ON PRODUTO.ID_PRODUTO = PRODUTO_CONTRATO.ID_PRODUTO ");
                    query.Append(" LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GHE_FONTE_AGENTE_EXAME_ASO ON GHE_FONTE_AGENTE_EXAME_ASO.ID_GHE_FONTE_AGENTE_EXAME_ASO = MOVIMENTO.ID_GHE_FONTE_AGENTE_EXAME_ASO ");
                    query.Append(" LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME GHE_FONTE_AGENTE_EXAME ON GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME = GHE_FONTE_AGENTE_EXAME_ASO.ID_GHE_FONTE_AGENTE_EXAME ");
                    query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO_EXAME_ASO CLIENTE_FUNCAO_EXAME_ASO ON CLIENTE_FUNCAO_EXAME_ASO.ID_CLIENTE_FUNCAO_EXAME_ASO = MOVIMENTO.ID_CLIENTE_FUNCAO_EXAME_ASO ");
                    query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO_EXAME CLIENTE_FUNCAO_EXAME ON CLIENTE_FUNCAO_EXAME.ID_CLIENTE_FUNCAO_EXAME = CLIENTE_FUNCAO_EXAME_ASO.ID_CLIENTE_FUNCAO_EXAME ");
                    query.Append(" LEFT JOIN SEG_EXAME EXAME ON (EXAME.ID_EXAME = GHE_FONTE_AGENTE_EXAME.ID_EXAME OR EXAME.ID_EXAME = CLIENTE_FUNCAO_EXAME.ID_EXAME) ");
                    query.Append(" LEFT JOIN SEG_CENTROCUSTO CENTRO_CUSTO ON CENTRO_CUSTO.ID = MOVIMENTO.ID_CENTROCUSTO ");
                    query.Append(" LEFT JOIN SEG_ASO ASO ON ASO.ID_ASO = MOVIMENTO.ID_ASO ");
                    query.Append(" WHERE MOVIMENTO.ID_EMPRESA = :VALUE10 AND (MOVIMENTO.ID_GHE_FONTE_AGENTE_EXAME_ASO IS NOT NULL OR MOVIMENTO.ID_CLIENTE_FUNCAO_EXAME_ASO IS NOT NULL) AND MOVIMENTO.ID_PRODUTO_CONTRATO IS NULL ");

                    if (isClient)
                        query.Append(" AND MOVIMENTO.ID_CLIENTE = :VALUE1");

                    if (isCentroCusto)
                        query.Append(" AND MOVIMENTO.ID_CENTROCUSTO = :VALUE11 ");
                 
                    if (isDataInclusao)
                        query.Append(" AND MOVIMENTO.DT_INCLUSAO BETWEEN :VALUE2 AND :VALUE3 ");
                    
                    if (isDataFaturamento)
                        query.Append(" AND MOVIMENTO.DT_FATURAMENTO BETWEEN :VALUE4 AND :VALUE5 ");
                    
                    if (isNfe)
                        query.Append(" AND NFE.NUMERO_NFE = :VALUE6 ");
                    
                    if (isSituacao)
                        query.Append(" AND MOVIMENTO.SITUACAO = :VALUE7 ");
                    
                    if (isParticular)
                        query.Append(" AND CLIENTE.PARTICULAR = TRUE");
                    
                    if (isProduto)
                        query.Append(" AND PRODUTO.ID_PRODUTO = :VALUE9 ");
                    
                    if (isExame)
                        query.Append(" AND EXAME.ID_EXAME = :VALUE8 ");

                    if (isDataAtendimento)
                        query.Append(" AND ASO.DATA_ASO BETWEEN :VALUE12 AND :VALUE13 ");

                    if (isCredenciada)
                        query.Append(" AND MOVIMENTO.ID_CLIENTE_CREDENCIADA = :VALUE14");

                    query.Append("  ) AS QUANTIDADEEXAME ");

                    #endregion

                }


                NpgsqlDataAdapter adapterTotal = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapterTotal.SelectCommand.Parameters[0].Value = movimento.Cliente != null ? movimento.Cliente.Id : null;
                
                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                adapterTotal.SelectCommand.Parameters[1].Value = movimento.DataInclusao != null ? movimento.DataInclusao : null;
                
                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Timestamp));
                adapterTotal.SelectCommand.Parameters[2].Value = dataFinalLancamento;
                
                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Timestamp));
                adapterTotal.SelectCommand.Parameters[3].Value = movimento.DataFaturamento != null ? movimento.DataFaturamento : null;
                
                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Timestamp));
                adapterTotal.SelectCommand.Parameters[4].Value = dataFinalFaturamento;
                
                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Integer));
                adapterTotal.SelectCommand.Parameters[5].Value = movimento.Nfe != null ? movimento.Nfe.NumeroNfe : (long?)null;
                
                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                adapterTotal.SelectCommand.Parameters[6].Value = !String.IsNullOrWhiteSpace(movimento.Situacao) ? movimento.Situacao : null;
                
                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Integer));
                adapterTotal.SelectCommand.Parameters[7].Value = exame != null ? exame.Id : null;
                
                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Integer));
                adapterTotal.SelectCommand.Parameters[8].Value = produto != null ? produto.Id : null;

                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Integer));
                adapterTotal.SelectCommand.Parameters[9].Value = movimento.Empresa.Id;

                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Integer));
                adapterTotal.SelectCommand.Parameters[10].Value = movimento.CentroCusto != null ? movimento.CentroCusto.Id : (long?)null;

                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Timestamp));
                adapterTotal.SelectCommand.Parameters[11].Value = DataInicialAtendimento == null ? (DateTime?)null : DataInicialAtendimento;

                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Timestamp));
                adapterTotal.SelectCommand.Parameters[12].Value = DataFinalAtendimento == null ? (DateTime?)null : DataFinalAtendimento;

                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Integer));
                adapterTotal.SelectCommand.Parameters[13].Value = movimento.ClienteCredenciada != null ? movimento.ClienteCredenciada.Id : (long?)null;
                
                adapterTotal.Fill(ds, "Total");

                return ds;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMovimentoByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public Movimento findMovimentoByProdutoOrExame(Movimento movimento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMovimentoByProdutoOrExame");

            Movimento movimentoProcurado = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID_MOVIMENTO, ID_GHE_FONTE_AGENTE_EXAME_ASO, ID_CLIENTE_FUNCAO_EXAME_ASO, ID_CLIENTE, ");
                query.Append(" ID_PRODUTO_CONTRATO, ID_NFE, DT_INCLUSAO, DT_FATURAMENTO, SITUACAO, PRC_UNIT,  ");
                query.Append(" COM_VALOR, ID_ESTUDO, ID_ASO, QUANTIDADE, USUARIO, ID_USUARIO, ID_EMPRESA, PRECO_CUSTO, ID_CLIENTE_UNIDADE, DATA_GRAVACAO, ID_CENTROCUSTO, ID_CONTRATO_EXAME, ID_CLIENTE_CREDENCIADA");
                query.Append(" FROM SEG_MOVIMENTO WHERE TRUE ");

                // verificando os produtos enviados.

                if (movimento.GheFonteAgenteExameAso != null)
                    query.Append(" AND ID_GHE_FONTE_AGENTE_EXAME_ASO = :VALUE1 ");

                if (movimento.ClienteFuncaoExameAso != null)
                    query.Append(" AND ID_CLIENTE_FUNCAO_EXAME_ASO = :VALUE2 ");

                if (movimento.ContratoProduto != null)
                    query.Append(" AND ID_PRODUTO_CONTRATO = :VALUE3 ");

                if (movimento.Nfe != null)
                    query.Append(" AND ID_NFE = :VALUE4 ");

                if (movimento.Estudo != null)
                    query.Append(" AND ID_ESTUDO = :VALUE5 ");

                if (movimento.Aso != null)
                    query.Append(" AND ID_ASO = :VALUE6 ");

                if (!String.IsNullOrEmpty(movimento.Situacao))
                    query.Append(" AND SITUACAO = :VALUE7 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = movimento.GheFonteAgenteExameAso != null ? movimento.GheFonteAgenteExameAso.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = movimento.ClienteFuncaoExameAso != null ? movimento.ClienteFuncaoExameAso.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = movimento.ContratoProduto != null ? movimento.ContratoProduto.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = movimento.Nfe != null ? movimento.Nfe.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Integer));
                command.Parameters[4].Value = movimento.Estudo != null ? movimento.Estudo.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Integer));
                command.Parameters[5].Value = movimento.Aso != null ? movimento.Aso.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = String.IsNullOrEmpty(movimento.Situacao) ? null : movimento.Situacao;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    movimentoProcurado = new Movimento ( dr.GetInt64(0), dr.IsDBNull(1) ? null : new GheFonteAgenteExameAso(dr.GetInt64(1), null, null, null, null, false, false, null, false, null, null, null, null, false, null, null, null, false, false, null,null, false, false, null, null, true, true),  dr.IsDBNull(2) ? null : new ClienteFuncaoExameASo(dr.GetInt64(2), null, null, null, null, false, false, null, false, null, null, null, null, false, null, null, null, false, null, null, null, null, false), dr.IsDBNull(3) ? null : new Cliente(dr.GetInt64(3)),  dr.IsDBNull(4) ? null : new ContratoProduto(dr.GetInt64(4), null, null, null, null, null, null, String.Empty, true), dr.IsDBNull(5) ? null : new Nfe(dr.GetInt64(5), null, null, (long?)null, 0, DateTime.Now, null, null, 0, 0, null, null, null, null, DateTime.Now, null, string.Empty, null, string.Empty, null, null, null), dr.GetDateTime(6), dr.IsDBNull(7) ? null : (DateTime?)dr.GetDateTime(7), dr.GetString(8), dr.GetDecimal(9), dr.IsDBNull(10) ? null : (Decimal?)dr.GetDecimal(10), dr.IsDBNull(11) ? null : new Estudo(dr.GetInt64(11)), dr.IsDBNull(12) ? null : new Aso(dr.GetInt64(12)), dr.GetInt32(13), dr.IsDBNull(15) ? null : new Usuario(dr.GetInt64(15), dr.GetString(14), string.Empty, string.Empty, string.Empty, null, String.Empty, String.Empty, String.Empty, String.Empty, null, null, String.Empty, false, null, String.Empty, false, string.Empty, string.Empty), dr.IsDBNull(16) ? null :  new Empresa(dr.GetInt64(16), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, null, null, false, null, string.Empty, 0, 0, 0, 0, 0, 0, 0), dr.GetDecimal(17), dr.IsDBNull(18) ? null : new Cliente(dr.GetInt64(18)), null, dr.GetDateTime(19), dr.IsDBNull(20) ? null : new CentroCusto(dr.GetInt64(20), null, string.Empty, string.Empty), null, dr.IsDBNull(22) ? null : new Cliente(dr.GetInt64(22)));
                    movimentoProcurado.ContratoExame = dr.IsDBNull(21) ? null : new ContratoExame(dr.GetInt64(21));
                    
                }

                return movimentoProcurado;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMovimentoByProdutoOrExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public void changeStatus(Movimento movimento, String status, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método changeStatus");

            try
            {
                
                NpgsqlCommand command = new NpgsqlCommand("UPDATE SEG_MOVIMENTO SET SITUACAO = :VALUE2 WHERE ID_MOVIMENTO = :VALUE1", dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = movimento.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar)); 
                command.Parameters[1].Value = status;
                                
                command.ExecuteNonQuery();

                
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - changeStatus", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findMovimentoDetalhado(Cliente cliente, DateTime? dataInicialLancamento, DateTime? dataFinalLancamento, Boolean analitico, Boolean faturado, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMovimentoDetalhado");

            try
            {
                StringBuilder query = new StringBuilder();

                if (analitico)
                {
                    
                    if (cliente != null)
                    {
                        query.Append(" SELECT  ");
                        query.Append(" ASO.CODIGO_ASO,  ");
                        query.Append(" ASO.NOME_FUNCIONARIO,  ");
                        query.Append(" ASO.RG_FUNCIONARIO,  ");
                        query.Append(" EXTRACT(YEAR FROM AGE(ASO.DATANASC_FUNCIONARIO)) AS IDADE,  ");
                        query.Append(" FUNCAO.DESCRICAO,  ");
                        query.Append(" ASO.RAZAO_EMPRESA,  ");
                        query.Append(" EXAME.DESCRICAO AS EXAME_DESCRICAO,  ");
                        query.Append(" PERIODICIDADE.DESCRICAO AS PERIODICIDADE_DESCRICAO,  ");
                        query.Append(" MOVIMENTO.DT_INCLUSAO,  ");
                        query.Append(" ASO.DATA_ASO,  ");
                        query.Append(" MOVIMENTO.PRC_UNIT  ");
                        query.Append(" FROM SEG_MOVIMENTO MOVIMENTO ");
                        query.Append(" JOIN SEG_ASO ASO ON ASO.ID_ASO = MOVIMENTO.ID_ASO AND ASO.SITUACAO = 'F' ");
                        query.Append(" JOIN SEG_PERIODICIDADE PERIODICIDADE ON PERIODICIDADE.ID_PERIODICIDADE = ASO.ID_PERIODICIDADE ");
                        query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTEFUNCIONARIO ON CLIENTEFUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO = ASO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                        query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTEFUNCAO ON CLIENTEFUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTEFUNCIONARIO.ID_SEG_CLIENTE_FUNCAO ");
                        query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTEFUNCAO.ID_FUNCAO ");
                        query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GHEASO ON GHEASO.ID_GHE_FONTE_AGENTE_EXAME_ASO = MOVIMENTO.ID_GHE_FONTE_AGENTE_EXAME_ASO ");
                        query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GHEEXAME ON GHEEXAME.ID_GHE_FONTE_AGENTE_EXAME = GHEASO.ID_GHE_FONTE_AGENTE_EXAME ");
                        query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = GHEEXAME.ID_EXAME ");

                        if (faturado)
                        {
                            query.Append(" WHERE MOVIMENTO.SITUACAO <> 'C' AND MOVIMENTO.ID_CLIENTE = :VALUE1 AND MOVIMENTO.DT_INCLUSAO BETWEEN :VALUE2 AND :VALUE3 ");
                        }
                        else
                        {
                            query.Append(" WHERE MOVIMENTO.SITUACAO = 'P' AND DT_FATURAMENTO IS NULL AND MOVIMENTO.ID_CLIENTE = :VALUE1 AND MOVIMENTO.DT_INCLUSAO BETWEEN :VALUE2 AND :VALUE3 ");
                        }

                        
                        query.Append(" UNION (  ");
                        query.Append(" SELECT ASO.CODIGO_ASO, ASO.NOME_FUNCIONARIO, ASO.RG_FUNCIONARIO,  ");
                        query.Append(" EXTRACT(YEAR FROM AGE(ASO.DATANASC_FUNCIONARIO)) AS IDADE,  ");
                        query.Append(" FUNCAO.DESCRICAO, ASO.RAZAO_EMPRESA, EXAME.DESCRICAO, PERIODICIDADE.DESCRICAO, MOVIMENTO.DT_INCLUSAO, ASO.DATA_ASO, MOVIMENTO.PRC_UNIT ");
                        query.Append(" FROM SEG_MOVIMENTO MOVIMENTO ");
                        query.Append(" JOIN SEG_ASO ASO ON ASO.ID_ASO = MOVIMENTO.ID_ASO AND ASO.SITUACAO = 'F' ");
                        query.Append(" JOIN SEG_PERIODICIDADE PERIODICIDADE ON PERIODICIDADE.ID_PERIODICIDADE = ASO.ID_PERIODICIDADE ");
                        query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTEFUNCIONARIO ON CLIENTEFUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO = ASO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                        query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTEFUNCAO ON CLIENTEFUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTEFUNCIONARIO.ID_SEG_CLIENTE_FUNCAO ");
                        query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTEFUNCAO.ID_FUNCAO ");
                        query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME_ASO CLIENTEASO ON CLIENTEASO.ID_CLIENTE_FUNCAO_EXAME_ASO = MOVIMENTO.ID_CLIENTE_FUNCAO_EXAME_ASO ");
                        query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME CLIENTEEXAME ON CLIENTEEXAME.ID_CLIENTE_FUNCAO_EXAME = CLIENTEASO.ID_CLIENTE_FUNCAO_EXAME ");
                        query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = CLIENTEEXAME.ID_EXAME ");

                        if (faturado)
                        {
                            query.Append(" WHERE MOVIMENTO.SITUACAO <> 'C' AND MOVIMENTO.ID_CLIENTE = :VALUE1 AND MOVIMENTO.DT_INCLUSAO BETWEEN :VALUE2 AND :VALUE3) ");

                        }
                        else
                        {
                            query.Append(" WHERE MOVIMENTO.SITUACAO = 'P' AND DT_FATURAMENTO IS NULL AND MOVIMENTO.ID_CLIENTE = :VALUE1 AND MOVIMENTO.DT_INCLUSAO BETWEEN :VALUE2 AND :VALUE3) ");
                        }
                        
                        query.Append(" ORDER BY CODIGO_ASO ");
                    }
                    else
                    {
                        query.Append(" SELECT  ");
                        query.Append(" ASO.CODIGO_ASO,  ");
                        query.Append(" ASO.NOME_FUNCIONARIO,  ");
                        query.Append(" ASO.RG_FUNCIONARIO,  ");
                        query.Append(" EXTRACT(YEAR FROM AGE(ASO.DATANASC_FUNCIONARIO)) AS IDADE,  ");
                        query.Append(" FUNCAO.DESCRICAO,  ");
                        query.Append(" ASO.RAZAO_EMPRESA,  ");
                        query.Append(" EXAME.DESCRICAO AS EXAME_DESCRICAO,  ");
                        query.Append(" PERIODICIDADE.DESCRICAO AS PERIODICIDADE_DESCRICAO,  ");
                        query.Append(" MOVIMENTO.DT_INCLUSAO,  ");
                        query.Append(" ASO.DATA_ASO,  ");
                        query.Append(" MOVIMENTO.PRC_UNIT  ");
                        query.Append(" FROM SEG_MOVIMENTO MOVIMENTO ");
                        query.Append(" JOIN SEG_ASO ASO ON ASO.ID_ASO = MOVIMENTO.ID_ASO AND ASO.SITUACAO = 'F' ");
                        query.Append(" JOIN SEG_PERIODICIDADE PERIODICIDADE ON PERIODICIDADE.ID_PERIODICIDADE = ASO.ID_PERIODICIDADE ");
                        query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTEFUNCIONARIO ON CLIENTEFUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO = ASO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                        query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTEFUNCAO ON CLIENTEFUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTEFUNCIONARIO.ID_SEG_CLIENTE_FUNCAO ");
                        query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTEFUNCAO.ID_FUNCAO ");
                        query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GHEASO ON GHEASO.ID_GHE_FONTE_AGENTE_EXAME_ASO = MOVIMENTO.ID_GHE_FONTE_AGENTE_EXAME_ASO ");
                        query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GHEEXAME ON GHEEXAME.ID_GHE_FONTE_AGENTE_EXAME = GHEASO.ID_GHE_FONTE_AGENTE_EXAME ");
                        query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = GHEEXAME.ID_EXAME ");

                        if (faturado)
                        {
                            query.Append(" WHERE MOVIMENTO.SITUACAO <> 'C' AND MOVIMENTO.DT_INCLUSAO BETWEEN :VALUE2 AND :VALUE3 ");
                        }
                        else
                        {
                            query.Append(" WHERE MOVIMENTO.SITUACAO = 'P' AND DT_FATURAMENTO IS NULL AND MOVIMENTO.DT_INCLUSAO BETWEEN :VALUE2 AND :VALUE3 ");
                        }
                        
                        
                        query.Append(" UNION (  ");
                        query.Append(" SELECT ASO.CODIGO_ASO, ASO.NOME_FUNCIONARIO, ASO.RG_FUNCIONARIO,  ");
                        query.Append(" EXTRACT(YEAR FROM AGE(ASO.DATANASC_FUNCIONARIO)) AS IDADE,  ");
                        query.Append(" FUNCAO.DESCRICAO, ASO.RAZAO_EMPRESA, EXAME.DESCRICAO, PERIODICIDADE.DESCRICAO, MOVIMENTO.DT_INCLUSAO, ASO.DATA_ASO, MOVIMENTO.PRC_UNIT ");
                        query.Append(" FROM SEG_MOVIMENTO MOVIMENTO ");
                        query.Append(" JOIN SEG_ASO ASO ON ASO.ID_ASO = MOVIMENTO.ID_ASO AND ASO.SITUACAO = 'F' ");
                        query.Append(" JOIN SEG_PERIODICIDADE PERIODICIDADE ON PERIODICIDADE.ID_PERIODICIDADE = ASO.ID_PERIODICIDADE ");
                        query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTEFUNCIONARIO ON CLIENTEFUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO = ASO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                        query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTEFUNCAO ON CLIENTEFUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTEFUNCIONARIO.ID_SEG_CLIENTE_FUNCAO ");
                        query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTEFUNCAO.ID_FUNCAO ");
                        query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME_ASO CLIENTEASO ON CLIENTEASO.ID_CLIENTE_FUNCAO_EXAME_ASO = MOVIMENTO.ID_CLIENTE_FUNCAO_EXAME_ASO ");
                        query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME CLIENTEEXAME ON CLIENTEEXAME.ID_CLIENTE_FUNCAO_EXAME = CLIENTEASO.ID_CLIENTE_FUNCAO_EXAME ");
                        query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = CLIENTEEXAME.ID_EXAME ");

                        if (faturado)
                        {
                            query.Append(" WHERE MOVIMENTO.SITUACAO <> 'C' AND MOVIMENTO.DT_INCLUSAO BETWEEN :VALUE2 AND :VALUE3) ");
                        }
                        else
                        {
                            query.Append(" WHERE MOVIMENTO.SITUACAO = 'P' AND DT_FATURAMENTO IS NULL AND MOVIMENTO.DT_INCLUSAO BETWEEN :VALUE2 AND :VALUE3) ");
                        }
                        
                        
                        query.Append(" ORDER BY CODIGO_ASO ");
                    }

                }
                else
                {
                    if (cliente != null)
                    {
                        query.Append(" SELECT ASO.CODIGO_ASO, ASO.NOME_FUNCIONARIO, ASO.RG_FUNCIONARIO, ");
                        query.Append(" EXTRACT(YEAR FROM AGE(ASO.DATANASC_FUNCIONARIO)) AS IDADE, ");
                        query.Append(" FUNCAO.DESCRICAO, ASO.RAZAO_EMPRESA, PERIODICIDADE.DESCRICAO AS PERIODICIDADE_DESCRICAO, DATE(MOVIMENTO.DT_INCLUSAO), ASO.DATA_ASO ");
                        query.Append(" FROM SEG_MOVIMENTO MOVIMENTO ");
                        query.Append(" JOIN SEG_ASO ASO ON ASO.ID_ASO = MOVIMENTO.ID_ASO AND ASO.SITUACAO = 'F' ");
                        query.Append(" JOIN SEG_PERIODICIDADE PERIODICIDADE ON PERIODICIDADE.ID_PERIODICIDADE = ASO.ID_PERIODICIDADE ");
                        query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTEFUNCIONARIO ON CLIENTEFUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO = ASO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                        query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTEFUNCAO ON CLIENTEFUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTEFUNCIONARIO.ID_SEG_CLIENTE_FUNCAO ");
                        query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTEFUNCAO.ID_FUNCAO ");
                        query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GHEASO ON GHEASO.ID_GHE_FONTE_AGENTE_EXAME_ASO = MOVIMENTO.ID_GHE_FONTE_AGENTE_EXAME_ASO ");
                        query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GHEEXAME ON GHEEXAME.ID_GHE_FONTE_AGENTE_EXAME = GHEASO.ID_GHE_FONTE_AGENTE_EXAME ");
                        query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = GHEEXAME.ID_EXAME ");

                        if (faturado)
                        {
                            query.Append(" WHERE MOVIMENTO.SITUACAO <> 'C' AND MOVIMENTO.ID_CLIENTE = :VALUE1 AND MOVIMENTO.DT_INCLUSAO BETWEEN :VALUE2 AND :VALUE3 ");
                        }
                        else
                        {
                            query.Append(" WHERE MOVIMENTO.SITUACAO = 'P' AND DT_FATURAMENTO IS NULL AND MOVIMENTO.ID_CLIENTE = :VALUE1 AND MOVIMENTO.DT_INCLUSAO BETWEEN :VALUE2 AND :VALUE3 ");
                        }
                                                
                        query.Append(" UNION ( ");
                        query.Append(" SELECT ASO.CODIGO_ASO, ASO.NOME_FUNCIONARIO, ASO.RG_FUNCIONARIO, ");
                        query.Append(" EXTRACT(YEAR FROM AGE(ASO.DATANASC_FUNCIONARIO)) AS IDADE, ");
                        query.Append(" FUNCAO.DESCRICAO, ASO.RAZAO_EMPRESA, PERIODICIDADE.DESCRICAO AS PERIODICIDADE_DESCRICAO, DATE(MOVIMENTO.DT_INCLUSAO), ASO.DATA_ASO ");
                        query.Append(" FROM SEG_MOVIMENTO MOVIMENTO ");
                        query.Append(" JOIN SEG_ASO ASO ON ASO.ID_ASO = MOVIMENTO.ID_ASO AND ASO.SITUACAO = 'F' ");
                        query.Append(" JOIN SEG_PERIODICIDADE PERIODICIDADE ON PERIODICIDADE.ID_PERIODICIDADE = ASO.ID_PERIODICIDADE ");
                        query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTEFUNCIONARIO ON CLIENTEFUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO = ASO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                        query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTEFUNCAO ON CLIENTEFUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTEFUNCIONARIO.ID_SEG_CLIENTE_FUNCAO ");
                        query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTEFUNCAO.ID_FUNCAO ");
                        query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME_ASO CLIENTEASO ON CLIENTEASO.ID_CLIENTE_FUNCAO_EXAME_ASO = MOVIMENTO.ID_CLIENTE_FUNCAO_EXAME_ASO ");
                        query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME CLIENTEEXAME ON CLIENTEEXAME.ID_CLIENTE_FUNCAO_EXAME = CLIENTEASO.ID_CLIENTE_FUNCAO_EXAME ");
                        query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = CLIENTEEXAME.ID_EXAME ");

                        if (faturado)
                        {
                            query.Append(" WHERE MOVIMENTO.SITUACAO <> 'C' AND MOVIMENTO.ID_CLIENTE = :VALUE1 AND MOVIMENTO.DT_INCLUSAO BETWEEN :VALUE2 AND :VALUE3 ) ");
                        }
                        else
                        {
                            query.Append(" WHERE MOVIMENTO.SITUACAO = 'P' AND DT_FATURAMENTO IS NULL AND MOVIMENTO.ID_CLIENTE = :VALUE1 AND MOVIMENTO.DT_INCLUSAO BETWEEN :VALUE2 AND :VALUE3 ) ");
                        }

                        
                        query.Append(" ORDER BY NOME_FUNCIONARIO ASC ");

                    }
                    else
                    {
                        query.Append(" SELECT ASO.CODIGO_ASO, ASO.NOME_FUNCIONARIO, ASO.RG_FUNCIONARIO, ");
                        query.Append(" EXTRACT(YEAR FROM AGE(ASO.DATANASC_FUNCIONARIO)) AS IDADE, ");
                        query.Append(" FUNCAO.DESCRICAO, ASO.RAZAO_EMPRESA, PERIODICIDADE.DESCRICAO AS PERIODICIDADE_DESCRICAO, DATE(MOVIMENTO.DT_INCLUSAO), ASO.DATA_ASO ");
                        query.Append(" FROM SEG_MOVIMENTO MOVIMENTO ");
                        query.Append(" JOIN SEG_ASO ASO ON ASO.ID_ASO = MOVIMENTO.ID_ASO AND ASO.SITUACAO = 'F' ");
                        query.Append(" JOIN SEG_PERIODICIDADE PERIODICIDADE ON PERIODICIDADE.ID_PERIODICIDADE = ASO.ID_PERIODICIDADE ");
                        query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTEFUNCIONARIO ON CLIENTEFUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO = ASO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                        query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTEFUNCAO ON CLIENTEFUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTEFUNCIONARIO.ID_SEG_CLIENTE_FUNCAO ");
                        query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTEFUNCAO.ID_FUNCAO ");
                        query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GHEASO ON GHEASO.ID_GHE_FONTE_AGENTE_EXAME_ASO = MOVIMENTO.ID_GHE_FONTE_AGENTE_EXAME_ASO ");
                        query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GHEEXAME ON GHEEXAME.ID_GHE_FONTE_AGENTE_EXAME = GHEASO.ID_GHE_FONTE_AGENTE_EXAME ");
                        query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = GHEEXAME.ID_EXAME ");

                        if (faturado)
                        {
                            query.Append(" WHERE MOVIMENTO.SITUACAO <> 'C' AND MOVIMENTO.DT_INCLUSAO BETWEEN :VALUE2 AND :VALUE3  ");
                        }
                        else
                        {
                            query.Append(" WHERE MOVIMENTO.SITUACAO = 'P' AND DT_FATURAMENTO IS NULL AND MOVIMENTO.DT_INCLUSAO BETWEEN :VALUE2 AND :VALUE3  ");
                        }
                        
                        
                        query.Append(" UNION ( ");
                        query.Append(" SELECT ASO.CODIGO_ASO, ASO.NOME_FUNCIONARIO, ASO.RG_FUNCIONARIO, ");
                        query.Append(" EXTRACT(YEAR FROM AGE(ASO.DATANASC_FUNCIONARIO)) AS IDADE, ");
                        query.Append(" FUNCAO.DESCRICAO, ASO.RAZAO_EMPRESA, PERIODICIDADE.DESCRICAO AS PERIODICIDADE_DESCRICAO, DATE(MOVIMENTO.DT_INCLUSAO), ASO.DATA_ASO ");
                        query.Append(" FROM SEG_MOVIMENTO MOVIMENTO ");
                        query.Append(" JOIN SEG_ASO ASO ON ASO.ID_ASO = MOVIMENTO.ID_ASO AND ASO.SITUACAO = 'F' ");
                        query.Append(" JOIN SEG_PERIODICIDADE PERIODICIDADE ON PERIODICIDADE.ID_PERIODICIDADE = ASO.ID_PERIODICIDADE ");
                        query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTEFUNCIONARIO ON CLIENTEFUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO = ASO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                        query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTEFUNCAO ON CLIENTEFUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTEFUNCIONARIO.ID_SEG_CLIENTE_FUNCAO ");
                        query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTEFUNCAO.ID_FUNCAO ");
                        query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME_ASO CLIENTEASO ON CLIENTEASO.ID_CLIENTE_FUNCAO_EXAME_ASO = MOVIMENTO.ID_CLIENTE_FUNCAO_EXAME_ASO ");
                        query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME CLIENTEEXAME ON CLIENTEEXAME.ID_CLIENTE_FUNCAO_EXAME = CLIENTEASO.ID_CLIENTE_FUNCAO_EXAME ");
                        query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = CLIENTEEXAME.ID_EXAME ");

                        if (faturado)
                        {
                            query.Append(" WHERE MOVIMENTO.SITUACAO <> 'C' AND MOVIMENTO.DT_INCLUSAO BETWEEN :VALUE2 AND :VALUE3 ) ");
                        }
                        else
                        {
                            query.Append(" WHERE MOVIMENTO.SITUACAO = 'P' AND DT_FATURAMENTO IS NULL AND MOVIMENTO.DT_INCLUSAO BETWEEN :VALUE2 AND :VALUE3 ) ");
                        }
                        
                        query.Append(" ORDER BY NOME_FUNCIONARIO ASC ");
                    }

                }
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = cliente != null ? cliente.Id : null;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[1].Value = dataInicialLancamento;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[2].Value = dataFinalLancamento;

                DataSet ds = new DataSet();
                adapter.Fill(ds, "Movimentos");

                return ds;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMovimentoDetalhado", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public Decimal findSomaMovimentoByFilter(Movimento movimento, DateTime? dataFinalLancamento, DateTime? dataFinalFaturamento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMovimentoByFilter");

            try
            {

                StringBuilder query = new StringBuilder();
                Decimal ValorTotal = 0;


                if (movimento.isNullForFilter())
                {
                    // sem parâmetro para pesquisa

                    query.Append(" SELECT SUM(MOV.PRC_UNIT * MOV.QUANTIDADE) AS VALORTOTAL ");
                    query.Append(" FROM SEG_MOVIMENTO MOV ");

                    query.Append(" LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA ON GFAEA.ID_GHE_FONTE_AGENTE_EXAME_ASO = MOV.ID_GHE_FONTE_AGENTE_EXAME_ASO ");
                    query.Append(" LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME GFAE ON GFAE.ID_GHE_FONTE_AGENTE_EXAME = GFAEA.ID_GHE_FONTE_AGENTE_EXAME ");
                    query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA ON CFEA.ID_CLIENTE_FUNCAO_EXAME_ASO = MOV.ID_CLIENTE_FUNCAO_EXAME_ASO ");
                    query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO_EXAME CFE ON CFE.ID_CLIENTE_FUNCAO_EXAME = CFEA.ID_CLIENTE_FUNCAO_EXAME ");
                    query.Append(" LEFT JOIN SEG_EXAME E ON (E.ID_EXAME = GFAE.ID_EXAME OR E.ID_EXAME = CFE.ID_EXAME) ");
                    query.Append(" LEFT JOIN SEG_CLIENTE C ON C.ID_CLIENTE = MOV.ID_CLIENTE ");
                    query.Append(" LEFT JOIN SEG_PRODUTO_CONTRATO PC ON PC.ID_PRODUTO_CONTRATO = MOV.ID_PRODUTO_CONTRATO ");
                    query.Append(" LEFT JOIN SEG_PRODUTO P ON P.ID_PRODUTO = PC.ID_PRODUTO ");
                    query.Append(" LEFT JOIN SEG_ASO A ON A.ID_ASO = MOV.ID_ASO ");
                    query.Append(" LEFT JOIN SEG_ESTUDO ES ON ES.ID_ESTUDO = MOV.ID_ESTUDO ");
                    query.Append(" LEFT JOIN SEG_NFE NFE ON NFE.ID_NFE = MOV.ID_NFE ");

                }
                else
                {
                    query.Append(" SELECT SUM(MOV.PRC_UNIT * MOV.QUANTIDADE) AS VALORTOTAL ");
                    query.Append(" FROM SEG_MOVIMENTO MOV ");

                    query.Append(" LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA ON GFAEA.ID_GHE_FONTE_AGENTE_EXAME_ASO = MOV.ID_GHE_FONTE_AGENTE_EXAME_ASO ");
                    query.Append(" LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME GFAE ON GFAE.ID_GHE_FONTE_AGENTE_EXAME = GFAEA.ID_GHE_FONTE_AGENTE_EXAME ");
                    query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA ON CFEA.ID_CLIENTE_FUNCAO_EXAME_ASO = MOV.ID_CLIENTE_FUNCAO_EXAME_ASO ");
                    query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO_EXAME CFE ON CFE.ID_CLIENTE_FUNCAO_EXAME = CFEA.ID_CLIENTE_FUNCAO_EXAME ");
                    query.Append(" LEFT JOIN SEG_EXAME E ON (E.ID_EXAME = GFAE.ID_EXAME OR E.ID_EXAME = CFE.ID_EXAME) ");
                    query.Append(" LEFT JOIN SEG_CLIENTE C ON C.ID_CLIENTE = MOV.ID_CLIENTE ");
                    query.Append(" LEFT JOIN SEG_PRODUTO_CONTRATO PC ON PC.ID_PRODUTO_CONTRATO = MOV.ID_PRODUTO_CONTRATO ");
                    query.Append(" LEFT JOIN SEG_PRODUTO P ON P.ID_PRODUTO = PC.ID_PRODUTO ");
                    query.Append(" LEFT JOIN SEG_ASO A ON A.ID_ASO = MOV.ID_ASO ");
                    query.Append(" LEFT JOIN SEG_ESTUDO ES ON ES.ID_ESTUDO = MOV.ID_ESTUDO ");
                    query.Append(" LEFT JOIN SEG_NFE NFE ON NFE.ID_NFE = MOV.ID_NFE ");
                    query.Append(" WHERE TRUE  ");

                    // testando se o usuário selecionou o cliente

                    if (movimento.Cliente != null)
                        query.Append(" AND MOV.ID_CLIENTE = :VALUE1 ");

                    if (movimento.DataInclusao != null)
                        query.Append(" AND MOV.DT_INCLUSAO BETWEEN :VALUE2 AND :VALUE3 ");

                    if (movimento.DataFaturamento != null)
                        query.Append(" AND MOV.DT_FATURAMENTO BETWEEN :VALUE4 AND :VALUE5 ");

                    if (movimento.Nfe != null)
                        query.Append(" AND NFE.NUMERO_NFE = :VALUE6 ");

                    if (!String.IsNullOrEmpty(movimento.Situacao))
                        query.Append(" AND MOV.SITUACAO = :VALUE7 ");

                }

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = movimento.Cliente != null ? movimento.Cliente.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                command.Parameters[1].Value = movimento.DataInclusao != null ? movimento.DataInclusao : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Timestamp));
                command.Parameters[2].Value = dataFinalLancamento;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Timestamp));
                command.Parameters[3].Value = movimento.DataFaturamento != null ? movimento.DataFaturamento : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Timestamp));
                command.Parameters[4].Value = dataFinalFaturamento;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Integer));
                command.Parameters[5].Value = movimento.Nfe != null ? movimento.Nfe.NumeroNfe : (long?)null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = string.IsNullOrEmpty(movimento.Situacao) ? movimento.Situacao : string.Empty;
                

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    ValorTotal = dr.IsDBNull(0) ? 0 : dr.GetDecimal(0);
                }

                return ValorTotal;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findSomaMovimentoByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void updateMovimentoFaturado(Movimento movimento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateMovimentoFaturado");

            try
            {

                StringBuilder query = new StringBuilder();
                
                query.Append(" UPDATE SEG_MOVIMENTO SET  ");
                query.Append(" ID_NFE = :VALUE2,  ");
                query.Append(" DT_FATURAMENTO = :VALUE3,  ");
                query.Append(" SITUACAO = 'F', ID_EMPRESA = :VALUE4 ");
                query.Append(" WHERE ID_MOVIMENTO = :VALUE1  ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = movimento.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = movimento.Nfe != null ? movimento.Nfe.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Timestamp));
                command.Parameters[2].Value = movimento.Nfe != null ? movimento.Nfe.DataEmissao : movimento.DataFaturamento;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = movimento.Empresa.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateMovimentoFaturado", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public List<Movimento> findMovimentosByNotaFiscal(Nfe notaFiscal, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMovimentosByNotaFiscal");

            try
            {
                StringBuilder query = new StringBuilder();

                List<Movimento> movimentos = new List<Movimento>();

                if (String.Equals(notaFiscal.Situacao, ApplicationConstants.CANCELADA))
                {

                    query.Append(" SELECT I.ID_ITENS_CANCELADOS_NF, I.ID_GHE_FONTE_AGENTE_EXAME_ASO, I.ID_CLIENTE_FUNCAO_EXAME_ASO,  ");
                    query.Append(" I.ID_CLIENTE, I.ID_PRODUTO_CONTRATO, I.ID_NFE, I.DT_INCLUSAO, I.DT_FATURAMENTO, I.SITUACAO,  ");
                    query.Append(" I.PRC_UNIT, I.COM_VALOR, I.ID_ESTUDO, I.ID_ASO, I.QUANTIDADE, I.USUARIO, I.ID_USUARIO, ");
                    query.Append(" E.DESCRICAO, P.NOME, FORMATA_CODIGO_ATENDIMENTO(A.CODIGO_ASO), N.NUMERO_NFE, F.NOME, ");
                    query.Append(" GFAEA.ID_GHE_FONTE_AGENTE_EXAME, E.ID_EXAME, CFE.ID_CLIENTE_FUNCAO_EXAME, P.ID_PRODUTO, P.DESCRICAO, ");
                    query.Append(" CFF.ID_CLIENTE_FUNCAO_FUNCIONARIO, I.PRECO_CUSTO, GFAE.CONFINADO, GFAE.ALTURA, GFAE.ELETRICIDADE, CFF.ID_FUNCIONARIO   ");
                    query.Append(" FROM SEG_ITENS_CANCELADOS_NF I  ");
                    query.Append(" LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA ON GFAEA.ID_GHE_FONTE_AGENTE_EXAME_ASO = I.ID_GHE_FONTE_AGENTE_EXAME_ASO ");
                    query.Append(" LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME GFAE ON GFAE.ID_GHE_FONTE_AGENTE_EXAME = GFAEA.ID_GHE_FONTE_AGENTE_EXAME ");
                    query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA ON CFEA.ID_CLIENTE_FUNCAO_EXAME_ASO = I.ID_CLIENTE_FUNCAO_EXAME_ASO ");
                    query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO_EXAME CFE ON CFE.ID_CLIENTE_FUNCAO_EXAME = CFEA.ID_CLIENTE_FUNCAO_EXAME ");
                    query.Append(" LEFT JOIN SEG_EXAME E ON (E.ID_EXAME = GFAE.ID_EXAME OR E.ID_EXAME = CFE.ID_EXAME) ");
                    query.Append(" LEFT JOIN SEG_CLIENTE C ON C.ID_CLIENTE = I.ID_CLIENTE ");
                    query.Append(" LEFT JOIN SEG_PRODUTO_CONTRATO PC ON PC.ID_PRODUTO_CONTRATO = I.ID_PRODUTO_CONTRATO ");
                    query.Append(" LEFT JOIN SEG_PRODUTO P ON P.ID_PRODUTO = PC.ID_PRODUTO ");
                    query.Append(" LEFT JOIN SEG_ASO A ON A.ID_ASO = I.ID_ASO ");
                    query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CFF ON CFF.ID_CLIENTE_FUNCAO_FUNCIONARIO = A.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                    query.Append(" LEFT JOIN SEG_FUNCIONARIO F ON F.ID_FUNCIONARIO = CFF.ID_FUNCIONARIO ");
                    query.Append(" LEFT JOIN SEG_NFE N ON N.ID_NFE = I.ID_NFE ");
                    query.Append(" LEFT JOIN SEG_ESTUDO ES ON ES.ID_ESTUDO = I.ID_ESTUDO ");
                    query.Append(" WHERE I.ID_NFE = :VALUE1 ");
                }
                else
                {
                    query.Append(" SELECT MOVIMENTO.ID_MOVIMENTO, MOVIMENTO.ID_GHE_FONTE_AGENTE_EXAME_ASO, MOVIMENTO.ID_CLIENTE_FUNCAO_EXAME_ASO,  ");
                    query.Append(" MOVIMENTO.ID_CLIENTE, MOVIMENTO.ID_PRODUTO_CONTRATO, MOVIMENTO.ID_NFE, MOVIMENTO.DT_INCLUSAO, MOVIMENTO.DT_FATURAMENTO, MOVIMENTO.SITUACAO,  ");
                    query.Append(" MOVIMENTO.PRC_UNIT, MOVIMENTO.COM_VALOR, MOVIMENTO.ID_ESTUDO, MOVIMENTO.ID_ASO, MOVIMENTO.QUANTIDADE, MOVIMENTO.USUARIO, MOVIMENTO.ID_USUARIO, ");
                    query.Append(" EXAME.DESCRICAO, PRODUTO.NOME, FORMATA_CODIGO_ATENDIMENTO(ATENDIMENTO.CODIGO_ASO), NFE.NUMERO_NFE, FUNCIONARIO.NOME, ");
                    query.Append(" GFAEA.ID_GHE_FONTE_AGENTE_EXAME, EXAME.ID_EXAME, CFE.ID_CLIENTE_FUNCAO_EXAME, PRODUTO.ID_PRODUTO, PRODUTO.DESCRICAO, ");
                    query.Append(" CFF.ID_CLIENTE_FUNCAO_FUNCIONARIO, MOVIMENTO.PRECO_CUSTO, MOVIMENTO.ID_CLIENTE_UNIDADE, MOVIMENTO.DATA_GRAVACAO, GFAE.CONFINADO, GFAE.ALTURA, MOVIMENTO.ID_CENTROCUSTO, GFAE.ELETRICIDADE, CFF.ID_FUNCIONARIO ");
                    query.Append(" FROM SEG_MOVIMENTO MOVIMENTO  ");
                    query.Append(" LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA ON GFAEA.ID_GHE_FONTE_AGENTE_EXAME_ASO = MOVIMENTO.ID_GHE_FONTE_AGENTE_EXAME_ASO ");
                    query.Append(" LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME GFAE ON GFAE.ID_GHE_FONTE_AGENTE_EXAME = GFAEA.ID_GHE_FONTE_AGENTE_EXAME ");
                    query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA ON CFEA.ID_CLIENTE_FUNCAO_EXAME_ASO = MOVIMENTO.ID_CLIENTE_FUNCAO_EXAME_ASO ");
                    query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO_EXAME CFE ON CFE.ID_CLIENTE_FUNCAO_EXAME = CFEA.ID_CLIENTE_FUNCAO_EXAME ");
                    query.Append(" LEFT JOIN SEG_EXAME EXAME ON (EXAME.ID_EXAME = GFAE.ID_EXAME OR EXAME.ID_EXAME = CFE.ID_EXAME) ");
                    query.Append(" LEFT JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = MOVIMENTO.ID_CLIENTE ");
                    query.Append(" LEFT JOIN SEG_PRODUTO_CONTRATO PRODUTO_CONTRATO ON PRODUTO_CONTRATO.ID_PRODUTO_CONTRATO = MOVIMENTO.ID_PRODUTO_CONTRATO ");
                    query.Append(" LEFT JOIN SEG_PRODUTO PRODUTO ON PRODUTO.ID_PRODUTO = PRODUTO_CONTRATO.ID_PRODUTO ");
                    query.Append(" LEFT JOIN SEG_ASO ATENDIMENTO ON ATENDIMENTO.ID_ASO = MOVIMENTO.ID_ASO ");
                    query.Append(" LEFT JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CFF ON CFF.ID_CLIENTE_FUNCAO_FUNCIONARIO = ATENDIMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                    query.Append(" LEFT JOIN SEG_FUNCIONARIO FUNCIONARIO ON FUNCIONARIO.ID_FUNCIONARIO = CFF.ID_FUNCIONARIO ");
                    query.Append(" LEFT JOIN SEG_NFE NFE ON NFE.ID_NFE = MOVIMENTO.ID_NFE ");
                    query.Append(" LEFT JOIN SEG_ESTUDO ESTUDO ON ESTUDO.ID_ESTUDO = MOVIMENTO.ID_ESTUDO ");
                    query.Append(" WHERE MOVIMENTO.ID_NFE = :VALUE1 ");
                }
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = notaFiscal.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    /* montando movimento para itens não cancelados */

                    if (String.Equals(notaFiscal.Situacao, ApplicationConstants.CANCELADA))
                    {
                        Movimento movimento = new Movimento();
                        movimento.Id = dr.GetInt64(0);

                        if (!dr.IsDBNull(1))
                        {
                            GheFonteAgenteExameAso gheFonteAgenteExameAso = new GheFonteAgenteExameAso();
                            gheFonteAgenteExameAso.Id = dr.GetInt64(1);
                            GheFonteAgenteExame gheFonteAgenteExame = new GheFonteAgenteExame();
                            gheFonteAgenteExame.Id = dr.GetInt64(21);
                            Exame exame = new Exame();
                            exame.Id = dr.GetInt64(22);
                            exame.Descricao = dr.GetString(16);
                            gheFonteAgenteExame.Exame = exame;
                            gheFonteAgenteExameAso.GheFonteAgenteExame = gheFonteAgenteExame;
                            gheFonteAgenteExameAso.Confinado = dr.GetBoolean(28);
                            gheFonteAgenteExameAso.Altura = dr.GetBoolean(29);
                            gheFonteAgenteExameAso.Eletricidade = dr.GetBoolean(30);
                            movimento.GheFonteAgenteExameAso = gheFonteAgenteExameAso;
                        }

                        if (!dr.IsDBNull(2))
                        {
                            ClienteFuncaoExameASo clienteFuncaoExameAso = new ClienteFuncaoExameASo();
                            clienteFuncaoExameAso.Id = dr.GetInt64(2);
                            ClienteFuncaoExame clienteFuncaoExame = new ClienteFuncaoExame();
                            clienteFuncaoExame.Id = dr.GetInt64(23);
                            Exame exame = new Exame();
                            exame.Id = dr.GetInt64(22);
                            exame.Descricao = dr.GetString(16);
                            clienteFuncaoExame.Exame = exame;
                            clienteFuncaoExameAso.ClienteFuncaoExame = clienteFuncaoExame;
                            movimento.ClienteFuncaoExameAso = clienteFuncaoExameAso;
                        }

                        Cliente cliente = new Cliente();
                        cliente.Id = dr.GetInt64(3);
                        movimento.Cliente = cliente;

                        if (!dr.IsDBNull(4))
                        {
                            ContratoProduto contratoProduto = new ContratoProduto();
                            contratoProduto.Id = dr.GetInt64(4);
                            Produto produto = new Produto();
                            produto.Id = dr.GetInt64(24);
                            produto.Nome = dr.GetString(17);
                            contratoProduto.Produto = produto;
                            movimento.ContratoProduto = contratoProduto;
                        }

                        movimento.Nfe = notaFiscal;
                        movimento.DataInclusao = dr.GetDateTime(6);
                        movimento.DataFaturamento = dr.GetDateTime(7);
                        movimento.Situacao = dr.GetString(8);
                        movimento.PrecoUnit = dr.GetDecimal(9);
                        movimento.CustoUnitario = dr.GetDecimal(27);
                        movimento.ValorComissao = dr.IsDBNull(10) ? 0 : dr.GetDecimal(10);

                        if (!dr.IsDBNull(11))
                        {
                            Estudo pcmso = new Estudo();
                            pcmso.Id = dr.GetInt64(11);
                            movimento.Estudo = pcmso;
                        }

                        if (!dr.IsDBNull(12))
                        {
                            Aso atendimento = new Aso();
                            atendimento.Id = dr.GetInt64(12);
                            atendimento.Codigo = dr.GetString(18);
                            ClienteFuncaoFuncionario clienteFuncaoFuncionario = new ClienteFuncaoFuncionario();
                            clienteFuncaoFuncionario.Id = dr.GetInt64(26);
                            Funcionario funcionario = new Funcionario();
                            funcionario.Id = dr.GetInt64(31);
                            funcionario.Nome = dr.GetString(20);
                            clienteFuncaoFuncionario.Funcionario = funcionario;

                            atendimento.ClienteFuncaoFuncionario = clienteFuncaoFuncionario;
                            movimento.Aso = atendimento;
                        }

                        movimento.Quantidade = dr.GetInt32(13);
                        Usuario usuario = new Usuario();
                        usuario.Id = dr.GetInt64(15);
                        usuario.Nome = dr.GetString(14);
                        movimento.Usuario = usuario;

                        movimentos.Add(movimento);



                        //movimentosLancados.Add(new Movimento(dr.GetInt64(0), dr.IsDBNull(1) ? null : new GheFonteAgenteExameAso(dr.GetInt64(1), null, null, new GheFonteAgenteExame(dr.GetInt64(21), new Exame(dr.GetInt64(22), dr.GetString(16)), null, 0, null, dr.GetBoolean(28), dr.GetBoolean(29), dr.GetBoolean(30)), null, false, false, null, false, null, null, null, null, false, null, null, null, false, false, null, null, false, false, null, null, false), dr.IsDBNull(2) ? null : new ClienteFuncaoExameASo(dr.GetInt64(2), null, null, new ClienteFuncaoExame(dr.GetInt64(23), null, new Exame(dr.GetInt64(22), dr.GetString(16)), null, 0), null, false, false, null, false, null, null, null, null, false, null, null, null, false, null, null, null, null), notaFiscal.Cliente, dr.IsDBNull(4) ? null : new ContratoProduto(dr.GetInt64(4), new Produto(dr.GetInt64(24), dr.GetString(25), string.Empty, 0, string.Empty, 0, false, string.Empty), null, null, null, null, null, String.Empty, true), notaFiscal, dr.GetDateTime(6), dr.GetDateTime(7), dr.GetString(8), dr.GetDecimal(9), dr.IsDBNull(10) ? null : (Decimal?)dr.GetDecimal(10), null, dr.IsDBNull(12) ? null : new Aso(dr.GetInt64(12), null, dr.IsDBNull(26) ? null : new ClienteFuncaoFuncionario(dr.GetInt64(26), null, new Funcionario(dr.GetString(20), string.Empty, DateTime.Now, String.Empty, String.Empty, String.Empty), null, null, null, string.Empty, false, string.Empty, string.Empty, false, true), dr.GetString(18), DateTime.Now, null, null, null, String.Empty, null, null, String.Empty, null, null, null, null, null, null, String.Empty, String.Empty, String.Empty, null, null, string.Empty, null, null, null, string.Empty, false, string.Empty, string.Empty, string.Empty), dr.GetInt32(13), new Usuario(dr.GetInt64(15), dr.GetString(14), string.Empty, string.Empty, string.Empty, null, String.Empty, String.Empty, String.Empty, String.Empty, null, null, String.Empty, false, null, String.Empty, false, false, string.Empty, string.Empty), null, dr.GetDecimal(27), null, null, DateTime.Now, null));
                    }

                        
                    else
                    {
                        Movimento movimento = new Movimento();
                        movimento.Id = dr.GetInt64(0);

                        if (!dr.IsDBNull(1))
                        {
                            GheFonteAgenteExameAso gheFonteAgenteExameAso = new GheFonteAgenteExameAso();
                            gheFonteAgenteExameAso.Id = dr.GetInt64(1);
                            GheFonteAgenteExame gheFonteAgenteExame = new GheFonteAgenteExame();
                            gheFonteAgenteExame.Id = dr.GetInt64(21);
                            Exame exame = new Exame();
                            exame.Id = dr.GetInt64(22);
                            exame.Descricao = dr.GetString(16);
                            gheFonteAgenteExame.Exame = exame;
                            gheFonteAgenteExameAso.GheFonteAgenteExame = gheFonteAgenteExame;
                            gheFonteAgenteExameAso.Confinado = dr.GetBoolean(30);
                            gheFonteAgenteExameAso.Altura = dr.GetBoolean(31);
                            gheFonteAgenteExameAso.Eletricidade = dr.GetBoolean(33);
                            movimento.GheFonteAgenteExameAso = gheFonteAgenteExameAso;
                        }

                        if (!dr.IsDBNull(2))
                        {
                            ClienteFuncaoExameASo clienteFuncaoExameAso = new ClienteFuncaoExameASo();
                            clienteFuncaoExameAso.Id = dr.GetInt64(2);
                            ClienteFuncaoExame clienteFuncaoExame = new ClienteFuncaoExame();
                            clienteFuncaoExame.Id = dr.GetInt64(23);
                            Exame exame = new Exame();
                            exame.Id = dr.GetInt64(22);
                            exame.Descricao = dr.GetString(16);
                            clienteFuncaoExame.Exame = exame;
                            clienteFuncaoExameAso.ClienteFuncaoExame = clienteFuncaoExame;
                            movimento.ClienteFuncaoExameAso = clienteFuncaoExameAso;
                        }

                        Cliente cliente = new Cliente();
                        cliente.Id = dr.GetInt64(3);
                        movimento.Cliente = cliente;

                        if (!dr.IsDBNull(4))
                        {
                            ContratoProduto contratoProduto = new ContratoProduto();
                            contratoProduto.Id = dr.GetInt64(4);
                            Produto produto = new Produto();
                            produto.Id = dr.GetInt64(24);
                            produto.Nome = dr.GetString(17);
                            contratoProduto.Produto = produto;
                            movimento.ContratoProduto = contratoProduto;
                        }

                        movimento.Nfe = notaFiscal;
                        movimento.DataInclusao = dr.GetDateTime(6);
                        movimento.DataFaturamento = dr.GetDateTime(7);
                        movimento.DataGravacao = dr.GetDateTime(29);
                        movimento.Situacao = dr.GetString(8);
                        movimento.PrecoUnit = dr.GetDecimal(9);
                        movimento.CustoUnitario = dr.GetDecimal(27);
                        movimento.ValorComissao = dr.IsDBNull(10) ? 0 : dr.GetDecimal(10);

                        if (!dr.IsDBNull(11))
                        {
                            Estudo pcmso = new Estudo();
                            pcmso.Id = dr.GetInt64(11);
                            movimento.Estudo = pcmso;
                        }

                        if (!dr.IsDBNull(12))
                        {
                            Aso atendimento = new Aso();
                            atendimento.Id = dr.GetInt64(12);
                            atendimento.Codigo = dr.GetString(18);
                            ClienteFuncaoFuncionario clienteFuncaoFuncionario = new ClienteFuncaoFuncionario();
                            clienteFuncaoFuncionario.Id = dr.GetInt64(26);
                            Funcionario funcionario = new Funcionario();
                            funcionario.Id = dr.GetInt64(34);
                            funcionario.Nome = dr.GetString(20);
                            clienteFuncaoFuncionario.Funcionario = funcionario;

                            atendimento.ClienteFuncaoFuncionario = clienteFuncaoFuncionario;
                            movimento.Aso = atendimento;
                        }
                        
                        movimento.Quantidade = dr.GetInt32(13);
                        Usuario usuario = new Usuario();
                        usuario.Id = dr.GetInt64(15);
                        usuario.Nome = dr.GetString(14);
                        movimento.Usuario = usuario;

                        if (!dr.IsDBNull(28))
                        {
                            Cliente unidade = new Cliente();
                            unidade.Id = dr.GetInt64(28);
                            movimento.Unidade = unidade;
                        }

                        if (!dr.IsDBNull(32))
                        {
                            CentroCusto centroCusto = new CentroCusto();
                            centroCusto.Id = dr.GetInt64(32);
                            movimento.CentroCusto = centroCusto;
                        }

                        movimentos.Add(movimento);
                    }

                    //movimentosLancados.Add(new Movimento(dr.GetInt64(0), dr.IsDBNull(1) ? null : new GheFonteAgenteExameAso(dr.GetInt64(1), null, null, new GheFonteAgenteExame(dr.GetInt64(21), new Exame(dr.GetInt64(22), dr.GetString(16)), null, 0, null, dr.GetBoolean(30), dr.GetBoolean(31), dr.GetBoolean(33)), null, false, false, null, false, null, null, null, null, false, null, null, null, false, false, null, null, false, false, null, null, false), dr.IsDBNull(2) ? null : new ClienteFuncaoExameASo(dr.GetInt64(2), null, null, new ClienteFuncaoExame(dr.GetInt64(23), null, new Exame(dr.GetInt64(22), dr.GetString(16)), null, 0), null, false, false, null, false, null, null, null, null, false, null, null, null, false, null, null, null, null), notaFiscal.Cliente, dr.IsDBNull(4) ? null : new ContratoProduto(dr.GetInt64(4), new Produto(dr.GetInt64(24), dr.GetString(25), string.Empty, 0, string.Empty, 0, false, string.Empty), null, null, null, null, null, String.Empty, true), notaFiscal, dr.GetDateTime(6), dr.GetDateTime(7), dr.GetString(8), dr.GetDecimal(9), dr.IsDBNull(10) ? null : (Decimal?)dr.GetDecimal(10), null, dr.IsDBNull(12) ? null : new Aso(dr.GetInt64(12), null, dr.IsDBNull(26) ? null : new ClienteFuncaoFuncionario(dr.GetInt64(26), null, new Funcionario(dr.GetString(20), string.Empty, DateTime.Now, String.Empty, String.Empty, String.Empty), null, null, null, string.Empty, false, string.Empty, string.Empty, false, true), dr.GetString(18), DateTime.Now, null, null, null, String.Empty, null, null, String.Empty, null, null, null, null, null, null, String.Empty, String.Empty, String.Empty, null, null, string.Empty, null, null, null, string.Empty, false, string.Empty, string.Empty, string.Empty), dr.GetInt32(13), new Usuario(dr.GetInt64(15), dr.GetString(14), string.Empty, string.Empty, string.Empty, null, String.Empty, String.Empty, String.Empty, String.Empty, null, null, String.Empty, false, null, String.Empty, false, false, string.Empty, string.Empty), null, dr.GetDecimal(27), dr.IsDBNull(28) ? null : new Cliente(dr.GetInt64(28)), null, dr.GetDateTime(29), dr.IsDBNull(32) ? null : new CentroCusto(dr.GetInt64(32), null, string.Empty, string.Empty))); 
                }

                return movimentos;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMovimentosByNotaFiscal", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findMovimentoByNotaInRelatorioNota(Nfe nfe, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMovimentoByNotaInRelatorioNota");

            try
            {

                StringBuilder query = new StringBuilder();

                if (!String.Equals(nfe.Situacao, ApplicationConstants.CANCELADA))
                {
                    query.Append(" SELECT A.ITEM, SUM(A.QUANTIDADE) AS QUANTIDADE, SUM(A.PRECO) AS PRECO, SUM(A.PRECO_TOTAL) AS PRECO_TOTAL ");
                    query.Append(" FROM (  ");
                    query.Append(" SELECT E.DESCRICAO AS ITEM, SUM(M.QUANTIDADE) AS QUANTIDADE, M.PRC_UNIT AS PRECO, SUM(M.QUANTIDADE) * M.PRC_UNIT AS PRECO_TOTAL  ");
                    query.Append(" FROM SEG_MOVIMENTO M ");
                    query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA ON GFAEA.ID_GHE_FONTE_AGENTE_EXAME_ASO = M.ID_GHE_FONTE_AGENTE_EXAME_ASO  ");
                    query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GFAE ON GF)E.ID_GHE_FONTE_AGENTE_EXAME = GFAEA.ID_GHE_FONTE_AGENTE_EXAME  ");
                    query.Append(" JOIN SEG_EXAME E ON E.ID_EXAME = GFAE.ID_EXAME  ");
                    query.Append(" WHERE M.ID_NFE = :VALUE1  ");
                    query.Append(" GROUP BY E.DESCRICAO, M.QUANTIDADE,M.PRC_UNIT  ");
                    query.Append(" UNION ALL ( ");
                    query.Append(" SELECT E.DESCRICAO AS ITEM, SUM(M.QUANTIDADE) AS QUANTIDADE, M.PRC_UNIT AS PRECO, SUM(M.QUANTIDADE) * M.PRC_UNIT AS PRECO_TOTAL  ");
                    query.Append(" FROM SEG_MOVIMENTO M  ");
                    query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA ON CFEA.ID_CLIENTE_FUNCAO_EXAME_ASO = M.ID_CLIENTE_FUNCAO_EXAME_ASO  ");
                    query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME CFE ON CFE.ID_CLIENTE_FUNCAO_EXAME = CFEA.ID_CLIENTE_FUNCAO_EXAME  ");
                    query.Append(" JOIN SEG_EXAME E ON E.ID_EXAME = CFE.ID_EXAME  ");
                    query.Append(" WHERE M.ID_NFE = :VALUE1  ");
                    query.Append(" GROUP BY E.DESCRICAO, M.QUANTIDADE,M.PRC_UNIT)  ");
                    query.Append(" UNION ALL (  ");
                    query.Append(" SELECT P.DESCRICAO AS ITEM, SUM(M.QUANTIDADE) AS QUANTIDADE, M.PRC_UNIT AS PRECO, SUM(M.QUANTIDADE) * M.PRC_UNIT AS PRECO_TOTAL  ");
                    query.Append(" FROM SEG_MOVIMENTO M  ");
                    query.Append(" JOIN SEG_PRODUTO_CONTRATO PC ON PC.ID_PRODUTO_CONTRATO = M.ID_PRODUTO_CONTRATO  ");
                    query.Append(" JOIN SEG_PRODUTO P ON P.ID_PRODUTO = PC.ID_PRODUTO  ");
                    query.Append(" WHERE M.ID_NFE = :VALUE1  ");
                    query.Append(" GROUP BY P.DESCRICAO, M.QUANTIDADE,M.PRC_UNIT)  ");
                    query.Append(" ) AS A  ");
                    query.Append(" GROUP BY A.ITEM  ");
                    query.Append(" ORDER BY A.ITEM ASC ");
                }
                else
                {
                    query.Append(" SELECT A.ITEM, SUM(A.QUANTIDADE) AS QUANTIDADE, SUM(A.PRECO) AS PRECO, SUM(A.PRECO_TOTAL) AS PRECO_TOTAL ");
                    query.Append(" FROM (  ");
                    query.Append(" SELECT E.DESCRICAO AS ITEM, SUM(M.QUANTIDADE) AS QUANTIDADE, M.PRC_UNIT AS PRECO, SUM(M.QUANTIDADE) * M.PRC_UNIT AS PRECO_TOTAL  ");
                    query.Append(" FROM SEG_ITENS_CANCELADOS_NF M ");
                    query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA ON GFAEA.ID_GHE_FONTE_AGENTE_EXAME_ASO = M.ID_GHE_FONTE_AGENTE_EXAME_ASO  ");
                    query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GFAE ON GFAE.ID_GHE_FONTE_AGENTE_EXAME = GFAEA.ID_GHE_FONTE_AGENTE_EXAME  ");
                    query.Append(" JOIN SEG_EXAME E ON E.ID_EXAME = GFAE.ID_EXAME  ");
                    query.Append(" WHERE M.ID_NFE = :VALUE1  ");
                    query.Append(" GROUP BY E.DESCRICAO, M.QUANTIDADE,M.PRC_UNIT  ");
                    query.Append(" UNION ALL ( ");
                    query.Append(" SELECT E.DESCRICAO AS ITEM, SUM(M.QUANTIDADE) AS QUANTIDADE, M.PRC_UNIT AS PRECO, SUM(M.QUANTIDADE) * M.PRC_UNIT AS PRECO_TOTAL  ");
                    query.Append(" FROM SEG_ITENS_CANCELADOS_NF M  ");
                    query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA ON CFEA.ID_CLIENTE_FUNCAO_EXAME_ASO = M.ID_CLIENTE_FUNCAO_EXAME_ASO  ");
                    query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME CFE ON CFE.ID_CLIENTE_FUNCAO_EXAME = CFEA.ID_CLIENTE_FUNCAO_EXAME  ");
                    query.Append(" JOIN SEG_EXAME E ON E.ID_EXAME = CFE.ID_EXAME  ");
                    query.Append(" WHERE M.ID_NFE = :VALUE1  ");
                    query.Append(" GROUP BY E.DESCRICAO, M.QUANTIDADE,M.PRC_UNIT)  ");
                    query.Append(" UNION ALL (  ");
                    query.Append(" SELECT P.DESCRICAO AS ITEM, SUM(M.QUANTIDADE) AS QUANTIDADE, M.PRC_UNIT AS PRECO, SUM(M.QUANTIDADE) * M.PRC_UNIT AS PRECO_TOTAL  ");
                    query.Append(" FROM SEG_ITENS_CANCELADOS_NF M  ");
                    query.Append(" JOIN SEG_PRODUTO_CONTRATO PC ON PC.ID_PRODUTO_CONTRATO = M.ID_PRODUTO_CONTRATO  ");
                    query.Append(" JOIN SEG_PRODUTO P ON P.ID_PRODUTO = PC.ID_PRODUTO  ");
                    query.Append(" WHERE M.ID_NFE = :VALUE1  ");
                    query.Append(" GROUP BY P.DESCRICAO, M.QUANTIDADE,M.PRC_UNIT)  ");
                    query.Append(" ) AS A  ");
                    query.Append(" GROUP BY A.ITEM  ");
                    query.Append(" ORDER BY A.ITEM ASC ");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = nfe.Id;

                DataSet ds = new DataSet();
                
                adapter.Fill(ds, "Movimentos");

                return ds;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMovimentoByNotaInRelatorioNota", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void estornaFaturamentoMovimento(Movimento movimento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método estornaFaturamentoMovimento");

            try
            {

                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_MOVIMENTO SET  ");
                query.Append(" ID_NFE = NULL,  ");
                query.Append(" DT_FATURAMENTO = NULL, SITUACAO = 'P' ");
                query.Append(" WHERE ID_MOVIMENTO = :VALUE1  ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = movimento.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateMovimentoFaturado", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public DataSet findMovimentoInRecibo(Cliente cliente, DateTime dataInicial, DateTime dataFinal, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMovimentoInRecibo");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT A.ITEM, SUM(A.QUANTIDADE) AS QUANTIDADE, SUM(A.PRECO) AS PRECO, SUM(A.PRECO_TOTAL) AS PRECO_TOTAL  ");
                query.Append(" FROM (   ");
                query.Append(" SELECT E.DESCRICAO AS ITEM, SUM(M.QUANTIDADE) AS QUANTIDADE, M.PRC_UNIT AS PRECO, SUM(M.QUANTIDADE) * M.PRC_UNIT AS PRECO_TOTAL  ");
                query.Append(" FROM SEG_MOVIMENTO M  ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA ON GFAEA.ID_GHE_FONTE_AGENTE_EXAME_ASO = M.ID_GHE_FONTE_AGENTE_EXAME_ASO   ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GFAE ON GFAE.ID_GHE_FONTE_AGENTE_EXAME = GFAEA.ID_GHE_FONTE_AGENTE_EXAME   ");
                query.Append(" JOIN SEG_EXAME E ON E.ID_EXAME = GFAE.ID_EXAME   ");
                query.Append(" WHERE M.ID_CLIENTE = :VALUE1 AND M.DT_FATURAMENTO BETWEEN :VALUE2 AND :VALUE3 AND M.SITUACAO = 'F' AND M.ID_NFE IS NULL  ");
                query.Append(" GROUP BY E.DESCRICAO, M.QUANTIDADE,M.PRC_UNIT   ");
                query.Append(" UNION ALL (  ");
                query.Append(" SELECT E.DESCRICAO AS ITEM, SUM(M.QUANTIDADE) AS QUANTIDADE, M.PRC_UNIT AS PRECO, SUM(M.QUANTIDADE) * M.PRC_UNIT AS PRECO_TOTAL   ");
                query.Append(" FROM SEG_MOVIMENTO M   ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA ON CFEA.ID_CLIENTE_FUNCAO_EXAME_ASO = M.ID_CLIENTE_FUNCAO_EXAME_ASO   ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME CFE ON CFE.ID_CLIENTE_FUNCAO_EXAME = CFEA.ID_CLIENTE_FUNCAO_EXAME  ");
                query.Append(" JOIN SEG_EXAME E ON E.ID_EXAME = CFE.ID_EXAME   ");
                query.Append(" WHERE M.ID_CLIENTE = :VALUE1 AND M.DT_FATURAMENTO BETWEEN :VALUE2 AND :VALUE3 AND M.SITUACAO = 'F' AND M.ID_NFE IS NULL");
                query.Append(" GROUP BY E.DESCRICAO, M.QUANTIDADE,M.PRC_UNIT)   ");
                query.Append(" UNION ALL (   ");
                query.Append(" SELECT P.DESCRICAO AS ITEM, SUM(M.QUANTIDADE) AS QUANTIDADE, M.PRC_UNIT AS PRECO, SUM(M.QUANTIDADE) * M.PRC_UNIT AS PRECO_TOTAL   ");
                query.Append(" FROM SEG_MOVIMENTO M   ");
                query.Append(" JOIN SEG_PRODUTO_CONTRATO PC ON PC.ID_PRODUTO_CONTRATO = M.ID_PRODUTO_CONTRATO   ");
                query.Append(" JOIN SEG_PRODUTO P ON P.ID_PRODUTO = PC.ID_PRODUTO   ");
                query.Append(" WHERE M.ID_CLIENTE = :VALUE1 AND M.DT_FATURAMENTO BETWEEN :VALUE2 AND :VALUE3 AND M.SITUACAO = 'F' AND M.ID_NFE IS NULL ");
                query.Append(" GROUP BY P.DESCRICAO, M.QUANTIDADE,M.PRC_UNIT)   ");
                query.Append(" ) AS A   ");
                query.Append(" GROUP BY A.ITEM   ");
                query.Append(" ORDER BY A.ITEM ASC  ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = cliente.Id;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[1].Value = dataInicial;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[2].Value = dataFinal;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Movimentos");

                return ds;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMovimentoInRecibo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findColaboradorByMovimentoInRecibo(Cliente cliente, DateTime dataInicial, DateTime dataFinal, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findColaboradorByMovimentoInRecibo");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT DISTINCT A.NOME_FUNCIONARIO, A.RG_FUNCIONARIO  ");
                query.Append(" FROM SEG_MOVIMENTO M   ");
                query.Append(" JOIN SEG_ASO A ON M.ID_ASO = A.ID_ASO AND M.ID_CLIENTE = :VALUE1 AND M.DT_FATURAMENTO BETWEEN :VALUE2 AND :VALUE3 AND M.SITUACAO = 'F' AND M.ID_NFE IS NULL ");
                query.Append(" ORDER BY A.NOME_FUNCIONARIO ASC  ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = cliente.Id;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[1].Value = dataInicial;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[2].Value = dataFinal;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Movimentos");

                return ds;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMovimentoInRecibo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet somaMovimentoInternoByClienteInRelatorio(Cliente cliente, DateTime dataInicial, DateTime dataFinal, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método somaMovimentoInternoByClienteInRelatorio");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT SUM(A.PRECO_TOTAL) AS PRECO_TOTAL  ");
                query.Append(" FROM (   ");
                query.Append(" SELECT SUM(M.QUANTIDADE * M.PRC_UNIT ) AS PRECO_TOTAL  ");
                query.Append(" FROM SEG_MOVIMENTO M  ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA ON GFAEA.ID_GHE_FONTE_AGENTE_EXAME_ASO = M.ID_GHE_FONTE_AGENTE_EXAME_ASO   ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GFAE ON GFAE.ID_GHE_FONTE_AGENTE_EXAME = GFAEA.ID_GHE_FONTE_AGENTE_EXAME   ");
                query.Append(" JOIN SEG_EXAME E ON E.ID_EXAME = GFAE.ID_EXAME   ");
                query.Append(" WHERE M.ID_CLIENTE = :VALUE1 AND M.DT_FATURAMENTO BETWEEN :VALUE2 AND :VALUE3 AND M.SITUACAO = 'F' AND M.ID_NFE IS NULL  ");
                query.Append(" UNION ALL (  ");
                query.Append(" SELECT SUM(M.QUANTIDADE * M.PRC_UNIT ) AS PRECO_TOTAL   ");
                query.Append(" FROM SEG_MOVIMENTO M   ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA ON CFEA.ID_CLIENTE_FUNCAO_EXAME_ASO = M.ID_CLIENTE_FUNCAO_EXAME_ASO   ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME CFE ON CFE.ID_CLIENTE_FUNCAO_EXAME = CFEA.ID_CLIENTE_FUNCAO_EXAME  ");
                query.Append(" JOIN SEG_EXAME E ON E.ID_EXAME = CFE.ID_EXAME   ");
                query.Append(" WHERE M.ID_CLIENTE = :VALUE1 AND M.DT_FATURAMENTO BETWEEN :VALUE2 AND :VALUE3 AND M.SITUACAO = 'F' AND M.ID_NFE IS NULL   ");
                query.Append(" )   ");
                query.Append(" UNION ALL (   ");
                query.Append(" SELECT SUM(M.QUANTIDADE * M.PRC_UNIT ) AS PRECO_TOTAL   ");
                query.Append(" FROM SEG_MOVIMENTO M   ");
                query.Append(" JOIN SEG_PRODUTO_CONTRATO PC ON PC.ID_PRODUTO_CONTRATO = M.ID_PRODUTO_CONTRATO   ");
                query.Append(" JOIN SEG_PRODUTO P ON P.ID_PRODUTO = PC.ID_PRODUTO   ");
                query.Append(" WHERE M.ID_CLIENTE = :VALUE1 AND M.DT_FATURAMENTO BETWEEN :VALUE2 AND :VALUE3 AND M.SITUACAO = 'F' AND M.ID_NFE IS NULL    ");
                query.Append(" )   ");
                query.Append(" ) AS A   ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = cliente.Id;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[1].Value = dataInicial;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[2].Value = dataFinal;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "totalMovimento");

                return ds;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMovimentoInRecibo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void updateValorMovimento(Movimento movimento, Decimal valorAlterado, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateMovimentoFaturado");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_MOVIMENTO SET  ");
                query.Append(" PRC_UNIT = :VALUE2  ");
                query.Append(" WHERE ID_MOVIMENTO = :VALUE1  ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = movimento.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Numeric));
                command.Parameters[1].Value = valorAlterado;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateMovimentoFaturado", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public Boolean findMovimentoByGheFonteAgenteExameAso(GheFonteAgenteExameAso gheFonteAgenteExameAso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMovimentoByGheFonteAgenteExameAso");

            try
            {
                StringBuilder query = new StringBuilder();

                Boolean retorno = false;

                query.Append(" SELECT * ");
                query.Append(" FROM SEG_MOVIMENTO M   ");
                query.Append(" WHERE M.ID_GHE_FONTE_AGENTE_EXAME_ASO = :VALUE1 AND M.SITUACAO = 'P' ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonteAgenteExameAso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    retorno = true;
                }

                return retorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMovimentoByGheFonteAgenteExameAso", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean findMovimentoByClienteFuncaoExameAso(ClienteFuncaoExameASo clienteFuncaoExameAso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMovimentoByClienteFuncaoExameAso");

            try
            {
                StringBuilder query = new StringBuilder();

                Boolean retorno = false;

                query.Append(" SELECT * ");
                query.Append(" FROM SEG_MOVIMENTO M   ");
                query.Append(" WHERE M.ID_CLIENTE_FUNCAO_EXAME_ASO = :VALUE1 AND M.SITUACAO = 'P' ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncaoExameAso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    retorno = true;
                }

                return retorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMovimentoByClienteFuncaoExameAso", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public LinkedList<Movimento> findByProdutoInDataMovimento(Produto produto, DateTime dataLancamentoInicial, Cliente cliente, Boolean insertedInProtocolo,  DateTime dataLancamentoFinal, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByProdutoInDataMovimento");

            try
            {
                StringBuilder query = new StringBuilder();

                LinkedList<Movimento> movimentos = new LinkedList<Movimento>();

                query.Append(" SELECT MOVIMENTO.ID_MOVIMENTO, MOVIMENTO.ID_EMPRESA, MOVIMENTO.ID_GHE_FONTE_AGENTE_EXAME_ASO, ");
                query.Append(" MOVIMENTO.ID_CLIENTE_FUNCAO_EXAME_ASO, MOVIMENTO.ID_CLIENTE, MOVIMENTO.ID_PRODUTO_CONTRATO, ");
                query.Append(" MOVIMENTO.ID_NFE, MOVIMENTO.DT_INCLUSAO, MOVIMENTO.DT_FATURAMENTO, MOVIMENTO.SITUACAO, MOVIMENTO.PRC_UNIT, ");
                query.Append(" MOVIMENTO.COM_VALOR, MOVIMENTO.ID_ESTUDO, MOVIMENTO.ID_ASO, MOVIMENTO.QUANTIDADE, MOVIMENTO.USUARIO, ");
                query.Append(" MOVIMENTO.ID_USUARIO, MOVIMENTO.PRECO_CUSTO, MOVIMENTO.ID_CLIENTE_UNIDADE, MOVIMENTO.ID_PROTOCOLO, MOVIMENTO.DATA_GRAVACAO, MOVIMENTO.ID_CENTROCUSTO, MOVIMENTO.ID_CONTRATO_EXAME, MOVIMENTO.ID_CLIENTE_CREDENCIADA ");
                query.Append(" FROM  ");
                query.Append(" SEG_MOVIMENTO MOVIMENTO ");
                query.Append(" JOIN SEG_PRODUTO_CONTRATO PC ON PC.ID_PRODUTO_CONTRATO = MOVIMENTO.ID_PRODUTO_CONTRATO ");
                query.Append(" JOIN SEG_PRODUTO P ON P.ID_PRODUTO = PC.ID_PRODUTO " );
                query.Append(" WHERE P.ID_PRODUTO = :VALUE1 AND MOVIMENTO.DT_INCLUSAO BETWEEN :VALUE2 AND :VALUE3 ");

                if (cliente != null)
                    query.Append(" AND MOVIMENTO.ID_CLIENTE = :value4 ");

                if (insertedInProtocolo)
                    query.Append(" AND MOVIMENTO.ID_PROTOCOLO IS NULL ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = produto.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                command.Parameters[1].Value = dataLancamentoInicial;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Timestamp));
                command.Parameters[2].Value = dataLancamentoFinal;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = cliente != null ? cliente.Id : null;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    Movimento movimento = new Movimento( dr.GetInt64(0), dr.IsDBNull(2) ? null : new GheFonteAgenteExameAso(dr.GetInt64(2)), dr.IsDBNull(3) ? null : new ClienteFuncaoExameASo(dr.GetInt64(3)), dr.IsDBNull(4) ? null : new Cliente(dr.GetInt64(4)), dr.IsDBNull(5) ? null : new ContratoProduto((Int64?)dr.GetInt64(5),null, null, null, null, null, null, String.Empty, true), dr.IsDBNull(6) ? null : new Nfe(dr.GetInt64(6)), dr.GetDateTime(7), dr.IsDBNull(8) ? null : (DateTime?)dr.GetDateTime(8), dr.GetString(9), dr.GetDecimal(10), dr.IsDBNull(11) ? null : (Decimal?)dr.GetDecimal(11),  dr.IsDBNull(12) ? null : new Estudo((Int64?)dr.GetInt64(12)), dr.IsDBNull(13) ? null : new Aso(dr.GetInt64(13)), dr.GetInt32(14), dr.IsDBNull(16) ? null : new Usuario(dr.GetInt64(16)), dr.IsDBNull(1) ? null : new Empresa(dr.GetInt64(1)), dr.IsDBNull(17) ? null : (Decimal?)dr.GetDecimal(17), dr.IsDBNull(18) ? null : new Cliente(dr.GetInt64(18)), dr.IsDBNull(19) ? null : new Protocolo(dr.GetInt64(19)), dr.GetDateTime(20), dr.IsDBNull(21) ? null : new CentroCusto(dr.GetInt64(21), null, string.Empty, string.Empty), null, dr.IsDBNull(23) ? null : new Cliente(dr.GetInt64(23)));
                    movimento.ContratoExame = dr.IsDBNull(22) ? null : new ContratoExame(dr.GetInt64(22));
                    movimentos.AddLast(movimento);                         
                }

                return movimentos;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByProdutoInDataMovimento", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Movimento findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            Movimento movimento = new Movimento();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT MOVIMENTO.ID_MOVIMENTO, MOVIMENTO.ID_EMPRESA, MOVIMENTO.ID_GHE_FONTE_AGENTE_EXAME_ASO,  ");
                query.Append(" MOVIMENTO.ID_CLIENTE_FUNCAO_EXAME_ASO, MOVIMENTO.ID_CLIENTE, MOVIMENTO.ID_PRODUTO_CONTRATO, MOVIMENTO.ID_NFE,  ");
                query.Append(" MOVIMENTO.DT_INCLUSAO, MOVIMENTO.DT_FATURAMENTO, MOVIMENTO.SITUACAO, MOVIMENTO.PRC_UNIT, MOVIMENTO.COM_VALOR, ");
                query.Append(" MOVIMENTO.ID_ESTUDO, MOVIMENTO.ID_ASO, MOVIMENTO.QUANTIDADE, MOVIMENTO.USUARIO, MOVIMENTO.ID_USUARIO,  ");
                query.Append(" MOVIMENTO.PRECO_CUSTO, MOVIMENTO.ID_CLIENTE_UNIDADE, MOVIMENTO.ID_PROTOCOLO, MOVIMENTO.DATA_GRAVACAO, MOVIMENTO.ID_CENTROCUSTO ");
                query.Append(" FROM SEG_MOVIMENTO MOVIMENTO  ");
                query.Append(" WHERE MOVIMENTO.ID_MOVIMENTO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    movimento.Id = dr.GetInt64(0);

                    Empresa empresa = new Empresa();
                    empresa.Id = dr.GetInt64(1);
                    movimento.Empresa = empresa;

                    if (!dr.IsDBNull(2))
                    {
                        GheFonteAgenteExameAso gheFonteAgenteExameAso = new GheFonteAgenteExameAso();
                        gheFonteAgenteExameAso.Id = dr.GetInt64(2);
                        movimento.GheFonteAgenteExameAso = gheFonteAgenteExameAso;
                    }

                    if (!dr.IsDBNull(3))
                    {
                        ClienteFuncaoExameASo clienteFuncaoExameAso = new ClienteFuncaoExameASo();
                        clienteFuncaoExameAso.Id = dr.GetInt64(3);
                        movimento.ClienteFuncaoExameAso = clienteFuncaoExameAso;
                    }

                    Cliente cliente = new Cliente();
                    cliente.Id = dr.GetInt64(4);
                    movimento.Cliente = cliente;

                    if (!dr.IsDBNull(5))
                    {
                        ContratoProduto contratoProduto = new ContratoProduto();
                        contratoProduto.Id = dr.GetInt64(5);
                        movimento.ContratoProduto = contratoProduto;
                    }

                    if (!dr.IsDBNull(6))
                    {
                        Nfe notaFiscal = new Nfe();
                        notaFiscal.Id = dr.GetInt64(6);
                        movimento.Nfe = notaFiscal;
                    }

                    movimento.DataInclusao = dr.GetDateTime(7);
                    movimento.DataFaturamento = dr.IsDBNull(8) ? (DateTime?)null : dr.GetDateTime(8);
                    movimento.Situacao = dr.GetString(9);
                    movimento.PrecoUnit = dr.GetDecimal(10);
                    movimento.ValorComissao = dr.IsDBNull(11) ? 0 : dr.GetDecimal(11);

                    if (!dr.IsDBNull(12))
                    {
                        Estudo pcmso = new Estudo();
                        pcmso.Id = dr.GetInt64(12);
                        movimento.Estudo = pcmso;
                    }

                    if (!dr.IsDBNull(13))
                    {
                        Aso atendimento = new Aso();
                        atendimento.Id = dr.GetInt64(13);
                        movimento.Aso = atendimento;
                    }

                    movimento.Quantidade = dr.GetInt32(14);

                    Usuario usuario = new Usuario();
                    usuario.Id = dr.GetInt64(16);
                    usuario.Nome = dr.GetString(15);
                    movimento.Usuario = usuario;

                    movimento.CustoUnitario = dr.GetDecimal(17);

                    if (!dr.IsDBNull(18))
                    {
                        Cliente unidade = new Cliente();
                        unidade.Id = dr.GetInt64(18);
                        movimento.Unidade = unidade;
                    }

                    if (!dr.IsDBNull(19))
                    {
                        Protocolo protocolo = new Protocolo();
                        protocolo.Id = dr.GetInt64(19);
                        movimento.Protocolo = protocolo;
                    }

                    movimento.DataGravacao = dr.GetDateTime(20);

                    if (!dr.IsDBNull(21))
                    {
                        CentroCusto centroCusto = new CentroCusto();
                        centroCusto.Id = dr.GetInt64(21);
                        movimento.CentroCusto = centroCusto;
                    }

                }

                #region contratoProduto

                if (movimento.ContratoProduto != null)
                {
                    query.Length = 0;

                    query.Append(" SELECT PRODUTO_CONTRATO.ID_PRODUTO_CONTRATO, PRODUTO_CONTRATO.ID_PRODUTO, PRODUTO_CONTRATO.ID_CONTRATO, PRODUTO_CONTRATO.PRECO_CONTRATADO, PRODUTO_CONTRATO.CUSTO_CONTRATO, PRODUTO_CONTRATO.DATA_INCLUSAO, ");
                    query.Append(" PRODUTO.ID_PRODUTO, PRODUTO.NOME, PRODUTO.SITUACAO, PRODUTO.PRECO, PRODUTO.TIPO, PRODUTO.CUSTO, PRODUTO.TIPO_ESTUDO,");
                    query.Append(" CONTRATO.ID_CONTRATO, CONTRATO.ID_CLIENTE, CONTRATO.CODIGO_CONTRATO, CONTRATO.DATA_ELABORACAO, CONTRATO.SITUACAO, CONTRATO.OBSERVACAO, CONTRATO.ID_USUARIO_CRIOU, CONTRATO.ID_USUARIO_FINALIZOU, CONTRATO.ID_USUARIO_CANCELOU, CONTRATO.MOTIVO_CANCELAMENTO, CONTRATO.ID_CLIENTE_PRESTADOR, CONTRATO.CONTRATO_AUTOMATICO, CONTRATO.DATA_CANCELAMENTO, CONTRATO.ID_CONTRATO_PAI, CONTRATO.BLOQUEIA_INADIMPLENCIA, CONTRATO.GERA_MENSALIDADE, CONTRATO.GERA_NUMERO_VIDAS, CONTRATO.DATA_COBRANCA, CONTRATO.COBRANCA_AUTOMATICA, CONTRATO.VALOR_MENSALIDADE, CONTRATO.VALOR_NUMERO_VIDAS, CONTRATO.DIAS_BLOQUEIO, CONTRATO.ID_CONTRATO_RAIZ, CONTRATO.ID_CLIENTE_PROPOSTA, CONTRATO.PROPOSTA, CONTRATO.DATA_FIM, CONTRATO.DATA_INICIO, CONTRATO.PROPOSTA_FORMA_PAGAMENTO, CONTRATO.ID_USUARIO_ENCERROU, CONTRATO.DATA_ENCERRAMENTO, CONTRATO.MOTIVO_ENCERRAMENTO , PRODUTO_CONTRATO.SITUACAO, PRODUTO_CONTRATO.ALTERA, CONTRATO.ID_EMPRESA, PRODUTO.PADRAO_CONTRATO ");
                    query.Append(" FROM SEG_PRODUTO_CONTRATO PRODUTO_CONTRATO ");
                    query.Append(" JOIN SEG_PRODUTO PRODUTO ON PRODUTO.ID_PRODUTO = PRODUTO_CONTRATO.ID_PRODUTO ");
                    query.Append(" JOIN SEG_CONTRATO CONTRATO ON CONTRATO.ID_CONTRATO = PRODUTO_CONTRATO.ID_CONTRATO");
                    query.Append(" WHERE PRODUTO_CONTRATO.ID_PRODUTO_CONTRATO = :VALUE1 ");

                    NpgsqlCommand commandContratoProduto = new NpgsqlCommand(query.ToString(), dbConnection);

                    commandContratoProduto.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                    commandContratoProduto.Parameters[0].Value = movimento.ContratoProduto.Id;

                    NpgsqlDataReader drcp = commandContratoProduto.ExecuteReader();

                    while (drcp.Read())
                    {
                        ContratoProduto contratoProduto = new ContratoProduto();
                        contratoProduto.Id = drcp.GetInt64(0);
                        Produto produto = new Produto();
                        produto.Id = drcp.GetInt64(1);
                        produto.Nome = drcp.GetString(7);
                        produto.Situacao = drcp.GetString(8);
                        produto.Preco = drcp.GetDecimal(9);
                        produto.Tipo = drcp.GetString(10);
                        produto.Custo = drcp.GetDecimal(11);
                        produto.PadraoContrato = drcp.GetBoolean(47);
                        contratoProduto.Produto = produto;

                        Contrato contrato = new Contrato();
                        contrato.Id = drcp.GetInt64(2);
                        contrato.CodigoContrato = drcp.GetString(15);

                        Cliente cliente = new Cliente();
                        cliente.Id = drcp.GetInt64(14);
                        contrato.Cliente = cliente;

                        Usuario usuarioCriadorContrato = new Usuario();
                        usuarioCriadorContrato.Id = drcp.GetInt64(19);
                        contrato.UsuarioCriador = usuarioCriadorContrato;

                        Usuario usuarioFinalizouContrato = new Usuario();
                        usuarioFinalizouContrato.Id = drcp.GetInt64(20);
                        contrato.UsuarioFinalizou = usuarioFinalizouContrato;

                        if (!drcp.IsDBNull(21))
                        {
                            Usuario usuarioCancelouContrato = new Usuario();
                            usuarioCancelouContrato.Id = drcp.GetInt64(21);
                            contrato.UsuarioCancelamento = usuarioCancelouContrato;
                            contrato.MotivoCancelamento = drcp.GetString(22);
                            contrato.DataCancelamento = drcp.GetDateTime(25);
                        }

                        if (!drcp.IsDBNull(23))
                        {
                            Cliente prestador = new Cliente();
                            prestador.Id = drcp.GetInt64(23);
                            contrato.Prestador = prestador;
                        }

                        contrato.ContratoAutomatico = drcp.GetBoolean(24);

                        if (!drcp.IsDBNull(26))
                        {
                            Contrato contratoPai = new Contrato();
                            contratoPai.Id = drcp.GetInt64(26);
                            contrato.ContratoPai = contratoPai;
                        }

                        contrato.BloqueiaInadimplencia = drcp.GetBoolean(27);
                        contrato.GeraMensalidade = drcp.GetBoolean(28);
                        contrato.GeraNumeroVidas = drcp.GetBoolean(29);
                        contrato.DataCobranca = drcp.IsDBNull(30) ? 0 : drcp.GetInt32(30);
                        contrato.CobrancaAutomatica = drcp.GetBoolean(31);
                        contrato.ValorMensalidade = drcp.IsDBNull(32) ? 0 : drcp.GetDecimal(32);
                        contrato.ValorNumeroVidas = drcp.IsDBNull(33) ? 0 : drcp.GetDecimal(33);
                        contrato.DiasInadimplenciaBloqueio = drcp.IsDBNull(34) ? 0 : drcp.GetInt32(34);

                        if (!drcp.IsDBNull(35))
                        {
                            Contrato contratoRaiz = new Contrato();
                            contratoRaiz.Id = drcp.GetInt64(35);
                            contrato.ContratoRaiz = contratoRaiz;
                        }

                        if (!drcp.IsDBNull(36))
                        {
                            ClienteProposta clienteProposta = new ClienteProposta();
                            clienteProposta.Id = drcp.GetInt64(36);
                        }

                        contrato.Proposta = drcp.GetBoolean(37);
                        contrato.DataFim = drcp.IsDBNull(38) ? (DateTime?)null : drcp.GetDateTime(38);
                        contrato.DataInicio = drcp.GetDateTime(39);
                        contrato.PropostaFormaPagamento = drcp.GetString(40);

                        if (!drcp.IsDBNull(41))
                        {
                            Usuario usuarioEncerrou = new Usuario();
                            usuarioEncerrou.Id = drcp.GetInt64(41);
                            contrato.UsuarioEncerrou = usuarioEncerrou;
                            contrato.DataEncerramento = drcp.GetDateTime(42);
                            contrato.MotivoEncerramento = drcp.GetString(43);
                        }

                        contrato.Situacao = drcp.GetString(44);
                        contratoProduto.Altera = drcp.GetBoolean(45);

                        Empresa empresa = new Empresa();
                        empresa.Id = drcp.GetInt64(46);
                        contrato.Empresa = empresa;
                        produto.PadraoContrato = drcp.GetBoolean(47);
                        contratoProduto.Contrato = contrato;

                        movimento.ContratoProduto = contratoProduto;
                    }
                }

                #endregion

                return movimento;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void insertMovimentoInProtocolo(Movimento movimento, Protocolo protocolo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertMovimentoInProtocolo");

            try
            {

                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_MOVIMENTO SET ID_PROTOCOLO = :VALUE1 WHERE ID_MOVIMENTO = :VALUE2 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = protocolo.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = movimento.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertMovimentoInProtocolo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public List<Movimento> findAllLancamentoByAtendimento(Aso atendimento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllLancamentoByAtendimento");

            try
            {
                StringBuilder query = new StringBuilder();

                List<Movimento> movimentos = new List<Movimento>();

                query.Append(" SELECT MOVIMENTO.ID_MOVIMENTO, MOVIMENTO.ID_EMPRESA, MOVIMENTO.ID_GHE_FONTE_AGENTE_EXAME_ASO,  ");
                query.Append(" MOVIMENTO.ID_CLIENTE_FUNCAO_EXAME_ASO, MOVIMENTO.ID_CLIENTE, MOVIMENTO.ID_PRODUTO_CONTRATO, MOVIMENTO.ID_NFE,  ");
                query.Append(" MOVIMENTO.DT_INCLUSAO, MOVIMENTO.DT_FATURAMENTO, MOVIMENTO.SITUACAO, MOVIMENTO.PRC_UNIT, MOVIMENTO.COM_VALOR, ");
                query.Append(" MOVIMENTO.ID_ESTUDO, MOVIMENTO.ID_ASO, MOVIMENTO.QUANTIDADE, MOVIMENTO.USUARIO, MOVIMENTO.ID_USUARIO,  ");
                query.Append(" MOVIMENTO.PRECO_CUSTO, MOVIMENTO.ID_CLIENTE_UNIDADE, MOVIMENTO.ID_PROTOCOLO, MOVIMENTO.DATA_GRAVACAO, MOVIMENTO.ID_CENTROCUSTO, MOVIMENTO.ID_CONTRATO_EXAME, MOVIMENTO.ID_CLIENTE_CREDENCIADA ");
                query.Append(" FROM SEG_MOVIMENTO MOVIMENTO  ");
                query.Append(" WHERE MOVIMENTO.ID_ASO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = atendimento.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    Movimento movimento = new Movimento();
                    movimento.Id = dr.GetInt64(0);
                    movimento.GheFonteAgenteExameAso = dr.IsDBNull(2) ? null : new GheFonteAgenteExameAso(dr.GetInt64(2));
                    movimento.ClienteFuncaoExameAso = dr.IsDBNull(3) ? null : new ClienteFuncaoExameASo(dr.GetInt64(3));
                    movimento.Cliente = new Cliente(dr.GetInt64(4));
                    movimento.ContratoProduto = dr.IsDBNull(5) ? null : new ContratoProduto(dr.GetInt64(5), null, null, null, null, null, null, String.Empty, true);
                    movimento.Nfe = dr.IsDBNull(6) ? null : new Nfe(dr.GetInt64(6));
                    movimento.DataInclusao = dr.GetDateTime(7);
                    movimento.DataFaturamento = dr.IsDBNull(8) ? null : (DateTime?)dr.GetDateTime(8);
                    movimento.Situacao = dr.GetString(9);
                    movimento.PrecoUnit = dr.IsDBNull(10) ? null : (Decimal?)dr.GetDecimal(10);
                    movimento.ValorComissao = dr.IsDBNull(11) ? null : (Decimal?)dr.GetDecimal(11);
                    movimento.Estudo = dr.IsDBNull(12) ? null : new Estudo(dr.GetInt64(12));
                    movimento.Aso = dr.IsDBNull(13) ? null : new Aso(dr.GetInt64(13));
                    movimento.Quantidade = dr.GetInt32(14);
                    movimento.Usuario = new Usuario(dr.GetInt64(16));
                    movimento.Usuario.Login = dr.IsDBNull(15) ? string.Empty : dr.GetString(15);
                    movimento.Empresa = dr.IsDBNull(1) ? null : new Empresa(dr.GetInt64(1));
                    movimento.CustoUnitario = dr.IsDBNull(17) ? null : (Decimal?)dr.GetDecimal(17);
                    movimento.Unidade = dr.IsDBNull(18) ? null : new Cliente(dr.GetInt64(18));
                    movimento.Protocolo = dr.IsDBNull(19) ? null : new Protocolo(dr.GetInt64(19));
                    movimento.DataGravacao = dr.GetDateTime(20);
                    movimento.CentroCusto = dr.IsDBNull(21) ? null : new CentroCusto(dr.GetInt64(21), null, string.Empty, string.Empty);
                    movimento.ContratoExame = dr.IsDBNull(22) ? null : new ContratoExame(dr.GetInt64(22));
                    movimento.ClienteCredenciada = dr.IsDBNull(23) ? null : new Cliente(dr.GetInt64(23));

                    movimentos.Add(movimento); 
                }

                return movimentos;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllLancamentoByAtendimento", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public FileFinanceiroXlsx createFileXlsx(Cliente cliente, DateTime dataInicialLancamento, DateTime dataFinalLancamento, DateTime? dataInicialAtendimento, DateTime? dataFinalAtendimento, string situacao, string tipoAnalise, long idCentroCusto, Cliente credenciada, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método createFileXlsx");

            try
            {
                List<object[]> objectExame = new List<object[]> { };
                List<object[]> objectProduto = new List<object[]> { };
                
                FileFinanceiroXlsx arquivo = new FileFinanceiroXlsx();
                StringBuilder query = new StringBuilder();

                /* Exames */
                if (string.IsNullOrEmpty(tipoAnalise) || string.Equals(tipoAnalise, "E"))
                {
                    query.Clear();
                    query.Append("SELECT ATENDIMENTO.RAZAO_EMPRESA, (CASE WHEN LENGTH(ATENDIMENTO.CNPJ_EMPRESA) = 14 THEN FORMATA_CNPJ(ATENDIMENTO.CNPJ_EMPRESA) ELSE FORMATA_CPF(ATENDIMENTO.CNPJ_EMPRESA) END ) AS CNPJ_CLIENTE, CENTRO_CUSTO.DESCRICAO AS CENTRO_CUSTO, FUNCIONARIO.NOME, FUNCIONARIO.RG, FUNCIONARIO.CPF, FUNCAO.DESCRICAO AS FUNCAO, CAST(ATENDIMENTO.DATA_ASO AS DATE) AS DATA_ATENDIMENTO, FORMATA_CODIGO_ATENDIMENTO(ATENDIMENTO.CODIGO_ASO) AS CODIGO_ATENDIMENTO, (CASE WHEN TIPO.IND_PERIODICO = TRUE THEN 'PERIODICO' ELSE TIPO.DESCRICAO END) AS TIPO_ATENDIMENTO, EXAME.DESCRICAO AS EXAME, MOVIMENTO.PRC_UNIT AS PRECO_UNITARIO, MOVIMENTO.QUANTIDADE, (CASE WHEN MOVIMENTO.SITUACAO = 'P' THEN 'PENDENTE' ELSE 'FATURADO' END ) AS SITUACAO ");
                    query.Append("FROM SEG_MOVIMENTO MOVIMENTO  ");
                    query.Append("LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GHE_FONTE_AGENTE_EXAME_ASO ON GHE_FONTE_AGENTE_EXAME_ASO.ID_GHE_FONTE_AGENTE_EXAME_ASO = MOVIMENTO.ID_GHE_FONTE_AGENTE_EXAME_ASO  ");
                    query.Append("LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME GHE_FONTE_AGENTE_EXAME ON GHE_FONTE_AGENTE_EXAME.ID_GHE_FONTE_AGENTE_EXAME = GHE_FONTE_AGENTE_EXAME_ASO.ID_GHE_FONTE_AGENTE_EXAME  ");
                    query.Append("LEFT JOIN SEG_CLIENTE_FUNCAO_EXAME_ASO CLIENTE_FUNCAO_EXAME_ASO ON CLIENTE_FUNCAO_EXAME_ASO.ID_CLIENTE_FUNCAO_EXAME_ASO = MOVIMENTO.ID_CLIENTE_FUNCAO_EXAME_ASO  ");
                    query.Append("LEFT JOIN SEG_CLIENTE_FUNCAO_EXAME CLIENTE_FUNCAO_EXAME ON CLIENTE_FUNCAO_EXAME.ID_CLIENTE_FUNCAO_EXAME = CLIENTE_FUNCAO_EXAME_ASO.ID_CLIENTE_FUNCAO_EXAME  ");
                    query.Append("LEFT JOIN SEG_CENTROCUSTO CENTRO_CUSTO ON CENTRO_CUSTO.ID = MOVIMENTO.ID_CENTROCUSTO ");
                    query.Append("JOIN SEG_EXAME EXAME ON (EXAME.ID_EXAME = GHE_FONTE_AGENTE_EXAME.ID_EXAME OR EXAME.ID_EXAME = CLIENTE_FUNCAO_EXAME.ID_EXAME)  ");
                    query.Append("JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = MOVIMENTO.ID_CLIENTE  ");
                    query.Append("JOIN SEG_ASO ATENDIMENTO ON ATENDIMENTO.ID_ASO = MOVIMENTO.ID_ASO  ");
                    query.Append("JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CLIENTE_FUNCAO_FUNCIONARIO ON CLIENTE_FUNCAO_FUNCIONARIO.ID_CLIENTE_FUNCAO_FUNCIONARIO = ATENDIMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                    query.Append("JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = CLIENTE_FUNCAO_FUNCIONARIO.ID_SEG_CLIENTE_FUNCAO ");
                    query.Append("JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = CLIENTE_FUNCAO.ID_FUNCAO ");
                    query.Append("JOIN SEG_FUNCIONARIO FUNCIONARIO ON FUNCIONARIO.ID_FUNCIONARIO = CLIENTE_FUNCAO_FUNCIONARIO.ID_FUNCIONARIO ");
                    query.Append("JOIN SEG_EMPRESA EMPRESA ON EMPRESA.ID_EMPRESA = MOVIMENTO.ID_EMPRESA ");
                    query.Append("JOIN SEG_PERIODICIDADE TIPO ON TIPO.ID_PERIODICIDADE = ATENDIMENTO.ID_PERIODICIDADE ");
                    query.Append(" WHERE MOVIMENTO.ID_EMPRESA = :VALUE4 ");
                    query.Append(" AND MOVIMENTO.ID_CLIENTE = :VALUE1 ");
                    query.Append(" AND MOVIMENTO.DT_INCLUSAO BETWEEN :VALUE2 AND :VALUE3 ");

                    if (string.IsNullOrEmpty(situacao))
                        query.Append(" AND MOVIMENTO.SITUACAO <> 'C' ");

                    else if (string.Equals(situacao, "P"))
                        query.Append(" AND MOVIMENTO.SITUACAO = 'P'");

                    else
                        query.Append(" AND MOVIMENTO.SITUACAO = 'F'");

                    if (dataInicialAtendimento != null)
                        query.Append(" AND ATENDIMENTO.DATA_ASO BETWEEN :VALUE5 AND :VALUE6 ");

                    if (idCentroCusto != 0)
                        query.Append(" AND MOVIMENTO.ID_CENTROCUSTO = :VALUE7 ");

                    if (credenciada != null)
                        query.Append(" AND MOVIMENTO.ID_CLIENTE_CREDENCIADA = :VALUE8 ");

                    NpgsqlCommand commandExame = new NpgsqlCommand(query.ToString(), dbConnection);

                    commandExame.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                    commandExame.Parameters[0].Value = cliente.Id;

                    commandExame.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                    commandExame.Parameters[1].Value = dataInicialLancamento;

                    commandExame.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Timestamp));
                    commandExame.Parameters[2].Value = dataFinalLancamento;

                    commandExame.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                    commandExame.Parameters[3].Value = PermissionamentoFacade.usuarioAutenticado.Empresa.Id;

                    commandExame.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Timestamp));
                    commandExame.Parameters[4].Value = dataInicialAtendimento == null ? (DateTime?)null : dataInicialAtendimento;

                    commandExame.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Timestamp));
                    commandExame.Parameters[5].Value = dataFinalAtendimento == null ? (DateTime?)null : dataFinalAtendimento;

                    commandExame.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Integer));
                    commandExame.Parameters[6].Value = idCentroCusto;

                    commandExame.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Integer));
                    commandExame.Parameters[7].Value = credenciada != null ? credenciada.Id : (long?)null;

                    NpgsqlDataReader drExame = commandExame.ExecuteReader();

                    while (drExame.Read())
                    {
                        objectExame.Add(new object[] {drExame.IsDBNull(0) ? string.Empty : drExame.GetString(0), drExame.IsDBNull(1) ? string   .Empty : drExame.GetString(1), drExame.IsDBNull(2) ? string.Empty : drExame.GetString(2), drExame.IsDBNull(3) ? string.Empty : drExame.GetString(3) , drExame.IsDBNull(4) ? string.Empty : drExame.GetString(4), drExame.IsDBNull(5) ? string.Empty : drExame.GetString(5), drExame.IsDBNull(6) ? string.Empty : drExame.GetString(6), drExame.IsDBNull(7) ? (string)null : drExame.GetDateTime(7).ToShortDateString(), drExame.IsDBNull(8) ? string.Empty : drExame.GetString(8), drExame.IsDBNull(9) ? string.Empty : drExame.GetString(9), drExame.IsDBNull(10) ? string.Empty : drExame.GetString(10), drExame.IsDBNull(11) ? (decimal?)null : drExame.GetDecimal(11), drExame.IsDBNull(12) ? (int?)null : drExame.GetInt32(12), drExame.IsDBNull(13) ? string.Empty : drExame.GetString(13)});
                    }
                    
                    
                }

                /* Produtos */
                if (string.IsNullOrEmpty(tipoAnalise) || string.Equals(tipoAnalise, "P"))
                {
                    query.Clear();
                    query.Append("SELECT CLIENTE.RAZAO_SOCIAL, (CASE WHEN LENGTH(CLIENTE.CNPJ) = 14 THEN FORMATA_CNPJ(CLIENTE.CNPJ) ELSE FORMATA_CPF(CLIENTE.CNPJ) END ) AS CNPJ_CLIENTE, CENTRO_CUSTO.DESCRICAO AS CENTRO_CUSTO, PRODUTO.NOME AS PRODUTO, MOVIMENTO.PRC_UNIT AS PRECO_UNITARIO, MOVIMENTO.QUANTIDADE, (CASE WHEN MOVIMENTO.SITUACAO = 'P' THEN 'PENDENTE' ELSE 'FATURADO' END ) AS SITUACAO, MOVIMENTO.DT_INCLUSAO AS DATA_INCLUSAO ");
                    query.Append("FROM SEG_MOVIMENTO MOVIMENTO  ");
                    query.Append("JOIN SEG_PRODUTO_CONTRATO PRODUTO_CONTRATO ON PRODUTO_CONTRATO.ID_PRODUTO_CONTRATO = MOVIMENTO.ID_PRODUTO_CONTRATO ");
                    query.Append("JOIN SEG_PRODUTO PRODUTO ON PRODUTO.ID_PRODUTO = PRODUTO_CONTRATO.ID_PRODUTO ");
                    query.Append("JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = MOVIMENTO.ID_CLIENTE  ");
                    query.Append("JOIN SEG_EMPRESA EMPRESA ON EMPRESA.ID_EMPRESA = MOVIMENTO.ID_EMPRESA ");
                    query.Append("LEFT JOIN SEG_CENTROCUSTO CENTRO_CUSTO ON CENTRO_CUSTO.ID = MOVIMENTO.ID_CENTROCUSTO ");
                    query.Append("WHERE MOVIMENTO.ID_EMPRESA = :VALUE4 ");
                    query.Append("AND MOVIMENTO.ID_CLIENTE = :VALUE1 ");
                    query.Append("AND MOVIMENTO.DT_INCLUSAO BETWEEN :VALUE2 AND :VALUE3 ");

                    if (string.IsNullOrEmpty(situacao))
                        query.Append(" AND MOVIMENTO.SITUACAO <> 'C' ");

                    else if (string.Equals(situacao, "P"))
                        query.Append(" AND MOVIMENTO.SITUACAO = 'P'");

                    else
                        query.Append(" AND MOVIMENTO.SITUACAO = 'F'");

                    if (idCentroCusto != 0)
                        query.Append(" AND MOVIMENTO.ID_CENTROCUSTO = :VALUE5 ");

                    if (credenciada != null)
                        query.Append(" AND MOVIMENTO.ID_CLIENTE_CREDENCIADA = :VALUE6 ");


                    NpgsqlCommand commandProduto = new NpgsqlCommand(query.ToString(), dbConnection);

                    commandProduto.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                    commandProduto.Parameters[0].Value = cliente.Id;

                    commandProduto.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                    commandProduto.Parameters[1].Value = dataInicialLancamento;

                    commandProduto.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Timestamp));
                    commandProduto.Parameters[2].Value = dataFinalLancamento;

                    commandProduto.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                    commandProduto.Parameters[3].Value = PermissionamentoFacade.usuarioAutenticado.Empresa.Id;

                    commandProduto.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Integer));
                    commandProduto.Parameters[4].Value = idCentroCusto;

                    commandProduto.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Integer));
                    commandProduto.Parameters[5].Value = credenciada != null ? credenciada.Id : (long?)null;

                    NpgsqlDataReader drProduto = commandProduto.ExecuteReader();

                    while (drProduto.Read())
                    {
                        objectProduto.Add( new object[] { drProduto.IsDBNull(0) ? string.Empty : drProduto.GetString(0), drProduto.IsDBNull(1) ? string.Empty : drProduto.GetString(1), drProduto.IsDBNull(2) ? string.Empty : drProduto.GetString(2), drProduto.IsDBNull(3) ? string.Empty : drProduto.GetString(3), drProduto.IsDBNull(4) ? (decimal?)null : drProduto.GetDecimal(4), drProduto.IsDBNull(5) ? (int?)null : drProduto.GetInt32(5), drProduto.IsDBNull(6) ? string.Empty : drProduto.GetString(6), drProduto.IsDBNull(7) ? (string)null : drProduto.GetDateTime(7).ToShortDateString()});
                    }

                    
                }
                arquivo.ListExames = objectExame;
                arquivo.ListProdutos = objectProduto;
                return arquivo;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - createFileXlsx", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
