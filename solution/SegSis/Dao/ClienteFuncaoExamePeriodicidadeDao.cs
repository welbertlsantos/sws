﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;
using NpgsqlTypes;
using SWS.Entidade;
using System.Data;
using SWS.View.Resources;

namespace SWS.Dao
{
    class ClienteFuncaoExamePeriodicidadeDao : IClienteFuncaoExamePeriodicidadeDao
    {
        public ClienteFuncaoExamePeriodicidade insert(ClienteFuncaoExamePeriodicidade clienteFuncaoExamePeriodicidade, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            StringBuilder query = new StringBuilder();
            
            try
            {
                query.Append(" INSERT INTO SEG_CLIENTE_FUNCAO_EXAME_PERIODICIDADE ");
                query.Append(" (ID_PERIODICIDADE, ID_CLIENTE_FUNCAO_EXAME, SITUACAO, PERIODO_VENCIMENTO) VALUES (:VALUE1, :VALUE2, 'A', :VALUE3)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncaoExamePeriodicidade.Periodicidade.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = clienteFuncaoExamePeriodicidade.ClienteFuncaoExame.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = clienteFuncaoExamePeriodicidade.PeriodoVencimento != null ? clienteFuncaoExamePeriodicidade.PeriodoVencimento : null;

                command.ExecuteNonQuery();

                return clienteFuncaoExamePeriodicidade;
             
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public DataSet listaPeriodicidadesByClienteFuncaoExame(ClienteFuncaoExame clienteFuncaoExame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método listaPeriodicidadesByClienteFuncaoExame");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT PERIODICIDADE.ID_PERIODICIDADE, PERIODICIDADE.DESCRICAO, CLIENTE_FUNCAO_EXAME_PERIODICIDADE.PERIODO_VENCIMENTO FROM ");
                query.Append(" SEG_CLIENTE_FUNCAO_EXAME_PERIODICIDADE CLIENTE_FUNCAO_EXAME_PERIODICIDADE ");
                query.Append(" JOIN SEG_PERIODICIDADE PERIODICIDADE ON PERIODICIDADE.ID_PERIODICIDADE = CLIENTE_FUNCAO_EXAME_PERIODICIDADE.ID_PERIODICIDADE ");
                query.Append(" WHERE ID_CLIENTE_FUNCAO_EXAME = :VALUE1 AND CLIENTE_FUNCAO_EXAME_PERIODICIDADE.SITUACAO = 'A'");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = clienteFuncaoExame.Id;
                
                DataSet ds = new DataSet();

                adapter.Fill(ds, "Periodicidades");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - listaPeriodicidadesByClienteFuncaoExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void delete(ClienteFuncaoExamePeriodicidade clienteFuncaoExamePeriodicidade, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" DELETE FROM SEG_CLIENTE_FUNCAO_EXAME_PERIODICIDADE WHERE ID_CLIENTE_FUNCAO_EXAME = :VALUE1 AND ID_PERIODICIDADE = :VALUE2", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncaoExamePeriodicidade.ClienteFuncaoExame.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = clienteFuncaoExamePeriodicidade.Periodicidade.Id;

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void update(ClienteFuncaoExamePeriodicidade clienteFuncaoExamePeriodicidade, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" UPDATE SEG_CLIENTE_FUNCAO_EXAME_PERIODICIDADE SET SITUACAO = :VALUE3, PERIODO_VENCIMENTO = :VALUE4 WHERE ID_CLIENTE_FUNCAO_EXAME = :VALUE1 AND ID_PERIODICIDADE = :VALUE2 ", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncaoExamePeriodicidade.ClienteFuncaoExame.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = clienteFuncaoExamePeriodicidade.Periodicidade.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = clienteFuncaoExamePeriodicidade.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = clienteFuncaoExamePeriodicidade.PeriodoVencimento != null ? clienteFuncaoExamePeriodicidade.PeriodoVencimento : null;

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<ClienteFuncaoExamePeriodicidade> listAllByClienteFuncaoByPeriodicidade(ClienteFuncao clienteFuncao, Periodicidade periodicidade, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método listAllByClienteFuncao");

            StringBuilder query = new StringBuilder();

            List<ClienteFuncaoExamePeriodicidade> clienteFuncaoExamePeriodicidade = new List<ClienteFuncaoExamePeriodicidade>();

            try
            {
                query.Append(" SELECT PERIODICIDADE.ID_PERIODICIDADE, PERIODICIDADE.DESCRICAO, CLIENTE_FUNCAO_EXAME_PERIODICIDADE.PERIODO_VENCIMENTO, CLIENTE_FUNCAO_EXAME_PERIODICIDADE.SITUACAO, CLIENTE_FUNCAO_EXAME.ID_CLIENTE_FUNCAO_EXAME, CLIENTE_FUNCAO_EXAME.IDADE_EXAME, CLIENTE_FUNCAO_EXAME.SITUACAO, CLIENTE_FUNCAO_EXAME.ID_EXAME FROM ");
                query.Append(" SEG_CLIENTE_FUNCAO_EXAME_PERIODICIDADE CLIENTE_FUNCAO_EXAME_PERIODICIDADE ");
                query.Append(" JOIN SEG_PERIODICIDADE PERIODICIDADE ON PERIODICIDADE.ID_PERIODICIDADE = CLIENTE_FUNCAO_EXAME_PERIODICIDADE.ID_PERIODICIDADE ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME CLIENTE_FUNCAO_EXAME ON CLIENTE_FUNCAO_EXAME.ID_CLIENTE_FUNCAO_EXAME = CLIENTE_FUNCAO_EXAME_PERIODICIDADE.ID_CLIENTE_FUNCAO_EXAME ");
                query.Append(" WHERE CLIENTE_FUNCAO_EXAME.ID_CLIENTE_FUNCAO = :VALUE1 AND CLIENTE_FUNCAO_EXAME_PERIODICIDADE.SITUACAO = 'A' AND CLIENTE_FUNCAO_EXAME.SITUACAO = 'A' ");
                if (periodicidade.Id == 2) // indicador de períodico
                    query.Append(" AND PERIODICIDADE.ID_PERIODICIDADE IN (1,2) ");
                else
                    query.Append(" AND PERIODICIDADE.ID_PERIODICIDADE = :VALUE2 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncao.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = periodicidade.Id;

                NpgsqlDataReader dr = command.ExecuteReader();
                
                while (dr.Read())
                {
                    clienteFuncaoExamePeriodicidade.Add(new ClienteFuncaoExamePeriodicidade(new ClienteFuncaoExame(dr.GetInt64(4), clienteFuncao, new Exame(dr.GetInt64(7)), dr.GetString(6), dr.GetInt32(5) ), new Periodicidade(dr.GetInt64(0), dr.GetString(1), null), dr.GetString(3), dr.IsDBNull(2) ? (int?)null : dr.GetInt32(2)));
                }

                return clienteFuncaoExamePeriodicidade;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - listAllByClienteFuncao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
