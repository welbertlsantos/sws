﻿using Npgsql;
using NpgsqlTypes;
using SWS.Entidade;
using SWS.Excecao;
using SWS.Helper;
using SWS.IDao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.Dao
{
    class MonitoramentoGheFonteAgenteDao : IMonitoramentoGheFonteAgenteDao
    {
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_MONITORAMENTO_GHEFONTEAGE_ID_MONITORAMENTO_GHEFONTEAGEN_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public MonitoramentoGheFonteAgente insert(MonitoramentoGheFonteAgente monitoramentoGheFonteAgente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            try
            {
                monitoramentoGheFonteAgente.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_MONITORAMENTO_GHEFONTEAGENTE (ID_MONITORAMENTO_GHEFONTEAGENTE, ID_GHE_FONTE_AGENTE, ID_MONITORAMENTO, TIPO_AVALIACAO, INTENSIDADE_CONCENTRACAO, LIMITE_TOLERANCIA, UNIDADE_MEDICAO, TECNICA_MEDICAO, UTILIZA_EPC, EFICACIA_EPC, UTILIZA_EPI, EFICACIA_EPI, MEDIDA_PROTECAO, CONDICAO_FUNCIONAMENTO, USO_INIT, PRAZO_VALIDADE, PERIODICIDADE_TROCA, HIGIENIZACAO, NUMERO_PROCESSO_JUDICIAL) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6, :VALUE7, :VALUE8, :VALUE9, :VALUE10, :VALUE11, :VALUE12, :VALUE13, :VALUE14, :VALUE15, :VALUE16, :VALUE17, :VALUE18, :VALUE19 ) ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = monitoramentoGheFonteAgente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = monitoramentoGheFonteAgente.GheFonteAgente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = monitoramentoGheFonteAgente.Monitoramento.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = monitoramentoGheFonteAgente.TipoAvaliacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Numeric));
                command.Parameters[4].Value = monitoramentoGheFonteAgente.IntensidadeConcentracao;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Numeric));
                command.Parameters[5].Value = monitoramentoGheFonteAgente.LimiteTolerancia;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = monitoramentoGheFonteAgente.UnidadeMedicao;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Text));
                command.Parameters[7].Value = monitoramentoGheFonteAgente.TecnicaMedicao;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                command.Parameters[8].Value = monitoramentoGheFonteAgente.UtilizaEpc;

                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                command.Parameters[9].Value = monitoramentoGheFonteAgente.EficaciaEpc;

                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Varchar));
                command.Parameters[10].Value = monitoramentoGheFonteAgente.UtilizaEpi;

                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Varchar));
                command.Parameters[11].Value = monitoramentoGheFonteAgente.EficaciaEpi;

                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Varchar));
                command.Parameters[12].Value = monitoramentoGheFonteAgente.MedidaProtecao;

                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Varchar));
                command.Parameters[13].Value = monitoramentoGheFonteAgente.CondicaoFuncionamento;

                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Varchar));
                command.Parameters[14].Value = monitoramentoGheFonteAgente.UsoIniterrupto;

                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Varchar));
                command.Parameters[15].Value = monitoramentoGheFonteAgente.PrazoValidade;

                command.Parameters.Add(new NpgsqlParameter("VALUE17", NpgsqlDbType.Varchar));
                command.Parameters[16].Value = monitoramentoGheFonteAgente.PeriodicidadeTroca;

                command.Parameters.Add(new NpgsqlParameter("VALUE18", NpgsqlDbType.Varchar));
                command.Parameters[17].Value = monitoramentoGheFonteAgente.Higienizacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE19", NpgsqlDbType.Varchar));
                command.Parameters[18].Value = monitoramentoGheFonteAgente.NumeroProcessoJudicial;

                command.ExecuteNonQuery();

                return monitoramentoGheFonteAgente;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public MonitoramentoGheFonteAgente update(MonitoramentoGheFonteAgente monitoramentoGheFonteAgente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_MONITORAMENTO_GHEFONTEAGENTE SET ");
                query.Append(" ID_GHE_FONTE_AGENTE = :VALUE2, ID_MONITORAMENTO = :VALUE3, TIPO_AVALIACAO = :VALUE4, INTENSIDADE_CONCENTRACAO = :VALUE5, LIMITE_TOLERANCIA = :VALUE6, UNIDADE_MEDICAO = :VALUE7, TECNICA_MEDICAO = :VALUE8, UTILIZA_EPC = :VALUE9, EFICACIA_EPC = :VALUE10, UTILIZA_EPI = :VALUE11, EFICACIA_EPI = :VALUE12, MEDIDA_PROTECAO = :VALUE13, CONDICAO_FUNCIONAMENTO = :VALUE14, USO_INIT = :VALUE15, PRAZO_VALIDADE = :VALUE16, PERIODICIDADE_TROCA = :VALUE17, HIGIENIZACAO = :VALUE18, NUMERO_PROCESSO_JUDICIAL = :VALUE19 ");
                query.Append(" WHERE ID_MONITORAMENTO_GHEFONTEAGENTE = :VALUE1  ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = monitoramentoGheFonteAgente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = monitoramentoGheFonteAgente.GheFonteAgente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = monitoramentoGheFonteAgente.Monitoramento.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = monitoramentoGheFonteAgente.TipoAvaliacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Numeric));
                command.Parameters[4].Value = monitoramentoGheFonteAgente.IntensidadeConcentracao;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Numeric));
                command.Parameters[5].Value = monitoramentoGheFonteAgente.LimiteTolerancia;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = monitoramentoGheFonteAgente.UnidadeMedicao;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Text));
                command.Parameters[7].Value = monitoramentoGheFonteAgente.TecnicaMedicao;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                command.Parameters[8].Value = monitoramentoGheFonteAgente.UtilizaEpc;

                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                command.Parameters[9].Value = monitoramentoGheFonteAgente.EficaciaEpc;

                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Varchar));
                command.Parameters[10].Value = monitoramentoGheFonteAgente.UtilizaEpi;

                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Varchar));
                command.Parameters[11].Value = monitoramentoGheFonteAgente.EficaciaEpi;

                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Varchar));
                command.Parameters[12].Value = monitoramentoGheFonteAgente.MedidaProtecao;

                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Varchar));
                command.Parameters[13].Value = monitoramentoGheFonteAgente.CondicaoFuncionamento;

                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Varchar));
                command.Parameters[14].Value = monitoramentoGheFonteAgente.UsoIniterrupto;

                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Varchar));
                command.Parameters[15].Value = monitoramentoGheFonteAgente.PrazoValidade;

                command.Parameters.Add(new NpgsqlParameter("VALUE17", NpgsqlDbType.Varchar));
                command.Parameters[16].Value = monitoramentoGheFonteAgente.PeriodicidadeTroca;

                command.Parameters.Add(new NpgsqlParameter("VALUE18", NpgsqlDbType.Varchar));
                command.Parameters[17].Value = monitoramentoGheFonteAgente.Higienizacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE19", NpgsqlDbType.Varchar));
                command.Parameters[18].Value = monitoramentoGheFonteAgente.NumeroProcessoJudicial;


                command.ExecuteNonQuery();

                return monitoramentoGheFonteAgente;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public MonitoramentoGheFonteAgente findById(long id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            MonitoramentoGheFonteAgente monitoramentoGheFonteAgente = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID_MONITORAMENTO_GHEFONTEAGENTE, ID_GHE_FONTE_AGENTE, ID_MONITORAMENTO, TIPO_AVALIACAO, INTENSIDADE_CONCENTRACAO, LIMITE_TOLERANCIA, UNIDADE_MEDICAO, TECNICA_MEDICAO, UTILIZA_EPC, EFICACIA_EPC, UTILIZA_EPI, EFICACIA_EPI, MEDIDA_PROTECAO, CONDICAO_FUNCIONAMENTO, USO_INIT, PRAZO_VALIDADE, PERIODICIDADE_TROCA, HIGIENIZACAO, NUMERO_PROCESSO_JUDICIAL ");
                query.Append(" FROM SEG_MONITORAMENTO_GHEFONTEAGENTE  ");
                query.Append(" WHERE ID_MONITORAMENTO_GHEFONTEAGENTE = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    monitoramentoGheFonteAgente = new MonitoramentoGheFonteAgente(dr.GetInt64(0), new GheFonteAgente(dr.GetInt64(1)), new Monitoramento(dr.GetInt64(2)), dr.IsDBNull(3) ? string.Empty : dr.GetString(3), dr.IsDBNull(4) ? (Decimal?)null : dr.GetDecimal(4), dr.IsDBNull(5) ? (Decimal?)null : dr.GetDecimal(5), dr.IsDBNull(6) ? string.Empty : dr.GetString(6), dr.IsDBNull(7) ? string.Empty : dr.GetString(7), dr.IsDBNull(8) ? string.Empty : dr.GetString(8), dr.IsDBNull(9) ? string.Empty : dr.GetString(9), dr.IsDBNull(10) ? string.Empty : dr.GetString(10), dr.IsDBNull(11) ? string.Empty : dr.GetString(11), dr.IsDBNull(12) ? string.Empty : dr.GetString(12), dr.IsDBNull(13) ? string.Empty : dr.GetString(13), dr.IsDBNull(14) ? string.Empty : dr.GetString(14), dr.IsDBNull(15) ? string.Empty : dr.GetString(15), dr.IsDBNull(16) ? string.Empty : dr.GetString(16), dr.IsDBNull(17) ? string.Empty : dr.GetString(17), dr.IsDBNull(18) ? string.Empty : dr.GetString(18));
                }


                return monitoramentoGheFonteAgente;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void delete(MonitoramentoGheFonteAgente monitoramentoGheFonteAgente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" DELETE FROM SEG_MONITORAMENTO_GHEFONTEAGENTE ");
                query.Append(" WHERE ID_MONITORAMENTO_GHEFONTEAGENTE = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = monitoramentoGheFonteAgente.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }


        public List<MonitoramentoGheFonteAgente> findAllByMonitoramento(Monitoramento monitoramento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByMonitoramento");

            List<MonitoramentoGheFonteAgente> monitoramentoGheFonteAgentes = new List<MonitoramentoGheFonteAgente>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID_MONITORAMENTO_GHEFONTEAGENTE, ID_GHE_FONTE_AGENTE, ID_MONITORAMENTO, TIPO_AVALIACAO, INTENSIDADE_CONCENTRACAO, LIMITE_TOLERANCIA, UNIDADE_MEDICAO, TECNICA_MEDICAO, UTILIZA_EPC, EFICACIA_EPC, UTILIZA_EPI, EFICACIA_EPI, MEDIDA_PROTECAO, CONDICAO_FUNCIONAMENTO, USO_INIT, PRAZO_VALIDADE, PERIODICIDADE_TROCA, HIGIENIZACAO, NUMERO_PROCESSO_JUDICIAL ");
                query.Append(" FROM SEG_MONITORAMENTO_GHEFONTEAGENTE");
                query.Append(" WHERE ID_MONITORAMENTO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = monitoramento.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    monitoramentoGheFonteAgentes.Add(new MonitoramentoGheFonteAgente(dr.GetInt64(0), new GheFonteAgente(dr.GetInt64(1)), new Monitoramento(dr.GetInt64(2)), dr.IsDBNull(3) ? string.Empty : dr.GetString(3), dr.IsDBNull(4) ? (Decimal?)null : dr.GetDecimal(4), dr.IsDBNull(5) ? (Decimal?)null : dr.GetDecimal(5), dr.IsDBNull(6) ? string.Empty : dr.GetString(6), dr.IsDBNull(7) ? string.Empty : dr.GetString(7), dr.IsDBNull(8) ? string.Empty : dr.GetString(8), dr.IsDBNull(9) ? string.Empty : dr.GetString(9), dr.IsDBNull(10) ? string.Empty : dr.GetString(10), dr.IsDBNull(11) ? string.Empty : dr.GetString(11), dr.IsDBNull(12) ? string.Empty : dr.GetString(12), dr.IsDBNull(13) ? string.Empty : dr.GetString(13), dr.IsDBNull(14) ? string.Empty : dr.GetString(14), dr.IsDBNull(15) ? string.Empty : dr.GetString(15), dr.IsDBNull(16) ? string.Empty : dr.GetString(16), dr.IsDBNull(17) ? string.Empty : dr.GetString(17), dr.IsDBNull(18) ? string.Empty : dr.GetString(18)));
                }

                return monitoramentoGheFonteAgentes;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByMonitoramento", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public MonitoramentoGheFonteAgente findMonitoramentoGheFonteAgenteByGheFonteAgente(GheFonteAgente gheFonteAgente, Monitoramento monitoramento, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findMonitoramentoGheFonteAgenteByGheFonteAgente");

            MonitoramentoGheFonteAgente monitoramentoGheFonteAgente = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID_MONITORAMENTO_GHEFONTEAGENTE, ID_GHE_FONTE_AGENTE, ID_MONITORAMENTO, TIPO_AVALIACAO, INTENSIDADE_CONCENTRACAO, LIMITE_TOLERANCIA, UNIDADE_MEDICAO, TECNICA_MEDICAO, UTILIZA_EPC, EFICACIA_EPC, UTILIZA_EPI, EFICACIA_EPI, MEDIDA_PROTECAO, CONDICAO_FUNCIONAMENTO, USO_INIT, PRAZO_VALIDADE, PERIODICIDADE_TROCA, HIGIENIZACAO, NUMERO_PROCESSO_JUDICIAL ");
                query.Append(" FROM SEG_MONITORAMENTO_GHEFONTEAGENTE");
                query.Append(" WHERE ID_GHE_FONTE_AGENTE = :VALUE1 ");
                query.Append(" AND ID_MONITORAMENTO = :VALUE2 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonteAgente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = monitoramento.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    monitoramentoGheFonteAgente = new MonitoramentoGheFonteAgente(dr.GetInt64(0), new GheFonteAgente(dr.GetInt64(1)), new Monitoramento(dr.GetInt64(2)), dr.IsDBNull(3) ? string.Empty : dr.GetString(3), dr.IsDBNull(4) ? (Decimal?)null : dr.GetDecimal(4), dr.IsDBNull(5) ? (Decimal?)null : dr.GetDecimal(5), dr.IsDBNull(6) ? string.Empty : dr.GetString(6), dr.IsDBNull(7) ? string.Empty : dr.GetString(7), dr.IsDBNull(8) ? string.Empty : dr.GetString(8), dr.IsDBNull(9) ? string.Empty : dr.GetString(9), dr.IsDBNull(10) ? string.Empty : dr.GetString(10), dr.IsDBNull(11) ? string.Empty : dr.GetString(11), dr.IsDBNull(12) ? string.Empty : dr.GetString(12), dr.IsDBNull(13) ? string.Empty : dr.GetString(13), dr.IsDBNull(14) ? string.Empty : dr.GetString(14), dr.IsDBNull(15) ? string.Empty : dr.GetString(15), dr.IsDBNull(16) ? string.Empty : dr.GetString(16), dr.IsDBNull(17) ? string.Empty : dr.GetString(17), dr.IsDBNull(18) ? string.Empty : dr.GetString(18));
                }

                return monitoramentoGheFonteAgente;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findMonitoramentoGheFonteAgenteByGheFonteAgente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

    }
}

