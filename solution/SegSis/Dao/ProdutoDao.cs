﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using SWS.Excecao;
using NpgsqlTypes;
using Npgsql;
using System.Data;
using SWS.Helper;
using SWS.View.Resources;

namespace SWS.Dao
{
    class ProdutoDao :IProdutoDao
    {

        public DataSet findProdutoAtivoNotInContrato(Contrato contrato, Produto produto, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findProdutoAtivoNotInContrato");

            try
            {
                StringBuilder query = new StringBuilder();

                if (String.IsNullOrEmpty(produto.Nome))
                {
                    query.Append(" SELECT ID_PRODUTO, NOME, SITUACAO, PRECO, TIPO, CUSTO, FALSE ");
                    query.Append(" FROM SEG_PRODUTO WHERE SITUACAO = 'A' AND ");
                    query.Append(" ID_PRODUTO NOT IN (SELECT ID_PRODUTO FROM SEG_PRODUTO_CONTRATO WHERE ID_CONTRATO = :VALUE1 AND SITUACAO = 'A' ) ");
                    query.Append(" ORDER BY DESCRICAO ASC ");

                }
                else
                {
                    query.Append(" SELECT ID_PRODUTO, NOME, SITUACAO, PRECO, TIPO, CUSTO, FALSE ");
                    query.Append(" FROM SEG_PRODUTO WHERE SITUACAO = 'A' AND ");
                    query.Append(" DESCRICAO LIKE :VALUE2 AND ");
                    query.Append(" ID_PRODUTO NOT IN (SELECT ID_PRODUTO FROM SEG_PRODUTO_CONTRATO WHERE ID_CONTRATO = :VALUE1 AND SITUACAO = 'A' ) ");
                    query.Append(" ORDER BY DESCRICAO ASC ");
                }
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = contrato.Id;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = produto.Nome.ToUpper() + "%";

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Produtos");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findProdutoAtivoNotInContrato", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findProdutoAtivoInGeneral(Produto produto, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findProdutoAtivoInGeneral");

            try
            {
                StringBuilder query = new StringBuilder();

                if (String.IsNullOrEmpty(produto.Nome))
                {
                    query.Append(" SELECT ID_PRODUTO, NOME, SITUACAO, PRECO, TIPO, CUSTO, FALSE ");
                    query.Append(" FROM SEG_PRODUTO WHERE SITUACAO = 'A' ");
                    query.Append(" ORDER BY DESCRICAO ASC ");
                }
                else
                {
                    query.Append(" SELECT ID_PRODUTO, NOME, SITUACAO, PRECO, TIPO, CUSTO, FALSE ");
                    query.Append(" FROM SEG_PRODUTO WHERE SITUACAO = 'A' ");
                    query.Append(" AND DESCRICAO LIKE :VALUE1 ");
                    query.Append(" ORDER BY DESCRICAO ASC ");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = produto.Nome.ToUpper() + "%";

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Produtos");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findProdutoAtivoInGeneral", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_PRODUTO_ID_PRODUTO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Produto insert(Produto produto, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            try
            {
                
                StringBuilder query = new StringBuilder();

                produto.Id = recuperaProximoId(dbConnection);
                
                query.Append(" INSERT INTO SEG_PRODUTO (ID_PRODUTO, NOME, SITUACAO, PRECO, TIPO, CUSTO, PADRAO_CONTRATO, DESCRICAO) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, 'A', :VALUE3, :VALUE4, :VALUE5, :VALUE6, :VALUE7  )");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = produto.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = produto.Nome;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Numeric));
                command.Parameters[2].Value = produto.Preco;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = produto.Tipo;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Numeric));
                command.Parameters[4].Value = produto.Custo;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Boolean));
                command.Parameters[5].Value = produto.PadraoContrato;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Text));
                command.Parameters[6].Value = produto.Descricao;

                command.ExecuteNonQuery();

                return produto;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Produto findProdutoByDescricao(String descricao, NpgsqlConnection dbConnection)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findProdutoByDescricao");

            Produto produtoRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_PRODUTO, NOME, SITUACAO, PRECO, TIPO, CUSTO, PADRAO_CONTRATO, DESCRICAO ");
                query.Append(" FROM SEG_PRODUTO WHERE NOME = :VALUE1 AND SITUACAO = 'A'");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = descricao.ToUpper();

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    produtoRetorno = new Produto(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetDecimal(3), dr.GetString(4),dr.GetDecimal(5),  dr.GetBoolean(6), dr.IsDBNull(7) ? string.Empty : dr.GetString(7));
                }

                return produtoRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findProdutoByDescricao", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public DataSet findProdutoByFilter(Produto produto, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findProdutoByFilter");

            try
            {
                StringBuilder query = new StringBuilder();

                if (produto.isNullForFilter())
                {
                    query.Append(" SELECT ID_PRODUTO, NOME, SITUACAO, PRECO, TIPO, CUSTO, PADRAO_CONTRATO ");
                    query.Append(" FROM SEG_PRODUTO ORDER BY DESCRICAO ASC");
                }
                else
                {
                    query.Append(" SELECT ID_PRODUTO, NOME, SITUACAO, PRECO, TIPO, CUSTO, PADRAO_CONTRATO ");
                    query.Append(" FROM SEG_PRODUTO WHERE TRUE ");
                    

                    if (!String.IsNullOrEmpty(produto.Nome))
                        query.Append(" AND DESCRICAO LIKE :VALUE1 ");

                    if (!String.IsNullOrEmpty(produto.Situacao))
                        query.Append ( " AND SITUACAO = :VALUE2 ");

                    query.Append(" ORDER BY DESCRICAO ASC ");

                }


                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = produto.Nome + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = produto.Situacao;
                                
                DataSet ds = new DataSet();
                adapter.Fill(ds, "Produtos");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findProdutoByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Produto updateProduto(Produto produto, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateProduto");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_PRODUTO SET NOME = :VALUE2, PRECO = :VALUE3, CUSTO = :VALUE4 , SITUACAO = :VALUE5, PADRAO_CONTRATO = :VALUE6, DESCRICAO = :VALUE7 ");
                query.Append(" WHERE ID_PRODUTO = :VALUE1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = produto.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = produto.Nome;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Numeric));
                command.Parameters[2].Value = produto.Preco;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Numeric));
                command.Parameters[3].Value = produto.Custo;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = produto.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Boolean));
                command.Parameters[5].Value = produto.PadraoContrato;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Text));
                command.Parameters[6].Value = produto.Descricao;

                command.ExecuteNonQuery();

                return produto;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateProduto", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Produto findProdutoById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findProdutoById");

            Produto produtoRetorno = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID_PRODUTO, NOME, SITUACAO, PRECO, TIPO, CUSTO, PADRAO_CONTRATO, DESCRICAO ");
                query.Append(" FROM SEG_PRODUTO WHERE ID_PRODUTO = :VALUE1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    produtoRetorno = new Produto(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetDecimal(3), dr.GetString(4), dr.GetDecimal(5), dr.GetBoolean(6), dr.IsDBNull(7) ? string.Empty : dr.GetString(7));
                }

                return produtoRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findProdutoById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean verificaPodeExcluirProduto(Produto produto, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaPodeExcluirProduto");

            Boolean retorno = Convert.ToBoolean(false);

            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" SELECT ID_PRODUTO FROM SEG_PRODUTO_CONTRATO WHERE ID_PRODUTO = :VALUE1 ", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = produto.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    retorno = true;
                }

                return retorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaPodeExcluirProduto", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findProdutoByTipoEstudo(String tipo_estudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findProdutoByTipo");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID_PRODUTO, NOME, SITUACAO, PRECO, TIPO, CUSTO, TIPO_ESTUDO ");
                query.Append(" FROM SEG_PRODUTO WHERE SITUACAO = 'A' AND TIPO_ESTUDO = :VALUE1 ");
                query.Append(" ORDER BY DESCRICAO ASC ");
                
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = tipo_estudo;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Produtos");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findProdutoByTipo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findProdutoAtivoNotInPropostaContrato(List<Produto> produtos, Produto produto, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findProdutoAtivoNotInPropostaContrato");

            try
            {
                StringBuilder query = new StringBuilder();
                Int32 count = 0;

                if (String.IsNullOrEmpty(produto.Nome))
                {

                    query.Append(" SELECT ID_PRODUTO, NOME, SITUACAO, PRECO, TIPO_ESTUDO, CUSTO, FALSE ");
                    query.Append(" FROM SEG_PRODUTO WHERE SITUACAO = 'A' ");

                    if (produtos.Count > 0)
                    {
                        query.Append(" AND ID_PRODUTO NOT IN ( ");

                        foreach (Produto pr in produtos)
                        {

                            query.Append(pr.Id);

                            if (count < produtos.Count - 1)
                            {
                                query.Append(",");
                            }

                            if (count == produtos.Count - 1)
                            {
                                query.Append(")");
                            }
                            count++;
                        }

                    }

                }
                else
                {
                    query.Append(" SELECT ID_PRODUTO, NOME, SITUACAO, PRECO, TIPO_ESTUDO, CUSTO, FALSE ");
                    query.Append(" FROM SEG_PRODUTO WHERE SITUACAO = 'A' AND ");
                    query.Append(" DESCRICAO LIKE :VALUE1 ");

                    if (produtos.Count > 0)
                    {
                        query.Append(" AND ID_PRODUTO NOT IN ( ");

                        foreach (Produto pr in produtos)
                        {

                            query.Append(pr.Id);

                            if (count < produtos.Count - 1)
                            {
                                query.Append(",");
                            }

                            if (count == produtos.Count - 1)
                            {
                                query.Append(")");
                            }
                            count++;
                        }
                    }

                }

                query.Append(" ORDER BY DESCRICAO ASC ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = produto.Nome + "%";

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Produtos");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findProdutoAtivoNotInPropostaContrato", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<Produto> findAllAtivosPadraoContrato(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllPadraoContrato");

            try
            {
                StringBuilder query = new StringBuilder();
                List<Produto> produtos = new List<Produto>();

                query.Append(" SELECT ID_PRODUTO, NOME, SITUACAO, PRECO, TIPO, CUSTO, PADRAO_CONTRATO, DESCRICAO ");
                query.Append(" FROM SEG_PRODUTO WHERE PADRAO_CONTRATO = TRUE ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    produtos.Add(new Produto(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetDecimal(3), dr.GetString(4), dr.GetDecimal(5), dr.GetBoolean(6), dr.IsDBNull(7) ? string.Empty : dr.GetString(7)));
                }

                return produtos;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllPadraoContrato", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
