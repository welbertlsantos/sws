﻿using Npgsql;
using NpgsqlTypes;
using SWS.Excecao;
using SWS.Helper;
using SWS.IDao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS.Dao
{
    class LoginAuditoriaDao : ILoginAuditoriaDao
    {

        public void registrarLoginAuditoria(string login, string ip, Npgsql.NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método registrarLoginAuditoria");
            try
            {
                Int64 idLoginAuditoria = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" insert into seg_login_auditoria (id_login_auditoria, login, ip, data_alteracao, situacao) values (:value1, :value2, :value3, :value4, :value5)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value2", NpgsqlDbType.Varchar));
                command.Parameters.Add(new NpgsqlParameter("value3", NpgsqlDbType.Varchar));
                command.Parameters.Add(new NpgsqlParameter("value4", NpgsqlDbType.Timestamp));
                command.Parameters.Add(new NpgsqlParameter("value5", NpgsqlDbType.Varchar));

                command.Parameters[0].Value = idLoginAuditoria;
                command.Parameters[1].Value = login;
                command.Parameters[2].Value = ip;
                command.Parameters[3].Value = DateTime.Now;
                command.Parameters[4].Value = "A";

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - registrarLoginAuditoria", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void desativarLoginAuditoria(String login, String ip, Npgsql.NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método desativarLoginAuditoria");
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" update seg_login_auditoria set ");
                query.Append(" situacao = :value3 ");
                query.Append(" where login = :value2 ");
                query.Append(" and ip <> :value1 ");
                query.Append(" and situacao = :value4 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = ip;

                command.Parameters.Add(new NpgsqlParameter("value2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = login;

                command.Parameters.Add(new NpgsqlParameter("value3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = "I";

                command.Parameters.Add(new NpgsqlParameter("value4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = "A";

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - desativarLoginAuditoria", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public bool isLoginAtivo(String login, String ip, Npgsql.NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método isLoginAtivo");

            bool retorno = false;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT * FROM SEG_LOGIN_AUDITORIA  ");
                query.Append(" WHERE LOGIN = :VALUE1  ");
                query.Append(" AND IP = :VALUE2  ");
                query.Append(" AND SITUACAO = :VALUE3  ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = login;
                command.Parameters[1].Value = ip;
                command.Parameters[2].Value = 'A';

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    retorno = true;
                }

                return retorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - isLoginAtivo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        private Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("select nextval(('seg_login_auditoria_id_login_auditoria_seq'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
