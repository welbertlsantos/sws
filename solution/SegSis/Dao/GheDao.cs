﻿using SWS.Entidade;
using Npgsql;
using SWS.Excecao;
using System;
using NpgsqlTypes;
using System.Data;
using System.Text;
using SWS.IDao;
using SWS.View.Resources;
using System.Collections.Generic;
using SWS.Helper;


namespace SWS.Dao
{
    class GheDao : IGheDao
    {
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_GHE_ID_GHE_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoIdGhe", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Ghe insert(Ghe ghe, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            StringBuilder query = new StringBuilder();

            try
            {
                ghe.Id = recuperaProximoId(dbConnection);

                query.Append(" INSERT INTO SEG_GHE ( ID_GHE, ID_ESTUDO, DESCRICAO, NEXP, SITUACAO, NUMERO_PO ) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, 'A', :VALUE5)");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = ghe.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = ghe.Estudo.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = ghe.Descricao.ToUpper();
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = ghe.Nexp;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = ghe.NumeroPo;
                                
                command.ExecuteNonQuery();

                return ghe;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public DataSet findAtivoByEstudo(Estudo estudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtivoByEstudo");

            try
            {
                StringBuilder query = new StringBuilder();
                
                query.Append(" SELECT GHE.ID_GHE, GHE.ID_ESTUDO, GHE.DESCRICAO, GHE.NEXP, GHE.NUMERO_PO FROM SEG_GHE GHE WHERE ");
                query.Append(" GHE.ID_ESTUDO = :VALUE1 AND GHE.SITUACAO = 'A'");
                query.Append(" ORDER BY GHE.DESCRICAO ASC");
                
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = estudo.Id;
                                
                DataSet dsGhe = new DataSet();

                adapter.Fill(dsGhe, "Ghes");

                return dsGhe;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtivoByEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
            
        }

        public HashSet<Ghe> findAllByEstudo(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllByEstudo");
            
            HashSet<Ghe> listaGhe = new HashSet<Ghe>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID_GHE, ID_ESTUDO, DESCRICAO, NEXP, SITUACAO, NUMERO_PO FROM SEG_GHE WHERE ID_ESTUDO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    listaGhe.Add(new Ghe(dr.GetInt64(0), new Estudo(dr.GetInt64(1)), dr.GetString(2), dr.GetInt32(3), dr.GetString(4), dr.IsDBNull(5) ? string.Empty : dr.GetString(5)));
                }

                return listaGhe;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllByEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Ghe findById(Int64? id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            Ghe gheRetorno = null;

            HashSet<Nota> listaNorma = new HashSet<Nota>();
            HashSet<Setor> listaSetor = new HashSet<Setor>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID_GHE, ID_ESTUDO, DESCRICAO, NEXP, SITUACAO, NUMERO_PO FROM SEG_GHE WHERE ID_GHE = :VALUE1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gheRetorno = new Ghe(dr.GetInt64(0), dr.IsDBNull(1) ? null : new Estudo(dr.GetInt64(1)), dr.GetString(2), dr.GetInt32(3), dr.IsDBNull(4) ? string.Empty : dr.GetString(4), dr.IsDBNull(5) ? string.Empty : dr.GetString(5));
                }

                // preenchendo a colecao de normas no GHE
                query.Clear();
                query.Append(" SELECT NORMA.ID_NORMA, NORMA.DESCRICAO, NORMA.SITUACAO, NORMA.CONTEUDO FROM SEG_NORMA NORMA ");
                query.Append(" JOIN SEG_NORMA_GHE NORMA_GHE ON NORMA_GHE.ID_NORMA = NORMA.ID_NORMA ");
                query.Append(" JOIN SEG_GHE GHE ON GHE.ID_GHE = NORMA_GHE.ID_GHE ");
                query.Append(" WHERE GHE.ID_GHE = :VALUE1 ");
                
                NpgsqlCommand commandNorma = new NpgsqlCommand(query.ToString(), dbConnection);

                commandNorma.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                commandNorma.Parameters[0].Value = id;

                NpgsqlDataReader drNorma = commandNorma.ExecuteReader();

                while (drNorma.Read())
                {
                    listaNorma.Add(new Nota(drNorma.GetInt64(0), drNorma.GetString(1), drNorma.GetString(2), drNorma.GetString(3)));
                }

                gheRetorno.Norma = listaNorma;

                return gheRetorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public Ghe update(Ghe ghe, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_GHE SET DESCRICAO = :VALUE1, NEXP = :VALUE4, SITUACAO = :VALUE5, NUMERO_PO = :VALUE6 ");
                query.Append(" WHERE ID_GHE = :VALUE2 AND ID_ESTUDO = :VALUE3 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = ghe.Descricao.ToUpper();
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = ghe.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = ghe.Estudo.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = ghe.Nexp;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = ghe.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = ghe.NumeroPo;

                command.ExecuteNonQuery();

                return ghe;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void deleteByEstudo(Int64 id, Estudo estudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" DELETE FROM SEG_GHE WHERE ID_GHE = :VALUE1 AND ID_ESTUDO = :VALUE2 ", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = estudo.Id;
                
                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void delete(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" DELETE FROM SEG_GHE WHERE ID_GHE = :VALUE1 ", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                command.ExecuteNonQuery();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Ghe findByDescricao(Ghe ghe, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByDescricao");

            Ghe gheRetorno = null;
            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" SELECT ID_GHE, DESCRICAO, NEXP, SITUACAO, NUMERO_PO FROM SEG_GHE WHERE ID_ESTUDO = :VALUE1 AND DESCRICAO LIKE :VALUE2 AND SITUACAO = 'A' ", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = ghe.Estudo.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = ghe.Descricao;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gheRetorno = new Ghe(dr.GetInt64(0), null, dr.GetString(1), dr.GetInt32(2), dr.GetString(3), dr.IsDBNull(4) ? string.Empty : dr.GetString(4));

                }
                return gheRetorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByDescricao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 FindNtotalExpostoByEstudo(Estudo estudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando FindNtotalExpostoByGhe FindNtotalExpostoByEstudo");

            Int64 nTotalExposto = 0;
            
            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" SELECT SUM(NEXP) FROM SEG_GHE WHERE ID_ESTUDO = :VALUE1", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = estudo.Id;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    if (!dr.IsDBNull(0))
                    {
                        nTotalExposto = dr.GetInt64(0);
                    }

                }

                return nTotalExposto;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - FindNtotalExpostoByEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int32 buscaQuantidadeByEstudo(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método buscaQuantidadeByEstudo");

            Int32 quatidade = 0;

            try
            {
                StringBuilder query = new StringBuilder();

                NpgsqlCommand command = new NpgsqlCommand(" SELECT CAST(COUNT(*) AS INTEGER) FROM SEG_GHE WHERE ID_ESTUDO = :VALUE1", dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    quatidade = dr.GetInt32(0);
                }

                return quatidade;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - buscaQuantidadeByEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int32 buscaQuantidadeRiscoInGhe(Ghe ghe, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método buscaQuantidadeRiscoInGhe");

            Int32 quantidadeRisco = 0;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT CAST(COUNT(*) AS INTEGER)");
                query.Append(" FROM SEG_GHE GHE");
                query.Append(" LEFT JOIN SEG_GHE_FONTE GHE_FONTE ON GHE.ID_GHE = GHE_FONTE.ID_GHE AND GHE_FONTE.PRINCIPAL <> 't'");
                query.Append(" LEFT JOIN SEG_FONTE FONTE ON FONTE.ID_FONTE = GHE_FONTE.ID_FONTE ");
                query.Append(" LEFT JOIN SEG_GHE_FONTE_AGENTE GHE_FONTE_AGENTE ON GHE_FONTE_AGENTE.ID_GHE_FONTE = GHE_FONTE.ID_GHE_FONTE ");
                query.Append(" LEFT JOIN SEG_AGENTE AGENTE ON AGENTE.ID_AGENTE = GHE_FONTE_AGENTE.ID_AGENTE");
                query.Append(" LEFT JOIN SEG_GRAD_SOMA GRAD_SOMA ON GHE_FONTE_AGENTE.ID_GRAD_SOMA = GRAD_SOMA.ID_GRAD_SOMA");
                query.Append(" LEFT JOIN SEG_GRAD_EFEITO GRAD_EFEITO ON GHE_FONTE_AGENTE.ID_GRAD_EFEITO = GRAD_EFEITO.ID_GRAD_EFEITO ");
                query.Append(" LEFT JOIN SEG_GRAD_EXPOSICAO GRAD_EXPOSICAO ON GHE_FONTE_AGENTE.ID_GRAD_EXPOSICAO = GRAD_EXPOSICAO.ID_GRAD_EXPOSICAO ");
                query.Append(" LEFT JOIN SEG_GHE_FONTE_AGENTE_EPI GHE_FONTE_AGENTE_EPI ON GHE_FONTE_AGENTE_EPI.ID_GHE_FONTE_AGENTE = GHE_FONTE_AGENTE.ID_GHE_FONTE_AGENTE AND GHE_FONTE_AGENTE_EPI.CLASSIFICACAO = 'MC'");
                query.Append(" LEFT JOIN SEG_EPI EPI ON EPI.ID_EPI = GHE_FONTE_AGENTE_EPI.ID_EPI");
                query.Append(" WHERE GHE.ID_GHE = :VALUE1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = ghe.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    quantidadeRisco = dr.GetInt32(0);
                }

                return quantidadeRisco;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - buscaQuantidadeRiscoInGhe", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int32 buscaQuantidadeNotas(Ghe ghe, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método buscaQuantidadeNotas");

            Int32 quantidadeNotas = 0;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT CAST(COUNT(*) AS INTEGER)");
                query.Append(" FROM SEG_NORMA_GHE");
                query.Append(" WHERE ID_GHE = :VALUE1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = ghe.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    quantidadeNotas = dr.GetInt32(0);
                }

                return quantidadeNotas;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - buscaQuantidadeNotas", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<Ghe> findAtivoInEstudoByClienteFuncao(ClienteFuncao clienteFuncao, Estudo estudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtivoInEstudoByClienteFuncao");

            List<Ghe> listaGhe = new List<Ghe>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GHE.ID_GHE, GHE.ID_ESTUDO, GHE.DESCRICAO, GHE.NEXP, GHE.SITUACAO, GHE.NUMERO_PO FROM SEG_GHE GHE ");
                query.Append(" JOIN SEG_GHE_SETOR GHE_SETOR ON GHE_SETOR.ID_GHE = GHE.ID_GHE AND GHE_SETOR.SITUACAO = 'A'  ");
                query.Append(" JOIN SEG_GHE_SETOR_CLIENTE_FUNCAO GHE_SETOR_CLIENTE_FUNCAO ON GHE_SETOR_CLIENTE_FUNCAO.ID_GHE_SETOR = GHE_SETOR.ID_GHE_SETOR AND GHE_SETOR_CLIENTE_FUNCAO.SITUACAO = 'A'  ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO CLIENTE_FUNCAO ON CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = GHE_SETOR_CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO ");
                query.Append(" WHERE GHE.ID_ESTUDO = :VALUE1 AND ");
                query.Append(" CLIENTE_FUNCAO.ID_SEG_CLIENTE_FUNCAO = :VALUE2 AND ");
                query.Append(" GHE.SITUACAO = 'A' ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = estudo.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = clienteFuncao.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    listaGhe.Add(new Ghe(dr.GetInt64(0), estudo, dr.GetString(2), dr.GetInt32(3), dr.GetString(4), dr.IsDBNull(5) ? string.Empty : dr.GetString(5)));

                }

                return listaGhe;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtivoInEstudoByClienteFuncao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean isGheUsed(Ghe ghe, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método isGheUsed");

            Boolean result = false;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT * ");
                query.Append(" FROM SEG_ASO_GHE_SETOR   ");
                query.Append(" WHERE ID_GHE = :VALUE1   ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = ghe.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    result = true;
                }

                return result;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - isGheUsed", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

    }
}
