﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using System.Data;
using Npgsql;
using SWS.Excecao;
using SWS.Entidade;
using NpgsqlTypes;
using SWS.Helper;


namespace SWS.Dao
{
    class GradExposicaoDao : IGradExposicaoDao
    {

        public DataSet findAllGradExposicao(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllGradExposicao");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_GRAD_EXPOSICAO, DESCRICAO, CATEGORIA, VALOR FROM SEG_GRAD_EXPOSICAO ORDER BY ID_GRAD_EXPOSICAO");
                
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                DataSet ds = new DataSet();
                adapter.Fill(ds, "Exposicoes");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllGradExposicao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public GradExposicao findGradExposicaoByGradExposicao(GradExposicao gradExposicao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findGradExposicaoByGradExposicao");

            GradExposicao gradExposicaoRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" select id_grad_exposicao, descricao, categoria, valor from seg_grad_exposicao where id_grad_exposicao = :value1");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                if (gradExposicao != null)
                {
                    command.Parameters[0].Value = gradExposicao.Id;
                }
                else
                {
                    command.Parameters[0].Value = null;
                }

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gradExposicaoRetorno = new GradExposicao(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetInt64(3));

                }

                return gradExposicaoRetorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findGradExposicaoByGradExposicao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
