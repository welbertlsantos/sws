﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;
using SWS.Entidade;
using NpgsqlTypes;
using System.Data;
using SWS.View.Resources;

namespace SWS.Dao
{
    class ComentarioFuncaoDao :IComentarioFuncaoDao
    {
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_COMENTARIO_FUNCAO_ID_COMENTARIO_FUNCAO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoIdAgente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public ComentarioFuncao insert(ComentarioFuncao comentarioFuncao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            try
            {
                comentarioFuncao.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_COMENTARIO_FUNCAO( ID_COMENTARIO_FUNCAO, ID_FUNCAO, ATIVIDADE, SITUACAO )");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, 'A')");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = comentarioFuncao.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = comentarioFuncao.Funcao.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Text));
                command.Parameters[2].Value = comentarioFuncao.Atividade.ToUpper();

                command.ExecuteNonQuery();

                return comentarioFuncao;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<ComentarioFuncao> findAtivosByFuncao(Funcao funcao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtivosByFuncao");

            List<ComentarioFuncao> comentarios = new List<ComentarioFuncao>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID_COMENTARIO_FUNCAO, ID_FUNCAO, ATIVIDADE, SITUACAO ");
                query.Append(" FROM SEG_COMENTARIO_FUNCAO ");
                query.Append(" WHERE ID_FUNCAO = :VALUE1 AND SITUACAO = 'A' ");
                query.Append(" ORDER BY ATIVIDADE ASC ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = funcao.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    comentarios.Add(new ComentarioFuncao(dr.GetInt64(0), funcao, dr.GetString(2), dr.GetString(3))); 
                }

                return comentarios;
                

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtivosByFuncao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void update(ComentarioFuncao comentarioFuncao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_COMENTARIO_FUNCAO SET SITUACAO = :VALUE2, ATIVIDADE = :VALUE3 WHERE ID_COMENTARIO_FUNCAO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = comentarioFuncao.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = comentarioFuncao.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Text));
                command.Parameters[2].Value = comentarioFuncao.Atividade.ToUpper();

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public ComentarioFuncao findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            ComentarioFuncao comentarioFuncao = null;

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT COMENTARIO_FUNCAO.ID_COMENTARIO_FUNCAO, COMENTARIO_FUNCAO.ID_FUNCAO, COMENTARIO_FUNCAO.ATIVIDADE, COMENTARIO_FUNCAO.SITUACAO, FUNCAO.DESCRICAO, FUNCAO.COD_CBO, FUNCAO.SITUACAO, FUNCAO.COMENTARIO ");
                query.Append(" FROM SEG_COMENTARIO_FUNCAO COMENTARIO_FUNCAO  ");
                query.Append(" JOIN SEG_FUNCAO FUNCAO ON FUNCAO.ID_FUNCAO = COMENTARIO_FUNCAO.ID_FUNCAO  ");
                query.Append(" WHERE COMENTARIO_FUNCAO.ID_COMENTARIO_FUNCAO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    comentarioFuncao = new ComentarioFuncao(dr.GetInt64(0),
                        dr.IsDBNull(1) ? null : new Funcao(dr.GetInt64(1), dr.GetString(2), dr.IsDBNull(3) ? String.Empty : dr.GetString(3), dr.GetString(4), dr.IsDBNull(5) ? String.Empty : dr.GetString(5)),
                        dr.IsDBNull(2) ? String.Empty : dr.GetString(2),
                        dr.GetString(3));
                }

                return comentarioFuncao;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
