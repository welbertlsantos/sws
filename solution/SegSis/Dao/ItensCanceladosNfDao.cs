﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;
using SWS.Entidade;
using NpgsqlTypes;

namespace SWS.Dao
{
    class ItensCanceladosNfDao :IItensCanceladosNfDao
    {
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("select nextval(('seg_itens_cancelados_nf_id_itens_cancelados_nf_seq'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void insertMovimentoCancelado(Movimento itensCancelados, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertMovimentoCancelado");

            try
            {
                itensCancelados.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" insert into seg_itens_cancelados_nf ( ");
                query.Append(" id_itens_cancelados_nf, ");
                query.Append(" id_ghe_fonte_agente_exame_aso, ");
                query.Append(" id_cliente_funcao_exame_aso, ");
                query.Append(" id_cliente, ");
                query.Append(" id_produto_contrato, ");
                query.Append(" id_nfe, ");
                query.Append(" dt_inclusao, ");
                query.Append(" dt_faturamento, ");
                query.Append(" situacao, "); 
                query.Append(" prc_unit, ");
                query.Append(" com_valor, ");
                query.Append(" id_estudo, ");
                query.Append(" id_aso, ");
                query.Append(" quantidade, ");
                query.Append(" usuario, ");
                query.Append(" id_usuario ) ");
                query.Append(" values (:value1, :value2, :value3, :value4, :value5, :value6, :value7, ");
                query.Append(" :value8, :value9, :value10, :value11, :value12, :value13, :value14, :value15, :value16) ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer)); 
                command.Parameters.Add(new NpgsqlParameter("value2", NpgsqlDbType.Integer)); 
                command.Parameters.Add(new NpgsqlParameter("value3", NpgsqlDbType.Integer)); 
                command.Parameters.Add(new NpgsqlParameter("value4", NpgsqlDbType.Integer)); 
                command.Parameters.Add(new NpgsqlParameter("value5", NpgsqlDbType.Integer)); 
                command.Parameters.Add(new NpgsqlParameter("value6", NpgsqlDbType.Integer));  
                command.Parameters.Add(new NpgsqlParameter("value7", NpgsqlDbType.Date)); 
                command.Parameters.Add(new NpgsqlParameter("value8", NpgsqlDbType.Date)); 
                command.Parameters.Add(new NpgsqlParameter("value9", NpgsqlDbType.Varchar)); 
                command.Parameters.Add(new NpgsqlParameter("value10", NpgsqlDbType.Numeric)); 
                command.Parameters.Add(new NpgsqlParameter("value11", NpgsqlDbType.Numeric)); 
                command.Parameters.Add(new NpgsqlParameter("value12", NpgsqlDbType.Integer)); 
                command.Parameters.Add(new NpgsqlParameter("value13", NpgsqlDbType.Integer)); 
                command.Parameters.Add(new NpgsqlParameter("value14", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value15", NpgsqlDbType.Varchar));
                command.Parameters.Add(new NpgsqlParameter("value16", NpgsqlDbType.Integer));

                command.Parameters[0].Value = itensCancelados.Id;
                command.Parameters[1].Value = itensCancelados.GheFonteAgenteExameAso != null ? itensCancelados.GheFonteAgenteExameAso.Id : null;
                command.Parameters[2].Value = itensCancelados.ClienteFuncaoExameAso != null ? itensCancelados.ClienteFuncaoExameAso.Id : null;
                command.Parameters[3].Value = itensCancelados.Cliente.Id;
                command.Parameters[4].Value = itensCancelados.ContratoProduto != null ? itensCancelados.ContratoProduto.Id : null;
                command.Parameters[5].Value = itensCancelados.Nfe != null ? itensCancelados.Nfe.Id : null;
                command.Parameters[6].Value = itensCancelados.DataInclusao;
                command.Parameters[7].Value = itensCancelados.DataFaturamento;
                command.Parameters[8].Value = itensCancelados.Situacao;
                command.Parameters[9].Value = itensCancelados.PrecoUnit;
                command.Parameters[10].Value = itensCancelados.ValorComissao != null ? itensCancelados.ValorComissao : null;
                command.Parameters[11].Value = itensCancelados.Estudo != null ? itensCancelados.Estudo.Id : null;
                command.Parameters[12].Value = itensCancelados.Aso != null ? itensCancelados.Aso.Id : null;
                command.Parameters[13].Value = itensCancelados.Quantidade;
                command.Parameters[14].Value = itensCancelados.Usuario.Login;
                command.Parameters[15].Value = itensCancelados.Usuario.Id;
                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertMovimentoCancelado", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }
    }
}
