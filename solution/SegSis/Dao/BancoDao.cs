﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;
using SWS.Entidade;
using NpgsqlTypes;
using System.Data;

namespace SWS.Dao
{
    class BancoDao :IBancoDao
    {
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_BANCO_ID_BANCO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public Banco insert(Banco banco, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            try
            {
                banco.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_BANCO( ID_BANCO, NOME, CODIGO_BANCO, CAIXA, SITUACAO, PRIVADO, BOLETO) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, 'A', :VALUE5, :VALUE6 )");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = banco.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = banco.Nome;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = banco.CodigoBanco;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Boolean));
                command.Parameters[3].Value = banco.Caixa;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Boolean));
                command.Parameters[4].Value = banco.Privado;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Boolean));
                command.Parameters[5].Value = banco.Boleto;

                command.ExecuteNonQuery();

                return banco;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Banco update(Banco banco, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_BANCO SET ");
                query.Append(" NOME = :VALUE2, ");
                query.Append(" CODIGO_BANCO = :VALUE3, ");
                query.Append(" CAIXA = :VALUE4, ");
                query.Append(" SITUACAO = :VALUE5, ");
                query.Append(" BOLETO = :VALUE6 ");
                query.Append(" WHERE ID_BANCO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = banco.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = banco.Nome;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = banco.CodigoBanco;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Boolean));
                command.Parameters[3].Value = banco.Caixa;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = banco.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Boolean));
                command.Parameters[5].Value = banco.Boleto;

                command.ExecuteNonQuery();

                return banco;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Banco findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            Banco banco = null;
            
            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID_BANCO, NOME, CODIGO_BANCO, CAIXA, SITUACAO, PRIVADO, BOLETO ");
                query.Append(" FROM SEG_BANCO WHERE ID_BANCO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    banco = new Banco(dr.GetInt64(0), dr.GetString(1), dr.IsDBNull(2) ? string.Empty : dr.GetString(2), dr.GetBoolean(3), dr.GetString(4), dr.GetBoolean(5), dr.GetBoolean(6));
                }

                return banco;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public DataSet findByFilter(Banco banco, Boolean? caixa, Boolean? boleto, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findBancoByFilter");
            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT ID_BANCO, NOME, CODIGO_BANCO, CAIXA, SITUACAO, PRIVADO, BOLETO FROM SEG_BANCO ");
                query.Append(" WHERE TRUE ");

                if (banco.Nome != null && !String.IsNullOrEmpty(banco.Nome))
                    query.Append(" AND NOME LIKE :VALUE1 ");

                if (!String.IsNullOrEmpty(banco.Situacao))
                    query.Append(" AND SITUACAO = :VALUE2");

                if (caixa != null)
                    query.Append(" AND CAIXA = :VALUE3");

                if (boleto != null)
                    query.Append(" AND BOLETO = :VALUE4");
                
                query.Append(" ORDER BY CODIGO_BANCO ASC");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = string.IsNullOrEmpty(banco.Nome) ? string.Empty : "%" + banco.Nome + "%"; 
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = string.IsNullOrEmpty(banco.Situacao) ? string.Empty : banco.Situacao;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Boolean));
                adapter.SelectCommand.Parameters[2].Value = caixa != null ? caixa : null;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Boolean));
                adapter.SelectCommand.Parameters[3].Value = boleto != null ? boleto : null;


                DataSet ds = new DataSet();
                adapter.Fill(ds, "Bancos");
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Banco findByNome(String nome, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findBancoByNome");

            try
            {
                StringBuilder query = new StringBuilder();

                Banco banco = null;

                query.Append(" SELECT ID_BANCO, NOME, CODIGO_BANCO, CAIXA, SITUACAO, PRIVADO, BOLETO FROM SEG_BANCO ");
                query.Append(" WHERE NOME = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = nome;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    banco = new Banco(dr.GetInt64(0), dr.GetString(1), dr.IsDBNull(2) ? string.Empty : dr.GetString(2), dr.GetBoolean(3), dr.GetString(4), dr.GetBoolean(5), dr.GetBoolean(6));
                }

                return banco;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByNome", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Banco findByCodigo(String codigo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByCodigo");

            try
            {
                StringBuilder query = new StringBuilder();

                Banco banco = null;

                query.Append(" SELECT ID_BANCO, NOME, CODIGO_BANCO, CAIXA, SITUACAO, PRIVADO, BOLETO FROM SEG_BANCO ");
                query.Append(" WHERE CODIGO_BANCO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = codigo;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    banco = new Banco(dr.GetInt64(0), dr.GetString(1), dr.IsDBNull(2) ? string.Empty : dr.GetString(2), dr.GetBoolean(3), dr.GetString(4), dr.GetBoolean(5), dr.GetBoolean(6));
                }

                return banco;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByCodigo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

    }
}
