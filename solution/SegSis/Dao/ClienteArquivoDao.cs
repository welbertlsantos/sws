﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using Npgsql;
using SWS.Helper;
using NpgsqlTypes;
using SWS.Excecao;
using SWS.View.Resources;

namespace SWS.Dao
{
    class ClienteArquivoDao : IClienteArquivoDao
    {
        public ClienteArquivo insertClienteArquivo(ClienteArquivo clienteArquivo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertClienteArquivo");

            try
            {
                clienteArquivo.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_CLIENTE_ARQUIVO (ID_CLIENTE_ARQUIVO, ID_ARQUIVO, ID_CLIENTE, DATA_CRIACAO, ");
                query.Append(" DATA_FINALIZACAO, SITUACAO) VALUES (:VALUE1, :VALUE2, :VALUE3, NOW(), :VALUE4, :VALUE5)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteArquivo.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = clienteArquivo.Arquivo.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = clienteArquivo.Cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Timestamp));
                command.Parameters[3].Value = clienteArquivo.DataFinalizacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = clienteArquivo.Situacao;
                
                command.ExecuteNonQuery();

                return clienteArquivo;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertClienteArquivo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public ClienteArquivo findClienteArquivoById(Int64 id, NpgsqlConnection dbConnection)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteArquivoById");

            ClienteArquivo clienteArquivoRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_CLIENTE_ARQUIVO, ID_ARQUIVO, ID_CLIENTE, DATA_CRIACAO, DATA_FINALIZACAO, ");
                query.Append(" SITUACAO FROM SEG_CLIENTE_ARQUIVO WHERE ID_CLIENTE_ARQUIVO = :VALUE1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    clienteArquivoRetorno = new ClienteArquivo(dr.GetInt64(0), new Arquivo(dr.GetInt64(1)),
                        new Cliente(dr.GetInt64(2)), dr.GetDateTime(3), dr.GetDateTime(4), dr.GetString(5));
                }

                return clienteArquivoRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteArquivoById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public ClienteArquivo findClienteArquivoByCliente(Cliente cliente, NpgsqlConnection dbConnection)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteArquivoByCliente");

            ClienteArquivo clienteArquivoRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_CLIENTE_ARQUIVO, ID_ARQUIVO, ID_CLIENTE, DATA_CRIACAO, DATA_FINALIZACAO, ");
                query.Append(" SITUACAO FROM SEG_CLIENTE_ARQUIVO WHERE ID_CLIENTE = :VALUE1 AND SITUACAO = :VALUE2 ");
                                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = ApplicationConstants.ATIVO;

                NpgsqlDataReader dr = command.ExecuteReader();
                DateTime? dataFinalizacao = null;

                while (dr.Read())
                {
                    if (!dr.IsDBNull(4)){
                        dataFinalizacao = dr.GetDateTime(4);
                    }
                    clienteArquivoRetorno = new ClienteArquivo(dr.GetInt64(0), new Arquivo(dr.GetInt64(1)), new Cliente(dr.GetInt64(2)), dr.GetDateTime(3), dataFinalizacao, dr.GetString(5));
                }

                return clienteArquivoRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteArquivoByCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void finalizaClienteArquivo(ClienteArquivo clienteArquivo, NpgsqlConnection dbConnection)
        {

            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método finalizaClienteArquivo");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("UPDATE SEG_CLIENTE_ARQUIVO SET DATA_FINALIZACAO = NOW(), SITUACAO = :VALUE1 WHERE ID_CLIENTE_ARQUIVO = :VALUE2", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = ApplicationConstants.DESATIVADO;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = clienteArquivo.Id;

                NpgsqlDataReader dr = command.ExecuteReader();
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - finalizaClienteArquivo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        private Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_CLIENTE_ARQUIVO_ID_CLIENTE_ARQUIVO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
