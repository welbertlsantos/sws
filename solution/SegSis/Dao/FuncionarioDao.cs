﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using NpgsqlTypes;
using Npgsql;
using SWS.Entidade;
using SWS.Excecao;
using System.Data;
using SWS.View.Resources;
using SWS.Helper;

namespace SWS.Dao
{
    class FuncionarioDao :IFuncionarioDao
    {
        public DataSet findFuncionarioByFilter(Funcionario funcionario, ClienteFuncao clienteFuncao, Cliente cliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFuncionarioByFilter");

            try
            {
                StringBuilder query = new StringBuilder();

                if (funcionario.isNullForFilter() && clienteFuncao == null && cliente == null)
                {
                    query.Append(" SELECT DISTINCT FUNCIONARIO.ID_FUNCIONARIO, FUNCIONARIO.NOME, FORMATA_CPF(FUNCIONARIO.CPF), FUNCIONARIO.RG,  ");
                    query.Append(" FUNCIONARIO.ENDERECO, FUNCIONARIO.NUMERO, FUNCIONARIO.COMPLEMENTO, FUNCIONARIO.BAIRRO, CIDADE.NOME AS CIDADE, FUNCIONARIO.UF, FUNCIONARIO.TELEFONE1, ");
                    query.Append(" FUNCIONARIO.TELEFONE2, FUNCIONARIO.EMAIL, FUNCIONARIO.TIPOSAN, FUNCIONARIO.FTRH, FUNCIONARIO.DATA_NASCIMENTO, FUNCIONARIO.PESO, FUNCIONARIO.ALTURA, ");
                    query.Append(" FUNCIONARIO.SITUACAO, FUNCIONARIO.CEP, FUNCIONARIO.SEXO, FUNCIONARIO.CTPS, FUNCIONARIO.PIS_PASEP ");
                    query.Append(" FROM SEG_FUNCIONARIO FUNCIONARIO ");
                    query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = FUNCIONARIO.ID_CIDADE ");
                    query.Append(" ORDER BY FUNCIONARIO.NOME ASC ");
                }
                else
                {
                    query.Append(" SELECT DISTINCT FUNCIONARIO.ID_FUNCIONARIO, FUNCIONARIO.NOME, FORMATA_CPF(FUNCIONARIO.CPF), FUNCIONARIO.RG, ");
                    query.Append(" FUNCIONARIO.ENDERECO, FUNCIONARIO.NUMERO, FUNCIONARIO.COMPLEMENTO, FUNCIONARIO.BAIRRO, CIDADE.NOME AS CIDADE, FUNCIONARIO.UF, FUNCIONARIO.TELEFONE1, ");
                    query.Append(" FUNCIONARIO.TELEFONE2, FUNCIONARIO.EMAIL, FUNCIONARIO.TIPOSAN, FUNCIONARIO.FTRH, FUNCIONARIO.DATA_NASCIMENTO, FUNCIONARIO.PESO, FUNCIONARIO.ALTURA, ");
                    query.Append(" FUNCIONARIO.SITUACAO, FUNCIONARIO.CEP, FUNCIONARIO.SEXO ");
                    query.Append(" FROM SEG_FUNCIONARIO FUNCIONARIO ");
                    query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = FUNCIONARIO.ID_CIDADE ");

                    if (cliente != null && clienteFuncao == null)
                    {
                        query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CFF ON CFF.ID_FUNCIONARIO = FUNCIONARIO.ID_FUNCIONARIO AND CFF.SITUACAO = 'A' ");
                        query.Append(" JOIN SEG_CLIENTE_FUNCAO CF ON CF.ID_SEG_CLIENTE_FUNCAO = CFF.ID_SEG_CLIENTE_FUNCAO AND CF.SITUACAO = 'A' ");
                        query.Append(" JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = CF.ID_CLIENTE AND CLIENTE.ID_CLIENTE = :VALUE5 ");
                    }
                    else if (clienteFuncao != null)
                    {
                        query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CFF ON CFF.ID_FUNCIONARIO = FUNCIONARIO.ID_FUNCIONARIO AND CFF.SITUACAO = 'A' ");
                        query.Append(" JOIN SEG_CLIENTE_FUNCAO CF ON CF.ID_SEG_CLIENTE_FUNCAO = CFF.ID_SEG_CLIENTE_FUNCAO AND CF.ID_SEG_CLIENTE_FUNCAO = :VALUE6 AND CF.SITUACAO = 'A' ");
                    }

                    if (!funcionario.isNullForFilter())
                        query.Append(" WHERE TRUE ");

                    if (!String.IsNullOrWhiteSpace(funcionario.Nome))
                        query.Append(" AND FUNCIONARIO.NOME LIKE :VALUE2 ");
                
                    if (!String.IsNullOrWhiteSpace(funcionario.Cpf))
                        query.Append(" AND FUNCIONARIO.CPF = :VALUE3 ");
                    
                    if (!String.IsNullOrWhiteSpace(funcionario.Rg))
                        query.Append(" AND FUNCIONARIO.RG = :VALUE4 ");

                    if (!string.IsNullOrWhiteSpace(funcionario.Situacao))
                        query.Append(" AND FUNCIONARIO.SITUACAO = :VALUE7 ");

                    query.Append(" ORDER BY FUNCIONARIO.NOME ASC");
                    
                }


                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = ApplicationConstants.ATIVO;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = funcionario.Nome + "%";
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[2].Value = funcionario.Cpf;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[3].Value = funcionario.Rg;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[4].Value = cliente != null ? cliente.Id : null;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[5].Value = clienteFuncao != null ? clienteFuncao.Id : null;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[6].Value = funcionario.Situacao;


                DataSet ds = new DataSet();
                adapter.Fill(ds, "Funcionarios");
                return ds;
            }

            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFuncionarioByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Funcionario findFuncionarioByCPF(String Cpf, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFuncionarioByCPF");
            
            try
            {
                StringBuilder query = new StringBuilder();
             
                Funcionario funcionarioRetorno = null;

                query.Append(" SELECT FUNCIONARIO.ID_FUNCIONARIO, FUNCIONARIO.NOME, FUNCIONARIO.CPF, FUNCIONARIO.RG, FUNCIONARIO.ENDERECO, FUNCIONARIO.NUMERO, FUNCIONARIO.COMPLEMENTO, FUNCIONARIO.BAIRRO, ");
                query.Append(" FUNCIONARIO.ID_CIDADE, FUNCIONARIO.UF, FUNCIONARIO.TELEFONE1, FUNCIONARIO.TELEFONE2, FUNCIONARIO.EMAIL, FUNCIONARIO.TIPOSAN, FUNCIONARIO.FTRH, FUNCIONARIO.DATA_NASCIMENTO, ");
                query.Append(" FUNCIONARIO.PESO, FUNCIONARIO.ALTURA, FUNCIONARIO.SITUACAO, FUNCIONARIO.CEP, FUNCIONARIO.SEXO, FUNCIONARIO.CTPS, FUNCIONARIO.PIS_PASEP, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, FUNCIONARIO.PCD, FUNCIONARIO.ORGAO_EMISSOR, FUNCIONARIO.UF_EMISSOR, FUNCIONARIO.SERIE, FUNCIONARIO.UF_EMISSOR_CTPS ");
                query.Append(" FROM SEG_FUNCIONARIO FUNCIONARIO ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = FUNCIONARIO.ID_CIDADE");
                query.Append(" WHERE TRUE ");
                query.Append(" AND FUNCIONARIO.CPF = :VALUE1 AND FUNCIONARIO.SITUACAO = 'A' ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = Cpf;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    funcionarioRetorno = new Funcionario(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.GetString(4), dr.GetString(5), dr.GetString(6), dr.GetString(7), dr.IsDBNull(8) ? null : new CidadeIbge(dr.GetInt64(8), dr.GetString(23), dr.GetString(24), dr.GetString(25)), dr.GetString(9), dr.GetString(10), dr.GetString(11), dr.GetString(12), dr.GetString(13),dr.GetString(14), dr.GetDateTime(15), dr.GetInt32(16), dr.GetDecimal(17), dr.GetString(18), dr.GetString(19),dr.GetString(20), dr.IsDBNull(21) ? String.Empty : dr.GetString(21), dr.IsDBNull(22) ? String.Empty : dr.GetString(22), dr.GetBoolean(26), dr.IsDBNull(27) ? string.Empty : dr.GetString(27), dr.IsDBNull(28) ? string.Empty : dr.GetString(28), dr.IsDBNull(29) ? string.Empty : dr.GetString(29), dr.IsDBNull(30) ? string.Empty : dr.GetString(30));
                }
                
                return funcionarioRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFuncionarioByCPF", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Funcionario findFuncionarioByNomeRg(String nome, String rg, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFuncionarioByNomeRg");

            try
            {
                StringBuilder query = new StringBuilder();

                Funcionario funcionarioRetorno = null;

                query.Append(" SELECT FUNCIONARIO.ID_FUNCIONARIO, FUNCIONARIO.NOME, FUNCIONARIO.CPF, FUNCIONARIO.RG, FUNCIONARIO.ENDERECO, FUNCIONARIO.NUMERO, FUNCIONARIO.COMPLEMENTO, FUNCIONARIO.BAIRRO, ");
                query.Append(" FUNCIONARIO.ID_CIDADE, FUNCIONARIO.UF, FUNCIONARIO.TELEFONE1, FUNCIONARIO.TELEFONE2, FUNCIONARIO.EMAIL, FUNCIONARIO.TIPOSAN, FUNCIONARIO.FTRH, FUNCIONARIO.DATA_NASCIMENTO, ");
                query.Append(" FUNCIONARIO.PESO, FUNCIONARIO.ALTURA, FUNCIONARIO.SITUACAO, FUNCIONARIO.CEP, FUNCIONARIO.SEXO, FUNCIONARIO.CTPS, FUNCIONARIO.PIS_PASEP, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, FUNCIONARIO.PCD, FUNCIONARIO.ORGAO_EMISSOR, FUNCIONARIO.UF_EMISSOR, FUNCIONARIO.SERIE, FUNCIONARIO.UF_EMISSOR_CTPS ");
                query.Append(" FROM SEG_FUNCIONARIO FUNCIONARIO ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = FUNCIONARIO.ID_CIDADE ");
                query.Append(" WHERE TRUE ");
                query.Append(" AND FUNCIONARIO.RG = :VALUE1 AND FUNCIONARIO.SITUACAO = 'A' AND FUNCIONARIO.NOME = :VALUE2 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = rg;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = String.IsNullOrEmpty(nome) ? null : nome;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    funcionarioRetorno = new Funcionario(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.GetString(4), dr.GetString(5), dr.GetString(6), dr.GetString(7), dr.IsDBNull(8) ? null : new CidadeIbge(dr.GetInt64(8), dr.GetString(23), dr.GetString(24), dr.GetString(25)), dr.GetString(9), dr.GetString(10), dr.GetString(11), dr.GetString(12), dr.GetString(13), dr.GetString(14), dr.GetDateTime(15), dr.GetInt32(16), dr.GetDecimal(17), dr.GetString(18), dr.GetString(19), dr.GetString(20), dr.IsDBNull(21) ? String.Empty : dr.GetString(21), dr.IsDBNull(22) ? String.Empty : dr.GetString(22), dr.GetBoolean(26), dr.IsDBNull(27) ? string.Empty : dr.GetString(27), dr.IsDBNull(28) ? string.Empty : dr.GetString(28), dr.IsDBNull(29) ? string.Empty : dr.GetString(29), dr.IsDBNull(30) ? string.Empty : dr.GetString(30));
                }

                return funcionarioRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFuncionarioByNomeRg", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Funcionario verificaPodeAlterarFuncionario(Funcionario funcionario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaPodeAlterarFuncionario");

            try
            {
                StringBuilder query = new StringBuilder();

                Funcionario funcionarioRetorno = null;

                query.Append(" SELECT FUNCIONARIO.ID_FUNCIONARIO, FUNCIONARIO.NOME, FUNCIONARIO.CPF, FUNCIONARIO.RG, FUNCIONARIO.ENDERECO, FUNCIONARIO.NUMERO, FUNCIONARIO.COMPLEMENTO, FUNCIONARIO.BAIRRO, ");
                query.Append(" FUNCIONARIO.ID_CIDADE, FUNCIONARIO.UF, FUNCIONARIO.TELEFONE1, FUNCIONARIO.TELEFONE2, FUNCIONARIO.EMAIL, FUNCIONARIO.TIPOSAN, FUNCIONARIO.FTRH, FUNCIONARIO.DATA_NASCIMENTO, ");
                query.Append(" FUNCIONARIO.PESO, FUNCIONARIO.ALTURA, FUNCIONARIO.SITUACAO, FUNCIONARIO.CEP, FUNCIONARIO.SEXO, FUNCIONARIO.CTPS, FUNCIONARIO.PIS_PASEP, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, FUNCIONARIO.PCD, FUNCIONARIO.ORGAO_EMISSOR, FUNCIONARIO.UF_EMISSOR, FUNCIONARIO.SERIE, FUNCIONARIO.UF_EMISSOR_CTPS ");
                query.Append(" FROM SEG_FUNCIONARIO FUNCIONARIO ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = FUNCIONARIO.ID_CIDADE ");
                query.Append(" WHERE TRUE ");
                query.Append(" AND FUNCIONARIO.RG = :VALUE1 AND FUNCIONARIO.NOME = :VALUE2 ");
                query.Append(" AND FUNCIONARIO.ID_FUNCIONARIO <> :VALUE3 AND FUNCIONARIO.CPF = :VALUE4 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = funcionario.Rg;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = funcionario.Nome;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = funcionario.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = funcionario.Cpf;
                

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    funcionarioRetorno = new Funcionario(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.GetString(4), dr.GetString(5), dr.GetString(6), dr.GetString(7), dr.IsDBNull(8) ? null :  new CidadeIbge(dr.GetInt64(8), dr.GetString(23), dr.GetString(24), dr.GetString(25)), dr.GetString(9), dr.GetString(10), dr.GetString(11), dr.GetString(12), dr.GetString(13), dr.GetString(14), dr.GetDateTime(15), dr.GetInt32(16), dr.GetDecimal(17), dr.GetString(18), dr.GetString(19), dr.GetString(20), dr.IsDBNull(21) ? String.Empty : dr.GetString(21), dr.IsDBNull(22) ? String.Empty : dr.GetString(22), dr.GetBoolean(26), dr.IsDBNull(27) ? string.Empty : dr.GetString(27), dr.IsDBNull(28) ? string.Empty : dr.GetString(28), dr.IsDBNull(29) ? string.Empty : dr.GetString(29), dr.IsDBNull(30) ? string.Empty : dr.GetString(30));
                }

                return funcionarioRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaPodeAlterarFuncionario", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Funcionario findFuncionarioById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFuncionarioById");

            try
            {
                StringBuilder query = new StringBuilder();
                Funcionario funcionarioRetorno = null;

                query.Append(" SELECT FUNCIONARIO.ID_FUNCIONARIO, FUNCIONARIO.NOME, FUNCIONARIO.CPF, FUNCIONARIO.RG, FUNCIONARIO.ENDERECO, FUNCIONARIO.NUMERO, FUNCIONARIO.COMPLEMENTO, FUNCIONARIO.BAIRRO, ");
                query.Append(" FUNCIONARIO.ID_CIDADE, FUNCIONARIO.UF, FUNCIONARIO.TELEFONE1, FUNCIONARIO.TELEFONE2, FUNCIONARIO.EMAIL, FUNCIONARIO.TIPOSAN, FUNCIONARIO.FTRH, FUNCIONARIO.DATA_NASCIMENTO, ");
                query.Append(" FUNCIONARIO.PESO, FUNCIONARIO.ALTURA, FUNCIONARIO.SITUACAO, FUNCIONARIO.CEP, FUNCIONARIO.SEXO, FUNCIONARIO.CTPS, FUNCIONARIO.PIS_PASEP, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, FUNCIONARIO.PCD, FUNCIONARIO.ORGAO_EMISSOR, FUNCIONARIO.UF_EMISSOR, FUNCIONARIO.SERIE, FUNCIONARIO.UF_EMISSOR_CTPS ");
                query.Append(" FROM SEG_FUNCIONARIO FUNCIONARIO ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = FUNCIONARIO.ID_CIDADE ");
                query.Append(" WHERE TRUE ");
                query.Append(" AND FUNCIONARIO.ID_FUNCIONARIO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    funcionarioRetorno = new Funcionario(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.IsDBNull(4) ? string.Empty : dr.GetString(4), dr.IsDBNull(5) ? string.Empty : dr.GetString(5), dr.IsDBNull(6) ? string.Empty : dr.GetString(6), dr.IsDBNull(7) ? string.Empty : dr.GetString(7), dr.IsDBNull(8) ? null :  new CidadeIbge(dr.GetInt64(8), dr.GetString(23), dr.GetString(24), dr.GetString(25)), dr.GetString(9), dr.IsDBNull(10) ? string.Empty : dr.GetString(10), dr.IsDBNull(11) ? string.Empty : dr.GetString(11), dr.IsDBNull(12) ? string.Empty : dr.GetString(12), dr.IsDBNull(13) ? string.Empty : dr.GetString(13), dr.GetString(14), dr.GetDateTime(15), dr.GetInt32(16), dr.GetDecimal(17), dr.GetString(18), dr.GetString(19), dr.GetString(20), dr.IsDBNull(21) ? string.Empty : dr.GetString(21), dr.IsDBNull(22)? string.Empty : dr.GetString(22), dr.GetBoolean(26), dr.IsDBNull(27) ? string.Empty : dr.GetString(27), dr.IsDBNull(28) ? string.Empty : dr.GetString(28), dr.IsDBNull(29) ? string.Empty : dr.GetString(29), dr.IsDBNull(30) ? string.Empty : dr.GetString(30));
                }

                return funcionarioRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFuncionarioById", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_FUNCIONARIO_ID_FUNCIONARIO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Funcionario insert(Funcionario funcionario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertFuncionario");

            try
            {
                funcionario.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_FUNCIONARIO ( ");
                query.Append(" ID_FUNCIONARIO, NOME, CPF, RG, ENDERECO, NUMERO, COMPLEMENTO, BAIRRO, ID_CIDADE, ");
                query.Append(" UF, TELEFONE1, TELEFONE2, EMAIL, TIPOSAN, FTRH, DATA_NASCIMENTO, PESO, ALTURA, ");
                query.Append(" SITUACAO, CEP, SEXO, CTPS, PIS_PASEP, PCD, ORGAO_EMISSOR, UF_EMISSOR, SERIE, UF_EMISSOR_CTPS) VALUES (:VALUE1, :VALUE2, :VALUE3, ");
                query.Append(" :VALUE4, :VALUE5, :VALUE6, :VALUE7, :VALUE8, :VALUE9, :VALUE10, ");
                query.Append(" :VALUE11, :VALUE12, :VALUE13, :VALUE14, :VALUE15, :VALUE16, :VALUE17, ");
                query.Append(" :VALUE18, :VALUE19, :VALUE20, :VALUE21, :VALUE22, :VALUE23, :VALUE24, :VALUE25, :VALUE26, :VALUE27, :VALUE28) ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = funcionario.Id;
 
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = funcionario.Nome;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = funcionario.Cpf;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = funcionario.Rg;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = funcionario.Endereco;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = funcionario.Numero;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = funcionario.Complemento;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Varchar));
                command.Parameters[7].Value = funcionario.Bairro;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Integer));
                command.Parameters[8].Value = funcionario.Cidade != null ? funcionario.Cidade.Id : null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                command.Parameters[9].Value = funcionario.Uf;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Varchar));
                command.Parameters[10].Value = funcionario.Telefone1;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Varchar));
                command.Parameters[11].Value = funcionario.Telefone2;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Varchar));
                command.Parameters[12].Value = funcionario.Email;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Varchar));
                command.Parameters[13].Value = funcionario.TipoSangue;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Varchar));
                command.Parameters[14].Value = funcionario.FatorRh;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Date));
                command.Parameters[15].Value = funcionario.DataNascimento;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE17", NpgsqlDbType.Integer));
                command.Parameters[16].Value = funcionario.Peso;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE18", NpgsqlDbType.Double));
                command.Parameters[17].Value = funcionario.Altura;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE19", NpgsqlDbType.Varchar));
                command.Parameters[18].Value = funcionario.Situacao;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE20", NpgsqlDbType.Varchar));
                command.Parameters[19].Value = funcionario.Cep;

                command.Parameters.Add(new NpgsqlParameter("VALUE21", NpgsqlDbType.Varchar));
                command.Parameters[20].Value = funcionario.Sexo;

                command.Parameters.Add(new NpgsqlParameter("VALUE22", NpgsqlDbType.Varchar));
                command.Parameters[21].Value = funcionario.Ctps;

                command.Parameters.Add(new NpgsqlParameter("VALUE23", NpgsqlDbType.Varchar));
                command.Parameters[22].Value = funcionario.PisPasep;

                command.Parameters.Add(new NpgsqlParameter("VALUE24", NpgsqlDbType.Boolean));
                command.Parameters[23].Value = funcionario.Pcd;

                command.Parameters.Add(new NpgsqlParameter("VALUE25", NpgsqlDbType.Varchar));
                command.Parameters[24].Value = funcionario.OrgaoEmissor;

                command.Parameters.Add(new NpgsqlParameter("VALUE26", NpgsqlDbType.Varchar));
                command.Parameters[25].Value = funcionario.UfEmissor;

                command.Parameters.Add(new NpgsqlParameter("VALUE27", NpgsqlDbType.Varchar));
                command.Parameters[26].Value = funcionario.Serie;

                command.Parameters.Add(new NpgsqlParameter("VALUE28", NpgsqlDbType.Varchar));
                command.Parameters[27].Value = funcionario.UfEmissorCtps;


                command.ExecuteNonQuery();

                return funcionario;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertFuncionario", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public Funcionario update(Funcionario funcionario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateFuncionario");

            try
            {
                StringBuilder query = new StringBuilder();


                query.Append(" UPDATE SEG_FUNCIONARIO SET ");
                query.Append(" NOME = :VALUE2, CPF = :VALUE3, ENDERECO = :VALUE4, NUMERO = :VALUE5, COMPLEMENTO = :VALUE6, BAIRRO = :VALUE7, ");
                query.Append(" ID_CIDADE = :VALUE8, UF = :VALUE9, TELEFONE1 = :VALUE10, TELEFONE2 = :VALUE11, ");
                query.Append(" EMAIL = :VALUE12, TIPOSAN = :VALUE13, FTRH = :VALUE14, DATA_NASCIMENTO = :VALUE15,  ");
                query.Append(" PESO = :VALUE16, ALTURA = :VALUE17, CEP = :VALUE18, SEXO = :VALUE19, RG = :VALUE20, SITUACAO = :VALUE21, CTPS = :VALUE22, PIS_PASEP = :VALUE23, PCD = :VALUE24, ORGAO_EMISSOR = :VALUE25, UF_EMISSOR = :VALUE26, SERIE = :VALUE27, UF_EMISSOR_CTPS = :VALUE28 ");
                query.Append(" WHERE ID_FUNCIONARIO = :VALUE1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = funcionario.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = funcionario.Nome;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = funcionario.Cpf;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = funcionario.Endereco;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = funcionario.Numero;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = funcionario.Complemento;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = funcionario.Bairro;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Integer));
                command.Parameters[7].Value = funcionario.Cidade != null ? funcionario.Cidade.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                command.Parameters[8].Value = funcionario.Uf;

                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                command.Parameters[9].Value = funcionario.Telefone1;

                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Varchar));
                command.Parameters[10].Value = funcionario.Telefone2;

                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Varchar));
                command.Parameters[11].Value = funcionario.Email;

                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Varchar));
                command.Parameters[12].Value = funcionario.TipoSangue;

                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Varchar));
                command.Parameters[13].Value = funcionario.FatorRh;

                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Date));
                command.Parameters[14].Value = funcionario.DataNascimento;

                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Integer));
                command.Parameters[15].Value = funcionario.Peso;

                command.Parameters.Add(new NpgsqlParameter("VALUE17", NpgsqlDbType.Double));
                command.Parameters[16].Value = funcionario.Altura;

                command.Parameters.Add(new NpgsqlParameter("VALUE18", NpgsqlDbType.Varchar));
                command.Parameters[17].Value = funcionario.Cep;

                command.Parameters.Add(new NpgsqlParameter("VALUE19", NpgsqlDbType.Varchar));
                command.Parameters[18].Value = funcionario.Sexo;

                command.Parameters.Add(new NpgsqlParameter("VALUE20", NpgsqlDbType.Varchar));
                command.Parameters[19].Value = funcionario.Rg;

                command.Parameters.Add(new NpgsqlParameter("VALUE21", NpgsqlDbType.Varchar));
                command.Parameters[20].Value = funcionario.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE22", NpgsqlDbType.Varchar));
                command.Parameters[21].Value = funcionario.Ctps;

                command.Parameters.Add(new NpgsqlParameter("VALUE23", NpgsqlDbType.Varchar));
                command.Parameters[22].Value = funcionario.PisPasep;

                command.Parameters.Add(new NpgsqlParameter("VALUE24", NpgsqlDbType.Boolean));
                command.Parameters[23].Value = funcionario.Pcd;

                command.Parameters.Add(new NpgsqlParameter("VALUE25", NpgsqlDbType.Varchar));
                command.Parameters[24].Value = funcionario.OrgaoEmissor;

                command.Parameters.Add(new NpgsqlParameter("VALUE26", NpgsqlDbType.Varchar));
                command.Parameters[25].Value = funcionario.UfEmissor;

                command.Parameters.Add(new NpgsqlParameter("VALUE27", NpgsqlDbType.Varchar));
                command.Parameters[26].Value = funcionario.Serie;

                command.Parameters.Add(new NpgsqlParameter("VALUE28", NpgsqlDbType.Varchar));
                command.Parameters[27].Value = funcionario.UfEmissorCtps;
                
                command.ExecuteNonQuery();

                return funcionario;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateFuncionario", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public void deleteFuncionario(Funcionario funcionario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteFuncionario");

            try
            {
                StringBuilder query = new StringBuilder();
                
                query.Append(" DELETE FROM SEG_FUNCIONARIO WHERE ID_FUNCIONARIO = :VALUE1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer)); // id
                command.Parameters[0].Value = funcionario.Id;
                
                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteFuncionario", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public DataSet findFuncionarioByFilterByFuncionario(Funcionario funcionario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFuncionarioByFilterByFuncionario");

            try
            {
                StringBuilder query = new StringBuilder();

                if (funcionario.isNullForFilter())
                {
                    query.Append(" SELECT FUNCIONARIO.ID_FUNCIONARIO, FUNCIONARIO.NOME, FORMATA_CPF(FUNCIONARIO.CPF), FUNCIONARIO.RG, ");
                    query.Append(" FUNCIONARIO.ENDERECO, FUNCIONARIO.NUMERO, FUNCIONARIO.COMPLEMENTO, FUNCIONARIO.BAIRRO, FUNCIONARIO.ID_CIDADE, FUNCIONARIO.UF, FUNCIONARIO.TELEFONE1, ");
                    query.Append(" FUNCIONARIO.TELEFONE2, FUNCIONARIO.EMAIL, FUNCIONARIO.TIPOSAN, FUNCIONARIO.FTRH, FUNCIONARIO.DATA_NASCIMENTO, FUNCIONARIO.PESO, FUNCIONARIO.ALTURA, ");
                    query.Append(" FUNCIONARIO.SITUACAO, FUNCIONARIO.CEP, FUNCIONARIO.SEXO ");
                    query.Append(" FROM SEG_FUNCIONARIO FUNCIONARIO ");
                    query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = FUNCIONARIO.ID_CIDADE ");
                    query.Append(" WHERE FUNCIONARIO.SITUACAO = :VALUE4 ");
                    query.Append(" ORDER BY FUNCIONARIO.NOME ASC");

                }
                else
                {
                    query.Append(" SELECT FUNCIONARIO.ID_FUNCIONARIO, FUNCIONARIO.NOME, FORMATA_CPF(FUNCIONARIO.CPF), FUNCIONARIO.RG, ");
                    query.Append(" FUNCIONARIO.ENDERECO, FUNCIONARIO.NUMERO, FUNCIONARIO.COMPLEMENTO, FUNCIONARIO.BAIRRO, FUNCIONARIO.ID_CIDADE, FUNCIONARIO.UF, FUNCIONARIO.TELEFONE1, ");
                    query.Append(" FUNCIONARIO.TELEFONE2, FUNCIONARIO.EMAIL, FUNCIONARIO.TIPOSAN, FUNCIONARIO.FTRH, FUNCIONARIO.DATA_NASCIMENTO, FUNCIONARIO.PESO, FUNCIONARIO.ALTURA, ");
                    query.Append(" FUNCIONARIO.SITUACAO, FUNCIONARIO.CEP, FUNCIONARIO.SEXO ");
                    query.Append(" FROM SEG_FUNCIONARIO FUNCIONARIO ");
                    query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = FUNCIONARIO.ID_CIDADE ");
                    query.Append(" WHERE TRUE  ");

                    if (!String.IsNullOrEmpty(funcionario.Situacao))
                        query.Append(" AND FUNCIONARIO.SITUACAO = :VALUE4 ");


                    if (!String.IsNullOrWhiteSpace(funcionario.Cpf))
                        query.Append(" AND FUNCIONARIO.CPF = :VALUE1 ");

                    if(!String.IsNullOrEmpty(funcionario.Rg))
                        query.Append (" AND FUNCIONARIO.RG = :VALUE2 ");

                    if (!String.IsNullOrEmpty(funcionario.Nome))
                    {
                        query.Append(" AND FUNCIONARIO.NOME LIKE :VALUE3 ");
                    }

                    query.Append( "ORDER BY FUNCIONARIO.NOME ASC ");

                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = funcionario.Cpf;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = funcionario.Rg;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[2].Value = funcionario.Nome + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[3].Value = funcionario.Situacao;

                DataSet ds = new DataSet();
                adapter.Fill(ds, "Funcionarios");
                return ds;
            }

            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteByFilterByFuncionario", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Funcionario findFuncionarioByRG(String rg, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findFuncionarioByRG");

            try
            {
                StringBuilder query = new StringBuilder();

                Funcionario funcionarioRetorno = null;

                query.Append(" SELECT FUNCIONARIO.ID_FUNCIONARIO, FUNCIONARIO.NOME, FORMATA_CPF(FUNCIONARIO.CPF), FUNCIONARIO.RG, ");
                query.Append(" FUNCIONARIO.ENDERECO, FUNCIONARIO.NUMERO, FUNCIONARIO.COMPLEMENTO, FUNCIONARIO.BAIRRO, FUNCIONARIO.ID_CIDADE, FUNCIONARIO.UF, FUNCIONARIO.TELEFONE1, ");
                query.Append(" FUNCIONARIO.TELEFONE2, FUNCIONARIO.EMAIL, FUNCIONARIO.TIPOSAN, FUNCIONARIO.FTRH, FUNCIONARIO.DATA_NASCIMENTO, FUNCIONARIO.PESO, FUNCIONARIO.ALTURA, ");
                query.Append(" FUNCIONARIO.SITUACAO, FUNCIONARIO.CEP, FUNCIONARIO.SEXO, FUNCIONARIO.CTPS, FUNCIONARIO.PIS_PASEP, FUNCIONARIO.PCD, FUNCIONARIO.ORGAO_EMISSOR, FUNCIONARIO.UF_EMISSOR, FUNCIONARIO.SERIE, FUNCIONARIO.UF_EMISSOR_CTPS ");
                query.Append(" FROM SEG_FUNCIONARIO FUNCIONARIO ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = FUNCIONARIO.ID_CIDADE ");
                query.Append(" WHERE RG = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));

                command.Parameters[0].Value = rg;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    funcionarioRetorno = new Funcionario(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.GetString(4), dr.GetString(5), dr.GetString(6), dr.GetString(7), dr.IsDBNull(8) ? null : new CidadeIbge(dr.GetInt64(8), dr.GetString(23), dr.GetString(24), dr.GetString(25)), dr.GetString(9), dr.GetString(10), dr.GetString(11), dr.GetString(12), dr.GetString(13), dr.GetString(14), dr.GetDateTime(15), dr.GetInt32(16), dr.GetDecimal(17), dr.GetString(18), dr.GetString(19), dr.GetString(20), dr.IsDBNull(21) ? String.Empty : dr.GetString(21), dr.IsDBNull(22) ? String.Empty : dr.GetString(22), dr.GetBoolean(23), dr.IsDBNull(24) ? string.Empty : dr.GetString(24), dr.IsDBNull(25) ? string.Empty : dr.GetString(25), dr.IsDBNull(26) ? string.Empty : dr.GetString(26), dr.IsDBNull(27) ? string.Empty : dr.GetString(27));
                }

                return funcionarioRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findFuncionarioByRG", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

    }
}
