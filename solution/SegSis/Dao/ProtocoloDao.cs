﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using Npgsql;
using SWS.Helper;
using SWS.Excecao;
using SWS.Entidade;
using NpgsqlTypes;
using System.Data;
using SWS.View.Resources;

namespace SWS.Dao
{
    class ProtocoloDao :IProtocoloDao
    {
        
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_PROTOCOLO_ID_PROTOCOLO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public Protocolo insertProtocolo(Protocolo protocolo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertProtocolo");

            try
            {
                protocolo.Id = recuperaProximoId(dbConnection);

                protocolo.Numero = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + Convert.ToString(protocolo.Id).PadLeft(6, '0');

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_PROTOCOLO( ID_PROTOCOLO, NUMERO, SITUACAO, DATA_GRAVACAO, ID_USUARIO_GEROU, ID_CLIENTE)");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6) ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = protocolo.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = protocolo.Numero;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = protocolo.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Timestamp));
                command.Parameters[3].Value = protocolo.DataGravacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Integer));
                command.Parameters[4].Value = protocolo.UsuarioGerou.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Integer));
                command.Parameters[5].Value = protocolo.Cliente.Id;

                command.ExecuteNonQuery();
                
                foreach(ClienteFuncaoExameASo cfea in protocolo.ClienteFuncaoExameAsos)
                {
                    StringBuilder queryCfea = new StringBuilder();
                    
                    queryCfea.Append(" UPDATE SEG_CLIENTE_FUNCAO_EXAME_ASO SET ID_PROTOCOLO = :VALUE2 WHERE ID_CLIENTE_FUNCAO_EXAME_ASO = :VALUE1 ");
                    
                    NpgsqlCommand commandCfea = new NpgsqlCommand(queryCfea.ToString(), dbConnection);

                    
                    commandCfea.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                    commandCfea.Parameters[0].Value = cfea.Id;

                    commandCfea.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                    commandCfea.Parameters[1].Value = protocolo.Id;
                    
                    commandCfea.ExecuteNonQuery();
                    
                }

                
                foreach (GheFonteAgenteExameAso gfaea in protocolo.GheFonteAgenteExameAsos)
                {
                
                    StringBuilder queryGfaea = new StringBuilder();
    
                    queryGfaea.Append(" UPDATE SEG_GHE_FONTE_AGENTE_EXAME_ASO SET ID_PROTOCOLO = :VALUE2 WHERE ID_GHE_FONTE_AGENTE_EXAME_ASO = :VALUE1 ");

                    NpgsqlCommand commandGfaea = new NpgsqlCommand(queryGfaea.ToString(), dbConnection);
                    
                    commandGfaea.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                    commandGfaea.Parameters[0].Value = gfaea.Id;

                    commandGfaea.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                    commandGfaea.Parameters[1].Value = protocolo.Id;
                    
                    commandGfaea.ExecuteNonQuery();
                    
                }

                return protocolo;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertProtocolo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Protocolo updateProtocolo(Protocolo protocolo, String acao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateProtocolo");

            try
            {
                StringBuilder query = new StringBuilder();

                if (String.Equals(acao, ApplicationConstants.FINALIZAPROTOCOLO))
                {

                    query.Append(" UPDATE SEG_PROTOCOLO SET DOCUMENTO_RESPONSAVEL = :VALUE2, ENVIADO = :VALUE3, OBSERVACAO =:VALUE4, DATA_FINALIZACAO = :VALUE5, ID_USUARIO_FINALIZOU = :VALUE6 WHERE ID_PROTOCOLO = :VALUE1 ");

                    NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                    command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                    command.Parameters[0].Value = protocolo.Id;

                    command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                    command.Parameters[1].Value = String.IsNullOrEmpty(protocolo.DocumentoResponsavel) ? String.Empty : protocolo.DocumentoResponsavel;

                    command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                    command.Parameters[2].Value = String.IsNullOrEmpty(protocolo.Enviado) ? String.Empty : protocolo.Enviado;

                    command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Text));
                    command.Parameters[3].Value = String.IsNullOrEmpty(protocolo.Observacao) ? String.Empty : protocolo.Observacao;

                    command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Timestamp));
                    command.Parameters[4].Value = protocolo.DataFinalizacao;

                    command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Integer));
                    command.Parameters[5].Value = protocolo.UsuarioFinalizou.Id;

                    command.ExecuteNonQuery();
                }
                else if (String.Equals(acao, ApplicationConstants.DELETEPROTOCOLO))
                {
                    query.Append(" UPDATE SEG_PROTOCOLO SET DATA_CANCELAMENTO = :VALUE1, ID_USUARIO_CANCELOU  = :VALUE2 WHERE ID_PROTOCOLO = :VALUE3");

                    NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                    command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Timestamp));
                    command.Parameters[0].Value = protocolo.DataCancelamento;

                    command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                    command.Parameters[1].Value = protocolo.UsuarioCancelou.Id;
                    
                    command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                    command.Parameters[2].Value = protocolo.Id;

                    command.ExecuteNonQuery();

                }
                
                return protocolo;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateProtocolo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void changeSituacao(Protocolo protocolo, String situacao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método changeSituacao");

            try
            {

                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_PROTOCOLO SET SITUACAO = :VALUE2 WHERE ID_PROTOCOLO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = protocolo.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = situacao;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - changeSituacao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findByFilter(Protocolo protocolo, DateTime? dataGravacaoFinal, DateTime? dataCancelamentoFinal,
            String tipoItem, Int64? idItem, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByFilter");

            try
            {
                StringBuilder query = new StringBuilder();
                
                if (protocolo.isNullForFilter() & idItem == null)
                {
                    query.Append(" SELECT PROTOCOLO.ID_PROTOCOLO, FORMATA_CODIGO_ATENDIMENTO(PROTOCOLO.NUMERO), ");
                    query.Append(" PROTOCOLO.DOCUMENTO_RESPONSAVEL, PROTOCOLO.SITUACAO, PROTOCOLO.DATA_GRAVACAO, USUARIOGRAVA.NOME, PROTOCOLO.DATA_CANCELAMENTO, USUARIOCANCELA.NOME, PROTOCOLO.ENVIADO, USUARIOFINALIZOU.NOME, PROTOCOLO.DATA_FINALIZACAO FROM SEG_PROTOCOLO PROTOCOLO ");
                    query.Append(" JOIN SEG_USUARIO USUARIOGRAVA ON USUARIOGRAVA.ID_USUARIO = PROTOCOLO.ID_USUARIO_GEROU ");
                    query.Append(" LEFT JOIN SEG_USUARIO USUARIOCANCELA ON USUARIOCANCELA.ID_USUARIO = PROTOCOLO.ID_USUARIO_CANCELOU ");
                    query.Append(" LEFT JOIN SEG_USUARIO USUARIOFINALIZOU ON USUARIOFINALIZOU.ID_USUARIO = PROTOCOLO.ID_USUARIO_FINALIZOU ");
                    query.Append(" ORDER BY PROTOCOLO.NUMERO ASC ");
                }
                else
                {
                    query.Append(" SELECT PROTOCOLO.ID_PROTOCOLO, FORMATA_CODIGO_ATENDIMENTO(PROTOCOLO.NUMERO), ");
                    query.Append(" PROTOCOLO.DOCUMENTO_RESPONSAVEL, PROTOCOLO.SITUACAO, PROTOCOLO.DATA_GRAVACAO, USUARIOGRAVA.NOME, PROTOCOLO.DATA_CANCELAMENTO, USUARIOCANCELA.NOME, PROTOCOLO.ENVIADO, USUARIOFINALIZOU.NOME, PROTOCOLO.DATA_FINALIZACAO FROM SEG_PROTOCOLO PROTOCOLO ");
                    query.Append(" JOIN SEG_USUARIO USUARIOGRAVA ON USUARIOGRAVA.ID_USUARIO = PROTOCOLO.ID_USUARIO_GEROU ");
                    query.Append(" LEFT JOIN SEG_USUARIO USUARIOCANCELA ON USUARIOCANCELA.ID_USUARIO = PROTOCOLO.ID_USUARIO_CANCELOU ");
                    query.Append(" LEFT JOIN SEG_USUARIO USUARIOFINALIZOU ON USUARIOFINALIZOU.ID_USUARIO = PROTOCOLO.ID_USUARIO_FINALIZOU ");
                    
                    /* testando para saber o produto escolhido para o filtro */

                    if (idItem != null)
                    {
                        if (String.Equals(ApplicationConstants.ASO, tipoItem))
                        {
                            query.Append(" JOIN SEG_ASO ASO ON ASO.ID_PROTOCOLO = PROTOCOLO.ID_PROTOCOLO AND ASO.ID_ASO = :VALUE8 ");
                        }
                        else if (String.Equals(ApplicationConstants.PCMSO, tipoItem))
                        {
                            query.Append(" JOIN SEG_ESTUDO PCMSO ON PCMSO.ID_PROTOCOLO = PROTOCOLO.ID_PROTOCOLO AND PCMSO.ID_ESTUDO = :VALUE8 ");
                        }
                        else if (String.Equals(ApplicationConstants.PPRA, tipoItem))
                        {
                            query.Append(" JOIN SEG_ESTUDO PPRA ON PPRA.ID_PROTOCOLO = PROTOCOLO.ID_PROTOCOLO AND PPRA.ID_ESTUDO = :VALUE8 ");
                        }
                        else if (String.Equals(ApplicationConstants.PRODUTO, tipoItem))
                        {
                            query.Append(" JOIN SEG_MOVIMENTO MOVIMENTO ON MOVIMENTO.ID_PROTOCOLO = PROTOCOLO.ID_PROTOCOLO AND MOVIMENTO.ID_MOVIMENTO = :VALUE8 ");
                        }
                    }

                    query.Append(" WHERE TRUE ");

                    if (!String.IsNullOrEmpty(protocolo.Numero))
                    {
                        query.Append(" AND PROTOCOLO.NUMERO = :VALUE1 ");
                    }

                    if (!String.IsNullOrEmpty(protocolo.Situacao))
                    {
                        query.Append(" AND PROTOCOLO.SITUACAO = :VALUE2 ");
                    }

                    if (protocolo.DataGravacao != null)
                    {
                        query.Append(" AND PROTOCOLO.DATA_GRAVACAO BETWEEN :VALUE3 AND :VALUE5 ");
                    }

                    if (protocolo.DataCancelamento != null)
                    {
                        query.Append(" AND PROTOCOLO.DATA_CANCELAMENTO BETWEEN :VALUE4 AND :VALUE6 ");
                    }

                    if (protocolo.Cliente != null)
                    {
                        query.Append(" AND PROTOCOLO.ID_CLIENTE = :VALUE7 ");
                    }

                    query.Append(" ORDER BY PROTOCOLO.NUMERO ASC");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = String.IsNullOrEmpty(protocolo.Numero) ? String.Empty : protocolo.Numero;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("value2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = String.IsNullOrEmpty(protocolo.Situacao) ? String.Empty : protocolo.Situacao;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("value3", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[2].Value = protocolo.DataGravacao != null ? protocolo.DataGravacao : (DateTime?)null;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("value4", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[3].Value = protocolo.DataCancelamento != null ? protocolo.DataCancelamento : (DateTime?)null;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("value5", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[4].Value = dataGravacaoFinal;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("value6", NpgsqlDbType.Timestamp));
                adapter.SelectCommand.Parameters[5].Value = dataCancelamentoFinal;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("value7", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[6].Value = protocolo.Cliente != null ? protocolo.Cliente.Id : null;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("value8", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[7].Value = idItem != null ? idItem : null;

                DataSet ds = new DataSet();
                adapter.Fill(ds, "Protocolos");
                return ds;
            }
            catch (NpgsqlException ex)
            {
                    LogHelper.logger.Error(this.GetType().Name + " - findByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findAllItensByProtocolo(Protocolo protocolo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllItensByProtocolo");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT CFEA.ID_CLIENTE_FUNCAO_EXAME_ASO AS ID, FORMATA_CODIGO_ATENDIMENTO(ASO.CODIGO_ASO) AS CODIGO_ITEM, ASO.RAZAO_EMPRESA AS CLIENTE, ASO.NOME_FUNCIONARIO AS COLABORADOR, ");
                query.Append(" ASO.RG_FUNCIONARIO AS RG, ASO.CPF_FUNCIONARIO AS CPF, PERIODICIDADE.DESCRICAO AS PERIODICIDADE, ");
                query.Append(" EXAME.DESCRICAO AS TIPO_ITEM, CFEA.DATA_EXAME AS DATA_ITEM, CFEA.RESULTADO AS LAUDO, 'EE' AS TIPO, FALSE  ");
                query.Append(" FROM SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA");
                query.Append(" JOIN SEG_ASO ASO ON ASO.ID_ASO = CFEA.ID_ASO");
                query.Append(" JOIN SEG_PERIODICIDADE PERIODICIDADE ON PERIODICIDADE.ID_PERIODICIDADE = ASO.ID_PERIODICIDADE ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME CFE ON CFE.ID_CLIENTE_FUNCAO_EXAME = CFEA.ID_CLIENTE_FUNCAO_EXAME ");
                query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = CFE.ID_EXAME ");
                query.Append(" WHERE CFEA.ID_PROTOCOLO = :VALUE1 ");
                query.Append(" UNION ( ");
                query.Append(" SELECT GFAEA.ID_GHE_FONTE_AGENTE_EXAME_ASO AS ID, FORMATA_CODIGO_ATENDIMENTO(ASO.CODIGO_ASO) AS CODIGO_ITEM, ASO.RAZAO_EMPRESA AS CLIENTE, ASO.NOME_FUNCIONARIO AS COLABORADOR, ");
                query.Append(" ASO.RG_FUNCIONARIO AS RG, ASO.CPF_FUNCIONARIO AS CPF, PERIODICIDADE.DESCRICAO AS PERIODICIDADE, ");
                query.Append(" EXAME.DESCRICAO AS TIPO_ITEM, GFAEA.DATA_EXAME AS DATA_ITEM, GFAEA.RESULTADO AS LAUDO, 'EP' AS TIPO, FALSE  ");
                query.Append(" FROM SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA");
                query.Append(" JOIN SEG_ASO ASO ON ASO.ID_ASO = GFAEA.ID_ASO");
                query.Append(" JOIN SEG_PERIODICIDADE PERIODICIDADE ON PERIODICIDADE.ID_PERIODICIDADE = ASO.ID_PERIODICIDADE ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GFAE ON GFAE.ID_GHE_FONTE_AGENTE_EXAME = GFAEA.ID_GHE_FONTE_AGENTE_EXAME ");
                query.Append(" JOIN SEG_EXAME EXAME ON EXAME.ID_EXAME = GFAE.ID_EXAME ");
                query.Append(" WHERE GFAEA.ID_PROTOCOLO = :VALUE1 ) ");
                query.Append(" UNION ( ");
                query.Append(" SELECT ASO.ID_ASO AS ID, FORMATA_CODIGO_ATENDIMENTO(ASO.CODIGO_ASO) AS CODIGO_ITEM, ASO.RAZAO_EMPRESA AS CLIENTE, ASO.NOME_FUNCIONARIO AS COLABORADOR, ");
                query.Append(" ASO.RG_FUNCIONARIO AS RG, ASO.CPF_FUNCIONARIO AS CPF, PERIODICIDADE.DESCRICAO AS PERIODICIDADE, ");
                query.Append(" 'ASO' AS TIPO_ITEM, ASO.DATA_ASO AS DATA_ITEM, ASO.CONCLUSAO AS LAUDO, 'AS' AS TIPO, FALSE ");
                query.Append(" FROM SEG_ASO ASO ");
                query.Append(" JOIN SEG_PERIODICIDADE PERIODICIDADE ON PERIODICIDADE.ID_PERIODICIDADE = ASO.ID_PERIODICIDADE ");
                query.Append(" WHERE ASO.ID_PROTOCOLO = :VALUE1 )");
                query.Append(" UNION (");
                query.Append(" SELECT PCMSO.ID_ESTUDO AS ID, FORMATA_COD_ESTUDO(PCMSO.COD_ESTUDO) AS CODIGO_ITEM, CLIENTE.RAZAO_SOCIAL AS CLIENTE, NULL AS COLABORADOR, NULL AS RG, NULL AS CPF, ");
                query.Append(" NULL AS PERIODICIDADE, 'PCMSO' AS TIPO_ITEM, PCMSO.DATA_CRIACAO AS DATA_ITEM, NULL AS LAUDO, 'PC' AS TIPO, FALSE ");
                query.Append(" FROM SEG_ESTUDO PCMSO ");
                query.Append(" JOIN SEG_REP_CLI RC ON RC.ID_SEG_REP_CLI = PCMSO.ID_SEG_REP_CLI ");
                query.Append(" JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = RC.ID_CLIENTE ");
                query.Append(" WHERE PCMSO.ESTUDO = TRUE AND PCMSO.ID_PROTOCOLO = :VALUE1 )");
                query.Append(" UNION (");
                query.Append(" SELECT PPRA.ID_ESTUDO AS ID, FORMATA_COD_ESTUDO(PPRA.COD_ESTUDO) AS CODIGO_ITEM, CLIENTE.RAZAO_SOCIAL AS CLIENTE, NULL AS COLABORADOR, NULL AS RG, NULL AS CPF, ");
                query.Append(" NULL AS PERIODICIDADE, 'PPRA' AS TIPO_ITEM, PPRA.DATA_CRIACAO AS DATA_ITEM, NULL AS LAUDO, 'PP' AS TIPO, FALSE ");
                query.Append(" FROM SEG_ESTUDO PPRA ");
                query.Append(" JOIN SEG_REP_CLI RC ON RC.ID_SEG_REP_CLI = PPRA.ID_SEG_REP_CLI ");
                query.Append(" JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = RC.ID_CLIENTE ");
                query.Append(" WHERE PPRA.ESTUDO = FALSE AND PPRA.ID_PROTOCOLO = :VALUE1 )");
                query.Append(" UNION (");
                query.Append(" SELECT MOVIMENTO.ID_MOVIMENTO AS ID, NULL AS CODIGO_ITEM, CLIENTE.RAZAO_SOCIAL AS CLIENTE, NULL AS COLABORADOR, NULL AS RG, NULL AS CPF,");
                query.Append(" NULL AS PERIODICIDADE, PRODUTO.NOME AS TIPO_ITEM, MOVIMENTO.DT_INCLUSAO AS DATA_ITEM, NULL AS LAUDO, 'P'  AS TIPO, FALSE ");
                query.Append(" FROM SEG_MOVIMENTO MOVIMENTO ");
                query.Append(" JOIN SEG_PRODUTO_CONTRATO PC ON PC.ID_PRODUTO_CONTRATO = MOVIMENTO.ID_PRODUTO_CONTRATO ");
                query.Append(" JOIN SEG_PRODUTO PRODUTO ON PRODUTO.ID_PRODUTO = PC.ID_PRODUTO ");
                query.Append(" JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = MOVIMENTO.ID_CLIENTE ");
                query.Append(" WHERE MOVIMENTO.ID_PROTOCOLO = :VALUE1 )");
                query.Append(" UNION ( ");
                query.Append(" SELECT DOCUMENTOPROTOCOLO.ID_DOCUMENTO_PROTOCOLO AS ID, FORMATA_CODIGO_ATENDIMENTO(ASO.CODIGO_ASO) AS CODIGO_ITEM, ASO.RAZAO_EMPRESA AS CLIENTE, ASO.NOME_FUNCIONARIO AS COLABORADOR, ");
                query.Append(" ASO.RG_FUNCIONARIO AS RG, ASO.CPF_FUNCIONARIO AS CPF, PERIODICIDADE.DESCRICAO AS PERIODICIDADE, DOCUMENTO.DESCRICAO AS TIPO_ITEM, DOCUMENTOPROTOCOLO.DATA_GRAVACAO AS DATA_ITEM, NULL AS LAUDO, 'DP' AS TIPO, FALSE  ");
                query.Append(" FROM SEG_DOCUMENTO_PROTOCOLO DOCUMENTOPROTOCOLO");
                query.Append(" JOIN SEG_ASO ASO ON ASO.ID_ASO = DOCUMENTOPROTOCOLO.ID_ASO ");
                query.Append(" JOIN SEG_PERIODICIDADE PERIODICIDADE ON PERIODICIDADE.ID_PERIODICIDADE = ASO.ID_PERIODICIDADE ");
                query.Append(" JOIN SEG_DOCUMENTO DOCUMENTO ON DOCUMENTO.ID_DOCUMENTO = DOCUMENTOPROTOCOLO.ID_DOCUMENTO ");
                query.Append(" WHERE DOCUMENTOPROTOCOLO.ID_PROTOCOLO = :VALUE1 )");
                query.Append(" ORDER BY CODIGO_ITEM, TIPO_ITEM ASC ");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[0].Value = protocolo.Id;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "ExamesProtocolo");

                
                #region totais na tela

                query.Length = 0;
                
                query.Append(" SELECT ");
                query.Append(" (SELECT COUNT(*) FROM SEG_CLIENTE_FUNCAO_EXAME_ASO ");
                query.Append(" WHERE ID_PROTOCOLO = :VALUE1 ) AS TOTALEXAMEEXTRA, ");
                query.Append(" (SELECT COUNT(*) FROM SEG_GHE_FONTE_AGENTE_EXAME_ASO ");
                query.Append(" WHERE ID_PROTOCOLO = :VALUE1 ) AS TOTALEXAMEPCMSO,");
                query.Append(" (SELECT ((SELECT COUNT(*) FROM SEG_CLIENTE_FUNCAO_EXAME_ASO WHERE ID_PROTOCOLO = :VALUE1 AND ");
                query.Append(" RESULTADO IS NOT NULL ) + ");
                query.Append(" (SELECT COUNT(*) FROM SEG_GHE_FONTE_AGENTE_EXAME_ASO ");
                query.Append(" WHERE ID_PROTOCOLO = :VALUE1 AND RESULTADO IS NOT NULL) )) AS EXAMELAUDADO, ");
                query.Append(" (SELECT COUNT(*) FROM SEG_ASO WHERE ID_PROTOCOLO = :VALUE1 ) AS QUANTIDADEASO, ");
                query.Append(" (SELECT COUNT(*) FROM SEG_ESTUDO WHERE ID_PROTOCOLO = :VALUE1) AS QUANTIDADEESTUDO, ");
                query.Append(" (SELECT COUNT(*) FROM SEG_MOVIMENTO WHERE ID_PROTOCOLO = :VALUE1 ) AS QUANTIDADEPRODUTO, ");
                query.Append(" (SELECT COUNT(*) FROM SEG_DOCUMENTO_PROTOCOLO WHERE ID_PROTOCOLO = :VALUE1 ) AS QUANTIDADEDOCUMENTOPROTOCOLO ");

                NpgsqlDataAdapter adapterTotal = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapterTotal.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                adapterTotal.SelectCommand.Parameters[0].Value = protocolo.Id;

                adapterTotal.Fill(ds, "Total");
                
                 #endregion
                
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllItensByProtocolo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public Protocolo findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            Protocolo protocolo = null;

            LinkedList<Aso> atendimentos = new LinkedList<Aso>();
            LinkedList<Estudo> pcmsos = new LinkedList<Estudo>();
            LinkedList<Estudo> ppras = new LinkedList<Estudo>();
            LinkedList<Movimento> movimentos = new LinkedList<Movimento>();

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" SELECT P.ID_PROTOCOLO, P.NUMERO, P.DOCUMENTO_RESPONSAVEL, P.SITUACAO, P.DATA_GRAVACAO, P.ID_USUARIO_GEROU,  ");
                query.Append(" P.DATA_CANCELAMENTO, P.ID_USUARIO_CANCELOU, P.ENVIADO, P.DATA_FINALIZACAO, P.ID_USUARIO_FINALIZOU, P.OBSERVACAO, ");
                query.Append(" P.MOTIVO_CANCELAMENTO, P.ID_CLIENTE ");
                query.Append(" FROM SEG_PROTOCOLO P ");
                query.Append(" WHERE P.ID_PROTOCOLO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = id;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    protocolo = new Protocolo(dr.GetInt64(0), 
                        dr.GetString(1), 
                        dr.IsDBNull(2) ? String.Empty : dr.GetString(2), 
                        dr.GetString(3),
                        dr.IsDBNull(4) ? (DateTime?)null : dr.GetDateTime(4), 
                        new Usuario(dr.GetInt64(5)),
                        dr.IsDBNull(6) ? (DateTime?)null : dr.GetDateTime(6),
                        dr.IsDBNull(7) ? null : new Usuario(dr.GetInt64(7)), 
                        dr.IsDBNull(8) ? String.Empty : dr.GetString(8), 
                        dr.IsDBNull(9) ? (DateTime?)null : dr.GetDateTime(9),
                        dr.IsDBNull(10) ? null : new Usuario(dr.GetInt64(10)),
                        dr.IsDBNull(11) ? String.Empty : dr.GetString(11), 
                        dr.IsDBNull(12) ? String.Empty : dr.GetString(12),
                        new Cliente(dr.GetInt64(13)));
                }

                #region atendimento

                if (protocolo != null)
                {
                    query.Length = 0;

                    query.Append(" SELECT ID_ASO FROM SEG_ASO WHERE ID_PROTOCOLO = :VALUE1 ");

                    NpgsqlCommand commandAtendimento = new NpgsqlCommand(query.ToString(), dbConnection);

                    commandAtendimento.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                    commandAtendimento.Parameters[0].Value = id;

                    NpgsqlDataReader drAtendimento = commandAtendimento.ExecuteReader();

                    while (drAtendimento.Read())
                    {
                        atendimentos.AddFirst(new Aso(drAtendimento.GetInt64(0)));
                    }
                    
                    protocolo.Atendimentos = atendimentos;

                    #endregion

                    #region ppra

                    query.Length = 0;

                    query.Append(" SELECT ID_ESTUDO FROM SEG_ESTUDO WHERE ESTUDO = FALSE AND ID_PROTOCOLO = :VALUE1 ");

                    NpgsqlCommand commandPpra = new NpgsqlCommand(query.ToString(), dbConnection);

                    commandPpra.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                    commandPpra.Parameters[0].Value = id;

                    NpgsqlDataReader drPpra = commandPpra.ExecuteReader();

                    while (drPpra.Read())
                    {
                        ppras.AddFirst(new Estudo(drPpra.GetInt64(0)));
                    }
                    protocolo.Ppras = ppras;

                    #endregion

                    #region pcmso

                    query.Length = 0;

                    query.Append("SELECT ID_ESTUDO FROM SEG_ESTUDO WHERE ESTUDO = TRUE AND ID_PROTOCOLO = :VALUE1   ");

                    NpgsqlCommand commandPcmso = new NpgsqlCommand(query.ToString(), dbConnection);

                    commandPcmso.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                    commandPcmso.Parameters[0].Value = id;

                    NpgsqlDataReader drPcmso = commandPcmso.ExecuteReader();

                    while (drPcmso.Read())
                    {
                        pcmsos.AddFirst(new Estudo(drPcmso.GetInt64(0)));
                    }

                    protocolo.Pcmsos = pcmsos;

                    #endregion

                    #region Movimento

                    query.Length = 0;

                    query.Append(" SELECT ID_MOVIMENTO FROM SEG_MOVIMENTO WHERE ID_PROTOCOLO = :VALUE1 ");

                    NpgsqlCommand commandMovimento = new NpgsqlCommand(query.ToString(), dbConnection);

                    commandMovimento.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                    commandMovimento.Parameters[0].Value = id;

                    NpgsqlDataReader drMovimento = commandMovimento.ExecuteReader();

                    while (drMovimento.Read())
                    {
                        movimentos.AddFirst(new Movimento(drMovimento.GetInt64(0), null, null, null, null, null, null, null, String.Empty, null, null, null, null, null, null, null, null, null, null, DateTime.Now, null, null, null));
                    }
                    protocolo.Movimentos = movimentos;
                    
                    #endregion
                
                }

                return protocolo;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void insertExameInProtocolo(Protocolo protocolo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertExameInProtocolo");

            try
            {
                
                Int32 quantidadeExame = 0;
                StringBuilder query = new StringBuilder();

                if (protocolo.ClienteFuncaoExameAsos.Count > 0)
                {
                    query.Append(" UPDATE SEG_CLIENTE_FUNCAO_EXAME_ASO SET ");
                    query.Append(" ID_PROTOCOLO = :VALUE1 WHERE ID_CLIENTE_FUNCAO_EXAME_ASO IN (");

                    foreach (ClienteFuncaoExameASo cfea in protocolo.ClienteFuncaoExameAsos)
                    {
                        query.Append(cfea.Id);
                        
                        quantidadeExame++;

                        if (quantidadeExame != protocolo.ClienteFuncaoExameAsos.Count)
                        {
                            query.Append(",");
                        }
                        else
                        {
                            query.Append(")");
                        }

                    }


                    NpgsqlCommand commandExameExtra = new NpgsqlCommand(query.ToString(), dbConnection);

                    commandExameExtra.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                    commandExameExtra.Parameters[0].Value = protocolo.Id;

                    commandExameExtra.ExecuteNonQuery();

                }

                /* zerando variavel auxiliar */
                quantidadeExame = 0;
                query.Length = 0;

                if (protocolo.GheFonteAgenteExameAsos.Count > 0)
                {
                    query.Append(" UPDATE SEG_GHE_FONTE_AGENTE_EXAME_ASO SET ");
                    query.Append(" ID_PROTOCOLO = :VALUE1 WHERE ID_GHE_FONTE_AGENTE_EXAME_ASO IN (");

                    foreach (GheFonteAgenteExameAso gfaea in protocolo.GheFonteAgenteExameAsos)
                    {
                        query.Append(gfaea.Id);
                        quantidadeExame++;

                        if (quantidadeExame != protocolo.GheFonteAgenteExameAsos.Count)
                        {
                            query.Append(",");
                        }
                        else
                        {
                            query.Append(")");
                        }
                    }

                    NpgsqlCommand commandExamePcmso = new NpgsqlCommand(query.ToString(), dbConnection);

                    commandExamePcmso.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                    commandExamePcmso.Parameters[0].Value = protocolo.Id;

                    commandExamePcmso.ExecuteNonQuery();

                }

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertExameInProtocolo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void deleteItemProtocolo(Int64 id, String tipo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método deleteItemProtocolo");

            try
            {
                
                StringBuilder query = new StringBuilder();

                if (String.Equals(tipo, ApplicationConstants.EXAME_PCMSO))
                {
                    query.Append(" UPDATE SEG_GHE_FONTE_AGENTE_EXAME_ASO SET ID_PROTOCOLO = NULL WHERE ID_GHE_FONTE_AGENTE_EXAME_ASO = :VALUE1  ");
                }
                else if (String.Equals(tipo, ApplicationConstants.EXAME_EXTRA))
                {
                    query.Append(" UPDATE SEG_CLIENTE_FUNCAO_EXAME_ASO SET ID_PROTOCOLO = NULL WHERE ID_CLIENTE_FUNCAO_EXAME_ASO = :VALUE1  ");
                }
                else if (String.Equals(tipo, ApplicationConstants.ASO))
                {
                    query.Append(" UPDATE SEG_ASO SET ID_PROTOCOLO = NULL WHERE ID_ASO = :VALUE1 ");
                }
                else if (String.Equals(tipo, ApplicationConstants.PCMSO))
                {
                    query.Append(" UPDATE SEG_ESTUDO SET ID_PROTOCOLO = NULL WHERE ID_ESTUDO = :VALUE1");
                }
                else if (String.Equals(tipo, ApplicationConstants.PPRA))
                {
                    query.Append(" UPDATE SEG_ESTUDO SET ID_PROTOCOLO = NULL WHERE ID_ESTUDO = :VALUE1");
                }
                else if (String.Equals(tipo, ApplicationConstants.PRODUTO))
                {
                    query.Append(" UPDATE SEG_MOVIMENTO SET ID_PROTOCOLO = NULL WHERE ID_MOVIMENTO = :VALUE1");
                }


                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = id;

                command.ExecuteNonQuery();
                
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - deleteItemProtocolo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void InsertItemProtocolo(ProtocoloItem protocoloItem, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método InsertItemProtocolo");

            try
            {

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_PROTOCOLO_ITEM (ID_ITEM, ID_PROTOCOLO, CODIGO_ITEM, CLIENTE, COLABORADOR, RG, CPF, PERIODICIDADE, DESCRICAO_ITEM, DATA_ITEM, LAUDO, TIPO) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6, :VALUE7, :VALUE8, :VALUE9, :VALUE10, :VALUE11, :VALUE12)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = protocoloItem.IdItem;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = protocoloItem.IdProtocolo;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = protocoloItem.CodigoItem;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = protocoloItem.Cliente;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = protocoloItem.Colaborador;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = protocoloItem.Rg;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = protocoloItem.Cpf;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Varchar));
                command.Parameters[7].Value = protocoloItem.Periodicidade;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                command.Parameters[8].Value = protocoloItem.DescricaoItem;

                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Date));
                command.Parameters[9].Value = protocoloItem.DataItem;

                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Varchar));
                command.Parameters[10].Value = protocoloItem.Laudo;

                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Varchar));
                command.Parameters[11].Value = protocoloItem.Tipo;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - InsertItemProtocolo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

    }
}
