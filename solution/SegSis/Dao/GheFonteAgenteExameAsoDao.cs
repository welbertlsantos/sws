﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using Npgsql;
using NpgsqlTypes;
using SWS.Helper;
using SWS.Excecao;
using SWS.Entidade;
using SWS.View.Resources;


namespace SWS.Dao
{
    class GheFonteAgenteExameAsoDao : IGheFonteAgenteExameAsoDao
    {

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoIdGheFonteAgenteExameAso");

            Int64 proximoId = Convert.ToInt64(0);
            
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_GHE_FONTE_AGENTE_EXAME_AS_ID_GHE_FONTE_AGENTE_EXAME_ASO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoIdASo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void insert(GheFonteAgenteExameAso gheFonteAgenteExameAso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            try
            {
                StringBuilder query = new StringBuilder();

                gheFonteAgenteExameAso.Id = recuperaProximoId(dbConnection);

                query.Append(" INSERT INTO SEG_GHE_FONTE_AGENTE_EXAME_ASO ( ");
                query.Append(" ID_GHE_FONTE_AGENTE_EXAME_ASO, ID_ASO, ID_GHE_FONTE_AGENTE_EXAME, ");
                query.Append(" DATA_EXAME, ATENDIDO, FINALIZADO, DUPLICADO, TRANSCRITO, ID_PRESTADOR, CONFINADO, ALTURA, ID_MEDICO, DATA_VALIDADE, DATA_ATENDIDO, DATA_FINALIZADO, ELETRICIDADE, IMPRIME_ASO )");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6, :VALUE7, :VALUE8, :VALUE9, :VALUE10, :VALUE11, :VALUE12, :VALUE13, :VALUE14, :VALUE15, :VALUE16, :VALUE17) ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonteAgenteExameAso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = gheFonteAgenteExameAso.Aso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = gheFonteAgenteExameAso.GheFonteAgenteExame.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Date));
                command.Parameters[3].Value = gheFonteAgenteExameAso.DataExame;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Boolean));
                command.Parameters[4].Value = gheFonteAgenteExameAso.Atendido;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Boolean));
                command.Parameters[5].Value = gheFonteAgenteExameAso.Finalizado;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Boolean));
                command.Parameters[6].Value = gheFonteAgenteExameAso.Duplicado;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Boolean));
                command.Parameters[7].Value = gheFonteAgenteExameAso.Transcrito;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Integer));
                command.Parameters[8].Value = gheFonteAgenteExameAso.Prestador != null ? (Int64)gheFonteAgenteExameAso.Prestador.Id : (Int64?)null;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Boolean));
                command.Parameters[9].Value = gheFonteAgenteExameAso.Confinado;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Boolean));
                command.Parameters[10].Value = gheFonteAgenteExameAso.Altura;

                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Integer));
                command.Parameters[11].Value = gheFonteAgenteExameAso.Medico != null ? gheFonteAgenteExameAso.Medico.Id : (long?)null;

                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Date));
                command.Parameters[12].Value = gheFonteAgenteExameAso.DataValidade != null ? gheFonteAgenteExameAso.DataValidade : (DateTime?)null;

                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Date));
                command.Parameters[13].Value = gheFonteAgenteExameAso.DataAtendido;

                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Date));
                command.Parameters[14].Value = gheFonteAgenteExameAso.DataFinalizado;

                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Boolean));
                command.Parameters[15].Value = gheFonteAgenteExameAso.Eletricidade;

                command.Parameters.Add(new NpgsqlParameter("VALUE17", NpgsqlDbType.Boolean));
                command.Parameters[16].Value = gheFonteAgenteExameAso.ImprimeASO;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<GheFonteAgenteExameAso> findAllExamesByAsoInSituacao(Aso aso, Boolean? atendido, Boolean? finalizado, Boolean? cancelado, Boolean? devolvido, Boolean? transcrito, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllExamesByAsoInSituacao");

            List<GheFonteAgenteExameAso> colecaoExameInAso = new List<GheFonteAgenteExameAso>();
            
            StringBuilder query = new StringBuilder();
            
            try
            {
                query.Append(" SELECT GFAEA.ID_GHE_FONTE_AGENTE_EXAME_ASO, GFAEA.ID_ASO, GFAEA.ID_GHE_FONTE_AGENTE_EXAME,  ");
                query.Append(" GFAEA.DATA_EXAME, GFAEA.ID_SALA_EXAME, GFAEA.ATENDIDO, GFAEA.FINALIZADO, GFAEA.LOGIN, GFAEA.DEVOLVIDO, ");
                query.Append(" GFAEA.DATA_ATENDIDO, GFAEA.DATA_DEVOLVIDO, GFAEA.DATA_FINALIZADO, GFAEA.ID_USUARIO, ");
                query.Append(" GFAE.ID_EXAME, E.DESCRICAO, E.SITUACAO, E.LABORATORIO, E.PRECO, E.PRIORIDADE, E.CUSTO, GFAE.IDADE_EXAME, GFAE.SITUACAO, E.EXTERNO, ");
                query.Append(" S.ID_SALA, S.DESCRICAO, S.SITUACAO, S.ANDAR, S.VIP, GFAEA.CANCELADO, GFAEA.TRANSCRITO, GFAEA.RESULTADO, GFAEA.OBSERVACAO_RESULTADO, GFAEA.ID_PRESTADOR, ");
                query.Append(" C.RAZAO_SOCIAL, C.FANTASIA, C.CNPJ, C.INSCRICAO, C.ENDERECO, C.NUMERO, C.COMPLEMENTO, C.BAIRRO, C.CEP, C.UF, ");
                query.Append(" C.TELEFONE1, C.TELEFONE2, C.EMAIL, C.DATA_CADASTRO, C.SITUACAO, C.VIP, C.USA_CONTRATO, C.PARTICULAR, C.UNIDADE, C.CREDENCIADA, C.PRESTADOR, C.ID_CIDADE_IBGE,");
                query.Append(" CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, ");
                query.Append(" C.DESTACA_ISS, C.GERA_COBRANCA_VALOR_LIQUIDO, C.ALIQUOTA_ISS, GFAEA.ID_PROTOCOLO, E.LIBERA_DOCUMENTO, GFAE.CONFINADO, GFAE.ALTURA, GFAEA.CONFINADO, GFAEA.ALTURA, C.CODIGO_CNES, E.CODIGO_TUSS, E.EXAME_COMPLEMENTAR, GFAEA.ID_MEDICO, MEDICO.NOME, MEDICO.CRM, C.USA_CENTRO_CUSTO, C.BLOQUEADO, C.CREDENCIADORA, C.FISICA, GFAEA.DATA_VALIDADE, E.PERIODO_VENCIMENTO, S.ID_EMPRESA, C.USA_PO, S.SLUG_SALA, E.PADRAO_CONTRATO, S.QTDE_CHAMADA, GFAE.ELETRICIDADE, GFAEA.ELETRICIDADE, C.SIMPLES_NACIONAL, GFAEA.IMPRIME_ASO ");
                query.Append(" FROM SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA  ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GFAE ON GFAE.ID_GHE_FONTE_AGENTE_EXAME = GFAEA.ID_GHE_FONTE_AGENTE_EXAME ");
                query.Append(" JOIN SEG_EXAME E ON E.ID_EXAME = GFAE.ID_EXAME ");
                query.Append(" LEFT JOIN SEG_SALA_EXAME SE ON SE.ID_SALA_EXAME = GFAEA.ID_SALA_EXAME ");
                query.Append(" LEFT JOIN SEG_SALA S ON S.ID_SALA =  SE.ID_SALA ");
                query.Append(" LEFT JOIN SEG_CLIENTE C ON C.ID_CLIENTE = GFAEA.ID_PRESTADOR ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = C.ID_CIDADE_IBGE");
                query.Append(" LEFT JOIN SEG_MEDICO MEDICO ON MEDICO.ID_MEDICO = GFAEA.ID_MEDICO ");
                query.Append(" WHERE GFAEA.ID_ASO = :VALUE1 ");
                query.Append(" AND GFAEA.DUPLICADO = FALSE ");
                
                if (atendido != null)
                    query.Append(" AND GFAEA.ATENDIDO = :VALUE2 ");

                if (finalizado != null)
                    query.Append(" AND GFAEA.FINALIZADO = :VALUE3 ");

                if (cancelado != null)
                    query.Append(" AND GFAEA.CANCELADO = :VALUE4 ");

                if (devolvido != null)
                    query.Append(" AND GFAEA.DEVOLVIDO = :VALUE5 ");

                if (transcrito != null)
                {
                    if ((Boolean)transcrito)
                        query.Append(" AND GFAEA.TRANSCRITO IS TRUE ");
                    else
                        query.Append(" AND GFAEA.TRANSCRITO IS FALSE ");
                }

                query.Append(" ORDER BY E.DESCRICAO ASC");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Boolean));
                command.Parameters[1].Value = atendido;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Boolean));
                command.Parameters[2].Value = finalizado;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Boolean));
                command.Parameters[3].Value = cancelado;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Boolean));
                command.Parameters[4].Value = devolvido;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    colecaoExameInAso.Add(new GheFonteAgenteExameAso(dr.GetInt64(0), dr.IsDBNull(4) ? null : new SalaExame(dr.GetInt64(4), dr.IsDBNull(13) ? null : new Exame(dr.GetInt64(13), dr.GetString(14), dr.GetString(15), dr.GetBoolean(16), dr.GetDecimal(17), dr.GetInt32(18), dr.GetDecimal(19), dr.GetBoolean(22), dr.GetBoolean(62), dr.IsDBNull(68) ? string.Empty : dr.GetString(68), dr.GetBoolean(69), dr.IsDBNull(78) ? (int?)null : dr.GetInt32(78), dr.GetBoolean(82)), new Sala(dr.GetInt64(23), dr.GetString(24), dr.GetString(25), dr.GetInt32(26), dr.GetBoolean(27), new Empresa(dr.GetInt64(79)), dr.GetString(81), dr.GetInt32(83)), null, null, null), aso, dr.IsDBNull(2) ? null : new GheFonteAgenteExame(dr.GetInt64(2), dr.IsDBNull(13) ? null : new Exame(dr.GetInt64(13), dr.GetString(14), dr.GetString(15), dr.GetBoolean(16), dr.GetDecimal(17), dr.GetInt32(18), dr.GetDecimal(19), dr.GetBoolean(22), dr.GetBoolean(62), dr.IsDBNull(68) ? string.Empty : dr.GetString(68), dr.GetBoolean(69), dr.IsDBNull(78) ? (int?)null : dr.GetInt32(78), dr.GetBoolean(82)), null, dr.GetInt32(20), dr.GetString(21), dr.GetBoolean(63), dr.GetBoolean(64), dr.GetBoolean(84)), dr.GetDateTime(3), dr.GetBoolean(5), dr.GetBoolean(6), !dr.IsDBNull(7) ? dr.GetString(7) : string.Empty, dr.GetBoolean(8), null, null, null, null, dr.GetBoolean(28), null, dr.IsDBNull(30) ? string.Empty : dr.GetString(30), dr.IsDBNull(30) ? string.Empty : dr.IsDBNull(31) ? string.Empty : dr.GetString(31), false, dr.GetBoolean(29), dr.IsDBNull(32) ? null : new Cliente(dr.GetInt64(32), dr.GetString(33), dr.GetString(34), dr.GetString(35), dr.GetString(36), dr.GetString(37), dr.GetString(38), dr.GetString(39), dr.GetString(40), dr.GetString(41), dr.GetString(42), dr.GetString(43), dr.GetString(44), dr.GetString(45), string.Empty, dr.GetDateTime(46), String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, dr.GetString(47), 0, 0, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, null, String.Empty, dr.GetBoolean(48), String.Empty, dr.GetBoolean(49), null, null, dr.GetBoolean(50), dr.GetBoolean(51), dr.GetBoolean(52), dr.GetBoolean(53), new CidadeIbge(dr.GetInt64(54), dr.GetString(55), dr.GetString(56), dr.GetString(57)), null, dr.GetBoolean(58), dr.GetBoolean(59), dr.GetDecimal(60), dr.IsDBNull(67) ? string.Empty : dr.GetString(67), dr.GetBoolean(73), dr.GetBoolean(74), dr.GetBoolean(75), dr.GetBoolean(76), dr.GetBoolean(80), dr.GetBoolean(86)), dr.IsDBNull(61) ? null : new Protocolo(dr.GetInt64(61), String.Empty, String.Empty, String.Empty, null, null, null, null, String.Empty, null, null, String.Empty, String.Empty, null), dr.GetBoolean(65), dr.GetBoolean(66), dr.IsDBNull(70) ? null : new Medico(dr.GetInt64(70), dr.GetString(71), dr.GetString(72), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty), dr.IsDBNull(77) ? (DateTime?)null : dr.GetDateTime(77), dr.GetBoolean(85), dr.GetBoolean(87)));
                }

                return colecaoExameInAso;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllExamesByAsoInSituacao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public LinkedList<GheFonteAgenteExameAso> findAllExamesByAsoInSituacaoNotExterno(Aso aso, Boolean? atendido, Boolean? finalizado, Boolean? cancelado, Boolean? devolvido, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllExamesByAsoInSituacao");

            LinkedList<GheFonteAgenteExameAso> colecaoExameInAso = new LinkedList<GheFonteAgenteExameAso>();

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT GFAEA.ID_GHE_FONTE_AGENTE_EXAME_ASO, GFAEA.ID_ASO, GFAEA.ID_GHE_FONTE_AGENTE_EXAME,  ");
                query.Append(" GFAEA.DATA_EXAME, GFAEA.ID_SALA_EXAME, GFAEA.ATENDIDO, GFAEA.FINALIZADO, GFAEA.LOGIN, GFAEA.DEVOLVIDO, ");
                query.Append(" GFAEA.DATA_ATENDIDO, GFAEA.DATA_DEVOLVIDO, GFAEA.DATA_FINALIZADO, GFAEA.ID_USUARIO, ");
                query.Append(" GFAE.ID_EXAME, E.DESCRICAO, E.SITUACAO, E.LABORATORIO, E.PRECO, E.PRIORIDADE, E.CUSTO, GFAE.IDADE_EXAME, GFAE.SITUACAO, E.EXTERNO, ");
                query.Append(" S.ID_SALA, S.DESCRICAO, S.SITUACAO, S.ANDAR, S.VIP, GFAEA.CANCELADO, E.LIBERA_DOCUMENTO, GFAE.CONFINADO, GFAE.ALTURA, GFAEA.CONFINADO, GFAEA.ALTURA, E.CODIGO_TUSS, E.EXAME_COMPLEMENTAR, GFAEA.ID_MEDICO, MEDICO.NOME, MEDICO.CRM, GFAEA.DATA_VALIDADE, E.PERIODO_VENCIMENTO, S.ID_EMPRESA, S.SLUG_SALA, E.PADRAO_CONTRATO, S.QTDE_CHAMADA, GFAE.ELETRICIDADE, GFAEA.ELETRICIDADE, GFAEA.IMPRIME_ASO ");
                query.Append(" FROM SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA  ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GFAE ON GFAE.ID_GHE_FONTE_AGENTE_EXAME = GFAEA.ID_GHE_FONTE_AGENTE_EXAME ");
                query.Append(" JOIN SEG_EXAME E ON E.ID_EXAME = GFAE.ID_EXAME AND E.EXTERNO IS FALSE");
                query.Append(" LEFT JOIN SEG_SALA_EXAME SE ON SE.ID_SALA_EXAME = GFAEA.ID_SALA_EXAME ");
                query.Append(" LEFT JOIN SEG_SALA S ON S.ID_SALA =  SE.ID_SALA ");
                query.Append(" LEFT JOIN SEG_MEDICO MEDICO ON MEDICO.ID_MEDICO = GFAEA.ID_MEDICO");
                query.Append(" WHERE GFAEA.ID_ASO = :VALUE1 ");
                query.Append(" AND GFAEA.DUPLICADO = FALSE ");

                if (atendido != null)
                    query.Append(" AND GFAEA.ATENDIDO = :VALUE2 ");

                if (finalizado != null)
                    query.Append(" AND GFAEA.FINALIZADO = :VALUE3 ");

                if (cancelado != null)
                    query.Append(" AND GFAEA.CANCELADO = :VALUE4 ");

                if (devolvido != null)
                    query.Append(" AND GFAEA.DEVOLVIDO = :VALUE5 ");

                query.Append(" AND GFAEA.TRANSCRITO IS FALSE ");
                query.Append(" ORDER BY E.DESCRICAO ASC");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Boolean));
                command.Parameters[1].Value = atendido;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Boolean));
                command.Parameters[2].Value = finalizado;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Boolean));
                command.Parameters[3].Value = cancelado;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Boolean));
                command.Parameters[4].Value = devolvido;

                NpgsqlDataReader dr = command.ExecuteReader();
                
                while (dr.Read())
                {
                    colecaoExameInAso.AddLast(new GheFonteAgenteExameAso(dr.GetInt64(0), dr.IsDBNull(4) ? null : new SalaExame(dr.GetInt64(4), dr.IsDBNull(13) ? null : new Exame(dr.GetInt64(13), dr.GetString(14), dr.GetString(15), dr.GetBoolean(16), dr.GetDecimal(17), dr.GetInt32(18), dr.GetDecimal(19), dr.GetBoolean(22), dr.GetBoolean(29), dr.IsDBNull(34) ? string.Empty : dr.GetString(34), dr.GetBoolean(35), dr.IsDBNull(40) ? (int?)null : dr.GetInt32(40), dr.GetBoolean(43)), new Sala(dr.GetInt64(23), dr.GetString(24), dr.GetString(25), dr.GetInt32(26), dr.GetBoolean(27), new Empresa(dr.GetInt64(41)), dr.GetString(42), dr.GetInt32(44)), null, null, null), aso, dr.IsDBNull(2) ? null : new GheFonteAgenteExame(dr.GetInt64(2), dr.IsDBNull(13) ? null : new Exame(dr.GetInt64(13), dr.GetString(14), dr.GetString(15), dr.GetBoolean(16), dr.GetDecimal(17), dr.GetInt32(18), dr.GetDecimal(19), dr.GetBoolean(22), dr.GetBoolean(29), dr.IsDBNull(34) ? string.Empty : dr.GetString(34), dr.GetBoolean(35), dr.IsDBNull(40) ? (int?)null : dr.GetInt32(40), dr.GetBoolean(43)), null, dr.GetInt32(20), dr.GetString(21), dr.GetBoolean(30), dr.GetBoolean(31), dr.GetBoolean(45)), dr.GetDateTime(3), dr.GetBoolean(5), dr.GetBoolean(6), dr.IsDBNull(7) ? string.Empty : dr.GetString(7), dr.GetBoolean(8), null, null, null, null, dr.GetBoolean(28), null, null, null, false, false, null, null, dr.GetBoolean(32), dr.GetBoolean(33), dr.IsDBNull(36) ? null : new Medico(dr.GetInt64(36), dr.GetString(37), dr.GetString(38), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, null, string.Empty,string.Empty, string.Empty), dr.IsDBNull(39) ? (DateTime?)null : dr.GetDateTime(39), dr.GetBoolean(46), dr.GetBoolean(47)));
                }

                return colecaoExameInAso;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllExamesByAsoInSituacao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public LinkedList<GheFonteAgenteExameAso> findAllExamesByAsoInSituacaoBySala(Aso aso, Boolean? atendido, Boolean? finalizado, Boolean? cancelado, Boolean? devolvido,
            Sala sala, bool ordenacao, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllExamesByAsoInSituacaoBySala");

            LinkedList<GheFonteAgenteExameAso> colecaoExameInAso = new LinkedList<GheFonteAgenteExameAso>();

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT GFAEA.ID_GHE_FONTE_AGENTE_EXAME_ASO, GFAEA.ID_SALA_EXAME, GFAEA.ID_ASO, GFAEA.ID_GHE_FONTE_AGENTE_EXAME,  ");
                query.Append(" GFAEA.DATA_EXAME, GFAEA.ATENDIDO, GFAEA.FINALIZADO, GFAEA.LOGIN, GFAEA.DEVOLVIDO, ");
                query.Append(" GFAEA.DATA_ATENDIDO, GFAEA.DATA_DEVOLVIDO, GFAEA.DATA_FINALIZADO, GFAEA.ID_USUARIO, ");
                query.Append(" GFAEA.CANCELADO, GFAEA.DATA_CANCELADO, GFAEA.RESULTADO, GFAEA.OBSERVACAO_RESULTADO, GFAEA.DUPLICADO, GFAEA.TRANSCRITO, GFAEA.ID_PRESTADOR, ");
                query.Append(" GFAE.ID_EXAME, E.DESCRICAO, E.SITUACAO, E.LABORATORIO, E.PRECO, E.PRIORIDADE, E.CUSTO, E.EXTERNO, GFAE.IDADE_EXAME, GFAE.SITUACAO, ");
                query.Append(" SE.ID_SALA, SE.TEMPO_MEDIO_ATENDIMENTO, SE.TEMPO_MEDIO_ATENDIMENTO_CALCULADO, SE.SITUACAO, SE.LIBERADA, ");
                query.Append(" C.RAZAO_SOCIAL, C.FANTASIA, C.CNPJ, C.INSCRICAO, C.ENDERECO, C.NUMERO, C.COMPLEMENTO, C.BAIRRO, C.CEP, C.UF, ");
                query.Append(" C.TELEFONE1, C.TELEFONE2, C.EMAIL, C.DATA_CADASTRO, C.SITUACAO, C.VIP, C.USA_CONTRATO, C.PARTICULAR, C.UNIDADE, C.CREDENCIADA, C.PRESTADOR, C.ID_CIDADE_IBGE, ");
                query.Append(" CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, ");
                query.Append(" S.DESCRICAO, S.SITUACAO, S.ANDAR, S.VIP, ");
                query.Append(" C.DESTACA_ISS, C.GERA_COBRANCA_VALOR_LIQUIDO, C.ALIQUOTA_ISS, E.LIBERA_DOCUMENTO, GFAE.CONFINADO, GFAE.ALTURA, GFAEA.CONFINADO, GFAEA.ALTURA, C.CODIGO_CNES, E.CODIGO_TUSS, E.EXAME_COMPLEMENTAR, GFAEA.ID_MEDICO, MEDICO.NOME, MEDICO.CRM, C.USA_CENTRO_CUSTO, C.BLOQUEADO, C.CREDENCIADORA, C.FISICA, ATENDIMENTO.CODIGO_ASO, GFAEA.DATA_VALIDADE, E.PERIODO_VENCIMENTO , S.ID_EMPRESA, C.USA_PO, S.SLUG_SALA, E.PADRAO_CONTRATO, S.QTDE_CHAMADA, GFAE.ELETRICIDADE, GFAEA.ELETRICIDADE, C.SIMPLES_NACIONAL, GFAEA.IMPRIME_ASO ");
                query.Append(" FROM SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA  ");
                query.Append(" LEFT JOIN SEG_GHE_FONTE_AGENTE_EXAME GFAE ON GFAE.ID_GHE_FONTE_AGENTE_EXAME = GFAEA.ID_GHE_FONTE_AGENTE_EXAME ");
                query.Append(" LEFT JOIN SEG_EXAME E ON E.ID_EXAME = GFAE.ID_EXAME ");
                query.Append(" LEFT JOIN SEG_SALA_EXAME SE ON E.ID_EXAME = SE.ID_EXAME AND SE.SITUACAO = 'A' ");
                query.Append(" LEFT JOIN SEG_SALA S ON S.ID_SALA = SE.ID_SALA ");
                query.Append(" LEFT JOIN SEG_CLIENTE C ON C.ID_CLIENTE = GFAEA.ID_PRESTADOR");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = C.ID_CIDADE_IBGE ");
                query.Append(" LEFT JOIN SEG_MEDICO MEDICO ON MEDICO.ID_MEDICO = GFAEA.ID_MEDICO ");
                query.Append(" LEFT JOIN SEG_ASO ATENDIMENTO ON ATENDIMENTO.ID_ASO = GFAEA.ID_ASO ");
                query.Append(" WHERE GFAEA.ID_ASO = :VALUE1 ");
                query.Append(" AND SE.ID_SALA = :VALUE6 ");
                query.Append(" AND GFAEA.DUPLICADO = FALSE ");

                if (atendido != null)
                {
                    query.Append(" AND GFAEA.ATENDIDO = :VALUE2 ");
                }

                if (finalizado != null)
                {
                    query.Append(" AND GFAEA.FINALIZADO = :VALUE3 ");
                }

                if (cancelado != null)
                {
                    query.Append(" AND GFAEA.CANCELADO = :VALUE4 ");
                }

                if (devolvido != null)
                {
                    query.Append(" AND GFAEA.DEVOLVIDO = :VALUE5 ");
                }

                query.Append(" AND GFAEA.TRANSCRITO IS FALSE ");

                if (ordenacao) query.Append("ORDER BY ATENDIMENTO.SENHA");
                else query.Append("ORDER BY ATENDIMENTO.CODIGO_ASO ASC ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Boolean));
                command.Parameters[1].Value = atendido;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Boolean));
                command.Parameters[2].Value = finalizado;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Boolean));
                command.Parameters[3].Value = cancelado;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Boolean));
                command.Parameters[4].Value = devolvido;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Integer));
                command.Parameters[5].Value = sala.Id;

                NpgsqlDataReader dr = command.ExecuteReader();
                
                while (dr.Read())
                {
                    colecaoExameInAso.AddLast(new GheFonteAgenteExameAso(dr.GetInt64(0), dr.IsDBNull(1) ? null : new SalaExame(dr.GetInt64(1), new Exame(dr.GetInt64(20), dr.GetString(21), dr.GetString(22), dr.GetBoolean(23), dr.GetDecimal(24), dr.GetInt32(25), dr.GetDecimal(26), dr.GetBoolean(27), dr.GetBoolean(67), dr.IsDBNull(73) ? string.Empty : dr.GetString(73), dr.GetBoolean(74), dr.IsDBNull(84) ? (int?)null : dr.GetInt32(84), dr.GetBoolean(88)), dr.IsDBNull(30) ? null : new Sala(dr.GetInt64(30), dr.GetString(60), dr.GetString(61), dr.GetInt32(62), dr.GetBoolean(63), new Empresa(dr.GetInt64(85)), dr.GetString(87), dr.GetInt32(89)), dr.GetInt32(31), dr.IsDBNull(32) ? (Int32?)null : dr.GetInt32(32), dr.GetString(33)), aso, dr.IsDBNull(3) ? null : new GheFonteAgenteExame(dr.GetInt64(3), new Exame(dr.GetInt64(20), dr.GetString(21), dr.GetString(22), dr.GetBoolean(23), dr.GetDecimal(24), dr.GetInt32(25), dr.GetDecimal(26), dr.GetBoolean(27), dr.GetBoolean(67), dr.IsDBNull(73) ? string.Empty : dr.GetString(73), dr.GetBoolean(74), dr.IsDBNull(84) ? (int?)null : dr.GetInt32(84), dr.GetBoolean(88)), null, dr.GetInt32(28), dr.GetString(29), dr.GetBoolean(68), dr.GetBoolean(69), dr.GetBoolean(90)), dr.IsDBNull(4) ? null : (DateTime?)dr.GetDateTime(4), dr.GetBoolean(5), dr.GetBoolean(6), dr.IsDBNull(7) ? String.Empty : dr.GetString(7), dr.GetBoolean(8), dr.IsDBNull(9) ? null : (DateTime?)dr.GetDateTime(9), dr.IsDBNull(10) ? null : (DateTime?)dr.GetDateTime(10), dr.IsDBNull(11) ? null : (DateTime?)dr.GetDateTime(11), dr.IsDBNull(12) ? null : new Usuario((Int64)dr.GetInt64(12)), dr.GetBoolean(13), dr.IsDBNull(14) ? null : (DateTime?)dr.GetDateTime(14), dr.IsDBNull(15) ? String.Empty : dr.GetString(15), dr.IsDBNull(16) ? String.Empty : dr.GetString(16), dr.GetBoolean(17), dr.GetBoolean(18), dr.IsDBNull(19) ? null : new Cliente(dr.GetInt64(19), dr.GetString(35), dr.GetString(36), dr.GetString(37), dr.GetString(38), dr.GetString(39), dr.GetString(40), dr.GetString(41), dr.GetString(42), dr.GetString(43), dr.GetString(44), dr.GetString(45), dr.GetString(46), dr.GetString(47), String.Empty, dr.GetDateTime(48), String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, dr.GetString(49), 0, 0, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, null, String.Empty, dr.GetBoolean(50), String.Empty, dr.GetBoolean(51), null, null, dr.GetBoolean(52), dr.GetBoolean(53), dr.GetBoolean(54), dr.GetBoolean(55), dr.IsDBNull(56) ? null : new CidadeIbge(dr.GetInt64(56), dr.GetString(57), dr.GetString(58), dr.GetString(59)), null, dr.GetBoolean(64), dr.GetBoolean(65), dr.GetDecimal(66), dr.IsDBNull(72) ? string.Empty : dr.GetString(72), dr.GetBoolean(78), dr.GetBoolean(79), dr.GetBoolean(80), dr.GetBoolean(81), dr.GetBoolean(86), dr.GetBoolean(92)), null, dr.GetBoolean(70), dr.GetBoolean(71), dr.IsDBNull(75) ? null : new Medico(dr.GetInt64(75), dr.GetString(76), dr.GetString(77), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty), dr.IsDBNull(83) ? (DateTime?)null : dr.GetDateTime(83), dr.GetBoolean(91), dr.GetBoolean(93)));
                }

                return colecaoExameInAso;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllExamesByAsoInSituacaoBySala", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void updateGheFonteAgenteExameAsoDaoInFilaAtendimento(ItemFilaAtendimento filaAtendimento, Boolean? atendido, Boolean? finalizado, Boolean? cancelado,
            Boolean? devolvido, Int64? idSalaExame, String tipoAtendimento, Usuario usuario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateGheFonteAgenteExameAsoDaoInFilaAtendimento");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_GHE_FONTE_AGENTE_EXAME_ASO SET ID_SALA_EXAME = :VALUE2, ");
                query.Append(" LOGIN = :VALUE5, ID_USUARIO = :VALUE6");

                if (tipoAtendimento.Equals(ApplicationConstants.ATENDER_EXAME))
                {
                    query.Append(", DATA_ATENDIDO = NOW()");
                }
                else if (tipoAtendimento.Equals(ApplicationConstants.FINALIZAR_EXAME))
                {
                    query.Append(", DATA_FINALIZADO = NOW(), DATA_DEVOLVIDO = NULL ");
                }
                else if (tipoAtendimento.Equals(ApplicationConstants.DEVOLVER_EXAME))
                {
                    query.Append(", DATA_DEVOLVIDO = NOW(), DATA_FINALIZADO = NULL");
                }
                else if (tipoAtendimento.Equals(ApplicationConstants.CANCELAR_EXAME))
                {
                    query.Append(", DATA_CANCELADO = NOW()");
                }
                else if (tipoAtendimento.Equals(ApplicationConstants.ESTORNAR_FINALIZACAO))
                {
                    query.Append(", DATA_DEVOLVIDO = NOW(), DATA_ATENDIDO = NULL, DATA_FINALIZADO = NULL ");
                }

                if (atendido != null)
                {
                    query.Append(", ATENDIDO = :VALUE3");
                }

                if (finalizado != null)
                {
                    query.Append(", FINALIZADO = :VALUE4");
                }

                if (cancelado != null)
                {
                    query.Append(", CANCELADO = :VALUE7");
                }

                if (devolvido != null)
                {
                    query.Append(", DEVOLVIDO = :VALUE8");
                }

                query.Append(" WHERE ID_GHE_FONTE_AGENTE_EXAME_ASO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Boolean));
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Boolean));
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                
                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Integer));
                
                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Boolean));
                
                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Boolean));

                command.Parameters[0].Value = filaAtendimento.IdItem;
                command.Parameters[1].Value = idSalaExame;
                command.Parameters[2].Value = atendido;
                command.Parameters[3].Value = finalizado;
                command.Parameters[4].Value = usuario.Login;
                command.Parameters[5].Value = usuario.Id;
                command.Parameters[6].Value = cancelado;
                command.Parameters[7].Value = devolvido;
                
                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateGheFonteAgenteExameAsoDaoInFilaAtendimento", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean verificaGheFonteAgenteExameASOInSituacao(Int64? idGheFonteAgenteExameAso, Boolean? atendido, Boolean? finalizado,
            Boolean? cancelado, Boolean? devolvido, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaGheFonteAgenteExameASOInSituacao");

            Boolean emAtendimento = false;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ATENDIDO FROM SEG_GHE_FONTE_AGENTE_EXAME_ASO ");
                query.Append(" WHERE ID_GHE_FONTE_AGENTE_EXAME_ASO = :VALUE1 ");

                if (atendido != null)
                {
                    query.Append(" AND ATENDIDO = :VALUE2 ");
                }

                if (finalizado != null)
                {
                    query.Append(" AND FINALIZADO = :VALUE3 ");
                }

                if (cancelado != null)
                {
                    query.Append(" AND CANCELADO = :VALUE4 ");
                }

                if (devolvido != null)
                {
                    query.Append(" AND DEVOLVIDO  = :VALUE5 ");
                }

                query.Append(" AND TRANSCRITO IS FALSE ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = idGheFonteAgenteExameAso;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Boolean));
                command.Parameters[1].Value = atendido;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Boolean));
                command.Parameters[2].Value = finalizado;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Boolean));
                command.Parameters[3].Value = devolvido;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Boolean));
                command.Parameters[4].Value = cancelado;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    if (dr.GetBoolean(0))
                    {
                        emAtendimento = true;
                    }
                }

                return emAtendimento;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - validaGheFonteAgenteExameAsoSendoAtendido", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public Boolean verificaExameAtendimento(Aso aso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaExameAtendimento");

            Boolean retorno = false;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_ASO, ID_GHE_FONTE_AGENTE_EXAME, DATA_EXAME, ID_SALA_EXAME, ATENDIDO, ");
                query.Append(" FINALIZADO, LOGIN, DEVOLVIDO, DATA_ATENDIDO, DATA_DEVOLVIDO, DATA_FINALIZADO, ");
                query.Append(" ID_USUARIO, CANCELADO, DATA_CANCELADO FROM SEG_GHE_FONTE_AGENTE_EXAME_ASO ");
                query.Append(" WHERE ID_ASO = :VALUE1 AND ATENDIDO = TRUE AND FINALIZADO = FALSE AND CANCELADO = FALSE ");
                query.Append(" AND TRANSCRITO = FALSE ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    retorno = true;
                }

                return retorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaExameAtendimento", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public void cancelaExameAso(Aso aso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método cancelaExameAso");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_GHE_FONTE_AGENTE_EXAME_ASO SET ");
                query.Append(" CANCELADO = TRUE, DATA_CANCELADO = :VALUE2 WHERE ");
                query.Append(" ID_ASO = :VALUE1");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                command.Parameters[1].Value = DateTime.Now;
                
                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - cancelaExameAso", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int32 buscaMaiorPrioridade(Aso aso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método buscaMaiorPrioridade");

            try
            {
                Int32 maiorPrioridade = 0;

                StringBuilder query = new StringBuilder();

                query.Append(" SELECT COALESCE(MAX(E.PRIORIDADE), 0) FROM SEG_EXAME E, SEG_ASO A, SEG_GHE_FONTE_AGENTE_EXAME GFAE, SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA ");
                query.Append(" WHERE GFAEA.ID_GHE_FONTE_AGENTE_EXAME = GFAE.ID_GHE_FONTE_AGENTE_EXAME ");
                query.Append(" AND GFAEA.ID_ASO = A.ID_ASO");
                query.Append(" AND GFAE.ID_EXAME = E.ID_EXAME");
                query.Append(" AND GFAEA.FINALIZADO = FALSE");
                query.Append(" AND GFAEA.CANCELADO = FALSE");
                query.Append(" AND GFAEA.TRANSCRITO = FALSE");
                query.Append(" AND A.ID_ASO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    maiorPrioridade = dr.GetInt32(0);
                }

                return maiorPrioridade;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - buscaMaiorPrioridade", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean permiteExcluirGhe(Ghe ghe, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método permiteExcluirGhe");

            try
            {
                Boolean podeExcluir = true;

                StringBuilder query = new StringBuilder();

                query.Append(" SELECT COUNT(*) FROM SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA, SEG_GHE_FONTE_AGENTE_EXAME GFAE, SEG_GHE_FONTE_AGENTE GFA, SEG_GHE_FONTE GF");
                query.Append(" WHERE GFAEA.ID_GHE_FONTE_AGENTE_EXAME = GFAE.ID_GHE_FONTE_AGENTE_EXAME");
                query.Append(" AND GFAE.ID_GHE_FONTE_AGENTE = GFA.ID_GHE_FONTE_AGENTE");
                query.Append(" AND GFA.ID_GHE_FONTE = GF.ID_GHE_FONTE");
                query.Append(" AND GF.ID_GHE = :VALUE1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = ghe.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    if (dr.GetInt64(0) > 0)
                    {
                        podeExcluir = false;
                    }
                }

                return podeExcluir;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - permiteExcluirGhe", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public HashSet<GheFonteAgenteExameAso> buscaExamesNaoAtendidosComMaiorprioridade(Aso aso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método buscaExamesNaoAtendidosComMaiorprioridade");

            try
            {
                HashSet<GheFonteAgenteExameAso> gheFonteAgenteExameAso = new HashSet<GheFonteAgenteExameAso>();

                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GFAEA.ID_GHE_FONTE_AGENTE_EXAME_ASO, GFAEA.ID_ASO, ");
                query.Append(" GFAEA.ID_GHE_FONTE_AGENTE_EXAME, GFAEA.DATA_EXAME, GFAEA.ID_SALA_EXAME, ");
                query.Append(" GFAEA.ATENDIDO, GFAEA.FINALIZADO, GFAEA.LOGIN, GFAEA.DEVOLVIDO, ");
                query.Append(" GFAEA.DATA_ATENDIDO, GFAEA.DATA_DEVOLVIDO, GFAEA.DATA_FINALIZADO, ");
                query.Append(" GFAEA.ID_USUARIO, GFAEA.CANCELADO, GFAEA.DATA_CANCELADO, ");
                query.Append(" GFAE.ID_GHE_FONTE_AGENTE_EXAME, GFAE.ID_EXAME, GFAE.ID_GHE_FONTE_AGENTE, GFAE.IDADE_EXAME, GFAEA.DUPLICADO, GFAEA.CONFINADO, GFAEA.ALTURA, GFAEA.ID_MEDICO, GFAEA.DATA_VALIDADE, GFAEA.ELETRICIDADE, GFAEA.IMPRIME_ASO ");
                query.Append(" FROM SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA, SEG_GHE_FONTE_AGENTE_EXAME GFAE, SEG_EXAME E");
                query.Append(" WHERE GFAEA.ID_GHE_FONTE_AGENTE_EXAME = GFAE.ID_GHE_FONTE_AGENTE_EXAME");
                query.Append(" AND GFAE.ID_EXAME = E.ID_EXAME");
                query.Append(" AND E.PRIORIDADE = (SELECT MAX(E.PRIORIDADE) FROM SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA, SEG_GHE_FONTE_AGENTE_EXAME GFAE, SEG_EXAME E");
                query.Append(" WHERE GFAEA.ID_GHE_FONTE_AGENTE_EXAME = GFAE.ID_GHE_FONTE_AGENTE_EXAME");
                query.Append(" AND GFAE.ID_EXAME = E.ID_EXAME AND GFAEA.ATENDIDO = FALSE AND GFAEA.DUPLICADO = FALSE AND GFAEA.CANCELADO = FALSE AND E.EXTERNO = FALSE AND GFAEA.ID_ASO = :VALUE1)");
                query.Append(" AND GFAEA.ATENDIDO = FALSE");
                query.Append(" AND GFAEA.DUPLICADO = FALSE");
                query.Append(" AND GFAEA.CANCELADO = FALSE");
                query.Append(" AND GFAEA.TRANSCRITO = FALSE");
                query.Append(" AND E.EXTERNO = FALSE");
                query.Append(" AND GFAEA.ID_ASO = :VALUE1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gheFonteAgenteExameAso.Add(new GheFonteAgenteExameAso(dr.GetInt64(0), null, aso, new GheFonteAgenteExame(dr.GetInt64(2)), dr.GetDateTime(3), dr.GetBoolean(5), dr.GetBoolean(6), dr.IsDBNull(7) ? string.Empty : dr.GetString(7), dr.GetBoolean(8), null, null, null, null, dr.GetBoolean(13), null, null, null, dr.GetBoolean(19), false,null, null, dr.GetBoolean(20), dr.GetBoolean(21), dr.IsDBNull(22) ? null : new Medico(dr.GetInt64(22)), dr.IsDBNull(23) ? (DateTime?)null : dr.GetDateTime(23), dr.GetBoolean(24), dr.GetBoolean(25)));
                }

                return gheFonteAgenteExameAso;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - buscaExamesNaoAtendidosComMaiorprioridade", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int32 buscaQuantidadesExamesSendoAtendidosEmUmaSala(SalaExame salaExame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método buscaQuantidadesExamesSendoAtendidosEmUmaSala");

            try
            {
                Int32 quantidadeAtendimentos = 0;

                StringBuilder query = new StringBuilder();

                query.Append(" SELECT CAST(COUNT(*) AS INTEGER) FROM SEG_GHE_FONTE_AGENTE_EXAME_ASO ");
                query.Append(" WHERE ID_SALA_EXAME = :VALUE1 ");
                query.Append(" AND ATENDIDO = TRUE AND FINALIZADO = FALSE ");
                query.Append(" AND DUPLICADO = FALSE");
                query.Append(" AND CANCELADO = FALSE");
                query.Append(" AND transcrito = FALSE");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = salaExame.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    if (dr.GetInt32(0) > 0)
                    {
                        quantidadeAtendimentos = dr.GetInt32(0);
                    }
                }

                return quantidadeAtendimentos;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - buscaQuantidadesExamesSendoAtendidosEmUmaSala", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void IniciarAtendimentoExame(GheFonteAgenteExameAso gheFonteAgenteExameAso, SalaExame salaExame, Boolean funcionarioVip, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método IniciarAtendimentoExame");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_GHE_FONTE_AGENTE_EXAME_ASO SET ID_SALA_EXAME = :VALUE1, ATENDIDO = TRUE,");
                
                if (funcionarioVip)
                {
                    query.Append(" DATA_ATENDIDO = CURRENT_DATE");
                }
                else
                {
                    query.Append(" DATA_ATENDIDO = NOW()");
                }
                
                query.Append(" WHERE ID_GHE_FONTE_AGENTE_EXAME_ASO = :VALUE2");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = salaExame.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = gheFonteAgenteExameAso.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - IniciarAtendimentoExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public HashSet<GheFonteAgenteExameAso> buscaExamesNaoAtendidosComMesmaPrioridadeAtendidosNaMesmaSala(Aso aso, Int32 prioridade, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método buscaExamesNaoAtendidosComMaiorprioridade");

            try
            {
                HashSet<GheFonteAgenteExameAso> gheFonteAgenteExameAso = new HashSet<GheFonteAgenteExameAso>();

                StringBuilder query = new StringBuilder();

                query.Append(" SELECT GFAEA.ID_GHE_FONTE_AGENTE_EXAME_ASO, GFAEA.ID_ASO, ");
                query.Append(" GFAEA.ID_GHE_FONTE_AGENTE_EXAME, GFAEA.DATA_EXAME, GFAEA.ID_SALA_EXAME, ");
                query.Append(" GFAEA.ATENDIDO, GFAEA.FINALIZADO, GFAEA.LOGIN, GFAEA.DEVOLVIDO, ");
                query.Append(" GFAEA.DATA_ATENDIDO, GFAEA.DATA_DEVOLVIDO, GFAEA.DATA_FINALIZADO, ");
                query.Append(" GFAEA.ID_USUARIO, GFAEA.CANCELADO, GFAEA.DATA_CANCELADO, ");
                query.Append(" GFAE.ID_GHE_FONTE_AGENTE_EXAME, GFAE.ID_EXAME, GFAE.ID_GHE_FONTE_AGENTE, GFAE.IDADE_EXAME, GFAEA.DUPLICADO, GFAEA.CONFINADO, GFAEA.ALTURA, GFAEA.DATA_VALIDADE, GFAEA.ELETRICIDADE, GFAEA.ID_MEDICO, GFAEA.IMPRIME_ASO ");
                query.Append(" FROM SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA, SEG_GHE_FONTE_AGENTE_EXAME GFAE, SEG_EXAME E ");
                query.Append(" WHERE GFAEA.ID_GHE_FONTE_AGENTE_EXAME = GFAE.ID_GHE_FONTE_AGENTE_EXAME");
                query.Append(" AND GFAE.ID_EXAME = E.ID_EXAME");
                query.Append(" AND E.PRIORIDADE = :VALUE2");
                query.Append(" AND GFAEA.ATENDIDO = FALSE");
                query.Append(" AND GFAEA.DUPLICADO = FALSE");
                query.Append(" AND GFAEA.CANCELADO = FALSE");
                query.Append(" AND GFAEA.TRANSCRITO = FALSE");
                query.Append(" AND E.EXTERNO = FALSE");
                query.Append(" AND GFAEA.ID_ASO = :VALUE1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = prioridade;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gheFonteAgenteExameAso.Add(new GheFonteAgenteExameAso(dr.GetInt64(0), null, aso, new GheFonteAgenteExame(dr.GetInt64(2)), dr.GetDateTime(3), dr.GetBoolean(5), dr.GetBoolean(6), dr.IsDBNull(7) ? string.Empty : dr.GetString(7), dr.GetBoolean(8), null, null, null, null, dr.GetBoolean(13), null, null, null, dr.GetBoolean(19), false,null, null, dr.GetBoolean(20), dr.GetBoolean(21), dr.IsDBNull(24) ? null : new Medico(dr.GetInt64(24)), dr.IsDBNull(22) ? (DateTime?)null : dr.GetDateTime(22), dr.GetBoolean(23), dr.GetBoolean(25)));
                }

                return gheFonteAgenteExameAso;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - buscaExamesNaoAtendidosComMaiorprioridade", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public HashSet<Int64> findAllExameInAso(Aso aso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllExameByAso");

            try
            {
                HashSet<Int64> exameRetornoSet = new HashSet<Int64>();

                StringBuilder query = new StringBuilder();

                query.Append(" SELECT DISTINCT EX.ID_EXAME FROM SEG_EXAME EX ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME GFAE ON GFAE.ID_EXAME = EX.ID_EXAME ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA ON GFAEA.ID_GHE_FONTE_AGENTE_EXAME = GFAE.ID_GHE_FONTE_AGENTE_EXAME ");
                query.Append(" WHERE GFAEA.ID_ASO = :VALUE1 AND CANCELADO <> 'TRUE' AND TRANSCRITO IS FALSE ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    Int64 id = dr.GetInt64(0);
                    exameRetornoSet.Add(id);
                }

                return exameRetornoSet;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllExameByAso", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean isPrimeiroAtendimento(Aso aso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método isPrimeiroAtendimento");

            StringBuilder query = new StringBuilder();

            Boolean primeiroAtendimento = true;

            try
            {
                query.Append(" SELECT COUNT(*) FROM SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA ");
                query.Append(" WHERE GFAEA.ID_ASO = :VALUE1 ");
                query.Append(" AND GFAEA.ATENDIDO = TRUE");
                query.Append(" AND GFAEA.TRANSCRITO IS FALSE ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    if (dr.GetInt64(0) > 0)
                    {
                        primeiroAtendimento = false;
                    }
                }

                return primeiroAtendimento;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - isPrimeiroAtendimento", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void alteraGheFonteAgenteExameAso(GheFonteAgenteExameAso gheFonteAgenteExameAso, NpgsqlConnection dbconnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alteraGheFonteAgenteExameAso");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_GHE_FONTE_AGENTE_EXAME_ASO SET ");
                query.Append(" DATA_EXAME = :VALUE2, TRANSCRITO = :VALUE3, DATA_VALIDADE = :VALUE4, IMPRIME_ASO = :VALUE5 WHERE ");
                query.Append(" ID_GHE_FONTE_AGENTE_EXAME_ASO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbconnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonteAgenteExameAso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Date));
                command.Parameters[1].Value = gheFonteAgenteExameAso.DataExame;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Boolean));
                command.Parameters[2].Value = gheFonteAgenteExameAso.Transcrito;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Date));
                command.Parameters[3].Value = gheFonteAgenteExameAso.DataValidade;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Boolean));
                command.Parameters[4].Value = gheFonteAgenteExameAso.ImprimeASO;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alteraGheFonteAgenteExameAso", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void updateInformacaoLaudo(GheFonteAgenteExameAso gheFonteAgenteExameAso, NpgsqlConnection dbconnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateInformacaoLaudo");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_GHE_FONTE_AGENTE_EXAME_ASO SET ");
                query.Append(" RESULTADO = :VALUE2, OBSERVACAO_RESULTADO = :VALUE3, ID_MEDICO = :VALUE4 WHERE ");
                query.Append(" ID_GHE_FONTE_AGENTE_EXAME_ASO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbconnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonteAgenteExameAso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = gheFonteAgenteExameAso.Resultado;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Text));
                command.Parameters[2].Value = !String.IsNullOrEmpty(gheFonteAgenteExameAso.Observacao) ? gheFonteAgenteExameAso.Observacao.ToUpper() : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = gheFonteAgenteExameAso.Medico != null ? gheFonteAgenteExameAso.Medico.Id : (long?)null;
                
                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateInformacaoLaudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void excluirPrestadorGheFonteAgenteExameAso(GheFonteAgenteExameAso gheFonteAgenteExameAso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método excluirPrestadorGheFonteAgenteExameAso");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_GHE_FONTE_AGENTE_EXAME_ASO SET ID_PRESTADOR = NULL WHERE ID_GHE_FONTE_AGENTE_EXAME_ASO = :VALUE1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonteAgenteExameAso.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - excluirPrestadorGheFonteAgenteExameAso", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void incluirPrestadorGheFonteAgenteExameAso(GheFonteAgenteExameAso gheFonteAgenteExameAso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirPrestadorGheFonteAgenteExameAso");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_GHE_FONTE_AGENTE_EXAME_ASO SET ID_PRESTADOR = :VALUE2 WHERE ID_GHE_FONTE_AGENTE_EXAME_ASO = :VALUE1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonteAgenteExameAso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = gheFonteAgenteExameAso.Prestador.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirPrestadorGheFonteAgenteExameAso", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Cliente findClienteByGheFonteAgenteExameASo(GheFonteAgenteExameAso gheFonteAgenteExameAso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteByGheFonteAgenteExameASo");

            Cliente cliente = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT C.ID_CLIENTE FROM SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA  ");
                query.Append(" JOIN SEG_ASO ASO ON ASO.ID_ASO = GFAEA.ID_ASO ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CFF ON CFF.ID_CLIENTE_FUNCAO_FUNCIONARIO = ASO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO CF ON CF.ID_SEG_CLIENTE_FUNCAO = CFF.ID_SEG_CLIENTE_FUNCAO ");
                query.Append(" JOIN SEG_CLIENTE C ON C.ID_CLIENTE = CF.ID_CLIENTE  ");
                query.Append(" WHERE GFAEA.ID_GHE_FONTE_AGENTE_EXAME_ASO = :VALUE1 ");


                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = gheFonteAgenteExameAso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    cliente = new Cliente(dr.GetInt64(0));
                }

                return cliente;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllExamesByAsoInSituacao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public GheFonteAgenteExameAso findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            StringBuilder query = new StringBuilder();

            GheFonteAgenteExameAso gheFonteAgenteExameAso = null;

            try
            {
                query.Append(" SELECT ID_GHE_FONTE_AGENTE_EXAME_ASO, ID_SALA_EXAME, ID_ASO, ID_GHE_FONTE_AGENTE_EXAME, ");
                query.Append(" DATA_EXAME, ATENDIDO, FINALIZADO, LOGIN, DEVOLVIDO, DATA_ATENDIDO, DATA_DEVOLVIDO,  ");
                query.Append(" DATA_FINALIZADO, ID_USUARIO, CANCELADO, DATA_CANCELADO, RESULTADO, OBSERVACAO_RESULTADO, ");
                query.Append(" DUPLICADO, TRANSCRITO, ID_PRESTADOR , ID_PROTOCOLO, CONFINADO, ALTURA, ID_MEDICO, DATA_VALIDADE, ELETRICIDADE, IMPRIME_ASO ");
                query.Append(" FROM SEG_GHE_FONTE_AGENTE_EXAME_ASO ");
                query.Append(" WHERE ID_GHE_FONTE_AGENTE_EXAME_ASO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gheFonteAgenteExameAso = new GheFonteAgenteExameAso(dr.GetInt64(0), dr.IsDBNull(1) ? null : new SalaExame(dr.GetInt64(1), null, null, null, null, string.Empty), dr.IsDBNull(2) ? null : new Aso(dr.GetInt64(2)), dr.IsDBNull(3) ? null : new GheFonteAgenteExame(dr.GetInt64(3)), dr.IsDBNull(4) ? null : (DateTime?)dr.GetDateTime(4), dr.GetBoolean(5), dr.GetBoolean(6), dr.IsDBNull(7) ? String.Empty : dr.GetString(7), dr.GetBoolean(8), dr.IsDBNull(9) ? null : (DateTime?)dr.GetDateTime(9), dr.IsDBNull(10) ? null : (DateTime?)dr.GetDateTime(10), dr.IsDBNull(11) ? null : (DateTime?)dr.GetDateTime(11), dr.IsDBNull(12) ? null : new Usuario(dr.GetInt64(12)), dr.GetBoolean(13), dr.IsDBNull(14) ? null : (DateTime?)dr.GetDateTime(14), dr.IsDBNull(15) ? String.Empty : dr.GetString(15), dr.IsDBNull(16) ? String.Empty : dr.GetString(16), dr.GetBoolean(17), dr.GetBoolean(18), dr.IsDBNull(19) ? null : new Cliente(dr.GetInt64(19)), dr.IsDBNull(20) ? null : new Protocolo(dr.GetInt64(20)), dr.GetBoolean(21), dr.GetBoolean(22), dr.IsDBNull(23) ? null : new Medico(dr.GetInt64(23)), dr.IsDBNull(24) ? (DateTime?)null : dr.GetDateTime(24), dr.GetBoolean(25), dr.GetBoolean(26)); 
                }

                return gheFonteAgenteExameAso;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean verificaExisteExameNaoLaudadoNoAtendimentoPorCliente(Cliente cliente, DateTime dataInicial, 
            DateTime dataFinal, Cliente unidade, Boolean matriz, CentroCusto centroCusto, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaExisteExameNaoLaudadoNoAtendimentoPorCliente");

            StringBuilder query = new StringBuilder();

            Boolean retorno = false;

            try
            {
                query.Append(" SELECT COUNT(*) FROM SEG_ASO ATENDIMENTO ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CFF ON CFF.ID_CLIENTE_FUNCAO_FUNCIONARIO = ATENDIMENTO.ID_CLIENTE_FUNCAO_FUNCIONARIO  ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO CF ON CF.ID_SEG_CLIENTE_FUNCAO = CFF.ID_SEG_CLIENTE_FUNCAO  ");
                query.Append(" JOIN SEG_CLIENTE CLIENTE ON CLIENTE.ID_CLIENTE = CF.ID_CLIENTE  ");
                query.Append(" JOIN SEG_GHE_FONTE_AGENTE_EXAME_ASO GFAEA ON GFAEA.ID_ASO = ATENDIMENTO.ID_ASO ");

                if (matriz)
                {
                    query.Append(" JOIN SEG_ESTUDO ESTUDO ON ESTUDO.ID_ESTUDO = ATENDIMENTO.ID_ESTUDO AND ID_CLIENTE_CONTRATADO IS NULL ");
                }
                
                if (unidade != null)
                {
                    query.Append(" JOIN SEG_ESTUDO ESTUDO ON ESTUDO.ID_ESTUDO = ATENDIMENTO.ID_ESTUDO AND ID_CLIENTE_CONTRATADO = :VALUE4 ");

                    
                }

                query.Append(" WHERE  ");
                query.Append(" ATENDIMENTO.DATA_ASO BETWEEN :VALUE2 AND :VALUE3 AND ATENDIMENTO.SITUACAO = 'F' AND ");
                query.Append(" CLIENTE.ID_CLIENTE = :VALUE1 AND");
                query.Append(" GFAEA.RESULTADO IS NULL AND ");
                query.Append(" GFAEA.DUPLICADO =  FALSE ");

                if (centroCusto != null)
                {
                    query.Append(" AND ATENDIMENTO.ID_CENTROCUSTO = :VALUE5 ");
                }

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                command.Parameters[1].Value = dataInicial;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Timestamp));
                command.Parameters[2].Value = dataFinal;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = unidade != null ? unidade.Id : null;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Integer));
                command.Parameters[4].Value = centroCusto != null ? centroCusto.Id : null;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    if (dr.GetInt64(0) > 0)
                    {
                        retorno = true;
                    }
                }

                return retorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaExisteExameNaoLaudadoNoAtendimentoPorCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

    }
}

