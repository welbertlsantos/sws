﻿using SWS.Entidade;
using Npgsql;
using SWS.Excecao;
using System;
using NpgsqlTypes;
using System.Data;
using System.Text;
using SWS.IDao;
using SWS.View.Resources;
using SWS.Helper;

namespace SWS.Dao
{
    class CronogramaDao : ICronogramaDao
    {
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_CRONOGRAMA_ID_CRONOGRAMA_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoIdCronograma", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Cronograma insert(Cronograma cronograma, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            StringBuilder query = new StringBuilder();

            try
            {
                cronograma.Id = recuperaProximoId(dbConnection);

                query.Append(" INSERT INTO SEG_CRONOGRAMA (ID_CRONOGRAMA, DESCRICAO) VALUES (:VALUE1, :VALUE2)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cronograma.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = cronograma.Descricao;
                
                command.ExecuteNonQuery();

                return cronograma;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        
        }

        public Cronograma update(Cronograma cronograma, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_CRONOGRAMA SET DESCRICAO = :VALUE1 WHERE ID_CRONOGRAMA = :VALUE2");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = cronograma.Descricao;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = cronograma.Id;

                command.ExecuteNonQuery();

                return cronograma;
            
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Cronograma findById(Int64 idCronograma, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            Cronograma cronograma = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_CRONOGRAMA, DESCRICAO FROM SEG_CRONOGRAMA WHERE ID_CRONOGRAMA = :VALUE1");
                
                NpgsqlCommand commandCronograma = new NpgsqlCommand(query.ToString(), dbConnection);

                commandCronograma.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                commandCronograma.Parameters[0].Value = idCronograma;

                NpgsqlDataReader drCronograma = commandCronograma.ExecuteReader();

                while (drCronograma.Read())
                {
                    cronograma = new Cronograma(drCronograma.GetInt64(0), drCronograma.GetString(1));
                }

                return cronograma;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Cronograma findByEstudo(Estudo estudo, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByEstudo");

            Cronograma cronograma = null;

            StringBuilder query = new StringBuilder();

            try
            {

                query.Append(" SELECT CRONOGRAMA.ID_CRONOGRAMA, CRONOGRAMA.DESCRICAO FROM SEG_CRONOGRAMA CRONOGRAMA ");
                query.Append(" JOIN SEG_ESTUDO ESTUDO ON ESTUDO.ID_CRONOGRAMA = CRONOGRAMA.ID_CRONOGRAMA ");
                query.Append(" WHERE ESTUDO.ID_ESTUDO = :VALUE1 ");
                
                NpgsqlCommand commandCronograma = new NpgsqlCommand(query.ToString(), dbConnection);

                commandCronograma.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                commandCronograma.Parameters[0].Value = estudo.Id;

                NpgsqlDataReader drCronograma = commandCronograma.ExecuteReader();

                while (drCronograma.Read())
                {
                    cronograma = new Cronograma(drCronograma.GetInt64(0), drCronograma.GetString(1));
                }

                return cronograma;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

    }
}
