﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using SWS.Helper;
using Npgsql;
using NpgsqlTypes;
using SWS.Excecao;

namespace SWS.Dao
{
    class AsoGheSetorDao : IAsoGheSetorDao
    {
        public AsoGheSetor insertAsoGheSetor(AsoGheSetor asoGheSetor, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insertAsoGheSetor");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" INSERT INTO SEG_ASO_GHE_SETOR(ID_ASO, ID_GHE, DESCRICAO_GHE, ID_SETOR, DESCRICAO_SETOR, NUMERO_PO) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6 )");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = asoGheSetor.IdAso;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = asoGheSetor.IdGhe;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = asoGheSetor.DescricaoGhe;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = asoGheSetor.IdSetor;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = asoGheSetor.DescricaoSetor;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = asoGheSetor.NumeroPo;

                command.ExecuteNonQuery();

                return asoGheSetor;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insertAsoGheSetor", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<String> findDescricaoSetoresByAso(Aso aso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findDescricaoSetoresByAso");

            StringBuilder query = new StringBuilder();
            List<String> descricaoSetores = new List<String>();
            try
            {
                query.Append("select distinct descricao_setor from seg_aso_ghe_setor where id_aso = :value1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = aso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    descricaoSetores.Add(dr.GetString(0));
                }

                return descricaoSetores;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findDescricaoSetoresByAso", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<AsoGheSetor> findByAso(Aso aso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByAso");

            StringBuilder query = new StringBuilder();
            List<AsoGheSetor> gheSetores = new List<AsoGheSetor>();
            try
            {
                query.Append(" SELECT ID_ASO, ID_GHE, DESCRICAO_GHE, ID_SETOR, DESCRICAO_SETOR, NUMERO_PO ");
                query.Append(" FROM SEG_ASO_GHE_SETOR WHERE ID_ASO = :VALUE1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    gheSetores.Add(new AsoGheSetor(aso.Id, dr.GetInt64(1), dr.GetString(2), dr.GetInt64(3), dr.GetString(4), dr.IsDBNull(5) ? string.Empty : dr.GetString(5)));
                }

                return gheSetores;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByAso", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

    }
}