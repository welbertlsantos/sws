﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using System.Data;
using SWS.Entidade;
using Npgsql;
using NpgsqlTypes;
using SWS.Excecao;
using SWS.View.Resources;
using SWS.Helper;

namespace SWS.Dao
{
    class NotaDao : INotaDao
    {
        public DataSet findByFilter(Nota nota, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findByFilter");

            try
            {
                StringBuilder query = new StringBuilder();

                if (nota.isNullForFilter())
                    query.Append("SELECT ID_NORMA, DESCRICAO, SITUACAO, CONTEUDO FROM SEG_NORMA ");
                else
                {
                    query.Append("SELECT ID_NORMA, DESCRICAO, SITUACAO, CONTEUDO FROM SEG_NORMA WHERE TRUE");

                    if (!String.IsNullOrWhiteSpace(nota.Descricao))
                        query.Append(" AND DESCRICAO LIKE :VALUE1 ");

                    if (!String.IsNullOrWhiteSpace(nota.Situacao))
                        query.Append(" AND SITUACAO = :VALUE2 ");
                }

                query.Append(" ORDER BY DESCRICAO ASC");

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[0].Value = nota.Descricao + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[1].Value = nota.Situacao;

                DataSet ds = new DataSet();
                adapter.Fill(ds, "Notas");
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public DataSet findByNotaGhe(Nota nota, Ghe ghe, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findNormaGheByFilter");

            try
            {
                StringBuilder query = new StringBuilder();

                if (nota.isNullForFilter())
                {
                    query.Append(" SELECT N.ID_NORMA, N.DESCRICAO, N.SITUACAO, N.CONTEUDO, FALSE FROM SEG_NORMA N WHERE");
                    query.Append(" N.ID_NORMA NOT IN (SELECT G.ID_NORMA FROM SEG_NORMA_GHE G WHERE G.ID_GHE = :VALUE3) ");
                    query.Append(" ORDER BY DESCRICAO ASC");
                }
                else
                {
                    query.Append(" SELECT N.ID_NORMA, N.DESCRICAO, N.SITUACAO, N.CONTEUDO, FALSE FROM SEG_NORMA N WHERE");
                    query.Append(" N.ID_NORMA NOT IN (SELECT G.ID_NORMA FROM SEG_NORMA_GHE G WHERE G.ID_GHE = :VALUE3) ");

                    if (!String.IsNullOrWhiteSpace(nota.Descricao))
                        query.Append(" AND N.DESCRICAO LIKE :VALUE1 ");

                    if (!String.IsNullOrWhiteSpace(nota.Situacao))
                        query.Append(" AND N.SITUACAO = :VALUE2 ");
                }

                query.Append(" order by descricao asc");
                
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[0].Value = nota.Descricao + "%";
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Text));
                adapter.SelectCommand.Parameters[1].Value = nota.Situacao;
                
                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[2].Value = ghe.Id;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Notas");
                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findNormaGheByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public Nota insert(Nota nota, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método incluirNorma");

            StringBuilder query = new StringBuilder();

            try
            {
                nota.Id = recuperaProximoId(dbConnection);

                query.Append(" INSERT INTO SEG_NORMA (ID_NORMA, DESCRICAO, SITUACAO, CONTEUDO ) VALUES ");
                query.Append(" (:VALUE1, :VALUE2, 'A', :VALUE3)");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = nota.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = nota.Descricao;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = nota.Conteudo;

                command.ExecuteNonQuery();

                return nota;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - incluirNorma", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_NORMA_ID_NORMA_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoIdAgente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Nota findByDescricao(Nota nota, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findNormaByDescricao");

            Nota normaRetorno = null;
            
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT ID_NORMA, DESCRICAO, SITUACAO, CONTEUDO FROM SEG_NORMA WHERE DESCRICAO = :VALUE1", dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = nota.Descricao.ToUpper();

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    normaRetorno = new Nota(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3));
                }

                return normaRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findNormaByDescricao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Nota findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findNormaById");

            Nota normaRetorno = null;
            
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT ID_NORMA, DESCRICAO, SITUACAO, CONTEUDO FROM SEG_NORMA WHERE ID_NORMA = :VALUE1", dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    normaRetorno = new Nota(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3));

                }

                return normaRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findNormaById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Nota update(Nota nota, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateNorma");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand(" UPDATE SEG_NORMA SET DESCRICAO = :VALUE2, SITUACAO = :VALUE3, CONTEUDO = :VALUE4 WHERE ID_NORMA = :VALUE1", dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = nota.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = nota.Descricao;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = nota.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = nota.Conteudo;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    nota = new Nota(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3));
                }

                return nota;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateNorma", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean findInGheEstudo(Nota norma, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findNormaByGheEstudo");

            Boolean normaRetorno = Convert.ToBoolean(false);
            Int64 countNorma = 0;
            
            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT COUNT(NG.*) FROM SEG_NORMA_GHE NG, SEG_GHE G, SEG_ESTUDO P WHERE ");
                query.Append(" NG.ID_NORMA = :VALUE1 AND ");
                query.Append(" NG.ID_GHE = G.ID_GHE AND ");
                query.Append(" G.ID_ESTUDO = P.ID_ESTUDO AND ");
                query.Append(" P.SITUACAO = 'F' ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = norma.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    countNorma = (dr.GetInt64(0));
                    
                }

                if (countNorma != 0)
                {
                    normaRetorno = Convert.ToBoolean(true);
                }

                return normaRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findNormaByGheEstudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean findInByGheNota(Nota norma, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findNormaByGhe");

            Boolean normaRetorno = Convert.ToBoolean(false);
            Int64 countNorma = 0;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT COUNT(NG.*) FROM SEG_NORMA_GHE NG WHERE ");
                query.Append(" NG.ID_NORMA = :VALUE1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                
                command.Parameters[0].Value = norma.Id;
                
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    countNorma = (dr.GetInt64(0));
                }

                if (countNorma != 0)
                {
                    normaRetorno = Convert.ToBoolean(true);
                }

                return normaRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findNormaByGhe", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

    }
}
