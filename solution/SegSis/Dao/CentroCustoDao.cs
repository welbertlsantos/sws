﻿using Npgsql;
using NpgsqlTypes;
using SWS.Entidade;
using SWS.Excecao;
using SWS.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;

namespace SWS.Dao
{
    public class CentroCustoDao : ICentroCustoDao
    {
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('PUBLIC.SEG_CENTROCUSTO_ID_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public CentroCusto insert(CentroCusto centroCusto, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            try
            {
                centroCusto.Id = recuperaProximoId(dbConnection);

                StringBuilder query = new StringBuilder();

                query.Append(" INSERT INTO SEG_CENTROCUSTO( ID, ID_CLIENTE, DESCRICAO, SITUACAO)");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, 'A')");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = centroCusto.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = centroCusto.Cliente.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = centroCusto.Descricao;

                command.ExecuteNonQuery();

                return centroCusto;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public CentroCusto update(CentroCusto centroCusto, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método update");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_CENTROCUSTO SET DESCRICAO = :VALUE2, SITUACAO = :VALUE3 ");
                query.Append(" WHERE ID = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = centroCusto.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = centroCusto.Descricao;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = centroCusto.Situacao;

                command.ExecuteNonQuery();

                return centroCusto;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - update", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<CentroCusto> findAtivosByCliente(Cliente cliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAtivosByCliente");

            List<CentroCusto> centroCustoCliente = new List<CentroCusto>();

            StringBuilder query = new StringBuilder();

            try
            {

                query.Append(" SELECT ID, ID_CLIENTE, DESCRICAO, SITUACAO  ");
                query.Append(" FROM SEG_CENTROCUSTO  ");
                query.Append(" WHERE ID_CLIENTE = :VALUE1 AND SITUACAO = 'A' ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = cliente.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    centroCustoCliente.Add(new CentroCusto(dr.GetInt64(0), new Cliente(dr.GetInt64(1)), dr.GetString(2), dr.GetString(3)));
                }

                return centroCustoCliente;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAtivosByCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void delete(CentroCusto centroCusto, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método delete");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" DELETE FROM SEG_CENTROCUSTO WHERE ID = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = centroCusto.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - delete", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public bool isUsed(CentroCusto centroCusto, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método isUsed");

            bool retorno = false;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT * FROM SEG_ASO  ");
                query.Append(" WHERE ID_CENTROCUSTO = :VALUE1  ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = centroCusto.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    retorno = true;
                }

                return retorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - isUsed", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public CentroCusto findById(long id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");

            CentroCusto centroCusto = null;

            StringBuilder query = new StringBuilder();

            try
            {

                query.Append(" SELECT ID, ID_CLIENTE, DESCRICAO, SITUACAO  ");
                query.Append(" FROM SEG_CENTROCUSTO  ");
                query.Append(" WHERE ID = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    centroCusto = new CentroCusto(dr.GetInt64(0), new Cliente(dr.GetInt64(1)), dr.GetString(2), dr.GetString(3));
                }

                return centroCusto;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}