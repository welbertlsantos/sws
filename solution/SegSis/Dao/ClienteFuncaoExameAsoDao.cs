﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWS.IDao;
using SWS.Entidade;
using NpgsqlTypes;
using Npgsql;
using SWS.Excecao;
using SWS.Helper;
using SWS.View.Resources;

namespace SWS.Dao
{
    class ClienteFuncaoExameAsoDao : IClienteFuncaoExameASoDao
    {
        
        public void insert(ClienteFuncaoExameASo clienteFuncaoExameAso,NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            clienteFuncaoExameAso.Id = recuperaProximoId(dbConnection);

            try
            {
                StringBuilder query = new StringBuilder();

                
                query.Append(" INSERT INTO SEG_CLIENTE_FUNCAO_EXAME_ASO ");
                query.Append(" (ID_CLIENTE_FUNCAO_EXAME_ASO, ID_ASO, ID_CLIENTE_FUNCAO_EXAME, DATA_EXAME, TRANSCRITO, ID_PRESTADOR, ID_MEDICO, DATA_VALIDADE, ATENDIDO, FINALIZADO, DATA_ATENDIDO, DATA_FINALIZADO, IMPRIME_ASO)");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6, :VALUE7, :VALUE8, :VALUE9, :VALUE10, :VALUE11, :VALUE12, :VALUE13 )");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncaoExameAso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = clienteFuncaoExameAso.Aso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Integer));
                command.Parameters[2].Value = clienteFuncaoExameAso.ClienteFuncaoExame.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Date));
                command.Parameters[3].Value = clienteFuncaoExameAso.DataExame;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Boolean));
                command.Parameters[4].Value = clienteFuncaoExameAso.Transcrito;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Integer));
                command.Parameters[5].Value = clienteFuncaoExameAso.Prestador != null ? clienteFuncaoExameAso.Prestador.Id : (Int64?)null;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Integer));
                command.Parameters[6].Value = clienteFuncaoExameAso.Medico != null ? clienteFuncaoExameAso.Medico.Id : (Int64?)null;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Date));
                command.Parameters[7].Value = clienteFuncaoExameAso.DataValidade != null ? clienteFuncaoExameAso.DataValidade : (DateTime?)null;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Boolean));
                command.Parameters[8].Value = clienteFuncaoExameAso.Atendido;

                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Boolean));
                command.Parameters[9].Value = clienteFuncaoExameAso.Finalizado;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Date));
                command.Parameters[10].Value = clienteFuncaoExameAso.DataAtendido;

                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Date));
                command.Parameters[11].Value = clienteFuncaoExameAso.DataFinalizado;

                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Boolean));
                command.Parameters[12].Value = clienteFuncaoExameAso.ImprimeASO;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_CLIENTE_FUNCAO_EXAME_ASO_ID_CLIENTE_FUNCAO_EXAME_ASO_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoId", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public List<ClienteFuncaoExameASo> findAllExamesByAsoInSituacao(Aso aso, Boolean? atendido, Boolean? finalizado, 
            Boolean? cancelado, Boolean? devolvido, Boolean? transcrito, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllExamesByAsoInSituacao");

            List<ClienteFuncaoExameASo> colecaoExameInAso = new List<ClienteFuncaoExameASo>();

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT CFEA.ID_CLIENTE_FUNCAO_EXAME_ASO, CFEA.ID_ASO, CFEA.ID_CLIENTE_FUNCAO_EXAME, ");
                query.Append(" CFEA.DATA_EXAME, CFEA.ID_SALA_EXAME, CFEA.ATENDIDO, CFEA.FINALIZADO, CFEA.LOGIN, CFEA.DEVOLVIDO, CFEA.DATA_ATENDIDO, ");
                query.Append(" CFEA.DATA_DEVOLVIDO, CFEA.DATA_FINALIZADO, CFEA.ID_USUARIO, CFE.ID_EXAME, E.DESCRICAO, E.SITUACAO, ");
                query.Append(" E.LABORATORIO, E.PRECO, E.PRIORIDADE, E.CUSTO, E.EXTERNO, S.ID_SALA, S.DESCRICAO, S.SITUACAO, S.ANDAR, S.VIP, CFEA.CANCELADO, CFEA.DATA_CANCELADO, ");
                query.Append(" CFEA.RESULTADO, CFEA.OBSERVACAO_RESULTADO, CFEA.TRANSCRITO, CFEA.ID_PRESTADOR,  ");
                query.Append(" C.RAZAO_SOCIAL, C.FANTASIA, C.CNPJ, C.INSCRICAO, C.ENDERECO, C.NUMERO, C.COMPLEMENTO, C.BAIRRO, C.CEP, ");
                query.Append(" C.UF, C.TELEFONE1, C.TELEFONE2, C.EMAIL, C.DATA_CADASTRO, C.SITUACAO, C.VIP, C.USA_CONTRATO, C.PARTICULAR,  ");
                query.Append(" C.UNIDADE, C.CREDENCIADA, C.PRESTADOR, C.ID_CIDADE_IBGE, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, ");
                query.Append(" C.DESTACA_ISS, C.GERA_COBRANCA_VALOR_LIQUIDO, C.ALIQUOTA_ISS, CFEA.ID_PROTOCOLO, E.LIBERA_DOCUMENTO, C.CODIGO_CNES, E.CODIGO_TUSS, E.EXAME_COMPLEMENTAR, CFEA.ID_MEDICO, MEDICO.NOME, MEDICO.CRM, C.USA_CENTRO_CUSTO, C.BLOQUEADO, C.CREDENCIADORA, C.FISICA, CFEA.DATA_VALIDADE, E.PERIODO_VENCIMENTO, CFE.IDADE_EXAME, S.ID_EMPRESA, C.USA_PO, S.SLUG_SALA, E.PADRAO_CONTRATO, S.QTDE_CHAMADA, C.SIMPLES_NACIONAL, CFEA.IMPRIME_ASO ");
                query.Append(" FROM SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME CFE ON CFE.ID_CLIENTE_FUNCAO_EXAME = CFEA.ID_CLIENTE_FUNCAO_EXAME ");
                query.Append(" LEFT JOIN SEG_SALA_EXAME SE ON SE.ID_SALA_EXAME = CFEA.ID_SALA_EXAME ");
                query.Append(" LEFT JOIN SEG_SALA S ON S.ID_SALA = SE.ID_SALA ");
                query.Append(" JOIN SEG_EXAME E ON E.ID_EXAME = CFE.ID_EXAME  ");
                query.Append(" LEFT JOIN SEG_CLIENTE C ON C.ID_CLIENTE = CFEA.ID_PRESTADOR ");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = C.ID_CIDADE_IBGE ");
                query.Append(" LEFT JOIN SEG_MEDICO MEDICO ON MEDICO.ID_MEDICO = CFEA.ID_MEDICO ");
                query.Append(" WHERE CFEA.ID_ASO = :VALUE1 ");
                
                
                // não pode montar a coleção de exames que foram atendidos e finalizado, Caso seja necessário
                // passar null nos dois parâmetros.

                if (atendido != null)
                    query.Append(" AND CFEA.ATENDIDO = :VALUE2 ");

                if (finalizado != null)
                    query.Append(" AND CFEA.FINALIZADO = :VALUE3 ");

                if (cancelado != null)
                    query.Append(" AND CFEA.CANCELADO = :VALUE4 ");

                if (devolvido != null)
                    query.Append(" AND CFEA.DEVOLVIDO = :VALUE5 ");
                
                if (transcrito != null)
                {
                    if ((Boolean)transcrito)
                        query.Append(" AND CFEA.TRANSCRITO IS TRUE ");
                    else
                        query.Append(" AND CFEA.TRANSCRITO IS FALSE ");
                }

                query.Append(" ORDER BY E.DESCRICAO ASC ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Boolean));
                command.Parameters[1].Value = atendido;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Boolean));
                command.Parameters[2].Value = finalizado;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Boolean));
                command.Parameters[3].Value = cancelado;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Boolean));
                command.Parameters[4].Value = devolvido;

                NpgsqlDataReader dr = command.ExecuteReader();
                

                while (dr.Read())
                {
                    colecaoExameInAso.Add(new ClienteFuncaoExameASo(dr.GetInt64(0), dr.IsDBNull(4) ? null : new SalaExame(dr.GetInt64(4), dr.IsDBNull(13) ? null : new Exame(dr.GetInt64(13), dr.GetString(14), dr.GetString(15), dr.GetBoolean(16), dr.GetDecimal(17), dr.GetInt32(18), dr.GetDecimal(19), dr.GetBoolean(20), dr.GetBoolean(61), dr.IsDBNull(63) ? string.Empty : dr.GetString(63), dr.GetBoolean(64), dr.IsDBNull(73) ? (int?)null : dr.GetInt32(73), dr.GetBoolean(78)), new Sala(dr.GetInt64(21), dr.GetString(22), dr.GetString(23), dr.GetInt32(24), dr.GetBoolean(25), new Empresa(dr.GetInt64(75)), dr.GetString(77), dr.GetInt32(79)), null, null, null), aso, dr.IsDBNull(2) ? null : new ClienteFuncaoExame(dr.GetInt64(2), null, dr.IsDBNull(13) ? null : new Exame(dr.GetInt64(13), dr.GetString(14), dr.GetString(15), dr.GetBoolean(16), dr.GetDecimal(17), dr.GetInt32(18), dr.GetDecimal(19), dr.GetBoolean(20), dr.GetBoolean(61), dr.IsDBNull(63) ? string.Empty : dr.GetString(63), dr.GetBoolean(64), dr.IsDBNull(73) ? (int?)null : dr.GetInt32(73), dr.GetBoolean(78)), null, dr.IsDBNull(74) ? 0 : dr.GetInt32(74)), dr.GetDateTime(3), dr.GetBoolean(5), dr.GetBoolean(6), dr.IsDBNull(7) ? String.Empty : dr.GetString(7), dr.GetBoolean(8), dr.IsDBNull(9) ? (DateTime?)null : dr.GetDateTime(9), dr.IsDBNull(10) ? (DateTime?)null : dr.GetDateTime(10), dr.IsDBNull(11) ? (DateTime?)null : dr.GetDateTime(11), dr.IsDBNull(12) ? null : new Usuario(dr.GetInt64(12)), dr.GetBoolean(26), dr.IsDBNull(27) ? (DateTime?)null : dr.GetDateTime(27), dr.IsDBNull(28) ? String.Empty : dr.GetString(28), dr.IsDBNull(29) ? String.Empty : dr.GetString(29), dr.GetBoolean(30), dr.IsDBNull(31) ? null : new Cliente(dr.GetInt64(31), dr.GetString(32), dr.GetString(33), dr.GetString(34), dr.GetString(35), dr.GetString(36), dr.GetString(37), dr.GetString(38), dr.GetString(39), dr.GetString(40), dr.GetString(41), dr.GetString(42), dr.GetString(43), dr.GetString(44), String.Empty, dr.GetDateTime(45), String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, dr.GetString(46), 0, 0, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, string.Empty, String.Empty, null, String.Empty, dr.GetBoolean(47), String.Empty, dr.GetBoolean(48), null, null, dr.GetBoolean(49), dr.GetBoolean(50), dr.GetBoolean(51), dr.GetBoolean(52), new CidadeIbge(dr.GetInt64(53), dr.GetString(54), dr.GetString(55), dr.GetString(56)), null, dr.GetBoolean(57), dr.GetBoolean(58), dr.GetDecimal(59), dr.IsDBNull(62) ? string.Empty : dr.GetString(62), dr.GetBoolean(68), dr.GetBoolean(69), dr.GetBoolean(70), dr.GetBoolean(71), dr.GetBoolean(76), dr.GetBoolean(80)), dr.IsDBNull(60) ? null : new Protocolo(dr.GetInt64(60), String.Empty, String.Empty, String.Empty, null, null, null, null, String.Empty, null, null, String.Empty, String.Empty, null), dr.IsDBNull(65) ? null : new Medico(dr.GetInt64(65), dr.GetString(66), dr.GetString(67), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty), dr.IsDBNull(72) ? (DateTime?)null : dr.GetDateTime(72), dr.GetBoolean(81)));
                }

                return colecaoExameInAso;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllExamesByAsoInSituacao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public LinkedList<ClienteFuncaoExameASo> findAllExamesByAsoInSituacaoNotExterno(Aso aso, Boolean? atendido, Boolean? finalizado,
            Boolean? cancelado, Boolean? devolvido, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllExamesByAsoInSituacaoNotExterno");

            LinkedList<ClienteFuncaoExameASo> colecaoExameInAso = new LinkedList<ClienteFuncaoExameASo>();

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT CFEA.ID_CLIENTE_FUNCAO_EXAME_ASO, CFEA.ID_ASO, CFEA.ID_CLIENTE_FUNCAO_EXAME, ");
                query.Append(" CFEA.DATA_EXAME, CFEA.ID_SALA_EXAME, CFEA.ATENDIDO, CFEA.FINALIZADO, CFEA.LOGIN, CFEA.DEVOLVIDO, CFEA.DATA_ATENDIDO, ");
                query.Append(" CFEA.DATA_DEVOLVIDO, CFEA.DATA_FINALIZADO, CFEA.ID_USUARIO, CFE.ID_EXAME, E.DESCRICAO, E.SITUACAO, ");
                query.Append(" E.LABORATORIO, E.PRECO, E.PRIORIDADE, E.CUSTO, E.EXTERNO, S.ID_SALA, S.DESCRICAO, S.SITUACAO, S.ANDAR, S.VIP, CFEA.CANCELADO, CFEA.DATA_CANCELADO, ");
                query.Append(" CFEA.RESULTADO, CFEA.OBSERVACAO_RESULTADO, CFEA.TRANSCRITO, CFEA.ID_PRESTADOR,  ");
                query.Append(" C.RAZAO_SOCIAL, C.FANTASIA, C.CNPJ, C.INSCRICAO, C.ENDERECO, C.NUMERO, C.COMPLEMENTO, C.BAIRRO, C.CEP, ");
                query.Append(" C.UF, C.TELEFONE1, C.TELEFONE2, C.EMAIL, C.DATA_CADASTRO, C.SITUACAO, C.VIP, C.USA_CONTRATO, C.PARTICULAR,  ");
                query.Append(" C.UNIDADE, C.CREDENCIADA, C.PRESTADOR, C.ID_CIDADE_IBGE, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, ");
                query.Append(" C.DESTACA_ISS, C.GERA_COBRANCA_VALOR_LIQUIDO, C.ALIQUOTA_ISS, E.LIBERA_DOCUMENTO, C.CODIGO_CNES, E.CODIGO_TUSS, E.EXAME_COMPLEMENTAR, CFEA.ID_MEDICO, MEDICO.NOME, MEDICO.CRM, C.USA_CENTRO_CUSTO, C.BLOQUEADO, C.CREDENCIADORA, C.FISICA, CFEA.DATA_VALIDADE, E.PERIODO_VENCIMENTO, CFE.IDADE_EXAME, S.ID_EMPRESA, C.USA_PO, S.SLUG_SALA, E.PADRAO_CONTRATO, S.QTDE_CHAMADA, C.SIMPLES_NACIONAL, CFEA.IMPRIME_ASO ");
                query.Append(" FROM SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME CFE ON CFE.ID_CLIENTE_FUNCAO_EXAME = CFEA.ID_CLIENTE_FUNCAO_EXAME ");
                query.Append(" LEFT JOIN SEG_SALA_EXAME SE ON SE.ID_SALA_EXAME = CFEA.ID_SALA_EXAME ");
                query.Append(" LEFT JOIN SEG_SALA S ON S.ID_SALA = SE.ID_SALA ");
                query.Append(" LEFT JOIN SEG_CLIENTE C ON C.ID_CLIENTE = CFEA.ID_PRESTADOR");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = C.ID_CIDADE_IBGE");
                query.Append(" LEFT JOIN SEG_MEDICO MEDICO ON MEDICO.ID_MEDICO = CFEA.ID_MEDICO ");
                query.Append(" JOIN SEG_EXAME E ON E.ID_EXAME = CFE.ID_EXAME AND E.EXTERNO IS FALSE ");
                query.Append(" WHERE CFEA.ID_ASO = :VALUE1 ");
                

                // não pode montar a coleção de exames que foram atendidos e finalizado, Caso seja necessário
                // passar null nos dois parâmetros.

                if (atendido != null)
                    query.Append(" AND CFEA.ATENDIDO = :VALUE2 ");

                if (finalizado != null)
                    query.Append(" AND CFEA.FINALIZADO = :VALUE3 ");

                if (cancelado != null)
                    query.Append(" AND CFEA.CANCELADO = :VALUE4 ");

                if (devolvido != null)
                    query.Append(" AND CFEA.DEVOLVIDO = :VALUE5 ");

                query.Append(" AND CFEA.TRANSCRITO IS FALSE ");
                query.Append(" ORDER BY E.DESCRICAO ASC ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Boolean));
                command.Parameters[1].Value = atendido;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Boolean));
                command.Parameters[2].Value = finalizado;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Boolean));
                command.Parameters[3].Value = cancelado;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Boolean));
                command.Parameters[4].Value = devolvido;

                NpgsqlDataReader dr = command.ExecuteReader();
                
                while (dr.Read())
                {
                    colecaoExameInAso.AddLast(new ClienteFuncaoExameASo(dr.GetInt64(0), dr.IsDBNull(4) ? null : new SalaExame(dr.GetInt64(4), new Exame(dr.GetInt64(13), dr.GetString(14), dr.GetString(15), dr.GetBoolean(16), dr.GetDecimal(17), dr.GetInt32(18), dr.GetDecimal(19), dr.GetBoolean(20), dr.GetBoolean(60), dr.IsDBNull(62) ? string.Empty : dr.GetString(62), dr.GetBoolean(63), dr.IsDBNull(73) ? (int?)null : dr.GetInt32(73), dr.GetBoolean(78)), new Sala(dr.GetInt64(21), dr.GetString(22), dr.GetString(23), dr.GetInt32(24), dr.GetBoolean(25), new Empresa(dr.GetInt64(75)), dr.GetString(77), dr.GetInt32(78)), null, null, null), aso, dr.IsDBNull(2) ? null : new ClienteFuncaoExame(dr.GetInt64(2), null, new Exame(dr.GetInt64(13), dr.GetString(14), dr.GetString(15), dr.GetBoolean(16), dr.GetDecimal(17), dr.GetInt32(18), dr.GetDecimal(19), dr.GetBoolean(20), dr.GetBoolean(60), dr.IsDBNull(62) ? string.Empty : dr.GetString(62), dr.GetBoolean(63), dr.IsDBNull(73) ? (int?)null : dr.GetInt32(73), dr.GetBoolean(78)), null, dr.GetInt32(74)), dr.GetDateTime(3), dr.GetBoolean(5), dr.GetBoolean(6), dr.IsDBNull(7) ? String.Empty : dr.GetString(7), dr.GetBoolean(8), dr.IsDBNull(9) ? (DateTime?)null : dr.GetDateTime(9), dr.IsDBNull(10) ? (DateTime?)null : dr.GetDateTime(10), dr.IsDBNull(11) ? (DateTime?)null : dr.GetDateTime(11), dr.IsDBNull(12) ? null : new Usuario(dr.GetInt64(12)), dr.GetBoolean(26), dr.IsDBNull(27) ? (DateTime?)null : dr.GetDateTime(27), dr.IsDBNull(28) ? String.Empty : dr.GetString(28), dr.IsDBNull(29) ? String.Empty : dr.GetString(29), dr.GetBoolean(30), dr.IsDBNull(31) ? null : new Cliente(dr.GetInt64(31), dr.GetString(32), dr.GetString(33), dr.GetString(34), dr.GetString(35), dr.GetString(36), dr.GetString(37), dr.GetString(38), dr.GetString(39), dr.GetString(40), dr.GetString(41), dr.GetString(42), dr.GetString(43), dr.GetString(44), String.Empty, dr.GetDateTime(45), String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, dr.GetString(46), 0, 0, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, null, String.Empty, dr.GetBoolean(47), String.Empty, dr.GetBoolean(48), null, null, dr.GetBoolean(49), dr.GetBoolean(50), dr.GetBoolean(51), dr.GetBoolean(52), new CidadeIbge(dr.GetInt64(53), dr.GetString(54), dr.GetString(55), dr.GetString(56)), null, dr.GetBoolean(57), dr.GetBoolean(58), dr.GetDecimal(59), dr.IsDBNull(61) ? string.Empty : dr.GetString(61), dr.GetBoolean(68), dr.GetBoolean(69), dr.GetBoolean(70), dr.GetBoolean(71), dr.GetBoolean(76), dr.GetBoolean(79)), null, dr.IsDBNull(64) ? null : new Medico(dr.GetInt64(64), dr.GetString(65), dr.GetString(66), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty), dr.IsDBNull(72) ? (DateTime?)null : dr.GetDateTime(72), dr.GetBoolean(80)));
                }

                return colecaoExameInAso;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllExamesByAsoInSituacao", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public LinkedList<ClienteFuncaoExameASo> findAllExamesByAsoInSituacaoBySala(Aso aso, Boolean? atendido, Boolean? finalizado, Boolean? cancelado, Boolean? devolvido, Sala sala, bool ordenacaoSenha, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllExamesByAsoInSituacaoBySala");

            LinkedList<ClienteFuncaoExameASo> colecaoExameInAso = new LinkedList<ClienteFuncaoExameASo>();

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT CFEA.ID_CLIENTE_FUNCAO_EXAME_ASO, CFEA.ID_ASO, CFEA.ID_CLIENTE_FUNCAO_EXAME, ");
                query.Append(" CFEA.DATA_EXAME, CFEA.ID_SALA_EXAME, CFEA.ATENDIDO, CFEA.FINALIZADO, CFEA.LOGIN, CFEA.DEVOLVIDO, CFEA.DATA_ATENDIDO, ");
                query.Append(" CFEA.DATA_DEVOLVIDO, CFEA.DATA_FINALIZADO, CFEA.ID_USUARIO, CFE.ID_EXAME, E.DESCRICAO, E.SITUACAO, ");
                query.Append(" E.LABORATORIO, E.PRECO, E.PRIORIDADE, E.CUSTO, E.EXTERNO, S.ID_SALA, S.DESCRICAO, S.SITUACAO, S.ANDAR, S.VIP, CFEA.CANCELADO, CFEA.DATA_CANCELADO, ");
                query.Append(" CFEA.RESULTADO, CFEA.OBSERVACAO_RESULTADO, CFEA.TRANSCRITO, CFEA.ID_PRESTADOR,  ");
                query.Append(" C.RAZAO_SOCIAL, C.FANTASIA, C.CNPJ, C.INSCRICAO, C.ENDERECO, C.NUMERO, C.COMPLEMENTO, C.BAIRRO, C.CEP, ");
                query.Append(" C.UF, C.TELEFONE1, C.TELEFONE2, C.EMAIL, C.DATA_CADASTRO, C.SITUACAO, C.VIP, C.USA_CONTRATO, C.PARTICULAR, ");
                query.Append(" C.UNIDADE, C.CREDENCIADA, C.PRESTADOR, C.ID_CIDADE_IBGE, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE, ");
                query.Append(" C.DESTACA_ISS, C.GERA_COBRANCA_VALOR_LIQUIDO, C.ALIQUOTA_ISS, E.LIBERA_DOCUMENTO, C.CODIGO_CNES, E.CODIGO_TUSS, E.EXAME_COMPLEMENTAR, CFEA.ID_MEDICO, MEDICO.NOME, MEDICO.CRM, C.USA_CENTRO_CUSTO, C.BLOQUEADO, C.CREDENCIADORA, C.FISICA, ATENDIMENTO.CODIGO_ASO, CFEA.DATA_VALIDADE, E.PERIODO_VENCIMENTO, CFE.IDADE_EXAME, C.USA_PO, E.PADRAO_CONTRATO, C.SIMPLES_NACIONAL, CFEA.IMPRIME_ASO ");
                query.Append(" FROM SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME CFE ON CFE.ID_CLIENTE_FUNCAO_EXAME = CFEA.ID_CLIENTE_FUNCAO_EXAME ");
                query.Append(" JOIN SEG_EXAME E ON E.ID_EXAME = CFE.ID_EXAME ");
                query.Append(" LEFT JOIN SEG_SALA_EXAME SE ON SE.ID_EXAME = E.ID_EXAME AND SE.SITUACAO = 'A' ");
                query.Append(" LEFT JOIN SEG_SALA S ON S.ID_SALA = SE.ID_SALA ");
                query.Append(" LEFT JOIN SEG_CLIENTE C ON C.ID_CLIENTE = CFEA.ID_PRESTADOR");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = C.ID_CIDADE_IBGE ");
                query.Append(" LEFT JOIN SEG_MEDICO MEDICO ON MEDICO.ID_MEDICO = CFEA.ID_MEDICO ");
                query.Append(" LEFT JOIN SEG_ASO ATENDIMENTO ON  ATENDIMENTO.ID_ASO = CFEA.ID_ASO");
                query.Append(" WHERE CFEA.ID_ASO = :VALUE1 ");
                query.Append(" AND SE.ID_SALA = :VALUE6 ");

                // não pode montar a coleção de exames que foram atendidos e finalizado, Caso seja necessário
                // passar null nos dois parâmetros.

                if (atendido != null)
                    query.Append(" AND CFEA.ATENDIDO = :VALUE2 ");

                if (finalizado != null)
                    query.Append(" AND CFEA.FINALIZADO = :VALUE3 ");

                if (cancelado != null)
                    query.Append(" AND CFEA.CANCELADO = :VALUE4 ");

                if (devolvido != null)
                    query.Append(" AND CFEA.DEVOLVIDO = :VALUE5 ");

                query.Append(" AND CFEA.TRANSCRITO IS FALSE ");

                if (ordenacaoSenha) query.Append("ORDER BY ATENDIMENTO.SENHA ASC ");
                else query.Append(" ORDER BY ATENDIMENTO.CODIGO_ASO ASC ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Boolean));
                command.Parameters[1].Value = atendido;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Boolean));
                command.Parameters[2].Value = finalizado;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Boolean));
                command.Parameters[3].Value = cancelado;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Boolean));
                command.Parameters[4].Value = devolvido;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Integer));
                command.Parameters[5].Value = sala.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    colecaoExameInAso.AddLast(new ClienteFuncaoExameASo(dr.GetInt64(0), dr.IsDBNull(4) ? null : new SalaExame(dr.GetInt64(4), new Exame(dr.GetInt64(13), dr.GetString(14), dr.GetString(15), dr.GetBoolean(16), dr.GetDecimal(17), dr.GetInt32(18), dr.GetDecimal(19), dr.GetBoolean(20), dr.GetBoolean(60), dr.IsDBNull(62) ? string.Empty : dr.GetString(62), dr.GetBoolean(63), dr.IsDBNull(73) ? (int?)null : dr.GetInt32(73), dr.GetBoolean(76)), sala, null, null, null), aso, dr.IsDBNull(2) ? null : new ClienteFuncaoExame(dr.GetInt64(2), null, new Exame(dr.GetInt64(13), dr.GetString(14), dr.GetString(15), dr.GetBoolean(16), dr.GetDecimal(17), dr.GetInt32(18), dr.GetDecimal(19), dr.GetBoolean(20), dr.GetBoolean(60), dr.IsDBNull(62) ? string.Empty : dr.GetString(62), dr.GetBoolean(63), dr.IsDBNull(73) ? (int?)null : dr.GetInt32(73), dr.GetBoolean(76)), null, dr.GetInt32(74)), dr.GetDateTime(3), dr.GetBoolean(5), dr.GetBoolean(6), dr.IsDBNull(7) ? String.Empty : dr.GetString(7), dr.GetBoolean(8), dr.IsDBNull(9) ? (DateTime?)null : dr.GetDateTime(9), dr.IsDBNull(10) ? (DateTime?)null : dr.GetDateTime(10), dr.IsDBNull(11) ? (DateTime?)null : dr.GetDateTime(11), dr.IsDBNull(12) ? null : new Usuario(dr.GetInt64(12)), dr.GetBoolean(26), dr.IsDBNull(27) ? (DateTime?)null : dr.GetDateTime(27), dr.IsDBNull(28) ? String.Empty : dr.GetString(28), dr.IsDBNull(29) ? String.Empty : dr.GetString(29), dr.GetBoolean(30), dr.IsDBNull(31) ? null : new Cliente(dr.GetInt64(31), dr.GetString(32), dr.GetString(33), dr.GetString(34), dr.GetString(35), dr.GetString(36), dr.GetString(37), dr.GetString(38), dr.GetString(39), dr.GetString(40), dr.GetString(41), dr.GetString(42), dr.GetString(43), dr.GetString(44), String.Empty, dr.GetDateTime(45), String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, dr.GetString(46), 0, 0, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, null, String.Empty, dr.GetBoolean(47), String.Empty, dr.GetBoolean(48), null, null, dr.GetBoolean(49), dr.GetBoolean(50), dr.GetBoolean(51), dr.GetBoolean(52), new CidadeIbge(dr.GetInt64(53), dr.GetString(54), dr.GetString(55), dr.GetString(56)), null, dr.GetBoolean(57), dr.GetBoolean(58), dr.GetDecimal(59), dr.IsDBNull(61) ? null : dr.GetString(61), dr.GetBoolean(68), dr.GetBoolean(69), dr.GetBoolean(70), dr.GetBoolean(71),  dr.GetBoolean(75), dr.GetBoolean(77)), null, dr.IsDBNull(64) ? null : new Medico(dr.GetInt64(64), dr.GetString(65), dr.GetString(66), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty), dr.IsDBNull(72) ? (DateTime?)null : dr.GetDateTime(72), dr.GetBoolean(78)));  
                }

                return colecaoExameInAso;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllExamesByAsoInSituacaoBySala", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void updateClienteFuncaoExameASoDaoInFilaAtendimento(ItemFilaAtendimento filaAtendimento, Boolean? atendido, Boolean? finalizado,
            Boolean? cancelado, Boolean? devolvido, Int64? idSalaExame, String tipoAtendimento, Usuario usuario, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateClienteFuncaoExameASoDaoInFilaAtendimento");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_CLIENTE_FUNCAO_EXAME_ASO SET ID_SALA_EXAME = :VALUE2,");
                query.Append(" LOGIN = :VALUE5, ID_USUARIO = :VALUE6");

                if (tipoAtendimento.Equals(ApplicationConstants.ATENDER_EXAME))
                    query.Append(", DATA_ATENDIDO = NOW()");
                
                else if (tipoAtendimento.Equals(ApplicationConstants.FINALIZAR_EXAME))
                {
                    query.Append(", DATA_FINALIZADO = NOW(), DATA_DEVOLVIDO = NULL ");
                }
                else if (tipoAtendimento.Equals(ApplicationConstants.DEVOLVER_EXAME))
                {
                    query.Append(", DATA_DEVOLVIDO = NOW(), DATA_FINALIZADO = NULL");
                }
                else if (tipoAtendimento.Equals(ApplicationConstants.CANCELAR_EXAME))
                {
                    query.Append(", DATA_CANCELADO = NOW()");
                }
                else if (tipoAtendimento.Equals(ApplicationConstants.ESTORNAR_FINALIZACAO))
                {
                    query.Append(", DATA_ATENDIDO= NULL, DATA_FINALIZADO = NULL, DATA_DEVOLVIDO = NOW()");
                }
                
                if (atendido != null)
                    query.Append(", ATENDIDO = :VALUE3");

                if (finalizado != null)
                    query.Append(", FINALIZADO = :VALUE4");

                if (cancelado != null)
                    query.Append(", CANCELADO = :VALUE7");

                if (devolvido != null)
                    query.Append(", DEVOLVIDO = :VALUE8");
                
                query.Append(" WHERE ID_CLIENTE_FUNCAO_EXAME_ASO = :VALUE1");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = filaAtendimento.IdItem;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = idSalaExame;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Boolean));
                command.Parameters[2].Value = atendido;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Boolean));
                command.Parameters[3].Value = finalizado;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = usuario.Login;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Integer));
                command.Parameters[5].Value = usuario.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Boolean));
                command.Parameters[6].Value = cancelado;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Boolean));
                command.Parameters[7].Value = devolvido;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateClienteFuncaoExameASoDaoInFilaAtendimento", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean verificaClienteFuncaoExameAsoInSituacao(Int64? idClienteFuncaoExameAso, Boolean? atendido, Boolean? finalizado, Boolean? cancelado,
            Boolean? devolvido, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método validaClienteFuncaoExameAsoSendoAtendido");

            Boolean emAtendimento = false;

            StringBuilder query = new StringBuilder();

            try
            {
                
                query.Append(" SELECT ATENDIDO FROM SEG_CLIENTE_FUNCAO_EXAME_ASO WHERE ");
                query.Append(" ID_CLIENTE_FUNCAO_EXAME_ASO = :VALUE1 " );

                if (atendido != null)
                    query.Append(" AND ATENDIDO = :VALUE2 ");

                if (finalizado != null)
                    query.Append(" AND FINALIZADO = :VALUE3 ");

                if (cancelado != null)
                    query.Append(" AND CANCELADO = :VALUE4 ");

                if (devolvido != null)
                    query.Append(" AND DEVOLVIDO = :VALUE5 ");

                query.Append(" AND TRANSCRITO IS FALSE ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = idClienteFuncaoExameAso;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Boolean));
                command.Parameters[1].Value = atendido;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Boolean));
                command.Parameters[2].Value = finalizado;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Boolean));
                command.Parameters[3].Value = devolvido;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Boolean));
                command.Parameters[4].Value = cancelado;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    if (dr.GetBoolean(0))
                    {
                        emAtendimento = true;
                    }
                }

                return emAtendimento;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - validaClienteFuncaoExameAsoSendoAtendido", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Boolean verificaExameAtendimento(Aso aso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método verificaExameAtendimento");

            Boolean retorno = false;

            StringBuilder query = new StringBuilder();

            try
            {

                query.Append(" SELECT ID_CLIENTE_FUNCAO_EXAME_ASO, ID_ASO, ID_CLIENTE_FUNCAO_EXAME, ID_SALA_EXAME, ");
                query.Append(" ATENDIDO, FINALIZADO, LOGIN, DEVOLVIDO, DATA_ATENDIDO, DATA_DEVOLVIDO, ");
                query.Append(" DATA_FINALIZADO, ID_USUARIO, CANCELADO, DATA_CANCELADO FROM SEG_CLIENTE_FUNCAO_EXAME_ASO ");
                query.Append(" WHERE ID_ASO = :VALUE1 AND ATENDIDO = TRUE AND FINALIZADO = FALSE AND CANCELADO = FALSE ");
                query.Append(" AND TRANSCRITO = FALSE ");
                                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    retorno = true;
                }

                return retorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - verificaExameAtendimento", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void cancelaExameAso(Aso aso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método cancelaExameAso");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_CLIENTE_FUNCAO_EXAME_ASO SET ");
                query.Append(" CANCELADO = TRUE, DATA_CANCELADO = :VALUE2 WHERE ");
                query.Append(" ID_ASO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Timestamp));
                command.Parameters[1].Value = DateTime.Now;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - cancelaExameAso", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int32 buscaMaiorPrioridade(Aso aso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método buscaMaiorPrioridade");

            try
            {
                Int32 maiorPrioridade = 0;

                StringBuilder query = new StringBuilder();

                query.Append(" SELECT COALESCE(MAX(E.PRIORIDADE), 0) FROM SEG_EXAME E, SEG_ASO A, SEG_CLIENTE_FUNCAO_EXAME CFE, SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA ");
                query.Append(" WHERE CFEA.ID_CLIENTE_FUNCAO_EXAME = CFE.ID_CLIENTE_FUNCAO_EXAME ");
                query.Append(" AND CFEA.ID_ASO = A.ID_ASO");
                query.Append(" AND CFE.ID_EXAME = E.ID_EXAME ");
                query.Append(" AND CFEA.FINALIZADO IS FALSE");
                query.Append(" AND CFEA.CANCELADO IS FALSE");
                query.Append(" AND CFEA.TRANSCRITO IS FALSE");
                query.Append(" AND A.ID_ASO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    maiorPrioridade = dr.GetInt32(0);
                }

                return maiorPrioridade;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - buscaMaiorPrioridade", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Int32 buscaQuantidadesExamesSendoAtendidosEmUmaSala(SalaExame salaExame, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método buscaQuantidadesExamesSendoAtendidosEmUmaSala");

            try
            {
                Int32 quantidadeAtendimentos = 0;

                StringBuilder query = new StringBuilder();

                query.Append(" SELECT CAST(COUNT(*) AS INTEGER) FROM SEG_CLIENTE_FUNCAO_EXAME_ASO ");
                query.Append(" WHERE ID_SALA_EXAME = :VALUE1 ");
                query.Append(" AND ATENDIDO IS TRUE AND FINALIZADO IS FALSE ");
                query.Append(" AND CANCELADO IS FALSE");
                query.Append(" AND TRANSCRITO IS FALSE");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = salaExame.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    if (dr.GetInt32(0) > 0)
                    {
                        quantidadeAtendimentos = dr.GetInt32(0);
                    }
                }

                return quantidadeAtendimentos;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - buscaQuantidadesExamesSendoAtendidosEmUmaSala", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public HashSet<ClienteFuncaoExameASo> buscaExamesNaoAtendidosComMaiorprioridade(Aso aso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método buscaExamesNaoAtendidosComMaiorprioridade");

            try
            {
                HashSet<ClienteFuncaoExameASo> clienteFuncaoExameASo = new HashSet<ClienteFuncaoExameASo>();

                StringBuilder query = new StringBuilder();

                query.Append(" SELECT CFEA.ID_CLIENTE_FUNCAO_EXAME_ASO, CFEA.ID_ASO, CFEA.ID_CLIENTE_FUNCAO_EXAME, ");
                query.Append(" CFEA.DATA_EXAME, CFEA.ID_SALA_EXAME, CFEA.ATENDIDO, CFEA.FINALIZADO, CFEA.LOGIN, CFEA.DEVOLVIDO, ");
                query.Append(" CFEA.DATA_ATENDIDO, CFEA.DATA_DEVOLVIDO, CFEA.DATA_FINALIZADO, CFEA.ID_USUARIO, ");
                query.Append(" CFEA.CANCELADO, CFEA.DATA_CANCELADO, CFEA.DATA_VALIDADE, CFEA.IMPRIME_ASO ");
                query.Append(" FROM SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA, SEG_CLIENTE_FUNCAO_EXAME CFE, SEG_EXAME E ");
                query.Append(" WHERE CFEA.ID_CLIENTE_FUNCAO_EXAME = CFE.ID_CLIENTE_FUNCAO_EXAME ");
                query.Append(" AND CFE.ID_EXAME = E.ID_EXAME ");
                query.Append(" AND E.PRIORIDADE = (SELECT MAX(E.PRIORIDADE) FROM SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA, SEG_CLIENTE_FUNCAO_EXAME CFE, SEG_EXAME E ");
                query.Append(" WHERE CFEA.ID_CLIENTE_FUNCAO_EXAME = CFE.ID_CLIENTE_FUNCAO_EXAME ");
                query.Append(" AND CFE.ID_EXAME = E.ID_EXAME AND CFEA.ATENDIDO IS FALSE AND CFEA.CANCELADO IS FALSE AND E.EXTERNO IS FALSE AND CFEA.ID_ASO = :VALUE1) ");
                query.Append(" AND CFEA.ATENDIDO IS FALSE ");
                query.Append(" AND CFEA.CANCELADO IS FALSE ");
                query.Append(" AND CFEA.TRANSCRITO IS FALSE ");
                query.Append(" AND E.EXTERNO IS FALSE ");
                query.Append(" AND CFEA.ID_ASO = :VALUE1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    clienteFuncaoExameASo.Add(new ClienteFuncaoExameASo(dr.GetInt64(0), null, aso, new ClienteFuncaoExame(dr.GetInt64(2)), dr.GetDateTime(3),dr.GetBoolean(5), dr.GetBoolean(6), dr.IsDBNull(7) ? string.Empty : dr.GetString(7), dr.GetBoolean(8), null, null, null, null, false, null, null, null, false, null,null, null, dr.IsDBNull(15) ? (DateTime?)null : dr.GetDateTime(15), dr.GetBoolean(16)));
                }

                return clienteFuncaoExameASo;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - buscaExamesNaoAtendidosComMaiorprioridade", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void IniciarAtendimentoExame(ClienteFuncaoExameASo clienteFuncaoExameAso, SalaExame salaExame, Boolean funcionarioVip, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método IniciarAtendimentoExame");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_CLIENTE_FUNCAO_EXAME_ASO SET ID_SALA_EXAME = :VALUE1, ATENDIDO = TRUE,");
                
                if (funcionarioVip)
                    query.Append(" DATA_ATENDIDO = CURRENT_DATE");
                else
                    query.Append(" DATA_ATENDIDO = NOW()");
                
                query.Append(" WHERE ID_CLIENTE_FUNCAO_EXAME_ASO = :VALUE2");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = salaExame.Id;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = clienteFuncaoExameAso.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - IniciarAtendimentoExame", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public HashSet<ClienteFuncaoExameASo> buscaExamesNaoAtendidosComMesmaPrioridadeAtendidosNaMesmaSala(Aso aso, Int32 prioridade, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método buscaExamesNaoAtendidosComMaiorprioridade");

            try
            {
                HashSet<ClienteFuncaoExameASo> clienteFuncaoExameASo = new HashSet<ClienteFuncaoExameASo>();

                StringBuilder query = new StringBuilder();

                query.Append(" SELECT CFEA.ID_CLIENTE_FUNCAO_EXAME_ASO, CFEA.ID_ASO, CFEA.ID_CLIENTE_FUNCAO_EXAME, ");
                query.Append(" CFEA.DATA_EXAME, CFEA.ID_SALA_EXAME, CFEA.ATENDIDO, CFEA.FINALIZADO, CFEA.LOGIN, CFEA.DEVOLVIDO, ");
                query.Append(" CFEA.DATA_ATENDIDO, CFEA.DATA_DEVOLVIDO, CFEA.DATA_FINALIZADO, CFEA.ID_USUARIO, ");
                query.Append(" CFEA.CANCELADO, CFEA.DATA_CANCELADO, CFEA.DATA_VALIDADE, CFEA.IMPRIME_ASO ");
                query.Append(" FROM SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA, SEG_CLIENTE_FUNCAO_EXAME CFE, SEG_EXAME E  ");
                query.Append(" WHERE CFEA.ID_CLIENTE_FUNCAO_EXAME = CFE.ID_CLIENTE_FUNCAO_EXAME ");
                query.Append(" AND CFE.ID_EXAME = E.ID_EXAME ");
                query.Append(" AND E.PRIORIDADE = :VALUE2");
                query.Append(" AND CFEA.ATENDIDO IS FALSE ");
                query.Append(" AND CFEA.CANCELADO IS FALSE ");
                query.Append(" AND E.EXTERNO IS FALSE ");
                query.Append(" AND CFEA.TRANSCRITO IS FALSE ");
                query.Append(" AND CFEA.ID_ASO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = prioridade;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    clienteFuncaoExameASo.Add(new ClienteFuncaoExameASo(dr.GetInt64(0), null, aso, new ClienteFuncaoExame(dr.GetInt64(2)), dr.GetDateTime(3),  dr.GetBoolean(5), dr.GetBoolean(6), dr.IsDBNull(7) ? string.Empty : dr.GetString(7), dr.GetBoolean(8),null,null,null, null, dr.GetBoolean(13), null, null, null, false,null,null, null, dr.IsDBNull(15) ? (DateTime?)null : dr.GetDateTime(15), dr.GetBoolean(16)));

                }

                return clienteFuncaoExameASo;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - buscaExamesNaoAtendidosComMaiorprioridade", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public HashSet<Int64> findAllExameInAso(Aso aso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findAllExameByAso");

            HashSet<Int64> exameRetornoSet = new HashSet<Int64>();
            
            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT DISTINCT EX.ID_EXAME ");
                query.Append(" FROM SEG_EXAME EX ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME CFE ON CFE.ID_EXAME = EX.ID_EXAME ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA ON CFEA.ID_CLIENTE_FUNCAO_EXAME = CFE.ID_CLIENTE_FUNCAO_EXAME ");
                query.Append(" WHERE CFEA.ID_ASO = :VALUE1 AND CANCELADO <> 'TRUE' AND TRANSCRITO <> 'TRUE' ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    exameRetornoSet.Add(dr.GetInt64(0));
                }

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findAllExameByAso", ex);
                throw new DataBaseConectionException(ex.Message);
            }

            return exameRetornoSet;
        }

        public Boolean isPrimeiroAtendimento(Aso aso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método isPrimeiroAtendimento");

            StringBuilder query = new StringBuilder();

            Boolean primeiroAtendimento = true;

            try
            {
                query.Append(" SELECT COUNT(*) FROM SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA ");
                query.Append(" WHERE CFEA.ID_ASO = :VALUE1 ");
                query.Append(" AND CFEA.ATENDIDO IS TRUE");
                query.Append(" AND CFEA.TRANSCRITO IS FALSE");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = aso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    if (dr.GetInt64(0) > 0)
                        primeiroAtendimento = false;
                }

                return primeiroAtendimento;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - isPrimeiroAtendimento", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void alteraClienteFuncaoExameAso(ClienteFuncaoExameASo clienteFuncaoExameAso, NpgsqlConnection dbconnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método alteraClienteFuncaoExameAso");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_CLIENTE_FUNCAO_EXAME_ASO SET ");
                query.Append(" DATA_EXAME = :VALUE2, TRANSCRITO = :VALUE3, DATA_VALIDADE = :VALUE4, IMPRIME_ASO = :VALUE5 WHERE ");
                query.Append(" ID_CLIENTE_FUNCAO_EXAME_ASO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbconnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncaoExameAso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Date));
                command.Parameters[1].Value = clienteFuncaoExameAso.DataExame;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Boolean));
                command.Parameters[2].Value = clienteFuncaoExameAso.Transcrito;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Date));
                command.Parameters[3].Value = clienteFuncaoExameAso.DataValidade;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Boolean));
                command.Parameters[4].Value = clienteFuncaoExameAso.ImprimeASO;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - alteraClienteFuncaoExameAso", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void updateInformacaoLaudo(ClienteFuncaoExameASo clienteFuncaoExameAso, NpgsqlConnection dbconnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateInformacaoLaudo");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_CLIENTE_FUNCAO_EXAME_ASO SET ");
                query.Append(" RESULTADO = :VALUE2, OBSERVACAO_RESULTADO = :VALUE3, ID_MEDICO = :VALUE4 WHERE ");
                query.Append(" ID_CLIENTE_FUNCAO_EXAME_ASO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbconnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncaoExameAso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = clienteFuncaoExameAso.Resultado;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Text));
                command.Parameters[2].Value = !String.IsNullOrEmpty(clienteFuncaoExameAso.ObservacaoResultado) ? clienteFuncaoExameAso.ObservacaoResultado.ToUpper() : string.Empty;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Integer));
                command.Parameters[3].Value = clienteFuncaoExameAso.Medico != null ? clienteFuncaoExameAso.Medico.Id : null;

                
                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateInformacaoLaudo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void excluirPrestadorClienteFuncaoExameAso(ClienteFuncaoExameASo clienteFuncaoExameAso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método excluirPrestadorClienteFuncaoExameAso");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_CLIENTE_FUNCAO_EXAME_ASO SET ID_PRESTADOR = NULL WHERE ID_CLIENTE_FUNCAO_EXAME_ASO = :VALUE1  ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncaoExameAso.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - excluirPrestadorClienteFuncaoExameAso", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void incluirPrestadorClienteFuncaoExameAso(ClienteFuncaoExameASo clienteFuncaoExameAso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método excluirPrestadorClienteFuncaoExameAso");

            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(" UPDATE SEG_CLIENTE_FUNCAO_EXAME_ASO SET ID_PRESTADOR = :VALUE2 WHERE ID_CLIENTE_FUNCAO_EXAME_ASO = :VALUE1  ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncaoExameAso.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                command.Parameters[1].Value = clienteFuncaoExameAso.Prestador.Id;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - excluirPrestadorClienteFuncaoExameAso", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Cliente findClienteByClienteFuncaoExameASo(ClienteFuncaoExameASo clienteFuncaoExameAso, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findClienteByClienteFuncaoExameASo");

            Cliente cliente = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT C.ID_CLIENTE FROM SEG_CLIENTE_FUNCAO_EXAME_ASO CFEA  ");
                query.Append(" JOIN SEG_ASO ASO ON ASO.ID_ASO = CFEA.ID_ASO ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO_FUNCIONARIO CFF ON CFF.ID_CLIENTE_FUNCAO_FUNCIONARIO = ASO.ID_CLIENTE_FUNCAO_FUNCIONARIO ");
                query.Append(" JOIN SEG_CLIENTE_FUNCAO CF ON CF.ID_SEG_CLIENTE_FUNCAO = CFF.ID_SEG_CLIENTE_FUNCAO ");
                query.Append(" JOIN SEG_CLIENTE C ON C.ID_CLIENTE = CF.ID_CLIENTE  ");
                query.Append(" WHERE CFEA.ID_CLIENTE_FUNCAO_EXAME_ASO = :VALUE1 ");


                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = clienteFuncaoExameAso.Id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    cliente = new Cliente(dr.GetInt64(0));
                }

                return cliente;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findClienteByClienteFuncaoExameASo", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public ClienteFuncaoExameASo findById(Int64 id, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findById");


            ClienteFuncaoExameASo clienteFuncaoExameAso = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT ID_CLIENTE_FUNCAO_EXAME_ASO, ID_SALA_EXAME, ID_ASO, ID_CLIENTE_FUNCAO_EXAME, ");
                query.Append(" DATA_EXAME, ATENDIDO, FINALIZADO, LOGIN, DEVOLVIDO, DATA_ATENDIDO, DATA_DEVOLVIDO,");
                query.Append(" DATA_FINALIZADO, ID_USUARIO, CANCELADO, DATA_CANCELADO, RESULTADO, OBSERVACAO_RESULTADO, ");
                query.Append(" TRANSCRITO, ID_PRESTADOR, ID_PROTOCOLO, ID_MEDICO, DATA_VALIDADE, IMPRIME_ASO");
                query.Append(" FROM SEG_CLIENTE_FUNCAO_EXAME_ASO");
                query.Append(" WHERE ID_CLIENTE_FUNCAO_EXAME_ASO = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = id;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    clienteFuncaoExameAso = new ClienteFuncaoExameASo(dr.GetInt64(0),dr.IsDBNull(1) ? null : new SalaExame(dr.GetInt64(1), null, null, null, null, string.Empty), dr.IsDBNull(2) ? null : new Aso(dr.GetInt64(2)), dr.IsDBNull(3) ? null : new ClienteFuncaoExame(dr.GetInt64(3)), dr.IsDBNull(4) ? null : (DateTime?)dr.GetDateTime(4), dr.GetBoolean(5), dr.GetBoolean(6), dr.IsDBNull(7) ? String.Empty : dr.GetString(7), dr.GetBoolean(8), dr.IsDBNull(9) ? null : (DateTime?)dr.GetDateTime(9), dr.IsDBNull(10) ? null : (DateTime?)dr.GetDateTime(10), dr.IsDBNull(11) ? null : (DateTime?)dr.GetDateTime(11), dr.IsDBNull(12) ? null : new Usuario(dr.GetInt64(12)), dr.GetBoolean(13), dr.IsDBNull(14) ? null : (DateTime?)dr.GetDateTime(14), dr.IsDBNull(15) ? String.Empty : dr.GetString(15), dr.IsDBNull(16) ? String.Empty : dr.GetString(16), dr.GetBoolean(17), dr.IsDBNull(18) ? null : new Cliente(dr.GetInt64(18)), dr.IsDBNull(19) ? null : new Protocolo(dr.GetInt64(19)), dr.IsDBNull(20) ? null : new Medico(dr.GetInt64(20)), dr.IsDBNull(21) ? (DateTime?)null : dr.GetDateTime(21), dr.GetBoolean(22));
                }

                return clienteFuncaoExameAso;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findById", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    }
}
