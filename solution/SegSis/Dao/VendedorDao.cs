﻿using SWS.Entidade;
using Npgsql;
using SWS.Excecao;
using System;
using NpgsqlTypes;
using System.Data;
using System.Text;
using SWS.IDao;
using SWS.Helper;

namespace SWS.Dao
{
    class VendedorDao : IVendedorDao
    {

        public DataSet findVendedorByFilter(Vendedor vendedor, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findVendedorByFilter");

            try
            {
                StringBuilder query = new StringBuilder();

                if (vendedor.isNullForFilter())
                {
                    query.Append("SELECT ID_VEND, NOME, SITUACAO ");
                    query.Append("FROM SEG_VENDEDOR ORDER BY NOME ASC");
                }
                else
                {
                    query.Append(" SELECT ID_VEND, NOME, SITUACAO ");
                    query.Append(" FROM SEG_VENDEDOR WHERE TRUE ");

                    if (!String.IsNullOrWhiteSpace(vendedor.Nome))
                        query.Append(" AND NOME LIKE :VALUE1 ");

                    if (!String.IsNullOrWhiteSpace(vendedor.Cpf))
                        query.Append(" AND CPF = :VALUE2 ");

                    if (!String.IsNullOrWhiteSpace(vendedor.Cidade))
                        query.Append(" AND CIDADE LIKE :VALUE3 ");

                    if (!String.IsNullOrWhiteSpace(vendedor.Uf))
                        query.Append(" AND UF = :VALUE4 ");

                    if (!String.IsNullOrWhiteSpace(vendedor.Situacao))
                        query.Append(" AND SITUACAO = :VALUE5 ");

                    query.Append(" ORDER BY NOME ASC");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = vendedor.Nome + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[1].Value = vendedor.Cpf;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[2].Value = vendedor.Cidade;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[3].Value = vendedor.Uf;

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[4].Value = vendedor.Situacao;

                DataSet ds = new DataSet();

                adapter.Fill(ds, "Vendedores");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findVendedorByFilter", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Vendedor insert(Vendedor vendedor, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método insert");

            StringBuilder query = new StringBuilder();

            try
            {
                vendedor.Id = recuperaProximoId(dbConnection);

                query.Append(" INSERT INTO SEG_VENDEDOR ( ID_VEND, NOME, CPF, RG, ENDERECO, NUMERO, COMPLEMENTO, ");
                query.Append(" BAIRRO, CIDADE, UF, CEP, TELEFONE1, TELEFONE2, EMAIL, COMISSAO, DATA_CADASTRO, SITUACAO, ID_CIDADE_IBGE ) ");
                query.Append(" VALUES (:VALUE1, :VALUE2, :VALUE3, :VALUE4, :VALUE5, :VALUE6, :VALUE7 , :VALUE8, :VALUE9, ");
                query.Append(" :VALUE10, :VALUE11, :VALUE12, :VALUE13, :VALUE14, :VALUE15, NOW(), :VALUE16, :VALUE17)");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);

                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = vendedor.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = vendedor.Nome;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = vendedor.Cpf;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = vendedor.Rg;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = vendedor.Endereco;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = vendedor.Numero;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = vendedor.Complemento;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Varchar));
                command.Parameters[7].Value = vendedor.Bairro;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                command.Parameters[8].Value = vendedor.Cidade;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                command.Parameters[9].Value = vendedor.Uf;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Varchar));
                command.Parameters[10].Value = vendedor.Cep;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Varchar));
                command.Parameters[11].Value = vendedor.Telefone1;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Varchar));
                command.Parameters[12].Value = vendedor.Telefone2;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Varchar));
                command.Parameters[13].Value = vendedor.Email.ToLower();
                
                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Numeric));
                command.Parameters[14].Value = vendedor.Comissao;
                
                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Varchar));
                command.Parameters[15].Value = vendedor.Situacao;

                command.Parameters.Add(new NpgsqlParameter("VALUE17", NpgsqlDbType.Integer));
                command.Parameters[16].Value = vendedor.CidadeIbge != null ? vendedor.CidadeIbge.Id : (long?)null;
                
                command.ExecuteNonQuery();

                return vendedor;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - insert", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
        
        public Int64 recuperaProximoId(NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método recuperaProximoId");

            Int64 proximoId = Convert.ToInt64(0);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand("SELECT NEXTVAL(('SEG_VENDEDOR_ID_VEND_SEQ'))", dbConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    proximoId = dr.GetInt64(0);
                }

                return proximoId;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - recuperaProximoIdVendedor", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Vendedor findVendedorByCpf(String cpf, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findVendedorByCpf");

            Vendedor vendedorRetorno = null;

            StringBuilder query = new StringBuilder();
            try
            {
                query.Append(" SELECT VENDEDOR.ID_VEND, VENDEDOR.NOME, VENDEDOR.CPF, VENDEDOR.RG, VENDEDOR.ENDERECO, VENDEDOR.NUMERO, VENDEDOR.COMPLEMENTO, ");
                query.Append(" VENDEDOR.BAIRRO, VENDEDOR.CIDADE, VENDEDOR.UF, VENDEDOR.CEP, VENDEDOR.TELEFONE1, VENDEDOR.TELEFONE2, VENDEDOR.EMAIL, VENDEDOR.COMISSAO, VENDEDOR.DATA_CADASTRO, VENDEDOR.SITUACAO, ");
                query.Append(" CIDADE.ID_CIDADE_IBGE, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE");
                query.Append(" FROM SEG_VENDEDOR VENDEDOR");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = VENDEDOR.ID_CIDADE_IBGE");
                query.Append(" WHERE VENDEDOR.CPF = :VALUE1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Varchar));
                command.Parameters[0].Value = cpf;

                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    vendedorRetorno = new Vendedor(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3),
                        dr.GetString(4), dr.GetString(5), dr.GetString(6), dr.GetString(7), dr.GetString(8),
                        dr.GetString(9), dr.GetString(10), dr.GetString(11), dr.IsDBNull(12) ? string.Empty : dr.GetString(12), dr.GetString(13), dr.GetDecimal(14), dr.GetDateTime(15), dr.GetString(16), dr.IsDBNull(17) ? null : new CidadeIbge(dr.GetInt64(17), dr.GetString(18), dr.GetString(19), dr.GetString(20)));
                }

                return vendedorRetorno;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findVendedorByCpf", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public Vendedor findVendedorById(Int64 idVendedor, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findVendedorById");

            Vendedor vendedorRetorno = null;

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" SELECT VENDEDOR.ID_VEND, VENDEDOR.NOME, VENDEDOR.CPF, VENDEDOR.RG, VENDEDOR.ENDERECO, VENDEDOR.NUMERO, VENDEDOR.COMPLEMENTO, ");
                query.Append(" VENDEDOR.BAIRRO, VENDEDOR.CIDADE, VENDEDOR.UF, VENDEDOR.CEP, VENDEDOR.TELEFONE1, VENDEDOR.TELEFONE2, VENDEDOR.EMAIL, VENDEDOR.COMISSAO, VENDEDOR.DATA_CADASTRO, VENDEDOR.SITUACAO, ");
                query.Append(" CIDADE.ID_CIDADE_IBGE, CIDADE.CODIGO, CIDADE.NOME, CIDADE.UF_CIDADE");
                query.Append(" FROM SEG_VENDEDOR VENDEDOR");
                query.Append(" LEFT JOIN SEG_CIDADE_IBGE CIDADE ON CIDADE.ID_CIDADE_IBGE = VENDEDOR.ID_CIDADE_IBGE");
                query.Append(" WHERE VENDEDOR.ID_VEND = :VALUE1 ");

                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));

                command.Parameters[0].Value = idVendedor;
                NpgsqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    vendedorRetorno = new Vendedor(dr.GetInt64(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.GetString(4), dr.GetString(5), dr.GetString(6), dr.GetString(7), dr.GetString(8), dr.GetString(9), dr.GetString(10), dr.GetString(11), dr.IsDBNull(12) ? string.Empty : dr.GetString(12), dr.GetString(13), dr.GetDecimal(14), dr.GetDateTime(15), dr.GetString(16), dr.IsDBNull(17) ? null : new CidadeIbge(dr.GetInt64(17), dr.GetString(18), dr.GetString(19), dr.GetString(20)));

                }
                return vendedorRetorno;

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findVendedorById", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public Vendedor update(Vendedor vendedor, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método updateVendedor");

            StringBuilder query = new StringBuilder();

            try
            {
                query.Append(" UPDATE SEG_VENDEDOR SET NOME = :VALUE2, CPF = :VALUE3, RG = :VALUE4, ");
                query.Append(" ENDERECO = :VALUE5, NUMERO = :VALUE6, COMPLEMENTO = :VALUE7, BAIRRO = :VALUE8, ");
                query.Append(" CIDADE = :VALUE9, UF = :VALUE10, CEP = :VALUE11, TELEFONE1 =:VALUE12, ");
                query.Append(" TELEFONE2 = :VALUE13, EMAIL = :VALUE14, COMISSAO = :VALUE15, DATA_CADASTRO = :VALUE16, ");
                query.Append(" SITUACAO = :VALUE17 WHERE ID_VEND = :VALUE1 ");
                
                NpgsqlCommand command = new NpgsqlCommand(query.ToString(), dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Integer));
                command.Parameters[0].Value = vendedor.Id;

                command.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Varchar));
                command.Parameters[1].Value = vendedor.Nome;

                command.Parameters.Add(new NpgsqlParameter("VALUE3", NpgsqlDbType.Varchar));
                command.Parameters[2].Value = vendedor.Cpf;

                command.Parameters.Add(new NpgsqlParameter("VALUE4", NpgsqlDbType.Varchar));
                command.Parameters[3].Value = vendedor.Rg;

                command.Parameters.Add(new NpgsqlParameter("VALUE5", NpgsqlDbType.Varchar));
                command.Parameters[4].Value = vendedor.Endereco;

                command.Parameters.Add(new NpgsqlParameter("VALUE6", NpgsqlDbType.Varchar));
                command.Parameters[5].Value = vendedor.Numero;

                command.Parameters.Add(new NpgsqlParameter("VALUE7", NpgsqlDbType.Varchar));
                command.Parameters[6].Value = vendedor.Complemento;

                command.Parameters.Add(new NpgsqlParameter("VALUE8", NpgsqlDbType.Varchar));
                command.Parameters[7].Value = vendedor.Bairro;

                command.Parameters.Add(new NpgsqlParameter("VALUE9", NpgsqlDbType.Varchar));
                command.Parameters[8].Value = vendedor.Cidade;

                command.Parameters.Add(new NpgsqlParameter("VALUE10", NpgsqlDbType.Varchar));
                command.Parameters[9].Value = vendedor.Uf;

                command.Parameters.Add(new NpgsqlParameter("VALUE11", NpgsqlDbType.Varchar));
                command.Parameters[10].Value = vendedor.Cep;

                command.Parameters.Add(new NpgsqlParameter("VALUE12", NpgsqlDbType.Varchar));
                command.Parameters[11].Value = vendedor.Telefone1;

                command.Parameters.Add(new NpgsqlParameter("VALUE13", NpgsqlDbType.Varchar));
                command.Parameters[12].Value = vendedor.Telefone2;

                command.Parameters.Add(new NpgsqlParameter("VALUE14", NpgsqlDbType.Varchar));
                command.Parameters[13].Value = vendedor.Email.ToLower();

                command.Parameters.Add(new NpgsqlParameter("VALUE15", NpgsqlDbType.Numeric));
                command.Parameters[14].Value = vendedor.Comissao;

                command.Parameters.Add(new NpgsqlParameter("VALUE16", NpgsqlDbType.Date));
                command.Parameters[15].Value = vendedor.DataCadastro;

                command.Parameters.Add(new NpgsqlParameter("VALUE17", NpgsqlDbType.Varchar));
                command.Parameters[16].Value = vendedor.Situacao;
                
                command.ExecuteNonQuery();

                return vendedor;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - updateVendedor", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }

        public void changeStatusVendedor(Vendedor vendedor, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método changeStatusVendedor");

            try
            {
                NpgsqlCommand command = new NpgsqlCommand("update seg_vendedor set situacao = :value2 where id_vend = :value1", dbConnection);
                
                command.Parameters.Add(new NpgsqlParameter("value1", NpgsqlDbType.Integer));
                command.Parameters.Add(new NpgsqlParameter("value2", NpgsqlDbType.Varchar));

                command.Parameters[0].Value = vendedor.Id;
                command.Parameters[1].Value = vendedor.Situacao;

                command.ExecuteNonQuery();

            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - changeStatusVendedor", ex);
                throw new DataBaseConectionException(ex.Message);
            }

        }

        public DataSet findVendedorByName(Vendedor vendedor, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findVendedorByName");

            try
            {
                StringBuilder query = new StringBuilder();

                if (String.IsNullOrWhiteSpace(vendedor.Nome))
                {
                    query.Append("SELECT ID_VEND, NOME FROM SEG_VENDEDOR WHERE SITUACAO = 'A' ORDER BY NOME ASC");
                }
                else
                {
                    query.Append("SELECT ID_VEND, NOME FROM SEG_VENDEDOR WHERE SITUACAO = 'A' AND NOME LIKE :VALUE1 ");
                    query.Append("ORDER BY NOME ASC");
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                                adapter.SelectCommand.Parameters[0].Value = vendedor.Nome + "%";
                
                DataSet ds = new DataSet();

                adapter.Fill(ds, "Vendedores");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findVendedorByName", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }


        public DataSet findVendedorNotInCliente(Vendedor vendedor, Cliente cliente, NpgsqlConnection dbConnection)
        {
            LogHelper.logger.Info(this.GetType().Name + " - Iniciando Método findVendedorNotInCliente");

            try
            {
                StringBuilder query = new StringBuilder();

                if (String.IsNullOrWhiteSpace(vendedor.Nome))
                {
                    query.Append("SELECT ID_VEND, NOME FROM SEG_VENDEDOR WHERE SITUACAO = 'A' AND ID_VEND NOT IN (SELECT ID_VEND FROM SEG_REP_CLI WHERE ID_CLIENTE = :VALUE2 AND SITUACAO = 'A' ) ORDER BY NOME ASC");
                }
                else
                {
                    query.Append("SELECT ID_VEND, NOME FROM SEG_VENDEDOR WHERE SITUACAO = 'A' AND NOME LIKE :VALUE1 AND ID_VEND NOT IN (SELECT ID_VEND FROM SEG_REP_CLI WHERE ID_CLIENTE = :VALUE1 AND SITUACAO = 'A' ) ORDER BY NOME ASC " );
                }

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query.ToString(), dbConnection);

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE1", NpgsqlDbType.Varchar));
                adapter.SelectCommand.Parameters[0].Value = vendedor.Nome + "%";

                adapter.SelectCommand.Parameters.Add(new NpgsqlParameter("VALUE2", NpgsqlDbType.Integer));
                adapter.SelectCommand.Parameters[1].Value = cliente.Id;


                DataSet ds = new DataSet();

                adapter.Fill(ds, "Vendedores");

                return ds;
            }
            catch (NpgsqlException ex)
            {
                LogHelper.logger.Error(this.GetType().Name + " - findVendedorNotInCliente", ex);
                throw new DataBaseConectionException(ex.Message);
            }
        }
    
    }
}
