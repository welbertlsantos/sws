﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SWS.Excecao;
using SWS.Entidade;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Globalization;

namespace SWS.View.ViewHelper
{
    public class ValidaCampoHelper
    {
        /// <summary>
        /// Verifica se o CPF é válido
        /// </summary>
        /// <param name="vrCPF"></param>
        /// <returns>Retorna true se válido</returns>
        
        public static bool ValidaCPF(string vrCPF)
        {
            string valor = vrCPF.Replace(".", "").Replace("-", "").Replace(",", "");

            if (String.IsNullOrEmpty(valor.Trim()))
            {
                return true;
            }

            if (valor.Length != 11){
                return false;
            }

            bool igual = true;

            for (int i = 1; i < 11 && igual; i++){
                if (valor[i] != valor[0]){
                    igual = false;
                }
            }

            if (igual || valor == "12345678909"){
                return false;
            }

            int[] numeros = new int[11];

            for (int i = 0; i < 11; i++){
              numeros[i] = int.Parse(valor[i].ToString());
            }

            int soma = 0;

            for (int i = 0; i < 9; i++){
                soma += (10 - i) * numeros[i];
            }
 
            int resultado = soma % 11;

            if (resultado == 1 || resultado == 0)
            {
                if (numeros[9] != 0){
                    return false;
                }
            }
            else if (numeros[9] != 11 - resultado){

                return false;
            }
 
            soma = 0;

            for (int i = 0; i < 10; i++){
                soma += (11 - i) * numeros[i];
            }

            resultado = soma % 11;

            if (resultado == 1 || resultado == 0)
            {
                if (numeros[10] != 0){
                    return false;
                }
            }
            else if (numeros[10] != 11 - resultado){
                return false;
            }
            
            return true;
            
        }

        /// <summary>
        /// Verifica se um CNPJ é valido
        /// </summary>
        /// <param name="vrCNPJ"></param>
        /// <returns>Retorna true se válido</returns>
        public static bool ValidaCNPJ(string vrCNPJ)
        {

            string CNPJ = vrCNPJ.Replace(".", "").Replace("/", "").Replace("-", "").Trim();

            int[] digitos, soma, resultado;
            int nrDig;
            string ftmt;
            bool[] CNPJOk;
            
            ftmt = "6543298765432";
            
            digitos = new int[14];
            soma = new int[2];
            soma[0] = 0;
            soma[1] = 0;
            resultado = new int[2];
            resultado[0] = 0;
            resultado[1] = 0;
            CNPJOk = new bool[2];
            CNPJOk[0] = false;
            CNPJOk[1] = false;
            try
            {
                for (nrDig = 0; nrDig < 14; nrDig++)
                {
                    digitos[nrDig] = int.Parse(
                    CNPJ.Substring(nrDig, 1));
                    
                    if (nrDig <= 11) soma[0] += (digitos[nrDig] * int.Parse(ftmt.Substring(nrDig + 1, 1)));
                    
                    if (nrDig <= 12) soma[1] += (digitos[nrDig] * int.Parse(ftmt.Substring(nrDig, 1)));
                    
                }
                
                for (nrDig = 0; nrDig < 2; nrDig++)
                {

                    resultado[nrDig] = (soma[nrDig] % 11);
                    if ((resultado[nrDig] == 0) || (resultado[nrDig] == 1))
                    {
                        CNPJOk[nrDig] = (digitos[12 + nrDig] == 0);
                    }
                    else
                    {
                        CNPJOk[nrDig] = (digitos[12 + nrDig] == (11 - resultado[nrDig]));
                    }
                }

                return (CNPJOk[0] && CNPJOk[1]);
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Retorna o CNAE formatado. Exemplo: 00.00-0/00
        /// </summary>
        /// <param name="str"></param>
        /// <returns>Retorna uma String que representa o CNAE formatado do tipo 00.00-0/00 </returns>
        public static String RetornaCnaeFormatado(String str)
        {
            String str1 = str;

            str1 = str.Substring(0, 2) + '.' + str.Substring(2, 2) + '-' + str.Substring(4, 1) + '/' + str.Substring(5, 2);

            return str1;

        }

        /// <summary>
        /// Exclui a formatação do CNAE para gravação no banco de dados.
        /// </summary>
        /// <param name="str"></param>
        /// <returns>CNAE sem formato</returns>
        public static String RetornaCnaSemFormato(String str)
        {
            String str1 = str;

            str1 = str.Replace(".", "").Replace(",", "").Replace("-", "").Replace("/", "");

            return str1;

        }

        /// <summary>
        /// Retorna o Código de atendimento formatado. Exemplo: 0000.00.000000 - ANO+MES+IDENTIFICADOR
        /// </summary>
        /// <param name="codigo"></param>
        /// <returns>Retorna uma string que representa o código do atendimento no formato 0000.00.000000</returns>
        public static String RetornaCodigoAtendimentoFormatado(String codigo)
        {
            String str = codigo.Substring(0, 4) + '.' + codigo.Substring(4, 2) + '.' + codigo.Substring(6, 6);

            return str;
            
        }

        /// <summary>
        /// Retorna o código do estudo formatado. Exemplo: 0000.00.0000000/00 - ANO+MES+IDENTIFICADOR/REVISAO
        /// </summary>
        /// <param name="codigo"></param>
        /// <returns>String que representa o código do estudo já formatado</returns>
        public static String RetornaCodigoEstudoFormatado(String codigo)
        {
            String str = codigo.Substring(0, 4) + '.' + codigo.Substring(4, 2) + '.' + codigo.Substring(6, 6) + '/' + codigo.Substring(12, 2);

            return str;
        }
        
        /// <summary>
        /// Calcula a idade do colaborador de acordo com a sua data de nascimento.
        /// </summary>
        /// <param name="dataNascimento"></param>
        /// <returns>Retorna um inteiro que representa a idade do colaborador.</returns>
        public static Int32 CalculaIdade(DateTime dataNascimento)
        {
            Int32 idade = DateTime.Now.Year - dataNascimento.Year;

            if (DateTime.Now.Month < dataNascimento.Month || (DateTime.Now.Month == dataNascimento.Month && DateTime.Now.Day < dataNascimento.Day))
            {
                idade--;
            }

            return idade;

        }

        
        public static Boolean formatar_moeda(System.Windows.Forms.TextBox campo, char separador_milhar, char separador_decimal, System.Windows.Forms.KeyPressEventArgs e)
        {
            char key;
            int i = 0;
            int j = 0;
            int len = 0;
            int len2 = 0;
            string strCheck = "0123456789";
            string aux;
            string aux2;
            String texto = campo.Text;
            Char[] textoSplit = texto.ToArray();

            
            if (!(char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar)))
            {
                return true;
            }
            

            if (!(char.IsDigit(e.KeyChar) || (Keys)e.KeyChar == Keys.Return ||
                (Keys)e.KeyChar == Keys.Delete) || (Keys)e.KeyChar == Keys.Back)
            {
                return true;
            }
            
            key = e.KeyChar; // Pegando o valor digitado

            len = campo.TextLength;

            for (i = 0; i < len; i++)
            {
                if ((textoSplit[i] != '0') && (textoSplit[i] != separador_decimal))
                {
                    break;
                }
            }

            aux = "";

            for (; i < len; i++)
            {
                if (strCheck.Contains(textoSplit[i]))
                {
                    aux += textoSplit[i];
                }
            }

            aux += key;

            len = aux.Length;

            if (len == 0)
            {
                campo.Text = "";
            }

            if (len == 1)
            {
                campo.Text = "0" + separador_decimal + "0" + aux;
            }

            if (len == 2)
            {
                campo.Text = "0" + separador_decimal + aux;
            }

            if (len > 2)
            {
                aux2 = "";
                char[] auxSplit = aux.ToArray();

                for (j = 0, i = len - 3; i >= 0; i--)
                {
                    if (j == 3)
                    {
                        aux2 += separador_milhar;
                        j = 0;
                    }

                    aux2 += auxSplit[i];

                    j++;
                }

                campo.Text = "";
                len2 = aux2.Length;
                char[] aux2Split = aux2.ToArray();
                for (i = len2 - 1; i >= 0; i--)
                {
                    campo.Text += aux2Split[i];
                }

                campo.Text += separador_decimal + aux.Substring(len - 2, 2);
            }

            return true;
        }
        
        
        /// <summary>
        /// Formata o campo para valor monetário.
        /// </summary>
        /// <param name="valor"></param>
        /// <returns>Retorna a string já formatada do tipo valor 00,00</returns>
        public static String FormataValorMonetario(String valor)
        {
            Char separadoDecimal = ',';
            Char[] textSeparado = valor.ToArray();
            String valorRetorno = null;
            Int32 tamanhoMaximo = 11;

            // tratando os zeros da esqueda e analizando a string

            valor = valor.TrimStart('0');

            if (valor.Contains(',') && valor[0] == ',')
            {
                valor = "0" + valor;
            }

            if (String.IsNullOrEmpty(valor))
            {
                return "0,00";
            }

            if (!valor.Contains(','))
            {
                valorRetorno = valor + separadoDecimal + "00";
                
            }
            else
            {
                if (valor.Length - (valor.IndexOf(',') + 1) == 0)
                {

                    valorRetorno = valor + "00";
                }
                else
                {
                    if (valor.Length - (valor.IndexOf(',') + 1) <= 1)
                    {
                        valorRetorno = valor + "0";
                    
                    }
                    else
                    {
                        valorRetorno = valor;
                    }
                        
                }

            }

            if (valorRetorno.Length <= tamanhoMaximo)
            {
                valorRetorno = SeparadoMilhar(valorRetorno);
            }
            else
            {
                throw new HelperException(HelperException.msg);
            }

            return valorRetorno;

        }

        public static string formataNumeroDecimal(string str, int casasDecimais)
        {
            if (string.IsNullOrEmpty(str))
                return str;
            
            Decimal number = Convert.ToDecimal(str);
            
            CultureInfo myCult = new CultureInfo("pt-Br");
            myCult.NumberFormat.NumberDecimalDigits = casasDecimais;
            myCult.NumberFormat.NumberDecimalSeparator = ",";

            string numberFormat = string.Format(myCult, "{0:N}", number);
            return numberFormat;

        }
        
        /// <summary>
        /// Método interno usado pelo método formataValorMonetario
        /// </summary>
        /// <param name="valor"></param>
        /// <returns>Retorna a string com separação de milhar.</returns>
        public static String SeparadoMilhar(String valor)
        {
            char separadoMilhar = '.';
            String valorRetorno = "";
            Int32 tamanhoString = valor.Length;

            if (valor.Length == 7)
            {
                valorRetorno = valor[0].ToString() + separadoMilhar.ToString() + valor.Substring(1, tamanhoString - 1);

            }
            else
            {
                if (valor.Length == 8)
                {
                    valorRetorno = valor.Substring(0, 2) + separadoMilhar.ToString() + valor.Substring(2, tamanhoString - 2);
                }
                else
                {
                    if (valor.Length == 9)
                    {
                        valorRetorno = valor.Substring(0, 3) + separadoMilhar.ToString() + valor.Substring(3, tamanhoString - 3);
                    }
                    else
                    {
                        if (valor.Length == 10)
                        {
                            valorRetorno = valor.Substring(0, 1) + separadoMilhar.ToString() + valor.Substring(1, 3) + separadoMilhar.ToString()
                            + valor.Substring(4, tamanhoString - 4);
                        }
                        else
                        {
                            if (valor.Length == 11)
                            {
                                valorRetorno = valor.Substring(0, 2) + separadoMilhar.ToString() + valor.Substring(2, 3) + separadoMilhar.ToString()
                                 + valor.Substring(5, tamanhoString - 5);
                            }
                            else
                            {
                                return valor;
                            }
                        }
                    }
                }
            }

            return valorRetorno;
        }

        public static String formataValorMonetarioBanco(String valor)
        {
            String valorFormatado = null;

            valorFormatado = valor.Replace(".","").Replace(",", ".");

            return valorFormatado;

        }

        /// <summary>
        /// Valida o evento Handler para informar se a tecla digitada é um número. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="textoDigitado"></param>
        /// <returns>Retorna true para o evento Handle permitindo que a tecla possa ser lida.</returns>
        public static Boolean FormataDigitacaoNumero(Object sender, KeyPressEventArgs e, String textoDigitado, int casasDecimais)
        {
            Int32 posVirgula;

            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == ',' || e.KeyChar == (char)8))
            {
                return true;
                
            }

            if (String.IsNullOrEmpty(textoDigitado))
            {
                posVirgula = -1;
            }
            else
            {
                posVirgula = textoDigitado.IndexOf(",");
            }
            if (posVirgula != -1)
            {
                if (e.KeyChar == ',')
                {
                    return true;
                }
                else
                {

                    if ((textoDigitado.Length - posVirgula) == (casasDecimais + 1))
                    {
                        if (e.KeyChar != (char)8)
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        public static Boolean FormataCampoNumerico(Object sender, KeyPressEventArgs e)
        {

            if (char.IsDigit(e.KeyChar) || e.KeyChar == (char)8)
            {
                return false; // Permite a entrada de números e Backspace
            }

            if (char.IsControl(e.KeyChar))
            {
                return false; // Permite teclas de controle como Delete, Enter, etc.
            }

            if (Control.ModifierKeys == Keys.Control && e.KeyChar == 22) // Ctrl + V
            {
                return false; // Permite Ctrl + V (colar)
            }

            return true;
        }

        /// <summary>
        /// Calcula a comissão devida de um produto para o vendedor.
        /// </summary>
        /// <param name="contratoProduto"></param>
        /// <param name="vendedor"></param>
        /// <returns>Retorna um decimal que corresponde ao valor da comissão devida a um vendedor.</returns>
        public static Decimal CalculaValorComissao(ContratoProduto contratoProduto, Vendedor vendedor)
        {
            Decimal valorComissao;

            // calculando valor de comissão do produto.

            valorComissao = (Decimal)contratoProduto.PrecoContratado * ((Decimal)vendedor.Comissao / 100);
            
            return valorComissao;
        

        }

        /// <summary>
        /// Valida o CEP digitado se está dentro dos padrões do CEP
        /// </summary>
        /// <param name="cep"></param>
        /// <returns>Retorna true se o CEP for válido. Se tem exatamente 8bytes.</returns>
        public static Boolean ValidaCepDigitado(String cep)
        {

            String cepverificar = cep.Replace(".","").Replace("-","").Replace(",","").Trim();

            if (cepverificar.Length > 0 && cepverificar.Length < 8)
            {
                return false;
            }

            return true;

        }

        /// <summary>
        /// Valida se a altura digitada está acima da altura máxima permitida que é de 2.3 metros.
        /// </summary>
        /// <param name="altura"></param>
        /// <returns>Retorna true se a altura for válida.</returns>
        public static Boolean ValidaAltura(String altura)
        {

            Decimal alturaMaxima = Convert.ToDecimal(2.3);

            if (!altura.Contains(","))
            {
                return false;
            }
            else
            {
                if (Convert.ToDecimal(altura) > alturaMaxima)
                {
                    return false;
                }
            }

            return true;

        }

        /// <summary>
        /// Permite que o campo receba somente números e letras.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns>Retorna true para o Handled e permite que a tecla seja lida caso atenda os padroes numeros e letras.</returns>
        public static Boolean FormataNumeroELetras(Object sender, KeyPressEventArgs e)
        {

            string caracteresPermitidos = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            if (e.KeyChar == (char)Keys.Back)
            {
                return false;
            }
            
            if (!(caracteresPermitidos.Contains(e.KeyChar.ToString().ToUpper())))
            {
                return true;
            }

            return false;
            
        }

        public static Boolean formataNumerosEPonto(Object sender, KeyPressEventArgs e)
        {

            string caracteresPermitidos = "0123456789.";

            if (e.KeyChar == (char)Keys.Back)
            {
                return false;
            }

            if (!(caracteresPermitidos.Contains(e.KeyChar.ToString().ToUpper())))
            {
                return true;
            }

            return false;

        }

        /// <summary>
        /// Formata o CPF digitado para o padrão 000.000.000-00
        /// </summary>
        /// <param name="cpf"></param>
        /// <returns>Retorna uma string que representa o CPF já formatado no padrão 000.000.000-00</returns>
        public static String FormataCpf(String cpf)
        {
            String str1 = cpf;

            if (String.IsNullOrEmpty(cpf))
            {
                return String.Empty;
            }

            str1 = cpf.Substring(0, 3) + '.' + cpf.Substring(3, 3) + '.' + cpf.Substring(6, 3) + '-' + cpf.Substring(9, 2);

            return str1;
        }
        
        /// <summary>
        /// Formata o CNPJ digitado para o padrão 00.000.000/0000-00 
        /// </summary>
        /// <param name="cnpj"></param>
        /// <returns>Retorna uma string que representa o CNPJ já formatado no padrão 00.000.000/0000-00  </returns>
        public static String FormataCnpj(String cnpj)
        {
            String str1 = cnpj;

            if (String.IsNullOrEmpty(cnpj))
            {
                return str1;
            }
            else
            {
                str1 = cnpj.Substring(0, 2) + '.' + cnpj.Substring(2, 3) + '.' + cnpj.Substring(5, 3) + '/' + cnpj.Substring(8, 4) + '-' + cnpj.Substring(12, 2);
            }

            return str1;
        }
        
        /// <summary>
        /// Formata o CEP digitado para o padrão 00.000-000
        /// </summary>
        /// <param name="cep"></param>
        /// <returns>Retorna uma string que representa o CEP formato no padrão 00.000-000</returns>
        public static String FormataCEP(String cep)
        {
            String str1 = cep;

            if (!String.IsNullOrEmpty(str1))
            {
                str1 = cep.Substring(0, 2) + '.' + cep.Substring(2, 3) + '-' + cep.Substring(5, 3);
            }
            return str1;
        }

        /// <summary>
        /// Confirma a validade de um email
        /// </summary>
        /// <param name="enderecoEmail">Email a ser validado</param>
        /// <returns>Retorna True se o email for valido</returns>
        public static bool ValidaEnderecoEmail(string enderecoEmail)
        {
            try
            {
                //define a expressão regulara para validar o email
                string texto_Validar = enderecoEmail;
                Regex expressaoRegex = new Regex(@"\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}");

                // testa o email com a expressão
                if (expressaoRegex.IsMatch(texto_Validar))
                {
                    // o email é valido
                    return true;
                }
                else
                {
                    // o email é inválido
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static bool ValidaIP(string str)
        {
            string pattern = str;
            Regex expressaoRegex = new Regex(@"\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b");
            
            if (expressaoRegex.IsMatch(pattern))
                return true;
            else
                return false;

        }

        public static Boolean ValidaEntradaIP(Object sender, KeyPressEventArgs e)
        {

            string caracteresPermitidos = "0123456789.";

            if (e.KeyChar == (char)Keys.Back)
                return false;

            if (!(caracteresPermitidos.Contains(e.KeyChar.ToString().ToUpper())))
                return true;

            return false;

        }
    }
}
