﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using SWS.ViewHelper;
using System.Data;
using SWS.Facade;

namespace SWS.View.ViewHelper
{
    public class ComboHelper
    {
        public static void iniciaComboAtivoInativo(ComboBox combo)
        {
            ArrayList arr = new ArrayList();
            arr.Add(new SelectItem("A", "Ativo"));
            arr.Add(new SelectItem("I", "Inativo"));

            combo.DataSource = arr;
            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";
            combo.DropDownStyle = ComboBoxStyle.DropDownList;

        }

        public static void iniciaComboTipoAgente(ComboBox combotipoAgente)
        {
            ArrayList arrNew = new ArrayList();
            arrNew.Add(new SelectItem("", "Todos"));
            arrNew.Add(new SelectItem("FÍSICO", "Físico"));
            arrNew.Add(new SelectItem("QUÍMICO", "Químico"));
            arrNew.Add(new SelectItem("BIOLÓGICO", "Biológico"));
            arrNew.Add(new SelectItem("ERGONÔMICO", "Ergonômico"));
            arrNew.Add(new SelectItem("ACIDENTE", "Acidente"));

            combotipoAgente.DataSource = arrNew;
            combotipoAgente.DisplayMember = "Nome";
            combotipoAgente.ValueMember = "Valor";
            combotipoAgente.DropDownStyle = ComboBoxStyle.DropDownList;

        }

        public static void iniciaComboTipoAgenteIncluir(ComboBox combotipoAgente)
        {
            ArrayList arrNew = new ArrayList();
            arrNew.Add(new SelectItem("FÍSICO", "Físico"));
            arrNew.Add(new SelectItem("QUÍMICO", "Químico"));
            arrNew.Add(new SelectItem("BIOLÓGICO", "Biológico"));
            arrNew.Add(new SelectItem("ERGONÔMICO", "Ergonômico"));
            arrNew.Add(new SelectItem("ACIDENTE", "Acidente"));

            combotipoAgente.DataSource = arrNew;
            combotipoAgente.DisplayMember = "Nome";
            combotipoAgente.ValueMember = "Valor";
            combotipoAgente.DropDownStyle = ComboBoxStyle.DropDownList;
        }


        public static void unidadeFederativa(ComboBox comboUf)
        {
            ArrayList arrNew = new ArrayList();

            arrNew.Add(new SelectItem("", ""));
            arrNew.Add(new SelectItem("AC", "Acre"));
            arrNew.Add(new SelectItem("AL", "Alagoas"));
            arrNew.Add(new SelectItem("AP", "Amapá"));
            arrNew.Add(new SelectItem("AM", "Amazonas"));
            arrNew.Add(new SelectItem("BA", "Bahia"));
            arrNew.Add(new SelectItem("CE", "Ceará"));
            arrNew.Add(new SelectItem("DF", "Distrito Federal"));
            arrNew.Add(new SelectItem("ES", "Espirito Santo"));
            arrNew.Add(new SelectItem("GO", "Goias"));
            arrNew.Add(new SelectItem("MA", "Maranhão"));
            arrNew.Add(new SelectItem("MT", "Mato Grosso"));
            arrNew.Add(new SelectItem("MS", "Mato Grosso Do Sul"));
            arrNew.Add(new SelectItem("MG", "Minas Gerais"));
            arrNew.Add(new SelectItem("PA", "Pará"));
            arrNew.Add(new SelectItem("PB", "Paraíba"));
            arrNew.Add(new SelectItem("PR", "Paraná"));
            arrNew.Add(new SelectItem("PE", "Pernambuco"));
            arrNew.Add(new SelectItem("PI", "Piauí"));
            arrNew.Add(new SelectItem("RJ", "Rio de Janeiro"));
            arrNew.Add(new SelectItem("RN", "Rio Grande Do Norte"));
            arrNew.Add(new SelectItem("RS", "Rio Grande Do Sul"));
            arrNew.Add(new SelectItem("RO", "Rondônia"));
            arrNew.Add(new SelectItem("RR", "Roraima"));
            arrNew.Add(new SelectItem("SC", "Santa Cataria"));
            arrNew.Add(new SelectItem("SP", "São Paulo"));
            arrNew.Add(new SelectItem("SE", "Sergipe"));
            arrNew.Add(new SelectItem("TO", "Tocantins"));

            comboUf.DataSource = arrNew;
            comboUf.DisplayMember = "Nome";
            comboUf.ValueMember = "Valor";
            comboUf.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void startComboUfSave(ComboBox combo)
        {
            IList<SelectItem> arrNew = new List<SelectItem>() { };

            arrNew.Add(new SelectItem("AC", "AC"));
            arrNew.Add(new SelectItem("AL", "AL"));
            arrNew.Add(new SelectItem("AP", "AP"));
            arrNew.Add(new SelectItem("AM", "AM"));
            arrNew.Add(new SelectItem("BA", "BA"));
            arrNew.Add(new SelectItem("CE", "CE"));
            arrNew.Add(new SelectItem("DF", "DF"));
            arrNew.Add(new SelectItem("ES", "ES"));
            arrNew.Add(new SelectItem("GO", "GO"));
            arrNew.Add(new SelectItem("MA", "MA"));
            arrNew.Add(new SelectItem("MT", "MT"));
            arrNew.Add(new SelectItem("MS", "MS"));
            arrNew.Add(new SelectItem("MG", "MG"));
            arrNew.Add(new SelectItem("PA", "PA"));
            arrNew.Add(new SelectItem("PB", "PB"));
            arrNew.Add(new SelectItem("PR", "PR"));
            arrNew.Add(new SelectItem("PE", "PE"));
            arrNew.Add(new SelectItem("PI", "PI"));
            arrNew.Add(new SelectItem("RJ", "RJ"));
            arrNew.Add(new SelectItem("RN", "RN"));
            arrNew.Add(new SelectItem("RS", "RS"));
            arrNew.Add(new SelectItem("RO", "RO"));
            arrNew.Add(new SelectItem("RR", "RR"));
            arrNew.Add(new SelectItem("SC", "SC"));
            arrNew.Add(new SelectItem("SP", "SP"));
            arrNew.Add(new SelectItem("SE", "SE"));
            arrNew.Add(new SelectItem("TO", "TO"));

            combo.DataSource = arrNew;
            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";
            combo.DropDownStyle = ComboBoxStyle.DropDownList;
            
            
        }

        public static void startComboMesSave(ComboBox comboUf)
        {
            ArrayList arrNew = new ArrayList();

            arrNew.Add(new SelectItem("JANEIRO", "Janeiro"));
            arrNew.Add(new SelectItem("FEVEREIRO", "Fevereiro"));
            arrNew.Add(new SelectItem("MARCO", "Março"));
            arrNew.Add(new SelectItem("ABRIL", "Abril"));
            arrNew.Add(new SelectItem("MAIO", "Maio"));
            arrNew.Add(new SelectItem("JUNHO", "Junho"));
            arrNew.Add(new SelectItem("JULHO", "Julho"));
            arrNew.Add(new SelectItem("AGOSTO", "Agosto"));
            arrNew.Add(new SelectItem("SETEMBRO", "Setembro"));
            arrNew.Add(new SelectItem("OUTUBRO", "Outubro"));
            arrNew.Add(new SelectItem("NOVEMBRO", "Novembro"));
            arrNew.Add(new SelectItem("DEZEMBRO", "Dezembro"));
            arrNew.Add(new SelectItem("TODOS OS MESES", "Todos os Meses"));
            
            comboUf.DataSource = arrNew;
            comboUf.DisplayMember = "Nome";
            comboUf.ValueMember = "Valor";
            comboUf.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void grauRisco(ComboBox grauRisco)
        {
            ArrayList arrNew = new ArrayList();

            arrNew.Add(new SelectItem("1", "Grau - 01"));
            arrNew.Add(new SelectItem("2", "Grau - 02"));
            arrNew.Add(new SelectItem("3", "Grau - 03"));
            arrNew.Add(new SelectItem("4", "Grau - 04"));
            

            grauRisco.DataSource = arrNew;
            grauRisco.DisplayMember = "Nome";
            grauRisco.ValueMember = "Valor";
            grauRisco.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void CarregaEPi(ComboBox epi)
        {
            ArrayList arrNew = new ArrayList();

            arrNew.Add(new SelectItem("O", "Obrigatório"));
            arrNew.Add(new SelectItem("E", "Eventual"));
            
            epi.DataSource = arrNew;
            epi.DisplayMember = "Nome";
            epi.ValueMember = "Valor";
            epi.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void PeriodicidadeExame(ComboBox periodicidade)
        {
            ArrayList arrNew = new ArrayList();

            arrNew.Add(new SelectItem("BI-ANUAL", "Bi-anual"));
            arrNew.Add(new SelectItem("ANUAL", "Anual"));
            arrNew.Add(new SelectItem("SEMESTRAL", "Semestral"));
            arrNew.Add(new SelectItem("ADMISSIONAL", "Admissional"));
            arrNew.Add(new SelectItem("DEMISSIONAL", "Demissional"));
            arrNew.Add(new SelectItem("RETORNO AO TRABALHO", "Retorno ao Trabalho"));
            arrNew.Add(new SelectItem("MUDANÇA DE RISCO", "Mudança de Risco"));
            arrNew.Add(new SelectItem("OUTROS", "Outros"));

            periodicidade.DataSource = arrNew;
            periodicidade.DisplayMember = "Nome";
            periodicidade.ValueMember = "Valor";
            periodicidade.DropDownStyle = ComboBoxStyle.DropDownList;


        }

        public static void PeriodicidadeASo(ComboBox periodicidade)
        {
            ArrayList arrNew = new ArrayList();

            arrNew.Add(new SelectItem("0", "Todos"));
            arrNew.Add(new SelectItem("2", "Periódico"));
            //arrNew.Add(new SelectItem("1", "Bi-anual"));
            //arrNew.Add(new SelectItem("2", "Anual"));
            arrNew.Add(new SelectItem("3", "Semestral"));
            arrNew.Add(new SelectItem("4", "Admissional"));
            arrNew.Add(new SelectItem("5", "Demissional"));
            arrNew.Add(new SelectItem("6", "Retorno ao Trabalho"));
            arrNew.Add(new SelectItem("7", "Mudança de Risco"));
            arrNew.Add(new SelectItem("8", "Outros"));


            periodicidade.DataSource = arrNew;
            periodicidade.DisplayMember = "Nome";
            periodicidade.ValueMember = "Valor";
            periodicidade.DropDownStyle = ComboBoxStyle.DropDownList;


        }

        public static void TipoSague(ComboBox sangue)
        {
            ArrayList arrNew = new ArrayList();

            arrNew.Add(new SelectItem("", ""));
            arrNew.Add(new SelectItem("A", "A"));
            arrNew.Add(new SelectItem("B", "B"));
            arrNew.Add(new SelectItem("AB", "AB"));
            arrNew.Add(new SelectItem("O", "O"));
            
            sangue.DataSource = arrNew;
            sangue.DisplayMember = "Nome";
            sangue.ValueMember = "Valor";
            sangue.DropDownStyle = ComboBoxStyle.DropDownList;

        }

        public static void FatorRH(ComboBox fator)
        {
            ArrayList arrNew = new ArrayList();

            arrNew.Add(new SelectItem("", ""));
            arrNew.Add(new SelectItem("+", "Positivo"));
            arrNew.Add(new SelectItem("-", "Negativo"));

            fator.DataSource = arrNew;
            fator.DisplayMember = "Nome";
            fator.ValueMember = "Valor";
            fator.DropDownStyle = ComboBoxStyle.DropDownList;

        }

        public static void PeriodicidadeASoIncluir(ComboBox periodicidade)
        {
            ArrayList arrNew = new ArrayList();

            arrNew.Add(new SelectItem("0", ""));
            arrNew.Add(new SelectItem("2", "Periódico"));
            //arrNew.Add(new SelectItem("1", "Bi-Anual"));
            //arrNew.Add(new SelectItem("2", "Anual"));
            arrNew.Add(new SelectItem("3", "Semestral"));
            arrNew.Add(new SelectItem("4", "Admissional"));
            arrNew.Add(new SelectItem("5", "Demissional"));
            arrNew.Add(new SelectItem("6", "Retorno ao Trabalho"));
            arrNew.Add(new SelectItem("7", "Mudança de Risco"));
            arrNew.Add(new SelectItem("8", "Outros"));

            periodicidade.DataSource = arrNew;
            periodicidade.DisplayMember = "Nome";
            periodicidade.ValueMember = "Valor";
            periodicidade.DropDownStyle = ComboBoxStyle.DropDownList;


        }

        public static void situacaoMovimento(ComboBox periodicidade)
        {
            ArrayList arrNew = new ArrayList();

            arrNew.Add(new SelectItem("", ""));
            arrNew.Add(new SelectItem("C", "Cancelado"));
            arrNew.Add(new SelectItem("E", "Estornado"));
            arrNew.Add(new SelectItem("F", "Faturado"));
            arrNew.Add(new SelectItem("P", "Pendente"));

            periodicidade.DataSource = arrNew;
            periodicidade.DisplayMember = "Nome";
            periodicidade.ValueMember = "Valor";
            periodicidade.DropDownStyle = ComboBoxStyle.DropDownList;
        }


        public static void situacaoAso(ComboBox situacaoAso)
        {
            ArrayList arrNew = new ArrayList();

            arrNew.Add(new SelectItem("", "Todos"));
            arrNew.Add(new SelectItem("I", "Cancelado"));
            arrNew.Add(new SelectItem("F", "Finalizado"));
            arrNew.Add(new SelectItem("C", "Gravado"));
            arrNew.Add(new SelectItem("B", "Bloueado"));

            situacaoAso.DataSource = arrNew;
            situacaoAso.DisplayMember = "Nome";
            situacaoAso.ValueMember = "Valor";
            situacaoAso.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void tempoExposicao(ComboBox tempoExposicao)
        {
            ArrayList arrNew = new ArrayList();

            arrNew.Add(new SelectItem("0", "Não identificado"));
            arrNew.Add(new SelectItem("1", "Eventual"));
            arrNew.Add(new SelectItem("2", "Habitual"));
            arrNew.Add(new SelectItem("3", "Permanente"));
            arrNew.Add(new SelectItem("4", "Intermitente"));
            arrNew.Add(new SelectItem("5", "Habitual e Intermitente"));
            arrNew.Add(new SelectItem("6", "Habitual e Permanente"));

            tempoExposicao.DataSource = arrNew;
            tempoExposicao.DisplayMember = "Nome";
            tempoExposicao.ValueMember = "Valor";
            tempoExposicao.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void startSexoIncluir(ComboBox sexo)
        {
            ArrayList arrNew = new ArrayList();

            arrNew.Add(new SelectItem("", ""));
            arrNew.Add(new SelectItem("M", "Masculino"));
            arrNew.Add(new SelectItem("F", "Feminino"));

            sexo.DataSource = arrNew;
            sexo.DisplayMember = "Nome";
            sexo.ValueMember = "Valor";
            sexo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void startSexoAlterar(ComboBox sexo)
        {
            ArrayList arrNew = new ArrayList();

            arrNew.Add(new SelectItem("M", "Masculino"));
            arrNew.Add(new SelectItem("F", "Feminino"));

            sexo.DataSource = arrNew;
            sexo.DisplayMember = "Nome";
            sexo.ValueMember = "Valor";
            sexo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void startTipoAtendimento(ComboBox sexo)
        {
            ArrayList arrNew = new ArrayList();

            arrNew.Add(new SelectItem("A", "Não Atendido"));
            arrNew.Add(new SelectItem("F", "Finalizado"));
            arrNew.Add(new SelectItem("C", "Cancelado"));

            sexo.DataSource = arrNew;
            sexo.DisplayMember = "Nome";
            sexo.ValueMember = "Valor";
            sexo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void startSituacaoCobranca(ComboBox situacaoCobranca)
        {
            ArrayList arrNew = new ArrayList();

            arrNew.Add(new SelectItem("", ""));
            arrNew.Add(new SelectItem("A", "Aberta"));
            arrNew.Add(new SelectItem("B", "Baixada"));
            arrNew.Add(new SelectItem("C", "Cancelada"));
            arrNew.Add(new SelectItem("D", "Desdobrada"));

            situacaoCobranca.DataSource = arrNew;
            
            situacaoCobranca.DisplayMember = "Nome";
            situacaoCobranca.ValueMember = "Valor";
            
            situacaoCobranca.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void situacaoNotaFiscal(ComboBox situacaoAso)
        {
            ArrayList arrNew = new ArrayList();

            arrNew.Add(new SelectItem("", ""));
            arrNew.Add(new SelectItem("F", "Faturada"));
            arrNew.Add(new SelectItem("C", "Cancelada"));

            situacaoAso.DataSource = arrNew;
            situacaoAso.DisplayMember = "Nome";
            situacaoAso.ValueMember = "Valor";
            situacaoAso.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void iniciaComboTipoCliente(ComboBox combo)
        {
            ArrayList arr = new ArrayList();
            arr.Add(new SelectItem("N", "Padrão"));
            arr.Add(new SelectItem("U", "Unidade"));
            arr.Add(new SelectItem("C", "Credenciada"));

            combo.DataSource = arr;

            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";

            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void iniciaComboCidade(ComboBox cidade, String uf)
        {
            try
            {
                ClienteFacade clienteFacade = ClienteFacade.getInstance();
                
                cidade.DataSource = clienteFacade.findAllCidadeByUf(uf);
                cidade.DisplayMember = "Nome";
                cidade.ValueMember = "Valor";
                cidade.DropDownStyle = ComboBoxStyle.DropDownList;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        public static void situacao(ComboBox combo)
        {
            ArrayList arrNew = new ArrayList();

            arrNew.Add(new SelectItem("", ""));
            arrNew.Add(new SelectItem("F", "Finalizado"));
            arrNew.Add(new SelectItem("I", "Cancelado"));
            arrNew.Add(new SelectItem("C", "Construção"));


            combo.DataSource = arrNew;
            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";
            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void ItemProtocolo(ComboBox combo)
        {
            ArrayList arrNew = new ArrayList();

            arrNew.Add(new SelectItem("AS", "Atendimento"));
            arrNew.Add(new SelectItem("PC", "PCMSO"));
            arrNew.Add(new SelectItem("PP", "PPRA"));
            arrNew.Add(new SelectItem("P", "Produto"));
            arrNew.Add(new SelectItem("DP", "Documento"));
            
            combo.DataSource = arrNew;
            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";
            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void ItemProtocoloPesquisa(ComboBox combo)
        {
            ArrayList arrNew = new ArrayList();

            arrNew.Add(new SelectItem("AS", "Atendimento"));
            arrNew.Add(new SelectItem("PC", "PCMSO"));
            arrNew.Add(new SelectItem("PP", "PPRA"));
            arrNew.Add(new SelectItem("P", "Produto"));

            combo.DataSource = arrNew;
            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";
            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void comboSituacao(ComboBox combo)
        {
            ArrayList arrNew = new ArrayList();

            arrNew.Add(new SelectItem("", ""));
            arrNew.Add(new SelectItem("A", "Ativo"));
            arrNew.Add(new SelectItem("I", "Inativo"));

            combo.DataSource = arrNew;
            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";
            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void comboTipoClienteFilter(ComboBox combo)
        {
            ArrayList arr = new ArrayList();
            arr.Add(new SelectItem("", ""));
            arr.Add(new SelectItem("P", "Pessoa Jurídica / MEI"));
            arr.Add(new SelectItem("U", "Unidade"));
            arr.Add(new SelectItem("C", "Credenciada"));
            arr.Add(new SelectItem("F", "Pessoa Física / CEI"));

            combo.DataSource = arr;

            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";

            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void TipoPesquisaCliente(ComboBox combo)
        {
            ArrayList arr = new ArrayList();

            arr.Add(new SelectItem("R", "Razão Social"));
            arr.Add(new SelectItem("F", "Nome de Fantasia"));
            arr.Add(new SelectItem("C", "CNPJ"));
            arr.Add(new SelectItem("CP", "CPF"));

            combo.DataSource = arr;

            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";

            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void cbLassegue(ComboBox combo)
        {
            ArrayList arr = new ArrayList();

            arr.Add(new SelectItem("", ""));
            arr.Add(new SelectItem("P", "Positivo"));
            arr.Add(new SelectItem("N", "Negativo"));

            combo.DataSource = arr;

            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";

            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void cbContraceptivo(ComboBox combo)
        {
            ArrayList arr = new ArrayList();

            arr.Add(new SelectItem("", "Nenhum"));
            arr.Add(new SelectItem("ANTICONCEPCIONAL", "Anticoncepcional"));
            arr.Add(new SelectItem("DIAFRAGMA", "Diafragma"));
            arr.Add(new SelectItem("DIU", "DIU"));
            arr.Add(new SelectItem("PRESERVATIVO", "Preservativo"));
            arr.Add(new SelectItem("OUTROS", "Outros"));

            combo.DataSource = arr;

            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";

            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void cbEstadoContrato(ComboBox combo)
        {
            ArrayList arr = new ArrayList();

            arr.Add(new SelectItem("", ""));
            arr.Add(new SelectItem("F", "Fechado"));
            arr.Add(new SelectItem("E", "Encerrado"));
            arr.Add(new SelectItem("C", "Cancelado"));
            arr.Add(new SelectItem("G", "Gravado"));

            combo.DataSource = arr;

            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";

            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void tipoPesquisaContrato(ComboBox combo)
        {
            ArrayList arr = new ArrayList();

            arr.Add(new SelectItem("T", "Código de Contrato"));
            arr.Add(new SelectItem("C", "CNPJ"));
            arr.Add(new SelectItem("F", "CPF"));

            combo.DataSource = arr;

            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";

            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        /// <summary>
        /// Combo que define a prioridade do e-mail.
        /// </summary>
        /// <param name="combo"></param>
        public static void prioridadeEmail(ComboBox combo)
        {
            ArrayList arr = new ArrayList();

            arr.Add(new SelectItem("Normal", "Normal"));
            arr.Add(new SelectItem("Low", "Baixa"));
            arr.Add(new SelectItem("High", "Alta"));

            combo.DataSource = arr;

            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";

            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        /// <summary>
        /// Combo que define o flag contrato para o cliente
        /// </summary>
        /// <param name="combo"> comboBox</param>

        public static void booleanTodos(ComboBox combo)
        {
            ArrayList arr = new ArrayList();

            arr.Add(new SelectItem("", "Todos"));
            arr.Add(new SelectItem("true", "Sim"));
            arr.Add(new SelectItem("false", "Não"));

            combo.DataSource = arr;

            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";

            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void pesquisaFuncionario(ComboBox combo)
        {
            ArrayList arr = new ArrayList();

            arr.Add(new SelectItem("N", "Nome"));
            arr.Add(new SelectItem("C", "CPF"));
            arr.Add(new SelectItem("R", "RG"));

            combo.DataSource = null;
            combo.DataSource = arr;

            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";

            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void Boolean(ComboBox combo, Boolean padraoDefault)
        {
            ArrayList arr = new ArrayList();

            if (padraoDefault)
            {
                arr.Add(new SelectItem("true", "Sim"));
                arr.Add(new SelectItem("false", "Não"));
            }
            else
            {
                arr.Add(new SelectItem("false", "Não"));
                arr.Add(new SelectItem("true", "Sim"));
            }
            
            
            combo.DataSource = arr;


            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";

            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }


        public static void findTipoContato (ComboBox combo)
        {
            try
            {
                ClienteFacade clienteFacade = ClienteFacade.getInstance();

                ArrayList arr = new ArrayList();

                DataSet ds = clienteFacade.findTipoContato();

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    arr.Add(new SelectItem(row["id"].ToString(), row["descricao"].ToString()));
                }

                combo.DataSource = arr;
                combo.DisplayMember = "Nome";
                combo.ValueMember = "Valor";
                combo.DropDownStyle = ComboBoxStyle.DropDownList;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atençao", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        
        public static void brPdh(ComboBox combo)
        {
            ArrayList arr = new ArrayList();

            arr.Add(new SelectItem("", ""));
            arr.Add(new SelectItem("BR", "Beneficiário reabilitado"));
            arr.Add(new SelectItem("PDH", "Portador de deficiência habilitado"));

            combo.DataSource = arr;

            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";

            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void tipoImpressora(ComboBox combo)
        {
            ArrayList arr = new ArrayList();

            arr.Add(new SelectItem("T", "Térmica"));
            arr.Add(new SelectItem("P", "Padrão"));

            combo.DataSource = arr;

            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";

            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void carregaComboRisco(ComboBox combo)
        {
            try
            {
                EstudoFacade pcmsoFacade = EstudoFacade.getInstance();

                ArrayList arr = new ArrayList();
                arr.Add(new SelectItem("", ""));

                foreach (DataRow row in pcmsoFacade.findRisco().Tables[0].Rows)
                    arr.Add(new SelectItem(row["id_risco"].ToString(), row["descricao"].ToString()));

                combo.DataSource = arr;
                        
                combo.DisplayMember = "nome";
                combo.ValueMember = "valor";
                combo.DropDownStyle = ComboBoxStyle.DropDownList;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static void tipoContrato(ComboBox combo)
        {
            ArrayList arr = new ArrayList();

            arr.Add(new SelectItem("C", "Contrato"));
            arr.Add(new SelectItem("P", "Proposta"));

            combo.DataSource = arr;

            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";

            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void comboTipoCliente(ComboBox combo)
        {
            List<SelectItem> List = new List<SelectItem>();
            combo.DropDownStyle = ComboBoxStyle.DropDownList;
            
            combo.Items.Add(new SelectItem("P", "Pessoa Jurídica/MEI"));
            combo.Items.Add(new SelectItem("U", "Unidade"));
            combo.Items.Add(new SelectItem("C", "Credenciada"));
            combo.Items.Add(new SelectItem("F", "Pessoa Física/CEI"));

            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";
            
        }

        public static void atendimentoFila(ComboBox combo)
        {
            ArrayList arr = new ArrayList();

            arr.Add(new SelectItem("P", "Pendente"));
            arr.Add(new SelectItem("R", "Realizado"));

            combo.DataSource = arr;

            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";

            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void tipoRelatorio(ComboBox combo)
        {
            ArrayList arr = new ArrayList();

            arr.Add(new SelectItem("S", "Sintético"));
            arr.Add(new SelectItem("SS", "Sintético Simplificado"));
            arr.Add(new SelectItem("A", "Analítico"));
            arr.Add(new SelectItem("GA", "Geração de Arquivo XLSX"));
            arr.Add(new SelectItem("FE", "Financeiro Empresas"));

            combo.DataSource = arr;

            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";

            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void situacaoMovimentoRelatorio(ComboBox combo)
        {
            ArrayList arr = new ArrayList();

            arr.Add(new SelectItem("P", "Não Faturado"));
            arr.Add(new SelectItem("F", "Faturado"));
            arr.Add(new SelectItem("", "Todos"));

            combo.DataSource = arr;

            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";

            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void analiseRelatorioFinanceiro(ComboBox combo)
        {
            ArrayList arr = new ArrayList();

            arr.Add(new SelectItem("E", "Exames"));
            arr.Add(new SelectItem("P", "Produtos"));
            arr.Add(new SelectItem("", "Exames + Produtos"));

            combo.DataSource = arr;

            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";

            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void ordenacaoRelatorioMovimento(ComboBox combo)
        {
            ArrayList arr = new ArrayList();

            arr.Add(new SelectItem("A", "Curva ABC"));
            arr.Add(new SelectItem("D", "Descrição Ascendente"));
            arr.Add(new SelectItem("", "Sem Ordenação"));

            combo.DataSource = arr;

            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";

            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void periodoVencimentoCombo(ComboBox combo)
        {
            ArrayList arr = new ArrayList();

            arr.Add(new SelectItem("6", "6 meses")); // semestral
            arr.Add(new SelectItem("12", "12 meses")); // anual
            arr.Add(new SelectItem("24", "24 meses")); // bienal
            arr.Add(new SelectItem("36", "36 meses")); // trienal

            combo.DataSource = arr;

            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";

            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void periodoVencimentoCombo(DataGridViewComboBoxColumn combo)
        {
            ArrayList arr = new ArrayList();

            arr.Add(new SelectItem("", "0 meses")); 
            arr.Add(new SelectItem("6", "6 meses")); 
            arr.Add(new SelectItem("12", "12 meses")); 
            arr.Add(new SelectItem("24", "24 meses")); 
            arr.Add(new SelectItem("36", "36 meses")); 

            combo.DataSource = arr;

            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";

        }

        public static void situacaoAtendimentoRelatorioAtendimento(ComboBox combo)
        {
            ArrayList arr = new ArrayList();
            arr.Add(new SelectItem("T", "Todos"));
            arr.Add(new SelectItem("C", "Pendentes"));
            arr.Add(new SelectItem("F", "Finalizados"));
            arr.Add(new SelectItem("I", "Cancelados"));

            combo.DataSource = arr;
            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";
        }

        public static void situacaoExameRelatorioAtendimento(ComboBox combo)
        {
            ArrayList arr = new ArrayList();
            arr.Add(new SelectItem("T", "Todos"));
            arr.Add(new SelectItem("A", "Atendido"));
            arr.Add(new SelectItem("P", "Não atendido"));

            combo.DataSource = arr;
            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";
        }

        public static void tipoAtendimentoRelatorioAtendimento(ComboBox combo)
        {
            ArrayList arr = new ArrayList();
            arr.Add(new SelectItem("T", "todos"));
            arr.Add(new SelectItem("PER", "Periódico"));
            arr.Add(new SelectItem("ADM", "Admissional"));
            arr.Add(new SelectItem("DEM", "Demissional"));
            arr.Add(new SelectItem("MUD", "Mudança de Risco"));
            arr.Add(new SelectItem("RET", "Retorno ao trabalho"));
            arr.Add(new SelectItem("OUT", "Outros"));

            combo.DataSource = arr;
            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";
        }

        public static void tipoRelatorioRelatorioAtendimento(ComboBox combo)
        {
            ArrayList arr = new ArrayList();
            arr.Add(new SelectItem("RELATORIO_ATENDIMENTO.jasper", "Simplificado"));
            arr.Add(new SelectItem("RELATORIO_ATENDIMENTO_SIMPLIFICADO_COLABORADOR.jasper", "Simplificado por funcionário"));
            arr.Add(new SelectItem("RELATORIO_ATENDIMENTO_ANALITICO.jasper", "Detalhado"));
            arr.Add(new SelectItem("RELATORIO_ATENDIMENTO_ESTATISTICO.jasper", "Estatístico"));
            arr.Add(new SelectItem("RELATORIO_ATENDIMENTO_UNIDADE.jasper", "Detalhado matriz/unidade"));
            arr.Add(new SelectItem("RELATORIO_ATENDIMENTO_DIA.jasper", "Atendimento por dia"));
            arr.Add(new SelectItem("RELATORIO_ATENDIMENTO_SETOR.jasper", "Setor"));

            combo.DataSource = arr;
            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";
        }

        public static void identidadeOrgaoEmissor(ComboBox combo)
        {
            ArrayList arr = new ArrayList();

            arr.Add(new SelectItem("", ""));
            arr.Add(new SelectItem("1", "Conselho Regional de Medicina CRM"));
            arr.Add(new SelectItem("4", "Conselho de Engenharia Agronomia CREA"));
            arr.Add(new SelectItem("9", "Outros"));

            combo.DataSource = arr;
            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";
        }

        public static void tipoAvaliacao(ComboBox combo)
        {
            ArrayList arr = new ArrayList();

            arr.Add(new SelectItem("1", "Quantitativa"));
            arr.Add(new SelectItem("2", "Qualitativa"));

            combo.DataSource = arr;
            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";
        }

        public static void unidadeMedicaoAgente(ComboBox combo)
        {
            ArrayList arr = new ArrayList();

            arr.Add(new SelectItem("", ""));
            arr.Add(new SelectItem("1", "dose diária de ruído"));
            arr.Add(new SelectItem("2", "decibel linear (dB (linear))"));
            arr.Add(new SelectItem("3", "decibel (C) (dB(C))"));
            arr.Add(new SelectItem("4", "decibel (A) (dB(A))"));
            arr.Add(new SelectItem("5", "metro por segundo ao quadrado (m/s2)"));
            arr.Add(new SelectItem("6", "metro por segundo elevado a 1,75 (m/s1,75)"));
            arr.Add(new SelectItem("7", "parte de vapor ou gás por milhão de partes de ar contaminado (ppm)"));
            arr.Add(new SelectItem("8", "miligrama por metro cúbico de ar (mg/m3)"));
            arr.Add(new SelectItem("9", "fibra por centímetro cúbico (f/cm3)"));
            arr.Add(new SelectItem("10", "grau Celsius (ºC)"));
            arr.Add(new SelectItem("11", "metro por segundo (m/s)"));
            arr.Add(new SelectItem("12", "porcentual"));
            arr.Add(new SelectItem("13", "lux (lx)"));
            arr.Add(new SelectItem("14", "unidade formadora de colônias por metro cúbico (ufc/m3)"));
            arr.Add(new SelectItem("15", "dose diária"));
            arr.Add(new SelectItem("16", "dose mensal"));
            arr.Add(new SelectItem("17", "dose trimestral"));
            arr.Add(new SelectItem("18", "dose anual"));
            arr.Add(new SelectItem("19", "watt por metro quadrado (W/m2)"));
            arr.Add(new SelectItem("20", "ampère por metro (A/m)"));
            arr.Add(new SelectItem("21", "militesla (mT)"));
            arr.Add(new SelectItem("22", "microtesla (μT)"));
            arr.Add(new SelectItem("23", "miliampère (mA)"));
            arr.Add(new SelectItem("24", "quilovolt por metro (kV/m)"));
            arr.Add(new SelectItem("25", "volt por metro (V/m)"));
            arr.Add(new SelectItem("26", "joule por metro quadrado (J/m2)"));
            arr.Add(new SelectItem("27", "milijoule por centímetro quadrado (mJ/cm2)"));
            arr.Add(new SelectItem("28", "milisievert (mSv)"));
            arr.Add(new SelectItem("29", "milhão de partículas por decímetro cúbico (mppdc)"));
            arr.Add(new SelectItem("30", "umidade relativa do ar (UR (%))"));



            combo.DataSource = arr;
            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";
        }


        public static void utilizaEPC(ComboBox combo)
        {
            ArrayList arr = new ArrayList();

            arr.Add(new SelectItem("0", "Não se aplica"));
            arr.Add(new SelectItem("1", "Não implementa"));
            arr.Add(new SelectItem("2", "Implementa"));

            combo.DataSource = arr;
            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";
        }

        public static void utilizaEPI(ComboBox combo)
        {
            ArrayList arr = new ArrayList();

            arr.Add(new SelectItem("0", "Não se aplica"));
            arr.Add(new SelectItem("1", "Não utilizado"));
            arr.Add(new SelectItem("2", "Utilizado"));

            combo.DataSource = arr;
            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";
        }

        public static void booleandoEsocial(ComboBox combo)
        {
            ArrayList arr = new ArrayList();

            arr.Add(new SelectItem("N", "Não"));
            arr.Add(new SelectItem("S", "Sim"));

            combo.DataSource = arr;
            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";
        }

        public static void booleanoEsocialSemDefault(ComboBox combo)
        {
            ArrayList arr = new ArrayList();

            arr.Add(new SelectItem("", ""));
            arr.Add(new SelectItem("S", "Sim"));
            arr.Add(new SelectItem("N", "Não"));
            
            combo.DataSource = arr;
            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";
        }

        public static void tipoRelatorioCliente(ComboBox combo)
        {
            ArrayList arr = new ArrayList();

            arr.Add(new SelectItem("ANALITICO", "Analítico"));
            arr.Add(new SelectItem("SINTETICO", "Sintético"));
            arr.Add(new SelectItem("INDIVIDUAL", "Individual"));
            arr.Add(new SelectItem("RELATORIO_CLIENTE_FUNCOES.JASPER", "Por Função"));

            combo.DataSource = arr;

            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";

            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void situacaoEstudo(ComboBox combo)
        {
            ArrayList arr = new ArrayList();

            arr.Add(new SelectItem("", "Todos"));
            arr.Add(new SelectItem("C", "Em construção"));
            arr.Add(new SelectItem("A", "Em análise"));
            arr.Add(new SelectItem("F", "Finalizado"));
            arr.Add(new SelectItem("I", "Excluídos"));
            arr.Add(new SelectItem("T", "Avulsos"));
            
            combo.DataSource = arr;

            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";

            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }


        public static void comboMes(ComboBox comboUf)
        {
            ArrayList arrNew = new ArrayList();

            arrNew.Add(new SelectItem("1", "Janeiro"));
            arrNew.Add(new SelectItem("2", "Fevereiro"));
            arrNew.Add(new SelectItem("3", "Março"));
            arrNew.Add(new SelectItem("4", "Abril"));
            arrNew.Add(new SelectItem("5", "Maio"));
            arrNew.Add(new SelectItem("6", "Junho"));
            arrNew.Add(new SelectItem("7", "Julho"));
            arrNew.Add(new SelectItem("8", "Agosto"));
            arrNew.Add(new SelectItem("9", "Setembro"));
            arrNew.Add(new SelectItem("10", "Outubro"));
            arrNew.Add(new SelectItem("11", "Novembro"));
            arrNew.Add(new SelectItem("12", "Dezembro"));

            comboUf.DataSource = arrNew;
            comboUf.DisplayMember = "Nome";
            comboUf.ValueMember = "Valor";
            comboUf.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void comboAno(ComboBox comboUf)
        {
            ArrayList arrNew = new ArrayList();

            arrNew.Add(new SelectItem("2019", "2019"));
            arrNew.Add(new SelectItem("2020", "2020"));
            arrNew.Add(new SelectItem("2021", "2021"));
            arrNew.Add(new SelectItem("2022", "2022"));
            arrNew.Add(new SelectItem("2023", "2023"));
            arrNew.Add(new SelectItem("2024", "2024"));
            arrNew.Add(new SelectItem("2025", "2025"));
            arrNew.Add(new SelectItem("2026", "2026"));
            arrNew.Add(new SelectItem("2027", "2027"));
            arrNew.Add(new SelectItem("2028", "2028"));
            arrNew.Add(new SelectItem("2029", "2029"));
            arrNew.Add(new SelectItem("2030", "2030"));
            arrNew.Add(new SelectItem("2031", "2031"));
            arrNew.Add(new SelectItem("2032", "2032"));
            arrNew.Add(new SelectItem("2033", "2033"));
            arrNew.Add(new SelectItem("2034", "2034"));
            arrNew.Add(new SelectItem("2035", "2035"));
            arrNew.Add(new SelectItem("2036", "2036"));
            arrNew.Add(new SelectItem("2037", "2037"));
            arrNew.Add(new SelectItem("2038", "2038"));
            arrNew.Add(new SelectItem("2039", "2039"));
            arrNew.Add(new SelectItem("2040", "2040"));

            comboUf.DataSource = arrNew;
            comboUf.DisplayMember = "Nome";
            comboUf.ValueMember = "Valor";
            comboUf.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public static void classificacaoExame(ComboBox combo)
        {
            ArrayList arr = new ArrayList();

            arr.Add(new SelectItem("false", "Todos"));
            arr.Add(new SelectItem("true", "Somente laboratório"));

            combo.DataSource = arr;
            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";
        }

        public static void tipoEvento(ComboBox combo)
        {
            ArrayList arr = new ArrayList();

            arr.Add(new SelectItem("2220", "2220"));
            arr.Add(new SelectItem("2240", "2240"));

            combo.DataSource = arr;
            combo.DisplayMember = "Nome";
            combo.ValueMember = "Valor";
        }
        
    }

    
}
