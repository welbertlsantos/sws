﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWS.ViewHelper
{
    class EncodingHelper
    {
        public static string RemoverAcentos(string input)
        {
            if (string.IsNullOrEmpty(input))
                return "";
            else
            {
                byte[] bytes = System.Text.Encoding.GetEncoding("iso-8859-8").GetBytes(input);
                return System.Text.Encoding.UTF8.GetString(bytes);
            }
        }

    }
}
