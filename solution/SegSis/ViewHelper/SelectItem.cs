﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace SWS.ViewHelper
{
    public class SelectItem
    {
        String nome;
        String valor;

        public SelectItem(String valor, String nome)
        {
            Valor = valor;
            Nome = nome;
        }

        public SelectItem() { }

        public String Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        public string Valor
        {
            get { return valor; }
            set { valor = value; }
        }

    }
}
